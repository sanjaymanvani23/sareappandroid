package com.e.vediotrimmer.widget

import android.animation.ValueAnimator
import android.animation.ValueAnimator.AnimatorUpdateListener
import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.os.CountDownTimer
import android.os.Handler
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.e.vediotrimmer.R
import com.e.vediotrimmer.adapter.VideoFrameTrimAdapter
import com.e.vediotrimmer.adapter.VideoTrimAdapter
import com.e.vediotrimmer.interfaces.IVideoTrimmerView
import com.e.vediotrimmer.interfaces.VideoTrimListener
import com.e.vediotrimmer.utils.SliderLayoutManager
import com.e.vediotrimmer.utils.StorageUtil
import com.e.vediotrimmer.utils.Utils
import com.e.vediotrimmer.utils.VideoTrimmerUtil
import com.e.vediotrimmer.widget.RangeSeekBarView.OnRangeSeekBarChangeListener
import com.e.vediotrimmer.widget.VideoTrimmerView
import iknow.android.utils.thread.BackgroundExecutor
import iknow.android.utils.thread.UiThreadExecutor
import java.io.File
import java.util.concurrent.TimeUnit

class VideoTrimmerView @JvmOverloads constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr), IVideoTrimmerView {
    private val mMaxWidth = VideoTrimmerUtil.VIDEO_FRAMES_WIDTH
    private var mContext: Context? = null
    private var mLinearVideo: RelativeLayout? = null
    private var mVideoView: VideoView? = null
    private var mPlayView: ImageView? = null
    private var mVideoThumbRecyclerView: RecyclerView? = null
    private var video_recyclerView: RecyclerView? = null

    private var mRangeSeekBarView: RangeSeekBarView? = null
    private var mSeekBarLayout: LinearLayout? = null
    private var mRedProgressIcon: ImageView? = null
    private var btn_next: Button? = null
    private var back: ImageView? = null
    private var txtStartDuration: TextView? = null
    private var txtEndDuration: TextView? = null

    private var mAverageMsPx //
            = 0f
    private var averagePxMs //
            = 0f
    private var mSourceUri: Uri? = null
    private var mOnTrimVideoListener: VideoTrimListener? = null
    private var mDuration = 0
    private var mVideoThumbAdapter: VideoTrimAdapter? = null
    private var videoFrameTrimAdapter: VideoFrameTrimAdapter? = null
    private var restoreState = false

    //new
    private var mLeftProgressPos: Long = 0
    private var mRightProgressPos: Long = 0
    private var mRedProgressBarPos: Long = 0
    private var scrollPos: Long = 0
    private val mScaledTouchSlop = 0
    private var lastScrollX = 0
    private var isSeeking = false
    private var isOverScaledTouchSlop = false
    private var mThumbsTotalCount = 0
    private var mRedProgressAnimator: ValueAnimator? = null
    private val mAnimationHandler = Handler()
    private fun init(context: Context) {
        mContext = context
        LayoutInflater.from(context).inflate(R.layout.video_trimmer_view, this, true)
        video_recyclerView = findViewById(R.id.video_recyclerView)
        txtEndDuration = findViewById(R.id.txtEndDuration)
        txtStartDuration = findViewById(R.id.txtStartDuration)
        mLinearVideo = findViewById(R.id.layout_surface_view)
        mVideoView = findViewById(R.id.video_loader)
        mPlayView = findViewById(R.id.icon_video_play)
        mSeekBarLayout = findViewById(R.id.seekBarLayout)
        mRedProgressIcon = findViewById(R.id.positionIcon)
        mVideoThumbRecyclerView = findViewById(R.id.video_frames_recyclerView)
        mVideoThumbRecyclerView!!.layoutManager = LinearLayoutManager(
            mContext, LinearLayoutManager.HORIZONTAL, false)
        mVideoThumbAdapter = VideoTrimAdapter(mContext)
        mVideoThumbRecyclerView!!.adapter = mVideoThumbAdapter
        mVideoThumbRecyclerView!!.addOnScrollListener(mOnScrollListener)
        back = findViewById(R.id.back)
        btn_next = findViewById(R.id.btn_next)

        // Setting the padding such that the items will appear in the middle of the screen
        val padding: Int = Utils.getScreenWidth(context) / 2 - Utils.dpToPx(context, 10f)
        video_recyclerView?.setPadding(padding + 10, 0, padding, 0)


        video_recyclerView?.layoutManager = SliderLayoutManager(context).apply {
            callback = object : SliderLayoutManager.OnItemSelectedListener {
                override fun onItemSelected(layoutPosition: Int) {

                }
            }
        }

        videoFrameTrimAdapter = VideoFrameTrimAdapter(mContext)
        video_recyclerView!!.adapter = videoFrameTrimAdapter
        video_recyclerView?.isNestedScrollingEnabled = false
        setUpListeners()

        video_recyclerView?.smoothScrollToPosition(5)

        video_recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                try {

                } catch (e: Exception) {
                }

            }
        })
    }

    private fun initRangeSeekBarView() {
        if (mRangeSeekBarView != null) return
        val rangeWidth: Int
        mLeftProgressPos = 0
        if (mDuration <= VideoTrimmerUtil.MAX_SHOOT_DURATION) {
            mThumbsTotalCount = VideoTrimmerUtil.MAX_COUNT_RANGE
            rangeWidth = mMaxWidth
            mRightProgressPos = mDuration.toLong()
        } else {
            mThumbsTotalCount = (mDuration * 1.0f / (VideoTrimmerUtil.MAX_SHOOT_DURATION * 1.0f) * VideoTrimmerUtil.MAX_COUNT_RANGE).toInt()
            rangeWidth = mMaxWidth / VideoTrimmerUtil.MAX_COUNT_RANGE * mThumbsTotalCount
            mRightProgressPos = VideoTrimmerUtil.MAX_SHOOT_DURATION
        }
        mVideoThumbRecyclerView!!.addItemDecoration(
            SpacesItemDecoration2(
                VideoTrimmerUtil.RECYCLER_VIEW_PADDING, mThumbsTotalCount))

        mRangeSeekBarView = RangeSeekBarView(mContext, mLeftProgressPos, mRightProgressPos)
        mRangeSeekBarView!!.selectedMinValue = mLeftProgressPos
        mRangeSeekBarView!!.selectedMaxValue = mRightProgressPos
        mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
        mRangeSeekBarView!!.setMinShootTime(VideoTrimmerUtil.MIN_SHOOT_DURATION)
        mRangeSeekBarView!!.isNotifyWhileDragging = true
        mRangeSeekBarView!!.setOnRangeSeekBarChangeListener(mOnRangeSeekBarChangeListener)
        mSeekBarLayout!!.addView(mRangeSeekBarView)
        mAverageMsPx = mDuration * 1.0f / rangeWidth * 1.0f
        averagePxMs = mMaxWidth * 1.0f / (mRightProgressPos - mLeftProgressPos)
    }

    fun initVideoByURI(videoURI: Uri?) {
        mSourceUri = videoURI
        mVideoView!!.setVideoURI(videoURI)
        mVideoView!!.requestFocus()
    }

    private fun startShootVideoThumbs(context: Context?,
        videoUri: Uri?,
        totalThumbsCount: Int,
        startPosition: Long,
        endPosition: Long) {
        VideoTrimmerUtil.shootVideoThumbInBackground(
            context, videoUri, totalThumbsCount, startPosition, endPosition) { bitmap, interval ->
            if (bitmap != null) {
                UiThreadExecutor.runTask("", {
                    mVideoThumbAdapter!!.addBitmaps(bitmap)
                    // videoFrameTrimAdapter!!.addBitmaps(bitmap)
                }, 0L)
            }
        }
    }

    private fun startShootVideoThumbs1(context: Context?,
        videoUri: Uri?,
        totalThumbsCount: Int,
        startPosition: Long,
        endPosition: Long) {
        VideoTrimmerUtil.shootVideoThumbInBackground(
            context, videoUri, totalThumbsCount, startPosition, endPosition) { bitmap, interval ->
            if (bitmap != null) {
                UiThreadExecutor.runTask("", {
                    videoFrameTrimAdapter!!.addBitmaps(bitmap)
                }, 0L)
            }
        }
    }

    private fun onCancelClicked() {
        mOnTrimVideoListener!!.onCancel()
    }

    private fun videoPrepared(mp: MediaPlayer) {
        val lp = mLinearVideo!!.layoutParams
        val videoWidth = mp.videoWidth
        val videoHeight = mp.videoHeight
        val videoProportion = videoWidth.toFloat() / videoHeight.toFloat()
        val screenWidth = mLinearVideo!!.width
        val screenHeight = mLinearVideo!!.height
        if (videoHeight > videoWidth) {
            lp.width = screenWidth
            lp.height = screenHeight
        } else {
            lp.width = screenWidth
            val r = videoHeight / videoWidth.toFloat()
            lp.height = (lp.width * r).toInt()
        }
        mVideoView!!.layoutParams = lp
        mDuration = mVideoView!!.duration
        txtEndDuration!!.text =secToTime(mDuration.toLong())
        if (!restoreState) {
            seekTo(mRedProgressBarPos)
        } else {
            restoreState = false
            seekTo(mRedProgressBarPos)
        }
        initRangeSeekBarView()
        Log.e("mDuration  ::", "$mDuration")
        startShootVideoThumbs(mContext, mSourceUri, mThumbsTotalCount, 0, mDuration.toLong())
        startShootVideoThumbs1(
            mContext, mSourceUri, (mDuration.toLong() / 1000).toInt(), 0, mDuration.toLong())
    }

    private fun videoCompleted() {
        seekTo(mLeftProgressPos)
        setPlayPauseViewIcon(false)
    }

    private fun onVideoReset() {
        mVideoView!!.pause()
        setPlayPauseViewIcon(false)
    }

    private fun playVideoOrPause() {
        mRedProgressBarPos = mVideoView!!.currentPosition.toLong()
        if (mVideoView!!.isPlaying) {
            mVideoView!!.pause()
            pauseRedProgressAnimation()
        } else {
            mVideoView!!.start()
            playingRedProgressAnimation()
            onSplashHandler()
        }
        setPlayPauseViewIcon(mVideoView!!.isPlaying)
    }

    fun onVideoPause() {
        if (mVideoView!!.isPlaying) {
            seekTo(mLeftProgressPos)
            mVideoView!!.pause()
            setPlayPauseViewIcon(false)
            mRedProgressIcon!!.visibility = GONE
        }
    }

    fun setOnTrimVideoListener(onTrimVideoListener: VideoTrimListener?) {
        mOnTrimVideoListener = onTrimVideoListener
    }

    private fun setUpListeners() {
        back!!.setOnClickListener { onCancelClicked() }
        btn_next!!.setOnClickListener { onSaveClicked() }
        mVideoView!!.setOnPreparedListener { mp ->
            mp.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT)
            videoPrepared(mp)
        }
        mVideoView!!.setOnCompletionListener { videoCompleted() }
        mPlayView!!.setOnClickListener { playVideoOrPause() }
    }

    private fun onSaveClicked() {
        mVideoView!!.pause()
        val file = File(mSourceUri!!.path)
        VideoTrimmerUtil.startTrim(
            file, StorageUtil.getAppDataDir(), mLeftProgressPos, mRightProgressPos, mOnTrimVideoListener!!)
    }

    private fun seekTo(msec: Long) {
        mVideoView!!.seekTo(msec.toInt())
        Log.d(TAG, "seekTo = $msec")
    }

    private fun setPlayPauseViewIcon(isPlaying: Boolean) {
        mPlayView!!.setImageResource(if (isPlaying) R.drawable.ic_edit_pause else R.drawable.ic_edit_replay)
    }

    private val mOnRangeSeekBarChangeListener = OnRangeSeekBarChangeListener { bar, minValue, maxValue, action, isMin, pressedThumb ->
        Log.e(TAG, "-----minValue----->>>>>>$minValue")
        Log.e(TAG, "-----maxValue----->>>>>>$maxValue")
        mLeftProgressPos = minValue + scrollPos
        mRedProgressBarPos = mLeftProgressPos
        mRightProgressPos = maxValue + scrollPos
        Log.e(TAG, "-----mLeftProgressPos----->>>>>>$mLeftProgressPos")
        Log.e(TAG, "-----mRightProgressPos----->>>>>>$mRightProgressPos")
        when (action) {
            MotionEvent.ACTION_DOWN -> isSeeking = false
            MotionEvent.ACTION_MOVE -> {
                isSeeking = true
                seekTo((if (pressedThumb == RangeSeekBarView.Thumb.MIN) mLeftProgressPos else mRightProgressPos))
            }
            MotionEvent.ACTION_UP -> {
                isSeeking = false
                seekTo(mLeftProgressPos)
            }
            else -> {
            }
        }
        mDuration = mVideoView!!.duration
        onVideoReset()
        mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)


        Log.e(TAG, "-----mLeftProgressPos----->>>>>>$mLeftProgressPos")
        Log.e(TAG, "-----mRightProgressPos----->>>>>>$mRightProgressPos")
    }
    private val mOnScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            Log.d(TAG, "newState = $newState")
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            isSeeking = false
            val scrollX = calcScrollXDistance()
            if (Math.abs(lastScrollX - scrollX) < mScaledTouchSlop) {
                isOverScaledTouchSlop = false
                return
            }
            isOverScaledTouchSlop = true
            if (scrollX == -VideoTrimmerUtil.RECYCLER_VIEW_PADDING) {
                scrollPos = 0
            } else {
                isSeeking = true
                scrollPos = (mAverageMsPx * (VideoTrimmerUtil.RECYCLER_VIEW_PADDING + scrollX)).toLong()
                mLeftProgressPos = mRangeSeekBarView!!.selectedMinValue + scrollPos
                mRightProgressPos = mRangeSeekBarView!!.selectedMaxValue + scrollPos
                Log.d(TAG, "onScrolled >>>> mLeftProgressPos = $mLeftProgressPos")
                mRedProgressBarPos = mLeftProgressPos
                if (mVideoView!!.isPlaying) {
                    mVideoView!!.pause()
                    setPlayPauseViewIcon(false)
                }
                mRedProgressIcon!!.visibility = GONE
                seekTo(mLeftProgressPos)
                mRangeSeekBarView!!.setStartEndTime(mLeftProgressPos, mRightProgressPos)
                mRangeSeekBarView!!.invalidate()
            }
            lastScrollX = scrollX
        }
    }

    private fun calcScrollXDistance(): Int {
        val layoutManager = mVideoThumbRecyclerView!!.layoutManager as LinearLayoutManager?
        val position = layoutManager!!.findFirstVisibleItemPosition()
        val firstVisibleChildView = layoutManager.findViewByPosition(position)
        val itemWidth = firstVisibleChildView!!.width
        return position * itemWidth - firstVisibleChildView.left
    }

    private fun playingRedProgressAnimation() {
        pauseRedProgressAnimation()
        playingAnimation()
        mAnimationHandler.post(mAnimationRunnable)
    }

    private fun playingAnimation() {
        if (mRedProgressIcon!!.visibility == GONE) {
            mRedProgressIcon!!.visibility = VISIBLE
        }
        val params = mRedProgressIcon!!.layoutParams as LayoutParams
        val start = (VideoTrimmerUtil.RECYCLER_VIEW_PADDING + (mRedProgressBarPos - scrollPos) * averagePxMs).toInt()
        val end = (VideoTrimmerUtil.RECYCLER_VIEW_PADDING + (mRightProgressPos - scrollPos) * averagePxMs).toInt()
        mRedProgressAnimator = ValueAnimator.ofInt(start, end)
            .setDuration(mRightProgressPos - scrollPos - (mRedProgressBarPos - scrollPos))
        mRedProgressAnimator!!.interpolator = LinearInterpolator()
        mRedProgressAnimator!!.addUpdateListener(AnimatorUpdateListener { animation ->
            params.leftMargin = animation.animatedValue as Int
            mRedProgressIcon!!.layoutParams = params
            Log.d(TAG, "----onAnimationUpdate--->>>>>>>$mRedProgressBarPos")
        })
        mRedProgressAnimator!!.start()
    }

    private fun pauseRedProgressAnimation() {
        mRedProgressIcon!!.clearAnimation()
        if (mRedProgressAnimator != null && mRedProgressAnimator!!.isRunning) {
            mAnimationHandler.removeCallbacks(mAnimationRunnable)
            mRedProgressAnimator!!.cancel()
        }
    }

    private val mAnimationRunnable = Runnable {
        updateVideoProgress()
    }

    private fun updateVideoProgress() {
        val currentPosition = mVideoView!!.currentPosition.toLong()
        Log.e(TAG, "updateVideoProgress currentPosition = ${currentPosition / 1000}")
        txtStartDuration!!.text = secToTime(currentPosition)
        if (currentPosition >= mRightProgressPos) {
            mRedProgressBarPos = mLeftProgressPos
            pauseRedProgressAnimation()
            onVideoPause()
        } else {
            mAnimationHandler.post(mAnimationRunnable)
        }
    }

    fun onSplashHandler() {
        var position = 0
        object : CountDownTimer(mDuration.toLong(), 1000) {
            override fun onTick(millisUntilFinished: Long) {
                video_recyclerView?.smoothScrollToPosition(position++)
            }

            override fun onFinish() {

            }
        }.start()
    }

    /**
     * Cancel trim thread execut action when finish
     */
    override fun onDestroy() {
        BackgroundExecutor.cancelAll("", true)
        UiThreadExecutor.cancelAll("")
    }

    companion object {
        private val TAG = VideoTrimmerView::class.java.simpleName
    }

    init {
        init(context)
    }

    fun secToTime(totalSeconds: Long): String {
        return String.format("%02d:%02d",
            TimeUnit.MILLISECONDS.toMinutes(totalSeconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalSeconds)), // The change is in this line
            TimeUnit.MILLISECONDS.toSeconds(totalSeconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalSeconds)))
    }
}