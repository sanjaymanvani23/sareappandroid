package com.e.vediotrimmer.interfaces;

public interface VideoTrimListener {
    void onStartTrim();
    void onFinishTrim(String url,double durations,Long startTime,Long endTime);
    void onCancel();

}
