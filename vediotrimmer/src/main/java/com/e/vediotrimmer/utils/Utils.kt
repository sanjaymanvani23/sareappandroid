package com.e.vediotrimmer.utils

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.WindowManager

class Utils {
    companion object {
        @JvmStatic
        fun getScreenWidth(context: Context): Int {
            val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val dm = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(dm)
            return dm.widthPixels
        }
        @JvmStatic
        fun dpToPx(context: Context, value: Float) : Int {
            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value.toFloat(), context.resources.displayMetrics).toInt()
        }

        fun pxToDp(px: Int): Int {
            return (px / Resources.getSystem()
                .displayMetrics.density).toInt()
        }

        const val PUBLIC_URL = "https://api-sarevids-data.s3.ap-south-1.amazonaws.com/"
        const val FLAG_URL = "https://api.sarevids.com/graphql/images/flags/normal/"
        const val COVER_IMAGE_URL = "https://api-sarevids-data.s3.ap-south-1.amazonaws.com/"
      //  const val COVER_IMAGE_URL = "https://sare.in/rest/images/userCoverImage/"
        const val VIDEO_COVER_IMAGE_URL = "https://api-sarevids-data.s3.ap-south-1.amazonaws.com/"
        const val AUDIO_URL = "https://api-sarevids-data.s3.ap-south-1.amazonaws.com/"
//        const val AUDIO_URL = "https://sare.in/graphql/uploads/audios/"
        const val COLOR_CODE = "#707070"
        const val PAGE_SIZE = 15

    }
}