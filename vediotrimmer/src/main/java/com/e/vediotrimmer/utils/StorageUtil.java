package com.e.vediotrimmer.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;


import com.e.vediotrimmer.BuildConfig;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Locale;

import iknow.android.utils.BaseUtils;

@SuppressWarnings({ "ResultOfMethodCallIgnored", "FieldCanBeLocal" })
public class StorageUtil {

  public static String getAppDataDir() {
    String dstFile = Environment.getExternalStorageDirectory() + "/" + "SareApp" + new Date().getTime()
            + VideoTrimmerUtil.VIDEO_FORMAT;
    return  dstFile;
  }

}
