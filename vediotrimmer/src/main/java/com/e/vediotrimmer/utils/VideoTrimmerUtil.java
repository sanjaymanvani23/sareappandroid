package com.e.vediotrimmer.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.NonNull;

import com.coremedia.iso.boxes.Container;
import com.e.vediotrimmer.interfaces.VideoTrimListener;
import com.googlecode.mp4parser.FileDataSourceViaHeapImpl;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.googlecode.mp4parser.authoring.tracks.CroppedTrack;
import iknow.android.utils.DeviceUtil;
import iknow.android.utils.UnitConverter;
import iknow.android.utils.callback.SingleCallback;
import iknow.android.utils.thread.BackgroundExecutor;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.LinkedList;
import java.util.List;

public class VideoTrimmerUtil {

    private static final String TAG = VideoTrimmerUtil.class.getSimpleName();

    public static final long MIN_SHOOT_DURATION = 3000L;// Minimum editing time 3s

    private static final int VIDEO_MAX_TIME = 30;// 30 Second

    public static final long MAX_SHOOT_DURATION = VIDEO_MAX_TIME * 1000L;//How long does the video cut for up to 30s?

    public static final int MAX_COUNT_RANGE = 15;  //How many pictures are in the area of ​seekBar?

    private static final int SCREEN_WIDTH_FULL = DeviceUtil.getDeviceWidth();

    public static final int RECYCLER_VIEW_PADDING = UnitConverter.dpToPx(35);

    public static final int VIDEO_FRAMES_WIDTH = SCREEN_WIDTH_FULL - RECYCLER_VIEW_PADDING * 2;

    private static final int THUMB_WIDTH = (SCREEN_WIDTH_FULL - RECYCLER_VIEW_PADDING * 2) / VIDEO_MAX_TIME;

    private static final int THUMB_HEIGHT = UnitConverter.dpToPx(50);

    public static final String VIDEO_FORMAT = ".mp4";

    public static void startTrim(@NonNull File src, @NonNull String dst, long startMs, long endMs, @NonNull VideoTrimListener callback) {

        File file = new File(dst);

        file.getParentFile().mkdirs();

        try {

            generateVideo(src, file, startMs, endMs, callback);

        } catch (Exception e) {

            Log.d(TAG, "File Not Exits");
        }

    }

    private static void generateVideo(@NonNull File src, @NonNull File dst, long startMs, long endMs, @NonNull VideoTrimListener callback) {

        try {

            Movie movie = MovieCreator.build(new FileDataSourceViaHeapImpl(src.getAbsolutePath()));

            List<Track> tracks = movie.getTracks();

            movie.setTracks(new LinkedList<Track>());

            double startTime1 = startMs / 1000;

            double endTime1 = endMs / 1000;

            boolean timeCorrected = false;

            for (Track track : tracks) {

                if (track.getSyncSamples() != null && track.getSyncSamples().length > 0) {

                    if (timeCorrected) {

                        throw new RuntimeException("The startTime has already been corrected by another track with SyncSample. Not Supported.");
                    }

                    timeCorrected = true;
                }
            }

            for (Track track : tracks) {

                long currentSample = 0;

                double currentTime = 0;

                double lastTime = -1;

                long startSample1 = -1;

                long endSample1 = -1;

                for (int i = 0; i < track.getSampleDurations().length; i++) {

                    long delta = track.getSampleDurations()[i];

                    if (currentTime > lastTime && currentTime <= startTime1) {
                        // current sample is still before the new startTime
                        startSample1 = currentSample;
                    }
                    if (currentTime > lastTime && currentTime <= endTime1) {
                        // current sample is after the new start time and still before the new endTime
                        endSample1 = currentSample;
                    }

                    lastTime = currentTime;

                    currentTime += (double) delta / (double) track.getTrackMetaData().getTimescale();

                    currentSample++;
                }

                movie.addTrack(new AppendTrack(new CroppedTrack(track, startSample1, endSample1)));
            }

            Container out = new DefaultMp4Builder().build(movie);

            FileOutputStream fos = new FileOutputStream(dst);

            FileChannel fc = fos.getChannel();

            out.writeContainer(fc);

            fc.close();

            fos.close();

            callback.onFinishTrim(dst.toString(),(endMs-startMs)/1000,startMs,endMs);
        }
        catch (Exception e) {

            Log.d(TAG, "Generated eee" + e.toString());
        }
    }

    public static void shootVideoThumbInBackground(final Context context, final Uri videoUri, final int totalThumbsCount, final long startPosition,
                                                   final long endPosition, final SingleCallback<Bitmap, Integer> callback) {
        BackgroundExecutor.execute(new BackgroundExecutor.Task("", 0L, "") {
            @Override
            public void execute() {
                try {
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

                    mediaMetadataRetriever.setDataSource(context, videoUri);
                    // Retrieve media data use microsecond
                   // long interval = (endPosition - startPosition) / (totalThumbsCount - 1);

                    long interval = (totalThumbsCount - 1) == 0 ? 0 :
                            (endPosition - startPosition) / (totalThumbsCount - 1);

                  //  long duration = videoPlayer == null ? 0 : videoPlayer.getDuration();

                    for (long i = 0; i < totalThumbsCount; ++i) {

                        long frameTime = startPosition + interval * i;

                        Bitmap bitmap = mediaMetadataRetriever.getFrameAtTime(frameTime * 1000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC);

                        if (bitmap == null) continue;

                        try {

                            bitmap = Bitmap.createScaledBitmap(bitmap, THUMB_WIDTH, THUMB_HEIGHT, false);

                        } catch (final Throwable t) {

                            t.printStackTrace();
                        }

                        callback.onSingleCallback(bitmap, (int) interval);
                    }

                    mediaMetadataRetriever.release();
                }
                catch (final Throwable e) {

                    Thread.getDefaultUncaughtExceptionHandler().uncaughtException(Thread.currentThread(), e);
                }
            }
        });
    }
}
