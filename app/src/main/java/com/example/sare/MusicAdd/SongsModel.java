package com.example.sare.MusicAdd;

public class SongsModel {

    public String mSongsName;
    public String mArtistName;
    public int mDuration;
    public String mPath;
    public String mAlbum;

    public SongsModel(String songsName,
                      String artistName,
                      int duration,
                      String album,
                      String path) {
        mSongsName = songsName;
        mArtistName = artistName;
        mDuration = duration;
        mPath = path;
        mAlbum = album;
    }

}
