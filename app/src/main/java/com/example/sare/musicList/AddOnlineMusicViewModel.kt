package com.example.sare.musicList

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.AudioListQuery
import com.example.sare.VideoListQuery
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class AddOnlineMusicViewModel: ViewModel() {

//    var videoArrayList = MutableLiveData<List<VideoListQuery.Video>>()
    var audioArrayList = MutableLiveData<NetworkResponse>()
    val apolloClient = ApolloClient.setupApollo("")

    fun getAudioList(){
        apolloClient.query(AudioListQuery.builder().build())
            .enqueue(object : ApolloCall.Callback<AudioListQuery.Data>(){
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    audioArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<AudioListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.audios() != null){
                                audioArrayList.postValue(NetworkResponse.SUCCESS.audioList(
                                    response.data()?.audios()!!.audio()!!))

                            }else{
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                audioArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            audioArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                        }
                    }
                }
            })
    }
}