package com.example.sare.musicList

import android.database.Cursor
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.R
import com.example.sare.extensions.customToast
import com.example.sare.videoRecording.AudioRunningTask
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.fragment_add_music.*
import kotlinx.android.synthetic.main.fragment_add_music.view.*

class FragmentAddVideoMusic: Fragment(), AddMusicListeners {
    var audioCurrepted =false
    var root : View? = null
    var mPlayer: MediaPlayer? = null
    var musicListAdapter1 : MusicListAdapter?=null
    var r : Runnable? = null
    var handler : Handler? = null
    var currentPosition : Int = -1
    private lateinit var viewModel: AudioRunningTask
    var mDuration = 0
    var mLeftPosRange = 0F
    var mRightPosRange = 0F
    var mLeftDuration = 0
    var mRightDuration = 0
    var mMaxPos = 0F
    val ar = arrayOf("https://sare123.b-cdn.net/KGF-Flute-Bgm.mp3", "https://sare123.b-cdn.net/KGF-Monster-Bgm.mp3", "https://sare123.b-cdn.net/KGF-Monster-Love-Bgm.mp3", "https://sare123.b-cdn.net/kgf.mp3", "https://sare123.b-cdn.net/Salaam-Rocky-Bhai-Ringtone.mp3")
    private var mData: ArrayList<SongsModel>? = ArrayList()
    var cursor: Cursor? = null
    var uri: Uri? = null
    var useMusicListeners : UseMusicListeners?= null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root =  inflater.inflate(R.layout.fragment_add_music, container, false)
        loadData("")
        setMusicRecyclerView()

        handler = Looper.myLooper()?.let { Handler(it) }
        mPlayer = MediaPlayer()

        viewModel = ViewModelProvider(this)[AudioRunningTask::class.java]

        return root
    }

    fun setUseMusicListener(useMusicListeners: UseMusicListeners){
        this.useMusicListeners = useMusicListeners
    }

    override fun setAudioListener(dummy: SongsModel) {
        if(audioCurrepted)
            manageClickAdapter(dummy)
        else
            customToast( "Audio file corrupted!", requireActivity(),0)
    }

    override fun setAudioListener(dummy: AudioListQuery.Audio) {
        //TODO("Not yet implemented")
    }

    override fun setUseAudioListener(dummy: SongsModel) {
        if(audioCurrepted)
            useMusicListeners?.setAudioListener(dummy, mLeftDuration, mRightDuration)
        else
            customToast( "Audio file corrupted!", requireActivity(),0)


    }
    override fun setUseAudioListener(dummy: AudioListQuery.Audio) {
        //manageClickAdapter(dummy)
    }



    override fun stopMusic() {
        mPlayer?.pause()
    }

    override fun playMusic() {
        mPlayer?.start()
    }

    override fun onSeekBarChange(duration: Int, rightDuration: Int, leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        val delay = ((((mPlayer?.duration)?: 100) * 1000 / 100))/1000
        mPlayer?.seekTo(duration + delay)
        mPlayer?.start()
        mLeftPosRange = leftPosRange
        mLeftDuration = duration
        mRightPosRange = rightPosRange
        mRightDuration = rightDuration
    }

    override fun onSeekBarRightChange(duration: Int, rightPosRange: Float, maxPos: Float) {

        val delay = ((((mPlayer?.duration)?: 100) * 2000 / 100))/1000
        mPlayer?.seekTo(duration - delay)
        mPlayer?.start()
        mRightPosRange = rightPosRange
        mRightDuration = duration
    }

    override fun posRange(leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        mRightPosRange = rightPosRange
        mLeftPosRange = leftPosRange
        mMaxPos = maxPos
    }


    fun setMusicRecyclerView(){


        Log.d("tag","add video music:: $mData")
        musicListAdapter1 = MusicListAdapter(
            requireContext(),
            mData!!
        )

        musicListAdapter1!!.setOnClick(this)

        root?.music_list?.adapter = musicListAdapter1
        root?.music_list?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)

    }

    fun manageClickAdapter(dummy: SongsModel) {
        playProgress(dummy)
    }

    fun searchQuery(search: String){
      //  loadData(search)
        musicListAdapter1?.getFilter()?.filter(search.toString())
    }


    private fun loadData(searchString : String) {

        val contentResolver = requireContext().contentResolver
        var selectionArgs: Array<String>? = null
        var selection: String? = null
        if (searchString != null && searchString.length > 0) {
            selection = "title LIKE ?"
            selectionArgs = arrayOf("%$searchString%")
        }

        var uri1 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI

        var cursor1 = contentResolver.query(uri1!!, null, selection, selectionArgs, null)
          // Get List of Video Storage
        mData = ArrayList()

          //looping through all rows and adding to list
          if (cursor1 != null && cursor1.moveToFirst()) {
              do {
                  val title =
                      cursor1.getString(cursor1.getColumnIndex(MediaStore.Video.Media.TITLE))
                  val duration =
                      cursor1.getInt(cursor1.getColumnIndex(MediaStore.Video.Media.DURATION))
                  val data =
                      cursor1.getString(cursor1.getColumnIndex(MediaStore.Video.Media.DATA))


                  if (duration < 0){
                      Log.e("cursor1Duration" , duration.toString())
                  }else{
                      Log.e("cursor1Duration" , "")
                  }

                  Log.e("cursor1data" , duration.toString())

                  if (duration >= 5000){
                      mData!!.add(
                          SongsModel(
                              title,
                              "<Unknown>",
                              duration,
                              "",
                              data
                          )
                      )
                  }

              } while (cursor1.moveToNext())
          }

        musicListAdapter1?.updatedData(mData!!)

    }


    fun recyclerViewVisibility(){
        music_list.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }


    fun playProgress(dummy: SongsModel){
        currentPosition = mData?.indexOf(dummy)!!
        val fullPath = mData?.get(currentPosition)?.mPath

        Log.e("FullPath",fullPath.toString())

        mPlayer?.reset()
        val audioUrl = mData!![currentPosition].mPath

        mPlayer?.setAudioAttributes(
            AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )

        try {
            mPlayer?.setDataSource(audioUrl)
            mPlayer?.prepare()
            mPlayer?.setOnPreparedListener{
                mPlayer?.start()
                mRightPosRange = 100f
                mRightDuration = mPlayer?.duration ?: 100
            }
            mPlayer?.prepareAsync()
            mPlayer?.isLooping = true
            audioCurrepted=true
        } catch (e: Exception) {
            e.printStackTrace()
            if(r != null)
                handler?.removeCallbacks(r!!)
            Log.d("Music", "Music Player : Audio file corrupted! : " + e.message)
            if(e.message?.contains("Prepare failed")?:false) {
                audioCurrepted=false
                customToast( "Audio file corrupted!", requireActivity(),0)
            }
        }

        mPlayer?.start()
        mDuration = mPlayer!!.duration

        r = Runnable {
            val mCurrentPosition: Int = mPlayer!!.currentPosition

            if (mCurrentPosition >= ((mRightPosRange * (mDuration))/mMaxPos)){
                mPlayer?.pause()
                musicListAdapter1?.setProgressLine(dummy, mCurrentPosition, mDuration,(mCurrentPosition*100)/mDuration.toFloat(), false, fullPath!!,mCurrentPosition.toLong())
            }else{
                musicListAdapter1?.setProgressLine(dummy, mCurrentPosition, mDuration,(mCurrentPosition*100)/mDuration.toFloat(), true,fullPath!!,mCurrentPosition.toLong())
            }

            if(mCurrentPosition > mRightDuration + 200) {
                mPlayer?.seekTo(mLeftDuration)
                mPlayer?.start()
            }
           /* if (mCurrentPosition >= mPlayer?.duration!!){
                mPlayer?.seekTo(0)
                mPlayer?.start()
            }*/

            handler?.postDelayed(r!!, 10)
        }
        handler?.postDelayed(r!!, 10)
    }

    override fun onPause() {
        super.onPause()
        mPlayer?.pause()
    }
}