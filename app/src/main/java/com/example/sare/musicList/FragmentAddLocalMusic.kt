package com.example.sare.musicList

import android.content.Context
import android.database.Cursor
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.R
import com.example.sare.extensions.secToTime
import com.example.sare.videoRecording.AudioRunningTask
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.fragment_add_music.*
import kotlinx.android.synthetic.main.fragment_add_music.view.*
import kotlin.math.roundToLong


class FragmentAddLocalMusic: Fragment(), AddMusicListeners {

    private var filterTypes: List<FilterType>? = null
    private var songsModel: List<SongsModel>? = null
    var root : View? = null
    var mPlayer: MediaPlayer? = null
    var musicListAdapter1 : MusicListAdapter?=null
    var r : Runnable? = null
    var handler : Handler? = null
    var currentPosition : Int = -1
    private lateinit var viewModel: AudioRunningTask
    var mDuration = 0
    var mLeftPosRange = 0F
    var mRightPosRange = 0F

    var mLeftDuration = 0
    var mRightDuration = 0
    var mMaxPos = 0F
    private var mData: ArrayList<SongsModel>? = ArrayList()

    var cursor: Cursor? = null

    var uri: Uri? = null
    var useMusicListeners : UseMusicListeners?= null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root =  inflater.inflate(R.layout.fragment_add_music, container, false)
        loadData("")
        setMusicRecyclerView()

        handler = Looper.myLooper()?.let {Handler(it)}
        mPlayer = MediaPlayer()

        viewModel = ViewModelProvider(this)[AudioRunningTask::class.java]

        return root
    }

    fun setUseMusicListener(useMusicListeners: UseMusicListeners){
        this.useMusicListeners = useMusicListeners
    }


    override fun setAudioListener(dummy: SongsModel) {
        manageClickAdapter(dummy)
    }

    override fun setAudioListener(dummy: AudioListQuery.Audio) {
    }

    override fun setUseAudioListener(dummy: SongsModel) {
        //manageClickAdapter(dummy)

        val trimDuration = mRightDuration - mLeftDuration
        Log.d("tag", "Audio Online :${trimDuration.toFloat().roundToLong()} ::: $mLeftDuration ::: $mRightDuration ${secToTime(trimDuration.toLong())}")
        if((dummy.mDuration/1000)>15)
        {
            if (trimDuration.toFloat().roundToLong() >= 15000) {
                Toast.makeText(activity, "Please trim audio under 15 second. ", Toast.LENGTH_SHORT).show()
            }
            else
            {
                useMusicListeners?.setAudioListener(dummy, mLeftDuration, mRightDuration)
            }
        }
        else{
            useMusicListeners?.setAudioListener(dummy, mLeftDuration, mRightDuration)
        }

    }
    override fun setUseAudioListener(dummy: AudioListQuery.Audio) {

        //manageClickAdapter(dummy)

    }

    override fun stopMusic() {
        mPlayer?.pause()
    }

    override fun playMusic() {
        mPlayer?.start()
    }

    override fun onSeekBarChange(duration: Int, rightDuration: Int, leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        Log.d("Music", "Test Test Duration Left : " + (mPlayer?.duration))
        Log.d("Music", "Test Test Duration Left : " + ((((mPlayer?.duration)?: 100) * 2000 / 100))/1000)
        val delay = ((((mPlayer?.duration)?: 100) * 1000 / 100))/1000
        mPlayer?.seekTo(duration + delay)
        mPlayer?.start()
        mLeftPosRange = leftPosRange
        mLeftDuration = duration
        mRightPosRange = rightPosRange
        mRightDuration = rightDuration
    }

    override fun onSeekBarRightChange(duration: Int, rightPosRange: Float, maxPos: Float) {
        Log.d("Music", "Test Test Duration Right : " + (mPlayer?.duration))
        Log.d("Music", "Test Test Duration Right : " + ((((mPlayer?.duration)?: 100) * 2000 / 100))/1000)
        val delay = ((((mPlayer?.duration)?: 100) * 2000 / 100))/1000
        mPlayer?.seekTo(duration - delay)
        mPlayer?.start()
        mRightPosRange = rightPosRange
        mRightDuration = duration
    }

    override fun posRange(leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        mRightPosRange = rightPosRange
        mLeftPosRange = leftPosRange
        mMaxPos = maxPos
    }


    fun setMusicRecyclerView(){
        filterTypes = FilterType.createFilterList()

        musicListAdapter1 = MusicListAdapter(
            requireContext(),
            mData!!
        )


        musicListAdapter1!!.setOnClick(this)

/*
        root?.music_list?.adapter = musicListAdapter
        root?.music_list?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
*/

        root?.music_list?.adapter = musicListAdapter1
        root?.music_list?.layoutManager = LinearLayoutManager(
            requireContext(),
            RecyclerView.VERTICAL,
            false
        )

    }

    fun manageClickAdapter(dummy: SongsModel) {
        playProgress(dummy)
    }

    fun searchQuery(search: String){
        //loadData(search)
        musicListAdapter1?.getFilter()?.filter(search.toString())
    }



    /*private fun loadData(searchString : String) {
        val contentResolver = requireContext().contentResolver
        var selectionArgs: Array<String>? = null
        var selection: String? = null
        if (searchString != null && searchString.length > 0) {
            selection = "title LIKE ?"
            selectionArgs = arrayOf("%$searchString%")
        }

        // Get List of Music Storage
        mData = ArrayList()

        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        cursor = contentResolver.query(
            uri!!,  // Uri
            null,
            selection,
            selectionArgs,
            MediaStore.Audio.Media.DEFAULT_SORT_ORDER
        )

        if (cursor == null) {
            Toast.makeText(
                requireContext(),
                "Something Went Wrong.",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!cursor!!.moveToFirst()) {
            Toast.makeText(
                requireContext(),
                "No Music Found on SD Card.",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val Title = cursor!!.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val Artist = cursor!!.getColumnIndex(MediaStore.Audio.Media.ARTIST)
            val songLocation =
                cursor!!.getColumnIndex(MediaStore.Audio.Media.DATA)
            do {
                val SongTitle = cursor!!.getString(Title)
                val SongArtist = cursor!!.getString(Artist)
                val SongLocation = cursor!!.getString(songLocation)

                Log.e("SongTitle", SongTitle)

                if (cursor!!.getInt(3) >= 5000){
                    mData!!.add(
                        SongsModel(
                            SongTitle,
                            SongArtist,
                            cursor!!.getInt(3),
                            "",
                            SongLocation
                        )
                    )
                }

            } while (cursor!!.moveToNext())
        }
        musicListAdapter1?.updatedData(mData!!)

    }*/

    private fun loadData(searchString: String) {
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val projection = arrayOf(
            MediaStore.Audio.AudioColumns.DATA,
            MediaStore.Audio.AudioColumns.ALBUM,
            MediaStore.Audio.AudioColumns.TITLE,
            MediaStore.Audio.AudioColumns.DURATION,
            MediaStore.Audio.ArtistColumns.ARTIST
        )

        val c = requireContext().contentResolver.query(
            uri,
            projection,
            null,
            null,
            null
        )
        if (c != null) {
            while (c.moveToNext()) {
                //val audioModel = AudioModel()
                val path = c.getString(0)
                val album = c.getString(1)
                val title = c.getString(2)
                val duration = c.getInt(3)
                val artist = c.getString(4)
                val name = path.substring(path.lastIndexOf("/") + 1)
                Log.e("Name :$name", " Album :$album")
                Log.e("Path :$path", " Artist :$artist")
                //tempAudioList.add(audioModel)
                mData!!.add(
                    SongsModel(
                        title,
                        artist,
                        duration,
                        "",
                        path
                    )
                )
            }
            c.close()
        }
        musicListAdapter1?.updatedData(mData!!)

    }

    fun recyclerViewVisibility(){
        music_list.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    fun recyclerViewInvisibility(){
        music_list.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    fun playProgress(dummy: SongsModel){

        currentPosition = mData?.indexOf(dummy)!!
        val fullPath = mData?.get(currentPosition)?.mPath

        mPlayer?.reset()
        val audioUrl = mData!![currentPosition].mPath

        mPlayer?.setAudioAttributes(
            AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )

        try {
            mPlayer?.setDataSource(audioUrl)
            mPlayer?.prepare()
            mPlayer?.isLooping = true
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mPlayer?.start()
        mRightPosRange = 100f
        mRightDuration = mPlayer?.duration ?: 100
        mDuration = mPlayer!!.duration

        r = Runnable {
            val mCurrentPosition: Int = mPlayer!!.currentPosition

            if (mCurrentPosition >= ((mRightPosRange * (mDuration))/mMaxPos)){
                mPlayer?.pause()
                musicListAdapter1?.setProgressLine(
                    dummy,
                    mCurrentPosition,
                    mDuration,
                    (mCurrentPosition * 100) / mDuration.toFloat(),
                    false,
                    fullPath!!,
                    mCurrentPosition.toLong()
                )
            }else{
                musicListAdapter1?.setProgressLine(
                    dummy,
                    mCurrentPosition,
                    mDuration,
                    (mCurrentPosition * 100) / mDuration.toFloat(),
                    true,
                    fullPath!!,
                    mCurrentPosition.toLong()
                )
            }

            if(mCurrentPosition > mRightDuration + 200) {
                val delay = ((((mPlayer?.duration)?: 100) * 500 / 100))/1000
                mPlayer?.seekTo(mLeftDuration + delay)
                mPlayer?.start()
            }
            /*if (mCurrentPosition >= mPlayer?.duration!!){
                mPlayer?.seekTo(0)
                mPlayer?.start()
            }*/

            handler?.postDelayed(r!!, 10)
        }
        handler?.postDelayed(r!!, 10)
    }

    override fun onPause() {
        super.onPause()
        mPlayer?.pause()
    }

}