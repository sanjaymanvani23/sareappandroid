package com.example.sare.musicList

import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.SongsModel

interface AddMusicListeners {

    //Music Adapter Listeners
    fun setAudioListener(dummy: SongsModel)
    fun setAudioListener(dummy: AudioListQuery.Audio)
    fun setUseAudioListener(dummy: SongsModel)
    fun setUseAudioListener(dummy: AudioListQuery.Audio)
    fun stopMusic()
    fun playMusic()
    fun onSeekBarChange(duration: Int, rightDuration: Int, leftPosRange: Float, rightPosRange: Float,  maxPos: Float)
    fun onSeekBarRightChange(duration: Int, rightPosRange: Float, maxPos: Float)
    fun posRange(leftPosRange: Float, rightPosRange: Float, maxPos: Float)

}

interface UseMusicListeners {

    fun setAudioListener(dummy: SongsModel, leftDuration: Int, rightDuration: Int)
    fun setAudioListener(dummy: AudioListQuery.Audio, leftDuration: Int, rightDuration: Int)

}