package com.example.sare.musicList

import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.R
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.dashboard.VideoListViewModel
import com.example.sare.extensions.customToast
import com.example.sare.videoRecording.AudioRunningTask
import com.example.sare.videoRecording.FilterType
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_add_music.*
import kotlinx.android.synthetic.main.fragment_add_music.view.*
import kotlinx.android.synthetic.main.fragment_add_music.view.progressBar
import kotlinx.android.synthetic.main.fragment_video.view.*

class FragmentAddOnlineMusic: Fragment(), AddMusicListeners {

    var root : View? = null
    var mPlayer: MediaPlayer? = null
    var musicListAdapter1 : MusicListOnlineAdapter?=null
    var r : Runnable? = null
    var handler : Handler? = null
    var currentPosition : Int = -1
    private lateinit var viewModel: AudioRunningTask
    var mDuration = 0
    var mLeftPosRange = 0F
    var mRightPosRange = 0F
    var mLeftDuration = 0
    var mRightDuration = 0
    var mMaxPos = 0F
    //val ar = arrayOf("https://sare123.b-cdn.net/KGF-Flute-Bgm.mp3", "https://sare123.b-cdn.net/KGF-Monster-Bgm.mp3", "https://sare123.b-cdn.net/KGF-Monster-Love-Bgm.mp3", "https://sare123.b-cdn.net/kgf.mp3", "https://sare123.b-cdn.net/Salaam-Rocky-Bhai-Ringtone.mp3")
    private var mData: ArrayList<AudioListQuery.Audio>? = null
    var cursor: Cursor? = null
    var useMusicListeners : UseMusicListeners?= null

    var uri: Uri? = null
    var token: String? = null
    private val EXTERNAL_COLUMNS = arrayOf(
        MediaStore.Audio.Media._ID,
        MediaStore.Audio.Media.TITLE,
        MediaStore.Audio.Media.ARTIST,
        MediaStore.Audio.Media.DURATION,
        MediaStore.Audio.Media.ALBUM,
        MediaStore.Audio.Media.DATA,
        MediaStore.Audio.Media.IS_RINGTONE,
        MediaStore.Audio.Media.IS_ALARM,
        MediaStore.Audio.Media.IS_NOTIFICATION,
        MediaStore.Audio.Media.IS_MUSIC,
        MediaStore.Audio.Media.ALBUM_ID,
        "\"" + MediaStore.Audio.Media.EXTERNAL_CONTENT_URI + "\""
    )
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root =  inflater.inflate(R.layout.fragment_add_music, container, false)
        getSharedPreference()
        setMusicRecyclerView()
        //loadData("")


        handler = Looper.myLooper()?.let { Handler(it) }
        mPlayer = MediaPlayer()

        viewModel = ViewModelProvider(this)[AudioRunningTask::class.java]

        getAudioList(token.toString())
        return root
    }

    fun getSharedPreference(){
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        token = preferences.getString("TOKEN", null)
        Log.d("tag","token:::$token")
    }

    fun setUseMusicListener(useMusicListeners: UseMusicListeners){
        this.useMusicListeners = useMusicListeners
    }

    fun recyclerViewVisibility(){
        music_list.visibility = VISIBLE
        progressBar.visibility = GONE
    }

    fun recyclerViewInvisibility(){
        music_list.visibility = GONE
        progressBar.visibility = VISIBLE
    }

    fun searchQuery(search: String){
        musicListAdapter1?.getFilter()?.filter(search.toString())
    }

    override fun setAudioListener(dummy: SongsModel) {
        //manageClickAdapter(dummy)
    }
    override fun setAudioListener(dummy: AudioListQuery.Audio) {
        Log.d("tag", "Audio Online : " + dummy)
        manageClickAdapter(dummy)
    }

    override fun setUseAudioListener(dummy: SongsModel) {
        //manageClickAdapter(dummy)
    }
    override fun setUseAudioListener(dummy: AudioListQuery.Audio) {
        Log.d("tag", "Audio Online : " + dummy)
        useMusicListeners?.setAudioListener(dummy, mLeftDuration, mRightDuration)
    }

    override fun stopMusic() {
        mPlayer?.pause()
    }

    override fun playMusic() {
        mPlayer?.start()
    }

    override fun onSeekBarChange(duration: Int, rightDuration: Int, leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        val delay = ((((mPlayer?.duration)?: 100) * 1000 / 100))/1000
        mPlayer?.seekTo(duration + delay)
        mPlayer?.start()
        mLeftPosRange = leftPosRange
        mLeftDuration = duration
        mRightPosRange = rightPosRange
        mRightDuration = rightDuration
    }

    override fun onSeekBarRightChange(duration: Int, rightPosRange: Float, maxPos: Float) {

        if (duration < 0){
            mPlayer?.seekTo(0)
        } else {
            val delay = ((((mPlayer?.duration)?: 100) * 2000 / 100))/1000
            mPlayer?.seekTo(duration - delay)
        }

        mPlayer?.start()
        mRightPosRange = rightPosRange
        mRightDuration = duration
    }

    override fun posRange(leftPosRange: Float, rightPosRange: Float, maxPos: Float) {
        mRightPosRange = rightPosRange
        mLeftPosRange = leftPosRange
        mMaxPos = maxPos
    }


    fun setMusicRecyclerView(){

        mData = ArrayList()
        Log.d("tag","add music:::$mData")
        musicListAdapter1 = MusicListOnlineAdapter(
            requireContext(),
            mData!!
        )
        musicListAdapter1!!.setOnClick(this)
        root?.music_list?.adapter = musicListAdapter1
        root?.music_list?.layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
    }


    fun manageClickAdapter(dummy: AudioListQuery.Audio) {
        playProgress(dummy)
    }

   /* private fun loadData(searchString: String) {

        val contentResolver = requireContext().contentResolver
        var selectionArgs: Array<String>? = null
        var selection: String? = null
        if (searchString.isNotEmpty()) {
            selection = "title LIKE ?"
            selectionArgs = arrayOf("%$searchString%")
        }
        mData = ArrayList()
        // Get List of Music Storage

        uri = MediaStore.Audio.Media.INTERNAL_CONTENT_URI
        var COLUMNS: Array<String> = EXTERNAL_COLUMNS
        cursor = contentResolver.query(
            uri!!,  // Uri
            null,
            selection,
            selectionArgs,
            MediaStore.Audio.Media.DEFAULT_SORT_ORDER
        )

        if (cursor == null) {
            Toast.makeText(
                requireContext(),
                "Something Went Wrong.",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!cursor!!.moveToFirst()) {
            Toast.makeText(
                requireContext(),
                "No Music Found on SD Card.",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            val Title = cursor!!.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val Artist = cursor!!.getColumnIndex(MediaStore.Audio.Media.ARTIST)
            val songLocation =
                cursor!!.getColumnIndex(MediaStore.Audio.Media.DATA)
            do {
                val SongTitle = cursor!!.getString(Title)
                val SongArtist = cursor!!.getString(Artist)
                val SongLocation = cursor!!.getString(songLocation)


                if (cursor!!.getInt(3) >= 5000){
                    mData!!.add(
                        SongsModel(
                            SongTitle,
                            SongArtist,
                            cursor!!.getInt(3),
                            "",
                            SongLocation
                        )
                    )
                }

                for (i in mData!!){
                    Log.e("MdataDuration", i.mDuration.toString())
                }

            } while (cursor!!.moveToNext())
        }

        mPlayer?.reset()
        musicListAdapter1?.updatedData(mData!!)

    }*/


    fun playProgress(dummy: AudioListQuery.Audio){

        val fullPath =  com.example.sare.Utils.AUDIO_URL  + dummy.name()

        mPlayer?.reset()

        val audioUrl = fullPath

        mPlayer?.setAudioAttributes(
            AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .build()
        )

        try {
            mPlayer?.setDataSource(audioUrl)
            mPlayer?.prepare()
            mPlayer?.setOnPreparedListener{
                mPlayer?.start()
                mRightPosRange = 100f
                mRightDuration = mPlayer?.duration ?: 100
            }
            mPlayer?.prepareAsync()
            mPlayer?.setOnErrorListener(object : MediaPlayer.OnErrorListener {
                override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
                   customToast("Error : $what", requireActivity(),0)
                    return false
                }

            })

            mPlayer?.isLooping = true
        } catch (e: Exception) {
            e.printStackTrace()
            if(r != null)
                handler?.removeCallbacks(r!!)
            Log.d("Music", "Music Player : Audio file corrupted! : " + e.message)
            if(e.message?.contains("Prepare failed")?:false) {
                customToast( "Audio file corrupted!", requireActivity(),0)
            }

        }

        mPlayer?.start()
        mDuration = mPlayer!!.duration
        Log.e("mDuration",mDuration.toString())



        r = Runnable {
            val mCurrentPosition: Int = mPlayer!!.currentPosition

            if (mCurrentPosition >= ((mRightPosRange * (mDuration))/mMaxPos)){
                mPlayer?.pause()
                musicListAdapter1?.setProgressLine(dummy, mCurrentPosition, mDuration,(mCurrentPosition*100)/mDuration.toFloat(), false, fullPath!!,mCurrentPosition.toLong())
            }else{
                musicListAdapter1?.setProgressLine(dummy, mCurrentPosition, mDuration,(mCurrentPosition*100)/mDuration.toFloat(), true,fullPath!!,mCurrentPosition.toLong())
            }

            if(mCurrentPosition > mRightDuration + 200) {
                mPlayer?.seekTo(mLeftDuration)
                mPlayer?.start()
            }
            /*if (mCurrentPosition >= mPlayer?.duration!!){
                mPlayer?.seekTo(0)
                mPlayer?.start()
            }*/

            handler?.postDelayed(r!!, 10)
        }
        handler?.postDelayed(r!!, 10)
    }

    private fun getAudioList(token : String) {
        viewModel?.getAudioList(token.toString())

        viewModel?.audioArrayList?.observe(viewLifecycleOwner , Observer {

            root?.progressBar?.visibility = GONE
            when(it){
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_LONG)
                        .setAction("RETRY") {
                            viewModel?.getAudioList(token.toString())
                        }
                    snackbar.show()
                }

                is NetworkResponse.SUCCESS.audioList -> {
                    //root?.bottom_info?.visibility = VISIBLE
                    Log.d("tag","audioList:::${it.response}")
                    musicListAdapter1?.updatedData(it.response)
                    //root?.download?.isEnabled = true
                }

            }


        })
    }

    override fun onPause() {
        super.onPause()
        mPlayer?.pause()
    }


}