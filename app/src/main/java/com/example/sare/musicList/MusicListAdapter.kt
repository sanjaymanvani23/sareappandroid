package com.example.sare.musicList

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.MusicAdd.Utils
import com.example.sare.R
import com.example.sare.videoRecording.RangeSlider
import com.example.sare.widget.waveForm.WaveformSeekBar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_music_list.view.*
import java.util.*

class MusicListAdapter(private val context: Context, var parent: List<SongsModel>) :
    RecyclerView.Adapter<MusicListAdapter.ViewHolder>() , Filterable {

    var mAddMusicListeners: AddMusicListeners? = null
    var currentposition: Int = -1
    private var maxPos: Float = 0f
    private var leftPosRange: Float = 0f
    private var rightPosRange: Float = 0f
    private var leftMargin: Int = 0
    private var mDuration: Int = 0
    var handler: Handler? = null
    private var play = true
    private var audioWaveSet = false
    private var rangeBar: RangeSlider? = null
    var params = RelativeLayout.LayoutParams(
        10, 110)

    var filter: CustomFilter? = null
    lateinit var parentList: List<SongsModel>
    lateinit var parentAudioList: List<SongsModel>

    init {
        handler = Looper.myLooper()?.let { Handler(it) }
        parentList=parent
        parentAudioList=parent
    }

    init {
        handler = Looper.myLooper()?.let { Handler(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_music_list, parent, false))
    }

    override fun getItemCount(): Int {
        return parentAudioList.size
    }

    fun setOnClick(addMusicListeners: AddMusicListeners) {
        mAddMusicListeners = addMusicListeners
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val imageUri = "https://i.imgur.com/tGbaZCY.jpg"
        Picasso.with(context).load(imageUri).fit().into(holder.cover_image)

        holder.musicController.visibility = View.GONE

        holder.duration.text = Utils.makeShortTimeString(
            context, (parentAudioList[position].mDuration / 1000).toLong())

        rangeBar?.duration = parentAudioList[position].mDuration.toDouble()


        holder.waveFormLayout.visibility = View.GONE

        holder.use.visibility = View.GONE

        holder.music_name.text = parentAudioList[position].mSongsName
        holder.music_description.text = parentAudioList[position].mArtistName

        holder.musicController.visibility = View.GONE

        if (position == currentposition) {
            holder.musicController.visibility = VISIBLE
            holder.waveFormLayout.visibility = VISIBLE
            //holder.tvAudioDuration.visibility = VISIBLE
            holder.use.visibility = VISIBLE
        }


    }

    fun updatedData(data: ArrayList<SongsModel>) {
        currentposition = -1
        this.parentAudioList = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        if (payloads.firstOrNull() != null) {
            with(holder.itemView) {

                (payloads.first() as Bundle).getFloat("progress").also {
                    if (it <= 100) waveformSeekBar.progress = it.toInt()
                }

                (payloads.first() as Bundle).getBoolean("imageController").also {

                    if (play) {
                        if (it) {
                            musicController.setImageDrawable(
                                ResourcesCompat.getDrawable(
                                    context.resources, R.drawable.ic_pause_2, context.theme))
                            musicController.visibility = VISIBLE
                            playButton.visibility = View.INVISIBLE
                        } else {
                            musicController.setImageDrawable(
                                ResourcesCompat.getDrawable(
                                    context.resources, R.drawable.ic_play_2, context.theme))
                            musicController.visibility = View.INVISIBLE
                            playButton.visibility = VISIBLE
                        }
                    } else {
                        musicController.setImageDrawable(
                            ResourcesCompat.getDrawable(
                                context.resources, R.drawable.ic_play_2, context.theme))
                        musicController.visibility = View.INVISIBLE
                        playButton.visibility = VISIBLE
                    }

                }


                (payloads.first() as Bundle).getInt("leftMargin").also {
                    if (it < maxPos) {
                        params.setMargins(it, 0, 0, 0)
                        progress.layoutParams = params
                    }
                }

                (payloads.first() as Bundle).getLong("mSec").also {
                    duration.text = Utils.makeShortTimeString(
                        context, it / 1000)

                }

                (payloads.first() as Bundle).getBoolean("seekController").also {
                    if (it) {
                        (payloads.first() as Bundle).getInt("seekLeftPinIndex").also { seekLeftPinIndex ->

                            (payloads.first() as Bundle).getInt("seekRightPinIndex").also { seekRightPinIndex ->

                                val time = (rangeSlider.width / 100) - (((rangeSlider.width / 100) - seekRightPinIndex) + seekLeftPinIndex)
                                Log.d("MusicListAudio", "Data 11 1111 : ${rangeSlider.width / 100}")
                                //Log.d("MusicListAudio", "Data 11 2222 : ${rangeSlider.width - seekRightPinIndex}")
                                Log.d("MusicListAudio", "Data 11 Right : $seekRightPinIndex")
                                Log.d("MusicListAudio", "Data 11 Left : $seekLeftPinIndex")
                                Log.d("MusicListAudio", "Data 11 4444 : $time")
                                Log.d(
                                    "MusicListAudio", "Data 11 2222 : ${
                                        Utils.makeShortTimeString(
                                            context, (time / 1000).toLong())
                                    }")
                                /*  tv_audio_duration.text = Utils.makeShortTimeString(
                                      context,
                                      time.toLong() / 1000
                                  )*/
                                val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                                    time * rangeSlider.width / 100, RelativeLayout.LayoutParams.WRAP_CONTENT)
                                params.setMargins(
                                    seekLeftPinIndex * rangeSlider.width / 100,
                                    4,
                                    seekRightPinIndex * rangeSlider.width / 100,
                                    0)
                                params.addRule(RelativeLayout.BELOW, R.id.layout)
                                //tv_audio_duration.layoutParams = params
                            }
                        }
                    }
                }

            }
        }
    }

    fun setProgressLine(dummy: SongsModel,
        currentPosition: Int,
        duration: Int,
        progress: Float,
        image: Boolean,
        path: String,
        sec: Long) {

        leftMargin = ((currentPosition * maxPos) / duration).toInt()
        mDuration = duration

        notifyItemChanged(parentAudioList.indexOf(dummy), Bundle().apply {
            putInt("leftMargin", leftMargin)
            putFloat("progress", progress)
            putBoolean("imageController", image)
            putLong("mSec", sec)
            putBoolean("seekController", false)
        })

    }

    private fun getDummyWaveSample(): IntArray {
        val data = IntArray(50)
        for (i in data.indices) data[i] = Random().nextInt(data.size)

        return data
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cover_image = view.cover_image
        var layout = view.layout
        var waveFormLayout = view.waveFormLayout

        //var rangeBar = view.rangeSlider
        var progress = view.progress
        var musicController = view.musicController
        var waveformSeekBar = view.waveformSeekBar
        var music_name = view.music_name
        var duration = view.duration
        var music_description = view.music_description
        var use = view.use
        //var tvAudioDuration = view.tv_audio_duration

        init {

            //rangeBar.initMaxWidth()
            rangeBar = view.rangeSlider
            rangeBar?.duration = mDuration.toDouble()

            layout.setOnClickListener {
                if (!musicController.isVisible) {
                    play = true
                    audioWaveSet = true
                    currentposition = adapterPosition
                    notifyDataSetChanged()
                    mAddMusicListeners?.setAudioListener(parentAudioList[currentposition])
                } else {
                    play = if (play) {
                        mAddMusicListeners?.stopMusic()
                        false
                    } else {
                        mAddMusicListeners?.playMusic()
                        true
                    }
                }
            }

            use.setOnClickListener {
                Log.d("tag", "Audio Online 000: " + parentAudioList[adapterPosition])
                mAddMusicListeners?.setAudioListener(parentAudioList[adapterPosition])
            }

            waveformSeekBar.apply {
                progress = 0
                waveWidth = dp(context, 1f)
                waveGap = dp(context, 0.5f)
                waveMinHeight = dp(context, 5f)
                waveCornerRadius = dp(context, 2f)
                waveGravity = WaveformSeekBar.WaveGravity.CENTER
                waveBackgroundColor = ContextCompat.getColor(context, R.color.white)
                waveProgressColor = ContextCompat.getColor(context, R.color.progressOrange)
                sample = getDummyWaveSample()
            }

            rangeBar?.setRangeChangeListener(object : RangeSlider.OnRangeChangeListener {
                override fun onRangeStart(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int, isRight: Boolean) {
                    play = false
                    mAddMusicListeners?.stopMusic()
                }

                override fun onRangeEnd(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int, isRight: Boolean) {
                    //Log.e("IndexPosition", index.toString())
                    play = true
                    leftPosRange = leftPinIndex.toFloat()
                    rightPosRange = rightPinIndex.toFloat()

                    if (isRight) {
                        Log.e("atg Right mDuration", "$mDuration")
                        Log.e("atg Right rightPosRange", "$rightPosRange")
                        Log.e("atg Right maxPos", "$maxPos")
                        Log.e("atg Right mDuration", "" + (((mDuration / 100) * rightPosRange)).toInt())
                        mAddMusicListeners?.onSeekBarRightChange(
                            (((mDuration / 100) * rightPosRange)).toInt(), rightPosRange, maxPos)
                    } else {
                        Log.e("atg Left mDuration", "$mDuration")
                        Log.e("atg Left leftPosRange", "$leftPosRange")
                        Log.e("atg Left rightPosRange", "$rightPosRange")
                        Log.e("atg Left maxPos", "$maxPos")
                        Log.e("atg Left mDuration", "" + (((mDuration / 100) * leftPosRange)).toInt())
                        mAddMusicListeners?.onSeekBarChange(
                            (((mDuration / 100) * leftPosRange)).toInt(),
                            (((mDuration / 100) * rightPosRange)).toInt(),
                            leftPosRange,
                            rightPosRange,
                            maxPos)
                    }

                    use.setOnClickListener {
                        Log.d("tag", "Audio Online 000: Range" + parentAudioList[adapterPosition])
                        mAddMusicListeners?.setUseAudioListener(parentAudioList[currentposition])
                    }

                }

                override fun onRangeChange(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int) {
                }

            })



        }
    }

    override fun getFilter(): Filter? {

        if (filter == null) {
            Log.e("Suze 111", "" + parentList.size)
            filter = CustomFilter(parentList, this)
        }
        return filter
    }

    class CustomFilter(filterList: List<SongsModel>, adapter: MusicListAdapter) : Filter() {
        var adapter: MusicListAdapter
        var filterList: List<SongsModel>
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            var constraint = constraint
            val results = FilterResults()
            if (constraint != null && constraint.isNotEmpty()) {
                constraint = constraint.toString().toUpperCase()
                val filteredPlayers: MutableList<SongsModel> = ArrayList()
                Log.e("Suze 3333", "" + constraint)
                for (i in filterList.indices) {
                    if (filterList[i].mSongsName.toString().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList[i])
                    } else {
                        Log.e("Hrllo", "hhhhh")
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = filterList.size
                results.values = filterList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            adapter.parentAudioList = results.values as List<SongsModel>
            adapter.notifyDataSetChanged()
        }

        init {
            this.adapter = adapter
            this.filterList = filterList
            Log.e("Suze 2222", "" + filterList.size)
        }
    }

}