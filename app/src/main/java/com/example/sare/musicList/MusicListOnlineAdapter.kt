package com.example.sare.musicList

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.AudioListQuery
import com.example.sare.MusicAdd.Utils
import com.example.sare.R
import com.example.sare.videoRecording.RangeSlider
import com.example.sare.widget.waveForm.WaveformSeekBar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_music_list.view.*
import java.util.*


class MusicListOnlineAdapter(private val context: Context, private var audioList: List<AudioListQuery.Audio>) :
    RecyclerView.Adapter<MusicListOnlineAdapter.ViewHolder>() , Filterable {

    var mAddMusicListeners: AddMusicListeners? = null
    var currentposition: Int = -1
    private var maxPos: Float = 0f
    private var leftPosRange: Float = 0f
    private var rightPosRange: Float = 0f
    private var leftMargin: Int = 0
    private var mDuration: Int = 0
    var handler: Handler? = null
    private var play = true
    private var audioWaveSet = false
    private var rangeBar: RangeSlider? = null
    lateinit var parentList: List<AudioListQuery.Audio>
    lateinit var parentAudioList: List<AudioListQuery.Audio>
    var filter: CustomFilter? = null
    var params = RelativeLayout.LayoutParams(10, 110)

    init {
        handler = Looper.myLooper()?.let { Handler(it) }
        parentList=audioList
        parentAudioList=audioList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_music_list, parent, false))
    }

    override fun getItemCount(): Int {
        return parentAudioList.size
    }

    fun setOnClick(addMusicListeners: AddMusicListeners) {
        mAddMusicListeners = addMusicListeners
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val imageUri = "https://i.imgur.com/tGbaZCY.jpg"
        Picasso.with(context).load(imageUri).fit().into(holder.cover_image)

        holder.musicController.visibility = View.GONE

        val fullPath = com.example.sare.Utils.AUDIO_URL + parentAudioList[position].name()
        Log.d("Music", "Music Data Data Duration : " + getDuration(fullPath))

        holder.waveFormLayout.visibility = View.GONE
        //holder.tvAudioDuration.visibility = View.GONE
        holder.use.visibility = View.GONE

        holder.music_name.text = parentAudioList[position].title()
        holder.music_description.text = parentAudioList[position].user()?.name() ?: ""

        holder.musicController.visibility = View.GONE

        if (position == currentposition) {
            holder.musicController.visibility = VISIBLE
            holder.waveFormLayout.visibility = VISIBLE
            //holder.tvAudioDuration.visibility = VISIBLE
            holder.use.visibility = VISIBLE
        }


    }

    fun updatedData(data: List<AudioListQuery.Audio>) {
        currentposition = -1
        this.parentAudioList = data
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
        if (payloads.firstOrNull() != null) {
            with(holder.itemView) {

                (payloads.first() as Bundle).getFloat("progress").also {
                    if (it <= 100) waveformSeekBar.progress = it.toInt()
                }

                (payloads.first() as Bundle).getBoolean("imageController").also {

                    if (play) {
                        if (it) {
                            musicController.setImageDrawable(
                                ResourcesCompat.getDrawable(
                                    context.resources, R.drawable.ic_pause_2, context.theme))
                            musicController.visibility = VISIBLE
                            playButton.visibility = INVISIBLE
                        } else {
                            musicController.setImageDrawable(
                                ResourcesCompat.getDrawable(
                                    context.resources, R.drawable.ic_play_2, context.theme))
                            musicController.visibility = INVISIBLE
                            playButton.visibility = VISIBLE
                        }
                    } else {
                        musicController.setImageDrawable(
                            ResourcesCompat.getDrawable(
                                context.resources, R.drawable.ic_play_2, context.theme))
                        musicController.visibility = INVISIBLE
                        playButton.visibility = VISIBLE
                    }

                }

                (payloads.first() as Bundle).getInt("leftMargin").also {
                    if (it < maxPos) {
                        params.setMargins(it, 0, 0, 0)
                        progress.layoutParams = params
                    }
                }

                (payloads.first() as Bundle).getLong("mSec").also {
                    duration.text = Utils.makeShortTimeString(
                        context, it / 1000)

                }

                (payloads.first() as Bundle).getBoolean("seekController").also {
                    if (it) {
                        (payloads.first() as Bundle).getInt("seekLeftPinIndex").also { seekLeftPinIndex ->

                            (payloads.first() as Bundle).getInt("seekRightPinIndex").also { seekRightPinIndex ->

                                val time = (rangeSlider.width / 100) - (((rangeSlider.width / 100) - seekRightPinIndex) + seekLeftPinIndex)
                                //                                tv_audio_duration.text = Utils.makeShortTimeString(
                                //                                    context,
                                //                                    time.toLong() / 1000
                                //                                )
                                val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                                    time * rangeSlider.width / 100, RelativeLayout.LayoutParams.WRAP_CONTENT)
                                params.setMargins(
                                    seekLeftPinIndex * rangeSlider.width / 100,
                                    4,
                                    seekRightPinIndex * rangeSlider.width / 100,
                                    0)
                                params.addRule(RelativeLayout.BELOW, R.id.layout)
                                //                                tv_audio_duration.layoutParams = params
                            }
                        }
                    }
                }

            }
        }
    }

    fun setProgressLine(dummy: AudioListQuery.Audio,
        currentPosition: Int,
        duration: Int,
        progress: Float,
        image: Boolean,
        path: String,
        sec: Long) {

        leftMargin = ((currentPosition * maxPos) / duration).toInt()
        mDuration = duration

        notifyItemChanged(parentAudioList.indexOf(dummy), Bundle().apply {
            putInt("leftMargin", leftMargin)
            putFloat("progress", progress)
            putBoolean("imageController", image)
            putLong("mSec", sec)
            putBoolean("seekController", false)
        })

    }

    private fun getDummyWaveSample(): IntArray {
        val data = IntArray(50)
        for (i in data.indices) data[i] = Random().nextInt(data.size)

        return data
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cover_image = view.cover_image
        var layout = view.layout
        var waveFormLayout = view.waveFormLayout

        var progress = view.progress
        var musicController = view.musicController
        var waveformSeekBar = view.waveformSeekBar
        var music_name = view.music_name
        var duration = view.duration
        var music_description = view.music_description
        var use = view.use
        //var tvAudioDuration = view.tv_audio_duration

        init {
            rangeBar = view.rangeSlider
            rangeBar?.duration = mDuration.toDouble()
            //rangeBar.initMaxWidth()

            layout.setOnClickListener {
                Log.d("Test", "DATA DATA DATA")
                if (!musicController.isVisible) {
                    play = true
                    audioWaveSet = true
                    currentposition = adapterPosition
                    notifyDataSetChanged()
                    mAddMusicListeners?.setAudioListener(parentAudioList[currentposition])
                } else {
                    play = if (play) {
                        mAddMusicListeners?.stopMusic()
                        false
                    } else {
                        mAddMusicListeners?.playMusic()
                        true
                    }
                }
            }

            use.setOnClickListener {
                Log.d("tag", "Online Audio Online 000: " + parentAudioList[adapterPosition])
                mAddMusicListeners?.setUseAudioListener(parentAudioList[currentposition])
            }

            waveformSeekBar.apply {
                progress = 0
                waveWidth = dp(context, 1f)
                waveGap = dp(context, 0.5f)
                waveMinHeight = dp(context, 5f)
                waveCornerRadius = dp(context, 2f)
                waveGravity = WaveformSeekBar.WaveGravity.CENTER
                waveBackgroundColor = ContextCompat.getColor(context, R.color.white)
                waveProgressColor = ContextCompat.getColor(context, R.color.progressOrange)
                sample = getDummyWaveSample()
            }

            /* rangeBar.setRangeChangeListener { view, leftPinIndex, rightPinIndex ->

                 //val rightDuration = rangeBar.width - rightPinIndex
                 //val leftDuration = leftPinIndex

                 ///tvAudioDuration.text = Utils.makeShortTimeString(context, (time / 1000).toLong())
                 //Handler().post(Runnable {
                 //    tvAudioDuration.text = "0.00"
                 Log.d("MusicListAudio", "Data 11 Right iii : $rightPinIndex")
                 Log.d("MusicListAudio", "Data 11 Left iii : $leftPinIndex")
                     notifyItemChanged(position, Bundle().apply {
                         putInt("seekLeftPinIndex", leftPinIndex)
                         putInt("seekRightPinIndex", rightPinIndex)
                         putBoolean("seekController", true)
                     })
                // })

             }*/

            rangeBar?.setRangeChangeListener(object : RangeSlider.OnRangeChangeListener {
                override fun onRangeStart(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int, isRight: Boolean) {
                    play = false
                    mAddMusicListeners?.stopMusic()
                }

                override fun onRangeEnd(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int, isRight: Boolean) {
                    //Log.e("IndexPosition", index.toString())
                    play = true
                    leftPosRange = leftPinIndex.toFloat()
                    rightPosRange = rightPinIndex.toFloat()
                    if (isRight) {
                        mAddMusicListeners?.onSeekBarRightChange(
                            (((mDuration / 100) * rightPosRange)).toInt(), rightPosRange, maxPos)
                    } else {
                        mAddMusicListeners?.onSeekBarChange(
                            (((mDuration / 100) * leftPosRange)).toInt(),
                            (((mDuration / 100) * rightPosRange)).toInt(),
                            leftPosRange,
                            rightPosRange,
                            maxPos)
                    }

                    //if (index == 1){

                    //  if (mDuration <= 60000){
                    //     mAddMusicListeners?.onSeekBarRightChange((((rightPosRange * mDuration)/maxPos) - 2000).toInt(), rightPosRange, maxPos)
                    // }else if (mDuration > 60000){
                    //      mAddMusicListeners?.onSeekBarRightChange((((rightPosRange * mDuration)/maxPos) - 3000).toInt(), rightPosRange, maxPos)
                    //  }

                    //}else if(index == 0){
                    // mAddMusicListeners?.onSeekBarChange((((leftPosRange * mDuration)/maxPos) + 200).toInt(), leftPosRange, maxPos)
                    //}
                }

                override fun onRangeChange(view: RangeSlider?, leftPinIndex: Int, rightPinIndex: Int) {
                }

            })

            /* rangeBar.addOnRangeSeekBarListener(object : OnRangeSeekBarListener {
                 override fun onCreate(
                     rangeSeekBarView: RangeSeekBarView,
                     index: Int,
                     value: Float
                 ) {
                     maxPos = rangeBar.thumbs[RangeSeekBarView.ThumbType.RIGHT.index].pos
                     leftPosRange = rangeBar.thumbs[RangeSeekBarView.ThumbType.LEFT.index].pos
                     rightPosRange = rangeBar.thumbs[RangeSeekBarView.ThumbType.RIGHT.index].pos

                     mAddMusicListeners?.posRange(leftPosRange, rightPosRange, maxPos)
                 }

                 override fun onSeek(rangeSeekBarView: RangeSeekBarView, index: Int, value: Float) {
                 }

                 override fun onSeekStart(
                     rangeSeekBarView: RangeSeekBarView,
                     index: Int,
                     value: Float
                 ) {
                     play = false
                     mAddMusicListeners?.stopMusic()
                 }

                 override fun onSeekStop(
                     rangeSeekBarView: RangeSeekBarView,
                     index: Int,
                     value: Float
                 ) {

                     Log.e("IndexPosition", index.toString())
                     play = true
                     leftPosRange = rangeBar.thumbs[RangeSeekBarView.ThumbType.LEFT.index].pos
                     rightPosRange = rangeBar.thumbs[RangeSeekBarView.ThumbType.RIGHT.index].pos
                     if (index == 1){

                         if (mDuration <= 60000){
                             mAddMusicListeners?.onSeekBarRightChange((((rightPosRange * mDuration)/maxPos) - 2000).toInt(), rightPosRange, maxPos)
                         }else if (mDuration > 60000){
                             mAddMusicListeners?.onSeekBarRightChange((((rightPosRange * mDuration)/maxPos) - 3000).toInt(), rightPosRange, maxPos)
                         }

                     }else if(index == 0){
                         mAddMusicListeners?.onSeekBarChange((((leftPosRange * mDuration)/maxPos) + 200).toInt(), leftPosRange, maxPos)
                     }
                 }

             })*/

            /*  musicController.setOnClickListener {

                  play = if (play){
                      mAddMusicListeners?.stopMusic()
                      false
                  }else{
                      mAddMusicListeners?.playMusic()
                      true
                  }
              }*/

        }
    }

    private fun getDuration(pathStr: String): Int {
        return 0
    }

    override fun getFilter(): Filter? {

        if (filter == null) {
            Log.e("Suze 111", "" + parentList.size)
            filter = CustomFilter(parentList, this)
        }
        return filter
    }

    class CustomFilter(filterList: List<AudioListQuery.Audio>, adapter: MusicListOnlineAdapter) : Filter() {
        var adapter: MusicListOnlineAdapter
        var filterList: List<AudioListQuery.Audio>
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            var constraint = constraint
            val results = FilterResults()
            if (constraint != null && constraint.length > 0) {
                constraint = constraint.toString().toUpperCase()
                val filteredPlayers: MutableList<AudioListQuery.Audio> = ArrayList()
                Log.e("Suze 3333", "" + constraint)
                for (i in filterList.indices) {
                    if (filterList[i].name().toString().toUpperCase().contains(constraint)) {
                        filteredPlayers.add(filterList[i])
                    } else {
                        Log.e("Hrllo", "hhhhh")
                    }
                }
                results.count = filteredPlayers.size
                results.values = filteredPlayers
            } else {
                results.count = filterList.size
                results.values = filterList
            }
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            adapter.parentAudioList = results.values as List<AudioListQuery.Audio>

            adapter.notifyDataSetChanged()
        }

        init {
            this.adapter = adapter
            this.filterList = filterList
            Log.e("Suze 2222", "" + filterList.size)
        }
    }
}