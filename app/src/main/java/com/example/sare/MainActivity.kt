package com.example.sare

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.customToast
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openNewActivity

import com.example.sare.extensions.showToast
import com.example.sare.settings.viewmodel.SettingsViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_video_upload.*
import org.json.JSONObject


@RequiresApi(api = Build.VERSION_CODES.M)
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adjustFontScale(resources.configuration)
        setContentView(R.layout.activity_main)
        MyApplication.mainActivityInsnace(this)

    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    fun openProfileUser() {
        val navController = findNavController(R.id.nav_host_fragment)
        navController.navigate(R.id.mainFragment, bundleOf("type" to 5))
    }
    open fun openVideoScreen() {
        val navController = findNavController(R.id.nav_host_fragment)
        navController.navigate(R.id.mainFragment, bundleOf("type" to 2))
    }
    fun adjustFontScale(configuration: Configuration) {
        if (configuration.fontScale > 1) {
            configuration.fontScale = 1f
            val metrics = resources.displayMetrics
            val wm = getSystemService(WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(metrics)
            metrics.scaledDensity = configuration.fontScale * metrics.density
            baseContext.resources.updateConfiguration(configuration, metrics)
        }
    }

    override fun attachBaseContext(baseContext: Context) {
        val newContext: Context
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            val displayMetrics: DisplayMetrics = baseContext.resources.displayMetrics
            val configuration: Configuration = baseContext.resources.configuration
            if (displayMetrics.densityDpi !== DisplayMetrics.DENSITY_DEVICE_STABLE) {
                // Current density is different from Default Density. Override it
                configuration.densityDpi = DisplayMetrics.DENSITY_DEVICE_STABLE
                newContext = baseContext.createConfigurationContext(configuration)
            } else {
                // Same density. Just use same context
                newContext = baseContext
            }
        } else {
            // Old API. Screen zoom not supported
            newContext = baseContext
        }
        super.attachBaseContext(newContext)

    }

     fun logoutUser() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        var userId = ""
        if (user != null) {
            val mainObject = JSONObject(user)
            userId = mainObject.getString("_id")
        }
       val settingsViewModel = ViewModelProvider(this)[SettingsViewModel::class.java]

        settingsViewModel?.logout(userId)

        settingsViewModel?.logoutUser?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.mainActivity), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        settingsViewModel?.logout(userId)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToastMain(it.errorMsg, 0)
                    MyApplication.logoutClearPreference(this)
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.logoutUser -> {
                    MyApplication.logoutClearPreference(this)
                    openNewActivity(LoginActivity::class.java)
                }
            }
        })

    }


}