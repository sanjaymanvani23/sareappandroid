package com.example.sare.customeview.bottomnavigation

import android.animation.Animator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.TransitionDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.sare.R
import com.example.sare.customeview.PorterShapeImageView
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder


class SareBottomNavigation : LinearLayout {
    private lateinit var toolbarLayout: ConstraintLayout
    private lateinit var layoutCreateVideo: FrameLayout
    private lateinit var itemChat: RadioButton
    private lateinit var itemHome: RadioButton
    private lateinit var itemMusic: RadioButton
    private lateinit var itemStatus: RadioButton
    private lateinit var ivProfileSelf: PorterShapeImageView
    private lateinit var ivProfileSelfBorder: ImageView
    var colorCode: String = ""
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs, android.R.attr.textViewStyle)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        LayoutInflater.from(context).inflate(R.layout.sare_bottomavigation_view, this)
        itemChat = findViewById(R.id.itemChat)
        itemHome = findViewById(R.id.itemHome)
        layoutCreateVideo = findViewById(R.id.layoutCreateVideo)
        itemMusic = findViewById(R.id.itemMusic)
        itemStatus = findViewById(R.id.itemStatus)
        ivProfileSelf = findViewById(R.id.ivProfileSelf)
        ivProfileSelfBorder = findViewById(R.id.ivProfileSelfBorder)
        toolbarLayout = findViewById(R.id.toolbarLayout)
     /*   toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.pureWhite))
        if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect_new)

        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)

        }*/
      /*  frameLayoutItemChat = findViewById(R.id.frameLayoutItemChat)
        frameLayoutItemHome = findViewById(R.id.frameLayoutItemHome)*/
    }




    fun setItemChatListener() {
        itemChat.isChecked=true
        itemHome.isChecked=true
        itemMusic.isChecked=true
        itemStatus.isChecked=false
        ivProfileSelfBorder.setBorder("#000000")
        toolbarLayout.setBackgroundColor(Color.parseColor("#FFFFFF"))
        toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.pureWhite))
       /* if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect_new)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect)
        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect)

        }*/
   //     startAnimation(itemChat)

    }
    fun setItemHomeListener() {
        itemChat.isChecked=false
        itemHome.isChecked=false
        itemMusic.isChecked=true
        itemStatus.isChecked=false
        ivProfileSelfBorder.setBorder("#000000")
        toolbarLayout.setBackgroundColor(Color.parseColor("#FFFFFF"))
       /* toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.text_black))
        if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect_new)
        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
        }*/
       // startAnimation(itemHome)
    }

    fun startAnimation(view:View)
    {
        val x: Int = view.left
        val y: Int = view.bottom
        val startRadius = 0
        val endRadius: Int = Math.max(toolbarLayout.width, toolbarLayout.height)
        val animator: Animator = ViewAnimationUtils.createCircularReveal(toolbarLayout,x,y,startRadius.toFloat(),endRadius.toFloat())
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.duration = 1000

        animator.start()
    }
    fun setItemPopupListener() {
        itemChat.isChecked=false
        itemHome.isChecked=false
        itemMusic.isChecked=false
        itemStatus.isChecked=false
        ivProfileSelfBorder.setBorder("#000000")
       // toolbarLayout.setBackgroundColor(Color.parseColor("#000000"))
       // toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.text_black))
        /*if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect_new)
        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
        }*/
    }
    fun setItemStatusListener() {
        itemChat.isChecked=false
        itemHome.isChecked=true
        itemMusic.isChecked=true
        itemStatus.isChecked=true
//        toolbarLayout.setBackgroundColor(Color.parseColor("#FFFFFF"))
//        toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.pureWhite))
      /*  if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect_new)
        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect)

        }*/
        ivProfileSelfBorder.setBorder("#000000")
        // startAnimation(itemStatus)
    }

    fun setItemProfileListener() {
        itemChat.isChecked=false
        itemHome.isChecked=true
        itemMusic.isChecked=true
        itemStatus.isChecked=false
      //  toolbarLayout.setBackgroundColor(Color.parseColor("#FFFFFF"))
        ivProfileSelfBorder.setBorder(colorCode)
   //     toolbarLayout.backgroundTintList = ColorStateList.valueOf(resources.getColor(R.color.pureWhite))
      /*  if (toolbarLayout.backgroundTintList == ColorStateList.valueOf(resources.getColor(R.color.pureWhite))){
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect_new)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect_new)

        }else{
            itemStatus.setBackgroundResource(R.drawable.ic_nav_status_select_unselect)
            itemChat.setBackgroundResource(R.drawable.ic_nav_chat_select_unselect)

        }*/
    }
    fun setItemChatListener(listener: View.OnClickListener?) {
        itemChat.setOnClickListener(listener)
    }
    fun setItemHomeListener(listener: View.OnClickListener?) {
        itemHome.setOnClickListener(listener)
    }
    fun setItemMusicListener(listener: View.OnClickListener?) {
        layoutCreateVideo.setOnClickListener(listener)
    }
    fun setItemStatusListener(listener: View.OnClickListener?) {
        itemStatus.setOnClickListener(listener)

    }
    fun setItemProfileListener(listener: View.OnClickListener?) {
        ivProfileSelf.setOnClickListener(listener)

    }
    fun setUserProfile(url: String) {
        ivProfileSelf.loadUrl(url)
    }
    fun setUserProfileBorder(code: String) {
        ivProfileSelfBorder.setBorder(code)
        colorCode = code
    }
}