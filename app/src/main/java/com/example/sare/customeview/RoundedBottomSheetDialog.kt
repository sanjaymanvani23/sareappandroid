package com.example.sare.customeview

import android.content.Context
import com.example.sare.R
import com.google.android.material.bottomsheet.BottomSheetDialog

open class RoundedBottomSheetDialog(context: Context) : BottomSheetDialog(context, R.style.BottomSheetDialogTheme)