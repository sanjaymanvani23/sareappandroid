package com.example.sare.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitBuilder {

    public const val BASE_URL_TEST = "https://api.sarevids.com/"
   // public const val BASE_URL_TEST = "http://192.168.1.5:6666/"

    public fun getRetrofit(): Retrofit {
        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        val client = OkHttpClient.Builder().writeTimeout(5L, TimeUnit.MINUTES)
            .readTimeout(5L, TimeUnit.MINUTES)
            .addInterceptor(interceptor)
            .build()


        return Retrofit.Builder()
            .baseUrl(BASE_URL_TEST)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()


    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}