package com.example.sare.api

import com.apollographql.apollo.exception.ApolloException

interface ApolloApiResponseListener {
    fun getApiSuccessResponse(apiResponseManager: ApolloApiResponse<*>)
    fun getApiFailResponse(errorMsg: ApolloException, type: Int)
}