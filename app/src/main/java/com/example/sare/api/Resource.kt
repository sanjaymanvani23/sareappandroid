package com.d.parastask.api

import com.example.sare.data.ResponseData


data class Resource<out T>(
    val status: Boolean
) {
    companion object {
        fun <T> success(data: T): Resource<T> = Resource(status = true)


    }
}