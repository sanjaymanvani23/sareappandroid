package com.example.sare.api

class ApolloApiResponse<T>(var response: T, var type: Int)