package com.example.sare.api

import com.example.sare.data.ResponseData
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Api REST API access points
 */
@JvmSuppressWildcards
interface   ApiService {

    @Multipart
    @POST("rest/api/user/updateProfile")
    fun uploadImage(
        @Part("colorCode") colorCode: RequestBody,
        @Part file: MultipartBody.Part,
        @Header("Authorization") token: String
    ): Call<ResponseData>

    @Multipart
    @POST("rest/api/user/coverImage")
    fun uploadCoverImage(
        @Part file: MultipartBody.Part,
        @Header("Authorization") token: String
    ): Call<ResponseData>


    @Multipart
    @POST("rest/api/user/updateProfile")
    fun updateUserData(
        @Part("colorCode") colorCode: RequestBody,
        @Part("bio") bio: RequestBody,
//        @Part("location") location: RequestBody,
        @Part("name") name: RequestBody,
        @Part("username") username: RequestBody,
        @Part("email") email: RequestBody,

        @Part file: MultipartBody.Part,
        @Header("Authorization") token: String
    ): Call<ResponseData>

    @Multipart
    @POST("rest/api/video/store")
    fun videoStore(
        @Part video: MultipartBody.Part,
        @Part audio: MultipartBody.Part,
        @Part coverImage: MultipartBody.Part,
        @Part("title") title: RequestBody,
        @Part tags: List<MultipartBody.Part>,
        @Part("isDraft") isDraft: RequestBody,
        @Part("name") name: RequestBody,
        @Part("description") description: RequestBody,
        @Part("allowToWatch") allowToWatch: RequestBody,
        @Part("videoToComment") videoToComment: RequestBody,
        @Part("canAnyoneDuet") canAnyoneDuet: RequestBody,
        @Header("Authorization") token: String
    ): Call<ResponseData>


    @Multipart
    @POST("rest/api/video/store")
    fun videoStore(
        @Part video: MultipartBody.Part,
        @Part coverImage: MultipartBody.Part,
        @Part("title") title: RequestBody,
        @Part tags: List<MultipartBody.Part>,
        @Part("isDraft") isDraft: RequestBody,
        @Part("name") name: RequestBody,
        @Part("description") description: RequestBody,
        @Part("allowToWatch") allowToWatch: RequestBody,
        @Part("videoToComment") videoToComment: RequestBody,
        @Part("canAnyoneDuet") canAnyoneDuet: RequestBody,
        @Header("Authorization") token: String
    ): Call<ResponseData>

    @Multipart
    @POST("rest/api/bug/report")
    fun bugReport(
        @Part reportImage: List<MultipartBody.Part>,
        @Part("title") title: RequestBody,
        @Part("category") category: RequestBody,
        @Part("deviceName") deviceName: RequestBody,
        @Part("moblieId") moblieId: RequestBody,
        @Part("description") description: RequestBody,
        @Part("deviceId") deviceId: RequestBody,
        @Part("version") version: RequestBody,
        @Part("androidSdk") androidSdk: RequestBody,
        @Part("appVersion") appVersion: RequestBody,
        @Header("Authorization") token: String
    ): Call<ResponseData>

}
