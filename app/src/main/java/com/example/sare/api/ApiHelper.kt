package com.d.parastask.api

import com.example.sare.api.ApiService
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import retrofit2.http.Part

class ApiHelper(private val apiService: ApiService) {

    suspend fun uploadImage(
        @Part("colorCode") colorCode: RequestBody,
        @Part file: MultipartBody.Part,
        @Header("Authorization") token: String
    ) = apiService.uploadImage(colorCode, file, token)

}