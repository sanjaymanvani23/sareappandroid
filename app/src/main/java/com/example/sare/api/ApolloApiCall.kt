package com.example.sare.api

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.MyApplication
import com.example.sare.extensions.showToast
import com.example.sare.ui.login.LoginActivity

@SuppressLint("ParcelCreator")
class  ApiRequest<T>(
    val context: Context,
    objectType: T,
    private val apiResponseInterface: ApolloApiResponseListener,
    private val TYPE: Int
) : ApolloCall.Callback<T>() {

    private var call: ApolloCall<T>? = null

    init {
        call = objectType as ApolloCall<T>?
        call!!.enqueue(this)
    }


    override fun onResponse(response: Response<T>) {
        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
        val errorMsg = response.errors?.get(0)?.message.toString()

        when (errorCode.toString()) {
            "401" -> {
                MyApplication.bottomBarActivity.showToast(errorMsg)
                var intent=Intent(context,LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)


            }
            "403" -> {
                MyApplication.bottomBarActivity.showToast(errorMsg)
                var intent=Intent(context,LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)

            }
            else -> {
                apiResponseInterface.getApiSuccessResponse(ApolloApiResponse(response, TYPE))
            }
        }

    }

    override fun onFailure(e: ApolloException) {
        e.let { apiResponseInterface.getApiFailResponse(it, TYPE) }
    }

}