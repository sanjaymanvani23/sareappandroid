package com.example.sare.api

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.ApolloQueryCall
import com.example.sare.ExploreTagsListQuery
import com.example.sare.UserVideosQuery
import com.example.sare.VideoListQuery

class ApolloClientService {

    public fun getVideoList1(apolloClient: ApolloClient?,categoryList: List<String>, userId: String, type: String): ApolloQueryCall<VideoListQuery.Data>? {
        return apolloClient?.query(VideoListQuery(categoryList, userId, type, "views",0,150))
    }


    public fun getTagVideoList(apolloClient: ApolloClient?,
        categoryList: MutableList<String?>, userId: String,pageCount: Int,pageSize: Int):
            ApolloQueryCall<VideoListQuery.Data>? {
        return apolloClient?.query(VideoListQuery(categoryList, userId, "random", "createdAt",pageCount,pageSize))
    }

    public fun getUserVideoList(apolloClient: ApolloClient?,userId: String, pageCount:Int,PAGE_SIZE: Int,type: String): ApolloQueryCall<UserVideosQuery.Data>? {
        return apolloClient?.query(UserVideosQuery(userId, pageCount,PAGE_SIZE, type))
    }
}