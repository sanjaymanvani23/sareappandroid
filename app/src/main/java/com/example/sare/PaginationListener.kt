package com.example.sare

import android.util.Log
import androidx.annotation.NonNull
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.Utils.Companion.PAGE_SIZE


abstract class PaginationListener(val layoutManager: RecyclerView.LayoutManager?): RecyclerView.OnScrollListener() {


    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = layoutManager!!.childCount
        val totalItemCount = layoutManager!!.itemCount
        var firstVisibleItemPosition = 0
        if(layoutManager is GridLayoutManager) {
            val layoutManager1 = layoutManager as GridLayoutManager
            firstVisibleItemPosition = layoutManager1!!.findFirstVisibleItemPosition()
        } else if(layoutManager is LinearLayoutManager) {
            val layoutManager1 = layoutManager as LinearLayoutManager
            firstVisibleItemPosition = layoutManager1!!.findFirstVisibleItemPosition()
        } else{
            firstVisibleItemPosition = 0
        }

        Log.d("PaginationListener","isLoading()::::::${isLoading()}")
        Log.d("PaginationListener","isLastPage()::::::${isLastPage()}")
        if (!isLoading() && !isLastPage()) {

            Log.d("PaginationListener", "visibleItemCount()::::::$visibleItemCount")
            Log.d("PaginationListener","firstVisibleItemPosition()::::::${firstVisibleItemPosition}")
            Log.d("PaginationListener","totalItemCount()::::::${totalItemCount}")

            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                loadMoreItems()
            }
        }
    }

    abstract fun loadMoreItems()
    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
}