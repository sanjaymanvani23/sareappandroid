package com.example.sare.signUpDetails.adapter

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.ColorListQuery
import com.example.sare.R
import kotlinx.android.synthetic.main.item_select_color.view.*

class ColorSelectionAdapter(val list: List<ColorListQuery.Color>, private val context: Context) :
    RecyclerView.Adapter<ColorSelectionAdapter.ViewHolder>() {
    var onItemClick: ((ColorListQuery.Color) -> Unit)? = null

    var selectedIMageView: ImageView?= null
    var selectedPosition: Int = -1
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_select_color, parent, false)
        return ViewHolder(
            v
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, onItemClick!!)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: ColorListQuery.Color,
            context: Context,
            onItemClick: (ColorListQuery.Color) -> Unit
        ) {
            var imgColorLogo = itemView.imgColorLogo

            Log.d("tag", "Data : ${list.colorCode()}")

            imgColorLogo.setColorFilter(
                Color.parseColor(list.colorCode()),
                PorterDuff.Mode.SRC_IN
            )


//            onItemClick.invoke(list).apply {

//            }

//            if ()

          //  list.colorCode()?.get(adapterPosition)

            if(selectedPosition == adapterPosition) {
                itemView.imgFrame.visibility = View.VISIBLE
            } else {
                itemView.imgFrame.visibility = View.GONE
            }
            selectedIMageView?.visibility = View.GONE
            itemView.setOnClickListener {
                onItemClick.invoke(list)
                //list.colorCode()?.get(adapterPosition).apply {
                    selectedIMageView?.visibility = View.GONE
                    itemView.imgFrame.visibility = View.VISIBLE
                    selectedIMageView = itemView.imgFrame
                    selectedPosition = adapterPosition
                //}

//                adapterPosition.apply {  itemView.imgFrame.visibility = View.VISIBLE}

//                   if (list.colorCode()[adapterPosition]){
//                    list.colorCode()?.get(adapterPosition).apply {itemView.imgFrame.visibility = View.VISIBLE  }
//                }else{
//                    onItemClick.apply {itemView.imgFrame.visibility = View.GONE  }
//                }
//                onItemClick.apply { }
//                list.colorCode()?.get(adapterPosition)
//                    .apply { itemView.imgFrame.visibility = View.VISIBLE  }

            }






        }
    }

}
