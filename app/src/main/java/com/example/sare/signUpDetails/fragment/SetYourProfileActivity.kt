package com.example.sare.signUpDetails.fragment

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.RelativeLayout
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.d.parastask.MainViewModel
import com.d.parastask.MainViewModelFactory
import com.d.parastask.api.ApiHelper
import com.example.sare.*
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.openActivity
import com.example.sare.extensions.setBorder
import com.example.sare.signUpDetails.adapter.ColorSelectionAdapter
import com.example.sare.signUpDetails.viewmodel.ColorListViewModel
import com.example.sare.signUpDetails.viewmodel.SetYourProfileViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.fragment_set_your_profile.*
import kotlinx.android.synthetic.main.layout_select_color.*
import kotlinx.android.synthetic.main.layout_select_image_options.*
import kotlinx.android.synthetic.main.popup_exit_app.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.sql.Timestamp
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@RequiresApi(Build.VERSION_CODES.M)
class SetYourProfileActivity : AppCompatActivity() {

    private val CAMERA_REQUEST = 1888
    private val MY_CAMERA_PERMISSION_CODE = 100
    private val IMAGE_PICK_CODE = 1000
    private val PERMISSION_CODE = 1001
    var colorListViewModel: ColorListViewModel? = null
    var setYourProfileViewModel: SetYourProfileViewModel? = null
    var timestamp = Timestamp(System.currentTimeMillis())
    companion object {
        var uri: Uri? = null
    }
    var token: String? = null
    var colorCode: String? = null
    var selectedColorCode: String? = null
    var agreementAccepted: String? = null
    var user: String? = null
    var userId: String? = null
    var imageSaved: String? = null
    var colorCodeSaved: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_profile_fragment_new)
        setTheme()
        getSharedPreferences()
        colorListViewModel = ViewModelProvider(this)[ColorListViewModel::class.java]
        setYourProfileViewModel = ViewModelProvider(this)[SetYourProfileViewModel::class.java]
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        user = preferences.getString("USER", null)
        println(timestamp)
        setListeners()
        setBottomSheet()
        getColorPallete()
    }

    override fun onBackPressed() {
        showBackPopup()
    }

    fun showBackPopup() {
        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_exit_app)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnExit.setOnClickListener {
            dialog.dismiss()
            finish()
            finishAffinity()
        }
        dialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)

        user = preferences.getString("USER", null)

        if (user != null) {
            val mainObject = JSONObject(user)
            agreementAccepted = if (mainObject.has("agreementAccepted")) {
                mainObject.getString("agreementAccepted")
            } else {
                "false"
            }
            userId = mainObject.getString("_id")
        }
    }

    private fun getColorPallete() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        colorListViewModel?.getColorList()

        colorListViewModel?.colorArrayList?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(imgProfilePic, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            colorListViewModel?.getColorList()
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.colorList -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    setBottomSheetForColor(it.response)
                }
            }
        })
    }

    private fun setListeners() {

        imgBackSetYourProfile?.setOnClickListener {
            onBackPressed()
            finish()
        }
        val uuid = Settings.Secure.getString(
            contentResolver, Settings.Secure.ANDROID_ID)
        val apolloClient = ApolloClient.setupApollo("")
        textViewSkipForNow?.setOnClickListener {
            if (agreementAccepted.equals("false")) {
                openActivity(FragmentTermsCondition::class.java)

            } else {
                val preferences: SharedPreferences = getSharedPreferences(
                    StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                val userInfoListJsonString = user
                preferences.edit().putString("USER", userInfoListJsonString).apply()
                openActivity(BottomBarActivity::class.java)
                finish()
            }

        }

        button_next?.setOnClickListener {
            when {
                uri == null -> {
                    customToastMain("Please select picture to upload.!", 0)
                }
                selectedColorCode == null -> {
                    customToastMain("Please select preferred color.!", 0)
                }
                else -> {
                    button_next?.startAnimation {
                        uploadFile(uri!!)
                    }
                }
            }
        }

    }

    private fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = contentResolver.query(uri!!, projection, null, null, null) ?: return null
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }

    private fun uploadFile(fileUri: Uri) {

        val file = File(getPath(fileUri))

        val requestFile: RequestBody = file.asRequestBody(
            contentResolver.getType(fileUri)!!.toMediaTypeOrNull())

        val body = MultipartBody.Part.createFormData("profile", file.name, requestFile)

        val descriptionString = selectedColorCode.toString()
        val description: RequestBody = descriptionString.toRequestBody(MultipartBody.FORM)

        // finally, execute the request
        Log.d("tag", "description::$description")
        Log.d("tag", "descriptionString::$descriptionString")
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        setYourProfileViewModel?.setYourProfileViewModel(
            description, body, "Bereader ${token.toString()}")


        setYourProfileViewModel?.status?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    button_next?.revertAnimation()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(button_next, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            setYourProfileViewModel?.setYourProfileViewModel(
                                description, body, "Bereader ${token.toString()}")
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    button_next?.revertAnimation()
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.uploadImage -> {

                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", ":response:uploadimage::::" + it.response)
                    if (it.response.status == 200) {
                        customToastMain(getString(R.string.upload_success_message), 1)
                        imageSaved = getPath(uri)

                        colorCodeSaved = selectedColorCode

                        val preferences: SharedPreferences = getSharedPreferences(
                            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                        preferences.edit().putString("imageSaved", imageSaved).apply()
                        preferences.edit().putString("colorCodeSaved", colorCodeSaved).apply()
                        getUserDetails(userId.toString())
                        if (agreementAccepted.equals("false")) {
                            openActivity(FragmentTermsCondition::class.java)
                        } else {

                            getUserDetails(userId.toString())
                            openActivity(BottomBarActivity::class.java)
                            finish()
                        }
                    } else {
                        button_next?.revertAnimation()
                        customToastMain(getString(R.string.api_error_message), 0)
                    }

                }

            }

        })


    }

    private fun getUserDetails(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(id)).enqueue(object : ApolloCall.Callback<UserDetailsQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                e.printStackTrace()
                runOnUiThread {
                    customToastMain(e.message.toString(), 0)
                   MyApplication.mainActivity.logoutUser()
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()

                }
            }

            override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                //                    runBlocking {
                runOnUiThread {
                    progressDialog.hide()
                }
                Log.d("tag", "ResponseData::$response")
                runOnUiThread {
                    if (response.data?.user() != null) {
                        val preferences: SharedPreferences = getSharedPreferences(
                            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                        val gson = Gson()

                        // Get java object list json format string.
                        val userInfoListJsonString = gson.toJson(response.data?.user())

                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {

                            customToastMain(error.toString(), 0)
                           MyApplication.mainActivity.logoutUser()
                            openActivity(LoginActivity::class.java)
                            finish()
                            finishAffinity()
                        } else {
                            customToastMain(error.toString(), 0)
                        }
                    }

                }

                //                    }
            }
        })
    }

    private fun initRecyclerView(arrayList: List<ColorListQuery.Color>) {

        menu_recycler_view?.apply {
            layoutManager = LinearLayoutManager(
                menu_recycler_view?.context, RecyclerView.HORIZONTAL, false)
            adapter = ColorSelectionAdapter(
                arrayList, this@SetYourProfileActivity)

            (adapter as ColorSelectionAdapter).onItemClick = { data ->

                // do something with your item
                Log.d("tag", data.colorCode().toString())
                colorCode = data.colorCode()
            }
        }


    }

    private fun setBottomSheet() {
        val llBottomSheet = bottom_sheet_new

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        imgProfilePic?.setOnClickListener {
            overlay_view_profile?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        overlay_view_profile?.setOnClickListener {
            overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }


        txtTakePhoto?.setOnClickListener {
            overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA)
                //show popup to request runtime permission
                requestPermissions(permissions, MY_CAMERA_PERMISSION_CODE)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
        }

        txtChooseFromGallery?.setOnClickListener {
            overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission denied
                    val permissions = arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    //permission already granted
                    pickImageFromGallery()
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery()
            }
        }


        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view_profile?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode === MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)

            } else {
                customToastMain(getString(R.string.permission_denied), 0)
            }
        } else if (requestCode === PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery()
            } else {
                //permission from popup denied
                customToastMain(getString(R.string.permission_denied), 0)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null && data.data != null && requestCode == IMAGE_PICK_CODE) {
            try {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor = selectedImage?.let {
                    contentResolver.query(
                        it, filePathColumn, null, null, null)
                }!!
                cursor.moveToFirst()
                val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                val picturePath: String = cursor.getString(columnIndex)
                cursor.close()
                data.putExtra("picturePath", picturePath)
                setResult(Activity.RESULT_OK, data)


            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error::${e.message}")
                val returnFromGalleryIntent = Intent()
                setResult(
                    Activity.RESULT_CANCELED, returnFromGalleryIntent)
                finish()
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode === CAMERA_REQUEST) {
            val picturePath: Bitmap = data!!.extras!!["data"] as Bitmap
            val filesDir: File = filesDir

            val imageFile = File(filesDir, timestamp.time.toString() + ".jpg")
            println("imageFile:::$imageFile")

            val os: OutputStream
            try {
                os = FileOutputStream(imageFile)
                picturePath.compress(Bitmap.CompressFormat.JPEG, 90, os)
                os.flush()
                os.close()
            } catch (e: java.lang.Exception) {
                Log.e(javaClass.simpleName, "Error writing bitmap", e)
            }
            Log.d("tag", "picturePath Camera::$picturePath")
            imageCrop(imageFile)


        }

        if (requestCode === IMAGE_PICK_CODE) {
            if (resultCode === Activity.RESULT_OK) {
                val picturePath = data!!.getStringExtra("picturePath")
                //perform Crop on the Image Selected from Gallery
                Log.d("tag", "picturePath::$picturePath")
                performCrop(picturePath.toString())
            }
        }

        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {

                val path: String = MediaStore.Images.Media.insertImage(contentResolver, result.uri.path, "IMG_" + Calendar.getInstance().time, null)

                uri = Uri.parse(path)
                Glide.with(this)
                    .load(uri.toString())
                    .placeholder(R.drawable.ic_shape_round)
                    .error(R.drawable.ic_shape_round)
                    .into(imgProfilePic)

                imgProfileUserBorder.setBorder(colorCode ?: Utils.COLOR_CODE)

            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                runOnUiThread {
                    customToastMain("Something went wrong.!", 0)
                }

            }
        }

    }

    private fun setBottomSheetForColor(arrayList: List<ColorListQuery.Color>) {
        val llBottomSheet = bottom_sheet

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)
        initRecyclerView(arrayList)

        llBottomSheet.setOnClickListener {
            overlay_view?.visibility = View.VISIBLE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        relColor?.setOnClickListener {

            overlay_view?.visibility = View.VISIBLE

            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        txtDone?.setOnClickListener {

            if (colorCode != null) {
                overlay_view?.visibility = View.GONE
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                imgFrameColor?.visibility = View.VISIBLE
                imgFrame?.visibility = View.GONE


                Glide.with(this)
                    .load(uri.toString())
                    .placeholder(R.drawable.ic_shape_round)
                    .error(R.drawable.ic_shape_round)
                    .into(imgProfilePic)

                imgProfileUserBorder.setBorder(colorCode ?: Utils.COLOR_CODE)
                imgFrameColor?.setColorFilter(Color.parseColor(colorCode), PorterDuff.Mode.SRC_IN)
                selectedColorCode = colorCode
            }

        }



        overlay_view?.setOnClickListener {
            overlay_view?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                     }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        button_next?.dispose()
    }

    private fun imageCrop(file: File) {
        try {
            val contentUri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", file)
            CropImage.activity(contentUri).setGuidelines(CropImageView.Guidelines.ON).start(this)
        }
        catch (anfe: ActivityNotFoundException) {
            val errorMessage = "your device doesn't support the crop action!"
            customToastMain(errorMessage, 0)
        }
    }

    private fun performCrop(picUri: String) {
        try {

            val f = File(picUri)

            val contentUri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", f)

            CropImage.activity(contentUri).setAspectRatio(1, 1).setGuidelines(CropImageView.Guidelines.ON).start(this)
        }
        catch (anfe: ActivityNotFoundException) {
            val errorMessage = "your device doesn't support the crop action!"
            customToastMain(errorMessage, 0)
        }
    }
}