package com.example.sare.signUpDetails.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.example.sare.R
import kotlinx.android.synthetic.main.fragment_user_profile.view.*

class FragmentUserProfile : Fragment() {

    var root : View? = null
    var TAG = "FragmentUserProfile"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_user_profile, container, false)

        root?.scroller?.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, _, scrollY, _, oldScrollY ->

            if (scrollY > oldScrollY) {
                Log.e(TAG, "Scroll DOWN")
            }
            if (scrollY < oldScrollY) {
                Log.e(TAG, "Scroll UP")
            }
            if (scrollY == 0) {
                Log.e(TAG, "TOP SCROLL")
            }
            if (scrollY == v.measuredHeight - v.getChildAt(0).measuredHeight) {
                Log.e(TAG, "BOTTOM SCROLL")
            }
        })

        return root
    }

}