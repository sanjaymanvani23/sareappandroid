package com.example.sare.signUpDetails.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.ColorListQuery
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class ColorListViewModel : ViewModel() {
    var colorArrayList = MutableLiveData<NetworkResponse>()
    val apolloClient = ApolloClient.setupApollo("")

    fun getColorList() {
        apolloClient.query(ColorListQuery.builder().build())
            .enqueue(object : ApolloCall.Callback<ColorListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    colorArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<ColorListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.colors() != null) {
                                colorArrayList.postValue(
                                    NetworkResponse.SUCCESS.colorList(
                                        response.data()?.colors()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                colorArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            colorArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                        }
                    }
                }
            })
    }
}