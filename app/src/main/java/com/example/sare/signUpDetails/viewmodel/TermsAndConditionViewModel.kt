package com.example.sare.signUpDetails.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.AcceptAgreementMutation
import com.example.sare.ApolloClient
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class TermsAndConditionViewModel : ViewModel() {
    var status = MutableLiveData<NetworkResponse>()

    fun acceptAgreement(token: String) {

        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.mutate(
            AcceptAgreementMutation(
                "",
                "",
                "",
                "",
                "",
                2.toString(),
                "Agreement",
                ""
            )
        )
            .enqueue(object : ApolloCall.Callback<AcceptAgreementMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    e.printStackTrace()
                    status.postValue(NetworkResponse.ERROR(e))


                }

                override fun onResponse(response: Response<AcceptAgreementMutation.Data>) {

                    try{
                    if (response.data()?.updateUser() != null) {
                            runBlocking {
                                status.postValue(
                                    NetworkResponse.SUCCESS.acceptAgreement(
                                        response.data!!.updateUser()
                                    )
                                )
                            }

                    }  else {
                        val errorCode =
                            response.errors?.get(0)?.customAttributes?.get("status")
                        NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                        status.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                    }
                    } catch (e: Exception) {
                        status.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }

                }
            })
    }

}