package com.example.sare.signUpDetails.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.sare.MainActivity
import com.example.sare.api.ApiService
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.NetworkResponse
import com.example.sare.data.ResponseData
import com.example.sare.extensions.customToast
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SetYourProfileViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var status = MutableLiveData<NetworkResponse>()


    fun setYourProfileViewModel(description: RequestBody, body: MultipartBody.Part, token: String) {
        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)

        val call: Call<ResponseData> = service.uploadImage(
            description, body,
            token.toString()
        )

        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(
                call: Call<ResponseData?>?,
                response: Response<ResponseData?>?
            ) {
                Log.e("Upload Success:", response?.body().toString())

                if (response!!.isSuccessful) {

                    if (response.body() != null) {
                        runBlocking {
                            status.postValue(
                                NetworkResponse.SUCCESS.uploadImage(
                                    response.body()!!
                                )
                            )
                        }
                    } else {
                        runBlocking {

                            status.postValue(
                                NetworkResponse.ERROR_RESPONSE(
                                    response.errorBody().toString()
                                )
                            )
                        }
                    }

                } else {
                    runBlocking {
                        status.postValue(
                            NetworkResponse.ERROR_RESPONSE(
                                response.errorBody().toString()
                            )
                        )
                    }
                }


            }

            override fun onFailure(
                call: Call<ResponseData?>,
                t: Throwable
            ) {
                t.printStackTrace()
                status.postValue(NetworkResponse.ERROR(t))

                Log.e("Upload error:", t.message!!)

                context.runCatching {
                    customToast(t.message.toString(), context as MainActivity, 0)
                }
            }
        })
    }
}