package com.example.sare.signUpDetails.fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.signUpDetails.viewmodel.TermsAndConditionViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_terms_condition.*
import kotlinx.android.synthetic.main.popup_exit_app.*

@RequiresApi(Build.VERSION_CODES.M) class FragmentTermsCondition : AppCompatActivity() {

    var root: View? = null
    var token: String? = null
    var termsAndConditionViewModel: TermsAndConditionViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_terms_condition)

        getSharedPreferences()
        termsAndConditionViewModel = ViewModelProvider(this)[TermsAndConditionViewModel::class.java]
        setListeners()
        setTheme()
    }

    fun showBackPopup() {
        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_exit_app)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnExit.setOnClickListener {
            dialog.dismiss()
            finish()
            finishAffinity()
        }

        dialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onBackPressed() {
        showBackPopup()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::terms and condition$token")
    }


    private fun setListeners() {
        imgBack.setOnClickListener {
           finish()
        }
        terms_of_use_layout.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vogella.com/"))
            startActivity(i)
        }

        user_agreement_text.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vogella.com/"))
            startActivity(i)
        }

        privacy_policy_layout.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW, Uri.parse("https://www.vogella.com/"))
            startActivity(i)
        }


        button_finish.setOnClickListener {
            if (terms_condition_checkbox!!.isChecked) {

                button_finish.startAnimation {
                    acceptAgreement(token.toString())
                }
            } else {
                customToastMain("Please add tick to the checkbox!!",  0)
            }
        }

    }

    fun acceptAgreement(token: String) {
        termsAndConditionViewModel?.acceptAgreement(token.toString())

        termsAndConditionViewModel?.status?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    button_finish.revertAnimation()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(privacy_policy_layout, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            termsAndConditionViewModel?.acceptAgreement(token.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    button_finish.revertAnimation()
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.acceptAgreement -> {
                    Log.d("tag", "Data Sucess: ${it.response}")
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                    val gson = Gson()

                    // Get java object list json format string.
                    val userInfoListJsonString = gson.toJson(it.response)
                    Log.d("tag", "userInfoListJsonString::::$userInfoListJsonString")
                    preferences.edit().putString("USER", userInfoListJsonString).apply()
                    openActivity(BottomBarActivity::class.java)
                    finish()
                    finishAffinity()

                }

            }

        })
    }


    private fun setTheme() {
       window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
       window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
           window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
           window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
        terms_of_use_layout.background = ThemeManager.getDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.termsAndConditionPageBackground), R.dimen.io_list_corner_radius)
        terms_of_use_text.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))

        user_agreement_layout.background = ThemeManager.getDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.termsAndConditionPageBackground), R.dimen.io_list_corner_radius)

        user_agreement_text.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))

        privacy_policy_layout.background = ThemeManager.getDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.termsAndConditionPageBackground), R.dimen.io_list_corner_radius)
        privacy_policy_text.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))


    }

    override fun onDestroy() {
        super.onDestroy()
        button_finish.dispose()
    }

}