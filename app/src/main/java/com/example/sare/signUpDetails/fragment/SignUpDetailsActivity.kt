package com.example.sare.signUpDetails.fragment

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.*
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.ui.login.LoginActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_signup_details.*
import kotlinx.android.synthetic.main.popup_exit_app.*
import java.util.*

@RequiresApi(Build.VERSION_CODES.M)
class SignUpDetailsActivity : AppCompatActivity() {

    private val resultLoadImage: Int = 1
    private var gender = ""
    var name = ""
    var email = ""
    var type = ""
    var countryId = ""
    var uuid: String? = null
    var token: String = ""
    var datepicker: DatePickerDialog? = null
    var city: String = ""
    var state: String = ""
    var location: String = ""
    var isSowing=true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_email_verify)

        setTheme()
        getSharedPreferences()

        intent.extras ?.run {
            email = getString("email" ,"").toString()
            city = getString("city","").toString()
            state = getString("state","").toString()
            location = getString("location","").toString()
        }

        if (email.isEmpty() || email == "" || email == "null") {
            txtEmail?.isClickable = true
            txtEmail?.isFocusable = true
            txtEmail?.isFocusableInTouchMode = true
            txtEmail?.isCursorVisible = true
            txtEmail?.alpha = 1f

            txtEmail?.setText("")
        } else {
            txtEmail?.isClickable = false
            txtEmail?.isFocusable = false
            txtEmail?.isFocusableInTouchMode = false
            txtEmail?.isCursorVisible = false
            txtEmail?.alpha = 0.6f

            txtEmail?.setText(email)
        }
        uuid = Settings.Secure.getString(
            contentResolver,
            Settings.Secure.ANDROID_ID
        )
        Log.d("tag", "uuid::$uuid")

        txtPlace?.text = "$city, $state, $location"

        txtDateOfBirth?.inputType = InputType.TYPE_NULL
        txtDateOfBirth?.keyListener = null
        setListeners()


        if (txtDateOfBirth.hasFocus()){
            txtInputUserName.setBorderColor("#919191")
        }
    }


    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        token = preferences.getString("TOKEN", null).toString()

        Log.d("tag", "token::$token")
    }


    private fun setListeners() {

        txtDateOfBirth?.setOnTouchListener(View.OnTouchListener { p0, p1 ->
            openDatePickerDialog()
            return@OnTouchListener false

        })
        txtDateOfBirth1?.setOnClickListener {
            openDatePickerDialog()
        }
        txtMale?.setOnClickListener {

            gender = "Male"

            txtMale?.setTextColor(
                resources.getColor(
                    R.color.textColor,
                    theme
                )
            )

            txtMale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.otp_edit_box_corner_radius, 5, R.color.textColor
            )

            txtFemale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtFemale?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )

            txtOther?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtOther?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )
        }

        txtFemale?.setOnClickListener {
            gender = "Female"


            txtFemale?.setTextColor(
                resources.getColor(
                    R.color.textColor,
                    theme
                )
            )
            txtFemale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.otp_edit_box_corner_radius, 5,  R.color.textColor
            )

            txtOther?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtOther?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )

            txtMale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtMale?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )
        }

        txtOther?.setOnClickListener {
            gender = "Other"

            txtOther?.setTextColor(
                resources.getColor(
                    R.color.textColor,
                    theme
                )
            )


            txtOther?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.otp_edit_box_corner_radius, 5, R.color.textColor
            )

            txtFemale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtFemale?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )

            txtMale?.background = ThemeManager.otpGetDrawable(
                this, ThemeManager.colors(
                    this,
                    StringSingleton.textWhite
                ), R.dimen.card_corner, 3, R.color.gender_text
            )

            txtMale?.setTextColor(
                resources.getColor(
                    R.color.gender_text,
                    theme
                )
            )

        }

        signUp()
    }

    private fun signUp() {
        button_next?.setOnClickListener {
            validate()
        }
    }

    fun openDatePickerDialog()
    {
        Log.e("Open Dialog", "Dialog")
        val cldr = Calendar.getInstance()
        val day = cldr[Calendar.DAY_OF_MONTH]
        val month = cldr[Calendar.MONTH]
        val year = cldr[Calendar.YEAR]
        cldr.add(Calendar.YEAR, -18)
        // date picker dialog
        datepicker = DatePickerDialog(this, object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                txtDateOfBirth!!.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year)
                isSowing=true
            }
        }, year, month, day)

        datepicker?.setOnCancelListener {
            isSowing=true
        }
        datepicker!!.datePicker.maxDate = cldr.timeInMillis

        if(isSowing)
        {
            datepicker!!.show()
            isSowing=false
        }

    }

    private fun validate(): Boolean {
        val userName = txtUserName?.text.toString()
        val fullName = txtFullName?.text.toString()
        if (txtFullName?.text.toString().isEmpty()) {
            runOnUiThread {
                customToastMain("Full name can't be empty",0)
            }
        } else if (txtFullName?.text?.length!! < 6) {
            runOnUiThread {
                customToastMain("Fullname must be of length 6 or more",0)

            }
        } else if (txtFullName?.text?.length!! >= 6 && (TextUtils.isDigitsOnly(txtFullName?.text?.toString()) || !fullName.matches(
                "^[A-Za-z0-9_. ]*$".toRegex()
            )) ||
            txtFullName?.text?.toString()!!
                .startsWith(" ")
        ) {
            runOnUiThread {
                customToastMain("Please enter proper fullname  \n Ex: Sare Team ",0)
            }
        } else if (txtUserName?.text.toString().isEmpty()) {
            runOnUiThread {
                customToastMain("User name can't be empty",0)
            }
        } else if (txtUserName?.text?.toString()!!
                .startsWith(" ") || txtUserName?.text!!.length < 6 || txtUserName?.text!!.length > 20
            || !userName.matches("^[A-Za-z0-9_.]*$".toRegex())
        ) {
            runOnUiThread {
                customToastMain("Please enter proper username having 6 letters, no blank space, capital letters, small letters or underscore \n Ex: Sareteam",0)
            }
        } else if (txtEmail?.text.toString().isEmpty()) {
            runOnUiThread {
                customToastMain("Email can't be empty",0)
            }
        } else if (txtEmail?.text.toString().isNotEmpty() && !txtEmail?.text.toString()
                .matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$".toRegex())
        ) {
            runOnUiThread {
                customToastMain("Please enter valid email address \n Ex: sareteam@gmail.com",0)
            }
        } else if (gender.toString().isEmpty()) {
            runOnUiThread {
                customToastMain("Please select gender",0)
            }
        } else if (txtDateOfBirth?.text.toString().isEmpty()) {
            runOnUiThread {
                customToastMain("DOB can't be empty",  0)
            }
        } else {

            val progressDialog = CustomProgressDialogNew(this)
            runOnUiThread {
                progressDialog.hide()
            }
            runOnUiThread {

                button_next?.startAnimation()
            }
            val apolloClient = ApolloClient.setupApollo(token.toString())
            apolloClient.mutate(
                AcceptAgreementMutation(
                    txtUserName.text.toString().trim(),
                    txtFullName.text.toString().trim(),
                    txtEmail?.text.toString().trim(),
                    txtDateOfBirth.text.toString(),
                    gender,

                    "",
                    "",
                    ""
                )
            ).enqueue(object : ApolloCall.Callback<AcceptAgreementMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    runOnUiThread {

                        button_next?.revertAnimation()
                    }
                    customToastMain(e.message.toString(),  0)
//                    MyApplication.logoutClearPreference(this@SignUpDetailsActivity)
                    MyApplication.mainActivity.logoutUser()
                    openActivity(LoginActivity::class.java)
                    finish()
                    finishAffinity()
                }

                override fun onResponse(response: Response<AcceptAgreementMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data()?.updateUser() != null) {
                        Log.d("tag", "Data Success:: $response")
                        val preferences: SharedPreferences = getSharedPreferences(
                            StringSingleton.sharedPrefFile,
                            Context.MODE_PRIVATE
                        )
                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(response.data!!.updateUser())

                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        preferences.edit().remove("imageSaved").apply()
                        preferences.edit().remove("colorCodeSaved").apply()
                        openActivity(SetYourProfileActivity::class.java)
                        finish()
                        finishAffinity()
                    } else {
                        runOnUiThread {

                            button_next?.revertAnimation()
                        }
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        runOnUiThread {
                            val errorCode =
                                response.errors?.get(0)?.customAttributes?.get("status")
                            when {
                                errorCode?.toString()?.equals("401") == true -> {
                                    openActivity(LoginActivity::class.java)
                                    finish()
                                    finishAffinity()

                                }
                                error?.toString()?.equals("403") == true -> {
                                    openActivity(LoginActivity::class.java)
                                    finish()
                                    finishAffinity()
                                }
                                else -> {
                                    customToastMain(error.toString(),  0)
                                }
                            }

                        }

                    }
                }
            })
        }
        return false

    }


    private fun imageShape(picturePath: String) {
        val bitmap =
            BitmapFactory.decodeResource(
                resources,
                R.drawable.ic_email_verification_lt
            )
        var original = BitmapFactory.decodeFile(picturePath)

        val expectedHeight = 200
        val expectedWidth = 200

        original = getResizedBitmap(
            original,
            dipToPixels(
                this,
                expectedHeight.toFloat()
            ),
            dipToPixels(
                this,
                expectedWidth.toFloat()
            )
        )

        val result =
            Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val mCanvas = Canvas(result)
        val paint =
            Paint(Paint.ANTI_ALIAS_FLAG)
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)

        val widthMask = bitmap.width
        val heightMask = bitmap.height
        val centerX = (widthMask - original.width) * 0.5f
        val centerY = (heightMask - original.height) * 0.5f

        mCanvas.drawBitmap(original, centerX, centerY, null)
        mCanvas.drawBitmap(bitmap, 0f, 0f, paint)
        paint.xfermode = null

    }


    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this,
            StringSingleton.bodyBackground
        )
        window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this,
                StringSingleton.bodyBackground
            )
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_VISIBLE
        }
        otp_verify_logo?.setImageDrawable(
            ThemeManager.image(
                this,
                StringSingleton.verificationLogo
            )
        )
        main_layout?.setBackgroundColor(
            ThemeManager.colors(
                this,
                StringSingleton.textWhite
            )
        )
    }

    private fun getResizedBitmap(
        image: Bitmap,
        newHeight: Float,
        newWidth: Float
    ): Bitmap? {
        val width = image.width
        val height = image.height
        val scaleWidth = newWidth / width
        val scaleHeight = newHeight / height
        val matrix = Matrix()
        matrix.postScale(scaleWidth, scaleHeight)
        return Bitmap.createBitmap(image, 0, 0, width, height, matrix, false)
    }

    private fun dipToPixels(context: Context, dipValue: Float): Float {
        val metrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == resultLoadImage && resultCode == Activity.RESULT_OK && null != data) {
            val selectedImage: Uri? = data.data
            val filePathColumn =
                arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? = selectedImage?.let {
                contentResolver?.query(
                    it,
                    filePathColumn, null, null, null
                )
            }
            cursor?.moveToFirst()
            val columnIndex: Int = cursor?.getColumnIndex(filePathColumn[0])!!
            val picturePath: String = cursor.getString(columnIndex)
            //Convert Image to shape
            imageShape(picturePath)
            cursor.close()
        }
    }


    private fun hideKeyboard(activity: Activity) {
        val inputManager = activity
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // check if no view has focus:
        val currentFocusedView: View? = activity.currentFocus
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(
                currentFocusedView.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        runOnUiThread {
            button_next?.dispose()
        }
    }

    fun showBackPopup() {

        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_exit_app)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnExit.setOnClickListener {
            dialog.dismiss()
            finish()
            finishAffinity()
        }

        dialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    override fun onBackPressed() {
        showBackPopup()
    }
}