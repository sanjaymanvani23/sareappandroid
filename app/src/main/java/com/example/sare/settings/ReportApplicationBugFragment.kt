package com.example.sare.settings

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.BugTypesQuery
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.settings.adapter.BugTypesListAdapter
import com.example.sare.settings.adapter.ImageFramesAdapter
import com.example.sare.settings.viewmodel.ReportApplicationBugViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_report_application_bug.view.*
import kotlinx.android.synthetic.main.popup_upload_success.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class ReportApplicationBugFragment : Fragment() {
    var root: View? = null
    private val PERMISSION_CODE = 1001
    private val IMAGE_PICK_CODE = 1000
    var reportApplicationBugViewModel: ReportApplicationBugViewModel? = null
    var token: String? = ""
    var selectedImage: Uri? = null
    var showRecyclerView: Boolean = false
    var selectedImageList: ArrayList<Uri> = ArrayList()
    val list: ArrayList<File> = ArrayList()
    var categoryId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_report_application_bug, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@ReportApplicationBugFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        getSharedPreferences()
        reportApplicationBugViewModel =
            ViewModelProvider(this)[ReportApplicationBugViewModel::class.java]
        bugTypesList()
        setListeners()
        return root
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        token = preferences.getString("TOKEN", null)
    }

    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.imgOpenGallery?.setOnClickListener {
            openCamera()
        }
        root?.txtProblemReport?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length!! >= 150) {
                    customToast("Description can't be more than 150 letters", requireActivity(), 0)
                }
            }
        })
        root?.button_send?.setOnClickListener {
            when {
                root?.txtCategory?.text?.isNullOrEmpty()!! -> {
                    customToast("Please select any one category", requireActivity(), 0)
                }
                root?.txtTitleReport?.text?.isNullOrEmpty()!! -> {
                    customToast("Please write title for report", requireActivity(), 0)
                }
                root?.txtProblemReport?.text?.isNullOrEmpty()!! -> {
                    customToast("Please describe your problem in detail", requireActivity(), 0)
                }
                root?.txtProblemReport?.text?.length!! < 6 -> {

                    customToast("Description should be more than 6 letters", requireActivity(), 0)
                }
                else -> {
//                    root?.button_send?.startAnimation()
                    reportBug()
                }
            }
        }
        root?.relCategory?.setOnClickListener {
            root?.rvCategory?.visibility = View.VISIBLE
            if (showRecyclerView) {
                root?.rvCategory?.visibility = View.GONE
                bugTypesList()
                showRecyclerView = false
            } else {
                root?.rvCategory?.visibility = View.VISIBLE
                showRecyclerView = true
            }
        }
    }

    private fun openCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED ||
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED ||
                requireActivity().checkSelfPermission(Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_DENIED
            ) {
                //permission denied
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA

                )
                //show popup to request runtime permission
                requestPermissions(permissions, PERMISSION_CODE)
            } else {
                //permission already granted
                pickImageFromGallery()
            }
        } else {
            //system OS is < Marshmallow
            pickImageFromGallery()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("tag", "requestCode:::$requestCode")
        if (requestCode === PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                //permission from popup granted
                pickImageFromGallery()
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data != null && data.data != null && requestCode == IMAGE_PICK_CODE) {
            selectedImage = data.data
            if (selectedImageList.size < 5) {
                selectedImage?.let { selectedImageList.add(it) }
                initRecyclerView(selectedImageList)

            } else {
                customToast("You can only add 5 screenshots!!", requireActivity(), 0)
            }
        }
    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )

        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    private fun initRecyclerView(selectedImageList: ArrayList<Uri>) {
        root?.rvThumbnail?.visibility = View.VISIBLE
        root?.rvThumbnail?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvThumbnail!!.context,
                RecyclerView.HORIZONTAL, false
            )
            adapter = ImageFramesAdapter(selectedImageList)

        }

    }

    private fun reportBug() {
        val uuid = Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)
        for (s in selectedImageList) {
            list.add(File(getPath(s)))
        }

        val multipartList: ArrayList<MultipartBody.Part> = ArrayList()
        for (i in 0 until list.size) {
            val requestFile: RequestBody = RequestBody.create(
                "image/jpg".toMediaTypeOrNull(),
                list[i]
            )
            val body = MultipartBody.Part.createFormData("reportImage", list[i].name, requestFile)
            multipartList.add(body)
        }

        val descriptionString = root?.txtProblemReport?.text.toString()
        val titleString = root?.txtTitleReport?.text.toString()
        val categoryString = categoryId.toString()
        val deviceNameString = Build.MODEL.toString()
        val mobileIdString = uuid.toString()
        val deviceIdString = uuid.toString()
        val versionString = Build.VERSION.SDK_INT.toString()
        val androidSDKString = Build.VERSION.SDK.toString()
        val appVersionString = requireContext().packageManager.getPackageInfo(
            requireContext().packageName,
            0
        ).versionName.toString()

        val description: RequestBody = descriptionString
            .toRequestBody(MultipartBody.FORM)
        val title: RequestBody = titleString
            .toRequestBody(MultipartBody.FORM)
        val category: RequestBody = categoryString
            .toRequestBody(MultipartBody.FORM)
        val deviceName: RequestBody = deviceNameString
            .toRequestBody(MultipartBody.FORM)
        val mobileId: RequestBody = mobileIdString
            .toRequestBody(MultipartBody.FORM)
        val version: RequestBody = versionString
            .toRequestBody(MultipartBody.FORM)
        val androidSDK: RequestBody = androidSDKString
            .toRequestBody(MultipartBody.FORM)
        val appVersion: RequestBody = appVersionString
            .toRequestBody(MultipartBody.FORM)
        val deviceId: RequestBody = deviceIdString
            .toRequestBody(MultipartBody.FORM)

        // finally, execute the request
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.hide()
        }
        reportApplicationBugViewModel?.reportApplicationBugViewModel(
            multipartList,
            title,
            description,
            mobileId,
            deviceId,
            category,
            androidSDK,
            version,
            deviceName,
            appVersion,
            "Bereader ${token.toString()}",
            requireActivity()
        )


        reportApplicationBugViewModel?.status?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
//                    root?.button_send?.revertAnimation()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            reportApplicationBugViewModel?.reportApplicationBugViewModel(
                                multipartList,
                                title,
                                description,
                                mobileId,
                                deviceId,
                                category,
                                androidSDK,
                                version,
                                deviceName,
                                appVersion,
                                "Bereader ${token.toString()}",
                                requireActivity()
                            )
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
//                    root?.button_send?.revertAnimation()
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.bugReport -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", ":response:bug report::::" + it.response)
                    if (it.response.status == 200) {
//                       customToast(it.response.message.toString(), requireActivity(), 1)
                        showUploadPopup()
                    } else {
//                        root?.button_send?.revertAnimation()
                        customToast(getString(R.string.api_error_message), requireActivity(), 0)
                    }
                }
            }
        })

    }

    @NonNull
    private fun prepareImageFilePart(
        partName: String,
        file: File
    ): MultipartBody.Part {
        val requestFile: RequestBody = RequestBody.create(
            "image/jpg".toMediaTypeOrNull(),
            file
        )
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

    private fun getPath(uri: Uri?): String? {
        val projection =
            arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor =
            requireContext().contentResolver.query(uri!!, projection, null, null, null)
                ?: return null
        val column_index =
            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }

    override fun onDestroy() {
        super.onDestroy()
//        root?.button_send?.dispose()
    }

    private fun bugTypesList() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.hide()
        }
        reportApplicationBugViewModel?.getBugTypes(token.toString())
        reportApplicationBugViewModel?.bugTypeArrayList?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar
                        .make(
                            requireView(),
                            "Connection Error!",
                            Snackbar.LENGTH_SHORT
                        )
                        .setAction("RETRY") {
                            reportApplicationBugViewModel?.getBugTypes(token.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.getBugTypes -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    initBugRecyclerView(it.response)
                }

            }
        })
    }

    private fun initBugRecyclerView(list: List<BugTypesQuery.BugType>) {
        root?.rvCategory?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvCategory?.context, RecyclerView.VERTICAL, false
            )
            adapter =
                BugTypesListAdapter(list, context)
            (adapter as BugTypesListAdapter).onItemClick = { data ->
                root?.rvCategory?.visibility = View.GONE
                root?.txtCategory?.text = data.name().toString()
                categoryId = data._id().toString()
            }
        }
    }

    private fun showUploadPopup() {
        val dialog = Dialog(requireActivity(), R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_upload_success)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.txtHeading?.text = "Your request sent successfully"
        dialog.btnDone.setOnClickListener {
            dialog.dismiss()
            NavHostFragment.findNavController(this).navigateUp()
        }
        dialog.show()
    }


}