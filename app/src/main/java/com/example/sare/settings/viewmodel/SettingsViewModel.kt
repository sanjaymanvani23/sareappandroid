package com.example.sare.settings.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.DeactiveUserMutation
import com.example.sare.LogoutUserMutation
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton

class SettingsViewModel(application: Application) : AndroidViewModel(application) {

    private val context = getApplication<Application>().applicationContext
    var apolloClient: com.apollographql.apollo.ApolloClient? = null

    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE)
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        apolloClient = com.example.sare.ApolloClient.setupApollo(token ?: "")
    }

    var deactivateAccount = MutableLiveData<NetworkResponse>()
    var logoutUser = MutableLiveData<NetworkResponse>()

    fun deactivateAccount(id: String) {
        apolloClient?.mutate(DeactiveUserMutation(id))
            ?.enqueue(object : ApolloCall.Callback<DeactiveUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    deactivateAccount.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<DeactiveUserMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.deactiveUser() != null) {
                            deactivateAccount.postValue(
                                NetworkResponse.SUCCESS.deactiveUser(
                                    response.data?.deactiveUser()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            deactivateAccount.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        deactivateAccount.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }


    fun logout(id: String) {
        apolloClient?.mutate(LogoutUserMutation(id))?.enqueue(object : ApolloCall.Callback<LogoutUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    logoutUser.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<LogoutUserMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.logout() != null) {
                            logoutUser.postValue(
                                NetworkResponse.SUCCESS.logoutUser(
                                    response.data?.logout()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            logoutUser.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        logoutUser.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }
}