package com.example.sare.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.profile.RecyclerTouchListener
import com.example.sare.settings.adapter.BlockedUserChatListAdapter
import com.example.sare.settings.adapter.BlockedUserListAdapter
import com.example.sare.settings.viewmodel.BlockUserListViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_blocked_user.view.*

@RequiresApi(Build.VERSION_CODES.M)
class BlockedUserFragment : Fragment() {
    var root: View? = null
    var blockUserListViewModel: BlockUserListViewModel? = null
    var token: String? = ""
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var blockUserList: ArrayList<BlockUserListQuery.BlockUserList> = ArrayList()
    private var chatBlockedUsersLists: MutableList<BlockChatUserListQuery.BlockChatUserList> = mutableListOf()
    lateinit var blockedUserListAdapter: BlockedUserListAdapter
    lateinit var chatBlockedUserListAdapter: BlockedUserChatListAdapter
    lateinit var progressDialog: CustomProgressDialogNew
    lateinit var recyclerTouchListener: RecyclerTouchListener
    var type = ""

    override fun onResume() {
        super.onResume()
        root?.recyclerViewBlockedUser?.addOnItemTouchListener(recyclerTouchListener)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_blocked_user, container, false)
        type = arguments?.getString("type", null).toString()
        Log.d("tag", "type:::$type")
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@BlockedUserFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        getSharedPreference()
        setListeners()
        blockUserListViewModel = ViewModelProvider(this)[BlockUserListViewModel::class.java]
        progressDialog = CustomProgressDialogNew(requireActivity())
        recyclerTouchListener = RecyclerTouchListener(requireActivity(), root?.recyclerViewBlockedUser)
        if (type == "1") {
            initRecyclerView()
            observerBlockedUserList()
            getBlockedUserList()
            root?.txtTitle?.text = "Blocked Users"
        } else {
            getChatBlockedUserList()
            observerChatBlockedUserList()
            root?.txtTitle?.text = "Chat Blocked Users"
        }
        return root
    }

    //setListeners is for any layout when in design is clicked
    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this@BlockedUserFragment).navigateUp()
        }
    }

    //getSharedPreference is get the data stored in preferences
    private fun getSharedPreference() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    //block user list api called
    private fun getBlockedUserList() {
        progressDialog.show()
        blockUserListViewModel?.getBlockUserList(
            currentPage * Utils.PAGE_SIZE, token.toString())
    }

    //block user list observer called
    private fun observerBlockedUserList() {
        blockUserListViewModel?.blockUserList?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.hide()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            blockUserListViewModel?.getBlockUserList(
                                currentPage * Utils.PAGE_SIZE, token.toString())
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    root?.txtNoData?.visibility = View.VISIBLE
                    root?.recyclerViewBlockedUser?.visibility = View.GONE

                }

                is NetworkResponse.SUCCESS.getBlockUserList -> {
                    progressDialog.hide()
                    requireActivity().runOnUiThread {
                        isLoading = false
                        blockUserList.addAll(it.response)

                        root?.recyclerViewBlockedUser?.adapter?.notifyDataSetChanged()
                        if (it.response.size < Utils.PAGE_SIZE) {
                            isLastPage = true
                        }
                        if (root?.recyclerViewBlockedUser?.adapter?.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.recyclerViewBlockedUser?.visibility = View.GONE
                        } else {
                            root?.txtNoData?.visibility = View.GONE
                            root?.recyclerViewBlockedUser?.visibility = View.VISIBLE
                        }

                    }
                }
                else -> customToast(getString(R.string.api_error_message), requireActivity(), 0)
            }
        })
    } //block chat user list api called

    private fun getChatBlockedUserList() {
        progressDialog.show()
        blockUserListViewModel?.getChatBlockUserList(token.toString())
    }

    //block chat user list observer called
    private fun observerChatBlockedUserList() {
        blockUserListViewModel?.chatBlockUserList?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.hide()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            blockUserListViewModel?.getChatBlockUserList(token.toString())
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    root?.txtNoData?.visibility = View.VISIBLE
                    root?.recyclerViewBlockedUser?.visibility = View.GONE

                }

                is NetworkResponse.SUCCESS.getChatBlockUserList -> {
                    progressDialog.hide()
                    requireActivity().runOnUiThread {
                        chatBlockedUsersLists = it.response.toMutableList()
                        initChatBlockRecyclerView(chatBlockedUsersLists)
                        if (root?.recyclerViewBlockedUser?.adapter?.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.recyclerViewBlockedUser?.visibility = View.GONE
                        } else {
                            root?.txtNoData?.visibility = View.GONE
                            root?.recyclerViewBlockedUser?.visibility = View.VISIBLE
                        }

                    }
                }
                else -> customToast(getString(R.string.api_error_message), requireActivity(), 0)
            }
        })
    }

    private fun initChatBlockRecyclerView(list: List<BlockChatUserListQuery.BlockChatUserList>) {
        chatBlockedUserListAdapter = BlockedUserChatListAdapter(
            list as MutableList<BlockChatUserListQuery.BlockChatUserList>, requireContext(), this@BlockedUserFragment)
        root?.recyclerViewBlockedUser?.itemAnimator = DefaultItemAnimator()
        root?.recyclerViewBlockedUser?.apply {
            layoutManager = LinearLayoutManager(
                root?.recyclerViewBlockedUser?.context, RecyclerView.VERTICAL, false)

            adapter = BlockedUserChatListAdapter(
                list, requireContext(), this@BlockedUserFragment)

            recyclerTouchListener.setClickable(object : RecyclerTouchListener.OnRowClickListener {
                override fun onRowClicked(position: Int) {
                }

                override fun onIndependentViewClicked(independentViewID: Int, position: Int) {
                }
            }).setSwipeOptionViews(R.id.relCard, R.id.relDetails).setSwipeable(
                R.id.relDetails, R.id.relCard) { viewID, position ->
                when (viewID) {
                    R.id.relCard -> {
//                        unBlockChat(list[position].sender()?._id().toString(), position)
                        val apolloClient = ApolloClient.setupApollo(token.toString())
                        apolloClient.mutate(
                            UnblockChatMutation(
                                list[position].sender()!!._id()))
                            .enqueue(object : ApolloCall.Callback<UnblockChatMutation.Data>() {
                                override fun onFailure(e: ApolloException) {
                                    customToast(e.message.toString(), requireActivity(), 0)
                                    MyApplication.mainActivity.logoutUser()
                                    activity?.openNewActivity(LoginActivity::class.java)
                                }

                                override fun onResponse(response: Response<UnblockChatMutation.Data>) {
                                    requireActivity().runOnUiThread {
                                        if (response.data?.acceptChat()?.status() == true) {
                                            chatBlockedUserListAdapter.deleteItem(
                                                chatBlockedUsersLists, position)
                                            recyclerViewBlockedUser.adapter?.notifyItemRemoved(
                                                position)
                                            recyclerViewBlockedUser.adapter?.notifyDataSetChanged()
                                            if (chatBlockedUsersLists.size == 0) {
                                                root?.txtNoData?.visibility = View.VISIBLE
                                            } else {
                                                root?.txtNoData?.visibility = View.GONE

                                            }
                                            customToast("Chat User Unblocked Successfully!", requireActivity(), 1)
                                        } else {
                                            val error = response.errors?.get(0)?.message
                                            val errorCode = response.errors?.get(0)?.customAttributes?.get(
                                                "status")
                                            when {
                                                errorCode?.toString()?.equals("401") == true -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    activity?.openNewActivity(LoginActivity::class.java)
                                                }
                                                errorCode?.toString()?.equals("403") == true -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    activity?.openNewActivity(LoginActivity::class.java)
                                                }
                                                else -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                    }
                }
            }
            root?.recyclerViewBlockedUser?.addOnItemTouchListener(recyclerTouchListener)
        }
    }

   private fun unBlockChat(id: String, position: Int) {
       blockUserListViewModel?.unBlockChat(id, token.toString())


        blockUserListViewModel?.unBlockChat?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        requireView(), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        blockUserListViewModel?.unBlockChat(id, token.toString())
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.acceptChat -> {
                    if (it.response.status()) {
                        chatBlockedUserListAdapter.deleteItem(chatBlockedUsersLists, position)
                        root?.recyclerViewBlockedUser?.adapter?.notifyItemRemoved(position)
                        root?.recyclerViewBlockedUser?.adapter?.notifyDataSetChanged()
                        if (chatBlockedUserListAdapter.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                        } else {
                            root?.txtNoData?.visibility = View.GONE

                        }

                        customToast("Chat User Unblocked Successfully!", requireActivity(), 1)
                    }

                }
            }
        })
    }

    //block user list recyclerview called
    private fun initRecyclerView() {
        blockedUserListAdapter = BlockedUserListAdapter(
            blockUserList, requireContext(), this@BlockedUserFragment)
        root?.recyclerViewBlockedUser?.itemAnimator = DefaultItemAnimator()
        root?.recyclerViewBlockedUser?.apply {
            layoutManager = LinearLayoutManager(
                root?.recyclerViewBlockedUser?.context, RecyclerView.VERTICAL, false)
            addOnScrollListener(object :
                com.example.sare.PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getBlockedUserList()
                }

                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }

            })
            adapter = BlockedUserListAdapter(
                blockUserList, context, this@BlockedUserFragment)

            recyclerTouchListener.setClickable(object : RecyclerTouchListener.OnRowClickListener {
                override fun onRowClicked(position: Int) {
                }

                override fun onIndependentViewClicked(independentViewID: Int, position: Int) {
                }
            }).setSwipeOptionViews(R.id.relCard, R.id.relDetails).setSwipeable(
                R.id.relDetails, R.id.relCard) { viewID, position ->
                when (viewID) {
                    R.id.relCard -> {
                        val apolloClient = ApolloClient.setupApollo(token.toString())
                        apolloClient.mutate(
                            UnBlockUserMutation(
                                blockUserList[position].user()!!._id()))
                            .enqueue(object : ApolloCall.Callback<UnBlockUserMutation.Data>() {
                                override fun onFailure(e: ApolloException) {
                                    customToast(e.message.toString(), requireActivity(), 0)
                                    MyApplication.mainActivity.logoutUser()
                                    activity?.openNewActivity(LoginActivity::class.java)
                                }

                                override fun onResponse(response: Response<UnBlockUserMutation.Data>) {
                                    requireActivity().runOnUiThread {
                                        if (response.data?.unblockUser()?.status() == true) {
                                            blockedUserListAdapter.deleteItem(
                                                blockUserList, position)
                                            recyclerViewBlockedUser.adapter?.notifyItemRemoved(
                                                position)
                                            recyclerViewBlockedUser.adapter?.notifyDataSetChanged()
                                            if (blockUserList.size == 0) {
                                                root?.txtNoData?.visibility = View.VISIBLE
                                            } else {
                                                root?.txtNoData?.visibility = View.GONE

                                            }
                                            customToast("User Unblocked Successfully!", requireActivity(), 1)
                                        } else {
                                            val error = response.errors?.get(0)?.message
                                            val errorCode = response.errors?.get(0)?.customAttributes?.get(
                                                "status")
                                            when {
                                                errorCode?.toString()?.equals("401") == true -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    activity?.openNewActivity(LoginActivity::class.java)
                                                }
                                                errorCode?.toString()?.equals("403") == true -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    activity?.openNewActivity(LoginActivity::class.java)
                                                }
                                                else -> {
                                                    customToast(error.toString(), requireActivity(), 0)
                                                }
                                            }
                                        }
                                    }
                                }
                            })
                    }
                }
            }
            root?.recyclerViewBlockedUser?.addOnItemTouchListener(recyclerTouchListener)
        }
    }
}