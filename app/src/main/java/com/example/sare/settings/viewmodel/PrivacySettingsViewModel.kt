package com.example.sare.settings.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.UpdatePrivacyStatusMutation
import com.example.sare.UserDetailsQuery
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton

class PrivacySettingsViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var userDetails = MutableLiveData<NetworkResponse>()
    var updatePrivacyStatus = MutableLiveData<NetworkResponse>()
    var apolloClient: com.apollographql.apollo.ApolloClient? = null

    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        apolloClient = com.example.sare.ApolloClient.setupApollo(token ?: "")

    }

    fun getUserDetails(id: String) {

        apolloClient?.query(UserDetailsQuery(id))
            ?.enqueue(
                object : ApolloCall.Callback<UserDetailsQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.d("tag", "Error::" + e.message)
                        userDetails.postValue(NetworkResponse.ERROR(e))
                    }

                    override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                        Log.d("tag", "Response User Data::$response")
                        try {
                            if (response.data()?.user() != null) {
                                userDetails.postValue(
                                    NetworkResponse.SUCCESS.getEditUserDetails(
                                        response.data()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                userDetails.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            userDetails.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }

                    }
                })
    }

    fun updateStatus(status: Boolean) {
        apolloClient?.mutate(UpdatePrivacyStatusMutation(status))
            ?.enqueue(object : ApolloCall.Callback<UpdatePrivacyStatusMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    updatePrivacyStatus.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UpdatePrivacyStatusMutation.Data>) {

                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data() != null) {
                            updatePrivacyStatus.postValue(
                                NetworkResponse.SUCCESS.updatePrivacyStatus(
                                    response.data()!!
                                )
                            )
                        } else {
                            val errorCode =
                                response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                            updatePrivacyStatus.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        updatePrivacyStatus.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }

                }
            })
    }
}
