package com.example.sare.settings.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.BlockChatUserListQuery
import com.example.sare.BlockUserListQuery
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import kotlinx.android.synthetic.main.item_blocked_user.view.*

class BlockedUserChatListAdapter(
    var list: MutableList<BlockChatUserListQuery.BlockChatUserList>,
    private val context: Context,
    private val fragment: Fragment
) :
    RecyclerView.Adapter<BlockedUserChatListAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BlockedUserChatListAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_blocked_user, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, fragment)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    override fun getItemId(position: Int): Long {
        return if (list != null) list[position].sender()!!._id().toLong() else 0
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(
            item: BlockChatUserListQuery.BlockChatUserList,
            context: Context,
            fragment: Fragment
        ) {
            val name = itemView.txtName
            val id = itemView.txtId
            val likes = itemView.txtLikes
            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val txtVideoCounts = itemView.txtVideoCounts
            if (item.sender()?.likeCounts().toString() < 0.toString()) {
                likes.text = 0.toString()
            } else {
                likes.text = item.sender()?.likeCounts().toString()
            }
            if (item.sender()?.postsCounts().toString() < 0.toString()) {
                txtVideoCounts.text = 0.toString()
            } else {
                txtVideoCounts.text = item.sender()?.postsCounts().toString()
            }
            name.text = item.sender()?.name().toString()
            id.text = "@" + item.sender()?.username().toString()
            imgProfile?.loadUrl(Utils.PUBLIC_URL + item.sender()?.image()!!)
            imgProfileBorder?.setBorder(item.sender()?.colorCode() ?: Utils.COLOR_CODE)
        }

    }

    fun deleteItem(post: List<BlockChatUserListQuery.BlockChatUserList>, position: Int) {
        val postId = post[position].sender()?._id()
        if (list[position].sender()?._id() === postId) {
            this.list.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, itemCount)
        }
    }

}