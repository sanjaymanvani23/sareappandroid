package com.example.sare.settings.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class BlockUserListViewModel : ViewModel() {
    var blockUserList = MutableLiveData<NetworkResponse>()
    var chatBlockUserList = MutableLiveData<NetworkResponse>()
    var unBlockChat = MutableLiveData<NetworkResponse>()
    fun getBlockUserList(pageCount: Int, token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(BlockUserListQuery(pageCount, Utils.PAGE_SIZE))
            .enqueue(object : ApolloCall.Callback<BlockUserListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    blockUserList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<BlockUserListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.blockUserList() != null) {
                                blockUserList.postValue(
                                    NetworkResponse.SUCCESS.getBlockUserList(
                                        response.data()?.blockUserList()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                blockUserList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            blockUserList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getChatBlockUserList(token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(BlockChatUserListQuery.builder().build())
            .enqueue(object : ApolloCall.Callback<BlockChatUserListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    chatBlockUserList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<BlockChatUserListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.blockChatUserList() != null) {
                                chatBlockUserList.postValue(
                                    NetworkResponse.SUCCESS.getChatBlockUserList(
                                        response.data()?.blockChatUserList()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                chatBlockUserList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString
                                    ()))
                            }
                        } catch (e: Exception) {
                            chatBlockUserList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
    fun unBlockChat(senderId: String, token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient?.mutate(UnblockChatMutation(senderId))
            ?.enqueue(object : ApolloCall.Callback<UnblockChatMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    unBlockChat.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<UnblockChatMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.acceptChat() != null) {
                            unBlockChat.postValue(
                                NetworkResponse.SUCCESS.acceptChat(
                                    response.data?.acceptChat()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            unBlockChat.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        unBlockChat.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }
}