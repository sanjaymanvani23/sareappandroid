package com.example.sare.settings

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.settings.viewmodel.SettingsViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_settings.view.*
import kotlinx.android.synthetic.main.popup_deactive_acc.*
import kotlinx.android.synthetic.main.popup_logout.*
import org.json.JSONObject


@RequiresApi(Build.VERSION_CODES.M) class SettingsFragment : Fragment() {
    var root: View? = null
    var userId = ""
    var settingsViewModel: SettingsViewModel? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_settings, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finish()
            }
        }
        settingsViewModel = ViewModelProvider(this)[SettingsViewModel::class.java]
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback)
        setTheme()
        setData()
        setListeners()
        return root
    }

    private fun setData() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            userId = mainObject.getString("_id")
        }

        val manager: PackageManager = requireContext().packageManager
        val info: PackageInfo = manager.getPackageInfo(
            requireContext().packageName, 0)
        val version: String = info.versionName.toString()

        Log.d("tag", "version::$version")
        root?.txtAppVersion?.text = "App version $version"
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.bodyBackground)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.bodyBackground)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            activity?.finish()
        }
        root?.llEditProfile?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_editProfileFragment)
            }
        }
        root?.llNotification?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_notificationSettingsFragment)
            }
        }
        root?.llPrivacy?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_privacySettingsFragment)
            }
        }
        root?.llShareProfile?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_shareProfileFragment)
            }
        }
        root?.llBugReport?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_reportApplicationBugFragment)
            }
        }
        root?.llBlockedUser?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_blockedUserFragment)
            }
        }

        root?.llPrivacyMain?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.settingsFragment) {
                findNavController().navigate(R.id.action_settingsFragment_to_privacyFragment)
            }
        }
        root?.llLogout?.setOnClickListener {
            logoutDialog()
        }
        root?.llAccClosing?.setOnClickListener {
            closeAccountDialog()
        }
    }

     fun logoutDialog() {
        val dialog = Dialog(requireActivity(), R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_logout)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnLogout.setOnClickListener {
            //logoutUser(userId)
            MyApplication.mainActivity.logoutUser()
            dialog.dismiss()
        }

        dialog.btnExit.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun closeAccountDialog() {
        val dialog = Dialog(requireActivity(), R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_deactive_acc)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnYes.setOnClickListener {
            deactivateAccount(userId.toString())
            dialog.dismiss()
        }

        dialog.btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun deactivateAccount(id: String) {
        settingsViewModel?.deactivateAccount(id)

        settingsViewModel?.deactivateAccount?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        requireView(), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        settingsViewModel?.deactivateAccount(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.deactiveUser -> {
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }
            }
        })

    }

    private fun logoutUser(id: String) {
        settingsViewModel?.logout(id)

        settingsViewModel?.logoutUser?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        requireView(), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        settingsViewModel?.logout(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.logoutUser -> {
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }
            }
        })

    }
}