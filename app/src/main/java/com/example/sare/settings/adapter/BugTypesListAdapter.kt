package com.example.sare.settings.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.BugTypesQuery
import com.example.sare.R
import com.example.sare.ReportListQuery
import kotlinx.android.synthetic.main.item_report_list.view.*

class BugTypesListAdapter(
    var list: List<BugTypesQuery.BugType>,
    private val context: Context
) :
    RecyclerView.Adapter<BugTypesListAdapter.ViewHolder>() {

    var onItemClick: ((BugTypesQuery.BugType) -> Unit)? = null

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_report_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, onItemClick!!)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: BugTypesQuery.BugType,
            context: Context,
            onItemClick : ((BugTypesQuery.BugType) -> Unit)
        ) {

            val txtReport = itemView.txtReport
            txtReport.text = list.name()
            itemView.setOnClickListener {
                onItemClick.invoke(list)
            }



        }
    }

}
