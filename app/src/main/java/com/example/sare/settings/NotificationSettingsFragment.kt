package com.example.sare.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.NotificationDisplayQuery
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.settings.viewmodel.NotificationSettingsViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_notification_settings.view.*
import org.json.JSONObject
import java.util.*


class NotificationSettingsFragment : Fragment() {
    var root: View? = null
    var token: String? = null
    var userId: String? = null

    var comment: String? = null
    var likes: String? = null
    var follow: String? = null
    var chatRequest: String? = null
    var notificationSettingsViewModel: NotificationSettingsViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_notification_settings, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@NotificationSettingsFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        notificationSettingsViewModel = ViewModelProvider(this)[NotificationSettingsViewModel::class.java]
        setTheme()
        getSharedPreferences()
        displayData()
        getData()
        setListeners()
        root?.chkOff?.setCompoundDrawables(null,null,ContextCompat.getDrawable(requireActivity(),R.drawable.cb_checked),null)
        return root
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(),
            StringSingleton.bodyBackground
        )
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(),
                StringSingleton.bodyBackground
            )
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun getData() {
        root?.chkOff?.setOnClickListener {
            root?.chkOff!!.isChecked = true
        }

        root?.chkFollowBackFollowing?.setOnClickListener {
            root?.chkFollowBackFollowing!!.isChecked = true
        }

        root?.chkFollowers?.setOnClickListener {
            root?.chkFollowers!!.isChecked = true
        }

        root?.chkEveryone?.setOnClickListener {
            root?.chkEveryone!!.isChecked = true
        }

        root?.chkOffComments?.setOnClickListener {
            root?.chkOffComments!!.isChecked = true
        }

        root?.chkFollowBackFollowingComments?.setOnClickListener {
            root?.chkFollowBackFollowingComments!!.isChecked = true
        }

        root?.chkFollowersComments?.setOnClickListener {
            root?.chkFollowersComments!!.isChecked = true
        }

        root?.chkEveryoneComments?.setOnClickListener {
            root?.chkEveryoneComments!!.isChecked = true
        }

        root?.chkOffFollow?.setOnClickListener {
            root?.chkOffFollow!!.isChecked = true
        }

        root?.chkFollowBackFollowingFollow?.setOnClickListener {
            root?.chkFollowBackFollowingFollow!!.isChecked = true
        }

        root?.chkEveryoneFollow?.setOnClickListener {
            root?.chkEveryoneFollow!!.isChecked = true
        }

        root?.chkOffChatRequest?.setOnClickListener {
            root?.chkOffChatRequest!!.isChecked = true
        }

        root?.chkFollowBackFollowingChatRequest?.setOnClickListener {
            root?.chkFollowBackFollowingChatRequest!!.isChecked = true
        }

        root?.chkEveryoneChatRequest?.setOnClickListener {
            root?.chkEveryoneChatRequest!!.isChecked = true
        }

        root?.chkOff?.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                root?.chkFollowers!!.isChecked = false
                root?.chkFollowBackFollowing!!.isChecked = false
                root?.chkEveryone!!.isChecked = false
            }

            likes = root?.chkOff!!.text.toString().toUpperCase()
        }

        root?.chkFollowers?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOff!!.isChecked = false
                root?.chkFollowBackFollowing!!.isChecked = false
                root?.chkEveryone!!.isChecked = false
            }
            likes = root?.chkFollowers!!.text.toString().toUpperCase().replace(" ", "")
        }

        root?.chkFollowBackFollowing?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOff!!.isChecked = false
                root?.chkFollowers!!.isChecked = false
                root?.chkEveryone!!.isChecked = false
            }
            likes = root?.chkFollowBackFollowing!!.text.toString().toUpperCase().replace(" ", "")
        }

        root?.chkEveryone?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOff!!.isChecked = false
                root?.chkFollowers!!.isChecked = false
                root?.chkFollowBackFollowing!!.isChecked = false
            }
            likes = root?.chkEveryone!!.text.toString().toUpperCase().replace(" ", "")
        }


        root?.chkOffComments?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkFollowersComments!!.isChecked = false
                root?.chkFollowBackFollowingComments!!.isChecked = false
                root?.chkEveryoneComments!!.isChecked = false
            }
            comment = root?.chkOffComments!!.text.toString().toUpperCase()
        }

        root?.chkFollowersComments?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffComments!!.isChecked = false
                root?.chkFollowBackFollowingComments!!.isChecked = false
                root?.chkEveryoneComments!!.isChecked = false
            }
            comment = root?.chkFollowersComments!!.text.toString().toUpperCase().replace(" ", "")
        }

        root?.chkFollowBackFollowingComments?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffComments!!.isChecked = false
                root?.chkFollowersComments!!.isChecked = false
                root?.chkEveryoneComments!!.isChecked = false
            }
            comment = root?.chkFollowBackFollowingComments!!.text.toString().toUpperCase().replace(" ", "")
        }

        root?.chkEveryoneComments?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffComments!!.isChecked = false
                root?.chkFollowersComments!!.isChecked = false
                root?.chkFollowBackFollowingComments!!.isChecked = false
            }
            comment = root?.chkEveryoneComments!!.text.toString().toUpperCase().replace(" ", "")
        }


        root?.chkOffFollow?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkFollowBackFollowingFollow!!.isChecked = false
                root?.chkEveryoneFollow!!.isChecked = false
            }
            follow = root?.chkOffFollow!!.text.toString().toUpperCase()
        }

        root?.chkFollowBackFollowingFollow?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffFollow!!.isChecked = false
                root?.chkEveryoneFollow!!.isChecked = false
            }
            follow = root?.chkFollowBackFollowingFollow!!.text.toString().toUpperCase().replace(" ", "")
        }

        root?.chkEveryoneFollow?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffFollow!!.isChecked = false
                root?.chkFollowBackFollowingFollow!!.isChecked = false
            }
            follow = root?.chkEveryoneFollow!!.text.toString().toUpperCase(Locale.ROOT).replace(" ", "")
        }

        root?.chkOffChatRequest?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkFollowBackFollowingChatRequest!!.isChecked = false
                root?.chkEveryoneChatRequest!!.isChecked = false
            }
            chatRequest = root?.chkOffChatRequest!!.text.toString().toUpperCase(Locale.ROOT)
        }

        root?.chkFollowBackFollowingChatRequest?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffChatRequest!!.isChecked = false
                root?.chkEveryoneChatRequest!!.isChecked = false
            }
            chatRequest = root?.chkFollowBackFollowingChatRequest!!.text.toString().toUpperCase()
                .replace(" ", "")
        }

        root?.chkEveryoneChatRequest?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                root?.chkOffChatRequest!!.isChecked = false
                root?.chkFollowBackFollowingChatRequest!!.isChecked = false
            }
            chatRequest = root?.chkEveryoneChatRequest!!.text.toString().toUpperCase().replace(" ", "")
        }
    }


    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.txtSave?.setOnClickListener {
            updateNotification(
                comment.toString(),
                follow.toString(),
                likes.toString(),
                chatRequest.toString()
            )
        }
    }


    private fun updateNotification(
        comment: String,
        follow: String,
        likes: String,
        chatReq: String
    ) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        notificationSettingsViewModel?.saveNotificationSettings(comment, follow, likes, chatReq)

        notificationSettingsViewModel?.saveNotification?.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                requireActivity().runOnUiThread {
                    progressDialog.hide()
                }
                when (it) {
                    is NetworkResponse.ERROR -> {

                        it.error.printStackTrace()
                        val snackbar: Snackbar = Snackbar
                            .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                            .setAction("RETRY") {
                                notificationSettingsViewModel?.saveNotificationSettings(
                                    comment,
                                    follow,
                                    likes,
                                    chatReq
                                )
                            }
                        snackbar.show()
                    }
                    is NetworkResponse.ERROR_AUTHENTICATION -> {
                        customToast(it.errorMsg, requireActivity(),0)
                        MyApplication.mainActivity.logoutUser()
                        activity?.openNewActivity(LoginActivity::class.java)
                    }

                    is NetworkResponse.ERROR_RESPONSE -> {
                        customToast(it.errorMsg, requireActivity(),0)
                    }
                    is NetworkResponse.SUCCESS.saveNotificationSettings -> {

                        if (it.response.status() === true) {
                            requireActivity().runOnUiThread {
                                customToast("Changes saved successfully.!!",requireActivity(),1)
                                NavHostFragment.findNavController(this).navigateUp()
                            }
                        }

                    }

                }

            })
    }


    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        token = preferences.getString("TOKEN", null)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        userId = mainObject.getString("_id")
    }

    private fun displayData() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        if (activity != null && !activity?.isFinishing!!) {
            progressDialog.show()
        }
        notificationSettingsViewModel?.displayData(requireContext(), requireActivity())

        notificationSettingsViewModel?.displayNotification?.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                if (activity != null && !activity?.isFinishing!!) {
                    progressDialog.hide()
                }
                when (it) {
                    is NetworkResponse.ERROR -> {
                        if (activity != null && !activity?.isFinishing!!) {
                            progressDialog.hide()
                        }
                        it.error.printStackTrace()
                        val snackbar: Snackbar = Snackbar
                            .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                            .setAction("RETRY") {
                                notificationSettingsViewModel?.displayData(
                                    requireContext(),
                                    requireActivity()
                                )
                            }
                        snackbar.show()
                    }
                    is NetworkResponse.ERROR_AUTHENTICATION -> {
                        if (activity != null && !activity?.isFinishing!!) {
                            progressDialog.hide()
                        }
                        customToast(it.errorMsg, requireActivity(),0)
                        MyApplication.mainActivity.logoutUser()
                        activity?.openNewActivity(LoginActivity::class.java)
                    }

                    is NetworkResponse.ERROR_RESPONSE -> {
                        if (activity != null && !activity?.isFinishing!!) {
                            progressDialog.hide()
                        }
                        customToast(it.errorMsg, requireActivity(),0)
                    }
                    is NetworkResponse.SUCCESS.displayNotificationSettings -> {
                        if (activity != null && !activity?.isFinishing!!) {
                            progressDialog.hide()
                        }
                        setData(it.response.userNotificationPermission()!!)

                    }

                }

            })
    }

    private fun setData(data: NotificationDisplayQuery.UserNotificationPermission) {
        if (data?.video().toString()
                .toUpperCase() == root?.chkOff?.text.toString().toUpperCase()
        ) {
            root?.chkOff!!.isChecked = true
        } else {
            root?.chkOff?.isChecked == false
        }

        if (data?.video().toString()
                .toUpperCase() == root?.chkFollowers?.text.toString().toUpperCase().replace(" ", "")
        ) {
            root?.chkFollowers!!.isChecked = true
        } else {
            root?.chkFollowers?.isChecked == false
        }

        if (data?.video().toString()
                .toUpperCase() == root?.chkFollowBackFollowing?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkFollowBackFollowing!!.isChecked = true
        } else {
            root?.chkFollowBackFollowing?.isChecked == false
        }

        if (data?.video().toString()
                .toUpperCase() == root?.chkEveryone?.text.toString().toUpperCase().replace(" ", "")
        ) {
            root?.chkEveryone!!.isChecked = true
        } else {
            root?.chkEveryone?.isChecked = false
        }


        if (data?.comment().toString()
                .toUpperCase() == root?.chkOffComments?.text.toString().toUpperCase()
        ) {
            root?.chkOffComments!!.isChecked = true
        } else {
            root?.chkOffComments?.isChecked == false
        }

        if (data?.comment().toString()
                .toUpperCase() == root?.chkFollowersComments?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkFollowersComments!!.isChecked = true
        } else {
            root?.chkFollowersComments?.isChecked == false
        }

        if (data?.comment().toString()
                .toUpperCase() == root?.chkFollowBackFollowingComments?.text.toString()
                .toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkFollowBackFollowingComments!!.isChecked = true
        } else {
            root?.chkFollowBackFollowingComments?.isChecked == false
        }

        if (data?.comment().toString()
                .toUpperCase() == root?.chkEveryoneComments?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkEveryoneComments!!.isChecked = true
        } else {
            root?.chkEveryoneComments?.isChecked == false
        }


        if (data?.follow().toString()
                .toUpperCase() == root?.chkOffFollow?.text.toString().toUpperCase()
        ) {
            root?.chkOffFollow!!.isChecked = true
        } else {
            root?.chkOffFollow?.isChecked == false
        }

        if (data?.follow().toString()
                .toUpperCase() == root?.chkFollowBackFollowingFollow?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkFollowBackFollowingFollow!!.isChecked = true
        } else {
            root?.chkFollowBackFollowingFollow?.isChecked == false
        }

        if (data?.follow().toString()
                .toUpperCase() == root?.chkEveryoneFollow?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkEveryoneFollow!!.isChecked = true
        } else {
            root?.chkEveryoneFollow?.isChecked == false
        }

        if (data
                ?.chatRequest()!! == root?.chkOffChatRequest?.text.toString().toUpperCase()
        ) {
            root?.chkOffChatRequest!!.isChecked = true
        } else {
            root?.chkOffChatRequest?.isChecked == false
        }

        if (data
                ?.chatRequest()!! == root?.chkFollowBackFollowingChatRequest?.text.toString()
                .toUpperCase().replace(" ", "")
        ) {
            root?.chkFollowBackFollowingChatRequest!!.isChecked = true
        } else {
            root?.chkFollowBackFollowingChatRequest?.isChecked == false
        }

        if (data
                ?.chatRequest()!! == root?.chkEveryoneChatRequest?.text.toString().toUpperCase()
                .replace(" ", "")
        ) {
            root?.chkEveryoneChatRequest!!.isChecked = true
        } else {
            root?.chkEveryoneChatRequest?.isChecked == false
        }

    }
}