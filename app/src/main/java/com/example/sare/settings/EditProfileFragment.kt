package com.example.sare.settings

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.*
import com.example.sare.api.ApiService
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.hideKeyboard
import com.example.sare.data.ResponseData
import com.example.sare.extensions.*
import com.example.sare.settings.viewmodel.EditProfileViewModel
import com.example.sare.signUpDetails.adapter.ColorSelectionAdapter
import com.example.sare.signUpDetails.viewmodel.ColorListViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.layout_edit_profile.view.*
import kotlinx.android.synthetic.main.layout_select_color.view.*
import kotlinx.android.synthetic.main.layout_select_image_options.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.net.URL
import java.sql.Timestamp
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@RequiresApi(Build.VERSION_CODES.M) class EditProfileFragment : Fragment() {
    var root: View? = null
    var timestamp = Timestamp(System.currentTimeMillis())

    private val CAMERA_REQUEST = 1888
    private val MY_CAMERA_PERMISSION_CODE = 100
    private val IMAGE_PICK_CODE = 1000
    private val PERMISSION_CODE = 1001
    private val UPLOAD_FILE = 1002

    companion object {
        var uri: Uri? = null
        var imageFile: File? = null
        var photoURI: Uri? = null
    }

    var token: String? = null
    var userId: String? = null
    var colorCode: String? = null
    var imageUrl: String? = null
    var bio: String? = null
    var location: String? = null
    var gender: String? = null
    var email: String? = null
    var address: String? = null
    var dob: String? = null
    var city: String? = null
    var state: String? = null
    var mobile: String? = null
    var name: String? = null
    var username: String? = null
    var verify_user: Boolean = false
    var selectedColorCode: String? = null
    var colorListViewModel: ColorListViewModel? = null
    var editProfileViewModel: EditProfileViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_edit_profile, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@EditProfileFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        setTheme()
        uri = null
        getSharedPreferences()
        colorListViewModel = ViewModelProvider(this)[ColorListViewModel::class.java]
        editProfileViewModel = ViewModelProvider(this)[EditProfileViewModel::class.java]
        println(timestamp)
        getColorList()
        setBottomSheet()
        setListeners()
        userDetailsObserver(userId.toString())
        return root
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        address = preferences.getString("address", null)
        setData()
    }

    private fun setData() {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        if (user != null) {
            userId = mainObject.getString("_id")
            imageUrl = mainObject.getString("image")
            if (mainObject.has("colorCode")) {
                colorCode = mainObject.getString("colorCode")
            }
            mobile = if (mainObject.has("mobile")) {
                mainObject.getString("mobile")
            } else {
                null
            }
            email = if (mainObject.has("email")) {
                mainObject.getString("email")
            } else {
                null
            }
            verify_user = if (mainObject.has("verify_user")) {
                mainObject.getBoolean("verify_user")
            } else {
                false
            }
            bio = if (mainObject.has("bio")) {
                mainObject.getString("bio")
            } else {
                null
            }
            location = if (mainObject.has("location")) {
                mainObject.getString("location")
            } else {
                ""
            }
            state = if (mainObject.has("state")) {
                mainObject.getString("state")
            } else {
                ""
            }
            city = if (mainObject.has("city")) {
                mainObject.getString("city")
            } else {
                ""
            }
            gender = if (mainObject.has("gender")) {
                mainObject.getString("gender")
            } else {
                ""
            }
            dob = if (mainObject.has("dob")) {
                mainObject.getString("dob")
            } else {
                ""
            }
            name = mainObject.getString("name")
            username = mainObject.getString("username")
            imageUrl = Utils.PUBLIC_URL + (imageUrl.toString())

            root?.txtName?.setText(name.toString().trim())
            root?.txtUserName?.setText(username.toString())
            if (mobile != null && mobile!!.isNotEmpty()) {
                root?.view2?.visibility = View.VISIBLE
                root?.llMobile?.visibility = View.VISIBLE
                root?.mobile?.visibility = View.VISIBLE
                root?.txtNumber?.visibility = View.VISIBLE
                root?.txtNumber?.setText(mobile.toString())
                root?.imgIsVerified?.visibility = View.GONE
                root?.txtEmail?.isFocusable = true
                root?.txtEmail?.alpha = 1f
            } else {
                root?.txtEmail?.isFocusable = false
                root?.txtEmail?.alpha = 0.5f

                root?.view2?.visibility = View.GONE
                root?.mobile?.visibility = View.GONE
                root?.txtNumber?.visibility = View.GONE
                root?.txtNumber?.visibility = View.GONE
            }
            if (email != null && email!!.isNotEmpty()) {
                root?.view4?.visibility = View.VISIBLE
                root?.llEmail?.visibility = View.VISIBLE
                root?.email?.visibility = View.VISIBLE
                root?.txtEmail?.visibility = View.VISIBLE
                root?.txtEmail?.setText(email)
            } else {
                root?.view4?.visibility = View.GONE
                root?.email?.visibility = View.GONE
                root?.txtEmail?.visibility = View.GONE
                root?.llEmail?.visibility = View.GONE
            }
            root?.txtDOB?.setText(getParsedDate(dob.toString()))
            root?.txtGender?.setText(gender)
            if (city.isNullOrEmpty() || state.isNullOrEmpty() || location.isNullOrEmpty()) {
                root?.txtAddress?.setText(address.toString())
            } else {
                root?.txtAddress?.setText("$city, $state, $location")
            }
            root?.txtBio?.setText(bio)
            root?.imgProfilePic?.loadUrl(imageUrl!!)
            root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)
            root?.imgFrameColor?.setColorFilter(
                try {
                    Color.parseColor(colorCode)
                } catch (e: Exception) {
                    Color.parseColor(Utils.COLOR_CODE)
                })
        }
    }

    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.txtNumber?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                root?.imgDoVerifyMobile?.visibility = View.VISIBLE
            } else {
                root?.imgDoVerifyMobile?.visibility = View.GONE
            }
        }
        root?.imgIsVerified?.setOnClickListener {
            root?.imgDoVerify?.visibility = View.VISIBLE
        }
        root?.imgIsVerifiedMobile?.setOnClickListener {
            root?.imgDoVerifyMobile?.visibility = View.VISIBLE
        }

        root?.imgDoVerifyMobile?.setOnClickListener {}
        root?.llMobileNumber?.setOnClickListener {}
        root?.name?.setOnTouchListener { v, event ->

            root?.txtName?.onTouchEvent(event)
            root?.txtName?.setSelection(root?.txtName?.text!!.length)
            return@setOnTouchListener true
        }

        root?.txtBio?.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                root?.txtCounter?.visibility = View.VISIBLE
                root?.imgClearText?.visibility = View.VISIBLE
                var length: Int = root?.txtBio!!.text.length
                val convert = length.toString()
                root?.txtCounter?.text = "$convert/100"
                root?.imgClearText?.setOnClickListener {
                    root?.txtBio!!.text = null
                    length = 0
                }
            }
        }
        root?.imgClearText?.setOnClickListener {
            root?.txtBio!!.text = null
        }
        root?.bio?.setOnTouchListener { v, event ->
            root?.txtBio?.onTouchEvent(event)
            root?.txtBio?.setSelection(root?.txtBio?.text!!.length)
            return@setOnTouchListener true
        }

        root?.txtBio?.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {
                if (s!!.isNotEmpty() && s.length >= 100) {
                    customToast("Bio can't be more than 100 letters", requireActivity(), 0)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    root?.txtCounter?.visibility = View.VISIBLE
                    root?.imgClearText?.visibility = View.VISIBLE
                }
                root?.txtCounter?.text = "${s.length}/100"
                if (s.isNotEmpty() && s.length >= 100) {
                    customToast("Bio can't be more than 100 letters", requireActivity(), 0)
                }
            }
        })
        root?.txtSave?.setOnClickListener {
            hideKeyboard(requireActivity())
            val fullName = root?.txtName?.text.toString()

            if (root?.txtName?.text.toString().isEmpty()) {
                requireActivity().runOnUiThread {
                    customToast("Name can't be empty", requireActivity(), 0)
                }
            } else if (root?.txtName?.text?.length!! < 6) {
                requireActivity().runOnUiThread {
                    customToast("Please enter proper name", requireActivity(), 0)
                }
            } else if (!fullName.matches(
                    "^[A-Za-z0-9_. ]*$".toRegex()) || root?.txtName?.text?.toString()!!.startsWith(" ")
            ) {
                requireActivity().runOnUiThread {
                    customToast("Please enter proper name", requireActivity(), 0)
                }
            } else if (root?.txtName!!.text.length >= 30) {
                customToast("Name can't be more than 30 letters", requireActivity(), 0)
            } else if (root?.txtUserName?.text.toString().isEmpty()) {
                requireActivity().runOnUiThread {
                    customToast("User name can't be empty", requireActivity(), 0)
                }
            } else if (root?.txtUserName?.text?.toString()!!
                    .startsWith(" ") || root?.txtUserName?.text!!.length < 6 || root?.txtUserName?.text!!.length > 20 || !root?.txtUserName?.text.toString()
                    .matches("^[A-Za-z0-9_.]*$".toRegex())
            ) {
                requireActivity().runOnUiThread {
                    customToast(
                        "Please enter proper username having 6 letters, no blank space, capital letters, small letters or underscore",
                        requireActivity(),
                        0)
                }
            } else {
                if (uri != null) {
                    uploadFile(uri!!)
                } else {
                    if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                            Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                    ) {
                        val permissions = arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA)

                        requestPermissions(permissions, UPLOAD_FILE)
                    } else {
                        //permission already granted
                        try {
                            val url = URL(imageUrl)
                            val alreadySelectedBitmap = BitmapFactory.decodeStream(
                                url.openConnection().getInputStream())
                            val bytes = ByteArrayOutputStream()
                            alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                            val path: String = MediaStore.Images.Media.insertImage(
                                requireActivity().contentResolver,
                                alreadySelectedBitmap,
                                "IMG_" + Calendar.getInstance().time,
                                null)
                            uri = Uri.parse(path)

                        } catch (e: Exception) {
                            e.printStackTrace()
                            val alreadySelectedBitmap = BitmapFactory.decodeResource(
                                resources, R.drawable.ic_login_profile)
                            val bytes = ByteArrayOutputStream()
                            alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                            val path: String = MediaStore.Images.Media.insertImage(
                                requireActivity().contentResolver,
                                alreadySelectedBitmap,
                                "IMG_" + Calendar.getInstance().time,
                                null)
                            uri = Uri.parse(path)
                        }
                        uploadFile(uri!!)
                    }
                }
            }
        }

    }

    private fun getColorList() {
        val progressDialog = CustomProgressDialogNew(requireActivity())
        requireActivity().runOnUiThread {
            progressDialog.hide()
        }
        colorListViewModel?.getColorList()

        colorListViewModel?.colorArrayList?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            colorListViewModel?.getColorList()
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.colorList -> {
                    setBottomSheetForColor(it.response)
                }
            }
        })
    }

    private fun getUserDetails(id: String) {
        editProfileViewModel?.getEditUserDetails(id)
    }


    fun userDetailsObserver(id: String) {
        editProfileViewModel?.userDetails?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            editProfileViewModel?.getEditUserDetails(id)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.getEditUserDetails -> {
                    val preferences: SharedPreferences = requireActivity().getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                    val gson = Gson()
                    // Get java object list json format string.
                    val userInfoListJsonString = gson.toJson(it.response.user())
                    preferences.edit().putString("TOKEN", token).apply()

                    preferences.edit().putString("USER", userInfoListJsonString).apply()

                    NavHostFragment.findNavController(this).navigateUp()
                }
            }
        })

    }

    private fun getParsedDate(date: String): String {

        val df: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        var parsedDate: String? = null
        parsedDate = try {
            Log.d("tag", "date:::1:$date")
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault())
            outputFormatter1.format(date1)

        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        Log.d("tag", "date::$parsedDate")
        return parsedDate.toString()
    }

    private fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = requireActivity().contentResolver.query(uri!!, projection, null, null, null) ?: return null
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }

    private fun uploadFile(fileUri: Uri) {
        val progressDialog = CustomProgressDialogNew(requireActivity())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)
        Log.d("tag", "fileuri:::$fileUri")
        val file = File(getPath(fileUri))

        val requestFile: RequestBody = file.asRequestBody(
            requireActivity().contentResolver.getType(fileUri)!!.toMediaTypeOrNull())

        val body = MultipartBody.Part.createFormData("profile", file.name, requestFile)

        // add another part within the multipart request
        var descriptionString = ""
        var locationString = ""
        if (selectedColorCode != null) {
            descriptionString = selectedColorCode.toString()
        } else {
        }
        val bioString = root?.txtBio?.text.toString()

        locationString = address.toString()

        Log.d("tag", "biostring::::$bioString")
        val nameString = root?.txtName?.text.toString()
        val emailString = root?.txtEmail?.text.toString()
        val usernameString = root?.txtUserName?.text.toString()

        val description: RequestBody = descriptionString.toRequestBody(MultipartBody.FORM)
        val bio: RequestBody = bioString.toRequestBody(MultipartBody.FORM)

        val email: RequestBody = emailString.toRequestBody(MultipartBody.FORM)
        val username: RequestBody = usernameString.toRequestBody(MultipartBody.FORM)
        val name: RequestBody = nameString.toRequestBody(MultipartBody.FORM)
        val call: Call<ResponseData> = service.updateUserData(
            description, bio, name, username, email, body, "Bereader " + token.toString())
        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(call: Call<ResponseData?>?, response: Response<ResponseData?>?) {
                requireActivity().runOnUiThread {
                    progressDialog.hide()
                }
                val status = response?.body()?.status
                if (status == 200) {
                    customToast(getString(R.string.upload_success_message), requireActivity(), 1)

                    getUserDetails(userId.toString())

                } else {
                    requireActivity().runOnUiThread {

                        customToast(getString(R.string.api_error_message), requireActivity(), 0)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseData?>, t: Throwable) {
                Log.e("Upload error:", t.message!!)
                requireActivity().runOnUiThread {
                    customToast(getString(R.string.api_error_message), requireActivity(), 0)
                }
            }
        })
    }

    private fun setBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_new

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        root?.imgProfilePic?.setOnClickListener {
            root?.overlay_view_profile?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.overlay_view_profile?.setOnClickListener {

            root?.overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }


        root?.txtTakePhoto?.setOnClickListener {
            root?.overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA)
                //show popup to request runtime permission
                requestPermissions(permissions, MY_CAMERA_PERMISSION_CODE)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val filesDir: File = requireContext().filesDir

                imageFile = File(filesDir, timestamp.time.toString() + ".jpg")
                imageFile?.also {
                    photoURI = FileProvider.getUriForFile(
                        requireContext(), requireContext().applicationContext.packageName + ".provider", it)
                    startActivityForResult(cameraIntent, CAMERA_REQUEST)
                }
            }
        }

        root?.txtChooseFromGallery?.setOnClickListener {
            root?.overlay_view_profile?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission denied
                    val permissions = arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA

                    )
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    //permission already granted
                    pickImageFromGallery()
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery()
            }
        }


        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_profile?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_profile?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode === MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val filesDir: File = requireContext().filesDir

                imageFile = File(filesDir, timestamp.time.toString() + ".jpg")
                imageFile?.also {
                    photoURI = FileProvider.getUriForFile(
                        requireContext(), requireContext().applicationContext.packageName + ".provider", it)
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(cameraIntent, CAMERA_REQUEST)
                }
            } else {
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        } else if (requestCode === PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery()
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        } else if (requestCode === UPLOAD_FILE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                try {
                    val url = URL(imageUrl)
                    val alreadySelectedBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                    val bytes = ByteArrayOutputStream()
                    alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                    val path: String = MediaStore.Images.Media.insertImage(
                        requireActivity().contentResolver,
                        alreadySelectedBitmap,
                        "IMG_" + Calendar.getInstance().time,
                        null)
                    uri = Uri.parse(path)
                } catch (e: Exception) {
                    Log.d("tag", "error:::${e.message}")

                    e.printStackTrace()
                    val alreadySelectedBitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_login_profile)
                    val bytes = ByteArrayOutputStream()
                    alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                    val path: String = MediaStore.Images.Media.insertImage(
                        requireActivity().contentResolver,
                        alreadySelectedBitmap,
                        "IMG_" + Calendar.getInstance().time,
                        null)
                    uri = Uri.parse(path)

                }
                uploadFile(uri!!)
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data != null && data.data != null && requestCode == IMAGE_PICK_CODE) {
            try {
                val selectedImage = data.data

                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor = selectedImage?.let {
                    requireContext().contentResolver.query(
                        it, filePathColumn, null, null, null)
                }!!
                cursor.moveToFirst()
                val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                val picturePath: String = cursor.getString(columnIndex)
                cursor.close()
                data.putExtra("picturePath", picturePath)
                requireActivity().setResult(Activity.RESULT_OK, data)

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error::${e.message}")
                val returnFromGalleryIntent = Intent()
                requireActivity().setResult(Activity.RESULT_CANCELED, returnFromGalleryIntent)
                requireActivity().finish()
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode === CAMERA_REQUEST) {

            val picturePath: Bitmap = data!!.extras!!["data"] as Bitmap

            println("imageFile:::$imageFile")

            val os: OutputStream
            try {
                os = FileOutputStream(imageFile)
                picturePath.compress(Bitmap.CompressFormat.JPEG, 100, os)
                os.flush()
                os.close()
            } catch (e: java.lang.Exception) {
                Log.e(javaClass.simpleName, "Error writing bitmap", e)
            }
            Log.d("tag", "picturePath Camera::$picturePath")

            imageCrop(imageFile!!)

        }

        if (requestCode === IMAGE_PICK_CODE) {
            if (resultCode === Activity.RESULT_OK) {
                val picturePath = data!!.getStringExtra("picturePath")
                //perform Crop on the Image Selected from Gallery
                performCrop(picturePath.toString())


            }
        }


        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === RESULT_OK) {

                val path: String = MediaStore.Images.Media.insertImage(
                    requireActivity().contentResolver, result.uri.path, "IMG_" + Calendar.getInstance().time, null)

                uri = Uri.parse(path)
                root?.imgProfilePic?.loadUrl(uri.toString())
                root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                requireActivity().runOnUiThread {
                    customToast(error.toString(), requireActivity(), 0)

                }
            }
        }
    }

    private fun imageCrop(file: File) {
        try {

            val contentUri = FileProvider.getUriForFile(
                requireContext(), requireContext().applicationContext.packageName + ".provider", file)

            CropImage.activity(contentUri).setAspectRatio(1, 1).start(requireContext(), this)
        } // respond to users whose devices do not support the crop action
        catch (anfe: ActivityNotFoundException) {
            // display an error message
            val errorMessage = "your device doesn't support the crop action!"
            customToast(errorMessage, requireActivity(), 0)
        }
    }

    @SuppressLint("QueryPermissionsNeeded") private fun performCrop(picUri: String) {
        try {
            //Start Crop Activity
            val f = File(picUri)
            val contentUri = FileProvider.getUriForFile(
                requireContext(),
                requireContext().applicationContext.packageName + ".provider",
                f)
            CropImage.activity(contentUri).setAspectRatio(1, 1).start(requireContext(), this)
        } // respond to users whose devices do not support the crop action
        catch (anfe: ActivityNotFoundException) {
            // display an error message
            anfe.printStackTrace()
            val errorMessage = "your device doesn't support the crop action!"
            customToast(errorMessage, requireActivity(), 0)
        }
    }


    private fun setBottomSheetForColor(arrayList: List<ColorListQuery.Color>) {
        val llBottomSheet = root?.bottom_sheet

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)
        initRecyclerView(arrayList)

        llBottomSheet.setOnClickListener {
            root?.overlay_view?.visibility = View.VISIBLE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        root?.frameColorLogo?.setOnClickListener {
            root?.overlay_view?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.txtDone?.setOnClickListener {

            if (colorCode != null) {
                root?.overlay_view?.visibility = View.GONE
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                Log.d("tag", "colorCode::::$colorCode")
                root?.imgFrameColor?.visibility = View.VISIBLE
                if (uri != null) {

                    root?.imgProfilePic?.loadUrl(getPath(uri)!!)
                    root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)
                } else {

                    root?.imgProfilePic?.loadUrl(imageUrl.toString())
                    root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)
                }
                root?.imgFrameColor?.setColorFilter(
                    Color.parseColor(colorCode), PorterDuff.Mode.SRC_IN)

                selectedColorCode = colorCode
            }
        }
        root?.overlay_view?.setOnClickListener {
            root?.overlay_view?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun initRecyclerView(arrayList: List<ColorListQuery.Color>) {
        root?.menu_recycler_view?.apply {
            layoutManager = LinearLayoutManager(
                root?.menu_recycler_view?.context, RecyclerView.HORIZONTAL, false)
            adapter = ColorSelectionAdapter(
                arrayList, requireContext())
            (adapter as ColorSelectionAdapter).onItemClick = { data ->
                // do something with your item
                Log.d("tag", data.colorCode().toString())
                colorCode = data.colorCode().toString()
            }
        }
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.bodyBackground)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.bodyBackground)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

}