package com.example.sare.settings.viewmodel

import android.app.Activity
import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.BugTypesQuery
import com.example.sare.api.ApiService
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.NetworkResponse
import com.example.sare.data.ResponseData
import com.example.sare.extensions.customToast
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReportApplicationBugViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var status = MutableLiveData<NetworkResponse>()
    var bugTypeArrayList = MutableLiveData<NetworkResponse>()


    fun reportApplicationBugViewModel(
//        body: MultipartBody,
        body: List<MultipartBody.Part>,
        title: RequestBody,
        description: RequestBody,
        moblieId: RequestBody,
        deviceId: RequestBody,
        category: RequestBody,
        androidSDK: RequestBody,
        version: RequestBody,
        deviceName: RequestBody,
        appVersion: RequestBody,
        token: String,
        activity: Activity
    ) {
        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)

        val call: Call<ResponseData> = service.bugReport(
            body,
            title,
            category,
            deviceName,
            moblieId,
            description,
            deviceId,
            version,
            androidSDK,
            appVersion,
            token
        )


        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(
                call: Call<ResponseData?>?,
                response: Response<ResponseData?>?
            ) {
                Log.e("Upload Success:", response?.body().toString())

                if (response!!.isSuccessful) {

                    if (response.body() != null) {
                        runBlocking {
                            status.postValue(
                                NetworkResponse.SUCCESS.bugReport(
                                    response.body()!!
                                )
                            )
                        }
                    } else {
                        runBlocking {

                            status.postValue(
                                NetworkResponse.ERROR_RESPONSE(
                                    response.errorBody().toString()
                                )
                            )
                        }
                    }

                } else {
                    runBlocking {

                        status.postValue(
                            NetworkResponse.ERROR_RESPONSE(
                                response.errorBody().toString()
                            )
                        )
                    }
                }


            }

            override fun onFailure(
                call: Call<ResponseData?>,
                t: Throwable
            ) {
                t.printStackTrace()
                status.postValue(NetworkResponse.ERROR(t))

                Log.e("Upload error:", t.message!!)

                context.runCatching {
                    customToast( t.message.toString(),activity ,0)

                }
            }
        })
    }

    fun getBugTypes(token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(BugTypesQuery.builder().build())
            .enqueue(object : ApolloCall.Callback<BugTypesQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    bugTypeArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: com.apollographql.apollo.api.Response<BugTypesQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.bugTypes() != null) {

                                bugTypeArrayList.postValue(
                                    NetworkResponse.SUCCESS.getBugTypes(
                                        response.data()?.bugTypes()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                bugTypeArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            bugTypeArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }

                    }
                }
            })
    }



}