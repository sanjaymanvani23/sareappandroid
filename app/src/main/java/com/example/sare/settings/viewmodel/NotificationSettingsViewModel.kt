package com.example.sare.settings.viewmodel

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.NotificationDisplayQuery
import com.example.sare.UpdateNotificationMutation
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class NotificationSettingsViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var saveNotification = MutableLiveData<NetworkResponse>()
    var displayNotification = MutableLiveData<NetworkResponse>()
    var apolloClient: ApolloClient? = null


    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        apolloClient = com.example.sare.ApolloClient.setupApollo(token ?: "")

    }


    fun saveNotificationSettings(
        comment: String,
        follow: String,
        likes: String,
        chatRequest: String
    ) {
        Log.d("tag", "comment::$comment")
        Log.d("tag", "follow::$follow")
        Log.d("tag", "likes::$likes")
        Log.d("tag", "chatRequest::$chatRequest")
        apolloClient?.mutate(
            UpdateNotificationMutation(
                comment.toString(),
                follow.toString(),
                likes.toString(),
                chatRequest.toString()
            )
        )
            ?.enqueue(object : ApolloCall.Callback<UpdateNotificationMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error:::" + e.message)
                    saveNotification.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<UpdateNotificationMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.updateUserNotificationPermission() != null) {
                                Log.d("tag", "save notification data:::${response.data.toString()}")
                                saveNotification.postValue(
                                    NetworkResponse.SUCCESS.saveNotificationSettings(
                                        response.data?.updateUserNotificationPermission()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                saveNotification.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }

                        } catch (e: Exception) {
                            saveNotification.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))


                        }
                    }


                }

            })
    }

    fun displayData(context: Context, activity: Activity) {

        apolloClient?.query(NotificationDisplayQuery.builder().build())
            ?.enqueue(object : ApolloCall.Callback<NotificationDisplayQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error:::" + e.message)
                    displayNotification.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<NotificationDisplayQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.userNotificationPermission() != null) {
                                Log.d(
                                    "tag",
                                    "display notification data:::${response.data.toString()}"
                                )
                                displayNotification.postValue(
                                    NetworkResponse.SUCCESS.displayNotificationSettings(
                                        response.data()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                displayNotification.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            displayNotification.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}