package com.example.sare.settings

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.settings.viewmodel.PrivacySettingsViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_privacy_settings.view.*
import org.json.JSONObject


class PrivacySettingsFragment : Fragment() {
    var root: View? = null

    var token: String? = null
    var userId: String? = null
    var status: Boolean = false

    // var statusNew: Boolean = false
    //  var privacyStatus: Boolean = false
    var privacySettingsViewModel: PrivacySettingsViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_privacy_settings, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@PrivacySettingsFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        privacySettingsViewModel = ViewModelProvider(this)[PrivacySettingsViewModel::class.java]
        setTheme()
        getSharedPreferences()
        setListeners()
        userDetailsObserver(userId.toString())
        return root
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        userId = mainObject.getString("_id")
        setData()
    }

    private fun setData() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        status = mainObject.getBoolean("privacyStatus")
        if (status) {
            root?.imgStatusOff?.visibility = View.GONE
            root?.imgStatusOn?.visibility = View.VISIBLE
        } else {
            root?.imgStatusOn?.visibility = View.GONE
            root?.imgStatusOff?.visibility = View.VISIBLE
        }
    }

    private fun getUserDetails(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.hide()
        }
        privacySettingsViewModel?.getUserDetails(id)
    }

    fun userDetailsObserver(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.hide()
        }

        privacySettingsViewModel?.userDetails?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            privacySettingsViewModel?.getUserDetails(id)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.getEditUserDetails -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    requireActivity().runOnUiThread {
                        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
                            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                        val gson = Gson()

                        // Get java object list json format string.
                        val userInfoListJsonString = gson.toJson(it.response.user())
                        preferences.edit().putString("TOKEN", token).apply()
                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        if (it.response.user().privacyStatus() == true) {
                            root?.imgStatusOff?.visibility = View.GONE
                            root?.imgStatusOn?.visibility = View.VISIBLE
                        } else {
                            root?.imgStatusOn?.visibility = View.GONE
                            root?.imgStatusOff?.visibility = View.VISIBLE

                        }
                        NavHostFragment.findNavController(this).navigateUp()
                    }
                }
            }
        })
    }

    private fun setListeners() {
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }

        root?.llStatus?.setOnClickListener {
            if (status) {
                root?.imgStatusOn?.visibility = View.GONE
                root?.imgStatusOff?.visibility = View.VISIBLE
                status = false
            } else {
                root?.imgStatusOff?.visibility = View.GONE
                root?.imgStatusOn?.visibility = View.VISIBLE
                status = true
            }
        }
        root?.txtSave?.setOnClickListener {
            updateStatus()
        }
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.bodyBackground)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.bodyBackground)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun updateStatus() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        privacySettingsViewModel?.updateStatus(status)

        privacySettingsViewModel?.updatePrivacyStatus?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            privacySettingsViewModel?.updateStatus(status)
                        }
                    snackbar.show()
                }

                is NetworkResponse.SUCCESS.updatePrivacyStatus -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    customToast("Updated Successfully.!", requireActivity(), 1)
                    getUserDetails(userId.toString())
                }
            }
        })

    }
}