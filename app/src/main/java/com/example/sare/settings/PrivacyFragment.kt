package com.example.sare.settings

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.sare.R
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_privacy.view.*
import kotlinx.android.synthetic.main.layout_lastseen_options.view.*

class PrivacyFragment : Fragment() {
    var root : View ?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_privacy, container, false)

        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@PrivacyFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        root?.llShortVidsBlock?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.privacyFragment) {
                findNavController().navigate(R.id.action_privacyFragment_to_blockedUserFragment, bundleOf("type" to "1"))
            }
        }
        root?.llBlockChat?.setOnClickListener {
            if (findNavController().currentDestination?.id == R.id.privacyFragment) {
                findNavController().navigate(R.id.action_privacyFragment_to_blockedUserFragment, bundleOf("type" to "2"))
            }
        }

        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this@PrivacyFragment).navigateUp()
        }
        setBottomSheet()
        return root


    }

    private fun setBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_options

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        root?.llLastseen?.setOnClickListener {
            root?.overlay_view?.visible()

            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

                root?.viewVideo?.text = "Who can view your video"
                root?.txt1?.setOnClickListener {
                    root?.txt2?.isChecked = false
                    root?.txt3?.isChecked = false
                    root?.txt4?.isChecked = false
                    root?.txt1?.isChecked = true

                }
                root?.txt2?.setOnClickListener {
                    root?.txt1?.isChecked = false
                    root?.txt3?.isChecked = false
                    root?.txt4?.isChecked = false
                    root?.txt2?.isChecked = true

                }
                root?.txt3?.setOnClickListener {
                    root?.txt2?.isChecked = false
                    root?.txt1?.isChecked = false
                    root?.txt4?.isChecked = false
                    root?.txt3?.isChecked = true

                }
                root?.txt4?.setOnClickListener {
                    root?.txt2?.isChecked = false
                    root?.txt3?.isChecked = false
                    root?.txt1?.isChecked = false
                    root?.txt4?.isChecked = true

                }
                if (root?.txtLastSeen?.text == "Followers") {
                    root?.txt2?.isChecked = true
                } else if (root?.txtLastSeen?.text == "My contacts") {
                    root?.txt3?.isChecked = true
                } else if (root?.txtLastSeen?.text == "Everyone") {
                    root?.txt4?.isChecked = true
                } else {
                    root?.txt2?.isChecked = false
                    root?.txt3?.isChecked = false
                    root?.txt4?.isChecked = false

                }
                root?.txt1?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        root?.txtLastSeen?.text = root?.txt1?.text
                    }
                }
                root?.txt2?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        root?.txtLastSeen?.text = root?.txt2?.text

                    }
                }
                root?.txt3?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        root?.txtLastSeen?.text = root?.txt3?.text

                    }
                }
                root?.txt4?.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        root?.txtLastSeen?.text = root?.txt4?.text

                    }
                }

            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }
        root?.overlay_view?.setOnClickListener {
            root?.overlay_view?.gone()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view?.visible()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }


}