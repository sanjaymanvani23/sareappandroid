package com.example.sare.model

import com.example.sare.CommentsListQuery
import java.util.*

data class CommentReplies (
    var __typename: String? = null,
    var _id: String? = null,
    var comment: String? = null,
    private val commentedAt: String? = null,
    val user: Object,
    private val liked: Boolean? = null,
    private val likeCount: Int? = null
)
