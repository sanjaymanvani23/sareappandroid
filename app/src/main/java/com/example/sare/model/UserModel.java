package com.example.sare.model;

public class UserModel {

    public String userId;

    public String name;

    public String colorCode;

    private String username;

    private Object mobile;

    private String ssoId;

    private Integer followerCounts;

    private Integer followingCounts;

    private String createdAt;

    private Integer likeCounts;

    private String bio;

    private String image;

    private String coverImage;

    private String dob;

    private String email;

    private String gender;

    private Boolean followed;

    private Boolean followedBack;

    private Integer postsCounts;

    private Boolean privacyStatus;

    private String authentication;

    private Boolean agreementAccepted;

    private String status;

    private Boolean blocked;

    private String state;

    private String city;

    private String location;

    private Boolean verifyUser;

    private Integer downloadCounts;

}
