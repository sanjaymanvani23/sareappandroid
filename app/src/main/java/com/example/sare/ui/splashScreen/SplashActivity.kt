package com.example.sare.ui.splashScreen

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.SplashVersionQuery
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.auth.viewmodel.SplashViewModel
import com.example.sare.base.BaseActivity
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.customToast
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_splash.*
import kotlinx.android.synthetic.main.popup_update_app.*
import org.json.JSONObject

class SplashActivity : BaseActivity() {
    var token: String? = ""
    var user: String? = null
    var userId: String? = null
    private var splashViewModel: SplashViewModel? = null
    private var splashData: SplashVersionQuery.Data? = null
    private var videoComplete = false
    private var apiComplete = false
    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.fragment_splash)
        Glide.with(this).load(R.drawable.master_brand_name).into(imgSplash)

        splashViewModel = ViewModelProvider(this)[SplashViewModel::class.java]

        getSharedPreferences()



        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        Handler().postDelayed({/*
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()*/
            //            splashData?.let { it1 -> apiCall(it1) }
            versionCheck()
        }, 600) // 3000 is the delayed time in milliseconds.

        /*val video = Uri.parse("android.resource://" + packageName.toString() + "/" + R.raw.roleit_splash_video)

            videoSplash?.setVideoURI(video)

            videoSplash?.setZOrderOnTop(true)

            videoSplash?.start()

            videoSplash?.setOnCompletionListener {

                videoComplete = true

                splashData?.let { it1 -> apiCall(it1) }

            }*/



        setTheme()

    }



    private fun getSharedPreferences() {

        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        user = preferences.getString("USER", null)
        userId = if (user != null) {
            val mainObject = JSONObject(user)
            mainObject.getString("_id")
        } else {
            ""
        }

    }


    private fun versionCheck() {
        splashViewModel?.versionCheck(userId.toString())

        splashViewModel?.version?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(videoSplash, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            splashViewModel?.versionCheck(userId.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToastMain(it.errorMsg, 2)
                    openActivity(LoginActivity::class.java)

                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    customToastMain(it.errorMsg, 2)
                    openActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.versionCheck -> {
                    apiComplete = true
                    splashData = it.response
                    apiCall(splashData!!)
                }

            }
        })


    }

    private fun apiCall(it: SplashVersionQuery.Data) {

        if (apiComplete) {
            //        if (videoComplete && apiComplete) {

            if (it != null) {
                Log.d("tag", "Data Success: ${it}")
                val manager: PackageManager = packageManager
                val info: PackageInfo = manager.getPackageInfo(packageName, 0)
                val version: String = info.versionCode.toString()

                Log.d("tag", "version::$version")

                if (version < it.version().android().toString()) {

                    runOnUiThread {
                        showUpdatePopup()
                    }
                } else {
                    //Initialize the Handler
                    runOnUiThread {
                        if (it.version().user()?.status().toString() == "BLOCK") {
                            runOnUiThread {
                                customToast(getString(R.string.you_cannot_use_app), this, 0)
                            }
                        } else if (it.version().user().toString().isEmpty() || it.version()
                                .user() == null || it.version().user()!!.image().isNullOrEmpty() || it.version()
                                .user()!!.colorCode().isNullOrEmpty() || it.version().user()
                                ?.agreementAccepted() == false
                        ) {
                            openActivity(LoginActivity::class.java)
                            finish()

                        } /*else if (it.version().user() != null && it.version().user()?._id()!!.isNotEmpty()) {
                            if (it.version().user() != null &&
                            ) {
                                openActivity(FragmentTermsCondition::class.java)
                                finish()
                            } else {
                                val preferences: SharedPreferences = getSharedPreferences(
                                    StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                                val gson = Gson()
                                val userInfoListJsonString = gson.toJson(it.version().user())
                                preferences.edit().putString("USER", userInfoListJsonString).apply()
                                openActivity(BottomBarActivity::class.java)
                                finish()
                            }

                        } */ else {
                            val preferences: SharedPreferences = getSharedPreferences(
                                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                            val gson = Gson()
                            val userInfoListJsonString = gson.toJson(it.version().user())
                            preferences.edit().putString("USER", userInfoListJsonString).apply()
                            openActivity(BottomBarActivity::class.java)
                            finish()
                            /*openActivity(LoginActivity::class.java)
                            finish()*/
                        }
                    }
                }
            } else {

                runOnUiThread {
                    customToastMain("Please try again later", 0)
                }
            }

        }
    }

    private fun showUpdatePopup() {
        val dialog: Dialog = Dialog(this)
        // Include dialog.xml file
        dialog.setContentView(R.layout.popup_update_app)
        dialog.btnUpdateApp.setOnClickListener {
            val appPackageName: String = packageName // package name of the app
            Log.d("tag", "appPackageName::$appPackageName")

            try {
                startActivity(
                    Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
            } catch (anfe: ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
        }


        dialog.show()
    }

}