package com.example.sare.ui.optverify

import android.app.Activity
import android.content.Context
import android.content.IntentFilter
import android.content.SharedPreferences
import android.graphics.Color
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.auth.MySMSBroadcastReceiver
import com.example.sare.auth.viewmodel.LoginViewModel
import com.example.sare.auth.viewmodel.VerifyOTPViewModel
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.customToast
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.firebase.MyFirebaseMessagingService
import com.example.sare.signUpDetails.fragment.FragmentTermsCondition
import com.example.sare.signUpDetails.fragment.SetYourProfileActivity
import com.example.sare.signUpDetails.fragment.SignUpDetailsActivity
import com.example.sare.signUpDetails.viewmodel.TermsAndConditionViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_otp_verification.*

@RequiresApi(Build.VERSION_CODES.M) class VerifyOtpActivity : AppCompatActivity() {

    val TAG = "FRAGMENT VERIFY OTP"
    var root: View? = null
    var otp: String = ""
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1500 //3 seconds
    var mobileNo = ""
    var countryId = ""
    var countryCode = ""
    var deviceToken: String = ""

    var simpleMobileNo = ""
    private var r: Runnable? = null
    private var runnable: Runnable? = null
    var handler: Handler? = null
    var mySMSBroadcastReceiver: MySMSBroadcastReceiver = MySMSBroadcastReceiver()
    private var loginViewModel: LoginViewModel? = null
    private var verifyOtpViewModel: VerifyOTPViewModel? = null
    var latitude: String = ""
    var longitude: String = ""
    var city: String = ""
    var state: String = ""
    var location: String = ""

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        registerReceiver(mySMSBroadcastReceiver, filter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(mySMSBroadcastReceiver)
    }

    private fun registerBroadcastReceiver(et1: EditText, et2: EditText, et3: EditText, et4: EditText) {
        mySMSBroadcastReceiver.initOTPListener(object : MySMSBroadcastReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String) {
                Log.d("tag", "otp:::$otp")
                val firstLetter = otp[0]

                et1.setText(otp[0].toString())
                et2.setText(otp[1].toString())
                et3.setText(otp[2].toString())
                et4.setText(otp[3].toString())

                et1.background = ThemeManager.otpGetDrawable(
                    this@VerifyOtpActivity, ThemeManager.colors(
                        this@VerifyOtpActivity, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                et2.background = ThemeManager.otpGetDrawable(
                    this@VerifyOtpActivity, ThemeManager.colors(
                        this@VerifyOtpActivity, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                et3.background = ThemeManager.otpGetDrawable(
                    this@VerifyOtpActivity, ThemeManager.colors(
                        this@VerifyOtpActivity, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                et4.background = ThemeManager.otpGetDrawable(
                    this@VerifyOtpActivity, ThemeManager.colors(
                        this@VerifyOtpActivity, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
            }

            override fun onOTPTimeOut() {
                customToastMain("Timeout.. Please Click on Resend.",0)
            }

        })
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        registerReceiver(mySMSBroadcastReceiver, intentFilter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_otp_verification)
        deviceToken = MyFirebaseMessagingService.getToken(this)
        Log.d("tag", "deviceToken:::$deviceToken")
        getBundleData()

        loginViewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        verifyOtpViewModel = ViewModelProvider(this)[VerifyOTPViewModel::class.java]
        viewVerifyOtpObserver()


        startSmsListener()

        registerBroadcastReceiver(otp1!!, otp2!!, otp3!!, otp4!!)

        setTheme()

        mobile_no.text = mobileNo

        val imm = getSystemService(
            Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(otp1, 0)

        button_resend_otp.alpha = 1f

        setListners()
        updateProgress()

    }

    private fun getBundleData() {

        intent.extras?.run {
            mobileNo = getString("mobileNo", "").toString()
            countryId = getString("countryId", "").toString()
            countryCode = getString("countryCode", "").toString()
            simpleMobileNo = getString("simpleMobileNo", "").toString()
            latitude = getString("latitude", "").toString()
            longitude = getString("longitude", "").toString()
            city = getString("city", "").toString()
            state = getString("state", "").toString()
            location = getString("location", "").toString()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun setListners() {

        imgBackVerifyOTP.setOnClickListener {
            onBackPressed()
        }

        edit_image.setOnClickListener {
            onBackPressed()
        }


        otp1.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                v.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                otp1.setSelection(otp1.text.toString().length ?: 0)
            } else {
                if (otp1.text.isNotEmpty()) {
                    v.background = ThemeManager.otpGetDrawable(
                        this, ThemeManager.colors(
                            this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                } else {
                    v.background = ThemeManager.otpGetDrawable(
                        this,
                        ThemeManager.colors(
                            this, StringSingleton.textWhite),
                        R.dimen.otp_edit_box_corner_radius,
                        5,
                        R.color.otp_background)
                }
            }
        }

        otp2.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                v.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                otp2.setSelection(otp2.text.toString().length ?: 0)
            } else {
                if (otp2.text.isNotEmpty()) {
                    v.background = ThemeManager.otpGetDrawable(
                        this, ThemeManager.colors(
                            this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                } else {
                    v.background = ThemeManager.otpGetDrawable(
                        this,
                        ThemeManager.colors(
                            this, StringSingleton.textWhite),
                        R.dimen.otp_edit_box_corner_radius,
                        5,
                        R.color.otp_background)
                }

            }
        }

        otp3.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                v.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                otp3.setSelection(otp3.text.toString().length ?: 0)
            } else {
                if (otp3.text.isNotEmpty()) {
                    v.background = ThemeManager.otpGetDrawable(
                        this, ThemeManager.colors(
                            this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                } else {
                    v.background = ThemeManager.otpGetDrawable(
                        this,
                        ThemeManager.colors(
                            this, StringSingleton.textWhite),
                        R.dimen.otp_edit_box_corner_radius,
                        5,
                        R.color.otp_background)
                }
            }
        }

        otp4.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                v.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)
                otp4.setSelection(otp4.text.toString().length ?: 0)
            } else {
                v.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5, R.color.otp_background)

            }
        }

        otp1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length ?: 0 > 0) {
                    if (otp1.text!!.isNotEmpty() && otp2.text!!.isNotEmpty() && otp3.text!!.isNotEmpty() && otp4.text!!.isNotEmpty()) {
                        verifyOtp(otp)
                    } else {
                        otp2.requestFocus()
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        otp2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length ?: 0 > 0) {
                    if (otp1.text!!.isNotEmpty() && otp2.text!!.isNotEmpty() && otp3.text!!.isNotEmpty() && otp4.text!!.isNotEmpty()) {
                        verifyOtp(otp)
                    } else {
                        otp3.requestFocus()
                    }
                } else {
                    otp1.requestFocus()
                    val imm = getSystemService(
                        Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(otp1, 0)
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        otp3.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length ?: 0 > 0) {
                    if (otp1.text!!.isNotEmpty() && otp2.text!!.isNotEmpty() && otp3.text!!.isNotEmpty() && otp4.text!!.isNotEmpty()) {
                        verifyOtp(otp)
                    } else {
                        otp4.requestFocus()
                    }
                } else {
                    otp2.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        otp4.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s?.length ?: 0 > 0) {
                    otp = otp1.text.toString() + otp2.text.toString() + otp3.text.toString() + otp4.text.toString()

                    Log.d("tag", "otp::::$otp")

                    if (otp1.text!!.isNotEmpty() && otp2.text!!.isNotEmpty() && otp3.text!!.isNotEmpty() && otp4.text!!.isNotEmpty()) {
                        verifyOtp(otp)
                    }
                } else {
                    otp3.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })


        button_resend_otp.setOnClickListener {
            updateProgress()
            resendOtp()
        }

        otp2.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (otp2.text.length ?: 0 == 0) otp1.requestFocus()
            }
            return@setOnKeyListener false

        }

        otp3.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (otp3.text.length ?: 0 == 0) otp2.requestFocus()
            }
            return@setOnKeyListener false
        }

        otp4.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                if (otp4.text.length ?: 0 == 0) otp3.requestFocus()
            }
            return@setOnKeyListener false

        }
    }

    private fun resendOtp() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        loginViewModel?.doLogin(mobileNo, countryId)

        loginViewModel?.login?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(otp3, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            loginViewModel?.doLogin(mobileNo, countryId)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    openActivity(LoginActivity::class.java)
                    finish()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.doLogin -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (it.response.generateOtp() != null) {

                        runOnUiThread {
                            customToast("OTP sent successfully", this, 1)
                        }

                    } else {

                        runOnUiThread {
                            customToast("Please try again later", this, 0)
                        }
                    }

                }
            }
        })

    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }

        text_mobile_verification.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textDarkBlack))

        text_enter_otp.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))



        country_code.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textDarkBlack))
        mobile_no.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textDarkBlack))


        otp1.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))
        otp2.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))
        otp3.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))
        otp4.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textLight))

        otp1.background = ThemeManager.otpGetDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5, R.color.otp_background)

        otp2.background = ThemeManager.otpGetDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5, R.color.otp_background)

        otp3.background = ThemeManager.otpGetDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5, R.color.otp_background)

        otp4.background = ThemeManager.otpGetDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5, R.color.otp_background)

        main_layout.background = ThemeManager.getDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.no_corner_radius)
    }

    private fun verifyOtp(otp: String) {
        val progressDialog = CustomProgressDialogNew(this)

        progressDialog.hide()
        hideKeyboard(this)
        Log.d("tag", "simpleMobileNo:::$simpleMobileNo")
        Log.d("tag", "countryId:::$countryId")
        Log.d("tag", "otp:::$otp")

        verifyOtpViewModel?.verifyOTP(
            simpleMobileNo,
            countryId,
            otp,
            this,
            "ANDROID",
            deviceToken.toString(),
            latitude,
            longitude,
            city,
            state,
            location)


    }

    private fun viewVerifyOtpObserver() {
        val progressDialog = CustomProgressDialogNew(this)

        verifyOtpViewModel?.verifyOtp?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(otp3, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            verifyOtpViewModel?.verifyOTP(
                                simpleMobileNo,
                                countryId,
                                otp,
                                this,
                                "ANDROID",
                                deviceToken.toString(),
                                latitude,
                                longitude,
                                city,
                                state,
                                location)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, this, 0)
                    MyApplication.mainActivity.logoutUser()
                    openActivity(LoginActivity::class.java)
                    finish()
                }

                is NetworkResponse.SUCCESS.verifyOtp -> {

                    runOnUiThread {
                        progressDialog.hide()

                    }

                    Log.d("tag", "response::" + it.response)
                    val token = it.response.login().token()
                    val userId = it.response.login().userId()
                    val email = it.response.login().email()
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                    preferences.edit().putString("TOKEN", token).apply()

                    val gson = Gson()

                    // Get java object list json format string.
                    val userInfoListJsonString = gson.toJson(it.response.login())
                    //                    preferences.edit().putString("USER", userInfoListJsonString).apply()
                    if (it.response.login().username().toString().isEmpty() || it.response.login().username()
                            .toString() == "null" || it.response.login().username() == null
                    ) {
//                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        preferences.edit().putString("TOKEN", token).apply()
                        val bundle = bundleOf(
                            "city" to city, "state" to state, "location" to location)

                        openActivity(SignUpDetailsActivity::class.java, bundle)
                        finishAffinity()

                    } else {
                        getUserDetails(it.response.login().userId(), it.response.login().token()!!)
                    }

                }

                is NetworkResponse.ERROR_RESPONSE -> {

                    Log.d("tag", "error_response::${it.errorMsg}")
                    customToast(it.errorMsg, this, 0)
                    if (it.errorMsg.isNotEmpty()) {
                        mDelayHandler = Handler()
                        //Navigate with delay
                        runnable = Runnable {

                            val imm = getSystemService(
                                Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.showSoftInput(otp1, 0)
                            otp2.background = ThemeManager.otpGetDrawable(
                                this, ThemeManager.colors(
                                    this, StringSingleton.textWhite), R.dimen.otp_edit_box_corner_radius, 5)

                            otp3.background = ThemeManager.otpGetDrawable(
                                this,
                                ThemeManager.colors(
                                    this, StringSingleton.textWhite),
                                R.dimen.otp_edit_box_corner_radius,
                                5,
                                R.color.otp_background)

                            otp4.background = ThemeManager.otpGetDrawable(
                                this,
                                ThemeManager.colors(
                                    this, StringSingleton.textWhite),
                                R.dimen.otp_edit_box_corner_radius,
                                5,
                                R.color.otp_background)

                            otp1.setText("")
                            otp2.setText("")
                            otp3.setText("")
                            otp4.setText("")
                            otp1.requestFocus()

                            otp1.setTextColor(Color.parseColor("#000000"))
                            otp2.setTextColor(Color.parseColor("#000000"))
                            otp3.setTextColor(Color.parseColor("#000000"))
                            otp4.setTextColor(Color.parseColor("#000000"))

                        }

                        mDelayHandler?.postDelayed(runnable!!, SPLASH_DELAY)

                        otp1.background = ThemeManager.otpGetDrawable(
                            this,
                            ThemeManager.colors(
                                this, StringSingleton.textWhite),
                            R.dimen.otp_edit_box_corner_radius,
                            5,
                            R.color.otp_wrong)

                        otp2.background = ThemeManager.otpGetDrawable(
                            this,
                            ThemeManager.colors(
                                this, StringSingleton.textWhite),
                            R.dimen.otp_edit_box_corner_radius,
                            5,
                            R.color.otp_wrong)

                        otp3.background = ThemeManager.otpGetDrawable(
                            this,
                            ThemeManager.colors(
                                this, StringSingleton.textWhite),
                            R.dimen.otp_edit_box_corner_radius,
                            5,
                            R.color.otp_wrong)
                        otp4.background = ThemeManager.otpGetDrawable(
                            this,
                            ThemeManager.colors(
                                this, StringSingleton.textWhite),
                            R.dimen.otp_edit_box_corner_radius,
                            5,
                            R.color.otp_wrong)

                        otp1.setTextColor(Color.parseColor("#FA0E02"))
                        otp2.setTextColor(Color.parseColor("#FA0E02"))
                        otp3.setTextColor(Color.parseColor("#FA0E02"))
                        otp4.setTextColor(Color.parseColor("#FA0E02"))
                    } else {
                        runnable?.let { it1 -> mDelayHandler?.removeCallbacks(it1) }
                    }

                }
            }
        })
    }

    private fun startSmsListener() {
        val client = SmsRetriever.getClient(this)
        val task = client.startSmsRetriever()
        task.addOnSuccessListener {
            // Successfully started retriever, expect broadcast intent
            // ...
            Log.d("tag", "Waiting for the OTP")
        }

        task.addOnFailureListener {
            // Failed to start retriever, inspect Exception for more details
            // ...
            Log.d("tag", "Cannot Start SMS Retriever")
        }
    }

    private fun getUserDetails(id: String, token: String) {
        verifyOtpViewModel?.getUserDetails(id, token)

        verifyOtpViewModel?.userDetails?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(otp3, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            verifyOtpViewModel?.getUserDetails(id, token)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.getEditUserDetails -> {

                    Log.d("tag", "response::" + it.response)
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                    preferences.edit().putString("TOKEN", token).apply()

                    if (it.response.user().status().toString() == "BLOCK") {
                        runOnUiThread {
                            customToast(getString(R.string.you_cannot_use_app), this, 0)
                        }
                    } else if (it.response.user().colorCode().toString().isEmpty() || it.response.user().image()
                            .toString().isEmpty() || it.response.user().image().toString() == ""
                    ) {
                        preferences.edit().putString("TOKEN", token).apply()
                        val gson = Gson()

                        val userInfoListJsonString = gson.toJson(it.response.user())
                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        preferences.edit().remove("imageSaved").apply()
                        preferences.edit().remove("colorCodeSaved").apply()
                        openActivity(SetYourProfileActivity::class.java)

                    } else if (it.response.user().agreementAccepted() == false) {

                        openActivity(FragmentTermsCondition::class.java)

                    } else {

                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(it.response.user())
                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        openActivity(BottomBarActivity::class.java)
                        finish()
                    }
                }
            }
        })

    }

    private fun updateProgress() {
        handler = Handler()
        var count = 15

        r = Runnable {
            count--
            if (count >= 0) {
                timer_text.visibility = View.VISIBLE
                button_resend_otp.text = "After $count" + "s"
                timer_text.text = "Resend OTP"

                handler?.postDelayed(r!!, 1000)
                button_resend_otp.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.textOtp), R.dimen.otp_edit_box_corner_radius, 0)
                button_resend_otp.alpha = 0.5f
                button_resend_otp.isEnabled = false

            } else {
                button_resend_otp.text = "Resend OTP"
                timer_text.visibility = View.GONE
                button_resend_otp.background = ThemeManager.otpGetDrawable(
                    this, ThemeManager.colors(
                        this, StringSingleton.button_login_background), R.dimen.otp_edit_box_corner_radius, 0)
                button_resend_otp.alpha = 1f
                button_resend_otp.isEnabled = true


            }
        }

        handler?.postDelayed(r!!, 1000)
    }

    override fun onDestroy() {
        if (mDelayHandler != null) {
            runnable?.let { mDelayHandler!!.removeCallbacks(it) }
        }
        if (handler != null) {
            r?.let { handler!!.removeCallbacks(it) }

        }
        super.onDestroy()
    }


    private fun hideKeyboard(activity: Activity) {
        val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // check if no view has focus:
        val currentFocusedView: View? = activity.currentFocus
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(
                currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }

}