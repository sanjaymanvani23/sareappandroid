package com.example.sare.ui.video

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.UseMusicAdapter
import com.example.sare.dashboard.VideoListViewModel
import com.example.sare.extensions.customToast
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.openActivity
import com.example.sare.extensions.openNewActivity
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_use_music.*

class UseMusicActivity : AppCompatActivity(), ItemOnclickListener {
    var listMusic: ArrayList<VideoAudioListQuery.Video>? = null
    var audioId: String = ""
    var videoListViewModel: VideoListViewModel? = null
    var progressDialog: CustomProgressDialogNew? = null
    private val audios: MutableList<VideoAudioListQuery.Video> = mutableListOf<VideoAudioListQuery.Video>()
    private var profileUserVideoList: MutableList<VideoListQuery.Video> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_use_music)
        setTheme()
        audioId = intent?.getStringExtra("audioId").toString()
        videoListViewModel = ViewModelProvider(this)[VideoListViewModel::class.java]

        Log.d("tag", "audioId::::$audioId")
        initUseMusicRecyclerView()
        setObservers()


        audioList()
        imgBack?.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setObservers() {
        audioListObserver()
    }

    private fun audioList() {
        progressDialog = CustomProgressDialogNew(this)
        videoListViewModel?.getVideoAudioList(audioId)
    }

    private fun audioListObserver() {

        videoListViewModel?.videoAudioListArrayList?.observe(this, Observer {
            progressDialog?.hide()

            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog?.hide()

                    it.error.printStackTrace()
                    main_layout?.visibility = View.VISIBLE
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relUseMusic), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        videoListViewModel?.getVideoAudioList(audioId)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, this, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.videoAudioList -> {

                    txtId?.text = it.response.user()?.username()

                    imgFrameColor?.loadUrl(Utils.PUBLIC_URL + it.response.user()?.image().toString())

                    audios.addAll(it.response.videos()!!)

                    val gson = Gson()
                    val userInfoListJsonString = gson.toJson(it.response.videos())
                    val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                    val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(userInfoListJsonString, myType)

                    profileUserVideoList.addAll(userVideoList)

                    txtVideosCount?.text = audios.size.toString() + " Videos"

                    rvUseMusic?.adapter?.notifyDataSetChanged()

                    if (rvUseMusic?.adapter?.itemCount != 0) {
                        rvUseMusic?.visibility = View.VISIBLE
                        txtNoData?.visibility = View.GONE
                    } else {
                        rvUseMusic?.visibility = View.GONE
                        txtNoData?.visibility = View.VISIBLE
                    }
                }

                is NetworkResponse.ERROR_RESPONSE_NEW -> {
                    progressDialog?.hide()
                    customToast("Something went wrong.!", this, 0)
                }

            }

        })
    }

    override fun onItemClickListener(position: Int) {
        val gson = Gson()
        val userInfoListJsonString = gson.toJson(profileUserVideoList)
        var bundle = bundleOf(
            "navigationFlagProfile" to "UserMusic",
            "videoList" to userInfoListJsonString,
            "position" to position,
            "isVideoCurrentPage" to 1)
        //        findNavController().navigate(R.id.action_profileUserFragment_to_UserVideoFragment, bundle)

    }

    private fun initUseMusicRecyclerView() {
        rvUseMusic?.apply {
            layoutManager = GridLayoutManager(
                rvUseMusic?.context, 3)
            adapter = UseMusicAdapter(
                audios, context, this@UseMusicActivity)

        }
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.transparent)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.transparent)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }
}