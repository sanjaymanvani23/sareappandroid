package com.example.sare.ui.tag

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.adapter.TagsMainAdapter
import com.example.sare.dashboard.tags.SearchAccountAndTagsActivity
import com.example.sare.dashboard.viewmodel.TagsViewModel
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.extensions.openNewActivity
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragement_tags.*
import kotlinx.android.synthetic.main.fragement_tags.searchLayout
import kotlinx.android.synthetic.main.fragement_tags.txtNoData

@RequiresApi(Build.VERSION_CODES.M) class TagsActivity : AppCompatActivity() {

    var root: View? = null
    var tagsViewModel: TagsViewModel? = null
    private var currentPage: Int = 0
    var isLoading = false
    var isLastPage = false
    private var allList: MutableList<TrendingTagsAndProfileQuery.Trending> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragement_tags)
        setTheme()
        tagsViewModel = ViewModelProvider(this)[TagsViewModel::class.java]
        initRecyclerView()
        tagsList()
        tagsObserver()

        type_all.setOnClickListener {
            val intent = Intent()
            setResult(102, intent)
            overridePendingTransition(0,0)
            finish()
        }
        txtFollowing.setOnClickListener {
            val intent = Intent()
            setResult(101, intent)
            overridePendingTransition(0,0)
            finish()
        }
        searchLayout.setOnClickListener {
            openActivity(SearchAccountAndTagsActivity::class.java)
            overridePendingTransition(0, 0)

        }

    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.textWhite)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, "#FFFFFF")
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun tagsList() {
        tagsViewModel?.getTrendingTags(this, currentPage * Utils.PAGE_SIZE)
    }

    private fun tagsObserver() {
        val progressDialog = CustomProgressDialogNew(this)
       runOnUiThread {
            progressDialog.show()
        }
        tagsViewModel?.trendingTagsList?.observe(this, Observer { it ->

            when (it) {

                is NetworkResponse.ERROR -> {
                    progressDialog.hide()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        txtFollowing, "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        tagsViewModel?.getTrendingTags(this, currentPage * Utils.PAGE_SIZE)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()
                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.getTrendingTagsList -> {

                    runOnUiThread {
                        progressDialog.hide()
                    }
                    isLoading = false

                    allList.addAll(it.response)
                    recyclerView.adapter!!.notifyDataSetChanged()

                    if (it.response.size < Utils.PAGE_SIZE) {
                        isLastPage = true
                    }
                    if (recyclerView.adapter!!.itemCount == 0) {
                        txtNoData.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    } else {
                        txtNoData.visibility = View.GONE
                        recyclerView.visibility = View.VISIBLE
                    }
                }
            }
        })
    }


    private fun initRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(recyclerView.context, RecyclerView.VERTICAL, false)

            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {

                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    tagsList()
                }

                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }
            })

            adapter = TagsMainAdapter(
                allList, this@TagsActivity)
        }

    }
}
