package com.example.sare.ui.login

import android.Manifest
import android.animation.ValueAnimator
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import br.com.simplepass.loadingbutton.animatedDrawables.ProgressType
import br.com.simplepass.loadingbutton.customViews.ProgressButton
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.auth.GpsTracker
import com.example.sare.auth.viewmodel.LoginViewModel
import com.example.sare.base.BaseActivity
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.dashboard.hideKeyboard
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.openActivity
import com.example.sare.firebase.MyFirebaseMessagingService
import com.example.sare.signUpDetails.fragment.FragmentTermsCondition
import com.example.sare.signUpDetails.fragment.SignUpDetailsActivity
import com.example.sare.signUpDetails.fragment.SetYourProfileActivity
import com.example.sare.signUpDetails.viewmodel.TermsAndConditionViewModel
import com.example.sare.ui.optverify.VerifyOtpActivity
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

@RequiresApi(Build.VERSION_CODES.M) class LoginActivity : AppCompatActivity() {

    val apolloClient = ApolloClient.setupApollo("")
    lateinit var mGoogleSignInClient: GoogleSignInClient
    private val RC_SIGN_IN = 9001
    val TAG = "Fragemnt Login"
    private lateinit var callbackManager: CallbackManager
    var country: CountryQuery.Country? = null
    var isLoggedIn: Boolean = false
    var progressDialog: CustomProgressDialogNew? = null

    private var loginViewModel: LoginViewModel? = null

    var deviceToken: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var city: String = ""
    var state: String = ""
    var location: String = ""

    private var gpsTracker: GpsTracker? = null
    var isLocationGet: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_login)

        val accessToken = AccessToken.getCurrentAccessToken()
        isLoggedIn = accessToken != null && !accessToken.isExpired

        getSharedPreferences()
        getLocationOfDevice()

        loginViewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        progressDialog = CustomProgressDialogNew(this)
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)
        setCallback()
        setTheme()
        countryCode()
        setListeners()

    }

    private fun getLocationOfDevice() {
        try {

            if (checkSelfPermission(
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED && checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED
                && checkSelfPermission(
                        Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED && checkSelfPermission(
                    Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS)
                //show popup to request runtime permission
                requestPermissions(permissions, 101)
                Log.d("tag", "isLocationGet:::$isLocationGet")

            } else {
                Log.d("tag", "isLocationGet Before:::$isLocationGet")

                isLocationGet = true
                Log.d("tag", "isLocationGet After:::$isLocationGet")

                getLocation()
                Log.d("tag", "Location:::$latitude,$longitude")
                getAddress(latitude.toDouble(), longitude.toDouble())
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            runOnUiThread {
                customToastMain("Cannot get location!!", 0)
            }
        }

    }



    fun getAddress(lat: Double, lng: Double) {
        val geocoder = Geocoder(this, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(lat, lng, 1)
            val obj = addresses[0]
            var add = obj.getAddressLine(0)
            add = """
                $add
                ${obj.countryName}
                """.trimIndent()
            add = """
                $add
                ${obj.countryCode}
                """.trimIndent()
            add = """
                $add
                ${obj.adminArea}
                """.trimIndent()
            add = """
                $add
                ${obj.postalCode}
                """.trimIndent()
            add = """
                $add
                ${obj.subAdminArea}
                """.trimIndent()
            add = """
                $add
                ${obj.locality}
                """.trimIndent()
            add = """
                $add
                ${obj.subThoroughfare}
                """.trimIndent()
            Log.v("tag", "Address::::$add")
            Log.d("tag", "obj.locality:::${obj.locality}")
            Log.d("tag", "obj.locality:::${obj.countryName}")
            Log.d("tag", "obj.locality:::${obj.adminArea}")

            city = obj.locality
            state = obj.adminArea
            location = obj.countryName

            val preferences: SharedPreferences = getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            preferences.edit().putString("address", "$city, $state, $location").apply()

        } catch (e: Exception) {
            // TODO Auto-generated catch block
            e.printStackTrace()
            customToastMain(e.message.toString(), 0)
        }
    }

    fun getLocation() {
        gpsTracker = GpsTracker(this)
        if (gpsTracker!!.canGetLocation()) {
            latitude = gpsTracker!!.latitude.toString()
            longitude = gpsTracker!!.longitude.toString()

        } else {
            gpsTracker!!.showSettingsAlert()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permission Granted
                //Do your work here
                //Perform operations here only which requires permission
                Log.d("tag", "isLocationGet Before:::$isLocationGet")

                isLocationGet = true
                Log.d("tag", "isLocationGet After:::$isLocationGet")
                getLocation()
                Log.d("tag", "Location:::$latitude,$longitude")
                getAddress(latitude.toDouble(), longitude.toDouble())
            } else {
                customToastMain(getString(R.string.permission_denied), 0)

            }
        }
    }


    private fun getSharedPreferences() {
        FirebaseApp.initializeApp(this)
        deviceToken = MyFirebaseMessagingService.getToken(this)
        Log.d("tag", "deviceToken:::$deviceToken")
    }

    private fun setCallback() {
        if (isLoggedIn) {
            LoginManager.getInstance().logOut()
        }
        callbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                // App code
                Log.d(TAG, "Facebook token: " + loginResult?.accessToken?.token)

                if (loginResult != null) {
                    setFacebookData(loginResult.accessToken)
                }

            }

            override fun onCancel() {
                // App code
                Log.d(TAG, "Facebook onCancel.")

            }

            override fun onError(exception: FacebookException) {
                // App code
                Log.d(TAG, "Facebook onError.")

            }
        })
    }

    private fun setListeners() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        imageViewGoogle.setOnClickListener {
            mGoogleSignInClient.signOut()
            LoginManager.getInstance().logOut()
            signIn()
        }


        button_login.setOnClickListener {

            if (etPhoneNumber.text!!.isEmpty()) {
                customToastMain(getString(R.string.please_enter_no), 0)
            } else {

                if (etPhoneNumber.text!!.isNotEmpty() && etPhoneNumber.text.toString() != "0000000000" && !etPhoneNumber.text.toString()
                        .contains(" ") && TextUtils.isDigitsOnly(etPhoneNumber.text.toString())
                ) {

                    Log.d("tag", "country?._id::${country?._id()}")
                    if (country?.ISO2()?.toUpperCase().equals("IN") && etPhoneNumber.text!!.length == 10) {
                        button_login.startAnimation {
                            if (isLocationGet) {
                                login(country?._id().toString(), country?.dial().toString())
                            } else {
                                getLocationOfDevice()
                            }
                        }

                    } else if (!country?.ISO2()?.toUpperCase().equals("IN") && etPhoneNumber.text!!.length in 9..14) {
                        button_login.startAnimation {
                            if (isLocationGet == true) {

                                login(country?._id().toString(), country?.dial().toString())
                            } else {
                                getLocationOfDevice()
                            }
                        }
                    } else {
                        customToastMain("Please Enter Valid Phone Number", 0)
                    }

                } else {
                    runOnUiThread {
                        customToastMain("Please Enter Proper Phone Number", 0)
                    }

                }
            }
        }
        textSkipForNow.setOnClickListener {
            MyApplication.mainActivity.logoutUser()
            guestLogin()

        }
        imageViewFacebook.setOnClickListener {
            if (isLoggedIn) {
                LoginManager.getInstance().logOut()
            }
            Log.d(TAG, "Facebook clicked!!")
            LoginManager.getInstance().logInWithReadPermissions(
                this, Arrays.asList(
                    "email", "public_profile", "user_birthday"))

        }
    }

    private fun guestLogin() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        loginViewModel?.doGuestLogin()

        loginViewModel?.loginGuest?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        imageViewFacebook,
                        "Connection Error!",
                        Snackbar.LENGTH_SHORT).setAction("RETRY") {
                            loginViewModel?.doGuestLogin()
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.getGuestLogin -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)
                    if (it.response != null) {
                        Log.d("Fragemnt Login", "Data Sucess: ${it.response}")

                        val token = it.response.guestLogin().token()
                        val preferences: SharedPreferences = getSharedPreferences(
                            StringSingleton.sharedPrefFile,
                            Context.MODE_PRIVATE)
                        preferences.edit().putString("TOKEN", token).apply()
                        preferences.edit().putString("UserID", it.response.guestLogin().userId()).apply()
                        preferences.edit().putBoolean("ANONYMOUS", it.response.guestLogin().anonymous()!!).apply()
                        openActivity(BottomBarActivity::class.java)
                        finish()
                    } else {
                        runOnUiThread {
                            customToastMain("Please try again later", 0)
                        }
                    }

                }
            }

        })
    }

    private fun setFacebookData(loginResult: AccessToken) {
        val request = GraphRequest.newMeRequest(loginResult, object : GraphRequest.GraphJSONObjectCallback {
            override fun onCompleted(`object`: JSONObject?, response: GraphResponse) {
                // Application code
                try {
                    Log.i("ResponseData::::::::", "fb response:::$response")
                    var email = ""
                    var name = ""
                    var id = ""
                    if (response != null && response.jsonObject != null) {
                        if (response.jsonObject.has("email")) {
                            email = response.jsonObject.getString("email")
                        }
                        if (response.jsonObject.has("name")) {
                            name = response.jsonObject.getString("name")
                        }
                        if (response.jsonObject.has("id")) {
                            id = response.jsonObject.getString("id")
                        }
                    }

                    if (isLocationGet) {
                        ssoLogin("", id, email, "FB", name.toString(), "ANDROID", deviceToken)
                    } else {
                        getLocationOfDevice()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        })
        GraphRequest(AccessToken.getCurrentAccessToken(),
            "/me/permissions/",
            null,
            HttpMethod.DELETE,
            object : GraphRequest.Callback {
                override fun onCompleted(graphResponse: GraphResponse?) {
                    LoginManager.getInstance().logOut()
                }
            }).executeAsync()
        val parameters = Bundle()
        parameters.putString("fields", "id,name,email")
        request.parameters = parameters
        request.executeAsync()

    }

    private fun countryCode() {
        val zone: TimeZone = TimeZone.getDefault()
        val timezone: String = zone.id
        println("Display name for default time zone: $timezone")
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        loginViewModel?.getCountry(timezone.toString())

        loginViewModel?.country?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        imageViewFacebook,
                        "Connection Error!",
                        Snackbar.LENGTH_SHORT).setAction("RETRY") {
                            loginViewModel?.getCountry(timezone)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain("Please try again later", 0)

                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain("Please try again later", 0)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain("Please try again later", 0)
                }

                is NetworkResponse.SUCCESS.getCountry -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)
                    if (it.response != null) {
                        country = it.response.country()

                        runOnUiThread {
                            txtCountryCode.text = "+" + it.response.country()?.dial()
                            Log.d(
                                "tag",
                                "response.data?.country()?.ISO2():::${it.response.country()?.ISO2()!!.toLowerCase()}")
                            Glide.with(this).load(
                                Utils.FLAG_URL + (it.response.country()?.ISO2()!!.toLowerCase()) + ".png")
                                .into(imgFlag!!)
                        }

                    } else {

                        runOnUiThread {
                            customToastMain("Please try again later", 0)
                        }
                    }

                }
            }
        })
    }


    private fun login(countryId: String, countryCode: String) {

        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }


        loginViewModel?.doLogin(etPhoneNumber.text.toString(), countryId)

        loginViewModel?.login?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    button_login.revertAnimation()
                    val snackbar: Snackbar = Snackbar.make(button_login, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            button_login.startAnimation()
                            loginViewModel?.doLogin(etPhoneNumber.text.toString(), countryId)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    button_login.revertAnimation()
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    button_login.revertAnimation()
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.doLogin -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)
                    if (it.response.generateOtp().status()) {
                        //

                        runOnUiThread {
                            customToastMain("OTP sent successfully", 1)
                        }
                        val bundle: Bundle = bundleOf(
                            "mobileNo" to "+" + countryCode + " " + etPhoneNumber.text.toString(),
                            "simpleMobileNo" to etPhoneNumber.text.toString(),
                            "countryId" to countryId,
                            "countryCode" to countryCode,
                            "latitude" to latitude,
                            "longitude" to longitude,
                            "city" to city,
                            "state" to state,
                            "location" to location)
                        button_login.revertAnimation()
                        hideKeyboard(this)
                        openActivity(VerifyOtpActivity::class.java, bundle)


                    } else {

                        runOnUiThread {
                            button_login.revertAnimation()
                            customToastMain("Please try again later", 0)
                        }
                    }

                }
            }
        })

    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(
            signInIntent, RC_SIGN_IN)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        //        if (isLoggedIn) {
        //        }
        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult")

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }


    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.

            val googleEmail = account?.email ?: ""
            Log.d(TAG, "Google Email:$googleEmail")

            val googleId = account?.id ?: ""
            Log.d(TAG, "Google ID:$googleId")
            val name = account?.displayName
            Log.d(TAG, "name:$name")

            val googleFirstName = account?.givenName ?: ""
            Log.d(TAG, "Google First Name:$googleFirstName")

            val googleLastName = account?.familyName ?: ""
            Log.d(TAG, "Google Last Name:$googleLastName")

            val bundle: Bundle = bundleOf(
                "googleEmail" to googleEmail,
                "name" to name,
                "googleFirstName" to googleFirstName,
                "googleLastName" to googleLastName,
                "googleId" to googleId,
                "type" to "G",
                "countryId" to country?._id())

            Log.d("tag", "location get api call:::$isLocationGet")

            if (isLocationGet) {
                ssoGLogin("", googleId, googleEmail, "G", name.toString(), "ANDROID", deviceToken)
            } else {
                getLocationOfDevice()
            }

        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            e.printStackTrace()
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            Log.w(TAG, "signInResult:failed code=" + e.message)
            //            updateUI(null)
        }
    }


    private fun ssoLogin(mobile: String,
        id: String,
        email: String,
        type: String,
        name: String,
        deviceType: String,
        deviceToken: String) {
        Log.d("tag", "sso: $mobile")
        Log.d("tag", "sso: $id")
        Log.d("tag", "sso: $email")
        Log.d("tag", "sso: $type")
        Log.d("tag", "sso: $name")
        loginViewModel?.ssoLogin(
            mobile, id, email, type, name, deviceType, deviceToken, latitude, longitude, city, state, location)

        loginViewModel?.sso?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(etPhoneNumber, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            loginViewModel?.ssoLogin(
                                mobile,
                                id,
                                email,
                                type,
                                name,
                                deviceType,
                                deviceToken,
                                latitude,
                                longitude,
                                city,
                                state,
                                location)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {

                }

                is NetworkResponse.ERROR_FORBIDDEN -> {

                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.ssoLogin -> {

                    //                    preferences.edit().putString("TOKEN", token).apply()

                    Log.d("tag", "email::::${it.response.sso().email()}")
                    Log.d("tag", "username::::${it.response.sso().username()}")
                    Log.d("tag", "sso: ${it.response}")
                    val token = it.response.sso().token()
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)

                    if (it.response.sso().username().toString().isEmpty() || it.response.sso().username() == null) {
                        preferences.edit().putString("TOKEN", token).apply()
                        var bundle = Bundle()
                        if (it.response.sso().email().toString().isNotEmpty()) {
                            bundle = bundleOf(
                                "email" to it.response.sso().email().toString(),
                                "city" to city,
                                "state" to state,
                                "location" to location)
                        } else {
                            bundle = bundleOf("email" to null)
                        }
                        openActivity(SignUpDetailsActivity::class.java, bundle)
                        finishAffinity()
                    } else {
                        Log.d("tag", "getUserDetails userId::::${it.response.sso().userId()}")
                        Log.d("tag", "getUserDetails token::::${it.response.sso().token()}")
                        getUserDetails(it.response.sso().userId(), it.response.sso().token().toString())
                    }
                }
            }
        })

        loginViewModel?.ssoLogin(
            mobile, id, email, type, name, deviceType, deviceToken, latitude, longitude, city, state, location)

    }

    private fun ssoGLogin(mobile: String,
        id: String,
        email: String,
        type: String,
        name: String,
        deviceType: String,
        deviceToken: String) {
        Log.d("tag", "sso: $mobile")
        Log.d("tag", "sso: $id")
        Log.d("tag", "sso: $email")
        Log.d("tag", "sso: $type")
        Log.d("tag", "sso: $name")
        loginViewModel?.ssoLogin(
            mobile,
            id,
            email,
            type,
            name,
            deviceType,
            deviceToken,
            latitude,
            longitude,
            city,
            state,
            location)


        loginViewModel?.sso1?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(button_login, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            loginViewModel?.ssoLogin(
                                mobile,
                                id,
                                email,
                                type,
                                name,
                                deviceType,
                                deviceToken,
                                latitude,
                                longitude,
                                city,
                                state,
                                location)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {

                }

                is NetworkResponse.ERROR_FORBIDDEN -> {

                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.ssoLogin -> {

                    val token = it.response.sso().token()
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)

                    if (it.response.sso().username().toString().isEmpty() || it.response.sso().username() == null) {
                        preferences.edit().putString("TOKEN", token).apply()
                        var bundle = Bundle()
                        if (it.response.sso().email().toString().isNotEmpty()) {
                            bundle = bundleOf(
                                "email" to it.response.sso().email().toString(),
                                "city" to city,
                                "state" to state,
                                "location" to location)
                        } else {
                            bundle = bundleOf("email" to null)

                        }
                        openActivity(SignUpDetailsActivity::class.java, bundle)
                        finishAffinity()

                    } else {
                        Log.d("tag", "getUserDetails userId::::${it.response.sso().userId()}")
                        Log.d("tag", "getUserDetails token::::${it.response.sso().token()}")
                        getUserDetails(
                            it.response.sso().userId(), it.response.sso().token().toString())

                    }

                }
            }
        })


    }


    private fun getUserDetails(id: String, token: String) {

        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        loginViewModel?.getUserDetails(id, token)

        loginViewModel?.userDetails?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(button_login, "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            loginViewModel?.getUserDetails(id, token)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.getEditUserDetails -> {

                    Log.d("tag", "response::" + it.response)
                    Log.d("tag", "response: image:" + it.response.user().image())
                    Log.d("tag", "response: colorcode:" + it.response.user().colorCode())
                    val preferences: SharedPreferences = getSharedPreferences(
                        StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)

                    preferences.edit().putString("TOKEN", token).apply()

                    if (it.response.user().status().toString() == "BLOCK") {
                        runOnUiThread {
                            customToastMain(getString(R.string.you_cannot_use_app), 0)
                        }
                    } else if (it.response.user().username().toString().isEmpty() || it.response.user()
                            .username() == null || it.response.user().username().toString() == "null"
                    ) {

                        val bundle = bundleOf(
                            "email" to it.response.user().email().toString(),
                            "city" to city,
                            "state" to state,
                            "location" to location)
                        openActivity(SignUpDetailsActivity::class.java, bundle)
                        finishAffinity()
                    } else if (it.response.user().colorCode().toString().isEmpty() || it.response.user().image()
                            .toString().isEmpty()
                    ) {
                        val gson = Gson()

                        // Get java object list json format string.
                        val userInfoListJsonString = gson.toJson(it.response.user())

                        preferences.edit().putString("USER", userInfoListJsonString).apply()
                        preferences.edit().remove("imageSaved").apply()
                        preferences.edit().remove("colorCodeSaved").apply()
                        openActivity(SetYourProfileActivity::class.java)
                        finish()
                    } else if (it.response.user().agreementAccepted() == false) {
                        openActivity(FragmentTermsCondition::class.java)
                        finish()
                    } else {
                            val gson = Gson()
                            val userInfoListJsonString = gson.toJson(it.response.user())
                            preferences.edit().putString("USER",userInfoListJsonString).apply()
                            openActivity(BottomBarActivity::class.java)
                            finish()
                    }
                }
            }
        })

    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
        imgLogo.setImageDrawable(
            ThemeManager.image(
                this, StringSingleton.mainLogo))

        main_layout.setBackgroundColor(
            ThemeManager.colors(
                this, StringSingleton.bodyBackground))
        etPhoneNumber.background = ThemeManager.getDrawable(
            this, ThemeManager.colors(
                this, StringSingleton.textWhite), R.dimen.mobile_edit_box_corner_radius)


        terms_condition_1.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textBlackHeader))
        terms_condition_2.setTextColor(
            ThemeManager.colors(
                this, StringSingleton.textBlackHeader))
    }


    private fun defaultColor(context: Context) = ContextCompat.getColor(context, android.R.color.white)


    fun ProgressButton.morphDoneAndRevert(context: Context,
        fillColor: Int = defaultColor(context),
        bitmap: Bitmap,
        doneTime: Long = 3000,
        revertTime: Long = 4000) {
        progressType = ProgressType.INDETERMINATE
        startAnimation()
        Handler().run {
            postDelayed({ doneLoadingAnimation(fillColor, bitmap) }, doneTime)
            postDelayed(::revertAnimation, revertTime)
        }
    }

    private fun ProgressButton.morphAndRevert(revertTime: Long = 3000, startAnimationCallback: () -> Unit = {}) {
        startAnimation(startAnimationCallback)
        Handler().postDelayed(::revertAnimation, revertTime)
    }

    private fun ProgressButton.morphStopRevert(stopTime: Long = 1000, revertTime: Long = 2000) {
        startAnimation()
        Handler().postDelayed(::stopAnimation, stopTime)
        Handler().postDelayed(::revertAnimation, revertTime)
    }

    private fun progressAnimator(progressButton: ProgressButton) = ValueAnimator.ofFloat(0F, 100F).apply {
        duration = 1500
        startDelay = 500
        addUpdateListener { animation ->
            progressButton.setProgress(animation.animatedValue as Float)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        button_login.dispose()
    }
}