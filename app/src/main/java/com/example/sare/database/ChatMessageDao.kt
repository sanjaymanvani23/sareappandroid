package com.example.sare.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ChatMessageDao {

    @Query("SELECT * from chat_message")
    fun getAllChatMessages(): LiveData<List<ChatMessage>>

    @Query("SELECT * from chat_message WHERE sender_id = :senderId AND receiver_id = :receiverId")
    fun getChatMessages(senderId: String, receiverId: String): LiveData<List<ChatMessage>>

    @Query("SELECT * from chat_message WHERE message_id = :messageId")
    fun getChatMessageById(messageId: String): List<ChatMessage>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(chatMessage: ChatMessage)

    @Query("DELETE FROM chat_message")
    suspend fun deleteAll()
}