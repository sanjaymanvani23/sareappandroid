package com.example.sare.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = arrayOf(ChatMessage::class, ChatUser::class), version = 2, exportSchema = false)
@TypeConverters(Converters::class)
public abstract class SareRoomDatabase : RoomDatabase() {

    abstract fun chatMessageDao(): ChatMessageDao
    abstract fun chatUserDao(): ChatUserDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: SareRoomDatabase? = null

        fun getDatabase(context: Context): SareRoomDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    SareRoomDatabase::class.java,
                    "sare_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}