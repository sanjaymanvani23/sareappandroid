package com.example.sare.database
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "chat_user")
data class ChatUser(
    @ColumnInfo(name = "user_id")
    val userId: String,
    @ColumnInfo(name = "message_content")
    val messageContent: String,
    @ColumnInfo(name = "message_id")
    val messageId: String,
    @ColumnInfo(name = "user_image")
    val userImage: String,
    @ColumnInfo(name = "user_name")
    val userName: String,
    @ColumnInfo(name = "uesr_color_code")
    val userColorCode: String,
    @ColumnInfo(name = "date_time")
    val dateTime: Date = Date(),
    @ColumnInfo(name = "is_blocked")
    val blocked: Boolean
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}