package com.example.sare.database
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "chat_message")
data class ChatMessage(
    @ColumnInfo(name = "sender_id")
    val senderId: String,
    @ColumnInfo(name = "receiver_id")
    val receiverId: String,
    @ColumnInfo(name = "message_content")
    val messageContent: String,
    @ColumnInfo(name = "message_id")
    val messageId: String,
    @ColumnInfo(name = "date_time")
    val dateTime: Date = Date()
) {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long = 0
}