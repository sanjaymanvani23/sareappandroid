package com.example.sare.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ChatUserDao {

    @Query("SELECT * from chat_user")
    fun getChatUser(): LiveData<List<ChatUser>>

    @Query("SELECT * from chat_user WHERE user_id = :userId")
    fun getChatUserByUserId(userId: String): ChatUser

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(chatUser: ChatUser)

    @Query("DELETE FROM chat_user")
    suspend fun deleteAll()
}