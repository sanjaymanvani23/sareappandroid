package com.example.sare.dashboard.viewmodel

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class TagsViewModel : ViewModel() {
    var accountsArrayList = MutableLiveData<NetworkResponse>()
    var trendingTagsList = MutableLiveData<NetworkResponse>()
    var tagsList = MutableLiveData<NetworkResponse>()
    var exploreTagsList = MutableLiveData<NetworkResponse>()

    fun getTotalUsersList(pageCount: Int, context: Context) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UsersOnSareVidsQuery(pageCount, Utils.PAGE_SIZE, "createdAt", "", ""))
            .enqueue(object : ApolloCall.Callback<UsersOnSareVidsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    accountsArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UsersOnSareVidsQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.users() != null) {
                                accountsArrayList.postValue(
                                    NetworkResponse.SUCCESS.getUsersOnSareVids(
                                        response.data()?.users()!!.user()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(),
                                    errorCode.toString()
                                )
                                accountsArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0
                                        )?.message.toString()
                                    )
                                )
                            }
                        } catch (e: Exception) {
                            accountsArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getTagsList(search: String, context: Context) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(TagSearchListQuery(search))
            .enqueue(object : ApolloCall.Callback<TagSearchListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    tagsList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<TagSearchListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.tags() != null) {
                                tagsList.postValue(
                                    NetworkResponse.SUCCESS.getTagsList(
                                        response.data()?.tags()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(),
                                    errorCode.toString()
                                )
                                tagsList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0
                                        )?.message.toString()
                                    )
                                )
                            }
                        } catch (e: Exception) {
                            tagsList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getTrendingTags(context: Context, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(TrendingTagsAndProfileQuery(pageCount, Utils.PAGE_SIZE))
            .enqueue(object : ApolloCall.Callback<TrendingTagsAndProfileQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "error:::" + e.message)

                    trendingTagsList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<TrendingTagsAndProfileQuery.Data>) {
                    runBlocking {
                        Log.d("tag", "response:::" + response)
                        try {
                            if (response.data?.trending() != null) {
                                trendingTagsList.postValue(
                                    NetworkResponse.SUCCESS.getTrendingTagsList(
                                        response.data()?.trending()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(),
                                    errorCode.toString()
                                )
                                trendingTagsList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0
                                        )?.message.toString()
                                    )
                                )
                            }
                        } catch (e: Exception) {
                            trendingTagsList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getExploreVideoList(userId: String, type: String, context: Context, tagName: MutableList<String?>) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        Log.d("tag", "userId::$userId")
//        var tag = mutableListOf("#roleit")
        apolloClient?.query(ExploreTagsListQuery(tagName, userId, type,"createdAt"))
            //apolloClient?.query(VideoListQuery(categoryList, "5f23d12c6188ec01fcafd61a"))
            ?.enqueue(object : ApolloCall.Callback<ExploreTagsListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    exploreTagsList.postValue(NetworkResponse.ERROR(e))
                    Log.d("Video", "Data Failure : ${e.message}")

                }

                override fun onResponse(response: Response<ExploreTagsListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.videos() != null) {
                                exploreTagsList.postValue(
                                    NetworkResponse.SUCCESS.getExploreTagsList(
                                        response.data()?.videos()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(),
                                    errorCode.toString()
                                )
                                exploreTagsList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0
                                        )?.message.toString()
                                    )
                                )

                            }
                        } catch (e: Exception) {
                            exploreTagsList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}