package com.example.sare.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.UserVideosQuery
import com.example.sare.VideoListQuery

class VideoPlayerRecyclerAdapter(
    private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var mediaObjects: ArrayList<VideoListQuery.Video> = ArrayList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder = VideoPlayerViewHolder(
        LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_video_list_item, viewGroup, false)
    )


    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as VideoPlayerViewHolder).onBind(mediaObjects[i], context)
    }

    fun setVideoListData(mMediaObjects: ArrayList<VideoListQuery.Video>){
        this.mediaObjects = mMediaObjects
        VideoPlayerRecyclerAdapter(context).notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
            return mediaObjects.size
    }

    public  fun filter(userId:String)
    {
        for(i in mediaObjects.indices)
        {
            if(userId== mediaObjects[i]._id())
            {
                var user = VideoListQuery.User(mediaObjects[i].user()!!.__typename(),mediaObjects[i].user()?.postsCounts(),mediaObjects[i].user()?.username(),mediaObjects[i].user()?.name(),mediaObjects[i].user()!!._id(),true,mediaObjects[i].user()?.colorCode(),mediaObjects[i].user()?.image())
                var varrr= VideoListQuery.Video( mediaObjects[i]?.userId()!!,mediaObjects[i].__typename(),mediaObjects[i].title(),mediaObjects[i]._id(),mediaObjects[i].audioId(),mediaObjects[i].likeCounts(),mediaObjects[i].views(),mediaObjects[i].liked(),mediaObjects[i].name(),mediaObjects[i].imageName(),mediaObjects[i].commentCounts(),mediaObjects[i].description(),mediaObjects[i].bookmarked(),mediaObjects[i].audio(),user)
                mediaObjects[i]=varrr
            }
        }

    }


}