package com.example.sare.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.extensions.setBorder
import com.example.sare.loadDiamondImage
import kotlinx.android.synthetic.main.item_use_music.view.*

class TagsVideoAdapter(val list: List<String>, private val context: Context) :
    RecyclerView.Adapter<TagsVideoAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_tags_video, parent, false)
        return ViewHolder(
            v
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(list: String, context: Context) {
            var imgAudio = itemView.imgAudio
            var imgProfile = itemView.imgProfile
            var imgProfileBorder = itemView.imgProfileBorder

            Glide.with(context)
                .load(R.drawable.img_dummy_audio)
                .error(R.drawable.icon_video_play)
                .into(imgAudio)


            Glide.with(context)
                .load(R.drawable.girl_image)
                .error(R.drawable.icon_video_play)
                .into(imgProfile)

            imgProfileBorder?.setBorder("#FFFF1F")


        }
    }
}