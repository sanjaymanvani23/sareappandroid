package com.example.sare.dashboard.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.TrendingTagsAndProfileQuery
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.extensions.setBorder
import kotlinx.android.synthetic.main.item_tags_video.view.*

class TagsMainListAdapter(
    var list: MutableList<TrendingTagsAndProfileQuery.Video>,
    private val context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_tags_video, parent, false)
        return ViewHolderTags(v)

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderTags).bindItems(
            list[position], context
        )

    }

    override fun getItemCount(): Int {
        return list.size
    }



    class ViewHolderTags(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: TrendingTagsAndProfileQuery.Video, context: Context) {
            var imgAudio = itemView.imgAudio
            var imgProfile = itemView.imgProfile
            var imgProfileBorder = itemView.imgProfileBorder
            var txtTitle = itemView.txtTitle
            var txtViewCount = itemView.txtViewCount
            var txtLikeCount = itemView.txtLikeCount
         /*   Glide.with(context)
                .load(PUBLIC_URL+item.imageName())
                .error(R.drawable.video_default)
                .into(imgAudio)
            Glide.with(context)
                .load(PUBLIC_URL+item.user()?.image())
                .error(R.drawable.profile_default)
                .into(imgProfile)
            txtTitle.text = item.title().toString()
            txtLikeCount.text = item.likeCounts().toString()
            txtViewCount.text = item.views().toString()
            imgProfileBorder?.setBorder(item.user()?.colorCode().toString())*/


        }
    }
}