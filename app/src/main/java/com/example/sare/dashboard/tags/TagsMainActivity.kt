package com.example.sare.dashboard.tags

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sare.R
import com.example.sare.dashboard.TagsProfileAdapter
import com.example.sare.dashboard.TagsVideoAdapter
import kotlinx.android.synthetic.main.activity_tags_main.*
import kotlinx.android.synthetic.main.fragement_tags.view.*

class TagsMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tags_main)

        setDummyData()

        type_all?.setOnClickListener {
            onBackPressed()
        }
        txtFollowing?.setOnClickListener {
           onBackPressed()
        }
    }

    private fun setDummyData() {

        val list = mutableListOf<String>()
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        list.add("Test")
        rv_tag_1?.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = TagsVideoAdapter(
                list,
                context
            )
        }
    }
}