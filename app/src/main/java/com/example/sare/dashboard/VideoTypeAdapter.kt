package com.example.sare.dashboard

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CategoryListQuery
import com.example.sare.R
import kotlinx.android.synthetic.main.vide_type_list.view.*

class VideoTypeAdapter(private val data : List<CategoryListQuery.Category>,private val context : Context) : RecyclerView.Adapter<VideoTypeAdapter.ViewHolder>(){

    public var currentPosition : Int = -1
    private var onClickListener: OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.vide_type_list, parent, false))

    override fun getItemCount(): Int {
        return data.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val categoryName = data[position].categoryName()
        holder.type.text = categoryName?.substring(0,1)?.toUpperCase() + categoryName?.substring(1)
        //holder.type.setTextColor(context.resources.getColor(R.color.pureWhite, context.theme))
        holder.type.setTextColor(Color.parseColor("#99FFFFFF"))

        //holder.divider.visibility = GONE

        if (currentPosition != -1 && currentPosition == position){

            //holder.type.setTextColor(context.resources.getColor(R.color.pureBlack, context.theme))
            holder.type.setTextColor(Color.parseColor("#FFFFFF"))
            //holder.divider.visibility = VISIBLE

        }

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var type = itemView.type
        //var divider = itemView.divider

        init {
            type.setOnClickListener {
                currentPosition = adapterPosition
                onClickListener?.onClick(adapterPosition, data[adapterPosition].categoryName())
                notifyDataSetChanged()
            }
        }

    }

    public fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    interface OnClickListener {
        fun onClick(position: Int, value: String?)
    }
}
