package com.example.sare.dashboard

import android.util.SparseArray
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.sare.VideoListQuery
import com.google.gson.Gson

class StoriesPagerAdapter(fragment: Fragment, val dataList: ArrayList<VideoListQuery.Video>) : FragmentStateAdapter(
    fragment
) {

    private val mFragmentsHolded: SparseArray<StoryViewFragment> = SparseArray<StoryViewFragment>()

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun createFragment(position: Int): Fragment {

        val gson = Gson()

        val userInfoListJsonString = gson.toJson(dataList[position])

        var fragment =StoryViewFragment.newInstance(userInfoListJsonString)

        mFragmentsHolded.append(position, fragment as StoryViewFragment?)

        return fragment
    }
    fun getCachedItem(position: Int): StoryViewFragment? {
        return mFragmentsHolded[position, null]
    }
}