package com.example.sare.dashboard.model

import android.os.Environment
import java.io.File

class DummyData(val title: String, val url: String) {
    var APP_PATH_SD_CARD = "/QRCODE/"
    var fullPath =
        Environment.getExternalStorageDirectory().absolutePath + "/" + Environment.DIRECTORY_DOWNLOADS

    // Environment.getExternalStorageDirectory().absolutePath + APP_PATH_SD_CARD
    val file: File
        get() = File(fullPath, "$title.mp4")

}

