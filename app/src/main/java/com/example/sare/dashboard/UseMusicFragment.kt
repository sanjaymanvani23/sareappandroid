package com.example.sare.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager

import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import com.example.sare.extensions.customToast
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.openNewActivity
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.ui.login.LoginActivity

import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_use_music.view.*
import kotlinx.android.synthetic.main.fragment_use_music.view.main_layout

class UseMusicFragment : Fragment(), ItemOnclickListener {

    private var root: View? = null
    var listMusic: ArrayList<VideoAudioListQuery.Video>? = null
    var audioId: String = ""
    var videoListViewModel: VideoListViewModel? = null
    var progressDialog: CustomProgressDialogNew? = null
    private val audios: MutableList<VideoAudioListQuery.Video> = mutableListOf<VideoAudioListQuery.Video>()
    private var profileUserVideoList: MutableList<VideoListQuery.Video> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_use_music, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@UseMusicFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback
        )

        audioId = arguments?.get("audioId").toString()
        videoListViewModel = ViewModelProvider(this)[VideoListViewModel::class.java]

        Log.d("tag", "audioId::::$audioId")
        initUseMusicRecyclerView()
        setObservers()


        audioList()
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        return root
    }

    private fun setObservers() {
        audioListObserver()
    }

    private fun audioList() {
        progressDialog = CustomProgressDialogNew(requireActivity())
        videoListViewModel?.getVideoAudioList(audioId)
    }
    private fun audioListObserver() {

        videoListViewModel?.videoAudioListArrayList?.observe(viewLifecycleOwner, Observer {
            progressDialog?.hide()

            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog?.hide()

                    it.error.printStackTrace()
                    root?.main_layout?.visibility = View.VISIBLE
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getVideoAudioList(audioId)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(),0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(),0)
                }

                is NetworkResponse.SUCCESS.videoAudioList -> {

                    root?.txtId?.text = it.response.user()?.username()

                    root?.imgFrameColor?.loadUrl(Utils.PUBLIC_URL + it.response.user()?.image().toString() )

                    audios.addAll(it.response.videos()!!)

                    val gson = Gson()
                    val userInfoListJsonString = gson.toJson(it.response.videos())
                    val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                    val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(userInfoListJsonString, myType)

                    profileUserVideoList.addAll(userVideoList)

                    root?.txtVideosCount?.text = audios.size.toString() + " Videos"

                    root?.rvUseMusic?.adapter?.notifyDataSetChanged()

                    if (root?.rvUseMusic?.adapter?.itemCount != 0) {
                        root?.rvUseMusic?.visibility = View.VISIBLE
                        root?.txtNoData?.visibility = View.GONE
                    } else {
                        root?.rvUseMusic?.visibility = View.GONE
                        root?.txtNoData?.visibility = View.VISIBLE
                    }
                }

                is NetworkResponse.ERROR_RESPONSE_NEW -> {
                    progressDialog?.hide()
                    customToast("Something went wrong.!", requireActivity(), 0)
                }

            }


        })
    }
    override fun onItemClickListener(position: Int) {
        if (findNavController().currentDestination?.id == R.id.useMusicFragment) {
            val gson = Gson()
            val userInfoListJsonString = gson.toJson(profileUserVideoList)
            var bundle = bundleOf("navigationFlagProfile" to "UserMusic" , "videoList" to userInfoListJsonString ,"position" to position, "isVideoCurrentPage" to 1)
            findNavController().navigate(R.id.action_profileUserFragment_to_UserVideoFragment, bundle)
        }
    }
    private fun initUseMusicRecyclerView() {

        root?.rvUseMusic?.apply {
            layoutManager = GridLayoutManager(
                root?.rvUseMusic?.context,
                3
            )
            adapter = UseMusicAdapter(
                audios,
                context,this@UseMusicFragment
            )

           /* if (adapter?.itemCount != 0){
                root?.rvUseMusic?.visibility = View.VISIBLE
                root?.txtNoData?.visibility = View.GONE
            }else{
                root?.rvUseMusic?.visibility = View.GONE
                root?.txtNoData?.visibility = View.VISIBLE
            }*/

        }


    }

}