package com.example.sare.dashboard.tags

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.adapter.AccountsAdapter
import com.example.sare.dashboard.adapter.TagsListAdapter
import com.example.sare.dashboard.viewmodel.TagsViewModel
import com.example.sare.extensions.customToast
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import com.example.sare.profile.fragment.ProfileGuestFragment
import com.example.sare.profile.fragment.ProfileUserFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_search_account_and_tags.*
import org.json.JSONObject


@RequiresApi(Build.VERSION_CODES.M) class SearchAccountAndTagsActivity : AppCompatActivity() {
    var viewModel: TagsViewModel? = null
    private var accountsList: MutableList<UsersOnSareVidsQuery.User> = mutableListOf()
    var filterList: List<UsersOnSareVidsQuery.User> = ArrayList()
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    var userId: String? = ""
    var clicked: String? = null
    var pos: Int = 0
    var search: String? = ""
    private lateinit var accountsAdapter: AccountsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_account_and_tags)
        viewModel = ViewModelProvider(this)[TagsViewModel::class.java]
        getSharedPreferences()
        setTheme()
        initRecyclerView()
        setObservers()
        setListeners()
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.textWhite)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, "#FFFFFF")
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }


    fun goToProfileUserFragment(bundle: Bundle) {
        val transaction = this.supportFragmentManager.beginTransaction()
        val frag2 = ProfileUserFragment()
        frag2.arguments = bundle
        transaction.add(R.id.relMain, frag2)
        transaction.addToBackStack(null)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        transaction.commit()
    }

    fun goToProfileGuestFragment(bundle: Bundle) {
        val transaction = this.supportFragmentManager.beginTransaction()
        val frag2 = ProfileGuestFragment()
        frag2.arguments = bundle
        transaction.add(R.id.relMain, frag2)
        transaction.addToBackStack(null)
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        transaction.commit()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        userId = mainObject.getString("_id")
        Log.d("tag", "userId:::$userId")

    }

    private fun setListeners() {
        txtNoDataTags?.visible()

        imgBack.setOnClickListener {
            onBackPressed()
        }
        imgClose.setOnClickListener {
            etSearchUser.setText(null)
        }
        etSearchUser.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isNotEmpty()) {
                    imgClose.visible()
                    txtNoDataTags?.gone()
                    filter(s.toString())
                }
            }
        })


        button_tags.setOnClickListener {
            clicked = "1"
            button_tags.setTextColor(
                resources.getColor(
                    R.color.textColor, theme))
            button_accounts.setTextColor(
                resources.getColor(
                    R.color.black_50, theme))
            recyclerView?.gone()
            recyclerViewTags?.visible()
            txtNoDataTags?.visible()
            txtNoData?.gone()

            button_tags.setBackgroundResource(R.drawable.button_tab_background);
            button_accounts.setBackgroundResource(0)
            Log.d("tag", "pos::1::$pos")
            if (pos == 0 || pos == 2) {
                pos = 2
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_left);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.addAnimation(leftSwipe);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_accounts.startAnimation(set)
            } else if (pos == 1) {
                pos = 2
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_left);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.addAnimation(leftSwipe);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_accounts.startAnimation(set)
            } else {
                pos = 1
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_right);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_accounts.startAnimation(set)
            }
        }

        button_accounts.setOnClickListener {
            clicked = "2"
            accountsList.clear()
            isLoading = false
            isLastPage = false
            currentPage = 0
            getAccountsList()

            button_accounts.setTextColor(
                resources.getColor(
                    R.color.textColor, theme))
            button_tags.setTextColor(
                resources.getColor(
                    R.color.black_50, theme))
            button_accounts.setBackgroundResource(R.drawable.button_tab_background);
            button_tags.setBackgroundResource(0)
            if (pos == 0 || pos == 1) {
                pos = 1
                val rightSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_tags.startAnimation(set)

            } else if (pos == 2) {
                pos = 1
                val rightSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_tags.startAnimation(set)
            } else {
                pos = 2
                val leftSwipe = AnimationUtils.loadAnimation(this, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val set = AnimationSet(true)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_tags.startAnimation(set)
            }
        }
    }

    private fun setObservers() {
        observerAccountsList()
    }

    private fun initRecyclerView() {
        initAccountsRecyclerTagsList()
    }

    private fun initAccountsRecyclerTagsList() {
//        recyclerView.itemAnimator = DefaultItemAnimator()

        recyclerView.apply {
            layoutManager = LinearLayoutManager(
                recyclerView.context, RecyclerView.VERTICAL, false

            )

            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getAccountsList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val layoutManager1 = layoutManager as LinearLayoutManager
                    val firstVisibleItemPosition = layoutManager1!!.findFirstCompletelyVisibleItemPosition()
                    Log.e("Test Y Value", "$dy : $firstVisibleItemPosition")
                    if (dy > 0) {
                        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(windowToken, 0)

                        ll_options1?.gone()
                    } else {
                        if (firstVisibleItemPosition == 0) {
//                            ll_options1?.visible()
                        }
                    }
                }
            })
            adapter = AccountsAdapter(
                accountsList, context, this@SearchAccountAndTagsActivity, recyclerView, userId.toString())
        }
    }

    private fun filter(text: String) {
        val filteredNames: ArrayList<UsersOnSareVidsQuery.User> = ArrayList()
        if (clicked == "2") {
            recyclerViewTags.gone()
            txtNoDataTags.gone()
            if (accountsList.isNotEmpty()) {
                for (s in accountsList) {
                    if (s.name()?.toLowerCase()!!.contains(text.toLowerCase())) {
                        filteredNames.add(s)
                    }
                }
            }
            recyclerView.apply {
                layoutManager = LinearLayoutManager(
                    recyclerView.context, RecyclerView.VERTICAL, false)
                adapter = AccountsAdapter(
                    accountsList, context, this@SearchAccountAndTagsActivity, recyclerView, userId.toString())
                filterList = (adapter as AccountsAdapter).filterList(filteredNames)!!
                if (recyclerView?.adapter?.itemCount == 0) {
                    recyclerView?.gone()
                    recyclerViewTags?.gone()
                    txtNoData?.visible()
                    txtNoDataTags?.gone()
                } else {
                    recyclerView?.visible()
                    txtNoData?.gone()
                    recyclerViewTags?.gone()
                    txtNoDataTags?.gone()
                }
            }
        } else {
            recyclerView.gone()
            txtNoData.gone()
            observerTagsList()
            getTagsListList(text)
        }


    }

    private fun initTagsListRecyclerview(list: List<TagSearchListQuery.Tag>) {
        recyclerViewTags?.apply {
            layoutManager = LinearLayoutManager(
                recyclerView.context, RecyclerView.VERTICAL, false)
            adapter = TagsListAdapter(
                list, context, this@SearchAccountAndTagsActivity)
            recyclerViewTags.setOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    val layoutManager1 = layoutManager as LinearLayoutManager

                    val firstVisibleItemPosition = layoutManager1!!.findFirstCompletelyVisibleItemPosition()
                    Log.e("Test Y Value", "$dy : $firstVisibleItemPosition")
                    if (dy > 0) {
                        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(windowToken, 0)

                        ll_options1?.gone()
                    } else {
                        if (firstVisibleItemPosition == 0) {
//                            ll_options1?.visible()
                        }
                    }
                }
            })
        }

    }

    private fun getAccountsList() {
        viewModel?.getTotalUsersList(
            currentPage * Utils.PAGE_SIZE, this)
    }

    fun observerAccountsList() {
        viewModel?.accountsArrayList?.observe(this, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relMain), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                            viewModel?.getTotalUsersList(
                                currentPage * Utils.PAGE_SIZE, this)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    recyclerView?.gone()
                    txtNoData?.gone()
                }

                is NetworkResponse.SUCCESS.getUsersOnSareVids -> {
                    Log.d("tag", "response::followings" + it.response)
                    runOnUiThread {
                        Log.d("tag", "response published::" + it.response)
                        isLoading = false
                        Log.d("tag", "response::" + it.response)
                        if (it.response != null) {
                            accountsList.addAll(it.response)
                            Log.d("tag", "accountsArrayList::::SIZE::::${accountsList.size}")
                            recyclerView?.adapter?.notifyDataSetChanged()
                            if (it.response.size < Utils.PAGE_SIZE) {
                                isLastPage = true
                            }
                            if (recyclerView?.adapter?.itemCount == 0) {
                                recyclerView?.gone()
                                recyclerViewTags?.gone()
                                txtNoData?.visible()
                                txtNoDataTags?.gone()
                            } else {
                                recyclerView?.visible()
                                txtNoData?.gone()
                                recyclerViewTags?.gone()
                                txtNoDataTags?.gone()
                            }
                        } else {
                            runOnUiThread {
                                customToast(
                                    getString(R.string.api_error_message), this, 0)

                            }
                        }
                    }
                }
            }
        })
    }

    private fun getTagsListList(search: String) {
        viewModel?.getTagsList(
            search.toString(), this)
    }

    fun observerTagsList() {
        viewModel?.tagsList?.observe(this, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relMain), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                            viewModel?.getTagsList(
                                search.toString(), this)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    recyclerView?.gone()
                    txtNoData?.gone()
                }

                is NetworkResponse.SUCCESS.getTagsList -> {
                    runOnUiThread {
                        Log.d("tag", "response::" + it.response)
                        //                        txtNoDataTags?.visible()
                        initTagsListRecyclerview(it.response)
                        if (recyclerViewTags?.adapter?.itemCount == 0) {
                            recyclerView?.gone()
                            recyclerViewTags?.gone()
                            txtNoData?.gone()
                            txtNoDataTags?.visible()
                        } else {
                            recyclerViewTags?.visible()
                            txtNoData?.gone()
                            txtNoDataTags?.gone()
                            recyclerView?.gone()
                        }
                    }
                }
            }
        })
    }
}