package com.example.sare.dashboard

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import kotlinx.android.synthetic.main.activity_user_videos.*

@RequiresApi(Build.VERSION_CODES.M) class UserVideosActivity : AppCompatActivity() {
    lateinit var fragment: Fragment
    var bundle = Bundle()

    override fun onResume() {
        super.onResume()
        fragment = UserVideoFragment()
        var videoIdList = intent?.getStringExtra("videoList")
        var navigationFlagProfile = intent?.getStringExtra("navigationFlagProfile")

        var position = intent?.getIntExtra("position", 0)!!
        var isVideoCurrentPage = intent?.getIntExtra("isVideoCurrentPage", 0)!!

        bundle = bundleOf(
            "navigationFlagProfile" to navigationFlagProfile,
            "videoList" to videoIdList,
            "position" to position,
            "isVideoCurrentPage" to isVideoCurrentPage)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_videos)
        setTheme()
        fragment = VideoFragment()
        var videoIdList = intent?.getStringExtra("videoList")
        var navigationFlagProfile = intent?.getStringExtra("navigationFlagProfile")
        var position = intent?.getIntExtra("position", 0)!!
        var isVideoCurrentPage = intent?.getIntExtra("isVideoCurrentPage", 0)!!

         bundle = bundleOf(
            "navigationFlagProfile" to navigationFlagProfile,
            "videoList" to videoIdList,
            "position" to position,
            "isVideoCurrentPage" to isVideoCurrentPage)
        Log.d("tag", "bundle:::$bundle")
        replaceFragment(fragment, bundle)
    }

    fun replaceFragment(f: Fragment, bundle: Bundle?) {
        fragment = f
        f.arguments = bundle
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.relUserVideos, f)

        if (f is VideoFragment) {
            for (i in 1 until supportFragmentManager.backStackEntryCount) {
                supportFragmentManager.popBackStack()
            }
        } else {
            ft.addToBackStack(f.toString())
        }
        ft.commit()
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }
}