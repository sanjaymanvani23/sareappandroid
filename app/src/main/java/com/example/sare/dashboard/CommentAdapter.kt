package com.example.sare.dashboard

//import kotlinx.android.synthetic.main.layout_reply_back.view.*
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.*
import com.example.sare.profile.activity.ProfileGuestActivity
import com.example.sare.profile.fragment.ProfileUserFragment
import com.example.sare.ui.login.LoginActivity
import kotlinx.android.synthetic.main.item_comment.view.*
import kotlinx.android.synthetic.main.item_comment_reply.view.*
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.*
import org.json.JSONObject
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CommentAdapter(var list: MutableList<CommentsListQuery.Comment>,
    private val context: Context,
    private val videoId: String,
    private var recyclerView: RecyclerView,
    private var navigationFlag: String) : RecyclerView.Adapter<CommentAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: CommentAdapter.ViewHolder, position: Int) {
        holder.bindItems(list[position], context, videoId, list)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(list: CommentsListQuery.Comment,
            context: Context,
            videoId: String,
            listWhole: MutableList<CommentsListQuery.Comment>) {

            val name = itemView.tv_name
            val comment = itemView.tv_comment
            val time = itemView.tv_time
            val likeCount = itemView.tv_like_count
            val cv_reply = itemView.cv_reply
            val ll_reply = itemView.ll_reply
            val ll_replyNew = itemView.ll_replyNew
            val llView = itemView.llView
            val imgProfilePicLayout = itemView.imgProfilePicLayoutMain
            val imageView = itemView.imageView
            val imgBorder = itemView.imgProfileBorder
            val txtView = itemView.txtView
            val imgArrowUp = itemView.imgArrowUp


            name.text = list.user()?.name().toString()
            comment.text = list.comment()

            Log.d("tag", "comment time::${list.commentedAt()}")
            time.text = getParsedTime(list.commentedAt().toString())

            cv_reply.setOnClickListener {
                //showReplyDailog(list._id(), adapterPosition)
                mReplyClickListener?.clickReply(list, adapterPosition)
            }

            if (list.replyCounts() > 0) {
                ll_reply.visibility = View.VISIBLE
            } else {
                ll_reply.visibility = View.GONE
            }
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            val user = preferences.getString("USER", null)
            val mainObject = JSONObject(user)
            var myUserId = mainObject.getString("_id")

            imgProfilePicLayout.setOnClickListener {
                if (myUserId == list.user()!!._id()) {
                    val bundle = bundleOf("userId" to list.user()?._id(), "type" to "5")
                    context.openActivity(BottomBarActivity::class.java, bundle)
                } else {
                    val bundle = bundleOf("userId" to list.user()?._id())
                    context.openActivity(ProfileGuestActivity::class.java, bundle)
                }
            }

            var image: Bitmap? = null
            imageView.loadUrl(PUBLIC_URL + (list.user()?.image().toString()))
            imgBorder.setBorder(list.user()?.colorCode().toString())

            if (list?.likeCount()!! > 0 || list?.liked()!!) {
                if (list.likeCount().toString() == "1") {
                    likeCount.text = list?.likeCount().toString() + " Like"
                } else {
                    likeCount.text = list?.likeCount().toString() + " Likes"
                }
                likeCount.setTextColor(Color.parseColor("#FF005F"))
                //likeCount.visibility = View.VISIBLE
            } else {
                //     likeCount.visibility = View.GONE
                likeCount.text = "0 Like"
                likeCount.setTextColor(Color.parseColor("#000000"))
            }
            likeCount.setOnClickListener {
                likeCount?.isEnabled = false
                if (list?.liked()!!) {
                    disLikeComment(list._id(), adapterPosition, -1,likeCount)
                } else {
                    likeComment(list._id(), adapterPosition, -1,likeCount)
                }
            }
            if (list.replyCounts() > 3) {
                llView.visible()
            } else {
                llView.gone()
            }

            ll_reply.removeAllViews()
            ll_replyNew.removeAllViews()
            var rowCount = 0

            val viewReplyArrayList = ArrayList<String>()
            for (s in 0 until listWhole.size) {
                viewReplyArrayList.add(listWhole[s]._id())
            }
            Log.d("tag", "viewReplyArrayList:::${viewReplyArrayList}")

            var id = ""
            llView.setOnClickListener {
                id = list._id()
                if (txtView.text == "View all") {
                    ll_reply.gone()
                    ll_replyNew.visible()
                    imgArrowUp.rotation = 180f
                    if (viewReplyArrayList.contains(id)) {
                        rowCount = 0
                        //  rowCount += rowCount++
                        //reply comments show 3++ replies
                        if (list.replyCounts() > 0) {
                            list.replies()!!.forEach {
                                val replyView: View = LayoutInflater.from(context)
                                    .inflate(R.layout.item_comment_reply, ll_reply, false)
                                val nameReply = replyView.tv_sub_name
                                val commentReply = replyView.tv_sub_comment
                                val timeReply = replyView.tv_sub_time
                                val likeCountReply = replyView.tv_sub_like_count
                                val ic_comment_sub_profile = replyView.ic_comment_sub_profile
                                val comment_sub_line = replyView.comment_sub_line
                                val imgProfileBorder1 = replyView.imgProfileBorder1
                                val imgProfilePicLayout = replyView.imgProfilePicLayout
                                try {
                                    comment_sub_line.setBackgroundColor(
                                        Color.parseColor(
                                            list.user()?.colorCode()))
                                } catch (e: Exception) {
                                    comment_sub_line.setBackgroundColor(Color.parseColor("#707070"))
                                }
                                imgProfilePicLayout.setOnClickListener {
                                    if (myUserId == list.user()!!._id().toString()) {
                                        val bundle = bundleOf("userId" to list.user()?._id(), "type" to "5")
                                        context.openActivity(BottomBarActivity::class.java, bundle)
                                    } else {
                                        val bundle = bundleOf("userId" to list.user()?._id())
                                        context.openActivity(ProfileGuestActivity::class.java, bundle)
                                    }
                                }

                                ic_comment_sub_profile.loadUrl(PUBLIC_URL + it.user()?.image().toString())
                                imgProfileBorder1.setBorder(it.user()?.colorCode().toString())
                                nameReply.text = it.user()?.name().toString()
                                commentReply.text = it.comment()
                                timeReply.text = getParsedTime(it.commentedAt().toString())

                                if (it.likeCount()!! > 0 || it.liked()!!) {

                                    likeCountReply.setTextColor(Color.parseColor("#FF005F"))
                                    if (it.likeCount().toString() == "1") {
                                        likeCountReply.text = it.likeCount().toString() + " Like"
                                    } else {
                                        likeCountReply.text = it.likeCount().toString() + " Likes"
                                    }
                                } else {
                                    likeCountReply.text = "0 Like"
                                    likeCountReply.setTextColor(Color.parseColor("#000000"))
                                }
                                likeCountReply.tag = rowCount

                                likeCountReply.setOnClickListener { view ->
                                    likeCountReply?.isEnabled = false

                                    if (it.liked()!!) {
                                        disLikeComment(
                                            it._id(), adapterPosition, likeCountReply.tag.toString().toInt(),likeCountReply)

                                    } else {
                                        likeComment(
                                            it._id(), adapterPosition, likeCountReply.tag.toString().toInt(),likeCountReply)

                                    }
                                }
                                ll_replyNew.addView(replyView)
                                rowCount++
                            }
                            ll_replyNew.visibility = View.VISIBLE

                        } else {
                            ll_replyNew.visibility = View.GONE
                        }
                        txtView.text = "Hide all"
                    }
                } else {
                    imgArrowUp.rotation = 0f
                    txtView.text = "View all"
                    ll_reply.visible()
                    ll_replyNew.removeAllViews()
                }
            }
            //reply comments show 3 replies
            if (list.replyCounts() > 0) {
                list.replies()!!.forEach {
                    val replyView: View = LayoutInflater.from(context)
                        .inflate(R.layout.item_comment_reply, ll_reply, false)
                    val nameReply = replyView.tv_sub_name
                    val commentReply = replyView.tv_sub_comment
                    val timeReply = replyView.tv_sub_time
                    val likeCountReply = replyView.tv_sub_like_count
                    val ic_comment_sub_profile = replyView.ic_comment_sub_profile
                    val comment_sub_line = replyView.comment_sub_line
                    val imgProfileBorder1 = replyView.imgProfileBorder1
                    val imgProfilePicLayout = replyView.imgProfilePicLayout
                    try {
                        comment_sub_line.setBackgroundColor(
                            Color.parseColor(
                                list.user()?.colorCode()))
                    } catch (e: Exception) {
                        comment_sub_line.setBackgroundColor(Color.parseColor("#707070"))
                    }

                    ic_comment_sub_profile.loadUrl(PUBLIC_URL + it.user()?.image().toString())
                    imgProfileBorder1.setBorder(it.user()?.colorCode().toString())


                    nameReply.text = it.user()?.name().toString()
                    commentReply.text = it.comment()
                    timeReply.text = getParsedTime(it.commentedAt().toString())
                    val userId = it.user()?._id().toString()
                    imgProfilePicLayout.setOnClickListener {

                        if (myUserId == userId) {
                            val bundle = bundleOf("userId" to userId, "type" to "5")
                            context.openActivity(BottomBarActivity::class.java, bundle)
                        } else {
                            val bundle = bundleOf("userId" to userId)
                            context.openActivity(ProfileGuestActivity::class.java, bundle)
                        }
                    }
                    if (it.likeCount()!! > 0 || it.liked()!!) {
                        if (it.likeCount().toString() == "1") {
                            likeCountReply.text = it.likeCount().toString() + " Like"
                        } else {
                            likeCountReply.text = it.likeCount().toString() + " Likes"
                        }
                    } else {
                        likeCountReply.text = "0 Like"
                        likeCountReply.setTextColor(Color.parseColor("#000000"))
                    }
                    likeCountReply.tag = rowCount
                    likeCountReply.setOnClickListener { view ->
                        likeCountReply?.isEnabled = false

                        if (it.liked()!!) {
                            disLikeComment(
                                it._id(), adapterPosition, likeCountReply.tag.toString().toInt(),likeCountReply)

                        } else {
                            likeComment(
                                it._id(), adapterPosition, likeCountReply.tag.toString().toInt(),likeCountReply)

                        }
                    }
                    ll_reply.addView(replyView)
                    rowCount++
                    if (rowCount > 2 && !viewReplyArrayList.contains(id)) {
                        Log.d("tag", "not contains:::${id}")
                        return
                    }

                }
                ll_reply.visibility = View.VISIBLE

            } else {
                ll_reply.visibility = View.GONE
            }


        }

        fun likeComment(commentId: String, position: Int, subPosition: Int,txtLikeComment: TextView) {

            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            val token = preferences.getString("TOKEN", null)
            Log.d("tag", "token::$token")
            val apolloClient = ApolloClient.setupApollo(token ?: "")

            apolloClient.mutate(LikeCommentMutation(commentId))
                .enqueue(object : ApolloCall.Callback<LikeCommentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        val activity: Activity = if (navigationFlag == "ProfilePublic") {
                            context as UserVideosActivity
                        } else {
                            context as BottomBarActivity
                        }
                        activity.runOnUiThread {
                            txtLikeComment.isEnabled = true
                        }
                        //categoryArrayList.postValue(NetworkResponse.ERROR(e))
                    }

                    override fun onResponse(response: Response<LikeCommentMutation.Data>) {
                        // runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        Log.d("Video", "Data : $response")
                        if (response.data()?.likeComment() != null) {
                            val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                context as UserVideosActivity
                            } else {
                                context as BottomBarActivity
                            }
                            activity.runOnUiThread {
                            txtLikeComment.isEnabled = true
                            if (response.data()?.likeComment()?.status()!!) {
                                if (subPosition == -1) {
                                    val comment = list[position]
                                    list[position] = CommentsListQuery.Comment(
                                        comment.__typename(),
                                        comment._id(),
                                        comment.comment(),
                                        comment.commentedAt(),
                                        comment.user(),
                                        comment.liked()?.not(),
                                        comment.likeCount()?.plus(1),
                                        comment.replyCounts(),
                                        comment.replies())
//                                    val activity = context as BottomBarActivity
                                   notifyDataSetChanged() }
                                } else {
                                    val comment = list[position]
                                    var replies = list[position].replies()!!.toMutableList()
                                    val reply = list[position].replies()?.get(subPosition)
                                    replies[subPosition] = CommentsListQuery.Reply(
                                        reply?.__typename()!!,
                                        reply?._id(),
                                        reply.comment(),
                                        reply.commentedAt(),
                                        reply.user(),
                                        reply.liked()?.not(),
                                        reply.likeCount()?.plus(1))
                                    //replies?.set(position,replyNew)

                                    list[position] = CommentsListQuery.Comment(
                                        comment.__typename(),
                                        comment._id(),
                                        comment.comment(),
                                        comment.commentedAt(),
                                        comment.user(),
                                        comment.liked(),
                                        comment.likeCount(),
                                        comment.replyCounts(),
                                        replies)
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread { notifyDataSetChanged() }
                                }

                            }

                        } else {
                            val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                context as UserVideosActivity
                            } else {
                                context as BottomBarActivity
                            }
                            activity.runOnUiThread {
                                txtLikeComment.isEnabled = true
                            }
                            val error = response.errors?.get(0)?.message
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            Log.d("Video", "Data : $error")

                            when {
                                errorCode?.toString()?.equals("401") == true -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                }
                                errorCode?.toString()?.equals("403") == true -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                }
                                else -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        customToast(error.toString(), activity, 0)
                                    }
                                }
                            }
                        }
                    }
                })
        }

        fun disLikeComment(commentId: String, position: Int, subPosition: Int, txtLikeComment: TextView) {
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            val token = preferences.getString("TOKEN", null)
            Log.d("tag", "token::$token")
            val apolloClient = ApolloClient.setupApollo(token ?: "")
            apolloClient.mutate(DisLikeCommentMutation(commentId))
                .enqueue(object : ApolloCall.Callback<DisLikeCommentMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        val activity: Activity = if (navigationFlag == "ProfilePublic") {
                            context as UserVideosActivity
                        } else {
                            context as BottomBarActivity
                        }
                        activity.runOnUiThread {
                            txtLikeComment.isEnabled = true
                        }

                        //categoryArrayList.postValue(NetworkResponse.ERROR(e))
                    }

                    override fun onResponse(response: Response<DisLikeCommentMutation.Data>) {
                        // runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        Log.d("Video", "Data : $response")
                        if (response.data()?.dislikeComment() != null) {
                            val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                context as UserVideosActivity
                            } else {
                                context as BottomBarActivity
                            }
                            activity.runOnUiThread {
                                txtLikeComment.isEnabled = true
                            }

                            if (response.data()?.dislikeComment()?.status()!!) {

                                if (subPosition == -1) {
                                    val comment = list[position]
                                    list[position] = CommentsListQuery.Comment(
                                        comment.__typename(),
                                        comment._id(),
                                        comment.comment(),
                                        comment.commentedAt(),
                                        comment.user(),
                                        comment.liked()?.not(),
                                        comment.likeCount()?.minus(1),
                                        comment.replyCounts(),
                                        comment.replies())
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread { notifyDataSetChanged() }

                                } else {
                                    val comment = list[position]
                                    var replies = list[position].replies()!!.toMutableList()
                                    val reply = list[position].replies()?.get(subPosition)
                                    replies[subPosition] = CommentsListQuery.Reply(
                                        reply?.__typename()!!,
                                        reply?._id(),
                                        reply.comment(),
                                        reply.commentedAt(),
                                        reply.user(),
                                        reply.liked()?.not(),
                                        reply.likeCount()?.minus(1))
                                    //replies?.set(position,replyNew)

                                    list[position] = CommentsListQuery.Comment(
                                        comment.__typename(),
                                        comment._id(),
                                        comment.comment(),
                                        comment.commentedAt(),
                                        comment.user(),
                                        comment.liked(),
                                        comment.likeCount(),
                                        comment.replyCounts(),
                                        replies)
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread { notifyDataSetChanged() }
                                }

                            }

                        } else {
                            val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                context as UserVideosActivity
                            } else {
                                context as BottomBarActivity
                            }
                            activity.runOnUiThread {
                                txtLikeComment.isEnabled = true
                            }
                            val error = response.errors?.get(0)?.message
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            Log.d("Video", "Data : $error")


                            when {
                                errorCode?.toString()?.equals("401") == true -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                }
                                errorCode?.toString()?.equals("403") == true -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                }
                                else -> {
//                                    val activity = context as BottomBarActivity
                                    val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                        context as UserVideosActivity
                                    } else {
                                        context as BottomBarActivity
                                    }
                                    activity.runOnUiThread {
                                        customToast(error.toString(), activity, 0)
                                    }
                                }
                            }
                        }
                    }
                })
        }
    }

    fun createComment(cv_reply_view: CardView?,
        comment: String,
        videoId: String,
        parentId: String,
        position: Int,
        subPosition: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        val apolloClient = ApolloClient.setupApollo(token ?: "")
        apolloClient?.mutate(CreateCommentMutation(comment, videoId, parentId))
            ?.enqueue(object : ApolloCall.Callback<CreateCommentMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                }

                @RequiresApi(Build.VERSION_CODES.M)
                override fun onResponse(response: Response<CreateCommentMutation.Data>) {
                    //                        videoArrayList.postValue(response.data()?.videos()!!)
                    Log.d("tag", "response reply comment::${response.data?.comment()}")
                    if (response.data()?.comment() != null) {
                        // dialog.dismiss()

                        val comment = list[position]
                        var replies = list[position].replies()?.toMutableList() ?: mutableListOf()
                        //val reply = list[position].replies()?.get(subPosition)
                        val user = CommentsListQuery.User1(
                            comment.user()!!._id(),
                            comment.user()?.__typename()!!,
                            comment.user()?.name()!!,
                            comment.user()?.username()!!,
                            comment.user()?.image()!!,
                            comment.user()?.colorCode()!!)
                        replies.add(
                            subPosition, CommentsListQuery.Reply(
                                response.data?.comment()?.__typename()!!,
                                response.data?.comment()?._id()!!,
                                response.data?.comment()?.comment()!!,
                                response.data?.comment()?.commentedAt(),
                                user,
                                response.data?.comment()?.liked(),
                                response.data?.comment()?.likeCount()

                            ))

                        Log.d("tag", "replies:::$replies")
                        list[position] = CommentsListQuery.Comment(
                            comment.__typename(),
                            comment._id(),
                            comment.comment(),
                            comment.commentedAt(),
                            comment.user(),
                            comment.liked(),
                            comment.likeCount(),
                            comment.replyCounts().plus(1),
                            replies)


                        if (navigationFlag == "ProfilePublic") {
                            val activity = context as UserVideosActivity

                            activity.runOnUiThread {
                                cv_reply_view?.visibility = View.GONE
                                notifyDataSetChanged()
                            }
                        } else {
                            val activity = context as BottomBarActivity
                            activity.runOnUiThread {
                                cv_reply_view?.visibility = View.GONE
                                notifyDataSetChanged()
                            }
                        }

                    } else {
                        val error = response.errors?.get(0)?.message
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        Log.d("Video", "Data : $error")

                        when {
                            errorCode?.toString()?.equals("401") == true -> {
                                if (navigationFlag == "ProfilePublic") {
                                    val activity = context as UserVideosActivity
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                } else {
                                    val activity = context as BottomBarActivity
                                    activity.runOnUiThread {
                                        activity.openNewActivity(LoginActivity::class.java)
                                    }
                                }


                            }
                            errorCode?.toString()?.equals("403") == true -> {
                                val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                    context as UserVideosActivity
                                } else {
                                    context as BottomBarActivity
                                }
                                activity.runOnUiThread {
                                    activity.openNewActivity(LoginActivity::class.java)
                                }
                            }
                            else -> {
//                                val activity = context as BottomBarActivity
                                val activity: Activity = if (navigationFlag == "ProfilePublic") {
                                    context as UserVideosActivity
                                } else {
                                    context as BottomBarActivity
                                }
                                activity.runOnUiThread {
                                    customToast(error.toString(), activity, 0)
                                }
                            }
                        }
                    }
                }

            })
    }

    /* fun showReplyDailog(id: String, position: Int) {
         val dialog = Dialog(videoFragment.requireActivity())
         dialog.setContentView(R.layout.layout_reply_back)

         val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
         lp.copyFrom(dialog.window?.attributes)
         lp.width = WindowManager.LayoutParams.MATCH_PARENT
         lp.height = WindowManager.LayoutParams.WRAP_CONTENT
         lp.gravity = Gravity.BOTTOM
         dialog.window?.attributes = lp

         val ll_reply_comment = dialog.findViewById<LinearLayout>(R.id.ll_reply_comment)
         val tv_send_comment = dialog.findViewById<TextView>(R.id.tv_send_comment_reply)
         val et_message_comment = dialog.findViewById<EditText>(R.id.et_message_comment_reply)
         ll_reply_comment.visibility = View.VISIBLE

         tv_send_comment.setOnClickListener {
             createComment(
                 et_message_comment.text.toString(),
                 videoId,
                 id,
                 et_message_comment, position, 0, dialog
             )

         }
         dialog.show()
     }*/

    private fun getParsedTime(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        val DAY_MILLIS = 24 * HOUR_MILLIS
        val WEEK_MILLIS = 7 * DAY_MILLIS
        var parsedDate: String? = null
        parsedDate = try {
            var time: Long = df.parse(date).time
            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000;
            }

            val now = System.currentTimeMillis()

            val diff = now - time;

            Log.d("tag", "diff:::$diff")

            when {
                diff < MINUTE_MILLIS -> {
                    return "just now";
                }
                diff < 2 * MINUTE_MILLIS -> {
                    return "a minute ago";
                }
                diff < 50 * MINUTE_MILLIS -> {
                    return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                }
                diff < 90 * MINUTE_MILLIS -> {
                    return "an hour ago";
                }
                diff < 24 * HOUR_MILLIS -> {
                    return (diff / HOUR_MILLIS).toString() + " hours ago";
                }
                diff < 48 * HOUR_MILLIS -> {
                    return "yesterday";

                }
                diff < 7 * DAY_MILLIS -> {
                    return (diff / DAY_MILLIS).toString() + " days ago";
                }
                diff < 2 * WEEK_MILLIS -> {
                    return "a week ago";
                }
                diff < WEEK_MILLIS * 3 -> {
                    return (diff / WEEK_MILLIS).toString() + " weeks ago";
                }
                else -> {
                    val date1 = getParsedDate(date.toString())
                    Log.d("tag", date1)
                    parsedDate = date1
                    return parsedDate.toString()
                }
            }

        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            val date1 = getParsedDate(date.toString())
            Log.d("tag", date1)
            parsedDate = date1
            return parsedDate.toString()
        }
        return parsedDate.toString()
    }

    private fun getParsedDate(date: String): String {

        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        var parsedDate: String? = null
        parsedDate = try {
            Log.d("tag", "date:::1:$date")
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            outputFormatter1.format(date1)

        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        Log.d("tag", "date::$parsedDate")
        return parsedDate.toString()
    }

    private var mReplyClickListener: ReplyClickListener? = null

    fun setReplyClickListener(listener: ReplyClickListener?) {
        mReplyClickListener = listener
    }

    interface ReplyClickListener {
        fun clickReply(comment: CommentsListQuery.Comment, position: Int)
    }


}