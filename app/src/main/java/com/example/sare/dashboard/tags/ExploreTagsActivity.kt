package com.example.sare.dashboard.tags

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.ExploreTagsListQuery
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.adapter.ExploreTagsAdapter
import com.example.sare.dashboard.viewmodel.TagsViewModel
import com.example.sare.extensions.customToast
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.videoRecording.PortraitCameraActivity
import com.example.sare.widget.CustomProgressDialog
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_explore_tags.*
import kotlinx.android.synthetic.main.activity_explore_tags.imgBack
import kotlinx.android.synthetic.main.activity_explore_tags.txtNoData
import kotlinx.android.synthetic.main.layout_my_profile_scroll_new.view.*
import org.json.JSONObject

class ExploreTagsActivity : AppCompatActivity(), ItemOnclickListener {
    val list: ArrayList<String> = ArrayList()
    var viewModel: TagsViewModel? = null
    lateinit var progressDialog: CustomProgressDialog
    var id: String? = ""
    var tagName: String? = ""
    var videoCount: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_explore_tags)
        setTheme()
        tagName = intent.getStringExtra("tagName")
        videoCount = intent.getStringExtra("videoCount")
        txtTagName.text = tagName.toString()

        getSharedPreferences()
        viewModel = ViewModelProvider(this)[TagsViewModel::class.java]
        setListeners()
        getVideoList("random")
        videoListObserver("random")
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.textWhite)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, "#FFFFFF")
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun setListeners() {
        imgBack.setOnClickListener {
            onBackPressed()
        }

        button_shoot_video.setOnClickListener {
            startActivity(Intent(this, PortraitCameraActivity::class.java))
        }
        btnGoUp?.setOnClickListener {
            btnGoUp.gone()
            rvExploreTags.smoothScrollToPosition(0)
        }
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)

        if (user != null) {
            val mainObject = JSONObject(user)
            id = mainObject.getString("_id")
        }
        Log.d("tag", "_id:::::$id")
    }


    private fun initRecyclerview(list: List<ExploreTagsListQuery.Video>) {
        rvExploreTags?.apply {
            layoutManager = GridLayoutManager(
                rvExploreTags.context, 3)
            adapter = ExploreTagsAdapter(
                list, context, this@ExploreTagsActivity)



            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0) btnGoUp?.visible()
                }
            })

        }
    }


    override fun onItemClickListener(position: Int) {
        //customToast("Item Clicked", this, 1)
    }

    private fun getVideoList(type: String) {
        progressDialog = CustomProgressDialog(this)
        viewModel?.getExploreVideoList(id.toString(), type, this, mutableListOf(tagName))

    }

    fun videoListObserver(type: String) {
        progressDialog = CustomProgressDialog(this)

        viewModel?.exploreTagsList?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relMain), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        viewModel?.getExploreVideoList(id.toString(), type, this, mutableListOf(tagName))
                    }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.getExploreTagsList -> {
                    txtVideoCounts.text = it.response.size.toString() + " Videos"
                    initRecyclerview(it.response)
                    if (rvExploreTags?.adapter?.itemCount != 0) {
                        rvExploreTags?.visibility = View.VISIBLE
                        txtNoData?.visibility = View.GONE
                    } else {
                        rvExploreTags?.visibility = View.GONE
                        txtNoData?.visibility = View.VISIBLE
                    }
                }

            }
        })
    }


}