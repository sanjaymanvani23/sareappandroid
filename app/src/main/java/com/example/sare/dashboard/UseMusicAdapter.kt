package com.example.sare.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.VideoAudioListQuery
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.loadDiamondImage
import com.example.sare.profile.listener.ItemOnclickListener
import kotlinx.android.synthetic.main.item_use_music.view.*

class UseMusicAdapter(val list: List<VideoAudioListQuery.Video>,
    private val context: Context,
    var itemOnclickListener: ItemOnclickListener) : RecyclerView.Adapter<UseMusicAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_use_music, parent, false)
        return ViewHolder(
            v, itemOnclickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, var itemOnclickListener: ItemOnclickListener) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(list: VideoAudioListQuery.Video, context: Context) {
            var imgAudio = itemView.imgAudio
            var imgProfile = itemView.imgProfile
            var imgProfileBorder = itemView.imgProfileBorder
            var txtViewCount = itemView.txtViewCount
            var txtLikeCount = itemView.txtLikeCount
            var txtNameVideo = itemView.txtNameVideo
            txtViewCount.text = list.views().toString()
            txtLikeCount.text = list.likeCounts().toString()
            txtNameVideo.text = list.user().name().toString()
            Glide.with(context).load(Utils.PUBLIC_URL + list.imageName().toString()).error(R.drawable.video_default)
                .placeholder(R.drawable.video_default).into(imgAudio)
            imgProfile.loadUrl(Utils.PUBLIC_URL + list.user().image().toString())
            imgProfileBorder.setBorder(list.user().colorCode().toString() ?: Utils.COLOR_CODE)
            itemView.setOnClickListener {
                itemOnclickListener.onItemClickListener(adapterPosition)
            }

        }
    }
}