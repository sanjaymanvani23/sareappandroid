package com.example.sare.dashboard

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.sare.R
import com.example.sare.UserVideosQuery
import com.example.sare.VideoListQuery
import com.example.sare.appManager.StringSingleton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.user_fragemnt_video.view.userVideoViewPager
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M)
class UserVideoFragment : Fragment() {
    private var root: View? = null
    var loginUserId: String? = ""

    var videoPosition: Int=0
    private lateinit var storiesPagerAdapter: StoriesPagerAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        root = inflater.inflate(R.layout.user_fragemnt_video, container, false)

        if (arguments != null) {


            val user = preferences.getString("USER", null)
            if (user != null) {
                val mainObject = JSONObject(user)
                loginUserId = mainObject.getString("_id")
            }

            var videoIdList1 = arguments?.getString("videoList")

            videoPosition= arguments?.getInt("position")!!

            val gson = Gson()

            val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type

            var videoList = gson.fromJson<List<VideoListQuery.Video>>(videoIdList1, myType) as ArrayList<VideoListQuery.Video>

            storiesPagerAdapter = StoriesPagerAdapter(this, videoList!! as ArrayList<VideoListQuery.Video>)

            root?.userVideoViewPager!!.adapter = storiesPagerAdapter

            root?.userVideoViewPager!!.setCurrentItem(videoPosition,true)

        }
        else
        {

        }
        return root
    }
}

