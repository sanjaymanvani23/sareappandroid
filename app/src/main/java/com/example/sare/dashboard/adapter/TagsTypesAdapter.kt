package com.example.sare.dashboard.adapter

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.CountDownTimer
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.VideoView
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.dashboard.UserVideosActivity
import com.example.sare.dashboard.VideoListViewModel
import com.example.sare.dashboard.hideKeyboard
import com.example.sare.extensions.*
import com.example.sare.profile.activity.ProfileGuestActivity
import com.example.sare.ui.login.LoginActivity
import com.example.sare.ui.tag.TagsActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_video.view.*
import kotlinx.android.synthetic.main.item_tags_profile.view.*
import kotlinx.android.synthetic.main.item_tags_profile.view.txtFollowers
import kotlinx.android.synthetic.main.item_tags_profile.view.txtName
import kotlinx.android.synthetic.main.item_tags_video.view.*
import kotlinx.android.synthetic.main.item_tags_video.view.imgProfile
import kotlinx.android.synthetic.main.item_tags_video.view.imgProfileBorder
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.view.*
import kotlinx.android.synthetic.main.popup_report.*
import org.json.JSONObject
import java.io.FileInputStream


class TagsTypesAdapter(var list: TrendingTagsAndProfileQuery.Trending,
    private val context: Context,
    private val activity: Activity) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_ONE = 1
    private val TYPE_TWO = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val view: View
        return when (viewType) {
            TYPE_ONE -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_tags_video, parent, false)
                ViewHolderTags(view)
            }
            TYPE_TWO -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_tags_profile, parent, false)
                ViewHolderProfile(view)
            }
            else -> throw RuntimeException("The type has to be ONE or TWO")

        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (list != null) {
            when (list.type()) {
                "TAG" -> {
                    var videoModel = list.videos()
                    (holder as ViewHolderTags).bindItems(videoModel?.get(position)!!, context, activity, videoModel)
                }
                "USER" -> {
                    var userModel = list.users()
                    (holder as ViewHolderProfile).bindItems(
                        userModel?.get(position)!!, context, activity)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        when (list.type()) {
            "TAG" -> return list.videos()!!.size
            "USER" -> return list.users()!!.size
        }
        return list.videos()!!.size
    }


    override fun getItemViewType(position: Int): Int {

        when (list.type()) {
            "TAG" -> return return TYPE_ONE
            "USER" -> return TYPE_TWO
        }
        return TYPE_ONE
    }

    class ViewHolderTags(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mainlist: MutableList<VideoListQuery.Video> = ArrayList()

        fun bindItems(videoModel: TrendingTagsAndProfileQuery.Video,
            context: Context,
            activity: Activity,
            list: MutableList<TrendingTagsAndProfileQuery.Video>) {
            var imgAudio = itemView.imgAudio
            var imgProfile = itemView.imgProfile
            var imgProfileBorder = itemView.imgProfileBorder
            var txtViewCount = itemView.txtViewCount
            var txtLikeCount = itemView.txtLikeCount
            var txtTitle = itemView.txtTitle

            imgProfile.loadUrl(PUBLIC_URL + videoModel.user()!!.image())
            imgProfileBorder.setBorder(videoModel.user()!!.colorCode()!!)
            txtViewCount.text = videoModel.views().toString()
            txtLikeCount.text = videoModel.likeCounts().toString()
            txtTitle.text = videoModel.user()!!.name().toString()
            Glide.with(context).load(PUBLIC_URL + videoModel.imageName().toString()).dontAnimate()
                .placeholder(R.drawable.video_default).error(R.drawable.video_default).into(imgAudio)
            Log.d("tag", "video name:::::${videoModel.name()}")


            val profileUserVideoList = ArrayList<VideoListQuery.Video>()

            val model = VideoListQuery.Video(
                videoModel.__typename(),
                videoModel.user()!!._id(),
                videoModel.title(),
                videoModel._id(),
                videoModel.audioId(),
                videoModel.likeCounts(),
                videoModel.views(),
                videoModel.liked(),
                videoModel.name(),
                videoModel.imageName(),
                videoModel.commentCounts(),
                videoModel.description(),
                videoModel.bookmarked(),
                VideoListQuery.Audio(videoModel.__typename(), videoModel.audio()?.active()),
                VideoListQuery.User(
                    videoModel.__typename(),
                    videoModel.user()!!.postsCounts(),
                    videoModel.user()!!.username(),
                    videoModel.user()!!.name(),
                    videoModel.user()!!._id(),
                    videoModel.user()!!.followed(),
                    videoModel.user()!!.colorCode(),
                    videoModel.user()!!.image()))
            profileUserVideoList.add(model)

            var isVideoCurrentPage = 0
            Log.d("tag", "mainlist::::::::::::::$mainlist")

            itemView.setOnClickListener {
                //                val bundle = bundleOf("userId" to userModel._id())
                val gson = Gson()
                val userInfoListJsonString = gson.toJson(profileUserVideoList)
                var bundle = bundleOf(
                    "navigationFlagProfile" to "TagPublic",
                    "videoList" to userInfoListJsonString,
                    "tagName" to list[adapterPosition].name().toString(),
                    "position" to position,
                    "isVideoCurrentPage" to 0)
                activity?.openActivity(UserVideosActivity::class.java, bundle)
                activity?.overridePendingTransition(0, 0)
            }


        }


        //to get the path of video
        private fun getPath(uri: Uri?, context: Context): String? {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor = context.contentResolver.query(uri!!, projection, null, null, null) ?: return null
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }

        //to play video for 2 seconds
        private fun initializePlayer(videoFile: String?, imgAudio: VideoView, activity: Activity) {
            imgAudio.setVideoURI(Uri.parse(videoFile))
            imgAudio.setOnPreparedListener {
                val counter: CountDownTimer = object : CountDownTimer(5000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        imgAudio.start()
                    }

                    override fun onFinish() {
                        //code fire after finish
                        imgAudio.stopPlayback()
                    }
                }
                counter.start()
                /*      val t: Thread = object : Thread() {
                          override fun run() {
                              try {
      //                            while (!isInterrupted) {
                                      sleep(10000) // every 10s
                                      activity.runOnUiThread(Runnable {
                                          // update View here!
                                          imgAudio.start()
                                      })
      //                            }
                                  Log.e("tag","try")
                              } catch (e: InterruptedException) {
                                  e.printStackTrace()
                                  Log.e("tag","error::${e.message}")
                              }
                          }
                      }
                      t.start()*/
            }



            //            initializePlayer(AmazonUtil.getSignedUrl(videoModel.name()), imgAudio, activity)

            /* var retriever: MediaMetadataRetriever? = null
             var inputStream: FileInputStream? = null
             //            try {

             retriever = MediaMetadataRetriever()*/
            //                inputStream = FileInputStream(File(AmazonUtil.getSignedUrl(videoModel.name())).absolutePath)

            /* val myVideoUri = Uri.parse(AmazonUtil.getSignedUrl(videoModel.name()).toString())
             Log.d("tag","myVideoUri:::$myVideoUri")

             val file = File(myVideoUri.getPath()) //create path from uri
             Log.d("tag","file:::$file")

             val split: Array<String> = file.path.split(":".toRegex()).toTypedArray() //split the path.

            val filePath = split[1]
//                val file = File(myVideoUri.getPath())

//                val filePath: String = PathUtil.getPath(context, myVideoUri)
             Log.d("tag","filePath:::$filePath")

             retriever.setDataSource(filePath)*/
            //                val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toLong()

            /*  retriever.setDataSource(AmazonUtil.getSignedUrl(videoModel.name().toString()), HashMap<String, String>())
              val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toLong()
              val duration_millisec = time!!.toInt() //duration in millisec

              val duration_second = duration_millisec / 1000 //millisec to sec.

              val frames = ArrayList<Bitmap>()
              val interval: Long = ((duration_second - 0) / (5 - 1)).toLong()

              for (i in 0 until 2) {
                  val frameTime: Long = 0 + interval * i
                  var bitmap: Bitmap? = retriever.getFrameAtTime(
                      frameTime * 5000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC) ?: continue
                  try {
                      frames.add(bitmap!!)
                  } catch (t: Throwable) {
                      t.printStackTrace()
                  }

              }
              //                val bitmap = retriever.getFrameAtTime(5000000 * i.toLong());
              //                frames.add(bitmap);
              Log.d("tag", "frames:::$frames")
              val animation = AnimationDrawable()
              animation.addFrame(BitmapDrawable(context.getResources(), frames[0]),5)
  //            animation.addFrame(BitmapDrawable(context.getResources(), frames[1]), 5)
  //            animation.addFrame(BitmapDrawable(context.getResources(), frames[2]), 5)
              animation.isOneShot = false

              imgAudio.setImageDrawable(animation)

              // start the animation!

              // start the animation!
              animation.start()*/
            /*} catch (e: Exception) {
                println("Exception============${e.message}")
            }*/


        }
    }

    class ViewHolderProfile(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(userModel: TrendingTagsAndProfileQuery.User1, context: Context, activity: Activity) {
            var imgProfile = itemView.imgProfile1
            var imgProfileBorder = itemView.imgProfileBorder1
            var button = itemView.btnFollow
            var txtName = itemView.txtName
            var txtFollowers = itemView.txtFollowers

            txtName.text = userModel.name().toString()
            txtFollowers.text = "${userModel.followerCounts().toString()} Followers"

            imgProfile.loadUrl(PUBLIC_URL + userModel.image())
            imgProfileBorder.setBorder(userModel.colorCode()!!)

            itemView.setOnClickListener {
                val bundle = bundleOf("userId" to userModel._id())
                activity?.openActivity(ProfileGuestActivity::class.java, bundle)
                activity?.overridePendingTransition(0, 0)
            }

            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            val token = preferences.getString("TOKEN", null)
            val apolloClientNew = ApolloClient.setupApollo(token.toString())

            if (userModel.followed() == true) {
                button.text = "Following"
                button.setButtonBgColor("#000000")

            } else {
                button.text = "Follow"
                button.setButtonBgColor("#FF005F")

            }
            button.setOnClickListener {
                if (button.text == "Following") {
                    apolloClientNew.mutate(
                        UnFollowUserMutation(
                            userModel._id()))

                        .enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                            override fun onFailure(e: ApolloException) {
                                Log.d("tag", "Error Data : ${e.message}")
                            }

                            override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                                activity.runOnUiThread {
                                    Log.d("tag", "res following adapter::${response}")
                                    if (response.data?.unfollow()?.status() == true) {
                                        button.text = "Follow"
                                        button.setTextColor(Color.parseColor("#FF005F"))
                                        customToast("User Unfollowed Successfully", activity, 1)

                                    } else {
                                        val error = response.errors?.get(0)?.customAttributes?.get("status")
                                        Log.d("tag", "Data Error: $error")
                                        if (error?.toString()?.equals("401") == true) {
                                            customToast(
                                                response.errors?.get(0)?.message.toString(), activity, 0)

                                        } else {
                                            customToast(
                                                response.errors?.get(0)?.message.toString(), activity, 0)

                                        }
                                    }
                                }

                            }
                        })
                } else {
                    apolloClientNew.mutate(
                        FollowUserMutation(
                            userModel._id())).enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                        override fun onFailure(e: ApolloException) {
                            Log.d("tag", "Error Data : ${e.message}")
                        }

                        override fun onResponse(response: Response<FollowUserMutation.Data>) {
                            activity.runOnUiThread {

                                if (response.data?.follow()?.status()!!) {

                                    button.text = "Following"
                                    button.setTextColor(Color.parseColor("#000000"))
                                    customToast("User Followed Successfully", activity, 1)

                                } else {

                                    val error = response.errors?.get(0)?.customAttributes?.get("status")
                                    Log.d("tag", "Data Error:1 $error")

                                    if (error?.toString()?.equals("401") == true) {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), activity, 0)

                                    } else {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), activity, 0)

                                    }
                                }

                            }
                        }
                    })
                }

            }
        }
    }
}