package com.example.sare.dashboard.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.MainFragment
import com.example.sare.dashboard.tags.SearchAccountAndTagsActivity
import com.example.sare.extensions.customToast
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.profile.activity.ProfileGuestActivity
import com.example.sare.profile.fragment.ProfileUserFragment
import com.facebook.Profile
import kotlinx.android.synthetic.main.item_follower_following_list.view.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M)

public class AccountsAdapter(
    var list: MutableList<UsersOnSareVidsQuery.User>,
    private val context: Context,
    private val activity: Activity, val recyclerView: RecyclerView,  val userIdMain: String
) :
    RecyclerView.Adapter<AccountsAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_follower_following_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, activity, list, recyclerView, userIdMain)
        holder.setIsRecyclable(false)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(item: UsersOnSareVidsQuery.User, context: Context, activity: Activity,
                      list :  MutableList<UsersOnSareVidsQuery.User>,
                      recyclerView: RecyclerView, userIdMain: String) {
            val policy =
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            StrictMode.setThreadPolicy(policy)

            val name = itemView.txtName
            val id = itemView.txtId
            val likes = itemView.txtLikes
            val imgProfileBorder=itemView.imgProfileBorder
            val imgProfile = itemView.imgProfile
            val relSingleItem = itemView.relSingleItem
            val txtVideoCounts = itemView.txtVideoCounts
            val cardview = itemView.smallView
            val main_layout = itemView.main_layout
            cardview.visibility = View.GONE
            var button = itemView.button_following
            if (item.likeCounts().toString() < 0.toString()) {
                likes.text = 0.toString()
            } else {
                likes.text = item.likeCounts().toString()
            }

            if (item.postsCounts().toString() < 0.toString()) {
                txtVideoCounts.text = 0.toString()
            } else {
                txtVideoCounts.text = item.postsCounts().toString()
            }
            name.text = item.name().toString()
            id.text = "@" + item.username().toString()

            Log.e("Color code",""+item.colorCode())
            imgProfile?.loadUrl( Utils.PUBLIC_URL  + item.image().toString()!!)
            imgProfileBorder?.setBorder(item.colorCode() ?: Utils.COLOR_CODE)
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile,
                Context.MODE_PRIVATE
            )
            var userId: String? = null
            val token = preferences.getString("TOKEN", null)
            val user = preferences.getString("USER", null)
            if (user != null) {
                val mainObject = JSONObject(user)
                userId = mainObject.getString("_id")
            }

            imgProfile.setOnClickListener {
                if (item._id() == userId) {
                    context.startActivity(Intent(context, MainActivity::class.java).putExtra("type",5))
                } else {
                    Log.d("tag", "selectedVideo!!.user()._id()::::${item._id()}")
                    activity.startActivity(Intent(activity, ProfileGuestActivity::class.java).putExtra("userId",item._id()).putExtra("type",""))
                }
            }


            main_layout.setOnClickListener {
                if (item._id() == userId) {
                    context.startActivity(Intent(context, MainActivity::class.java).putExtra("type",5))
                } else {
                    activity.startActivity(Intent(activity, ProfileGuestActivity::class.java).putExtra("userId",item._id()).putExtra("type",""))
                }
            }

            val apolloClientNew = ApolloClient.setupApollo(token.toString())

            if (item.followed() == true) {
                button.text = "Following"
                button.setTextColor(Color.parseColor("#000000"))
            } else {
                button.text = "Follow"
                button.setTextColor(Color.parseColor("#FF005F"))


            }
            button.setOnClickListener {
                if (button.text == "Following") {

                    apolloClientNew.mutate(
                        UnFollowUserMutation(
                            item._id()
                        )
                    )
                        .enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                            override fun onFailure(e: ApolloException) {
                                Log.d("tag", "Error Data : ${e.message}")
                            }

                            override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                                activity.runOnUiThread {
                                    Log.d("tag", "res following adapter::${response}")
                                    if (response.data?.unfollow()?.status() == true) {
                                        button.text = "Follow"
                                        button.setTextColor(Color.parseColor("#FF005F"))
                                        customToast( "User Unfollowed Successfully", activity,1)

                                    } else {
                                        val error =
                                            response.errors?.get(0)?.customAttributes?.get("status")
                                        Log.d("tag", "Data Error: $error")
                                        if (error?.toString()?.equals("401") == true) {
                                            customToast(response.errors?.get(0)?.message.toString(),activity,0)

                                        } else {
                                            customToast(response.errors?.get(0)?.message.toString(),activity,0)

                                        }
                                    }
                                }

                            }
                        })
                } else {
                    apolloClientNew.mutate(
                        FollowUserMutation(
                            item._id()
                        )
                    )
                        .enqueue(object :
                            ApolloCall.Callback<FollowUserMutation.Data>() {
                            override fun onFailure(e: ApolloException) {
                                Log.d("tag", "Error Data : ${e.message}")
                            }

                            override fun onResponse(response: Response<FollowUserMutation.Data>) {
                                activity.runOnUiThread {

                                    if (response.data?.follow()?.status()!!) {

                                        button.text = "Following"
                                        button.setTextColor(Color.parseColor("#000000"))
                                        customToast( "User Followed Successfully",activity,1)

                                    } else {

                                        val error =
                                            response.errors?.get(0)?.customAttributes?.get("status")
                                        Log.d("tag", "Data Error:1 $error")

                                        if (error?.toString()?.equals("401") == true) {
                                            customToast(response.errors?.get(0)?.message.toString(),activity,0)

                                        } else {
                                            customToast(response.errors?.get(0)?.message.toString(),activity,0)

                                        }
                                    }

                                }
                            }
                        })
                }

            }
        }
    }

    fun filterList(filteredNames: List<UsersOnSareVidsQuery.User>): List<UsersOnSareVidsQuery.User>? {
        this.list = filteredNames as MutableList<UsersOnSareVidsQuery.User>
        notifyDataSetChanged()
        return filteredNames
    }

}