package com.example.sare.dashboard;


import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sare.AmazonUtil;
import com.example.sare.MyApplication;
import com.example.sare.R;
import com.example.sare.VideoListQuery;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.database.ExoDatabaseProvider;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.android.exoplayer2.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class VideoPlayerRecyclerView extends RecyclerView {

    private static final String TAG = "VideoPlayerRecyclerView";

    private enum VolumeState {ON, OFF}

    int targetPosition;
    // ui
    private ImageView thumbnail, volumeControl, imgPlay;
    private ProgressBar progressBar;
    private ProgressBar videoProgress;
    private View viewHolderParent;
    private FrameLayout frameLayout;
    private PlayerView videoSurfaceView;
    public SimpleExoPlayer videoPlayer;

    // vars
    private List<VideoListQuery.Video> mediaObjects = new ArrayList<>();
    private int videoSurfaceDefaultHeight = 0;
    private int screenDefaultHeight = 0;
    private Context context;
    private int playPosition = -1;
    private boolean isVideoViewAdded;
    private Runnable r;
    Handler handler = new Handler();
    // controlling playback state
    private VolumeState volumeState;
    private videoData videoData;
    public boolean isVideoPlayed = false;
    private static  CacheDataSourceFactory cacheDataSourceFactory;
       public VideoPlayerRecyclerView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public VideoPlayerRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public static CacheDataSourceFactory getSimpleCache()
    {
        return cacheDataSourceFactory;
    }

    private void init(Context context) {
        this.context = context.getApplicationContext();
        Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        videoSurfaceDefaultHeight = point.x;
        screenDefaultHeight = point.y;

        videoSurfaceView = new PlayerView(this.context);
        videoSurfaceView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_ZOOM);


        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory();
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        // 2. Create the player
        videoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

        videoSurfaceView.setUseController(false);
        videoSurfaceView.setPlayer(videoPlayer);
        setVolumeControl(VolumeState.ON);

        addOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d(TAG, "onScrollStateChanged: called.");
                    if (thumbnail != null) { // show the old thumbnail
                       thumbnail.setVisibility(VISIBLE);
                    }

                    // There's a special case when the end of the list has been reached.
                    // Need to handle that with this bit of logic

                    Log.d(TAG, "DATA DATA Siii: " + mediaObjects.size());
                    if (!recyclerView.canScrollVertically(1)) {
                        playVideo(true);
                    } else {
                        playVideo(false);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

        addOnChildAttachStateChangeListener(new OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {

            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
                if (viewHolderParent != null && viewHolderParent.equals(view)) {
                    resetVideoView();
                }

            }
        });

        videoPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, @Nullable Object manifest, int reason) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playWhenReady) {
                    updateProgress();
                }

                switch (playbackState) {

                    case Player.STATE_BUFFERING:
                        Log.e(TAG, "onPlayerStateChanged: Buffering video.");
                        isVideoPlayed = false;
                        if (progressBar != null) {
                            progressBar.setVisibility(VISIBLE);

                        }


                        break;
                    case Player.STATE_ENDED:
                        Log.d(TAG, "onPlayerStateChanged: Video ended.");
                        videoPlayer.seekTo(0);
                        break;
                    case Player.STATE_IDLE:

                        break;
                    case Player.STATE_READY:
                        thumbnail.setVisibility(GONE);

                        Log.e(TAG, "onPlayerStateChanged: Ready to play.");
                        isVideoPlayed = true;
                        if (progressBar != null) {
                            videoData.playVideo(targetPosition);
                            progressBar.setVisibility(GONE);

                        }

                        if (!isVideoViewAdded) {
                            addVideoView();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onRepeatModeChanged(int repeatMode) {

            }

            @Override
            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity(int reason) {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }

            @Override
            public void onSeekProcessed() {

            }
        });

    }


    public void playVideo(boolean isEndOfList) {


        if (!isEndOfList) {
            int startPosition = ((LinearLayoutManager) Objects.requireNonNull(getLayoutManager())).findFirstVisibleItemPosition();
            int endPosition = ((LinearLayoutManager) getLayoutManager()).findLastVisibleItemPosition();

            // if there is more than 2 list-items on the screen, set the difference to be 1
            if (endPosition - startPosition > 1) {
                endPosition = startPosition + 1;
            }

            // something is wrong. return.
            if (startPosition < 0 || endPosition < 0) {
                return;
            }

            // if there is more than 1 list-item on the screen
            if (startPosition != endPosition) {

                int startPositionVideoHeight = getVisibleVideoSurfaceHeight(startPosition);

                int endPositionVideoHeight = getVisibleVideoSurfaceHeight(endPosition);

                targetPosition = startPositionVideoHeight > endPositionVideoHeight ? startPosition : endPosition;
            } else {
                targetPosition = startPosition;

            }
        } else {
            targetPosition = mediaObjects.size() - 1;
        }

        Log.d(TAG, "playVideo: target position: " + targetPosition);

        // video is already playing so return
        if (targetPosition == playPosition) {
            return;
        }

        // set the position of the list-item that is to be played
        playPosition = targetPosition;
        if (videoSurfaceView == null) {
            return;
        }

        // remove any old surface views from previously playing videos
        videoSurfaceView.setVisibility(INVISIBLE);
        removeVideoView(videoSurfaceView);

        int currentPosition = targetPosition - ((LinearLayoutManager) Objects.requireNonNull(getLayoutManager())).findFirstVisibleItemPosition();

        View child = getChildAt(currentPosition);
        if (child == null) {
            return;
        }

        VideoPlayerViewHolder holder = (VideoPlayerViewHolder) child.getTag();
        if (holder == null) {
            playPosition = -1;
            return;
        }

        thumbnail = holder.thumbnail;
        progressBar = holder.progressBar;
        imgPlay = holder.imgPlay;
        volumeControl = holder.volumeControl;
        viewHolderParent = holder.itemView;
        videoProgress = holder.videoProgress;
        frameLayout = holder.itemView.findViewById(R.id.media_container);

        videoSurfaceView.setPlayer(videoPlayer);

        viewHolderParent.setOnClickListener(videoViewClickListener);

        cacheDataSourceFactory = new CacheDataSourceFactory(
                MyApplication.getSimpleCache(),
                new DefaultHttpDataSourceFactory("App"),
                CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR
        );
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(
                context, Util.getUserAgent(context, "RecyclerView VideoPlayer"));
//        String mediaUrl =AmazonUtil.INSTANCE.getSignedUrl("videos/" +  mediaObjects.get(targetPosition).name());
        String mediaUrl1 = AmazonUtil.INSTANCE.getSignedUrl(mediaObjects.get(targetPosition).name());
        Log.d("tag","mediaUrl1::::"+mediaUrl1);
        // String mediaUrl = AmazonUtil.INSTANCE.getSignedUrl("videos/" + mediaObjects.get(targetPosition).name());
        Log.d("tag", "mediaObjects.get(targetPosition).name():::::" + mediaObjects.get(targetPosition).name());
        //   String mediaUrl = "https://api-sarevids-data.s3.ap-south-1.amazonaws.com/"+ mediaObjects.get(targetPosition).name();
        //   Log.d(TAG, "DATA DATA : " + mediaUrl);
      //  if (mediaUrl != null) {

        String mediaUrl = "";
        if (mediaUrl1 != null) {
            mediaUrl = mediaUrl1.replace(
                       "https://s3.ap-south-1.amazonaws.com/vids.sarevids.com/sarevidsvideos/", "https://d20p3vlgg4r8y8.cloudfront.net/");
        }
        // }
        Log.d("tag","mediaUrl::::"+mediaUrl);


       /* assert mediaUrl != null;//
        mediaUrl = mediaUrl.substring(0, 17) + ".cdn." + mediaUrl.substring(18);
        Log.e("MediaUrl", mediaUrl);*/

//        Log.d("tag", "Data : " + AmazonUtil.INSTANCE.getSignedUrl(mediaObjects.get(targetPosition).imageName()));

        //Glide.with(thumbnail.getContext()).load(AmazonUtil.INSTANCE.getSignedUrl(mediaObjects.get(targetPosition).imageName())).into(thumbnail);

        videoData.download(targetPosition, mediaUrl);

        if (mediaUrl != null) {

            MediaSource videoSource = new ProgressiveMediaSource.Factory(cacheDataSourceFactory).createMediaSource(Uri.parse(mediaUrl));

            if (videoPlayer != null) {
                videoPlayer.prepare(videoSource);
                videoPlayer.setPlayWhenReady(true);
            }
        }
    }

    private OnClickListener videoViewClickListener = v -> toggleVolume();

    /**
     * Returns the visible region of the video surface on the screen.
     * if some is cut off, it will return less than the @videoSurfaceDefaultHeight
     *
     * @param playPosition
     * @return
     */
    private int getVisibleVideoSurfaceHeight(int playPosition) {
        int at = playPosition - ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();

        Log.d(TAG, "getVisibleVideoSurfaceHeight: at: " + at);

        View child = getChildAt(at);
        if (child == null) {
            return 0;
        }

        int[] location = new int[2];
        child.getLocationInWindow(location);

        if (location[1] < 0) {
            return location[1] + videoSurfaceDefaultHeight;
        } else {
            return screenDefaultHeight - location[1];
        }
    }


    // Remove the old player
    private void removeVideoView(PlayerView videoView) {
        ViewGroup parent = (ViewGroup) videoView.getParent();

        if (parent == null) {
            return;
        }

        int index = parent.indexOfChild(videoView);
        if (index >= 0) {
            parent.removeViewAt(index);
            isVideoViewAdded = false;
            if (viewHolderParent != null)
                viewHolderParent.setOnClickListener(null);
        }

    }

    private void addVideoView() {
        frameLayout.addView(videoSurfaceView);
        isVideoViewAdded = true;
        videoSurfaceView.requestFocus();

        videoSurfaceView.setVisibility(VISIBLE);
        videoSurfaceView.setAlpha(1);
        thumbnail.setVisibility(GONE);
    }

    public void resetVideoView() {

        if (isVideoViewAdded) {
            removeVideoView(videoSurfaceView);
            playPosition = -1;
            videoSurfaceView.setVisibility(INVISIBLE);
            thumbnail.setVisibility(VISIBLE);
        }
    }

    public void pauseVideo() {
        if (videoPlayer != null) {
            videoPlayer.setPlayWhenReady(false);
            isVideoPlayed = false;
        }

    }

    public void playVideo() {
        if (videoPlayer != null) {
            videoPlayer.setPlayWhenReady(true);
            isVideoPlayed = true;
        }
    }

    public void releasePlayer() {
        removeHandler();
        if (videoPlayer != null) {
            videoPlayer.release();
            videoPlayer = null;
        }

        viewHolderParent = null;
    }

    private void toggleVolume() {
        if (videoPlayer != null) {


            if (videoPlayer.getPlayWhenReady()) {
                videoPlayer.setPlayWhenReady(false);
                imgPlay.setVisibility(VISIBLE);
                animatePlayControl();
            } else {
                videoPlayer.setPlayWhenReady(true);
                imgPlay.setVisibility(GONE);

            }

            if (volumeState == VolumeState.OFF) {
                Log.d(TAG, "togglePlaybackState: enabling volume.");
                setVolumeControl(VolumeState.ON);

            } else if (volumeState == VolumeState.ON) {
                Log.d(TAG, "togglePlaybackState: disabling volume.");
                setVolumeControl(VolumeState.OFF);

            }
        }
    }

    private void setVolumeControl(VolumeState state) {
        volumeState = state;
        if (state == VolumeState.OFF) {
            videoPlayer.setVolume(0f);
            animateVolumeControl();
        } else if (state == VolumeState.ON) {
            videoPlayer.setVolume(1f);
            animateVolumeControl();
        }
    }

    public void removeHandler() {
        handler.removeCallbacks(r);
    }

    private void animateVolumeControl() {
        if (volumeControl != null) {
            volumeControl.bringToFront();
            if (volumeState == VolumeState.OFF) {
//                      volumeControl
            } else if (volumeState == VolumeState.ON) {
//                      volumeControl
            }
            volumeControl.animate().cancel();

            volumeControl.setAlpha(1f);

            volumeControl.animate()
                    .alpha(0f)
                    .setDuration(600).setStartDelay(1000);
        }
    }


    private void animatePlayControl() {
        if (imgPlay != null) {
            imgPlay.bringToFront();

            imgPlay.animate().cancel();

            imgPlay.setAlpha(0.5f);

        }
    }

    public void setMediaObjects(List<VideoListQuery.Video> mediaObjects) {
        Log.d(TAG, "DATA DATA : " + mediaObjects.size());
        //this.mediaObjects.clear();
        //this.mediaObjects.addAll(mediaObjects);
        this.mediaObjects = mediaObjects;
    }

    public void seAddtMediaObjects(List<VideoListQuery.Video> mediaObjects) {
        Log.d(TAG, "DATA DATA : " + mediaObjects.size());
        this.mediaObjects.addAll(mediaObjects);
    }



    private void updateProgress() {
        r = new Runnable() {
            @Override
            public void run() {

                long currentPosition = videoPlayer == null ? 0 : videoPlayer.getCurrentPosition();

                long duration = videoPlayer == null ? 0 : videoPlayer.getDuration();
                try {
                    videoProgress.setProgress((int) ((currentPosition * 100 / duration) * 1000));
                }catch (Exception e)
                {
                   Log.e("Exception",e.toString()) ;
                }



                handler.postDelayed(r, 10);
            }
        };
        handler.postDelayed(r, 1000);
    }

    void setDownloadListener(videoData videoData) {
        this.videoData = videoData;
    }

    interface videoData {
        void download(int posotion, String url);

        void playVideo(int posotion);
    }
}



























