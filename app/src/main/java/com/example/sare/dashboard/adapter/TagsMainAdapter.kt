package com.example.sare.dashboard.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.TrendingTagsAndProfileQuery
import com.example.sare.dashboard.tags.ExploreTagsActivity
import com.example.sare.ui.tag.TagsActivity
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import kotlinx.android.synthetic.main.item_tags_main_list.view.*

class TagsMainAdapter(var list: MutableList<TrendingTagsAndProfileQuery.Trending>,private val context: Context) : RecyclerView.Adapter<TagsMainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): TagsMainAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_tags_main_list, parent, false)
        return ViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TagsMainAdapter.ViewHolder, position: Int) {
        holder.bindItems(list, context)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("SetTextI18n")
        fun bindItems(list: MutableList<TrendingTagsAndProfileQuery.Trending>,context: Context) {
            val txtType = itemView.txtType
            val txtVideoLikeCount = itemView.txtVideoLikeCount
            val recyclerView = itemView.rvMain
            val rlTag = itemView.rlTag
            val imgRightArrow = itemView.imgRightArrow
            val activity = context as TagsActivity
            if (list[adapterPosition].type()?.toUpperCase() == "TAG") {
                txtVideoLikeCount.visible()
                imgRightArrow.visible()
                txtVideoLikeCount.text = list[adapterPosition].totalVideos().toString() + " Videos"
                txtType.text = list[adapterPosition].name().toString()

                rlTag.setOnClickListener {
                    activity.startActivity(
                        Intent(
                            activity,
                            ExploreTagsActivity::class.java
                        ).putExtra("tagName", txtType.text.toString())
                            .putExtra("videoCount", txtVideoLikeCount.text.toString())
                    )
                    activity.overridePendingTransition(0,0)
                }
            } else {
                txtVideoLikeCount.gone()
                imgRightArrow.gone()
                rlTag.isClickable = false
                txtType.text = "Top Profiles"
            }


            recyclerView.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = TagsTypesAdapter(list[adapterPosition], context, activity)
            }

        }


    }


}