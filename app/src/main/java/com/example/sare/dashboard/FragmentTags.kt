package com.example.sare.dashboard

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.*
import com.example.sare.PaginationListener
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.adapter.TagsMainAdapter
import com.example.sare.dashboard.tags.SearchAccountAndTagsActivity
import com.example.sare.dashboard.viewmodel.TagsViewModel
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragement_tags.view.*

@RequiresApi(Build.VERSION_CODES.M) class FragmentTags : Fragment() {

    var root: View? = null
    var tagsViewModel: TagsViewModel? = null
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var allList: MutableList<TrendingTagsAndProfileQuery.Trending> = mutableListOf()
    lateinit var tagsMainAdapter: TagsMainAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragement_tags, container, false)
        setTheme()
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
              MyApplication.mainActivity.openVideoScreen()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), onBackPressedCallback)
        tagsViewModel = ViewModelProvider(this)[TagsViewModel::class.java]
        initRecyclerView()
        tagsList()
        tagsObserver()

        root?.type_all?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.txtFollowing?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.searchLayout?.setOnClickListener {
            startActivity(Intent(requireActivity(), SearchAccountAndTagsActivity::class.java))
            activity?.overridePendingTransition(0, 0)
        }

        return root
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), "#FFFFFF")
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), "#FFFFFF")
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun tagsList() {
        tagsViewModel?.getTrendingTags(requireContext(), currentPage * Utils.PAGE_SIZE)
    }

    private fun tagsObserver() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        tagsViewModel?.trendingTagsList?.observe(viewLifecycleOwner, Observer { it ->

            when (it) {

                is NetworkResponse.ERROR -> {
                    progressDialog.hide()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        requireView(), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        tagsViewModel?.getTrendingTags(
                            requireContext(), currentPage * Utils.PAGE_SIZE)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()
                    isLastPage = true
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    isLastPage = true
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.getTrendingTagsList -> {

                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    isLoading = false

                    allList.addAll(it.response)

                    root?.recyclerView?.adapter?.notifyDataSetChanged()
                    if (it.response.size < Utils.PAGE_SIZE) {
                        isLastPage = true
                    }
                    if (root?.recyclerView?.adapter?.itemCount == 0) {
                        root?.txtNoData?.visibility = View.VISIBLE
                        root?.recyclerView?.visibility = View.GONE
                    } else {
                        root?.txtNoData?.visibility = View.GONE
                        root?.recyclerView?.visibility = View.VISIBLE

                    }

                }
            }
        })
    }


    private fun initRecyclerView() {
        tagsMainAdapter = TagsMainAdapter(allList, requireContext())
        root?.recyclerView?.apply {
            layoutManager = LinearLayoutManager(root?.recyclerView?.context, RecyclerView.VERTICAL, false)

            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {

                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    tagsList()
                }

                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }
            })

            adapter = TagsMainAdapter(
                allList, requireContext())
        }
    }
}
