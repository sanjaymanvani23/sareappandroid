package com.example.sare.dashboard

import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.d.parastask.MainViewModel
import com.example.sare.AmazonUtil
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.UserVideosQuery
import com.example.sare.extensions.gone
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_story_view.*

class StoryViewFragment : Fragment(R.layout.layout_story_view) {
     var storyUrl: String? = null
     var storiesDataModel:  UserVideosQuery.UserVideo?? = null
     var r: Runnable? = null
    var handler = Handler()

     var cacheDataSourceFactory: CacheDataSourceFactory? = null
     val simpleCache = MyApplication .simpleCache
     var simplePlayer: SimpleExoPlayer? = null
     var toPlayVideoPosition: Int = -1
    companion object {
        fun newInstance(storiesDataModel: String) = StoryViewFragment()
            .apply {
                arguments = Bundle().apply {
                    putString("Model", storiesDataModel)
                }
            }

    }

    private val viewModel by activityViewModels<MainViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var videoIdList1 = arguments?.getString("Model")
        val gson = Gson()
        val userVideoList = gson.fromJson<UserVideosQuery.UserVideo>(
            videoIdList1,
            UserVideosQuery.UserVideo::class.java
        )
        Log.e("Sare App Video Name ", "" + userVideoList.name())

        storiesDataModel = userVideoList
        setData()
    }

    private fun setData() {

        val simplePlayer = getPlayer()
        player_view_story.player = simplePlayer
        var mediaUrl1 = AmazonUtil.getSignedUrl(storiesDataModel!!.name())

        var mediaUrl = ""
        if (mediaUrl1 != null) {
            Log.e("Sare App Video Inside ", "" + mediaUrl1)
            mediaUrl = mediaUrl1.replace(
                "https://s3.ap-south-1.amazonaws.com/vids.sarevids.com/sarevidsvideos/",
                "https://d20p3vlgg4r8y8.cloudfront.net/"
            )
        }


        storyUrl = mediaUrl
        Log.e("Sare App Video Name ", "" + storyUrl)

        storyUrl?.let { prepareMedia(it) }

        itemView.setOnClickListener {
            Log.e("Sare App Video Name ", "Click Player")
            if (simplePlayer!!.playWhenReady) {
                simplePlayer.playWhenReady = false
                imgPlay.visibility = View.VISIBLE
            } else {
                simplePlayer.playWhenReady = true
                imgPlay.visibility = View.GONE
            }
        }

    }

    private fun updateProgress() {
        r = Runnable {
            val currentPosition: Long =
                if (simplePlayer == null) 0 else simplePlayer!!.currentPosition
            val duration: Long = if (simplePlayer == null) 0 else simplePlayer!!.duration
            try {
                videoProgress.progress = (currentPosition * 100 / duration * 1000).toInt()
            } catch (e: Exception) {
                Log.e("Exception", e.toString())
            }
            handler.postDelayed(r!!, 10)
        }
        handler.postDelayed(r!!, 1000)
    }

    override fun onPause() {
        pauseVideo()
        super.onPause()
    }

    override fun onResume() {
        restartVideo()
        super.onResume()
    }

    override fun onDestroy() {
        releasePlayer()
        super.onDestroy()
    }



    private fun prepareVideoPlayer() {
        simplePlayer = context?.let { ExoPlayerFactory.newSimpleInstance(it) }
        cacheDataSourceFactory = simpleCache?.let {
            CacheDataSourceFactory(
                it,
                DefaultHttpDataSourceFactory(
                    Util.getUserAgent(
                        requireContext(),
                        "App"
                    )
                )
            )
        }
    }

    private fun getPlayer(): SimpleExoPlayer? {
        if (simplePlayer == null) {
            prepareVideoPlayer()
        }
        return simplePlayer
    }

    private fun prepareMedia(linkUrl: String) {

        val uri = Uri.parse(linkUrl)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory!!).createMediaSource(
            uri
        )
        simplePlayer?.prepare(mediaSource, true, true)
        simplePlayer?.repeatMode = Player.REPEAT_MODE_ONE
        simplePlayer?.addListener(object : Player.EventListener {

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playWhenReady) {
                    updateProgress()
                }

            }

            override fun onPlayerError(error: ExoPlaybackException) {
                super.onPlayerError(error)
                Log.e("Sare App Error", error.toString())
            }
        })

        toPlayVideoPosition = -1
    }

    private fun setArtwork(drawable: Drawable, playerView: PlayerView) {
        playerView.useArtwork = true
        playerView.defaultArtwork = drawable
    }

    fun playVideo() {
//        simplePlayer?.volume = 0f
        simplePlayer?.playWhenReady = true
    }
    fun setMute(toMute:String)
    {
        if(toMute!="ACTIVE")
            simplePlayer?.volume = 0f
        else
            simplePlayer?.volume = 1f
    }
    private fun restartVideo() {
        if (simplePlayer == null) {
            storyUrl?.let { prepareMedia(it) }
        } else {
            simplePlayer?.seekToDefaultPosition()
            simplePlayer?.playWhenReady = true
        }
    }

    private fun pauseVideo() {
        simplePlayer?.playWhenReady = false
    }

    fun releasePlayer() {
        simplePlayer?.stop(true)
        simplePlayer?.release()
    }
}