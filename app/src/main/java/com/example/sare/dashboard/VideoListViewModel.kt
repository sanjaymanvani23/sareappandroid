package com.example.sare.dashboard

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.api.ApiRequest
import com.example.sare.api.ApolloApiResponse
import com.example.sare.api.ApolloApiResponseListener
import com.example.sare.api.ApolloClientService
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class VideoListViewModel(application: Application) : AndroidViewModel(application), ApolloApiResponseListener {
    private val context = getApplication<Application>().applicationContext
    var videoArrayList = MutableLiveData<NetworkResponse>()

    var userVideoArrayList = MutableLiveData<NetworkResponse>()

    var categoryArrayList = MutableLiveData<NetworkResponse>()
    var apolloClient: ApolloClient? = null
    var apolloClientService = ApolloClientService()

    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        Log.e("onPlayerStateChanged", "token::$token")
        apolloClient = com.example.sare.ApolloClient.setupApollo(token ?: "")

    }

    var commentArrayList = MutableLiveData<NetworkResponse>()
    var createComment = MutableLiveData<NetworkResponse>()
    var bookmarkVideo = MutableLiveData<NetworkResponse>()
    var videoLike = MutableLiveData<NetworkResponse>()
    var videoDisLike = MutableLiveData<NetworkResponse>()
    var reportArrayList = MutableLiveData<NetworkResponse>()
    var videoAudioListArrayList = MutableLiveData<NetworkResponse>()
    var followUser = MutableLiveData<NetworkResponse>()
    var unFollowUser = MutableLiveData<NetworkResponse>()
    var download = MutableLiveData<NetworkResponse>()
    var viewCount = MutableLiveData<NetworkResponse>()

    fun getVideoList(categoryList: List<String>, userId: String, type: String) {
        Log.d("tag", "userId::$userId")
        apolloClient?.query(VideoListQuery(categoryList, userId, type, "views",0,150))
            //apolloClient?.query(VideoListQuery(categoryList, "5f23d12c6188ec01fcafd61a"))
            ?.enqueue(object : ApolloCall.Callback<VideoListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    videoArrayList.postValue(NetworkResponse.ERROR(e))
                    Log.d("Video", "Data Failure : ${e.message}")

                }

                override fun onResponse(response: Response<VideoListQuery.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        try {
                            if (response.data?.videos() != null) {
                                videoArrayList.postValue(
                                    NetworkResponse.SUCCESS.videoList(
                                        response.data?.videos()!!))
                            } else {

                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                videoArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))

                            }
                        } catch (e: Exception) {
                            videoArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getVideoList1(categoryList: List<String>, userId: String, type: String) {
        Log.d("tag", "userId::$userId")
        ApiRequest(
            context, apolloClientService.getVideoList1(apolloClient, categoryList, userId, type), this, 101)
    }

    fun getVideoAudioList(audioId: String) {
        Log.d("tag", "audioId::$audioId")
        apolloClient?.query(VideoAudioListQuery(audioId))
            //  apolloClient?.query(VideoAudioListQuery("5f2bf8d61e6bb907c49e03e7"))
            //apolloClient?.query(VideoListQuery(categoryList, "5f23d12c6188ec01fcafd61a"))
            ?.enqueue(object : ApolloCall.Callback<VideoAudioListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    videoAudioListArrayList.postValue(NetworkResponse.ERROR(e))
                    Log.d("Video", "Data Failure : ${e.message}")

                }

                override fun onResponse(response: Response<VideoAudioListQuery.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        try {
                            if (response.data?.audio() != null) {
                                videoAudioListArrayList.postValue(
                                    NetworkResponse.SUCCESS.videoAudioList(
                                        response.data?.audio()!!))
                            } else {

                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                videoAudioListArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))


                            }
                        } catch (e: Exception) {
                            videoAudioListArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getCommentList(videoId: String, start: Int) {
        //        apolloClient?.query(CommentsListQuery("5f254aa668d68531a4191320"))
        Log.d("tag", "videoId:::$videoId")
        //val apolloClient = com.example.sare.ApolloClient.setupApollo("")
        apolloClient?.query(CommentsListQuery(videoId, start * PAGE_SIZE, PAGE_SIZE))
            ?.enqueue(object : ApolloCall.Callback<CommentsListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    commentArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<CommentsListQuery.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        Log.d("Video", "Video Comment : $response")
                        try {
                            if (response.data?.comments() != null) {
                                commentArrayList.postValue(
                                    NetworkResponse.SUCCESS.commentList(
                                        response.data?.comments()!!))
                            } else {

                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                commentArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            commentArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getCategoryList() {
        apolloClient?.query(CategoryListQuery.builder().build())
            ?.enqueue(object : ApolloCall.Callback<CategoryListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    categoryArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<CategoryListQuery.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        try {
                            if (response.data?.categories() != null && response.data?.categories()
                                    ?.isNotEmpty()!!
                            ) {
                                categoryArrayList.postValue(
                                    NetworkResponse.SUCCESS.categoryList(
                                        response.data?.categories()!!))
                            } else {

                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                categoryArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            categoryArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun createComment(comment: String, videoId: String, parentId: String) {
        apolloClient?.mutate(CreateCommentMutation(comment, videoId, parentId))
            ?.enqueue(object : ApolloCall.Callback<CreateCommentMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    createComment.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<CreateCommentMutation.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        try {
                            if (response.data?.comment() != null) {
                                createComment.postValue(
                                    NetworkResponse.SUCCESS.createCommentList(
                                        response.data?.comment()!!))
                            } else {

                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                createComment.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            createComment.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun videoLike(videoId: String) {
        Log.e("videoId", "" + videoId)
        ApiRequest(context, apolloClient?.mutate(VideoLikeMutation(videoId)), this, 102)
    }

    fun videoDisLike(videoId: String) {
        Log.d("tag", "videoId:::$videoId")
        apolloClient?.mutate(VideoDisLikeMutation(videoId))
            ?.enqueue(object : ApolloCall.Callback<VideoDisLikeMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    videoDisLike.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<VideoDisLikeMutation.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        Log.d("Video", "video : dislike")
                        try {

                            if (response.data?.dislike() != null) {
                                videoDisLike.postValue(
                                    NetworkResponse.SUCCESS.videoDisLike(
                                        response.data?.dislike()!!))

                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                videoDisLike.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            videoDisLike.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun bookmarkVideo(videoId: String, action: Boolean) {
        apolloClient?.mutate(BookmarkVideoMutation(videoId, action))
            ?.enqueue(object : ApolloCall.Callback<BookmarkVideoMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    bookmarkVideo.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<BookmarkVideoMutation.Data>) {
                    runBlocking {
                        //                        videoArrayList.postValue(response.data()?.videos()!!)
                        try {

                            if (response.data?.bookMark() != null) {
                                bookmarkVideo.postValue(
                                    NetworkResponse.SUCCESS.bookmarkVideo(
                                        response.data?.bookMark()!!))

                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                bookmarkVideo.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))

                            }
                        } catch (e: Exception) {
                            Log.e("Test Error", e.toString())
                            bookmarkVideo.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getReportList(context: Context) {
        apolloClient?.query(ReportListQuery.builder().build())
            ?.enqueue(object : ApolloCall.Callback<ReportListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    reportArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<ReportListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.reportType() != null) {
                                reportArrayList.postValue(
                                    NetworkResponse.SUCCESS.getReportList(
                                        response.data?.reportType()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                reportArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            reportArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                        }
                    }
                }
            })
    }

    fun followUser(id: String) {
        apolloClient?.mutate(
            FollowUserMutation(
                id))

            ?.enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    followUser.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<FollowUserMutation.Data>) {
                    Log.d("tag", "Response::: $response")

                    try {
                        if (response.data?.follow()?.status() == true) {
                            followUser.postValue(
                                NetworkResponse.SUCCESS.followUser(
                                    response.data?.follow()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                            followUser.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        followUser.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                    }
                }
            })
    }

    fun unFollowUser(id: String) {
        apolloClient?.mutate(
            UnFollowUserMutation(
                id))

            ?.enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    unFollowUser.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<UnFollowUserMutation.Data>) {

                    try {
                        if (response.data?.unfollow()?.status() == true) {
                            unFollowUser.postValue(
                                NetworkResponse.SUCCESS.unfollowUser(
                                    response.data?.unfollow()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            unFollowUser.postValue(
                                NetworkResponse.ERROR_RESPONSE(
                                    response.errors?.get(
                                        0)?.message.toString()))

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        unFollowUser.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun download(id: String) {
        apolloClient?.mutate(
            DownloadMutation(
                id))

            ?.enqueue(object : ApolloCall.Callback<DownloadMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    download.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<DownloadMutation.Data>) {

                    try {
                        if (response.data?.download()?.status() == true) {
                            download.postValue(
                                NetworkResponse.SUCCESS.download(
                                    response.data?.download()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            download.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        download.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun viewCount(id: String) {
        Log.d("tag", id)
        apolloClient?.mutate(
            ViewCountMutation(
                id))?.enqueue(object : ApolloCall.Callback<ViewCountMutation.Data>() {
            override fun onFailure(e: ApolloException) {
                Log.d("tag", "Error Data : ${e.message}")
                viewCount.postValue(NetworkResponse.ERROR(e))

            }

            override fun onResponse(response: Response<ViewCountMutation.Data>) {

                try {
                    if (response.data?.view()?.status() == true) {
                        viewCount.postValue(
                            NetworkResponse.SUCCESS.viewCount(
                                response.data?.view()!!))
                    } else if (response.data?.view()?.status() == false) {

                    } else {
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        NetworkResponse.ERROR_AUTHENTICATION(
                            response.errors?.get(0)?.message.toString(), errorCode.toString())
                        viewCount.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    viewCount.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                }
            }
        })
    }

    override fun getApiSuccessResponse(apiResponseManager: ApolloApiResponse<*>) {

        when (apiResponseManager.type) {
            101 -> {
                Log.e("Tag ", "Api call Success")
                var response = apiResponseManager.response as Response<VideoListQuery.Data>
                runBlocking {
                    try {
                        if (response.data?.videos() != null) {
                            videoArrayList.postValue(NetworkResponse.SUCCESS.videoList(response.data?.videos()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            videoArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        videoArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }

            }
            102 -> {
                var response = apiResponseManager.response as Response<VideoLikeMutation.Data>
                runBlocking {
                    Log.d("Video", "video : like")
                    try {
                        if (response.data?.like() != null) {
                            videoLike.postValue(NetworkResponse.SUCCESS.videoLike(response.data?.like()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            videoLike.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        videoLike.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            }
            103 -> {

                Log.e("SareApp  ", " Response Call Api User Video")

                var response = apiResponseManager.response as Response<UserVideosQuery.Data>

                Log.e("SareApp App", "Call " + response.toString())
                runBlocking {
                    try {
                        if (response.data?.userVideos() != null) {
                            userVideoArrayList.postValue(
                                NetworkResponse.SUCCESS.userVideoPublishedList(
                                    response.data?.userVideos()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            userVideoArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        userVideoArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            }
        }

    }

    override fun getApiFailResponse(errorMsg: ApolloException, type: Int) {
        when (type) {
            101 -> {
                videoArrayList.postValue(NetworkResponse.ERROR(errorMsg))
            }
            102 -> {
                bookmarkVideo.postValue(NetworkResponse.ERROR(errorMsg))
            }
            103 -> {
                Log.e("SareApp App", "Error Api User Video")
                userVideoArrayList.postValue(NetworkResponse.ERROR(errorMsg))
            }
        }
    }

    fun getUserVideoList(userId: String, pageCount: Int) {
        Log.e("SareApp App", "Call Api User Video")
        Log.e("SareApp App", "Page Count$userId")
        ApiRequest(
            context, apolloClientService.getUserVideoList(
                apolloClient, userId.toString(), pageCount * PAGE_SIZE, PAGE_SIZE, "Published"), this, 103)
    }

    fun getTagVideoList(categoryList: MutableList<String?>, userId: String, pageCount: Int) {
        Log.e("SareApp App", "Call Api Tags Video")
        Log.e("SareApp App", "Page Count:::$pageCount")
        ApiRequest(
            context, apolloClientService.getTagVideoList(apolloClient, categoryList, userId, pageCount * PAGE_SIZE, PAGE_SIZE), this, 101)
    }
}