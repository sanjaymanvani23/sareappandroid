package com.example.sare.dashboard.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.TagSearchListQuery
import com.example.sare.dashboard.tags.ExploreTagsActivity
import kotlinx.android.synthetic.main.item_tags_list.view.*

class TagsListAdapter(
    var list: List<TagSearchListQuery.Tag>,
    private val context: Context,
    private val activity: Activity
) :
    RecyclerView.Adapter<TagsListAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TagsListAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_tags_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, activity)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        public fun bindItems(
            item: TagSearchListQuery.Tag,
            context: Context,
            activity: Activity
        ) {
            val videoCounts = itemView.txtVideoCounts
            val txtHashTagName = itemView.txtHashTagName
            val llItem = itemView.ll_tag_item

            videoCounts.text = item.totalVideos().toString() + " Videos"
            txtHashTagName.text = item.name().toString()

            llItem.setOnClickListener {
                activity.startActivity(Intent(activity, ExploreTagsActivity::class.java).putExtra("tagName", txtHashTagName.text.toString())
                    .putExtra("videoCount", videoCounts.text.toString()))
                activity.overridePendingTransition(0,0)
            }


        }

    }

}