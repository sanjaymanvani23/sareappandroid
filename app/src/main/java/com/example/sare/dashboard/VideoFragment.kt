package com.example.sare.dashboard

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.app.Dialog
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.util.Log
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.ScaleAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import androidx.viewpager2.widget.ViewPager2
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.customeview.HashTagView.HashTag
import com.example.sare.dashboard.model.DownloadResult
import com.example.sare.dashboard.model.DummyData
import com.example.sare.dashboard.tags.ExploreTagsActivity
import com.example.sare.extensions.*
import com.example.sare.profile.activity.ProfileGuestActivity
import com.example.sare.profile.adapter.ReportListAdapter
import com.example.sare.ui.login.LoginActivity
import com.example.sare.ui.splashScreen.SplashActivity
import com.example.sare.ui.tag.TagsActivity
import com.example.sare.ui.video.UseMusicActivity
import com.example.sare.videoRecording.PortraitCameraActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nanotasks.BackgroundWork
import com.nanotasks.Completion
import com.nanotasks.Tasks
import iknow.android.utils.KeyboardUtil
import io.ktor.client.*
import kotlinx.android.synthetic.main.fragment_video.view.*
import kotlinx.android.synthetic.main.layout_comment_option.view.*
import kotlinx.android.synthetic.main.layout_use_video.view.*
import kotlinx.android.synthetic.main.popup_guest.*
import kotlinx.android.synthetic.main.popup_report.view.*
import kotlinx.android.synthetic.main.popup_share.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@RequiresApi(Build.VERSION_CODES.M) class VideoFragment : Fragment(), VideoPlayerRecyclerView.videoData {

    private val MY_CAMERA_PERMISSION_CODE = 100
    private val MY_WRITE_PERMISSION_CODE = 101
    private var REQUEST_CODE = 102
    val ktor: HttpClient by inject()
    private var mRecyclerView: VideoPlayerRecyclerView? = null
    var dummyData: DummyData? = null
    lateinit var progressDialog: CustomProgressDialogNew
    var videoList: MutableList<VideoListQuery.Video> = mutableListOf()
    private var root: View? = null
    var videoListViewModel: VideoListViewModel? = null
    var adapter: VideoPlayerRecyclerAdapter? = null
    var adapterVideoType: VideoTypeAdapter? = null
    var selectedVideo: VideoListQuery.Video? = null
    var selectedPosition: Int? = null
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    var bottomSheetBehavior1: BottomSheetBehavior<*>? = null
    var userId: String? = ""
    var token: String? = ""
    var id: String? = ""
    var image: String? = ""
    var username: String? = ""
    var name: String? = ""
    var colorCode: String? = ""
    var showRecyclerView: Boolean = false
    var linkToShare = ""
    var statusLikes: Boolean = false
    var any: Boolean = false
    var videoId: String? = ""
    var userStatus: String? = ""
    var newUserId: String? = ""
    val commentList = mutableListOf<CommentsListQuery.Comment>()
    private var currentPage: Int = 0

    var isLoading = false
    var isLastPage = false
    var replyComment: CommentsListQuery.Comment? = null
    var replyCommentPosition: Int? = 0
    var navigationFlag: String = ""

    var videoPosition: Int = 0

    var loginUserId: String? = ""
    var videoLogin = false
    var isVideoLastPage = false
    var isVideoCurrentPage: Int = 1
    var tagName: String? = ""
    lateinit var fragment: Fragment

    var mTextHashTag: HashTag? = null
    private var storiesPagerAdapter: StoriesPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        root = inflater.inflate(R.layout.fragment_video, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (navigationFlag == "ProfilePublic") {
                    // NavHostFragment.findNavController(this@VideoFragment).navigateUp()
                    activity?.finish()
                } else if (navigationFlag == "TagPublic") {
                    activity?.finish()
                } else  activity?.finish()
                // else NavHostFragment.findNavController(this@VideoFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback)

        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        videoListViewModel = ViewModelProvider(this)[VideoListViewModel::class.java]
        userId = preferences.getString("UserID", null)
        token = preferences.getString("TOKEN", null)
        any = preferences.getBoolean("ANONYMOUS", false)
        userStatus = preferences.getString("status", null)

        if (arguments != null) {

            navigationFlag = arguments?.getString("navigationFlagProfile", null).toString()

            root?.ll_options_video?.gone()

            Log.e("Else Paet", " " + navigationFlag)

            if (navigationFlag == "ProfilePublic") {
                root?.tv_follower?.gone()
                userProfileVideo(preferences)
            } else if (navigationFlag == "TagPublic") {
                root?.tv_follower?.gone()
                tagsVideo(preferences)
            } else if (navigationFlag == "UserMusic") {
                root?.tv_follower?.visible()
                userProfileVideo(preferences)
            } else {
                Log.e("SareApp ", "Arguemnt")
                videoId = arguments?.getString("videoId", null)
                newUserId = arguments?.getString("userId", null)
                selectedVideo?._id() == videoId
            }

        } else {
            root?.ll_options_video?.visible()
            progressDialog = CustomProgressDialogNew(context)
            root?.userVideoViewPager!!.gone()
            Log.e("Else Paet", "UserId::")
        }
        id = newUserId

        videoListViewModel?.getReportList(requireContext())

        Log.d("tag", "any::$any")
        if (userStatus.toString() == "BLOCK") {
            MyApplication.mainActivity.logoutUser()
            activity?.openNewActivity(SplashActivity::class.java)
            requireActivity().runOnUiThread {
                customToast(getString(R.string.you_cannot_use_app), requireActivity(), 0)
            }
        }

        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            id = mainObject.getString("_id")
            username = mainObject.getString("username")
            name = mainObject.getString("name")
            image = mainObject.getString("image")
            colorCode = mainObject.getString("colorCode")
            if (mainObject.has("anonymous")) {
                any = mainObject.getBoolean("anonymous")
            }

        }
        if (userId != null) {
            id = userId

        }
        setData(image, colorCode, username)
        root?.bottom_info?.visibility = GONE

        if (any) {
            root?.frameLayout?.setOnClickListener {
                showGuestPopup()
            }
            root?.iv_profile_guest?.setOnClickListener {
                showGuestPopup()
            }

            root?.tv_follower?.setOnClickListener {
                showGuestPopup()
            }

            root?.ll_options_video?.setOnClickListener {
                showGuestPopup()
            }

            root?.ll_right?.setOnClickListener {
                showGuestPopup()
            }

            root?.bottom_info?.setOnClickListener {
                showGuestPopup()
            }

        } else {
            root?.iv_profile_guest?.isEnabled = true
            setListeners()
            setBottomSheet()
            setUseVideoAsBottomSheet()
            setShareVideoBottomSheet("")
            Log.e("Sare App", "" + "Lisgererer")
        }


        initCommentRecyclerView()

        Log.e("navigationFlag", "" + navigationFlag)

        when {
            navigationFlag.isEmpty() -> {
                getVideoList(ArrayList<String>(), "random")
                videoListObserver(ArrayList<String>(), "random")
                setObserver()
            }
            else -> {
                root?.bottom_info?.visibility = VISIBLE

                userVideoListObserver()
                setObserver()
            }
        }
        return root
    }

    private fun setObserver() {

        likeObserver()
        dislikeObserver()

        createCommentObserver()
        getCommentObserver()
        downloadObserver()
        viewCountObserver()
        followObserver()
        unfollowObserver()
    }

    private fun initCommentRecyclerView() {

        root?.recycler_view_comment?.apply {
            layoutManager = LinearLayoutManager(requireContext())

            addOnScrollListener(object :
                com.example.sare.PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getCommentList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }


            })
            val commentAdapter = CommentAdapter(
                commentList, requireContext(), selectedVideo?._id().toString(), recycler_view_comment, navigationFlag)

            commentAdapter.setReplyClickListener(object : CommentAdapter.ReplyClickListener {
                override fun clickReply(comment: CommentsListQuery.Comment, position: Int) {
                    root?.tv_reply_name?.text = "Replying to ${comment.user()?.name()}"
                    root?.cv_reply_view?.visibility = VISIBLE
                    replyComment = comment
                    replyCommentPosition = position
                }
            })

            adapter = commentAdapter

        }

        root?.iv_reply_close?.setOnClickListener {
            root?.cv_reply_view?.visibility = GONE
            replyComment = null
            replyCommentPosition = 0
        }


    }


    private fun showGuestPopup() {

        val dialog = Dialog(requireActivity(), R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_guest)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)

        dialog.btnRegister.setOnClickListener {
            dialog.dismiss()
            MyApplication.mainActivity.logoutUser()
            activity?.openNewActivity(LoginActivity::class.java)

        }


        dialog.show()
    }

    private fun setData(url: String?, colorCode: String?, username: String?) {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)
        /* Handler().post(Runnable {
             requireActivity().runOnUiThread {
                 var image: Bitmap? = null
                 // try {
                 val urlNew = URL(PUBLIC_URL + (url.toString()))
                 Log.d("tag", "colorCode try::::::$colorCode")
                 Log.d("tag", "PUBLIC_URL+(url.toString()):::" + PUBLIC_URL + (url.toString()))


                root?.iv_profile_self!!.loadUrl(Utils.PUBLIC_URL + (url.toString()))

                 if (colorCode!!.isNotEmpty())

                   root?.iv_profile_self1!!.setBorder(colorCode!!)


             }
         })*/


    }


    private fun setListeners() {
        Log.e("SareApp", "Pagination Current page Loading : " + videoLogin)
        Log.e("SareApp", "navigationFlag : " + navigationFlag)
        if (navigationFlag == "TagPublic") {
            mRecyclerView?.apply {
                layoutManager = LinearLayoutManager(requireContext())
                addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                    override fun loadMoreItems() {
                        videoLogin = true
                        isVideoCurrentPage++
                        videoListViewModel?.getTagVideoList(
                            mutableListOf(tagName), loginUserId.toString(), isVideoCurrentPage)
                    }

                    override fun isLastPage(): Boolean {
                        Log.d("tag", "Pagination Current page Last page : " + isVideoLastPage)
                        return isVideoLastPage
                    }

                    override fun isPlayVideo(playVideo: Boolean) {
                        Log.e("SareApp", "Pagination Current page Last playVideo : " + playVideo)
                        playVideo(playVideo)
                    }

                    override fun isLoading(): Boolean {
                        Log.d("tag", "Pagination Current page Loading : " + videoLogin)
                        return videoLogin
                    }


                })

            }
        } else {
            mRecyclerView?.apply {
                layoutManager = LinearLayoutManager(requireContext())
                addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                    override fun loadMoreItems() {
                        videoLogin = true
                        isVideoCurrentPage++
                        videoListViewModel?.getUserVideoList(loginUserId.toString(), isVideoCurrentPage)
                    }

                    override fun isLastPage(): Boolean {
                        Log.d("tag", "Pagination Current page Last page : " + isVideoLastPage)
                        return isVideoLastPage
                    }

                    override fun isPlayVideo(playVideo: Boolean) {
                        Log.e("SareApp", "Pagination Current page Last playVideo : " + playVideo)
                        playVideo(playVideo)
                    }

                    override fun isLoading(): Boolean {
                        Log.d("tag", "Pagination Current page Loading : " + videoLogin)
                        return videoLogin
                    }


                })

            }
        }
        if (navigationFlag == "ProfilePublic") {
            // mRecyclerView!!.post(Runnable { mRecyclerView!!.smoothScrollToPosition(videoPosition) })
        }

        //imp  chattt
        /*  root?.iv_chat?.setOnClickListener {
              val bundle = bundleOf(
                  "userId" to id,
                  "name" to name,
                  "colorCode" to colorCode,
                  "image" to PUBLIC_URL + (image.toString())
              )
              if (findNavController().currentDestination?.id == R.id.homeFragment2) {
                  findNavController().navigate(
                      R.id.action_homeFragment2_to_chatUserFragment,
                      bundle
                  )
              }
          }*/

        /* root?.iv_status?.setOnClickListener {
             if (findNavController().currentDestination?.id == R.id.homeFragment2) {
                 findNavController().navigate(
                     R.id.action_homeFragment2_to_statusMainFragment
                 )
             }
         }*/


        root?.iv_like?.setOnClickListener {
            //animateHeart(like)
            Log.d("tag", "liked:status::::${selectedVideo?.liked()}")
            if (selectedVideo?.liked() == true) {
                disLikeVideo()
            } else {
                likeVideo()
            }
        }

        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val suggestion = preferences.getBoolean("suggestion", true)
        if (suggestion) {
            root?.layout_suggestion?.visibility = VISIBLE
            val gif: ImageView? = root?.imageView5
            Glide.with(requireContext()).load(R.drawable.swipe).into(gif!!)
            root?.layout_suggestion?.setOnClickListener {
                root?.layout_suggestion?.visibility = GONE
                preferences.edit().putBoolean("suggestion", false).apply()
            }
        } else {
            root?.layout_suggestion?.visibility = GONE
        }
        val listener = HashTag.OnHashTagClickListener {
            Log.d("tag", "tagName, it :::::${"#$it"}")
            activity?.openActivity(ExploreTagsActivity::class.java, bundleOf("tagName" to "#$it"))
            Log.d("tag", "startActivity:::")
        }
        root?.tv_desc?.setShadowLayer(root?.tv_desc?.textSize!!, 0f, 0f, Color.TRANSPARENT)

        val additionalSymbols = charArrayOf('#')
        mTextHashTag = HashTag.Creator.create(resources.getColor(R.color.pureWhite), listener, additionalSymbols)
        mTextHashTag!!.handle(root?.tv_desc)
        /* root?.tv_desc?.setOnClickListener {
 //            mTextHashTag = HashTag.Creator.create(resources.getColor(R.color.pureWhite), listner)
 //            mTextHashTag!!.handle(root?.tv_desc)
           HashTag.OnHashTagClickListener {
                 Log.d("tag", "hastag, it :::::$it")
                 Log.d("tag", "tagName, it :::::${"#$it"}")
                 activity?.startActivity(Intent(activity, ExploreTagsActivity::class.java).putExtra("tagName", "#"+it)
                     .putExtra("videoCount", "0"))
                 activity?.overridePendingTransition(0,0)
                 Log.d("tag", "startActivity:::")

             }
         }*/

        /*  mTextHashTag!!.onHashTagClicked {
              Log.d("tag", "hastag, it :::::$it")

          }*/


        root?.overlay_view_report_popup1?.setOnClickListener {
            hideKeyboard(requireActivity())

            root?.overlay_view_report_popup1?.visibility = View.GONE
            root?.layout_popup_report?.visibility = View.GONE
        }
        root?.iv_music?.setOnClickListener {
            val bundle = bundleOf("audioId" to selectedVideo!!.audioId())
            //            findNavController().navigate(R.id.action_homeFragment2_to_useMusicFragment, bundle)
            activity?.openActivity(UseMusicActivity::class.java, bundle)
        }



        root?.iv_save?.setOnClickListener {
            //animateHeart(wish)
            if (selectedVideo?.bookmarked() != null) {
                bookmarkVideo(selectedVideo?.bookmarked()?.not()!!)
            }
        }

        /*   root?.iv_download?.setOnClickListener {
               download(selectedVideo?._id()!!)
               //animateHeart(download)
               dummyData?.let { it1 -> downloadWithFlow(it1) }
               //progressDialog?.show()
           }*/
        Log.d("tag", "mRecyclerView?.isVideoPlayed ::::${mRecyclerView?.isVideoPlayed}")
        Log.d("tag", "selectedVideo?._id() ::::${selectedVideo?._id().toString()}")

        if (mRecyclerView?.isVideoPlayed == true) {
            viewCount(selectedVideo?._id().toString())
        }

        root?.ll_share?.setOnClickListener {
            //animateHeart(share)
            /*val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, selectedVideo?.user()?.username())
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)*/

            Toast.makeText(
                requireContext(), "Currently not working because some facebook share violence", Toast.LENGTH_SHORT)
                .show()
            //dummyData?.let { it1 -> openShareApps(it1) }

        }

        /* root?.iv_add_video?.setOnClickListener {
             openPortraitCameraActivity()
         }

         root?.iv_deo?.setOnClickListener {
             //animateHeart(duet)
             // setUseVideoAsBottomSheet()

         }

         root?.iv_add?.setOnClickListener {
             openPortraitCameraActivity()
         }*/

        /*root?.ll_home_bug?.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment2_to_fragementReportApplicationBug)
        }*/

        root?.type_all?.setOnClickListener {
            REQUEST_CODE = 102
            if (root?.type_all?.alpha ?: 0f < 1f) {
                if (mRecyclerView != null) {
                    mRecyclerView!!.releasePlayer()
                }
                root?.type_all?.setTextColor(Color.parseColor("#FFFFFF"))
                root?.type_all?.alpha = 1f
                root?.txtFollowing?.alpha = 0.5f
                root?.txtHashtag?.alpha = 0.5f

                adapterVideoType?.currentPosition = -1
                adapterVideoType?.notifyDataSetChanged()
                getVideoList(ArrayList<String>(), "random")
            }
        }
        root?.txtFollowing?.setOnClickListener {
            REQUEST_CODE = 101
            if (mRecyclerView != null) {
                mRecyclerView!!.releasePlayer()
            }
            root?.txtFollowing?.alpha = 1f
            root?.type_all?.alpha = 0.5f
            root?.txtHashtag?.alpha = 0.5f
            adapterVideoType?.currentPosition = -1
            adapterVideoType?.notifyDataSetChanged()
            getVideoList(ArrayList<String>(), "following")
        }

        root?.txtHashtag?.setOnClickListener {
            root?.txtFollowing?.alpha = 0.5f
            root?.txtHashtag?.alpha = 1f
            root?.type_all?.alpha = 0.5f
            val intent = Intent(activity, TagsActivity::class.java)
            startActivityForResult(intent, 102)
            activity?.overridePendingTransition(0, 0)

        }

        /* root?.iv_more?.setOnClickListener {
             it.visibility = View.INVISIBLE
             it.isClickable = false
             addBottomToTopAnimation()
             visibleAnimation(root?.iv_close!!)
         }

         root?.iv_close?.setOnClickListener {
             it.visibility = View.INVISIBLE
             it.isClickable = false
             addTopToBottomAnimation()
             visibleAnimation(root?.iv_more!!)
         }*/
        Log.d("tag", "main::::${root?.tv_follower?.text}")

        root?.tv_follower?.setOnClickListener {
            if (selectedVideo?.user()?.followed() == true) {
                unFollowUser(selectedVideo?.user()?._id()!!)
            } else {
                followUser(selectedVideo?.user()?._id()!!)
            }
        }

        root?.tv_send_comment?.setOnClickListener {

            if (root?.bottom_sheet_options?.et_message_comment?.text!!.isEmpty()) {
                Toast.makeText(requireContext(), "Please write a comment", Toast.LENGTH_SHORT).show()
            } else {
                if (root?.cv_reply_view?.visibility == VISIBLE) {
                    val commentAdapter = root?.recycler_view_comment?.adapter as CommentAdapter
                    commentAdapter.createComment(
                        root?.cv_reply_view,
                        root?.bottom_sheet_options?.et_message_comment?.text.toString().trim(),
                        selectedVideo?._id()!!,
                        replyComment?._id()!!,
                        replyCommentPosition!!,
                        0)
                    root?.bottom_sheet_options?.et_message_comment?.setText("")
                    hideKeyboard(requireActivity())
                } else {
                    createComment()
                }
            }
            //            root?.bottom_sheet_options?.et_message_comment?.setText("")
        }
        //        root?.tv_follower?.setOnClickListener {
        //            Log.d("tag","root?.tv_follower?.text::::${root?.tv_follower?.text}")
        ////            if (root?.tv_follower?.text == "•  Following") {
        ////                unFollowUser(selectedVideo?.user()?._id()!!)
        ////            } else {
        //                followUser(selectedVideo?.user()?._id()!!)
        ////            }
        //        }
    }


    fun openPortraitCameraActivity() {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO), MY_CAMERA_PERMISSION_CODE)
        } else {
            onStop()
            PortraitCameraActivity.startActivity(activity)
        }
    }

    private fun visibleAnimation(view: View) {

        view.animate().alpha(10F).setDuration(100).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = VISIBLE
                view.isClickable = true
                /* if (root?.iv_add?.isVisible!!)
                     root?.iv_add?.visibility = View.GONE
                 else
                     root?.iv_add?.visibility = View.VISIBLE*/
            }
        })

    }


    private fun addTopToBottomAnimation() {
        val sliderDown = AnimationUtils.loadAnimation(
            requireContext(), R.anim.timer_slide_down)

        root?.ll_right?.startAnimation(sliderDown)
        root?.ll_right?.visibility = GONE

    }

    private fun addBottomToTopAnimation() {
        val slideUp = AnimationUtils.loadAnimation(
            requireContext(), R.anim.slide_up)

        root?.ll_right?.startAnimation(slideUp)
        root?.ll_right?.visibility = VISIBLE
    }

    private fun initRecyclerView(arrayList: List<VideoListQuery.Video>) {
        mRecyclerView = root?.recycler_view
        val layoutManager = LinearLayoutManager(context)
        mRecyclerView?.layoutManager = layoutManager
        val snapHelper: SnapHelper = PagerSnapHelper()
        mRecyclerView?.onFlingListener = null
        snapHelper.attachToRecyclerView(mRecyclerView)
        adapter = context?.let {
            VideoPlayerRecyclerAdapter(
                it)
        }
        videoList = arrayList.toMutableList()
        mRecyclerView?.adapter = adapter
        mRecyclerView?.setMediaObjects(videoList)
        adapter?.setVideoListData(videoList as ArrayList<VideoListQuery.Video>)

        mRecyclerView?.setDownloadListener(this)
        mRecyclerView?.smoothScrollBy(0, 1) // for top
        requireActivity().window.statusBarColor = resources.getColor(
            R.color.black, requireContext().theme)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    private fun getVideoList(categoryList: List<String>, type: String) {
        progressDialog = CustomProgressDialogNew(context)
        Log.d("tag", "categoryList:::$categoryList")
        videoListViewModel?.getVideoList1(categoryList, id.toString(), type)

    }

    fun videoListObserver(categoryList: List<String>, type: String) {
        progressDialog.show()
        videoListViewModel?.videoArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.dismiss()
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getVideoList1(categoryList, id.toString(), type)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.dismiss()
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.dismiss()
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.videoList -> {
                    progressDialog.dismiss()
                    root?.bottom_info?.visibility = VISIBLE
                    //                    root?.iv_add?.visibility = VISIBLE
                    //                    root?.ll_bottom?.visibility = VISIBLE
                    //root?.ll_right?.visibility = VISIBLE
                    videoList = it.response.toMutableList()
                    //initRecyclerView(it.response)
                    addVideoList()
                }

            }

        })
    }

    fun userVideoListObserver() {

        videoListViewModel?.userVideoArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            //videoListViewModel?.getVideoList1(categoryList, id.toString(), type)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.userVideoPublishedList -> {
                    videoLogin = false
                    if (it.response.size < Utils.PAGE_SIZE) {
                        isVideoLastPage = true
                    }

                    val gson = Gson()
                    val userInfoListJsonString = gson.toJson(it.response)
                    val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                    val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(
                        userInfoListJsonString, myType)


                    videoList.addAll(userVideoList)
                    Log.e("SareApp App", "Call Api Size :" + userVideoList.size)
                    Log.e("SareApp App", "Call Api Size :" + videoList.size)
                    addVideoList()
                    /// mRecyclerView?.setMediaObjects(videoList)
                    //   adapter?.setVideoListData(videoList as ArrayList<VideoListQuery.Video>)
                    //   adapter?.notifyDataSetChanged()

                    //  storiesPagerAdapter.updateVideoList(userVideoList as ArrayList<VideoListQuery.Video>)

                }

            }
        })
    }


    private fun getCategoryList() {

        videoListViewModel?.getCategoryList()

        videoListViewModel?.categoryArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getCategoryList()
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.categoryList -> {
                    root?.menu_recycler_view?.apply {
                        layoutManager = LinearLayoutManager(
                            root?.menu_recycler_view?.context, RecyclerView.HORIZONTAL, false)
                        adapterVideoType = VideoTypeAdapter(it.response, context)
                        adapterVideoType?.setOnClickListener(object : VideoTypeAdapter.OnClickListener {
                            override fun onClick(position: Int, value: String?) {
                                if (mRecyclerView != null) {
                                    mRecyclerView!!.releasePlayer()
                                }
                                root?.type_all?.setTextColor(Color.parseColor("#99FFFFFF"))
                                val list = arrayListOf<String>(value!!)
                                getVideoList(list, "random")
                            }
                        })
                        adapter = adapterVideoType

                    }
                }

            }

        })


    }

    private fun createComment() {
        var comment = root?.bottom_sheet_options?.et_message_comment?.text.toString()
        Log.d("tag", "commnets::::$comment")
        videoListViewModel?.createComment(
            comment, selectedVideo?._id().toString(), "")

    }

    fun createCommentObserver() {
        var comment = root?.bottom_sheet_options?.et_message_comment?.text.toString()

        videoListViewModel?.createComment?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.createComment(
                                comment, selectedVideo?._id().toString(), "")
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.createCommentList -> {
                    Log.d("tag", "comment:: ${it.response}")
                    //getCommentList()
                    val user = CommentsListQuery.User(
                        it.response.user()?._id()!!,
                        it.response.user()?.__typename()!!,
                        it.response.user()?.name(),
                        it.response.user()?.username(),
                        it.response.user()?.image(),
                        it.response.user()?.colorCode())
                    val comment = CommentsListQuery.Comment(
                        it.response.__typename(),
                        it.response._id(),
                        it.response.comment(),
                        it.response.commentedAt(),
                        user,
                        it.response.liked(),
                        it.response.likeCount(),
                        0,
                        null)
                    commentList.add(0, comment)
                    videoList[selectedPosition!!] = VideoListQuery.Video(
                        selectedVideo?.user()?._id()!!,
                        selectedVideo?.__typename()!!,
                        selectedVideo?.title()!!,
                        selectedVideo?._id()!!,
                        selectedVideo?.audioId(),
                        selectedVideo?.likeCounts()!!,
                        selectedVideo?.views()!!,
                        selectedVideo?.liked(),
                        selectedVideo?.name()!!,
                        selectedVideo?.imageName(),
                        (selectedVideo?.commentCounts() ?: 0) + 1,
                        selectedVideo?.description(),
                        selectedVideo?.bookmarked(),
                        selectedVideo?.audio(),
                        selectedVideo?.user()!!)
                    selectedVideo = videoList[selectedPosition!!]

                    Log.d("Comment", "Comment Count count : ${selectedVideo?.commentCounts()}")
                    root?.tv_comment_count?.text = selectedVideo?.commentCounts().toString()
                    root?.recycler_view_comment?.adapter?.notifyDataSetChanged()
                    root?.bottom_sheet_options?.et_message_comment?.setText("")
                    if (root?.recycler_view_comment?.adapter?.itemCount != 0) {
                        root?.txtFirstComment?.visibility = GONE
                        root?.recycler_view_comment?.visibility = VISIBLE
                    } else {
                        root?.txtFirstComment?.visibility = VISIBLE
                        root?.recycler_view_comment?.visibility = GONE
                    }
                    hideKeyboard(requireActivity())

                }

            }

        })

    }

    private fun getCommentList() {

        videoListViewModel?.getCommentList(selectedVideo?._id().toString(), currentPage)

    }


    fun getCommentObserver() {
        videoListViewModel?.commentArrayList?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getCommentList(
                                selectedVideo?._id().toString(), currentPage)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.commentList -> {
                    Log.d("tag", "commentlist::${it.response}")
                    if (it.response.isNotEmpty()) {
                        root?.txtFirstComment?.visibility = GONE
                        root?.recycler_view_comment?.visibility = VISIBLE

                        Log.d("tag", "it.response:::${it.response}")

                        commentList.addAll(it.response)
                        root?.recycler_view_comment?.adapter?.notifyDataSetChanged()
                    } else {
                        Log.d("tag", "No data")
                        root?.txtFirstComment?.visibility = VISIBLE
                        root?.recycler_view_comment?.visibility = GONE
                    }

                    if (bottomSheetBehavior?.state != BottomSheetBehavior.STATE_EXPANDED) {

                    }
                }

            }

        })

    }

    private fun bookmarkVideo(action: Boolean) {

        videoListViewModel?.bookmarkVideo(selectedVideo?._id()!!, action)

        videoListViewModel?.bookmarkVideo?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.bookmarkVideo(selectedVideo?._id()!!, action)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.bookmarkVideo -> {
                    if (it.response.status()) {
                        videoList[selectedPosition!!] = VideoListQuery.Video(
                            selectedVideo?.user()?._id()!!,
                            selectedVideo?.__typename()!!,
                            selectedVideo?.title()!!,
                            selectedVideo?._id()!!,
                            selectedVideo?.audioId(),
                            selectedVideo?.likeCounts()!!,
                            selectedVideo?.views()!!,
                            selectedVideo?.liked(),
                            selectedVideo?.name()!!,
                            selectedVideo?.imageName(),
                            selectedVideo?.commentCounts()!!,
                            selectedVideo?.description(),
                            action,
                            selectedVideo?.audio(),
                            selectedVideo?.user()!!)
                        selectedVideo = videoList[selectedPosition!!]
                        if (action) {
                            root?.txtSave?.text = "Unsave"
                            root?.iv_save?.setImageResource(R.drawable.ic_unsave)
                        } else {
                            root?.iv_save?.setImageResource(R.drawable.ic_save)
                            root?.txtSave?.text = "Save"
                        }
                    } else {

                    }

                }

            }

        })


    }

    private fun likeVideo() {
        root?.iv_like?.isEnabled = false
        videoListViewModel?.videoLike(selectedVideo?._id()!!)
    }

    private fun likeObserver() {
        videoListViewModel?.videoLike?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.videoLike(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                    root?.iv_like?.isEnabled = true
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                    root?.iv_like?.isEnabled = true
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    // customToast(it.errorMsg, requireActivity(),0)
                    root?.iv_like?.isEnabled = true
                }

                is NetworkResponse.SUCCESS.videoLike -> {
                    if (it.response.status()) {
                        Log.d(
                            "tag", "like counts:before::${selectedVideo?.likeCounts().toString()}")

                        videoList[selectedPosition!!] = VideoListQuery.Video(
                            selectedVideo?.user()?._id()!!,
                            selectedVideo?.__typename()!!,
                            selectedVideo?.title()!!,
                            selectedVideo?._id()!!,
                            selectedVideo?.audioId(),
                            selectedVideo?.likeCounts()!! + 1,
                            selectedVideo?.views()!!,
                            true,
                            selectedVideo?.name()!!,
                            selectedVideo?.imageName(),
                            selectedVideo?.commentCounts()!!,
                            selectedVideo?.description(),
                            selectedVideo?.bookmarked(),
                            selectedVideo?.audio(),
                            selectedVideo?.user()!!)
                        selectedVideo = videoList[selectedPosition!!]
                        statusLikes = true
                        //                        root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
                        if (statusLikes == true) {
                            root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
                        } else {
                            root?.iv_like?.setImageResource(R.drawable.ic_home_like)
                        }
                        Log.d("tag", "like counts:::${selectedVideo?.likeCounts().toString()}")
                        if (selectedVideo!!.likeCounts() < 0) {
                            root?.tv_like_count?.text = 0.toString()
                        } else {
                            root?.tv_like_count?.text = selectedVideo?.likeCounts().toString()
                        }
                        root?.tv_video_count?.text = selectedVideo?.views().toString()
                    } else {
                        //                        root?.iv_like?.setImageResource(R.drawable.ic_home_like)

                    }
                    root?.iv_like?.isEnabled = true

                }

            }

        })
    }

    private fun disLikeVideo() {

        root?.iv_like?.isEnabled = false
        videoListViewModel?.videoDisLike(selectedVideo?._id()!!)

    }


    private fun dislikeObserver() {

        videoListViewModel?.videoDisLike?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.videoDisLike(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                    root?.iv_like?.isEnabled = true
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                    root?.iv_like?.isEnabled = true
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    root?.iv_like?.isEnabled = true
                }

                is NetworkResponse.SUCCESS.videoDisLike -> {
                    if (it.response.status()) {
                        videoList[selectedPosition!!] = VideoListQuery.Video(
                            selectedVideo?.user()?._id()!!,
                            selectedVideo?.__typename()!!,
                            selectedVideo?.title()!!,
                            selectedVideo?._id()!!,
                            selectedVideo?.audioId(),
                            selectedVideo?.likeCounts()!! - 1,
                            selectedVideo?.views()!!,
                            false,
                            selectedVideo?.name()!!,
                            selectedVideo?.imageName(),
                            selectedVideo?.commentCounts()!!,
                            selectedVideo?.description(),
                            selectedVideo?.bookmarked(),
                            selectedVideo?.audio(),
                            selectedVideo?.user()!!)
                        selectedVideo = videoList[selectedPosition!!]
                        //                        root?.iv_like?.setImageResource(R.drawable.ic_home_like)
                        statusLikes = false
                        if (statusLikes == false) {
                            root?.iv_like?.setImageResource(R.drawable.ic_home_like)
                        } else {
                            root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
                        }
                        Log.d("tag", "dislike counts:::${selectedVideo?.likeCounts().toString()}")

                        if (selectedVideo!!.likeCounts() < 0) {
                            root?.tv_like_count?.text = 0.toString()
                        } else {
                            root?.tv_like_count?.text = selectedVideo?.likeCounts().toString()
                        }
                    } else {
                        //                        root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
                    }
                    root?.iv_like?.isEnabled = true

                }

            }

        })
    }

    override fun onStop() {
        super.onStop()
        if (mRecyclerView != null) {
            mRecyclerView!!.pauseVideo()
            root?.imgPlay?.visibility = VISIBLE
        } else {
            root?.imgPlay?.visibility = GONE

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        //getActivity()?.getWindow()?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (mRecyclerView != null) mRecyclerView!!.releasePlayer()
        if (mRecyclerView != null) mRecyclerView!!.removeHandler()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (mRecyclerView != null) mRecyclerView!!.releasePlayer()
        if (mRecyclerView != null) mRecyclerView!!.removeHandler()

    }

    private fun animateHeart(view: ImageView) {
        val scaleAnimation = ScaleAnimation(
            1.0f, 0.5f, 1.0f, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        prepareAnimation(scaleAnimation)
        val animation = AnimationSet(true)
        animation.addAnimation(scaleAnimation)
        animation.duration = 200
        animation.fillAfter = true
        view.startAnimation(animation)
    }

    private fun prepareAnimation(animation: Animation): Animation? {
        animation.repeatCount = 1
        animation.repeatMode = Animation.REVERSE
        return animation
    }

    private fun downloadWithFlow(dummy: DummyData) {
        if (ActivityCompat.checkSelfPermission(
                requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_WRITE_PERMISSION_CODE)
        } else {
            val progressDialog = CustomProgressDialogNew(activity)
            progressDialog.show()

            Log.d("tag", "dummy:::${dummy.url}")
            CoroutineScope(Dispatchers.IO).launch {
                ktor.downloadFile(dummy.file, dummy.url).collect {
                    withContext(Dispatchers.Main) {
                        when (it) {
                            is DownloadResult.Success -> {
                                Log.d("tag", "success:::")
                                progressDialog.hide()
                            }
                            is DownloadResult.Error -> {
                                Log.d("tag", "error:::" + it.message)
                            }
                            is DownloadResult.Progress -> {
                                Log.d("tag", "progresss:::" + it.progress)
                                progressDialog.setProgress(it.progress)
                            }
                            else -> "Throws Error.!!"
                        }
                    }
                }
            }
        }

    }

    fun setUserDataFromCurrentVideoItem(position: Int?) {

        Log.d("Video", "DATA DATA Position : $position")
        selectedPosition = position
        selectedVideo = videoList[position!!]

        var url = "https://www.roleit.in/shareit/?" + selectedVideo!!._id()

        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)


        setShareVideoBottomSheet(url)
        root?.iv_profile_guest?.setOnClickListener {
            if (selectedVideo!!.user()?._id() == id) {
                Log.d("tag", "selectedVideo!!.user()._id()::::${selectedVideo!!.user()?._id()}")
                val bundle = bundleOf("type" to "5")
                activity?.runOnUiThread {
                    activity?.openActivity(BottomBarActivity::class.java, bundle)
                }

            } else {
                Log.d("tag", "selectedVideo!!.user()._id()::::${selectedVideo!!.user()?._id()}")
                val bundle = bundleOf("userId" to selectedVideo!!.user()?._id())
                activity?.runOnUiThread {
                    activity?.openActivity(ProfileGuestActivity::class.java, bundle)
                }
            }
        }

        root?.tv_user?.setOnClickListener {
            if (selectedVideo!!.user()?._id() == id) {
                Log.d("tag", "selectedVideo!!.user()._id()::::${selectedVideo!!.user()?._id()}")
                val bundle = bundleOf("type" to "5")
                activity?.runOnUiThread {
                    activity?.openActivity(BottomBarActivity::class.java, bundle)
                }

            } else {
                Log.d("tag", "selectedVideo!!.user()._id()::::${selectedVideo!!.user()?._id()}")
                val bundle = bundleOf("userId" to selectedVideo!!.user()?._id())
                activity?.runOnUiThread {
                    activity?.openActivity(ProfileGuestActivity::class.java, bundle)
                }
            }
        }


        root?.iv_profile_guest!!.loadUrl(
            Utils.PUBLIC_URL + (selectedVideo?.user()?.image().toString()))

        if (selectedVideo?.user()?.colorCode()!!.isNotEmpty()) root?.iv_profile_guest1!!.setBorder(
            selectedVideo?.user()?.colorCode()!!)



        root?.tv_user?.text = "@${selectedVideo?.user()?.username()}"


        root?.txtUserName?.text = selectedVideo?.user()?.username()
        /*  root?.iv_download?.setOnClickListener {
              download(selectedVideo?._id()!!)
              //animateHeart(download)
              dummyData?.let { it1 -> downloadWithFlow(it1) }
              //progressDialog?.show()
          }*/

        root?.iv_like?.setOnClickListener {
            //animateHeart(like)
            Log.d("tag", "liked:status::::${selectedVideo?.liked()}")
            if (selectedVideo?.liked() == true) {
                disLikeVideo()
            } else {
                likeVideo()
            }
        }
        root?.iv_save?.setOnClickListener {
            //animateHeart(wish)
            if (selectedVideo?.bookmarked() != null) {
                bookmarkVideo(selectedVideo?.bookmarked()?.not()!!)
            }
        }
        root?.tv_send_comment?.setOnClickListener {

            if (root?.bottom_sheet_options?.et_message_comment?.text!!.isEmpty()) {
                Toast.makeText(requireContext(), "Please write a comment", Toast.LENGTH_SHORT).show()
            } else {
                if (root?.cv_reply_view?.visibility == VISIBLE) {
                    val commentAdapter = root?.recycler_view_comment?.adapter as CommentAdapter
                    commentAdapter.createComment(
                        root?.cv_reply_view,
                        root?.bottom_sheet_options?.et_message_comment?.text.toString().trim(),
                        selectedVideo?._id()!!,
                        replyComment?._id()!!,
                        replyCommentPosition!!,
                        0)
                    root?.bottom_sheet_options?.et_message_comment?.setText("")
                    hideKeyboard(requireActivity())
                } else {
                    createComment()
                }
            }
            //            root?.bottom_sheet_options?.et_message_comment?.setText("")
        }

        root?.tv_video_count?.text = selectedVideo?.views().toString()
        if (selectedVideo?.description().toString().isNotEmpty()) {
            root?.tv_desc?.visible()
            root?.tv_desc?.text = selectedVideo?.description()
        } else {
            //root?.tv_desc?.gone()
        }

        root?.tv_comment_count?.text = selectedVideo?.commentCounts().toString()

        if (selectedVideo?.likeCounts().toString() < 0.toString()) {
            root?.tv_like_count?.text = 0.toString()
        } else {
            root?.tv_like_count?.text = selectedVideo?.likeCounts().toString()
        }
        if (selectedVideo?.bookmarked() == null) {
            root?.iv_save?.setImageResource(R.drawable.ic_save)
        } else if (selectedVideo?.bookmarked()!!) {
            root?.iv_save?.setImageResource(R.drawable.ic_unsave)
            root?.txtSave?.text = "Unsave"
        } else {
            root?.iv_save?.setImageResource(R.drawable.ic_save)
            root?.txtSave?.text = "Save"
        }

        if (selectedVideo?.liked() == null) {
            root?.iv_like?.setImageResource(R.drawable.ic_home_like)
        } else if (selectedVideo?.liked()!!) {
            root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
        } else {
            root?.iv_like?.setImageResource(R.drawable.ic_home_like)
        }

        if (selectedVideo?.user()?.followed()!!) {
            root?.tv_follower?.text = "•  Following"

        } else {
            root?.tv_follower?.text = "•  Follow"
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ISO_DATE_TIME
            val formatted = current.format(formatter)
            val urlVideo = getVideoUrl(selectedVideo!!.name())
            dummyData = urlVideo.let { DummyData(url = it, title = formatted) }
        }
        root?.iv_download?.setOnClickListener {
            download(selectedVideo?._id()!!)
            //animateHeart(download)
            dummyData?.let { it1 -> downloadWithFlow(it1) }
            //progressDialog?.show()
        }
        root?.bottom_info?.visibility = VISIBLE

    }

    override fun download(position: Int, url: String?) {

        Log.d("Video", "DATA DATA Position : $position")
        Log.d("Video", "DATA DATA url : $url")
        selectedPosition = position
        selectedVideo = videoList[position]
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)


        root?.iv_profile_guest?.setOnClickListener {
            val bundle = bundleOf("userId" to selectedVideo!!.user()?._id())
            activity?.openActivity(ProfileGuestActivity::class.java, bundle)
        }


        root?.iv_profile_guest!!.loadUrl(
            Utils.PUBLIC_URL + (selectedVideo?.user()?.image().toString()))

        if (selectedVideo?.user()?.colorCode()!!.isNotEmpty()) root?.iv_profile_guest1!!.setBorder(
            selectedVideo?.user()?.colorCode()!!)


        root?.tv_user?.text = "@${selectedVideo?.user()?.username()}"

        root?.txtUserName?.text = selectedVideo?.user()?.username()

        root?.tv_video_count?.text = selectedVideo?.views().toString()
        if (selectedVideo?.description().toString().isNotEmpty()) {
            root?.tv_desc?.visible()
            root?.tv_desc?.text = selectedVideo?.description()
        } else {
//            root?.tv_desc?.gone()
        }

        root?.tv_comment_count?.text = selectedVideo?.commentCounts().toString()

        if (selectedVideo?.likeCounts().toString() < 0.toString()) {
            root?.tv_like_count?.text = 0.toString()
        } else {
            root?.tv_like_count?.text = selectedVideo?.likeCounts().toString()
        }
        if (selectedVideo?.bookmarked()!!) {
            root?.iv_save?.setImageResource(R.drawable.ic_unsave)
            root?.txtSave?.text = "Unsave"
        } else {
            root?.iv_save?.setImageResource(R.drawable.ic_save)
            root?.txtSave?.text = "Save"
        }
        if (selectedVideo?.liked()!!) {
            root?.iv_like?.setImageResource(R.drawable.ic_home_like_selected)
        } else {
            root?.iv_like?.setImageResource(R.drawable.ic_home_like)
        }

        if (selectedVideo?.user()?.followed()!!) {
            root?.tv_follower?.text = "•  Following"

        } else {
            root?.tv_follower?.text = "•  Follow"

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ISO_DATE_TIME
            val formatted = current.format(formatter)
            dummyData = url?.let {
                DummyData(
                    url = it, title = formatted)
            }
        }
        root?.bottom_info?.visibility = VISIBLE

    }

    override fun playVideo(position: Int) {
        Log.e("onPlayerStateChanged", "Call Api")
        Log.e("onPlayerStateChanged iD", selectedVideo?._id()!!)
        videoListViewModel?.viewCount(selectedVideo?._id()!!)
    }

    private fun setUseVideoAsBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_use_video_as

        var bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        bottomSheetBehavior.isDraggable = false

        /*root?.iv_deo?.setOnClickListener {
            val duestBottomDialog = DuestBottomDialog()
            duestBottomDialog.show(requireActivity().supportFragmentManager, duestBottomDialog.tag)
        }*/

        root?.overlay_view_use_video?.setOnClickListener {

            root?.overlay_view_use_video?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        root?.overlay_view_use_video?.setOnClickListener {

            root?.overlay_view_use_video?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }
        bottomSheetBehavior.isDraggable = false

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_use_video?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_use_video?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_use_video?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        // root?.overlay_view_use_video?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun setShareVideoBottomSheet(url: String) {
        linkToShare = url
        // linkToShare = "SARE APP \n \n https://sare.in/${id}"

        val llBottomSheet = root?.bottom_sheet_share_video

        bottomSheetBehavior1 = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        root?.ll_share?.setOnClickListener {
            root?.bottom_sheet_share_video?.visibility = VISIBLE
            root?.overlay_view_share_video?.visibility = View.VISIBLE
            if (bottomSheetBehavior1?.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior1?.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior1?.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }
        root?.cv_facebook?.setOnClickListener {
            var facebookAppFound = false

            var shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
            val pm = requireContext().packageManager
            val activityList: List<ResolveInfo> = pm.queryIntentActivities(shareIntent, 0)
            for (app in activityList) {
                if (app.activityInfo.packageName.contains("com.facebook.katana")) {
                    val activityInfo: ActivityInfo = app.activityInfo
                    val name = ComponentName(
                        activityInfo.applicationInfo.packageName, activityInfo.name)
                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                    shareIntent.component = name
                    facebookAppFound = true
                    break
                }
            }
            if (!facebookAppFound) {
                val url = linkToShare
                val sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=$url"
                shareIntent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
            }
            requireContext().startActivity(shareIntent)
        }

        root?.cv_insta?.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "image/*"
                shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                shareIntent.setPackage("com.instagram.android")
                requireActivity().startActivity(shareIntent)

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error:::${e.message}")
                customToast("Instagram has not been installed", requireActivity(), 0)
            }

        }

        root?.cv_whats_app?.setOnClickListener {

            val pm: PackageManager = requireContext().packageManager
            try {
                val waIntent = Intent(Intent.ACTION_SEND)

                waIntent.type = "text/plain"
                val info: PackageInfo = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp")
                waIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                requireContext().startActivity(
                    Intent.createChooser(waIntent, "Share to"))
            } catch (e: PackageManager.NameNotFoundException) {
                customToast(getString(R.string.share_whatsapp_not_instaled), requireActivity(), 0)
            }
        }

        root?.cv_copy?.setOnClickListener {
            val clipboardManager = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clipData = ClipData.newPlainText("text", linkToShare)
            clipboardManager.setPrimaryClip(clipData)
            Log.d("tag", "clip data::$clipData")
            customToast("Link copied to clipboard!!", requireActivity(), 1)
        }


        root?.ll_mail?.setOnClickListener {
            //            //need this to prompts email client only
            val send = Intent(Intent.ACTION_SENDTO)
            val uriText = "mailto:" + "?subject=" + Uri.encode("Download Sare App") + "&body=" + Uri.encode(linkToShare)
            val uri = Uri.parse(uriText)

            send.data = uri

            try {
                startActivity(Intent.createChooser(send, "Share to"))
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("tag", "error:::${e.message}")
                customToast("Mail not send!!", requireActivity(), 0)
            }
        }

        root?.ll_more?.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_TEXT, linkToShare)
            startActivity(Intent.createChooser(share, "Share to"))

        }


        root?.ll_report?.setOnClickListener {
            root?.bottom_sheet_share_video?.visibility = GONE
            root?.overlay_view_share_video?.visibility = View.GONE
            root?.rvReportList?.visibility = GONE
            root?.overlay_view_report_popup1?.visibility = View.VISIBLE
            root?.layout_popup_report?.visibility = View.VISIBLE
            root?.rvReportList?.visibility = GONE
            root?.txtTitle?.setText("")
            root?.txtReportReason?.text = ""
            root?.txtTypeSomething?.setText("")
            root?.overlay_view_report_popup1?.visibility = View.VISIBLE
            root?.layout_popup_report?.visibility = View.VISIBLE
            root?.ll_select_report_reason?.setOnClickListener {
                root?.rvReportList?.visibility = View.VISIBLE

                reportList()
                if (showRecyclerView == true) {

                    root?.rvReportList?.visibility = View.GONE
                    reportList()
                    showRecyclerView = false

                } else {
                    root?.rvReportList?.visibility = View.VISIBLE
                    showRecyclerView = true
                }
            }


            root?.btnReportSend?.setOnClickListener {
                if (root?.txtReportReason?.text!!.isEmpty() || root?.txtTitle?.text!!.isEmpty()) customToast(
                    "Please select report reason!!", requireActivity(), 0)
            }

        }
        root?.ll_home_bug?.setOnClickListener {
            //            findNavController().navigate(R.id.reportApplicationBugFragment)
            activity?.openActivity(ReportApplicationActivity::class.java)
            activity?.overridePendingTransition(0, 0)
        }
        root?.overlay_view_report_popup1?.setOnClickListener {

            root?.overlay_view_share_video?.visibility = View.GONE
            root?.bottom_sheet_share_video?.visibility = View.GONE
            bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN
            hideKeyboard(requireActivity())

            root?.rvReportList?.visibility = GONE
            root?.overlay_view_report_popup1?.visibility = View.GONE
            root?.layout_popup_report?.visibility = View.GONE
            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }
        /* root?.iv_download?.setOnClickListener {
             download(selectedVideo?._id()!!)
             //animateHeart(download)
             dummyData?.let { it1 -> downloadWithFlow(it1) }
             //progressDialog?.show()
         }*/

        root?.overlay_view_share_video?.setOnClickListener {

            root?.overlay_view_share_video?.visibility = View.GONE
            root?.bottom_sheet_share_video?.visibility = View.GONE

            bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehavior1?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_share_video?.visibility = View.GONE
                        bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_share_video?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_share_video?.visibility = View.GONE
                        bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_share_video?.visibility = View.GONE
                        bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_EXPANDED
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun setBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_options

        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        root?.iv_comment?.setOnClickListener {
            root?.overlay_view_guest?.visibility = View.VISIBLE
            bottomSheetBehavior!!.isDraggable = false
            if (bottomSheetBehavior?.state != BottomSheetBehavior.STATE_EXPANDED) {
                //getCommentList()
                val url = URL(Utils.PUBLIC_URL + (selectedVideo?.user()?.image().toString()))
                var image: Bitmap? = null
                Log.d(
                    "tag",
                    "PUBLIC_URL + (selectedVideo?.user()?.image().toString():::${PUBLIC_URL + selectedVideo?.user()
                        ?.image().toString()}")
                root?.iv_comment_profile_1?.loadUrl(PUBLIC_URL + selectedVideo?.user()?.image().toString())
                root?.imgProfileBorder?.setBorder(selectedVideo?.user()?.colorCode().toString())
                root?.tv_comment_name?.text = selectedVideo?.user()?.name()
                root?.tv_comment_user?.text = "@${selectedVideo?.user()?.username()}"
                if (selectedVideo?.description().toString().isNotEmpty()) {
                    root?.tv_comment_description?.visible()
                    root?.tv_comment_description?.text = selectedVideo?.description()
                } else {
                    root?.tv_comment_description?.gone()
                }
                if (selectedVideo?.user()?.followed()!!) {
                    root?.tv_comment_follower?.text = "Following"
                } else {
                    root?.tv_comment_follower?.text = "Follow"
                }
                root?.tv_comment_follower?.setOnClickListener {
                    if (root?.tv_comment_follower?.text == "Following") {
                        unFollowUser(selectedVideo?.user()?._id()!!)
                    } else {
                        followUser(selectedVideo?.user()?._id()!!)
                    }
                }
                commentList.clear()
                root?.recycler_view_comment?.adapter?.notifyDataSetChanged()
                currentPage = 0
                getCommentList()
                bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }
        root?.imgCommentClose?.setOnClickListener {
            root?.overlay_view_guest?.visibility = View.GONE
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        root?.overlay_view_guest?.setOnClickListener {

            root?.overlay_view_guest?.visibility = View.GONE
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        /* root?.overlay_view_report_popup?.setOnClickListener {
             *//*root?.overlay_view?.setBackgroundColor(

                ThemeManager.colors(
                    requireContext(),
                    StringSingleton.bodyBackground
                )
            )*//*

            root?.overlay_view_report_popup?.visibility = View.GONE
            root?.layout_popup_report?.visibility = View.GONE
            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }*/
        /* root?.bottom_sheet_options?.txtReport?.setOnClickListener {
             bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
             root?.overlay_view_report_popup?.visibility = View.VISIBLE
             root?.layout_popup_report?.visibility = View.VISIBLE
             root?.ll_select_report_reason?.setOnClickListener {
                 reportList()
             }


         }*/

        /*   root?.overlay_view_report_popup1?.setOnClickListener {
               *//*root?.overlay_view?.setBackgroundColor(
                ThemeManager.colors(
                    requireContext(),
                    StringSingleton.bodyBackground
                )
            )*//*
            root?.rvReportList?.visibility = GONE

            root?.overlay_view_share_video?.visibility = View.GONE
            root?.bottom_sheet_share_video?.visibility = View.GONE
            root?.overlay_view_report_popup1?.visibility = View.GONE
            root?.layout_popup_report?.visibility = View.GONE


            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }
*/
        /* root?.bottom_sheet_options?.txtReport?.setOnClickListener {
             bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
             root?.overlay_view_report_popup?.visibility = View.VISIBLE
             root?.layout_popup_report?.visibility = View.VISIBLE
             root?.ll_select_report_reason?.setOnClickListener {
                 reportList()
             }*/


        bottomSheetBehavior?.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_guest?.visibility = View.GONE
                        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                        hideKeyboard(requireActivity())
                        Log.d("tag", "STATE_HIDDEN called!!!!")

                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_guest?.visibility = View.VISIBLE
                        //getCommentList()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_guest?.visibility = View.GONE
                        hideKeyboard(requireActivity())
                        //bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")

                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        //root?.overlay_view_guest?.visibility = View.GONE
                        //bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
                        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_EXPANDED
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun followUser(id: String) {
        Log.e("selectedPosition Rell: ", "" + id)
        root?.tv_follower?.isEnabled = false
        videoListViewModel?.followUser(id)
    }

    private fun followObserver() {
        videoListViewModel?.followUser?.observe(viewLifecycleOwner, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.followUser(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                    root?.tv_follower?.isEnabled = true
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                    root?.tv_follower?.isEnabled = true
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    root?.tv_follower?.isEnabled = true
                }

                is NetworkResponse.SUCCESS.followUser -> {
                    if (it.response.status()) {
                        requireActivity().runOnUiThread {
                            root?.tv_follower?.text = "•  Following"
                            root?.tv_comment_follower?.text = "Following"

                            val user = VideoListQuery.User(
                                selectedVideo?.user()?.__typename()!!,
                                selectedVideo?.user()?.postsCounts(),
                                selectedVideo?.user()?.username(),
                                selectedVideo?.user()?.name(),
                                selectedVideo?.user()?._id()!!,
                                true,
                                selectedVideo?.user()?.colorCode(),
                                selectedVideo?.user()?.image())

                            videoList[selectedPosition!!] = VideoListQuery.Video(
                                selectedVideo?.userId()!!,
                                selectedVideo?.__typename()!!,
                                selectedVideo?.title()!!,
                                selectedVideo?._id()!!,
                                selectedVideo?.audioId(),
                                selectedVideo?.likeCounts()!! + 1,
                                selectedVideo?.views()!!,
                                true,
                                selectedVideo?.name()!!,
                                selectedVideo?.imageName(),
                                selectedVideo?.commentCounts()!!,
                                selectedVideo?.description(),
                                selectedVideo?.bookmarked(),
                                selectedVideo?.audio(),
                                user)
                            selectedVideo = videoList[selectedPosition!!]

                            for (i in videoList.indices) {
                                Log.e(
                                    "selectedPosition : ",
                                    "" + selectedVideo?.user()?._id() + "  :: " + videoList[i]._id())
                                if (selectedVideo?.user()?._id() == videoList[i].user()?._id()) {
                                    var user = VideoListQuery.User(
                                        videoList[i].user()!!.__typename(),
                                        videoList[i].user()?.postsCounts(),
                                        videoList[i].user()?.username(),
                                        videoList[i].user()?.name(),
                                        videoList[i].user()!!._id(),
                                        true,
                                        videoList[i].user()?.colorCode(),
                                        videoList[i].user()?.image())
                                    var varrr = VideoListQuery.Video(
                                        selectedVideo?.userId()!!,
                                        videoList[i].__typename(),
                                        videoList[i].title(),
                                        videoList[i]._id(),
                                        videoList[i].audioId(),
                                        videoList[i].likeCounts(),
                                        videoList[i].views(),
                                        videoList[i].liked(),
                                        videoList[i].name(),
                                        videoList[i].imageName(),
                                        videoList[i].commentCounts(),
                                        videoList[i].description(),
                                        videoList[i].bookmarked(),
                                        selectedVideo?.audio(),
                                        user)
                                    videoList[i] = varrr
                                }
                            }
                        }

                    }
                    root?.tv_follower?.isEnabled = true
                }
            }
        })
    }

    private fun unFollowUser(id: String) {
        root?.tv_follower?.isEnabled = false
        videoListViewModel?.unFollowUser(id)
    }

    fun unfollowObserver() {

        videoListViewModel?.unFollowUser?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.unFollowUser(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                    root?.tv_follower?.isEnabled = true
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                    root?.tv_follower?.isEnabled = true
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    root?.tv_follower?.isEnabled = true
                }

                is NetworkResponse.SUCCESS.unfollowUser -> {
                    if (it.response.status()) {
                        requireActivity().runOnUiThread {
                            root?.tv_follower?.text = "•  Follow"
                            root?.tv_comment_follower?.text = "Follow"

                            val user = VideoListQuery.User(
                                selectedVideo?.user()?.__typename()!!,
                                selectedVideo?.user()?.postsCounts(),
                                selectedVideo?.user()?.username(),
                                selectedVideo?.user()?.name(),
                                selectedVideo?.user()?._id()!!,
                                false,
                                selectedVideo?.user()?.colorCode(),
                                selectedVideo?.user()?.image())

                            videoList[selectedPosition!!] = VideoListQuery.Video(
                                selectedVideo?.userId()!!,
                                selectedVideo?.__typename()!!,
                                selectedVideo?.title()!!,
                                selectedVideo?._id()!!,
                                selectedVideo?.audioId(),
                                selectedVideo?.likeCounts()!! + 1,
                                selectedVideo?.views()!!,
                                true,
                                selectedVideo?.name()!!,
                                selectedVideo?.imageName(),
                                selectedVideo?.commentCounts()!!,
                                selectedVideo?.description(),
                                selectedVideo?.bookmarked(),
                                selectedVideo?.audio(),
                                user)
                            selectedVideo = videoList[selectedPosition!!]
                            for (i in videoList.indices) {
                                Log.e(
                                    "selectedPosition : ",
                                    "" + selectedVideo?.user()?._id() + "  :: " + videoList[i]._id())
                                if (selectedVideo?.user()?._id() == videoList[i].user()?._id()) {
                                    var user = VideoListQuery.User(
                                        videoList[i].user()!!.__typename(),
                                        videoList[i].user()?.postsCounts(),
                                        videoList[i].user()?.username(),
                                        videoList[i].user()?.name(),
                                        videoList[i].user()!!._id(),
                                        false,
                                        videoList[i].user()?.colorCode(),
                                        videoList[i].user()?.image())
                                    var varrr = VideoListQuery.Video(
                                        selectedVideo?.userId()!!,
                                        videoList[i].__typename(),
                                        videoList[i].title(),
                                        videoList[i]._id(),
                                        videoList[i].audioId(),
                                        videoList[i].likeCounts(),
                                        videoList[i].views(),
                                        videoList[i].liked(),
                                        videoList[i].name(),
                                        videoList[i].imageName(),
                                        videoList[i].commentCounts(),
                                        videoList[i].description(),
                                        videoList[i].bookmarked(),
                                        selectedVideo?.audio(),
                                        user)
                                    videoList[i] = varrr
                                }
                            }
                        }


                    }
                    root?.tv_follower?.isEnabled = true
                }
            }
        })

    }


    private fun download(id: String) {
        videoListViewModel?.download(id)
    }

    fun downloadObserver() {

        videoListViewModel?.download?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.download(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.download -> {
                    if (it.response.status()) {

                        //                        Toast.makeText(requireContext(), "Downloaded Successfully.!!", Toast.LENGTH_SHORT).show()

                    }
                }
            }
        })

    }


    private fun viewCount(id: String) {
        Log.e("View Count", id)
        videoListViewModel?.download(id)
    }

    fun viewCountObserver() {

        videoListViewModel?.viewCount?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.viewCount(selectedVideo?._id()!!)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.viewCount -> {
                    if (it.response.status()) {

                        //                        Toast.makeText(requireContext(), "Downloaded Successfully.!!", Toast.LENGTH_SHORT).show()

                    }
                }
            }
        })

    }

    private fun reportList() {

        videoListViewModel?.reportArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        requireView(), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        videoListViewModel?.getReportList(requireContext())
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    Log.e("Test user", "Block User")

                    customToast(it.errorMsg.toString(), requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg.toString(), requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.getReportList -> {
                    Log.d("tag", "response::" + it.response)

                    initReportRecyclerView(it.response)
                }

            }

        })
    }

    private fun setupUI() {
        // linkToShare = "SARE APP \n \n https://sare.in/${selectedVideo?.user()?.username()}"
        //        linkToShare = "SARE APP \n \n https://sare.in/$id"
        //        val shareIntent = Intent(Intent.ACTION_SEND)
        //        shareIntent.type = "text/plain"

        root?.cv_facebook?.setOnClickListener {
            var facebookAppFound = false

            var shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)

            val pm = requireContext().packageManager
            val activityList: List<ResolveInfo> = pm.queryIntentActivities(shareIntent, 0)
            for (app in activityList) {
                if (app.activityInfo.packageName.contains("com.facebook.katana")) {
                    val activityInfo: ActivityInfo = app.activityInfo
                    val name = ComponentName(
                        activityInfo.applicationInfo.packageName, activityInfo.name)
                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                    shareIntent.component = name
                    facebookAppFound = true
                    break
                }
            }
            if (!facebookAppFound) {
                val url = linkToShare
                val sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=$url"
                shareIntent = Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
            }
            requireContext().startActivity(shareIntent)
        }

        root?.cv_insta?.setOnClickListener {
            try {

                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"

                shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                shareIntent.setPackage("com.instagram.android")
                requireActivity().startActivity(shareIntent)

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error:::${e.message}")
                customToast("Instagram has not been installed", requireActivity(), 0)
            }

        }

        root?.cv_whats_app?.setOnClickListener {

            val pm: PackageManager = requireContext().packageManager
            try {
                val waIntent = Intent(Intent.ACTION_SEND)

                waIntent.type = "text/plain"
                val info: PackageInfo = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)

                waIntent.setPackage("com.whatsapp")
                waIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)

                //                waIntent.putExtra(Intent.EXTRA_TEXT, text.toString() + " " + url)
                requireContext().startActivity(
                    Intent.createChooser(waIntent, "Share to"))
            } catch (e: PackageManager.NameNotFoundException) {
                customToast(getString(R.string.share_whatsapp_not_instaled), requireActivity(), 0)
            }
        }

        root?.cv_copy?.setOnClickListener {
            val clipboardManager = requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clipData = ClipData.newPlainText("text", linkToShare)
            clipboardManager.setPrimaryClip(clipData)
            Log.d("tag", "clip data::$clipData")
            customToast("Link copied to clipboard!!", requireActivity(), 1)
        }

        root?.cv_more?.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_TEXT, linkToShare)
            startActivity(Intent.createChooser(share, "Share to"))

        }


    }

    private fun initReportRecyclerView(list: List<ReportListQuery.ReportType>) {
        //        root?.rvReportList?.visibility = View.VISIBLE
        root?.rvReportList?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvReportList?.context, RecyclerView.VERTICAL, false)
            adapter = ReportListAdapter(list, context)
            (adapter as ReportListAdapter).onItemClick = { data ->
                root?.rvReportList?.visibility = View.GONE

                // do something with your item
                Log.d("tag", data.reportName().toString())
                root?.txtReportReason?.text = data.reportName().toString()
                root?.btnReportSend?.setOnClickListener {
                    if (root?.txtReportReason?.text!!.isEmpty() && root?.txtTitle?.text!!.isEmpty()) {
                        customToast(
                            "Report cant be submitted. Please select report reason!!", requireActivity(), 0)

                    } else if (root?.txtReportReason?.text!!.isEmpty()) {
                        customToast("Please select report reason!!", requireActivity(), 0)
                    } else if (root?.txtTitle?.text!!.isEmpty()) {
                        customToast("Please write report title!!", requireActivity(), 0)
                    } else if (root?.txtTypeSomething?.text!!.isEmpty()) {
                        customToast("Please type something about report!!", requireActivity(), 0)
                    } else {
                        val apolloClient = ApolloClient.setupApollo(token.toString())

                        Log.d(
                            "tag", " txtReportReason.text.toString()::" + root?.txtReportReason?.text.toString())
                        apolloClient.mutate(
                            StoreReportVideoMutation(
                                selectedVideo?._id().toString(),
                                data.reportType()!!,
                                root?.txtTitle?.text.toString(),
                                root?.txtReportReason?.text.toString()))
                            .enqueue(object : ApolloCall.Callback<StoreReportVideoMutation.Data>() {
                                override fun onFailure(e: ApolloException) {
                                    e.printStackTrace()
                                    Log.d("tag", "Error::" + e.message)
                                    customToast(e.message.toString(), requireActivity(), 0)

                                    MyApplication.mainActivity.logoutUser()
                                    activity?.openNewActivity(LoginActivity::class.java)
                                }

                                override fun onResponse(response: Response<StoreReportVideoMutation.Data>) {
                                    Log.d("tag", "response::$response")
                                    if (response.data?.storeReportVideo() != null) {
                                        requireActivity().runOnUiThread {
                                            customToast("Report Submitted!!", requireActivity(), 1)

                                            root?.txtTitle?.setText("")
                                            root?.txtReportReason?.text = ""
                                            root?.txtTypeSomething?.setText("")

                                            root?.overlay_view_report_popup1?.visibility = View.GONE
                                            root?.layout_popup_report?.visibility = View.GONE

                                            root?.overlay_view_share_video?.visibility = View.GONE
                                            root?.bottom_sheet_share_video?.visibility = View.GONE
                                            bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN

                                        }


                                        hideKeyboard(requireActivity())
                                    } else {
                                        requireActivity().runOnUiThread {
                                            val error = response.errors?.get(0)?.message
                                            Log.d("tag", "Data Error: $error")
                                            val errorCode = response.errors?.get(0)?.customAttributes?.get(
                                                "status")
                                            if (errorCode?.toString()?.equals("401") == true) {
                                                customToast(error.toString(), requireActivity(), 0)
                                                MyApplication.mainActivity.logoutUser()
                                                activity?.openNewActivity(LoginActivity::class.java)
                                            } else if (errorCode?.toString()?.equals("403") == true) {
                                                customToast(error.toString(), requireActivity(), 0)
                                                MyApplication.mainActivity.logoutUser()
                                                activity?.openNewActivity(LoginActivity::class.java)
                                            } else {
                                                customToast(error.toString(), requireActivity(), 0)
                                            }
                                            root?.overlay_view_report_popup1?.visibility = View.GONE
                                            root?.layout_popup_report?.visibility = View.GONE

                                            root?.overlay_view_share_video?.visibility = View.GONE
                                            root?.bottom_sheet_share_video?.visibility = View.GONE
                                            bottomSheetBehavior1?.state = BottomSheetBehavior.STATE_HIDDEN

                                        }
                                    }
                                }
                            })
                    }
                }
            }
        }
    }

    /* private fun getTagVideoList(categoryList: List<String>, type: String) {
         progressDialog = CustomProgressDialogNew(context)
         Log.d("tag", "categoryList:::$categoryList")
         videoListViewModel?.getTagVideoList(categoryList, id.toString(), type)

     }*/

    /*fun videoTagListObserver(categoryList: List<String>, type: String) {
        progressDialog.show()
        videoListViewModel?.videoArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.dismiss()
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getTagVideoList(categoryList, id.toString(), type)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.dismiss()
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.dismiss()
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.videoList -> {
                    progressDialog.dismiss()
                    root?.bottom_info?.visibility = VISIBLE
                    //                    root?.iv_add?.visibility = VISIBLE
                    //                    root?.ll_bottom?.visibility = VISIBLE
                    //root?.ll_right?.visibility = VISIBLE
                    videoList = it.response.toMutableList()
                    //initRecyclerView(it.response)
                    addVideoList()
                }

            }

        })
    }*/
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("Video", "Permission 1111111111111111111")
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            Log.d("Video", "Permission 222222222222")
            var temp = true
            for (i in grantResults.indices) {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                    temp = false
                }
            }
            if (temp) {
                Log.d("Video", "Permission 3333333333333333")
                /* onCreateActivity()
                 videoWidth = 720
                 videoHeight = 1280
                 cameraWidth = 1280
                 cameraHeight = 720*/
                onStop()
                PortraitCameraActivity.startActivity(activity)
            } else {
                customToast("Please allow permissions", requireActivity(), 0)
            }
        }
    }


    private fun openShareApps(dummy: DummyData) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "The Subject")

        Log.d("tag", "dummy:::${dummy.url}")


        if (dummy.url != null) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            val progressDialog = CustomProgressDialogNew(requireContext())
            progressDialog.show()
            Tasks.executeInBackground(activity, object : BackgroundWork<File> {
                @Throws(java.lang.Exception::class) override fun doInBackground(): File {
                    var outputFile: File? = null
                    var fileName: String? = null
                    val MEGABYTE = 1024 * 1024
                    try {
                        val newUrl = URL(dummy.url)
                        val c: HttpURLConnection = newUrl.openConnection() as HttpURLConnection
                        val path = newUrl.path.split("/".toRegex()).toTypedArray()
                        val mp3 = path[path.size - 1]
                        val PATH: String = Environment.getExternalStorageDirectory().toString() + "/DownLoad/"
                        Log.v("tag", "PATH::::$PATH")
                        val file = File(PATH)
                        file.mkdir()
                        fileName = mp3
                        outputFile = File(file, fileName)
                        Log.d(
                            "tag",
                            "file exists:::" + outputFile.exists().toString() + "::::or::not:::" + !outputFile.exists())
                        Log.d("tag", "outputFile:::$outputFile")
                        if (outputFile.exists()) {
                            return outputFile
                        } else {
                            outputFile.createNewFile()
                            c.connect()
                            val `is`: InputStream = c.inputStream
                            val fos = FileOutputStream(outputFile)
                            val totalsize: Int = c.contentLength
                            val buffer = ByteArray(MEGABYTE)
                            var count = 0
                            while (`is`.read(buffer).also({ count = it }) > 0) {
                                fos.write(buffer, 0, count)
                            }
                            fos.close()
                            Log.d("tag", "filename::$fileName")
                        }
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    return outputFile!!
                }
            }, object : Completion<File> {
                override fun onSuccess(context: Context?, result: File) {
                    progressDialog.hide()

                    try {
                        Log.d("tag", "result:::::file:::$result")
                        val videoUri: Uri = FileProvider.getUriForFile(
                            requireActivity(), BuildConfig.APPLICATION_ID + ".provider", result)

                        /* linkToShare =
                             "SARE APP \n \n https://sare.in/${selectedVideo?.user()?.username()}"*/
                        /*  val data: String = java.lang.String.valueOf(
                              Html.fromHtml(
                                  StringBuilder()
                                      .append(
                                          "<p>" + postJsonData.getComment().toString() + "</p>"
                                      )
                                      .append("<br/>")
                                      .append(
                                          "<b>" + activity!!.getString(android.R.string.textMessage) + "</b>"
                                      )
                                      .append("<br/>")
                                      .append(
                                          "<h1>" + activity!!.getString(android.R.string.clickHereTo) + "</h1>"
                                      )
                                      .append(
                                          "<a>" + activity!!.getString(android.R.string.dummyLink) + "</a>"
                                      ).toString()
                              )
                          )*/
                        /*Log.d("myTag", linkToShare)
                        sendIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                        sendIntent.type = "text/html"*/
                        sendIntent.putExtra(Intent.EXTRA_STREAM, videoUri)
                        sendIntent.type = "video/*"
                        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                        sendIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                        activity!!.startActivity(Intent.createChooser(sendIntent, "Share to"))

                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                        requireActivity().runOnUiThread {
                            customToast("Please try again: " + e.message, requireActivity(), 0)
                        }
                    }
                }

                override fun onError(context: Context?, e: java.lang.Exception) {
                    e.printStackTrace()
                    progressDialog.hide()

                    requireActivity().runOnUiThread {
                        customToast(
                            "Download failed:  " + e.message.toString(), requireActivity(), 0)
                    }
                }
            })

        }

    }


    fun userProfileVideo(preferences: SharedPreferences) {
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            loginUserId = mainObject.getString("_id")
        }

        var videoIdList1 = arguments?.getString("videoList")

        val gson = Gson()

        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type

        videoList = gson.fromJson<List<VideoListQuery.Video>>(videoIdList1, myType) as ArrayList<VideoListQuery.Video>

        videoPosition = arguments?.getInt("position")!!

        isVideoCurrentPage = arguments?.getInt("isVideoCurrentPage")!!

        //  initRecyclerView(videoList)

        storiesPagerAdapter = StoriesPagerAdapter(this, videoList as ArrayList<VideoListQuery.Video>)

        root?.userVideoViewPager!!.adapter = storiesPagerAdapter

        root?.userVideoViewPager!!.setCurrentItem(videoPosition, true)

        setUserDataFromCurrentVideoItem(videoPosition)

        root?.userVideoViewPager!!.visible()
        tagName = arguments?.getString("tagName")
        root?.userVideoViewPager!!.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                Log.e("SareApp   ", "" + position + " ::: " + isVideoCurrentPage)

                setUserDataFromCurrentVideoItem(position)

                val cachedFragmentEntering: StoryViewFragment = storiesPagerAdapter!!.getCachedItem(position)!!

                cachedFragmentEntering.playVideo()

                //  cachedFragmentEntering.setMute(videoList[position].audio()?.active()!!)

                if (navigationFlag == "ProfilePublic") {
                    Log.e("Sare", "ProfilePublic   position + 2::::::${position + 2}")
                    Log.e(
                        "Sare",
                        "ProfilePublic  isVideoCurrentPage * Utils.PAGE_SIZE::::::${isVideoCurrentPage * Utils.PAGE_SIZE}")
                    if ((position + 2) == isVideoCurrentPage * Utils.PAGE_SIZE) {
                        videoListViewModel?.getUserVideoList(
                            loginUserId.toString(), isVideoCurrentPage)
                        isVideoCurrentPage++
                    }

                } /*else if (navigationFlag == "TagPublic") {

                    Log.e("Sare","position + 2::::::${position + 2}")
                    Log.e("Sare","isVideoCurrentPage * Utils.PAGE_SIZE::::::${isVideoCurrentPage * Utils.PAGE_SIZE}")
                    if ((position + 2) == isVideoCurrentPage * Utils.PAGE_SIZE) {
                        videoListViewModel?.getTagVideoList(
                            mutableListOf(tagName), loginUserId.toString(), isVideoCurrentPage)
                        isVideoCurrentPage++
                    }
                }*/

            }

        })
    }


    fun tagsVideo(preferences: SharedPreferences) {
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            loginUserId = mainObject.getString("_id")
        }

        var videoIdList1 = arguments?.getString("videoList")
        var tagName = arguments?.getString("tagName")

        val gson = Gson()

        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type

        videoList = gson.fromJson<List<VideoListQuery.Video>>(videoIdList1, myType) as ArrayList<VideoListQuery.Video>

        Log.d("tag", "videoList:::$videoList")

        videoPosition = arguments?.getInt("position")!!

        isVideoCurrentPage = arguments?.getInt("isVideoCurrentPage")!!

        storiesPagerAdapter = StoriesPagerAdapter(this, videoList as ArrayList<VideoListQuery.Video>)

        root?.userVideoViewPager!!.adapter = storiesPagerAdapter

        root?.userVideoViewPager!!.setCurrentItem(videoPosition, true)

        setUserDataFromCurrentVideoItem(isVideoCurrentPage++)

        root?.userVideoViewPager!!.visible()

        root?.userVideoViewPager!!.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setUserDataFromCurrentVideoItem(position)
                val cachedFragmentEntering: StoryViewFragment = storiesPagerAdapter!!.getCachedItem(position)!!
                cachedFragmentEntering.playVideo()
                /*if (navigationFlag == "TagPublic") {
                    //                    if ((position + 2) == isVideoCurrentPage * Utils.PAGE_SIZE) {
                    videoListViewModel?.getTagVideoList(
                        mutableListOf(tagName), loginUserId.toString(), isVideoCurrentPage++)
                    isVideoCurrentPage++
                    //                    }
                }*/
            }

        })
    }


    fun addVideoList() {

        if (videoList.isNullOrEmpty()) return
        storiesPagerAdapter = StoriesPagerAdapter(this, videoList as ArrayList<VideoListQuery.Video>)

        root?.userVideoViewPager!!.adapter = storiesPagerAdapter

        root?.userVideoViewPager!!.setCurrentItem(0, true)

        setUserDataFromCurrentVideoItem(videoPosition)

        root?.userVideoViewPager!!.visible()

        root?.userVideoViewPager!!.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                setUserDataFromCurrentVideoItem(position)

                val cachedFragmentEntering: StoryViewFragment = storiesPagerAdapter!!.getCachedItem(position)!!

                cachedFragmentEntering.playVideo()

                //  cachedFragmentEntering.setMute(videoList[position].audio()?.active()!!)
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("tag","result code::::$resultCode")
        if (requestCode == 101) {
            if (mRecyclerView != null) {
                mRecyclerView!!.releasePlayer()
            }
            root?.txtFollowing?.alpha = 1f
            root?.type_all?.alpha = 0.5f
            root?.txtHashtag?.alpha = 0.5f
            adapterVideoType?.currentPosition = -1
            adapterVideoType?.notifyDataSetChanged()
            getVideoList(ArrayList<String>(), "following")
        }
        if (requestCode == 102) {
            if (mRecyclerView != null) {
                mRecyclerView!!.releasePlayer()
            }
            root?.bottom_info?.visibility = VISIBLE
            root?.txtFollowing?.alpha = 0.5f
            root?.type_all?.alpha = 1f
            root?.txtHashtag?.alpha = 0.5f


            adapterVideoType?.currentPosition = -1
            adapterVideoType?.notifyDataSetChanged()
            getVideoList(ArrayList<String>(), "random")

        }
    }


}


fun hideKeyboard(activity: Activity) {
    val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    // check if no view has focus:
    val currentFocusedView = activity.currentFocus
    if (currentFocusedView != null) {
        inputManager.hideSoftInputFromWindow(
            currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    }


}
