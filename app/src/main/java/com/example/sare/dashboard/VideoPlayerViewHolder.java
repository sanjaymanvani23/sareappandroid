package com.example.sare.dashboard;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sare.R;
import com.example.sare.VideoListQuery;


public class VideoPlayerViewHolder extends RecyclerView.ViewHolder {

    FrameLayout media_container;
    ImageView thumbnail, volumeControl,imgPlay;
    ProgressBar progressBar;
    ProgressBar videoProgress;
    View parent;

    public VideoPlayerViewHolder(@NonNull View itemView) {
        super(itemView);
        parent = itemView;
        media_container = itemView.findViewById(R.id.media_container);
        thumbnail = itemView.findViewById(R.id.thumbnail);
        progressBar = itemView.findViewById(R.id.progressBar);
        videoProgress = itemView.findViewById(R.id.videoProgress);
        volumeControl = itemView.findViewById(R.id.volume_control);
        imgPlay = itemView.findViewById(R.id.imgPlay);
    }

    public void onBind(VideoListQuery.Video mediaObject, Context context) {
        parent.setTag(this);
    }

}














