package com.example.sare.base

import android.os.Bundle
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import com.example.sare.util.RequestPermission
import com.example.sare.util.SessionManager
import io.ktor.client.*
import org.koin.android.ext.android.inject


abstract class BaseActivity : AppCompatActivity() {
    val ktor: HttpClient by inject()
    var sessionManager: SessionManager? = null
    var requestPermission: RequestPermission? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sessionManager = SessionManager(this)
        initRequestPermission()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
    fun initRequestPermission() {
        if (null == requestPermission)
            requestPermission = RequestPermission(this, false)
    }
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (null != requestPermission && requestCode == RequestPermission.REQUEST_CODE_PERMISSION)
            requestPermission!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}