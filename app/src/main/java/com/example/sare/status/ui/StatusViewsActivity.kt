package com.example.sare.status.ui

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.StoryViewerListQuery
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.customToast
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import com.example.sare.status.adapter.StatusViewsAdapter
import com.example.sare.status.viewmodel.StatusViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_status_views.*

class StatusViewsActivity : AppCompatActivity() {
    var storyId: String? = ""
    var token: String? = ""
    var statusViewModel: StatusViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_status_views)
        getSharedPreferences()
        storyId = intent.getStringExtra("storyId")
        statusViewModel = ViewModelProvider(this)[StatusViewModel::class.java]

        getStatusViewsList()
        getStatusViewsObserver()

        setListeners()
    }

    fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    private fun getStatusViewsList() {
        statusViewModel?.getStatusViewerList(token.toString(), storyId.toString())
    }

    private fun getStatusViewsObserver() {
        statusViewModel?.statusViewerList?.observe(this, Observer { it ->
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relGuestStory), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            statusViewModel?.getStatusViewerList(token.toString(), storyId.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }
                is NetworkResponse.SUCCESS.getStoryViewerList -> {

                    initRecyclerView(it.response)

                    txtViews.text = it.response.size.toString()

                }
            }
        })
    }

    private fun initRecyclerView(list: List<StoryViewerListQuery.StoryViewerList>) {
        rvStatusViews?.apply {
            layoutManager = LinearLayoutManager(
                rvStatusViews?.context, RecyclerView.VERTICAL, false)
            adapter = StatusViewsAdapter(
                list, context)
            if (adapter!!.itemCount != 0){
                rvStatusViews.visible()
                txtNoData.gone()
            }else{
                txtNoData.visible()
                rvStatusViews.gone()
            }
        }
    }


    fun setListeners() {
        imgBack?.setOnClickListener {
            val intent = intent
            setResult(100,intent)
            finish()
        }
    }
}