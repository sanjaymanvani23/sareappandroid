package com.example.sare.status

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.sare.R
import com.example.sare.status.adapter.MyStatusAdapter
import kotlinx.android.synthetic.main.fragment_my_status.view.*

class MyStatusFragment : Fragment() {
    var root: View? = null
    var list: ArrayList<String> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_my_status, container, false)
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                NavHostFragment.findNavController(this@MyStatusFragment).navigateUp()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback
        )
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        root?.rvMyStatus?.apply {
            layoutManager = GridLayoutManager(
                root?.rvMyStatus?.context,
                3
            )
            adapter = MyStatusAdapter(
                list, requireContext(),
                this@MyStatusFragment
            )
        }
        setListeners()
        return root
    }

    private fun setListeners(){
        root?.imgBack?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
    }
}