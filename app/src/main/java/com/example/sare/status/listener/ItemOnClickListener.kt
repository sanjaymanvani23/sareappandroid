package com.example.sare.status.listener

interface ItemOnClickListener {

    fun onItemClickGuestPreview(toJson: String,
        pos: Int,
        name: String,
        image: String,
        colorCode: String)
}