package com.example.sare.status.model

import org.jetbrains.annotations.Nullable

data class StatusModel(var isVideo: Boolean,
    var urlPath: String,
    var caption: String,
    var time: String,
    var views: String,
    var storyId: String,
    var isViewed: @Nullable Boolean?)