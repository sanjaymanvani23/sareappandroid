package com.example.sare.status.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.sare.AmazonUtil.getSignedUrl
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.StoryListQuery
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.customeview.statusView.StoriesProgressView
import com.example.sare.extensions.*
import com.example.sare.status.model.StatusModel
import com.example.sare.status.viewmodel.StatusViewModel
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_guest_status_preview.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@RequiresApi(Build.VERSION_CODES.M) class GuestStatusPreviewActivity : AppCompatActivity(),
    StoriesProgressView.StoriesListener {
    var cacheDataSourceFactory: CacheDataSourceFactory? = null
    val simpleCache = MyApplication.simpleCache
    private var counter = 0
    var pressTime = 0L
    var duration = 0L
    var limit = 500L
    var type: Int = 0
    var playbackPosition: Long = 0
    var token: String? = null
    val images: ArrayList<StatusModel> = ArrayList()
    var statusViewModel: StatusViewModel? = null
    private var simpleExoPlayer: SimpleExoPlayer? = null
    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private val onTouchListener = View.OnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                pressTime = System.currentTimeMillis()
                progressMyStatus?.pause()
                simpleExoPlayer?.playWhenReady = false
                return@OnTouchListener false
            }
            MotionEvent.ACTION_UP -> {
                val now = System.currentTimeMillis()
                progressMyStatus?.resume()
                simpleExoPlayer?.playWhenReady = true
                simpleExoPlayer?.seekTo(simpleExoPlayer?.currentPosition!!)
                return@OnTouchListener limit < now - pressTime
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_guest_status_preview)
        setTheme()
        getSharedPreferences()
        statusViewModel = ViewModelProvider(this)[StatusViewModel::class.java]
        setData()
        setListeners()
    }

    fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    fun setData() {
        val list = intent?.getStringExtra("list")
        val name = intent?.getStringExtra("name")
        val profile = intent?.getStringExtra("profile")
        val profileBorder = intent?.getStringExtra("profileBorder")
        val myType = object : TypeToken<List<StoryListQuery.Story1>>() {}.type
        val otherStoriesList = Gson().fromJson<List<StoryListQuery.Story1>>(
            list, myType) as ArrayList<StoryListQuery.Story1>
        Log.d("tag", "list:::$list")
        otherStoriesList.reverse()
        txtName.text = name.toString()
        imgProfile.loadUrl(profile!!)
        imgProfileBorder.setBorder(profileBorder.toString())
        for (i in otherStoriesList) {
            if (i.imageName().toString().endsWith(".png") || i.imageName().toString().endsWith(".jpg") || i.imageName()
                    .toString().endsWith(".jpeg")
            ) {
                images.add(
                    StatusModel(
                        false,
                        PUBLIC_URL + i.imageName().toString(),
                        i.content().toString(),
                        i.createdAt().toString(),
                        "",
                        i._id().toString(),
                        i.isViewed))
            } else {
                images.add(
                    StatusModel(
                        true,
                        getSignedUrl(i.videoName().toString())!!,
                        i.content().toString(),
                        i.createdAt().toString(),
                        "",
                        i._id().toString(),
                        i.isViewed))
            }
        }
        images.toTypedArray()
        progressMyStatus?.setStoriesCount(images.size)
        Handler().postDelayed({
            if (images[counter].isVideo) {
                try {
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(images[counter].urlPath, HashMap<String, String>())
                    duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
                    retriever.release()
                    Log.d("tag", "duration:::$duration")
                    progressMyStatus?.setStoryDuration(duration)
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressMyStatus?.setStoryDuration(15000L)
                }
            } else {
                progressMyStatus?.setStoryDuration(5000L)
            }
            progressMyStatus?.setStoriesListener(this)
            progressMyStatus?.startStories()
            setStoryData(counter)
        }, 50)

        // bind reverse view
        reverse?.setOnClickListener { progressMyStatus?.reverse() }
        reverse?.setOnTouchListener(onTouchListener)

        // bind skip view
        skip?.setOnClickListener { progressMyStatus?.skip() }
        skip?.setOnTouchListener(onTouchListener)
    }

    private fun prepareVideoPlayer() {
        simpleExoPlayer = let { ExoPlayerFactory.newSimpleInstance(it) }
        cacheDataSourceFactory = simpleCache?.let {
            CacheDataSourceFactory(it, DefaultHttpDataSourceFactory(Util.getUserAgent(this, "App")))
        }
    }

    private fun getPlayer(): SimpleExoPlayer? {
        if (simpleExoPlayer == null) {
            prepareVideoPlayer()
        }
        return simpleExoPlayer
    }

    private fun setListeners() {
        imgBack?.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setStoryData(counter: Int) {
        if (!images[counter].isViewed!!) {
            getStoryChecked(images[counter].storyId)
        }
        if (images[counter].isVideo) {
            imgMyStatus.gone()
            playerView.visible()
            initializePlayer(images[counter].urlPath)
            try {
                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(images[counter].urlPath, HashMap<String, String>())
                duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
                retriever.release()
                progressMyStatus?.setStoryDuration(duration)
                Log.d("tag", "duration::setStoryDuration::$duration")
            } catch (e: Exception) {
                e.printStackTrace()
                progressMyStatus?.setStoryDuration(15000L)
            }
            txtCaption.text = images[counter].caption.toString()
            txtTime.text = getParsedTime(images[counter].time.toString())
        } else {
            imgMyStatus.visible()
            playerView.gone()
            progressBar.gone()
            Glide.with(this).load(images[counter].urlPath).into(imgMyStatus)
            progressMyStatus?.setStoryDuration(5000L)
            txtTime.text = getParsedTime(images[counter].time.toString())
            txtCaption.text = images[counter].caption.toString()
        }
    }

    override fun onNext() {
        counter = ++counter
        setStoryData(counter)

    }

    override fun onPrev() {
        if (counter - 1 < 0) return
        counter = --counter
        setStoryData(counter)
    }

    override fun onDestroy() {
        // Very important !
        simpleExoPlayer?.playWhenReady = false
        simpleExoPlayer?.stop()
        progressMyStatus?.destroy()
        super.onDestroy()
    }

    override fun onComplete() {
        progressMyStatus?.destroy()
        onBackPressed()
    }

    override fun onStop() {
        simpleExoPlayer?.playWhenReady = false
        simpleExoPlayer?.stop()
        progressMyStatus?.destroy()
        super.onStop()
    }

    private fun initVideoView(videoFile: String) {
        videoStatus.setVideoURI(Uri.parse(videoFile))
        videoStatus.setOnPreparedListener {
            videoStatus.start()
        }
    }

    private fun getParsedTime(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        var parsedDate: String? = null
        parsedDate = try {
            var time: Long = df.parse(date).time
            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000
            }
            val now = System.currentTimeMillis()
            val diff = now - time;
            when {
                diff < MINUTE_MILLIS -> {
                    return "just now";
                }
                diff < 2 * MINUTE_MILLIS -> {
                    return "a minute ago";
                }
                diff < 50 * MINUTE_MILLIS -> {
                    return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                }
                diff < 90 * MINUTE_MILLIS -> {
                    return "an hour ago";
                }
                diff < 24 * HOUR_MILLIS -> {
                    return (diff / HOUR_MILLIS).toString() + " hours ago";
                }
                else -> {
                    parsedDate = "24 hours ago"
                    return parsedDate.toString()
                }
            }
        } catch (e: ParseException) {
            e.printStackTrace()
            parsedDate = "24 hours ago"
            return parsedDate.toString()
        }
        return parsedDate.toString()
    }

    private fun getStoryChecked(storyId: String) {
        statusViewModel?.getStoryChecked(token.toString(), storyId)
        statusViewModel?.checkStory?.observe(this, Observer { it ->
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relGuestStory), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            statusViewModel?.getStoryList(token.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }
                is NetworkResponse.SUCCESS.getStoryChecked -> {
                    Log.d("tag", "getStoryChecked:::::${it.response.status()}")
                }
            }
        })
    }

    private fun initializePlayer(url: String) {
        simpleExoPlayer = getPlayer()
        playerView.player = simpleExoPlayer
        val uri = Uri.parse(url)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory!!).createMediaSource(uri)
        simpleExoPlayer?.prepare(mediaSource, true, true)
        simpleExoPlayer?.playWhenReady = true
        simpleExoPlayer?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playWhenReady) {
                    progressBar.gone()
                }
                if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar.visible()
                } else if (playbackState == ExoPlayer.STATE_READY) {
                    progressBar.gone()
                }
            }
        })
    }

}