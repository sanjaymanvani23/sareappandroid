package com.example.sare.status.ui

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.*
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.*
import com.example.sare.status.adapter.StatusListAdapter
import com.example.sare.status.adapter.StatusSeenAdapter
import com.example.sare.status.listener.ItemOnClickListener
import com.example.sare.status.viewmodel.StatusViewModel
import com.example.sare.ui.login.LoginActivity
import com.example.sare.util.RequestPermission
import com.example.sare.videoRecording.PortraitCameraActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson

import kotlinx.android.synthetic.main.fragment_status_main.view.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M) class StatusMainFragment : Fragment(), ItemOnClickListener {
    var root: View? = null
    var statusViewModel: StatusViewModel? = null
    var token: String? = ""
    var image: String? = ""
    var colorCode: String? = ""
    var seenStatusList = ArrayList<StoryListQuery.OtherStory>()
    var newStatusList = ArrayList<StoryListQuery.OtherStory>()
    var filterNewList: List<StoryListQuery.OtherStory> = ArrayList()
    var filterSeenList: List<StoryListQuery.OtherStory> = ArrayList()
    private lateinit var statusSeenAdapter: StatusSeenAdapter
    private lateinit var statusListAdapter: StatusListAdapter

    lateinit var response: StoryListQuery.StoryList
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_status_main, container, false)
        getSharedPreferences()
        statusViewModel = ViewModelProvider(this)[StatusViewModel::class.java]
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finishAndRemoveTask()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback)
        storyList()
        storyListObserver()
        setListeners()
        return root
    }

    fun getSharedPreferences() {
        val preferences: SharedPreferences = requireContext().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            image = mainObject.getString("image")
            colorCode = mainObject.getString("colorCode")
            setData()
        }

    }

    fun setData() {
        root?.imgProfile?.loadUrl(PUBLIC_URL + image)
        root?.imgProfileBorder?.setBorder(colorCode.toString())
    }

    override fun onResume() {
        super.onResume()
        storyList()
    }


    private fun storyList() {
        statusViewModel?.getStoryList(token.toString())
    }

    private fun storyListObserver() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        statusViewModel?.storyList?.observe(viewLifecycleOwner, Observer { it ->

            when (it) {

                is NetworkResponse.ERROR -> {
                    progressDialog.hide()
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            statusViewModel?.getStoryList(token.toString())
                        }
                    snackbar.show()

                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()
                    customToast(it.errorMsg, requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.getStoryList -> {

                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    response = it.response
                    initRecyclerView(response)
                }
            }
        })
    }

    private fun initRecyclerView(response: StoryListQuery.StoryList) {
        newStatusList = ArrayList()
        seenStatusList = ArrayList()
        if (response.otherStories()?.size!! > 0) {
            root?.txtNoData?.gone()
            root?.relNewStatus?.visible()
            root?.relSeenStatus?.visible()
            for (i in 0 until response.otherStories()!!.size) {
                if (response.otherStories()?.get(i)?.viewedAllStory() == true) {
                    seenStatusList.add(response.otherStories()!![i])
                } else {
                    newStatusList.add(response.otherStories()!![i])
                }
            }
        } else {
            root?.txtNoData?.visible()
            root?.relNewStatus?.gone()
            root?.relSeenStatus?.gone()
        }
        if (seenStatusList.size > 0) {
            root?.relSeenStatus?.visible()
        } else {
            root?.relSeenStatus?.gone()
        }

        if (newStatusList.size > 0) {
            root?.relNewStatus?.visible()
        } else {
            root?.relNewStatus?.gone()
        }

        root?.rvNewStatusList?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvNewStatusList?.context, RecyclerView.VERTICAL, false)

        }
        statusListAdapter = StatusListAdapter(newStatusList, requireContext(), this@StatusMainFragment, this)
        root?.rvNewStatusList?.adapter = statusListAdapter

        root?.rvSeenStatusList?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvSeenStatusList?.context, RecyclerView.VERTICAL, false)
        }
        statusSeenAdapter = StatusSeenAdapter(seenStatusList, requireContext(), this@StatusMainFragment, this)
        root?.rvSeenStatusList?.adapter = statusSeenAdapter
    }

    private fun setListeners() {
        root?.imgSearch?.setOnClickListener {
            addBottomToTopAnimation()
        }
        root?.imgAddStory?.setOnClickListener {
            if (requireActivity().checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    RequestPermission.PERMISSION_RECORD_AUDIO,
                    RequestPermission.PERMISSION_CAMERA,
                    RequestPermission.PERMISSION_WRITE_STORAGE,
                    RequestPermission.PERMISSION_READ_STORAGE)

                requestPermissions(permissions, 101)

            } else {
                PortraitCameraActivity.startActivity(activity)
            }
        }
        root?.imgClose?.setOnClickListener {
            root?.etSearchUser?.setText("")
        }
        root?.main_layout?.setOnClickListener {
            if (response.myStories()?.story()?.size!! > 0) {

                val bundle = bundleOf(
                    "list" to Gson().toJson(response.myStories()),
                    "name" to root?.txtMyStatus?.text.toString(),
                    "profile" to (PUBLIC_URL + image),
                    "profileBorder" to colorCode.toString())
                activity?.openActivity(MyStatusPreviewActivity::class.java, bundle)
                activity?.overridePendingTransition(0, 0)
            } else {
                if (requireActivity().checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                ) {
                    val permissions = arrayOf(
                        RequestPermission.PERMISSION_RECORD_AUDIO,
                        RequestPermission.PERMISSION_CAMERA,
                        RequestPermission.PERMISSION_WRITE_STORAGE,
                        RequestPermission.PERMISSION_READ_STORAGE)

                    requestPermissions(permissions, 101)

                } else {
                    PortraitCameraActivity.startActivity(activity)
                }
            }
        }
        root?.imgProfilePicLayout?.setOnClickListener {
            if (response.myStories()?.story()!!.size > 0) {
                requireActivity().startActivity(
                    Intent(requireActivity(), MyStatusPreviewActivity::class.java).putExtra(
                        "list", Gson().toJson(response.myStories()))
                        .putExtra("name", root?.txtMyStatus?.text.toString()).putExtra("profile", PUBLIC_URL + image)
                        .putExtra("profileBorder", colorCode.toString()))
                requireActivity().overridePendingTransition(0, 0)
                activity?.overridePendingTransition(0, 0)
            } else {
                if (requireActivity().checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED
                ) {
                    val permissions = arrayOf(
                        RequestPermission.PERMISSION_RECORD_AUDIO,
                        RequestPermission.PERMISSION_CAMERA,
                        RequestPermission.PERMISSION_WRITE_STORAGE,
                        RequestPermission.PERMISSION_READ_STORAGE)

                    requestPermissions(permissions, 101)

                } else {
                    PortraitCameraActivity.startActivity(activity)
                }
            }
        }

        root?.nestedScrollView?.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            Log.d("tag", "y:::::$scrollY")
            Log.d("tag", "x:::::$scrollX")
            if (scrollY > 0) {
                root?.btnGoUp?.visible()
                if (!root?.imgSearch?.isVisible!!) addTopToBottomAnimation()
            } else {
                root?.btnGoUp?.gone()
            }
        }

        root?.etSearchUser?.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                root?.imgClose?.visible()
                filter(s.toString())
            }
        })

        root?.btnGoUp?.setOnClickListener {
            root?.nestedScrollView?.smoothScrollTo(0, 0)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 101) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                PortraitCameraActivity.startActivity(activity)
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        }
    }

    private fun filter(text: String) {
        val filteredNewNames: ArrayList<StoryListQuery.OtherStory> = ArrayList()
        val filteredSeenNames: ArrayList<StoryListQuery.OtherStory> = ArrayList()
        for (s in newStatusList) {
            if (s.user()?.name()!!.toLowerCase().contains(text.toLowerCase())) {
                filteredNewNames.add(s)
            }
        }
        statusListAdapter = StatusListAdapter(newStatusList, requireContext(), this@StatusMainFragment, this)
        filterNewList = statusListAdapter.filterList(filteredNewNames)!!
        for (s in seenStatusList) {
            if (s.user()?.name()!!.toLowerCase().contains(text.toLowerCase())) {
                filteredSeenNames.add(s)
            }
        }
        statusSeenAdapter = StatusSeenAdapter(seenStatusList, requireContext(), this@StatusMainFragment, this)
        filterSeenList = statusSeenAdapter.filterList(filteredSeenNames)!!
        if (filterSeenList.isNotEmpty()) {
            root?.txtNoData?.gone()
            root?.relSeenStatus?.visible()
        } else {
            root?.relSeenStatus?.gone()
        }
        if (filterNewList.isNotEmpty()) {
            root?.txtNoData?.gone()
            root?.relNewStatus?.visible()
        } else {
            root?.relNewStatus?.gone()
        }
        if (filterNewList.isEmpty() && filterSeenList.isEmpty()) {
            root?.txtNoData?.visible()
        }

    }

    private fun addBottomToTopAnimation() {
        root?.searchLayout?.visible()
        val slideUp = AnimationUtils.loadAnimation(requireContext(), R.anim.slide_left_bottom)
        root?.searchLayout?.startAnimation(slideUp)
        root?.imgSearch?.gone()
    }

    private fun addTopToBottomAnimation() {
        val slideUp = AnimationUtils.loadAnimation(requireContext(), R.anim.slide_right_bottom)
        root?.searchLayout?.startAnimation(slideUp)
        root?.searchLayout?.gone()
        root?.imgSearch?.visible()
        root?.imgSearch?.alpha = 0f
        root?.imgSearch?.animate()?.alpha(1f)?.duration = 400
    }

    override fun onItemClickGuestPreview(toJson: String, pos: Int, name: String, image: String, colorCode: String) {
        val bundle = bundleOf(
            "list" to toJson, "pos" to pos, "name" to name, "profile" to image, "profileBorder" to colorCode)
        activity?.openActivity(GuestStatusPreviewActivity::class.java, bundle)
        activity?.overridePendingTransition(0, 0)
    }
}