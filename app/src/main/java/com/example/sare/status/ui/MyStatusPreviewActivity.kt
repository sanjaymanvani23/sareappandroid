package com.example.sare.status.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.customeview.statusView.StoriesProgressView
import com.example.sare.extensions.*
import com.example.sare.status.model.StatusModel
import com.example.sare.status.viewmodel.StatusViewModel
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_my_status_preview.*
import kotlinx.android.synthetic.main.popup_delete.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@RequiresApi(Build.VERSION_CODES.M) class MyStatusPreviewActivity : AppCompatActivity(),
    StoriesProgressView.StoriesListener {
    var statusViewModel: StatusViewModel? = null
    private var counter = 0
    var pressTime = 0L
    var duration = 0L
    var limit = 500L
    var token: String? = ""
    var type: Int = 0
    var images: ArrayList<StatusModel> = ArrayList()
    var cacheDataSourceFactory: CacheDataSourceFactory? = null
    private var simpleExoPlayer: SimpleExoPlayer? = null
    val simpleCache = MyApplication.simpleCache
    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private val onTouchListener = View.OnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                pressTime = System.currentTimeMillis()
                progressMyStatus?.pause()
                simpleExoPlayer?.playWhenReady = false
                return@OnTouchListener false
            }
            MotionEvent.ACTION_UP -> {
                val now = System.currentTimeMillis()
                progressMyStatus?.resume()
                simpleExoPlayer?.playWhenReady = true
                simpleExoPlayer?.seekTo(simpleExoPlayer?.currentPosition!!)
                return@OnTouchListener limit < now - pressTime
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_status_preview)
        setTheme()
        val policy = StrictMode.ThreadPolicy.Builder().permitDiskWrites().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)
        Log.d("MyStatusPreviewActivity", "On create")
        statusViewModel = ViewModelProvider(this)[StatusViewModel::class.java]
        getSharedPreferences()
        setData()
        setListeners()
    }

    fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    fun setData() {
        val data = intent?.getStringExtra("list")
        val name = intent?.getStringExtra("name")
        val profile = intent?.getStringExtra("profile")
        var profileBorder = intent?.getStringExtra("profileBorder")
        val gson = Gson()
        val myType = object : TypeToken<StoryListQuery.MyStories>() {}.type
        val myStories = gson.fromJson<StoryListQuery.MyStories>(data, myType) as StoryListQuery.MyStories
        myStories.story()?.reverse()
        txtMyStatus.text = name.toString()
        imgProfile.loadUrl(profile!!)
        imgProfileBorder.setBorder(profileBorder.toString())

        for (i in 0 until myStories.story()!!.size) {
            if (myStories.story()?.get(i)?.imageName().toString().endsWith(".png") || myStories.story()?.get(i)
                    ?.imageName().toString().endsWith(".jpg") || myStories.story()?.get(i)?.imageName().toString()
                    .endsWith(".jpeg")
            ) {
                images.add(
                    StatusModel(
                        false,
                        Utils.PUBLIC_URL + myStories.story()?.get(i)?.imageName().toString(),
                        myStories.story()?.get(i)?.content().toString(),
                        myStories.story()?.get(i)?.createdAt().toString(),
                        myStories.story()?.get(i)?.views().toString(),
                        myStories.story()?.get(i)?._id().toString(),
                        false))
            } else {
                images.add(
                    StatusModel(
                        true,
                        AmazonUtil.getSignedUrl(myStories.story()?.get(i)?.videoName().toString())!!,
                        myStories.story()?.get(i)?.content().toString(),
                        myStories.story()?.get(i)?.createdAt().toString(),
                        myStories.story()?.get(i)?.views().toString(),
                        myStories.story()?.get(i)?._id().toString(),
                        false))
            }

        }
        images.toTypedArray()
        progressMyStatus?.setStoriesCount(images.size)
        Handler().postDelayed(Runnable {
            if (images[counter].isVideo) {
                try {
                    val retriever = MediaMetadataRetriever()
                    retriever.setDataSource(images[counter].urlPath, HashMap<String, String>())
                    duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
                    retriever.release()
                    progressMyStatus?.setStoryDuration(duration)
                } catch (e: Exception) {
                    e.printStackTrace()
                    progressMyStatus?.setStoryDuration(15000L)
                }
            } else {
                progressMyStatus?.setStoryDuration(5000L)
            }
            progressMyStatus?.setStoriesListener(this)
            progressMyStatus?.startStories()
            Log.d("MyStatusPreviewActivity", "setStoryData")
            setStoryData(counter)
        }, 50)

        // bind reverse view
        reverse?.setOnClickListener { progressMyStatus?.reverse() }
        reverse?.setOnTouchListener(onTouchListener)

        // bind skip view
        skip?.setOnClickListener { progressMyStatus?.skip() }
        skip?.setOnTouchListener(onTouchListener)
    }

    private fun prepareVideoPlayer() {
        simpleExoPlayer = let { ExoPlayerFactory.newSimpleInstance(it) }
        cacheDataSourceFactory = simpleCache?.let {
            CacheDataSourceFactory(it, DefaultHttpDataSourceFactory(Util.getUserAgent(this, "App")))
        }
    }

    private fun getPlayer(): SimpleExoPlayer? {
        if (simpleExoPlayer == null) {
            prepareVideoPlayer()
        }
        return simpleExoPlayer
    }

    private fun setListeners() {
        imgBack?.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onNext() {
        counter = ++counter
        setStoryData(counter)
    }

    override fun onPrev() {
        if (counter - 1 < 0) return
        counter = --counter
        setStoryData(counter)
    }

    override fun onDestroy() {
        // Very important !
        simpleExoPlayer?.playWhenReady = false
        simpleExoPlayer?.stop()
        progressMyStatus?.destroy()
        super.onDestroy()
    }

    override fun onComplete() {
        progressMyStatus?.destroy()
        onBackPressed()
    }

    override fun onStop() {
        simpleExoPlayer?.playWhenReady = false
        simpleExoPlayer?.stop()
        progressMyStatus?.destroy()
        super.onStop()
    }

    private fun initVideoView(videoFile: String) {
        //updateVideoProgress()
        videoStatus.setVideoURI(Uri.parse(videoFile))
        videoStatus.start()
    }

    private fun setStoryData(counter: Int) {
        if (images[counter].isVideo) {
            imgMyStatus.gone()
            playerView.visible()
            //            initVideoView(images[counter].urlPath)
            initializePlayer(images[counter].urlPath)
            try {
                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(images[counter].urlPath, HashMap<String, String>())
                duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION).toLong()
                retriever.release()
                progressMyStatus?.setStoryDuration(duration)
            } catch (e: Exception) {
                e.printStackTrace()
                progressMyStatus?.setStoryDuration(15000L)
            }
            txtCaption.text = images[counter].caption
            txtTime.text = getParsedTime(images[counter].time)
            txtViews.text = images[counter].views

            relTotalViews?.setOnClickListener {
                startActivityForResult(
                    Intent(this, StatusViewsActivity::class.java).putExtra(
                        "storyId", images[counter].storyId), 100)
            }
            imgDeleteStatus.setOnClickListener {
                deleteStoryPopup(images[counter].storyId)
            }
        } else {
            imgMyStatus.visible()
            playerView.gone()
            progressBar.gone()
            Glide.with(this).load(images[counter].urlPath).into(imgMyStatus)
            progressMyStatus?.setStoryDuration(5000L)
            txtTime.text = getParsedTime(images[counter].time)
            txtCaption.text = images[counter].caption
            txtViews.text = images[counter].views
            relTotalViews?.setOnClickListener {
                startActivityForResult(
                    Intent(this, StatusViewsActivity::class.java).putExtra(
                        "storyId", images[counter].storyId), 100)
            }
            imgDeleteStatus.setOnClickListener {
                deleteStoryPopup(images[counter].storyId)
            }
        }
        Log.d("MyStatusPreviewActivity", "setStoryData Complete")

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        images.clear()
        counter = 0
        setData()
    }

    private fun getParsedTime(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        df.setTimeZone(TimeZone.getTimeZone("GMT"));
        val SECOND_MILLIS = 1000
        val MINUTE_MILLIS = 60 * SECOND_MILLIS
        val HOUR_MILLIS = 60 * MINUTE_MILLIS
        var parsedDate: String? = null
        parsedDate = try {
            var time: Long = df.parse(date).time
            if (time < 1000000000000L) {
                // if timestamp given in seconds, convert to millis
                time *= 1000;
            }
            val now = System.currentTimeMillis()
            val diff = now - time;
            when {
                diff < MINUTE_MILLIS -> {
                    return "just now";
                }
                diff < 2 * MINUTE_MILLIS -> {
                    return "a minute ago";
                }
                diff < 50 * MINUTE_MILLIS -> {
                    return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                }
                diff < 90 * MINUTE_MILLIS -> {
                    return "an hour ago";
                }
                diff < 24 * HOUR_MILLIS -> {
                    return (diff / HOUR_MILLIS).toString() + " hours ago";
                }
                else -> {
                    parsedDate = "24 hours ago"
                    return parsedDate.toString()
                }
            }

        } catch (e: ParseException) {
            e.printStackTrace()
            parsedDate = "24 hours ago"
            return parsedDate.toString()
        }
        return parsedDate.toString()
    }

    private fun deleteStory(storyId: String) {
        statusViewModel?.deleteStory(token.toString(), storyId.toString())
        statusViewModel?.deleteStory?.observe(this, Observer { it ->
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relGuestStory), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            statusViewModel?.deleteStory(token.toString(), storyId.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }
                is NetworkResponse.SUCCESS.deleteStory -> {
                    Log.d("tag", "deleteStory:::${it.response}")
                    if (it.response.status()) {
                        Log.d("tag", "deleteStory:::${it.response.status()}")
                        onBackPressed()
                    }
                }
            }
        })
    }

    private fun deleteStoryPopup(storyId: String) {
        simpleExoPlayer?.pause()
        simpleExoPlayer?.playWhenReady = false
        progressMyStatus.pause()
        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_delete_status)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnDelete.setOnClickListener {
            deleteStory(storyId)
            dialog.dismiss()
        }
        dialog.btnCancel.setOnClickListener {
            progressMyStatus?.resume()
            simpleExoPlayer?.playWhenReady = true
            simpleExoPlayer?.seekTo(simpleExoPlayer?.currentPosition!!)
            progressMyStatus?.resume()
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun initializePlayer(url: String) {
        simpleExoPlayer = getPlayer()
        playerView.player = simpleExoPlayer
        val uri = Uri.parse(url)
        val mediaSource = ProgressiveMediaSource.Factory(cacheDataSourceFactory!!).createMediaSource(uri)
        simpleExoPlayer?.prepare(mediaSource, true, true)
        simpleExoPlayer?.playWhenReady = true
        simpleExoPlayer?.addListener(object : Player.EventListener {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playWhenReady) {
                    progressBar.gone()
                }
                if (playbackState == ExoPlayer.STATE_BUFFERING) {
                    progressBar.visible()
                } else if (playbackState == ExoPlayer.STATE_READY) {
                    progressBar.gone()
                }
            }
        })
    }
}