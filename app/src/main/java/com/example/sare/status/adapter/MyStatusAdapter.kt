package com.example.sare.status.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import kotlinx.android.synthetic.main.item_my_status.view.*

class MyStatusAdapter(
    var list: MutableList<String>,
    private val context: Context,
    private val fragment: Fragment
) :
    RecyclerView.Adapter<MyStatusAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyStatusAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_my_status, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.toString(), context, fragment)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(
            item: String,
            context: Context,
            fragment: Fragment
        ) {
            val name = itemView.txtViews
            val time = itemView.txtTime

            val imgStatus = itemView.imgStatus
            val imgProfileBorder = itemView.imgStatusDelete

            imgStatus.setOnClickListener {
                /*fragment.findNavController().navigate(
                    R.id.action_myStatusFragment_to_myStatusViewFragment
                )*/
            }

        }

    }

}