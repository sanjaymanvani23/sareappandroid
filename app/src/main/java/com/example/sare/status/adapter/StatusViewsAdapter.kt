package com.example.sare.status.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.StoryViewerListQuery
import com.example.sare.Utils
import com.example.sare.extensions.gone
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.extensions.visible
import kotlinx.android.synthetic.main.item_status_list.view.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StatusViewsAdapter(
    var list: List<StoryViewerListQuery.StoryViewerList>,
    private val context: Context
) :
    RecyclerView.Adapter<StatusViewsAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): StatusViewsAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_status_list, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(
            item: StoryViewerListQuery.StoryViewerList,
            context: Context
        ) {
            val name = itemView.txtName
            val time = itemView.txtTime
            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val relSingleItem = itemView.relSingleItem
            val txtStoryCount = itemView.txtStoryCount

            relSingleItem.visible()
            txtStoryCount.gone()
            name.text = item.user()?.name().toString()
            imgProfile.loadUrl(Utils.PUBLIC_URL + item.user()!!.image())
            time.text = getParsedTime(item.viewedAt().toString())
            imgProfileBorder?.setBorder(item.user()!!.colorCode().toString())

        }
        private fun getParsedTime(date: String): String {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            val SECOND_MILLIS = 1000
            val MINUTE_MILLIS = 60 * SECOND_MILLIS
            val HOUR_MILLIS = 60 * MINUTE_MILLIS
            val DAY_MILLIS = 24 * HOUR_MILLIS
            val WEEK_MILLIS = 7 * DAY_MILLIS
            var parsedDate: String? = null
            parsedDate = try {
                var time: Long = df.parse(date).time
                if (time < 1000000000000L) {
                    // if timestamp given in seconds, convert to millis
                    time *= 1000;
                }
                val now = System.currentTimeMillis()
                val diff = now - time;
                when {
                    diff < MINUTE_MILLIS -> {
                        return "just now";
                    }
                    diff < 2 * MINUTE_MILLIS -> {
                        return "a minute ago";
                    }
                    diff < 50 * MINUTE_MILLIS -> {
                        return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                    }
                    diff < 90 * MINUTE_MILLIS -> {
                        return "an hour ago";
                    }
                    diff < 24 * HOUR_MILLIS -> {
                        return (diff / HOUR_MILLIS).toString() + " hours ago";
                    }
                    else -> {
                        parsedDate = "24 hours ago"
                        return parsedDate.toString()
                    }
                }

            } catch (e: ParseException) {
                e.printStackTrace()
                parsedDate = "24 hours ago"
                return parsedDate.toString()
            }
            return parsedDate.toString()
        }
    }
}