package com.example.sare.status.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.FollowerListQuery
import com.example.sare.MainActivity
import com.example.sare.R
import com.example.sare.StoryListQuery
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.extensions.gone
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.status.listener.ItemOnClickListener
import com.example.sare.status.ui.GuestStatusPreviewActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.item_status_list.view.*
import org.jetbrains.annotations.Nullable
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StatusSeenAdapter(var list: @Nullable MutableList<StoryListQuery.OtherStory>,
    private val context: Context,
    private val fragment: Fragment,
    var listener: ItemOnClickListener) : RecyclerView.Adapter<StatusSeenAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatusSeenAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_status_list, parent, false)
        return ViewHolder(v, listener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, fragment)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View,var listener: ItemOnClickListener) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(item: StoryListQuery.OtherStory, context: Context, fragment: Fragment) {
            val name = itemView.txtName
            val txtTime = itemView.txtTime
            val imgProfile = itemView.imgProfile
            val relSingleItem = itemView.relDetails
            val imgProfileBorder = itemView.imgProfileBorder
            val txtStoryCount = itemView.txtStoryCount
            txtStoryCount.gone()
            name.text = item.user()!!.name()
            imgProfile.loadUrl(PUBLIC_URL + item.user()!!.image())
            val size=item.user()?.story()?.size?.minus(1)
            txtTime.text = getParsedTime(item.user()?.story()?.get(size!!)?.createdAt().toString())
            imgProfileBorder?.setBorder("#919191")
            relSingleItem.setOnClickListener {
                listener.onItemClickGuestPreview(
                    Gson().toJson(item.user()?.story()),
                    adapterPosition,
                    item.user()?.name().toString(),
                    PUBLIC_URL + item.user()?.image(),
                    item.user()?.colorCode().toString())
            }


        }

        private fun getParsedTime(date: String): String {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            val SECOND_MILLIS = 1000
            val MINUTE_MILLIS = 60 * SECOND_MILLIS
            val HOUR_MILLIS = 60 * MINUTE_MILLIS
            val DAY_MILLIS = 24 * HOUR_MILLIS
            val WEEK_MILLIS = 7 * DAY_MILLIS
            var parsedDate: String? = null
            parsedDate = try {
                var time: Long = df.parse(date).time
                if (time < 1000000000000L) {
                    // if timestamp given in seconds, convert to millis
                    time *= 1000;
                }
                val now = System.currentTimeMillis()
                val diff = now - time;
                when {
                    diff < MINUTE_MILLIS -> {
                        return "just now";
                    }
                    diff < 2 * MINUTE_MILLIS -> {
                        return "a minute ago";
                    }
                    diff < 50 * MINUTE_MILLIS -> {
                        return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                    }
                    diff < 90 * MINUTE_MILLIS -> {
                        return "an hour ago";
                    }
                    diff < 24 * HOUR_MILLIS -> {
                        return (diff / HOUR_MILLIS).toString() + " hours ago";
                    }
                    else -> {
                        parsedDate = "24 hours ago"
                        return parsedDate.toString()
                    }
                }

            } catch (e: ParseException) {
                e.printStackTrace()
                parsedDate = "24 hours ago"
                return parsedDate.toString()
            }
            return parsedDate.toString()
        }
    }

    fun filterList(filteredNames: List<StoryListQuery.OtherStory>): List<StoryListQuery.OtherStory>? {
        this.list = filteredNames as MutableList<StoryListQuery.OtherStory>
        notifyDataSetChanged()
        return filteredNames
    }
}



