package com.example.sare.status.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class StatusViewModel : ViewModel() {
    val storyList = MutableLiveData<NetworkResponse>()
    val statusViewerList = MutableLiveData<NetworkResponse>()
    val checkStory = MutableLiveData<NetworkResponse>()
    val deleteStory = MutableLiveData<NetworkResponse>()

    fun getStoryList(token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(StoryListQuery(0, 1000, ""))
            .enqueue(object : ApolloCall.Callback<StoryListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    storyList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<StoryListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.storyList() != null) {
                                storyList.postValue(NetworkResponse.SUCCESS.getStoryList(response.data()?.storyList()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                storyList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            storyList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getStoryChecked(token: String, storyId: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.mutate(CheckStoryMutation(storyId))
            .enqueue(object : ApolloCall.Callback<CheckStoryMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    checkStory.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<CheckStoryMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.addStoryView() != null) {
                                checkStory.postValue(NetworkResponse.SUCCESS.getStoryChecked(response.data()?.addStoryView()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                checkStory.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            checkStory.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getStatusViewerList(token: String, storyId: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(StoryViewerListQuery(storyId))
            .enqueue(object : ApolloCall.Callback<StoryViewerListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    statusViewerList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<StoryViewerListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.storyViewerList() != null) {
                                statusViewerList.postValue(NetworkResponse.SUCCESS.getStoryViewerList(response.data()?.storyViewerList()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                statusViewerList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            statusViewerList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun deleteStory(token: String, storyId: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.mutate(DeleteStoryMutation(storyId))
            .enqueue(object : ApolloCall.Callback<DeleteStoryMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    deleteStory.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<DeleteStoryMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.deleteStory() != null) {
                                deleteStory.postValue(NetworkResponse.SUCCESS.deleteStory(response.data()?.deleteStory()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                deleteStory.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString
                                    ()))
                            }
                        } catch (e: Exception) {
                            deleteStory.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}