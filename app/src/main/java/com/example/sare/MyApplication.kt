package com.example.sare

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Build
import android.os.StrictMode
import android.util.DisplayMetrics
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.work.WorkManager
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.file.FileUtils
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.upstream.cache.LeastRecentlyUsedCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import com.google.firebase.FirebaseApp
import iknow.android.utils.BaseUtils
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.logging.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.dsl.module

class MyApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        BaseUtils.init(this)
        FileUtils.createApplicationFolder()

        FirebaseApp.initializeApp(this@MyApplication)

        startKoin {
            androidContext(this@MyApplication)
            modules(module {
                single { WorkManager.getInstance(get()) }
                single { initKtorClient() }
            })
        }

        val leastRecentlyUsedCacheEvictor = LeastRecentlyUsedCacheEvictor(90 * 1024 * 1024)
        val databaseProvider: DatabaseProvider = ExoDatabaseProvider(this)

        if (simpleCache == null) {
            simpleCache = SimpleCache(cacheDir, leastRecentlyUsedCacheEvictor, databaseProvider)
        }
    }


    fun initKtorClient() = HttpClient(Android) {
        install(Logging) {
            logger = Logger.ANDROID
            level = LogLevel.ALL
        }
    }

    companion object {
        @JvmStatic
        var simpleCache: SimpleCache? = null
        var context: Context? = null


        lateinit var mainActivity:MainActivity

        lateinit var bottomBarActivity:BottomBarActivity

        fun mainActivityInsnace(mainActivity1:MainActivity)
        {
            this.mainActivity=mainActivity1
        }
        fun mainActivityInsnace():MainActivity
        {
            return  mainActivity
        }

        fun bottomBarActivityInsnace(mainActivity1: BottomBarActivity)
        {
            this.bottomBarActivity=mainActivity1
        }
        fun bottomBarActivityInsnace():BottomBarActivity
        {
            return  bottomBarActivity
        }

        @RequiresApi(Build.VERSION_CODES.M) fun logoutClearPreference(context: Context) {
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile,
                Context.MODE_PRIVATE
            )
            preferences.edit().clear().apply()

        }
    }

}

