package com.example.sare.profile.fragment

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.hideKeyboard
import com.example.sare.extensions.*
import com.example.sare.profile.adapter.ReportListAdapter
import com.example.sare.profile.adapter.VideoGridAdapter
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.profile.viewmodel.UserVideoListViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.view.*
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.view.header_cover_image
import kotlinx.android.synthetic.main.layout_profile_more_option.view.*
import kotlinx.android.synthetic.main.popup_report.view.*
import org.json.JSONObject
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ProfileGuestFragment : Fragment(), ItemOnclickListener {
    var root: View? = null
    var data: ArrayList<String> = ArrayList()

    var token: String? = null
    var videoListViewModel: UserVideoListViewModel? = null
    var userId: String? = null
    var type: String? = null
    var showRecyclerView: Boolean = false
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var videoList: MutableList<UserVideosQuery.UserVideo> = mutableListOf()

    private var profileUserVideoList: MutableList<VideoListQuery.Video> = mutableListOf()
    var progressDialog: CustomProgressDialogNew? = null
    var name: String? = null
    var colorCode: String? = null
    var image: String? = null
    var id: String? = null
    var alpha: Float = 0.0f
    lateinit var onBackPressedCallback: OnBackPressedCallback

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_profile_guest, container, false)
        requireActivity().window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        getSharedPreferences()
        progressDialog = CustomProgressDialogNew(requireContext())
        userId = arguments?.get("userId").toString()
        type = arguments?.get("type").toString()
        getUserDetails()

        onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {

                if (type == "FOLLOWERS") {
                    val bundle = bundleOf(
                        "userId" to id,
                        "name" to name,
                        "colorCode" to colorCode,
                        "image" to image,
                        "type" to "FOLLOWERS"
                    )

                    if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                        findNavController().navigate(
                            R.id.action_profileGuestFragment_to_followersFragment,
                            bundle
                        )
                    }
                } else if (type == "FOLLOWINGS") {
                    val bundle = bundleOf(
                        "userId" to id,
                        "name" to name,
                        "colorCode" to colorCode,
                        "image" to image,
                        "type" to "FOLLOWINGS"
                    )
                    if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                        findNavController().navigate(
                            R.id.action_profileGuestFragment_to_followersFragment,
                            bundle
                        )
                    }

                } else {
                    NavHostFragment.findNavController(this@ProfileGuestFragment)
                        .navigate(R.id.homeFragment2)
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback
        )
        Log.d("tag", "userId::::$userId")
        setTheme()
        videoListViewModel = ViewModelProvider(this)[UserVideoListViewModel::class.java]
        setBottomSheet()
        initRecyclerView()
        setObservers()
        getVideoList()
        setListeners()
        collapsing()



        return root
    }

    private fun setObservers() {
        getVideoListObserver()
    }


    private fun collapsing() {
        val mAppBarLayout = root!!.findViewById(R.id.AppFragment_AppBarLayout) as AppBarLayout
        val mToolbar = root!!.findViewById(R.id.AppFragment_Toolbar) as Toolbar
        mAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset -> /* mToolbar.setBackgroundColor(
                        changeAlpha(
                            resources.getColor(R.color.pureWhite),
                            Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()
                        )
                    )*/

            mToolbar.alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()

            alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()
            Log.d("tag", "alpha::::::$alpha")
            Log.d("tag", "videoList.size::::::${videoList.size}")
            if (alpha == 1f) {
                Log.d("tag", "1f::::::")
                mToolbar.visibility = View.VISIBLE
                root?.btnUpScroll?.visible()
                root?.imgMore1?.isClickable = true
                root?.imgBack?.isClickable = true
                root?.ll_back?.isClickable = false
                root?.imgMore?.isClickable = false

            } else if (alpha > 0.5f) {
                mToolbar.visibility = View.VISIBLE

            } else if (alpha == 0f) {
                Log.d("tag", "else::::")
                mToolbar.visibility = View.GONE
                root?.btnUpScroll?.gone()
                root?.imgMore1?.isClickable = false
                root?.imgBack?.isClickable = false
                root?.ll_back?.isClickable = true
                root?.imgMore?.isClickable = true
            }


        })


    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(),
            StringSingleton.transparent
        )
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(),
                StringSingleton.transparent
            )
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun setListeners() {
        root?.btnUpScroll?.setOnClickListener {
            root?.btnUpScroll?.gone()
            root?.recyclerView?.smoothScrollToPosition(0)
            root?.AppFragment_AppBarLayout!!.setExpanded(true,true)
        }
        root?.ll_back?.setOnClickListener {

            if (type == "FOLLOWERS") {
                val bundle = bundleOf(
                    "userId" to id,
                    "name" to name,
                    "colorCode" to colorCode,
                    "image" to image,
                    "type" to "FOLLOWERS"
                )

                if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                    findNavController().navigate(
                        R.id.action_profileGuestFragment_to_followersFragment,
                        bundle
                    )
                }
            } else if (type == "FOLLOWINGS") {
                val bundle = bundleOf(
                    "userId" to id,
                    "name" to name,
                    "colorCode" to colorCode,
                    "image" to image,
                    "type" to "FOLLOWINGS"
                )
                if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                    findNavController().navigate(
                        R.id.action_profileGuestFragment_to_followersFragment,
                        bundle
                    )
                }

            } else {
                NavHostFragment.findNavController(this@ProfileGuestFragment)
                    .navigate(R.id.homeFragment2)
            }
        }
        root?.imgBack?.setOnClickListener {
            //NavHostFragment.findNavController(this).navigateUp()
            if (type == "FOLLOWERS") {
                val bundle = bundleOf(
                    "userId" to id,
                    "name" to name,
                    "colorCode" to colorCode,
                    "image" to image,
                    "type" to "FOLLOWERS"
                )
                if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
//                    findNavController().popBackStack(R.id.profileGuestFragment, false);
                    findNavController().navigate(
                        R.id.action_profileGuestFragment_to_followersFragment,
                        bundle
                    )
                }
            } else if (type == "FOLLOWINGS") {
                val bundle = bundleOf(
                    "userId" to id,
                    "name" to name,
                    "colorCode" to colorCode,
                    "image" to image,
                    "type" to "FOLLOWINGS"
                )
                if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                    findNavController().navigate(
                        R.id.action_profileGuestFragment_to_followersFragment,
                        bundle
                    )
                }
            } else {
                NavHostFragment.findNavController(this@ProfileGuestFragment)
                    .navigate(R.id.homeFragment2)
            }
        }

    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        token = preferences.getString("TOKEN", null)
        val user = preferences.getString("USER", null)

        Log.d("tag", "user:::::$user")
        if (user != null) {
            val mainObject = JSONObject(user)

            id = mainObject.getString("_id")
            name = mainObject.getString("name")
            if (mainObject.has("image")) {
                image = mainObject.getString("image")
            }
            if (mainObject.has("colorCode")) {
                colorCode = mainObject.getString("colorCode")
            }
        }
        Log.d("tag", "token::$token")


    }


    private fun getVideoList() {
//        videoList.clear()
        /* isLoading = false
         isLastPage = false
         currentPage = 0*/
        // progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog?.show()
        }
        Log.d("tag", "Pagination Current page : " + currentPage)
        videoListViewModel?.getUserVideoList(
            requireContext(),
            userId.toString(),
            currentPage * PAGE_SIZE
        )
    }

    override fun onResume() {
        super.onResume()
        videoList.clear()
        isLoading = false
        isLastPage = false
        currentPage = 0
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun getVideoListObserver() {
        videoListViewModel?.videoArrayList?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog?.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    requireActivity().runOnUiThread {
                        progressDialog?.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getUserVideoList(
                                requireContext(),
                                userId.toString(),
                                currentPage * PAGE_SIZE
                            )
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(),0)
                }


                is NetworkResponse.SUCCESS.userVideoPublishedList -> {
                    requireActivity().runOnUiThread {
                        progressDialog?.hide()
                    }
                    isLoading = false
                    Log.d("tag", "response::" + it.response)
                    if (it.response != null) {


                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(it.response)
                        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                        val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(userInfoListJsonString, myType)

                        profileUserVideoList.addAll(userVideoList)

                        videoList.addAll(it.response)

                        root?.recyclerView?.adapter?.notifyDataSetChanged()
                        if (it.response.size < PAGE_SIZE) {
                            isLastPage = true
                        }


                        if (root?.recyclerView?.adapter?.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.relScroll?.visibility = View.GONE
                            disableScroll(root?.AppFragment_AppBarLayout!!)
                            /*root?.txtNoData?.setOnClickListener {
                                requireActivity().window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            }*/

//                            }
                        } else {
                            enableScroll(root?.AppFragment_CollapsingToolbarLayout!!)
                            root?.txtNoData?.visibility = View.GONE
                            root?.relScroll?.visibility = View.VISIBLE

                            /* root?.txtNoData?.setOnClickListener {
                                 requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                             }*/
                        }

                    } else {
                        requireActivity().runOnUiThread {
                            customToast(getString(R.string.api_error_message), requireActivity(),0)
                        }
                    }
                }
            }

        })
    }

    private fun enableScroll(collapsingToolbarLayout: CollapsingToolbarLayout) {
        Log.d("tag", "enableScroll")

        val params =
            collapsingToolbarLayout.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags = (
                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                        or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED
                )
        collapsingToolbarLayout.setLayoutParams(params)
    }

    private fun disableScroll(appBarLayout: AppBarLayout) {
        Log.d("tag", "disableScroll")
        /* val params =
             collapsingToolbarLayout.layoutParams as AppBarLayout.LayoutParams
         params.scrollFlags = 0
 //        collapsingToolbarLayout.isNestedScrollingEnabled = true
         collapsingToolbarLayout.layoutParams = params*/

        val params = appBarLayout.layoutParams as CoordinatorLayout.LayoutParams

        if (params.behavior == null)
            params.behavior = AppBarLayout.Behavior()
        val behaviour = params.behavior as AppBarLayout.Behavior
        behaviour.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                return false
            }
        })
    }

    private fun disableScroll(relativeLayout: RelativeLayout) {
        Log.d("tag", "disableScroll")
        /* val params =
             collapsingToolbarLayout.layoutParams as AppBarLayout.LayoutParams
         params.scrollFlags = 0
 //        collapsingToolbarLayout.isNestedScrollingEnabled = true
         collapsingToolbarLayout.layoutParams = params*/
        /*relativeLayout.isEnabled = false
        relativeLayout.isClickable = false*/
        //     requireActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        for (i in 0 until relativeLayout.getChildCount()) {
            val child: View = relativeLayout.getChildAt(i)
            child.isEnabled = false
        }

    }

    private fun initRecyclerView() {

        root?.recyclerView?.apply {
            layoutManager = GridLayoutManager(
                root?.recyclerView?.context,
                3
            )

            addOnScrollListener(object :
                PaginationListener(this.layoutManager as GridLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getVideoList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }


            })
            adapter = VideoGridAdapter(
                videoList,
                context, this@ProfileGuestFragment
            )

        }


    }

    private fun setBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_options

        val bottomSheetBehavior: BottomSheetBehavior<*> =
            BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        root?.imgMore1?.setOnClickListener {
            root?.overlay_view_guest?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.imgMore?.setOnClickListener {
            root?.overlay_view_guest?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.overlay_view_guest?.setOnClickListener {

            root?.overlay_view_guest?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        root?.overlay_view_report_popup?.setOnClickListener {

            root?.overlay_view_report_popup?.visibility = View.GONE
            root?.layout_popup_report?.visibility = View.GONE
            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }
        root?.bottom_sheet_options?.txtReport?.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            root?.overlay_view_report_popup?.visibility = View.VISIBLE
            root?.layout_popup_report?.visibility = View.VISIBLE
            root?.ll_select_report_reason?.setOnClickListener {
                root?.rvReportList?.visibility = View.VISIBLE
                reportList()
                if (showRecyclerView == true) {

                    root?.rvReportList?.visibility = View.GONE
                    reportList()
                    showRecyclerView = false

                } else {
                    root?.rvReportList?.visibility = View.VISIBLE
                    showRecyclerView = true
                }
            }


            root?.btnReportSend?.setOnClickListener {
                if (root?.txtReportReason?.text!!.isEmpty() || root?.txtTitle?.text!!.isEmpty())
                    customToast("Please select report reason!!",requireActivity(),0)
            }

        }


        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_guest?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_guest?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_guest?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_guest?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }


    private fun reportList() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        videoListViewModel?.getReportList(requireContext())
        videoListViewModel?.reportArrayList?.observe(viewLifecycleOwner, Observer {
            requireActivity().runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar
                        .make(
                            requireView(),
                            "Connection Error!",
                            Snackbar.LENGTH_SHORT
                        )
                        .setAction("RETRY") {
                            videoListViewModel?.getReportList(requireContext())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(),0)
                }

                is NetworkResponse.SUCCESS.getReportList -> {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)

                    initReportRecyclerView(it.response)
                }

            }
        })
    }


    private fun initReportRecyclerView(list: List<ReportListQuery.ReportType>) {
//        root?.rvReportList?.visibility = View.VISIBLE

        root?.rvReportList?.apply {
            layoutManager = LinearLayoutManager(
                root?.rvReportList?.context, RecyclerView.VERTICAL, false
            )
            adapter =
                ReportListAdapter(list, context)

            (adapter as ReportListAdapter).onItemClick = { data ->
                root?.rvReportList?.visibility = View.GONE

                // do something with your item
                Log.d("tag", data.reportName().toString())
                root?.txtReportReason?.text = data.reportName().toString()


                root?.btnReportSend?.setOnClickListener {
                    if (root?.txtReportReason?.text!!.isEmpty() && root?.txtTitle?.text!!.isEmpty()) {
                        customToast(   "Report cant be submitted. Please select report reason!!",requireActivity(),0)
                    } else if (root?.txtReportReason?.text!!.isEmpty()) {
                        customToast("Please select report reason!!",requireActivity(),0)
                    } else if (root?.txtTitle?.text!!.isEmpty()) {
                        customToast( "Please write report title!!",requireActivity(),0)
                    } else if (root?.txtTypeSomething?.text!!.isEmpty()) {
                        customToast( "Please type something about report!!",requireActivity(),0)
                    } else {
                        val apolloClient =
                            ApolloClient.setupApollo(token.toString())

                        Log.d(
                            "tag",
                            "txtReportReason.text.toString()::" + root?.txtReportReason?.text.toString()
                        )

                        apolloClient.mutate(
                            StoreReportMutation(
                                userId.toString(),
                                data.reportType()!!,
                                root?.txtTitle?.text.toString(),
                                root?.txtReportReason?.text.toString()
                            )
                        )
                            .enqueue(object :
                                ApolloCall.Callback<StoreReportMutation.Data>() {
                                override fun onFailure(e: ApolloException) {
                                    e.printStackTrace()
                                    Log.d("tag", "Error::" + e.message)
                                    customToast(e.message.toString(), requireActivity(), 0)
                                    MyApplication.mainActivity.logoutUser()
                                    activity?.openNewActivity(LoginActivity::class.java)
                                }

                                override fun onResponse(response: Response<StoreReportMutation.Data>) {
                                    Log.d("tag", "response::$response")
                                    if (response.data?.storeReportProfile() != null) {
                                        requireActivity().runOnUiThread {
                                            customToast(   "Report Submitted!!",requireActivity(),1)
                                            root?.txtTitle?.setText("")
                                            root?.txtReportReason?.text = ""
                                            root?.txtTypeSomething?.setText("")
                                            hideKeyboard(requireActivity())

                                            root?.overlay_view_report_popup?.visibility =
                                                View.GONE
                                            root?.layout_popup_report?.visibility =
                                                View.GONE
                                            val mainLayout = root?.layout_popup_report


                                        }
                                    } else {
                                        requireActivity().runOnUiThread {
                                            val error = response.errors?.get(0)?.message
                                            Log.d("tag", "Data Error: $error")
                                            val errorCode =
                                                response.errors?.get(0)?.customAttributes?.get("status")

                                            if (errorCode?.toString()?.equals("401") == true) {
                                                customToast(error.toString(), requireActivity(),0)
                                               MyApplication.mainActivity.logoutUser()
                                                 activity?.openNewActivity(LoginActivity::class.java)
                                            } else if (errorCode?.toString()
                                                    ?.equals("403") == true
                                            ) {
                                                customToast(error.toString(), requireActivity(),0)
                                               MyApplication.mainActivity.logoutUser()
                                                 activity?.openNewActivity(LoginActivity::class.java)
                                            }  else {
                                                customToast(error.toString(), requireActivity(), 0)
                                            }
                                            root?.txtTitle?.setText("")
                                            root?.txtReportReason?.text = ""
                                            root?.txtTypeSomething?.setText("")
                                            hideKeyboard(requireActivity())


                                            root?.overlay_view_report_popup?.visibility =
                                                View.GONE
                                            root?.layout_popup_report?.visibility =
                                                View.GONE
                                        }
                                    }
                                }

                            })
                    }
                }
            }

        }
    }


    private fun getUserDetails() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        val apolloClient =
            ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(userId.toString()))
            .enqueue(object : ApolloCall.Callback<UserDetailsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    /*Toast.makeText(requireContext(), e.message.toString(), Toast.LENGTH_SHORT)
                        .show()*/
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }

                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            getUserDetails()
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
//                    runBlocking {
                    Log.d("tag", "ResponseData::$response")
                    requireActivity().runOnUiThread {
                        if (response.data()?.user() != null) {
                            setData(response)
                        } else {
                            val error = response.errors?.get(0)?.message
                            Log.d("tag", "Data Error: $error")
                            val errorCode =
                                response.errors?.get(0)?.customAttributes?.get("status")
                            if (errorCode?.toString()?.equals("401") == true) {
                                customToast(error.toString(), requireActivity(),0)
                                MyApplication.mainActivity.logoutUser()
                                 activity?.openNewActivity(LoginActivity::class.java)
                            } else if (errorCode?.toString()?.equals("403") == true) {
                                customToast(error.toString(), requireActivity(), 0)
                                MyApplication.mainActivity.logoutUser()
                                 activity?.openNewActivity(LoginActivity::class.java)
                            } else {
                                customToast(error.toString(), requireActivity(), 0)
                            }
                        }

                    }
                }
            })
    }

    @SuppressLint("SetTextI18n")
    private fun setData(response: Response<UserDetailsQuery.Data>) {
        val policy =
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        StrictMode.setThreadPolicy(policy)


        root?.relative_layout_followers?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWERS"
            )
            if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                findNavController().navigate(
                    R.id.action_profileGuestFragment_to_followersFragment,
                    bundle
                )
            }
        }



        root?.relative_layout_followings?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWINGS"
            )
            if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                findNavController().navigate(
                    R.id.action_profileGuestFragment_to_followersFragment,
                    bundle
                )
            }
        }


        root?.imgProfilePic?.loadUrl(PUBLIC_URL + response.data?.user()?.image()!!)
        root?.imgProfilePicUser?.setBorder(colorCode ?: Utils.COLOR_CODE)

        root?.imgProfile?.loadUrl(PUBLIC_URL + response.data?.user()?.image()!!)
        root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

        val textToCopy = "https://sare.in/graphql/" + response.data!!.user()._id()

        root?.txtName?.text = response.data!!.user().name().toString()
        root?.txtName1?.text = response.data!!.user().name().toString()
        root?.txtId?.text = "@" + response.data!!.user().username().toString()
        if (response.data!!.user().likeCounts().toString() < 0.toString()) {
            root?.txtLikes?.text = 0.toString() + " Like"
        } else {
            root?.txtLikes?.text = response.data!!.user().likeCounts().toString() + " Likes"
        }

        if (response.data!!.user().followingCounts()!! > 0) {
            root?.txtFollowingCount?.text = response.data!!.user().followingCounts().toString()
        } else {
            root?.txtFollowingCount?.text = 0.toString()
        }
        val imageView = root?.header_cover_image
        Glide.with(requireContext())
            .load(PUBLIC_URL + (response.data?.user()?.coverImage().toString()))
            // .load(Utils.COVER_IMAGE_URL + response.data?.user()?.coverImage())
            .error(R.drawable.cover_default)
            .into(imageView!!)

        root?.txtPostCounts?.text = response.data!!.user().postsCounts().toString()
        root?.txtJoinedDate?.text =
            "Joined On " + getParsedDate(response.data!!.user().createdAt().toString())

        if (response.data!!.user().followerCounts()!! > 0) {
            root?.txtFollowersCount?.text = response.data!!.user().followerCounts().toString()
        } else {
            root?.txtFollowersCount?.text = 0.toString()
        }
//        if (response.data!!.user().s)

        if (response.data!!.user().blocked() == true) {
            root?.bottom_sheet_options?.txtBlock?.text = "Unblock"

        } else {
            root?.bottom_sheet_options?.txtBlock?.text = "Block"

        }


        if (response.data!!.user().followed() == true) {
            root?.button_follow?.text = "Following"
            root?.button_follow?.visibility = View.GONE
            root?.ll_options?.visibility = View.VISIBLE
        } else {
            root?.button_follow?.text = "Follow"
            root?.button_follow?.visibility = View.VISIBLE
            root?.ll_options?.visibility = View.GONE
        }
        root?.button_follow?.setOnClickListener {
            if (root?.button_follow?.text == "Following") {

                root?.button_follow?.visibility = View.GONE
                root?.ll_options?.visibility = View.VISIBLE

            } else {
                root?.button_follow?.text == "Follow"
                followUser(response.data!!.user()._id())

                if (response.data!!.user().followerCounts()!! > 0) {
                    root?.txtFollowersCount?.text =
                        (response.data!!.user().followerCounts()!! + 1).toString()
                } else {
                    root?.txtFollowersCount?.text = 0.toString()
                }

            }
        }

        root?.button_following?.setOnClickListener {
            unFollowUser(response.data!!.user()._id())
            if (response.data!!.user().followerCounts()!! > 0) {
                root?.txtFollowersCount?.text =
                    (root?.txtFollowersCount?.text.toString().toInt() - 1).toString()
            } else {
                root?.txtFollowersCount?.text = 0.toString()
            }

            root?.button_follow?.visibility = View.VISIBLE
            root?.ll_options?.visibility = View.GONE
        }

        if (!response.data!!.user().bio().isNullOrEmpty()) {
            root?.quote?.visibility = View.VISIBLE
            root?.quote?.text = response.data!!.user().bio().toString()
        } else {
            root?.quote?.visibility = View.GONE
        }

//        root?.button_chat?.setOnClickListener {
//
//        }

        root?.bottom_sheet_options?.txtCopyProfileLink?.setOnClickListener {
            val clipboardManager =
                requireActivity().getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clipData = ClipData.newPlainText("text", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Log.d("tag", "clip data::$clipData")
            customToast("Link copied to clipboard!!",requireActivity(),1)
        }
        root?.bottom_sheet_options?.txtShareProfile?.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_SUBJECT, "Share Profile Link.!!")
            share.putExtra(Intent.EXTRA_TEXT, textToCopy)
            startActivity(Intent.createChooser(share, "Share link!"))

        }


//        if (response.data!!.user().status() == "ACTIVE") {
//            root?.bottom_sheet_options?.txtBlock?.text = "Block"
        root?.bottom_sheet_options?.txtBlock?.setOnClickListener {
            if (response.data!!.user()
                    .blocked() == true || root?.bottom_sheet_options?.txtBlock?.text == "Unblock"
            ) {
                unBlockUser(response.data!!.user()._id().toString())
            } else {
                blockUser(response.data!!.user()._id().toString())
            }
        }


        root?.button_chat?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image()
            )
            if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                findNavController().navigate(
                    R.id.action_profileGuestFragment_to_chatFragment,
                    bundle
                )
            }
        }


    }

    private fun getParsedDate(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        var parsedDate: String? = null
        parsedDate = try {
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("MMMM yyyy")
            outputFormatter1.format(date1)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        return parsedDate.toString()
    }


    private fun blockUser(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::$token")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            BlockUserMutation(
                id
            )
        )

            .enqueue(object : ApolloCall.Callback<BlockUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            blockUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<BlockUserMutation.Data>) {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data != null && response.data?.blockUser()?.status()!!) {
                        requireActivity().runOnUiThread {
                                                        customToast("User Blocked!",requireActivity(),1)
                        }
                        root?.bottom_sheet_options?.txtBlock?.text = "Unblock"


                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode =
                            response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), requireActivity(),0)
                        }
                    }

                }
            })
    }


    private fun unBlockUser(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::$token")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            UnBlockUserMutation(
                id
            )
        )

            .enqueue(object : ApolloCall.Callback<UnBlockUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            unBlockUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UnBlockUserMutation.Data>) {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data != null && response.data?.unblockUser()?.status()!!) {
                        requireActivity().runOnUiThread {
                            customToast("User Unblocked!",requireActivity(),1)
                        }
                        root?.bottom_sheet_options?.txtBlock?.text = "Block"


                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode =
                            response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), requireActivity(),0)
                        }
                    }

                }
            })
    }


    private fun followUser(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::$token")
        Log.d("tag", "id::$id")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            FollowUserMutation(
                id
            )
        )

            .enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    customToast(e.message.toString(), requireActivity(), 0)
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            followUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<FollowUserMutation.Data>) {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data?.follow()?.status() == true) {
                        Log.d("tag", "response follow:::$response")
                        requireActivity().runOnUiThread {
                            root?.button_follow?.visibility = View.GONE
                            root?.button_follow?.text = "Following"
                            root?.ll_options?.visibility = View.VISIBLE
                        }


                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode =
                            response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), requireActivity(),0)
                        }
                    }

                }
            })

    }


    private fun unFollowUser(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        requireActivity().runOnUiThread {
            progressDialog.show()
        }
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            UnFollowUserMutation(
                id
            )
        )
            .enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar
                        .make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            unFollowUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                    requireActivity().runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response unfollow:::$response")
                    if (response.data?.unfollow()?.status() == true) {
                        requireActivity().runOnUiThread {

                            root?.button_follow?.visibility = View.VISIBLE
                            root?.button_follow?.text = "Follow"
                            root?.ll_options?.visibility = View.GONE
                            /*   root?.txtFollowersCount?.text =
                                (root?.txtFollowersCount?.text.toString().toInt() + 1).toString()*/

                        }


                    } else {

                        val error = response.errors?.get(0)?.message
                        val errorCode =
                            response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), requireActivity(),0)
                            MyApplication.mainActivity.logoutUser()
                             activity?.openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), requireActivity(),0)
                        }
                    }

                }
            })
    }


    override fun onItemClickListener(position: Int) {
        if (findNavController().currentDestination?.id == R.id.profileGuestFragment) {
            val gson = Gson()
            val userInfoListJsonString = gson.toJson(profileUserVideoList)
            var bundle = bundleOf("navigationFlagProfile" to "ProfilePublic" , "videoList" to userInfoListJsonString ,"position" to position, "isVideoCurrentPage" to currentPage+1)
            findNavController().navigate(R.id.action_profileUserFragment_to_UserVideoFragment, bundle)
        }
    }
}