package com.example.sare.profile.fragment

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import android.widget.RelativeLayout
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.dashboard.UserVideosActivity
import com.example.sare.extensions.*
import com.example.sare.notification.NotificationListActivity
import com.example.sare.profile.activity.FollowersFollowingActivity
import com.example.sare.profile.adapter.BookmarkedVideoGridAdapter
import com.example.sare.profile.adapter.DraftVideoGridAdapter
import com.example.sare.profile.adapter.PublishedVideoGridAdapter
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.profile.viewmodel.ProfileUserViewModel
import com.example.sare.profile.viewmodel.UserVideoListViewModel
import com.example.sare.signUpDetails.adapter.ColorSelectionAdapter
import com.example.sare.signUpDetails.viewmodel.ColorListViewModel
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.fragment_set_your_profile.view.*
import kotlinx.android.synthetic.main.layout_my_profile_scroll_new.*
import kotlinx.android.synthetic.main.layout_my_profile_scroll_new.view.*
import kotlinx.android.synthetic.main.layout_my_profile_scroll_new.view.imgProfilePic
import kotlinx.android.synthetic.main.layout_select_color.view.*
import kotlinx.android.synthetic.main.layout_select_image_options.view.*
import kotlinx.android.synthetic.main.layout_select_profile_change.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.net.URL
import java.sql.Timestamp
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@RequiresApi(Build.VERSION_CODES.M) class ProfileUserFragment : Fragment(), ItemOnclickListener {

    var root: View? = null
    var timestamp = Timestamp(System.currentTimeMillis())

    var videoListViewModel: UserVideoListViewModel? = null
    var profileUserViewModel: ProfileUserViewModel? = null
    private var CAMERA_REQUEST = 0
    private val MY_CAMERA_PERMISSION_CODE = 100
    private var IMAGE_PICK_CODE = 0
    private val PERMISSION_CODE = 1001
    private val UPLOAD_FILE = 1002
    var pos: Int = 0
    var userId: String? = ""
    var token: String? = ""
    var image: String? = ""
    var coverIimage: String? = ""
    var selectedColorCode: String? = ""
    var colorCode: String? = ""
    var isPosted: String? = ""
    var type: String? = ""
    var uri: Uri? = null
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var homePagePasscurrentPage: Int = 0
    var isLoadingBookmark = false
    var isLastPageBookMark = false
    private var currentPageBookmark: Int = 0
    var isLoadingDraft = false
    var isLastPageDraft = false
    private var currentPageDraft: Int = 0
    private var publishedArrayList: ArrayList<UserVideosQuery.UserVideo> = ArrayList()
    private var draftArrayList: MutableList<UserVideosQuery.UserVideo> = mutableListOf()
    private var bookmarkArrayList: MutableList<UserVideosQuery.UserVideo> = mutableListOf()
    lateinit var bookmarkAdapter: BookmarkedVideoGridAdapter
    lateinit var publishedVideoGridAdapter: PublishedVideoGridAdapter
    lateinit var draftVideoGridAdapter: DraftVideoGridAdapter
    lateinit var bottomSheetBehavior: BottomSheetBehavior<*>
    lateinit var bottomSheetBehaviorChange: BottomSheetBehavior<*>
    lateinit var userDataModel: UserDetailsQuery.User

    private var profileUserVideoList: MutableList<VideoListQuery.Video> = mutableListOf()
    var apiCalled: Boolean = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_profile_user, container, false)
        getSharedPreferences()
        type = arguments?.get("type").toString()
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                activity?.finishAndRemoveTask()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback)
        setTheme()

        setBottomSheet()
        setBottomSheetChangeProfilePic()

        getUserDetails(userId.toString())
        videoListViewModel = ViewModelProvider(requireActivity())[UserVideoListViewModel::class.java]
        profileUserViewModel = ViewModelProvider(requireActivity())[ProfileUserViewModel::class.java]
        (root?.recyclerView?.itemAnimator as SimpleItemAnimator).setSupportsChangeAnimations(false)
        initPublishedRecyclerView()
        publishedVideoListObserver()
        bookmarkObserver()
        draftObserver()
        getPublishedVideoList()
        /*    getBookmarkVideoList()
            getDraftVideoList()*/
        //        initBookMarkRecyclerView()
        /*        getDraftVideoList()
                getBookmarkVideoList()*/
        setListeners()

        collapsing()

        return root
    }

    private fun collapsing() {
        val mAppBarLayout = root!!.findViewById(R.id.AppFragment_AppBarLayout) as AppBarLayout
        val mToolbar = root!!.findViewById(R.id.AppFragment_Toolbar) as Toolbar
        mAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            mToolbar.alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()

            val alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()
            Log.d("tag", "alpha::::::$alpha")

            if (alpha == 1f) {

                mToolbar!!.visible()
                root?.btnUpScroll?.visible()
                root?.imgSet1?.isClickable = true
                root?.imgBack?.isClickable = true
                root?.ll_back?.isClickable = false
                root?.imgProfileEdit?.isClickable = false
            } else if (alpha > 0.4f) {
                mToolbar!!.visible()
                root?.btnUpScroll!!.visible()
                root?.imgSet1?.isClickable = true
                root?.imgBack?.isClickable = true
                root?.ll_back?.isClickable = false
                root?.imgProfileEdit?.isClickable = false
            } else if (alpha == 0f) {

                root?.btnUpScroll!!.gone()
                mToolbar!!.gone()
                root?.imgSet1?.isClickable = false
                root?.imgBack?.isClickable = false
                root?.ll_back?.isClickable = true
                root?.imgProfileEdit?.isClickable = true
            }

        })


    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.transparent)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.transparent)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    fun bookmarkObserver() {

        videoListViewModel?.bookmarkArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getBookMarkVideos(
                                requireContext(), userId.toString(), currentPage * Utils.PAGE_SIZE)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.userVideoBookMarkList -> {
                    apiCalled = true
                    isLoadingBookmark = false
                    if (it.response != null) {

                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(it.response)
                        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                        val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(
                            userInfoListJsonString, myType)
                        profileUserVideoList.addAll(userVideoList)

                        bookmarkArrayList.addAll(it.response)
                        root?.recyclerView?.adapter?.notifyDataSetChanged()
                        if (it.response.size < Utils.PAGE_SIZE) {
                            isLastPageBookMark = true
                        }
                        if (root?.recyclerView?.adapter?.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.txtNoData?.setImageResource(R.drawable.ic_no_saved)
                        } else {
                            root?.txtNoData?.visibility = View.GONE
                        }

                    } else {
                        activity?.runOnUiThread {
                            customToast(getString(R.string.api_error_message), requireActivity(), 0)

                        }
                    }
                }

            }
        })
    }

    fun removeBookmarkVideo(id: String, pos: Int) {

        videoListViewModel?.removeBookMark(requireContext(), id, false)

        videoListViewModel?.bookmarkVideo?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = View.VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.removeBookMark(requireContext(), id, false)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.bookmarkVideo -> {
                    if (it.response.status()) {
                        activity?.runOnUiThread {
                            recyclerView.adapter?.notifyItemRemoved(pos)
                            recyclerView.adapter?.notifyDataSetChanged()

                            if (bookmarkArrayList.size == 0) {
                                root?.txtNoData?.visibility = View.VISIBLE
                                root?.txtNoData?.setImageResource(R.drawable.ic_no_saved)
                            } else {
                                root?.txtNoData?.visibility = View.GONE
                            }
                            customToast("Video removed from bookmark list.!", requireActivity(), 1)
                        }
                    }

                }

            }

        })


    }

    fun removeVideo(id: String, type: Int, pos: Int) {
        videoListViewModel?.removeVideo(requireContext(), id)

        videoListViewModel?.removeVideo?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    root?.main_layout?.visibility = View.VISIBLE
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.removeVideo(requireContext(), id)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.removeVideo -> {
                    if (type == 1) {
                        root?.txtPostCount?.text = (root?.txtPostCount?.text?.toString()?.toInt()?.minus(1)).toString()
                        recyclerView.adapter?.notifyItemRemoved(pos)
                        recyclerView.adapter?.notifyDataSetChanged()
                        if (publishedArrayList.size == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.txtNoData?.setImageResource(R.drawable.ic_no_video)
                        } else {
                            root?.txtNoData?.visibility = View.GONE

                        }
                        activity?.runOnUiThread {
                            customToast("Video removed from the list.!", requireActivity(), 1)
                        }
                    } else {
                        recyclerView.adapter?.notifyItemRemoved(pos)
                        recyclerView.adapter?.notifyDataSetChanged()

                        if (draftArrayList.size == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.txtNoData?.setImageResource(R.drawable.ic_no_draft)

                        } else {
                            root?.txtNoData?.visibility = View.GONE

                        }
                        activity?.runOnUiThread {

                            customToast("Video removed from the list.!", requireActivity(), 1)
                        }
                    }


                }

            }

        })
    }

    private fun getBookmarkVideoList() {
        homePagePasscurrentPage = currentPageBookmark

        videoListViewModel?.getBookMarkVideos(
            requireContext(), userId.toString(), currentPageBookmark * Utils.PAGE_SIZE)
    }


    fun draftObserver() {
        videoListViewModel?.draftArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getDraftVideos(
                                requireContext(), userId.toString(), currentPageDraft * Utils.PAGE_SIZE)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }
                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.userVideoDraftList -> {
                    isLoadingDraft = false
                    Log.d("tag", "response::" + it.response)
                    if (it.response != null) {
                        apiCalled = true
                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(it.response)
                        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                        val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(
                            userInfoListJsonString, myType)

                        profileUserVideoList.addAll(userVideoList)

                        draftArrayList.addAll(it.response)
                        root?.recyclerView?.adapter?.notifyDataSetChanged()
                        if (it.response.size < Utils.PAGE_SIZE) {
                            isLastPageDraft = true
                        }
                        if (root?.recyclerView?.adapter?.itemCount == 0) {
                            root?.txtNoData?.visibility = View.VISIBLE
                            root?.txtNoData?.setImageResource(R.drawable.ic_no_draft)
                        } else {
                            root?.txtNoData?.visibility = View.GONE
                        }

                    } else {
                        activity?.runOnUiThread {
                            customToast(getString(R.string.api_error_message), requireActivity(), 0)

                        }
                    }

                }

            }

        })
    }

    private fun getDraftVideoList() {
        homePagePasscurrentPage = currentPageDraft
        videoListViewModel?.getDraftVideos(
            requireContext(), userId.toString(), currentPageDraft * Utils.PAGE_SIZE)


    }

    private fun getPublishedVideoList() {
        Log.d("tag", "Pagination Current page : " + currentPage)

        homePagePasscurrentPage = currentPage

        videoListViewModel?.getUserVideoList(
            requireContext(), userId.toString(), currentPage * PAGE_SIZE)
    }

    private fun publishedVideoListObserver() {

        videoListViewModel?.videoArrayList?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getUserVideoList(
                                requireContext(), userId.toString(), currentPage * Utils.PAGE_SIZE)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.userVideoPublishedList -> {
                    activity?.runOnUiThread {
                        apiCalled = true
                        Log.d("tag", "response published::" + it.response)
                        isLoading = false
                        if (it.response != null) {
                            val gson = Gson()
                            val userInfoListJsonString = gson.toJson(it.response)
                            val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                            val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(
                                userInfoListJsonString, myType)

                            profileUserVideoList.addAll(userVideoList)
                            publishedArrayList.addAll(it.response)
                            root?.recyclerView?.recycledViewPool?.clear()
                            root?.recyclerView?.adapter?.notifyDataSetChanged()
                            if (it.response.size < Utils.PAGE_SIZE) {
                                isLastPage = true
                            }
                            if (root?.recyclerView?.adapter?.itemCount == 0) {
                                root?.txtNoData?.visibility = View.VISIBLE
                                root?.txtNoData?.setImageResource(R.drawable.ic_no_video)
                            } else {
                                root?.txtNoData?.visibility = View.GONE
                            }

                        } else {
                            activity?.runOnUiThread {
                                customToast(getString(R.string.api_error_message), requireActivity(), 0)
                            }
                        }
                    }
                }

            }

        })
    }

    override fun onResume() {
        super.onResume()
        getUserDetails(userId.toString())
        publishedArrayList.clear()
        isLoading = false
        isLastPage = false
        currentPage = 0
        homePagePasscurrentPage = 0
    }

    private fun setListeners() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        root?.btnUpScroll?.setOnClickListener {
            root?.btnUpScroll?.gone()
            root?.recyclerView?.smoothScrollToPosition(0)
            root?.AppFragment_AppBarLayout!!.setExpanded(true, true)
        }
        root?.imgBack?.setOnClickListener {
            Log.d("tag", "clicked::: back111")

            preferences.edit().remove("isPosted").apply()

            NavHostFragment.findNavController(this).navigate(R.id.homeFragment2)
        }
        root?.imgSet1?.setOnClickListener {
            preferences.edit().remove("isPosted").apply()
            //            findNavController().navigate(R.id.action_profileUserFragment_to_settingsFragment)
            //                requireActivity().startActivity(Intent(requireActivity(), SettingsActivity::class.java))
            activity?.openActivity(MainActivity::class.java)
        }
        root?.ll_back?.isClickable = true
        root?.ll_back?.setOnClickListener {

            preferences.edit().remove("isPosted").apply()

            Log.d("tag", "clicked::: back")

        }


        root?.imgPublishedVideos?.setOnClickListener {
            homePagePasscurrentPage = 0
            profileUserVideoList.clear()
            publishedArrayList.clear()
            isLoading = false
            isLastPage = false
            currentPage = 0
            preferences.edit().remove("isPosted").apply()

            root?.imgPublishedVideos?.setBackgroundResource(R.drawable.button_tab_background)
            root?.imgPublishedVideos?.alpha = 1f
            root?.imgBookmarkVideos?.setBackgroundResource(0)
            root?.imgBookmarkVideos?.alpha = 1f

            if (pos == 4 || pos == 2) {
                pos = 5

                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val leftSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_left)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)

                val set = AnimationSet(true)

                set.duration = 200
                // set.addAnimation(anim)
                set.addAnimation(leftSwipe)
                //  set.addAnimation(fadeIn)
                set.fillAfter = true

                set.interpolator = LinearInterpolator()

                root?.imgPublishedVideos?.startAnimation(set)
            } else if (pos == 6) {
                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)

                val set = AnimationSet(true)

                set.duration = 200
                //   set.addAnimation(anim)
                //  set.addAnimation(fadeIn)
                set.fillAfter = true

                set.interpolator = LinearInterpolator()

                root?.imgPublishedVideos?.startAnimation(set)
            } else {
                pos = 1

                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_right)
                val leftSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_left)
                val set = AnimationSet(true)

                set.duration = 200
                //  set.addAnimation(anim)
                set.addAnimation(rightSwipe)
                set.addAnimation(leftSwipe)
                set.fillAfter = true

                set.interpolator = LinearInterpolator()
                root?.imgPublishedVideos?.startAnimation(set)
            }

            root?.imgDraftVideos?.setBackgroundResource(0)
            root?.imgDraftVideos?.alpha = 1f

            root?.imgBookmarkVideos?.setImageResource(R.drawable.ic_profile_save_selected)
            root?.imgDraftVideos?.setImageResource(R.drawable.ic_profile_draft)

            root?.imgPublishedVideos?.setImageResource(R.drawable.ic_profile_video_selected)
            root?.imgPublishedVideos?.setColorFilter(
                Color.parseColor("#FF005F"), PorterDuff.Mode.SRC_IN)
            root?.imgDraftVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)
            root?.imgBookmarkVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)
            initPublishedRecyclerView()
            getPublishedVideoList()
        }

        root?.imgBookmarkVideos?.setOnClickListener {
            homePagePasscurrentPage = 0
            profileUserVideoList.clear()
            bookmarkArrayList.clear()
            isLoadingBookmark = false
            isLastPageBookMark = false
            currentPageBookmark = 0
            preferences.edit().remove("isPosted").apply()

            root?.imgPublishedVideos?.setBackgroundResource(0)
            root?.imgPublishedVideos?.alpha = 1f
            root?.imgBookmarkVideos?.setBackgroundResource(R.drawable.button_tab_background)
            root?.imgBookmarkVideos?.alpha = 1f
            root?.imgDraftVideos?.setBackgroundResource(0)
            root?.imgDraftVideos?.alpha = 1f
            root?.txtNoData?.visibility = View.GONE
            root?.imgPublishedVideos?.setImageResource(R.drawable.ic_profile_video)
            root?.imgDraftVideos?.setImageResource(R.drawable.ic_profile_draft)
            root?.imgBookmarkVideos?.setImageResource(R.drawable.ic_profile_save)
            root?.imgBookmarkVideos?.setColorFilter(
                Color.parseColor("#FF005F"), PorterDuff.Mode.SRC_IN)
            root?.imgDraftVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)
            root?.imgPublishedVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)

            Log.d("tag", "pos:: $pos")
            if (pos == 3 || pos == 6) {

                pos = 4
                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val leftSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_left)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                val set = AnimationSet(true)

                set.duration = 200
                set.fillAfter = true
                set.addAnimation(anim)
                set.addAnimation(leftSwipe)
                //set.addAnimation(fadeIn)
                set.interpolator = LinearInterpolator()
                root?.imgBookmarkVideos?.startAnimation(set)
            } else if (pos == 1 || pos == 5 || pos == 0) {
                pos = 2

                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_right)
                val leftSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_left)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                val slideBottom = AnimationUtils.loadAnimation(
                    requireContext(), R.anim.slide_in_bottom)

                val set = AnimationSet(true)
                set.duration = 200
                set.fillAfter = true
                //  set.addAnimation(fadeIn)
                set.addAnimation(rightSwipe)
                set.interpolator = LinearInterpolator()
                root?.imgBookmarkVideos?.startAnimation(set)
            }
            initBookMarkRecyclerView()
            getBookmarkVideoList()
        }

        root?.imgDraftVideos?.setOnClickListener {
            homePagePasscurrentPage = 0
            profileUserVideoList.clear()
            draftArrayList.clear()
            isLoadingDraft = false
            isLastPageDraft = false
            currentPageDraft = 0
            preferences.edit().remove("isPosted").apply()
            Log.d("tag", "pos:: imgDraftVideos$pos")
            root?.imgDraftVideos?.setBackgroundResource(R.drawable.button_tab_background)
            root?.imgDraftVideos?.alpha = 1f
            root?.imgPublishedVideos?.setBackgroundResource(0)
            root?.imgPublishedVideos?.alpha = 1f
            root?.imgDraftVideos?.setColorFilter(
                Color.parseColor("#FF005F"), PorterDuff.Mode.SRC_IN)
            root?.imgBookmarkVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)
            root?.imgPublishedVideos?.setColorFilter(
                Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN)

            if (pos == 5 || pos == 1 || pos == 0 || pos == 3 || pos == 6) {
                pos = 6
                var anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                val set = AnimationSet(true)
                set.duration = 200
                set.addAnimation(anim)
                // set.addAnimation(fadeIn)
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                root?.imgDraftVideos?.startAnimation(set)
            } else {
                pos = 3
                val anim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(requireContext(), R.anim.anim_right)
                val fadeIn = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_in)
                val set = AnimationSet(true)
                set.duration = 200
                set.addAnimation(anim)
                //set.addAnimation(fadeIn)
                set.addAnimation(rightSwipe)
                set.fillAfter = true

                set.interpolator = LinearInterpolator()

                root?.imgDraftVideos?.startAnimation(set)
            }

            root?.imgBookmarkVideos?.setBackgroundResource(0)
            root?.imgBookmarkVideos?.alpha = 1f
            root?.txtNoData?.visibility = View.GONE
            initDraftRecyclerView()

            getDraftVideoList()

            root?.imgDraftVideos?.setImageResource(R.drawable.ic_profile_draft_selected)
            root?.imgBookmarkVideos?.setImageResource(R.drawable.ic_profile_save_selected)
            root?.imgPublishedVideos?.setImageResource(R.drawable.ic_profile_video)

        }

        root?.button_notification?.setOnClickListener {
            preferences.edit().remove("isPosted").apply()
            activity?.openActivity(NotificationListActivity::class.java)
            activity?.overridePendingTransition(0, 0)
        }

        root?.imgSettings?.setOnClickListener {
            preferences.edit().remove("isPosted").apply()
            activity?.openActivity(MainActivity::class.java)
            activity?.overridePendingTransition(0, 0)
        }

        root?.btnUpScroll?.setOnClickListener {
            root?.btnUpScroll?.gone()
            root?.AppFragment_AppBarLayout!!.setExpanded(true, true)
        }

    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        isPosted = preferences.getString("isPosted", null)
        Log.d("tag", "token user::$token")
        val user = preferences.getString("USER", null)
        Log.d("tag", "user::$user")

        if (user != null) {
            val mainObject = JSONObject(user)
            if (mainObject.has("image")) {

                image = mainObject.getString("image")
            }
            if (mainObject.has("coverImage")) {
                coverIimage = mainObject.getString("coverImage")
            }
            colorCode = mainObject.getString("colorCode")
            image = PUBLIC_URL + (image.toString())
            userId = mainObject.getString("_id")
        }

        setData(image, colorCode)
        Log.d("tag", "token::$token")
        Log.d("tag", "token::$token")
        Log.d("tag", "coverImage::$coverIimage")
    }


    private fun setData(url: String?, colorCode: String?) {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)
        root?.imgProfilePic?.loadUrl(url!!)
        root?.imgProfilePicUser?.setBorder(colorCode ?: Utils.COLOR_CODE)

        root?.imgProfile?.loadUrl(url!!)
        root?.imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

    }

    private fun initPublishedRecyclerView() {

        publishedVideoGridAdapter = PublishedVideoGridAdapter(
            publishedArrayList as ArrayList<UserVideosQuery.UserVideo>,
            requireContext(),
            this@ProfileUserFragment,
            root?.recyclerView!!,
            this)

        root?.recyclerView?.apply {
            layoutManager = GridLayoutManager(
                root?.recyclerView?.context, 3)


            addOnScrollListener(object : PaginationListener(this.layoutManager as GridLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getPublishedVideoList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }


            })

            Log.d("tag", "publishedArrayList:::::size:::::${publishedArrayList.size}")


            adapter = PublishedVideoGridAdapter(
                publishedArrayList as ArrayList<UserVideosQuery.UserVideo>,
                requireContext(),
                this@ProfileUserFragment,
                root?.recyclerView!!,
                this@ProfileUserFragment)

        }
    }

    private fun initBookMarkRecyclerView() {
        bookmarkAdapter = BookmarkedVideoGridAdapter(
            bookmarkArrayList as ArrayList<UserVideosQuery.UserVideo>,
            requireContext(),
            this@ProfileUserFragment,
            this@ProfileUserFragment,
            root?.recyclerView!!)
        root?.recyclerView?.apply {
            layoutManager = GridLayoutManager(
                root?.recyclerView?.context, 3)

            addOnScrollListener(object : PaginationListener(this.layoutManager as GridLayoutManager) {
                override fun loadMoreItems() {
                    isLoadingBookmark = true
                    currentPageBookmark++
                    getBookmarkVideoList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPageBookmark)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPageBookMark)
                    return isLastPageBookMark
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoadingBookmark)
                    return isLoadingBookmark
                }


            })

            adapter = BookmarkedVideoGridAdapter(
                bookmarkArrayList as ArrayList<UserVideosQuery.UserVideo>,
                requireContext(),
                this@ProfileUserFragment,
                this@ProfileUserFragment,
                root?.recyclerView!!)

        }

    }

    private fun initDraftRecyclerView() {
        draftVideoGridAdapter = DraftVideoGridAdapter(
            draftArrayList as ArrayList<UserVideosQuery.UserVideo>,
            requireContext(),
            this@ProfileUserFragment,
            root?.recyclerView!!)
        root?.recyclerView?.apply {
            layoutManager = GridLayoutManager(
                root?.recyclerView?.context, 3)

            addOnScrollListener(object : PaginationListener(this.layoutManager as GridLayoutManager) {
                override fun loadMoreItems() {
                    isLoadingDraft = true
                    currentPageDraft++
                    getDraftVideoList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPageDraft
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoadingDraft
                }


            })

            adapter = DraftVideoGridAdapter(
                draftArrayList as ArrayList<UserVideosQuery.UserVideo>,
                context,
                this@ProfileUserFragment,
                root?.recyclerView!!)

        }
    }

    private fun getUserDetails(id: String) {
        val progressDialog = CustomProgressDialogNew(requireContext())
        activity?.runOnUiThread {
            progressDialog.show()
        }
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(id)).enqueue(object : ApolloCall.Callback<UserDetailsQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                e.printStackTrace()
                activity?.runOnUiThread {

                    Log.d("tag", "Error::" + e.message)
                    customToast(e.message.toString(), requireActivity(), 0)
                    MyApplication.mainActivity.logoutUser()
                    activity?.openNewActivity(LoginActivity::class.java)
                }
            }

            override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                activity?.runOnUiThread {
                    progressDialog.hide()
                }
                Log.d("tag", "ResponseData::$response")

                activity?.runOnUiThread {
                    if (response.data()?.user() != null) {
                        userDataModel = response.data!!.user()
                        setData(response)
                    } else {
                        val error = response.errors()?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            callLoginFragment(requireContext(), error.toString())
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            callLoginFragment(requireContext(), error.toString())
                        } else {
                            customToast(error.toString(), requireActivity(), 0)
                        }
                    }

                }
            }
        })
    }

    private fun setData(response: Response<UserDetailsQuery.Data>) {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val imgProfilePicChange = root?.imgProfilePicChange
        Glide.with(requireContext()).load(PUBLIC_URL + response.data?.user()?.image())
            .error(R.drawable.ic_profile_icon_1).into(imgProfilePicChange!!)
        root?.imgProfileBorderChange?.setBorder(colorCode ?: Utils.COLOR_CODE)
        root?.imgFrameColorChange?.setBorder(response.data?.user()?.colorCode().toString())

        val imageView = root?.header_cover_image
        Glide.with(requireContext()).load(PUBLIC_URL + (response.data?.user()?.coverImage().toString()))
            .error(R.drawable.cover_default).placeholder(R.drawable.cover_default).into(imageView!!)


        root?.relative_layout_followers?.setOnClickListener {
            preferences.edit().remove("isPosted").apply()
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWERS")

            activity?.openActivity(FollowersFollowingActivity::class.java, bundle)
            activity?.overridePendingTransition(0, 0)
        }
        root?.relative_layout_followings?.setOnClickListener {
            preferences.edit().remove("isPosted").apply()

            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWINGS")
            activity?.openActivity(FollowersFollowingActivity::class.java, bundle)
            activity?.overridePendingTransition(0, 0)
        }
        root?.txtName?.text = response.data!!.user().name().toString()
        root?.txtName1?.text = response.data!!.user().name().toString()

        root?.txtId?.text = "@" + response.data!!.user().username().toString()
        if (response.data!!.user().likeCounts().toString() < 0.toString()) {
            root?.txtLikes?.text = 0.toString() + " Like"
        } else {
            root?.txtLikes?.text = response.data!!.user().likeCounts().toString() + " Likes"
        }

        if (response.data!!.user().followingCounts()!! > 0) {
            root?.txtFollowingCountUser?.text = response.data!!.user().followingCounts().toString()
        } else {
            root?.txtFollowingCountUser?.text = "0"
        }
        if (response.data!!.user().postsCounts()!! > 0) {
            root?.txtPostCount?.text = response.data!!.user().postsCounts().toString()
        } else {
            root?.txtPostCount?.text = "0"
        }

        if (response.data!!.user().followerCounts()!! > 0) {
            root?.txtFollowersCount?.text = response.data!!.user().followerCounts().toString()
        } else {
            root?.txtFollowersCount?.text = "0"

        }
        root?.txtJoinedDate?.text = "Joined On " + getParsedDate(response.data!!.user().createdAt().toString())
        if (!response.data!!.user().bio().isNullOrEmpty()) {
            root?.quote1?.visibility = View.VISIBLE

            root?.quote1?.text = response.data!!.user().bio().toString()
        } else {
            root?.quote1?.visibility = View.GONE
        }
    }

    private fun getParsedDate(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        var parsedDate: String? = null
        parsedDate = try {
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("MMMM yyyy")
            outputFormatter1.format(date1)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        return parsedDate.toString()
    }


    private fun setBottomSheet() {
        val llBottomSheet = root?.bottom_sheet_new

        /*val slideUp = AnimationUtils.loadAnimation(requireContext(), R.anim.slide_up_dialog)
        llBottomSheet?.startAnimation(slideUp)*/

        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)
        root?.imgProfileEdit?.isClickable = true

        root?.imgProfileEdit?.setOnClickListener {
            IMAGE_PICK_CODE = 200
            CAMERA_REQUEST = 1888

            root?.overlay_view_profile_user?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.overlay_view_profile_user?.setOnClickListener {
            root?.overlay_view_profile_user?.visibility = View.GONE
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            Log.d("tag", "setOnClickListener called!!!!")
        }


        root?.txtTakePhoto?.setOnClickListener {
            root?.overlay_view_profile_user?.visibility = View.GONE
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                    Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
            ) {
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA)
                //show popup to request runtime permission
                requestPermissions(permissions, MY_CAMERA_PERMISSION_CODE)
            } else {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            }
        }

        root?.txtChooseFromGallery?.setOnClickListener {
            root?.overlay_view_profile_user?.visibility = View.GONE
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                ) {
                    //permission denied
                    val permissions = arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA

                    )
                    //show popup to request runtime permission
                    requestPermissions(permissions, PERMISSION_CODE)
                } else {
                    //permission already granted
                    pickImageFromGallery()
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery()
            }
        }


        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_profile_user?.visibility = View.GONE
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_profile_user?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_profile_user?.visibility = View.GONE
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_profile_user?.visibility = View.GONE
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }
        })

    }

    private fun setBottomSheetChangeProfilePic() {
        val bottom_sheet_select_profile = root?.bottom_sheet_select_profile

        bottomSheetBehaviorChange = BottomSheetBehavior.from(bottom_sheet_select_profile as RelativeLayout)

        bottom_sheet_select_profile?.setOnClickListener {
            root?.overlay_view?.visibility = View.VISIBLE
            bottomSheetBehaviorChange.state = BottomSheetBehavior.STATE_EXPANDED
        }

        root?.imgProfilePicChange?.loadUrl(image.toString())
        root?.imgProfileBorderChange?.setBorder(colorCode ?: Utils.COLOR_CODE)
        root?.imgFrameColorChange?.setBorder(colorCode ?: Utils.COLOR_CODE)

        root?.imgProfilePic?.setOnClickListener {
            root?.overlay_view_profile_change_pic?.visibility = View.VISIBLE
            if (bottomSheetBehaviorChange.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        bottom_sheet_select_profile.frameColorLogoChange?.setOnClickListener {
            getColorList()
        }
        bottom_sheet_select_profile.imgProfilePicChange?.setOnClickListener {
            IMAGE_PICK_CODE = 300
            CAMERA_REQUEST = 1777
            root?.overlay_view_profile_change_pic?.visibility = View.GONE
            bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
            root?.overlay_view_profile_user?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.overlay_view_profile_change_pic?.setOnClickListener {

            root?.overlay_view_profile_change_pic?.visibility = View.GONE
            bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehaviorChange.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_profile_change_pic?.visibility = View.GONE
                        bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_profile_change_pic?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_profile_change_pic?.visibility = View.GONE
                        bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_profile_change_pic?.visibility = View.GONE
                        bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun pickImageFromGallery() {
        //Intent to pick image
        val intent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_PICK_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d("tag", "requestCode:::$requestCode")
        if (requestCode === MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, CAMERA_REQUEST)
            } else {
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        } else if (requestCode === PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery()
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        } else if (requestCode === UPLOAD_FILE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                try {
                    val url = URL(image)
                    val alreadySelectedBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                    val bytes = ByteArrayOutputStream()
                    alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                    val path: String = MediaStore.Images.Media.insertImage(
                        requireActivity().contentResolver,
                        alreadySelectedBitmap,
                        "IMG_" + Calendar.getInstance().time,
                        null)
                    uri = Uri.parse(path)
                } catch (e: Exception) {
                    Log.d("tag", "error:::${e.message}")

                    e.printStackTrace()
                    val alreadySelectedBitmap = BitmapFactory.decodeResource(resources, R.drawable.ic_login_profile)
                    val bytes = ByteArrayOutputStream()
                    alreadySelectedBitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

                    val path: String = MediaStore.Images.Media.insertImage(
                        requireActivity().contentResolver,
                        alreadySelectedBitmap,
                        "IMG_" + Calendar.getInstance().time,
                        null)
                    uri = Uri.parse(path)

                }
                uploadProfile(uri!!)
            } else {
                //permission from popup denied
                customToast(getString(R.string.permission_denied), requireActivity(), 0)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && data != null && data.data != null && requestCode == IMAGE_PICK_CODE) {
            try {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                val cursor: Cursor = selectedImage?.let {
                    requireContext().contentResolver.query(
                        it, filePathColumn, null, null, null)
                }!!
                cursor.moveToFirst()
                val columnIndex: Int = cursor.getColumnIndex(filePathColumn[0])
                val picturePath: String = cursor.getString(columnIndex)
                cursor.close()
                data.putExtra("picturePath", picturePath)
                requireActivity().setResult(Activity.RESULT_OK, data)

            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error::${e.message}")
                val returnFromGalleryIntent = Intent()
                requireActivity().setResult(Activity.RESULT_CANCELED, returnFromGalleryIntent)
                requireActivity().finish()
            }
        }

        if (resultCode == Activity.RESULT_OK && requestCode === CAMERA_REQUEST) {
            val picturePath: Bitmap = data!!.extras!!["data"] as Bitmap
            val filesDir: File = requireContext().filesDir

            val imageFile = File(filesDir, timestamp.time.toString() + ".jpg")
            println("imageFile:::$imageFile")

            val os: OutputStream
            try {
                os = FileOutputStream(imageFile)
                picturePath.compress(Bitmap.CompressFormat.JPEG, 90, os)
                os.flush()
                os.close()
            } catch (e: java.lang.Exception) {
                Log.e(javaClass.simpleName, "Error writing bitmap", e)
            }
            Log.d("tag", "picturePath Camera::$picturePath")
            imageCrop(imageFile)

        }

        if (requestCode === IMAGE_PICK_CODE) {
            if (resultCode === Activity.RESULT_OK) {
                val picturePath = data!!.getStringExtra("picturePath")
                //perform Crop on the Image Selected from Gallery
                Log.d("tag", "picturePath::$picturePath")
                performCrop(picturePath.toString())

            }
        }

        if (requestCode === CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            if (resultCode === Activity.RESULT_OK) {
                val path: String = MediaStore.Images.Media.insertImage(
                    requireActivity().contentResolver, result.uri.path, "IMG_" + Calendar.getInstance().time, null)

                uri = Uri.parse(path)

                if (uri == null) {
                    customToast("Please select picture to upload.!", requireActivity(), 0)
                } else {
                    selectedColorCode = colorCode

                    if (IMAGE_PICK_CODE == 200 || CAMERA_REQUEST == 1888) {

                        val imageView = root?.header_cover_image
                        Glide.with(requireContext()).load(uri).placeholder(R.drawable.cover_default)
                            .error(R.drawable.cover_default).into(imageView!!)

                        uploadFile(uri!!)
                    } else {

                        if (uri != null) {
                            val imageView = root?.imgProfilePic
                            Glide.with(requireContext()).load(uri).error(R.drawable.ic_login_profile).into(imageView!!)
                            val imageViewProfile = root?.imgProfilePicChange
                            Glide.with(requireContext()).load(uri).error(R.drawable.ic_login_profile)
                                .into(imageViewProfile!!)

                            uploadProfile(uri!!)
                        } else {
                            try {
                                val url = URL(image)
                                val alreadySelectedBitmap = BitmapFactory.decodeStream(
                                    url.openConnection().getInputStream())
                                val bytes = ByteArrayOutputStream()
                                alreadySelectedBitmap?.compress(
                                    Bitmap.CompressFormat.JPEG, 100, bytes)

                                val path: String = MediaStore.Images.Media.insertImage(
                                    requireActivity().contentResolver,
                                    alreadySelectedBitmap,
                                    "IMG_" + Calendar.getInstance().time,
                                    null)
                                uri = Uri.parse(path)


                            } catch (e: Exception) {
                                Log.d("tag", "error:::${e.message}")

                                e.printStackTrace()
                                val alreadySelectedBitmap = BitmapFactory.decodeResource(
                                    resources, R.drawable.ic_login_profile)
                                val bytes = ByteArrayOutputStream()
                                alreadySelectedBitmap?.compress(
                                    Bitmap.CompressFormat.JPEG, 100, bytes)

                                val path: String = MediaStore.Images.Media.insertImage(
                                    requireActivity().contentResolver,
                                    alreadySelectedBitmap,
                                    "IMG_" + Calendar.getInstance().time,
                                    null)
                                uri = Uri.parse(path)

                            }
                            uploadProfile(uri!!)

                        }
                    }
                }

            } else if (resultCode === CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                val error = result.error
                Log.d("tag", "error::::$error")
                activity?.runOnUiThread {
                    customToast("Something went wrong.!", requireActivity(), 1)
                }

            }
        }

    }

    private fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = requireActivity().contentResolver.query(uri!!, projection, null, null, null) ?: return null
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }


    private fun uploadFile(fileUri: Uri) {
        val file = File(getPath(fileUri))

        val requestFile: RequestBody = file.asRequestBody(
            requireActivity().contentResolver.getType(fileUri)!!.toMediaTypeOrNull())

        val body = MultipartBody.Part.createFormData("coverImage", file.name, requestFile)

        Log.d("tag", "body::$body")

        val progressDialog = CustomProgressDialogNew(requireContext())
        activity?.runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::::User profile fragment::$token")
        profileUserViewModel?.uploadCoverImage(
            body, token.toString())

        profileUserViewModel?.status?.observe(viewLifecycleOwner, Observer {
            activity?.runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            profileUserViewModel?.uploadCoverImage(
                                body, "Bereader ${token.toString()}")
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg.toString(), requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.uploadImage -> {
                    activity?.runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)
                    if (it.response.status == 200) {
                        customToast(
                            getString(R.string.upload_success_message), requireActivity(), 1)
                        val dataUserDetailObject = UserDetailsQuery.User(
                            userDataModel.__typename(),
                            userDataModel._id(),
                            userDataModel.name(),
                            userDataModel.mobile(),
                            userDataModel.followerCounts(),
                            userDataModel.followingCounts(),
                            userDataModel.username(),
                            userDataModel.createdAt(),
                            userDataModel.likeCounts(),
                            userDataModel.bio(),
                            userDataModel.image(),
                            it.response.path,
                            userDataModel.dob(),
                            userDataModel.email(),
                            userDataModel.gender(),
                            userDataModel.followed(),
                            userDataModel.followedBack(),
                            userDataModel.postsCounts(),
                            userDataModel.privacyStatus(),
                            selectedColorCode,
                            userDataModel.authentication(),
                            userDataModel.agreementAccepted(),
                            userDataModel.status(),
                            userDataModel.blocked(),
                            userDataModel.state(),
                            userDataModel.city(),
                            userDataModel.location(),
                            userDataModel.verify_user(),
                            userDataModel.downloadCounts())
                        Log.d("tag", "dataUserDetailObject:::$dataUserDetailObject")
                        //                        getUserDetails(userId.toString())
                        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
                            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                        val gson = Gson()

                        // Get java object list json format string.
                        val userInfoListJsonString = gson.toJson(dataUserDetailObject)

                        preferences.edit().putString("TOKEN", token).apply()
                        preferences.edit().putString("USER", userInfoListJsonString).apply()

                        val imageView = root?.header_cover_image
                        Glide.with(requireContext()).load(PUBLIC_URL + it.response.path)
                            .error(R.drawable.cover_default).placeholder(R.drawable.cover_default).into(imageView!!)

                    } else {
                        customToast(getString(R.string.api_error_message), requireActivity(), 0)
                    }

                }

            }

        })

    }

    private fun uploadProfile(fileUri: Uri) {
        val file = File(getPath(fileUri))

        val requestFile: RequestBody = file.asRequestBody(
            requireActivity().contentResolver.getType(fileUri)!!.toMediaTypeOrNull())

        val body = MultipartBody.Part.createFormData("profile", file.name, requestFile)

        val descriptionString = selectedColorCode.toString()
        val description: RequestBody = descriptionString.toRequestBody(MultipartBody.FORM)

        // finally, execute the request
        val progressDialog = CustomProgressDialogNew(requireContext())
        activity?.runOnUiThread {
            progressDialog.hide()
        }

        profileUserViewModel?.uploadProfile(
            description, body, "Bereader ${token.toString()}")

        profileUserViewModel?.statusImage?.observe(viewLifecycleOwner, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    root?.button_next?.revertAnimation()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            profileUserViewModel?.uploadProfile(
                                description, body, "Bereader ${token.toString()}")
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    root?.button_next?.revertAnimation()
                    customToast(it.errorMsg, requireActivity(), 0)
                }

                is NetworkResponse.SUCCESS.uploadImage -> {

                    activity?.runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", ":response:uploadimage::::" + it.response)
                    if (it.response.status == 200) {
                        customToast(
                            getString(R.string.upload_success_message), requireActivity(), 1)
                        val dataUserDetailObject = UserDetailsQuery.User(
                            userDataModel.__typename(),
                            userDataModel._id(),
                            userDataModel.name(),
                            userDataModel.mobile(),
                            userDataModel.followerCounts(),
                            userDataModel.followingCounts(),
                            userDataModel.username(),
                            userDataModel.createdAt(),
                            userDataModel.likeCounts(),
                            userDataModel.bio(),
                            it.response.path,
                            userDataModel.coverImage(),
                            userDataModel.dob(),
                            userDataModel.email(),
                            userDataModel.gender(),
                            userDataModel.followed(),
                            userDataModel.followedBack(),
                            userDataModel.postsCounts(),
                            userDataModel.privacyStatus(),
                            selectedColorCode,
                            userDataModel.authentication(),
                            userDataModel.agreementAccepted(),
                            userDataModel.status(),
                            userDataModel.blocked(),
                            userDataModel.state(),
                            userDataModel.city(),
                            userDataModel.location(),
                            userDataModel.verify_user(),
                            userDataModel.downloadCounts())
                        Log.d("tag", "dataUserDetailObject:::$dataUserDetailObject")
                        //                        getUserDetails(userId.toString())
                        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
                            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                        val gson = Gson()

                        // Get java object list json format string.
                        val userInfoListJsonString = gson.toJson(dataUserDetailObject)

                        preferences.edit().putString("TOKEN", token).apply()
                        preferences.edit().putString("USER", userInfoListJsonString).apply()

                    } else {
                        root?.button_next?.revertAnimation()

                        customToast(getString(R.string.api_error_message), requireActivity(), 0)
                    }

                }

            }

        })
    }

    fun saveDataToPreference() {

    }

    private fun imageCrop(file: File) {
        try {

            val contentUri = FileProvider.getUriForFile(
                requireContext(), requireContext().applicationContext.packageName + ".provider", file)

            if (IMAGE_PICK_CODE == 200 || CAMERA_REQUEST == 1888) {
                CropImage.activity(contentUri).setAspectRatio(4, 3).start(requireContext(), this)
            } else {
                CropImage.activity(contentUri).setAspectRatio(1, 1).start(requireContext(), this)
            }

        } // respond to users whose devices do not support the crop action
        catch (anfe: ActivityNotFoundException) {
            // display an error message
            val errorMessage = "your device doesn't support the crop action!"

            customToast(errorMessage, requireActivity(), 0)

        }
    }


    private fun performCrop(picUri: String) {
        try {

            // indicate image type and Uri
            val f = File(picUri)
            Log.d("tag", "file::::$f")
            val contentUri = FileProvider.getUriForFile(
                requireContext(), requireContext().applicationContext.packageName + ".provider", f)
            if (IMAGE_PICK_CODE == 200 || CAMERA_REQUEST == 1888) {
                CropImage.activity(contentUri).setAspectRatio(4, 3).start(requireContext(), this)
            } else {
                CropImage.activity(contentUri).setAspectRatio(1, 1).start(requireContext(), this)
            }

        } // respond to users whose devices do not support the crop action
        catch (anfe: ActivityNotFoundException) {
            // display an error message
            val errorMessage = "your device doesn't support the crop action!"

            customToast(errorMessage, requireActivity(), 0)

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        val progressDialog = CustomProgressDialogNew(requireContext())
        activity?.runOnUiThread {
            progressDialog.dismiss()
        }
    }

    /** Change color transparency based on percentage  */
    fun changeAlpha(color: Int, fraction: Float): Int {
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        val alpha = (Color.alpha(color) * fraction).toInt()
        return Color.argb(alpha, red, green, blue)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        root?.recyclerView?.adapter = null

    }

    override fun onItemClickListener(position: Int) {
        val gson = Gson()
        val userInfoListJsonString = gson.toJson(profileUserVideoList)
        var bundle = bundleOf(
            "navigationFlagProfile" to "ProfilePublic",
            "videoList" to userInfoListJsonString,
            "position" to position,
            "isVideoCurrentPage" to homePagePasscurrentPage + 1)
        activity?.openActivity(UserVideosActivity::class.java, bundle)
        activity?.finish()
    }

    private fun getColorList() {
        val progressDialog = CustomProgressDialogNew(requireContext())
        activity?.runOnUiThread {
            progressDialog.hide()
        }
        val colorListViewModel = ViewModelProvider(this)[ColorListViewModel::class.java]
        colorListViewModel?.getColorList()

        colorListViewModel?.colorArrayList?.observe(viewLifecycleOwner, Observer {
            activity?.runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(requireView(), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            colorListViewModel?.getColorList()
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_FORBIDDEN -> {
                    callLoginFragment(requireContext(), it.errorMsg.toString())
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, requireActivity(), 0)
                }
                is NetworkResponse.SUCCESS.colorList -> {

                    Log.d("tag", "response::" + it.response)
                    setBottomSheetForColor(it.response)

                }
            }
        })
    }

    private fun setBottomSheetForColor(arrayList: List<ColorListQuery.Color>) {
        val llBottomSheet = root?.bottom_sheet

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        initRecyclerView(arrayList)

        llBottomSheet?.setOnClickListener {
            root?.overlay_view_profile_select_color?.visibility = View.VISIBLE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        root?.frameColorLogoChange?.setOnClickListener {
            root?.overlay_view_profile_change_pic?.visibility = View.GONE
            bottomSheetBehaviorChange.setState(BottomSheetBehavior.STATE_HIDDEN)
            root?.overlay_view_profile_select_color?.visibility = View.VISIBLE
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        root?.txtDone?.setOnClickListener {
            if (colorCode != null) {

                root?.overlay_view_profile_select_color?.visibility = View.GONE
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                root?.overlay_view_profile_change_pic?.visibility = View.GONE
                bottomSheetBehaviorChange.state = BottomSheetBehavior.STATE_HIDDEN
                Log.d("tag", "colorCode::::$colorCode")


                root?.imgProfileBorderChange?.setBorder(colorCode ?: Utils.COLOR_CODE)
                root?.imgFrameColorChange?.setBorder(colorCode ?: Utils.COLOR_CODE)
                root?.imgProfilePicUser?.setBorder(colorCode ?: Utils.COLOR_CODE)

                selectedColorCode = colorCode


                if (uri != null) {
                    val imageView = root?.imgProfilePic
                    Glide.with(requireContext()).load(uri).error(R.drawable.ic_login_profile).into(imageView!!)
                    val imgProfilePicChange = root?.imgProfilePicChange
                    Glide.with(requireContext()).load(uri).error(R.drawable.ic_login_profile)
                        .into(imgProfilePicChange!!)

                    uploadProfile(uri!!)
                } else {

                    if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED || requireActivity().checkSelfPermission(
                            Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                    ) {
                        val permissions = arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA)
                        requestPermissions(permissions, UPLOAD_FILE)
                    } else {
                        try {
                            val url = URL(image)
                            val alreadySelectedBitmap = BitmapFactory.decodeStream(
                                url.openConnection().getInputStream())
                            val bytes = ByteArrayOutputStream()
                            alreadySelectedBitmap?.compress(
                                Bitmap.CompressFormat.JPEG, 100, bytes)

                            val path: String = MediaStore.Images.Media.insertImage(
                                requireActivity().contentResolver,
                                alreadySelectedBitmap,
                                "IMG_" + Calendar.getInstance().time,
                                null)
                            uri = Uri.parse(path)


                        } catch (e: Exception) {
                            Log.d("tag", "error:::${e.message}")

                            e.printStackTrace()
                            val alreadySelectedBitmap = BitmapFactory.decodeResource(
                                resources, R.drawable.ic_login_profile)
                            val bytes = ByteArrayOutputStream()
                            alreadySelectedBitmap?.compress(
                                Bitmap.CompressFormat.JPEG, 100, bytes)
                            val path: String = MediaStore.Images.Media.insertImage(
                                requireActivity().contentResolver,
                                alreadySelectedBitmap,
                                "IMG_" + Calendar.getInstance().time,
                                null)
                            uri = Uri.parse(path)
                        }
                        uploadProfile(uri!!)
                    }
                }
            }
        }

        root?.overlay_view_profile_select_color?.setOnClickListener {

            root?.overlay_view_profile_select_color?.visibility = View.GONE
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        root?.overlay_view_profile_select_color?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        root?.overlay_view_profile_select_color?.visibility = View.VISIBLE
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        root?.overlay_view_profile_select_color?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        root?.overlay_view_profile_select_color?.visibility = View.GONE
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }


            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                /*yourView.animate()
                    .y(if (v <= 0) view!!.y + mSheetBehavior.getPeekHeight() - yourView.getHeight() else view!!.height - yourView.getHeight())
                    .setDuration(0).start()*/
            }
        })
    }

    private fun initRecyclerView(arrayList: List<ColorListQuery.Color>) {

        root?.menu_recycler_view?.apply {
            layoutManager = LinearLayoutManager(
                root?.menu_recycler_view?.context, RecyclerView.HORIZONTAL, false)
            adapter = ColorSelectionAdapter(
                arrayList, requireContext())
            (adapter as ColorSelectionAdapter).onItemClick = { data ->
                // do something with your item
                Log.d("tag", data.colorCode().toString())
                colorCode = data.colorCode().toString()
            }
        }


    }
}