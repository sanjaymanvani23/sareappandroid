package com.example.sare.profile.viewmodel

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.FollowerListQuery
import com.example.sare.FollowingListQuery
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class FollowingFollowerListViewModel : ViewModel() {
    var followingArrayList = MutableLiveData<NetworkResponse>()
    var followerArrayList = MutableLiveData<NetworkResponse>()

    fun getFollowingList(context: Context, userId: String, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(FollowingListQuery(userId.toString(), pageCount, PAGE_SIZE))
            .enqueue(object : ApolloCall.Callback<FollowingListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "error:::" + e.message)

                    followingArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<FollowingListQuery.Data>) {
                    runBlocking {
                        Log.d("tag", "response:::" + response)
                        try {
                            if (response.data != null) {

                                followingArrayList.postValue(
                                    NetworkResponse.SUCCESS.getFollowingList(
                                        response.data()?.following()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                followingArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            followingArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getFollowerList(context: Context, userId: String, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(
            FollowerListQuery(userId.toString(), pageCount, PAGE_SIZE)
        )
            .enqueue(object : ApolloCall.Callback<FollowerListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "error:::" + e.message)

                    followerArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<FollowerListQuery.Data>) {
                    runBlocking {
                        Log.d("tag", "response:::" + response)
                        try {
                            if (response.data()?.followers() != null) {
                                followerArrayList.postValue(
                                    NetworkResponse.SUCCESS.getFollowerList(
                                        response.data()?.followers()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                followerArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            followerArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}
