package com.example.sare.profile.viewmodel

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.sare.api.ApiService
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.NetworkResponse
import com.example.sare.data.ResponseData
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileUserViewModel : ViewModel() {
    var status = MutableLiveData<NetworkResponse>()
    var statusImage = MutableLiveData<NetworkResponse>()

    fun uploadCoverImage(body: MultipartBody.Part, token: String) {
        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)
        Log.d("tag", "token::::User view model::;$token")

        val call: Call<ResponseData> = service.uploadCoverImage(
            body,
            "Bearer $token"
        )


        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(
                call: Call<ResponseData?>?,
                response: Response<ResponseData?>?
            ) {
                Log.d("tag", "response:::USER Profile::$response")

                if (response!!.isSuccessful) {

                    Log.d("tag", "response:::::" + response.body().toString())
                    if (response.body() != null) {
                        runBlocking {
                            status.postValue(
                                NetworkResponse.SUCCESS.uploadImage(
                                    response.body()!!
                                )
                            )
                        }
                    }else {
                        runBlocking {

                            status.postValue(
                                NetworkResponse.ERROR_RESPONSE(
                                    response.errorBody().toString()
                                )
                            )
                        }
                    }

                }else{
                    runBlocking {

                        status.postValue(
                            NetworkResponse.ERROR_RESPONSE(
                                response.errorBody().toString()
                            )
                        )
                    }
                }
            }

            override fun onFailure(
                call: Call<ResponseData?>,
                t: Throwable
            ) {
                t.printStackTrace()
                Log.d("tag", "error::::" + t.message)
                status.postValue(NetworkResponse.ERROR(t))

                Log.e("Upload error:", t.message!!)

            }
        })
    }




    fun uploadProfile(description: RequestBody, body: MultipartBody.Part, token: String) {
        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)

        val call: Call<ResponseData> = service.uploadImage(
            description, body,
            token.toString()
        )


        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(
                call: Call<ResponseData?>?,
                response: Response<ResponseData?>?
            ) {
                Log.e("Upload Success:", response?.body().toString())

                if (response!!.isSuccessful) {

                    if (response.body() != null) {
                        runBlocking {
                            statusImage.postValue(
                                NetworkResponse.SUCCESS.uploadImage(
                                    response.body()!!
                                )
                            )
                        }
                    } else {
                        runBlocking {

                            statusImage.postValue(
                                NetworkResponse.ERROR_RESPONSE(
                                    response.errorBody().toString()
                                )
                            )
                        }
                    }

                } else {
                    runBlocking {

                        statusImage.postValue(
                            NetworkResponse.ERROR_RESPONSE(
                                response.errorBody().toString()
                            )
                        )
                    }
                }


            }

            override fun onFailure(
                call: Call<ResponseData?>,
                t: Throwable
            ) {
                t.printStackTrace()
                statusImage.postValue(NetworkResponse.ERROR(t))

                Log.e("Upload error:", t.message!!)


            }
        })
    }
}