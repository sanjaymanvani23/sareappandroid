package com.example.sare.profile.activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.base.BaseActivity
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.*
import com.example.sare.profile.RecyclerTouchListener
import com.example.sare.profile.adapter.FollowerAdapter
import com.example.sare.profile.adapter.FollowingAdapter
import com.example.sare.profile.listener.ItemOnclickListenerFollower
import com.example.sare.profile.viewmodel.FollowingFollowerListViewModel
import com.example.sare.ui.login.LoginActivity
import com.example.sare.videoRecording.SliderLayoutManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_followers_following.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M) class FollowersFollowingActivity : BaseActivity(), ItemOnclickListenerFollower {
    var viewModel: FollowingFollowerListViewModel? = null
    var isClicked: Boolean = false
    var filterList: List<FollowerListQuery.Follower> = ArrayList()
    var filterFollowingList: List<FollowingListQuery.Following> = ArrayList()
    var userId: String? = null
    var name: String? = null
    var image: String? = null
    var colorCode: String? = null
    var type: String? = null
    var clicked: String? = null
    lateinit var followersAdapter: FollowerAdapter
    lateinit var followingAdapter: FollowingAdapter
    lateinit var recyclerTouchListener: RecyclerTouchListener
    private var arrayList: MutableList<FollowerListQuery.Follower> = mutableListOf()
    private var followingArrayList: MutableList<FollowingListQuery.Following> = mutableListOf()
    var pos: Int = 0
    var yPos = MutableLiveData<Float>()
    lateinit var progressDialog: CustomProgressDialogNew
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    var isLoadingFollowers = false
    var isLastPageFollowers = false
    private var cuurentPageFollowers: Int = 0
    var id: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_followers_following)
        type = intent.getStringExtra("type").toString()
        Log.d("tag", "type:::$type")
        Log.d("tag", "type1:::$type")

        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        id = mainObject.getString("_id")
        userId = intent.getStringExtra("userId").toString()
        name = intent.getStringExtra("name").toString()
        image = intent.getStringExtra("image").toString()
        colorCode = intent.getStringExtra("colorCode").toString()
        setData(colorCode, name)
        scrollView?.isEnabled = false
        scrollView1?.isEnabled = false
        setTheme()
        viewModel = ViewModelProvider(this)[FollowingFollowerListViewModel::class.java]
        recyclerTouchListener = RecyclerTouchListener(this@FollowersFollowingActivity, rvFollowers)
        searchLayout?.visibility = View.GONE
        val pos = getRelativeTop(ll_options)
        Log.d("tag", "pos top::::$pos")

        imgSearch?.setOnClickListener {
            rvFollowings.smoothScrollToPosition(0)
            addBottomToTopAnimation()
        }
        txtCancel?.setOnClickListener {
            addTopToBottomAnimation()
        }

        progressDialog = CustomProgressDialogNew(this@FollowersFollowingActivity)

        val vto: ViewTreeObserver = ll_options.getViewTreeObserver()
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    ll_options.getViewTreeObserver().removeGlobalOnLayoutListener(this)
                } else {
                    ll_options.getViewTreeObserver().removeOnGlobalLayoutListener(this)
                }
                yPos.value = ll_options.y

                Log.d("yPos TreeObserver", "on click on getViewTreeObserver::${yPos}")

            }
        })

        Log.d("yPos", "on click on create::${yPos}")

        initRecyclerView()
        setObservers()

        setListeners()
        if (type == "FOLLOWERS") {
            clicked = "1"
            progressDialog.show()
            button_followers?.setTextColor(
                resources.getColor(
                    R.color.textColor, this@FollowersFollowingActivity.theme))
            button_followings?.setTextColor(
                resources.getColor(
                    R.color.black, this@FollowersFollowingActivity.theme))
            button_followers?.setBackgroundResource(R.drawable.button_tab_background);
            button_followings?.setBackgroundResource(0)
            getFollowerList()
        } else if (type == "FOLLOWINGS") {
            clicked = "2"
            button_followings?.setTextColor(
                resources.getColor(
                    R.color.textColor, this@FollowersFollowingActivity.theme))
            button_followers?.setTextColor(
                resources.getColor(
                    R.color.black, this@FollowersFollowingActivity.theme))
            button_followings?.setBackgroundResource(R.drawable.button_tab_background);
            button_followers?.setBackgroundResource(0)
            getFollowingList()

        }
        setPadding()
    }

    private fun setObservers() {
        observerFollowingList()
        observerFollowerList()
    }


    private fun initRecyclerView() {
        initFollowingRecyclerView()
        initFollowerRecyclerView()
    }

    private fun setData(colorCode: String?, name: String?) {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)
        txtName?.text = name

        imgProfile?.loadUrl(
            Utils.PUBLIC_URL + (image.toString())!!)
        imgProfileBorderFollower?.setBorder(colorCode ?: Utils.COLOR_CODE)
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this@FollowersFollowingActivity, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this@FollowersFollowingActivity)) {
            window.navigationBarColor = ThemeManager.colors(
                this@FollowersFollowingActivity, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun setListeners() {
        imgClose?.setOnClickListener {
            etSearchUser?.setText("")
        }
        imgBack?.setOnClickListener {
            onBackPressed()
        }





        button_followers?.setOnClickListener {
            // yPos = ll_options.y
            Log.d("on click", "on click::$yPos")
            clicked = "1"
            progressDialog.show()
            button_followers?.setTextColor(
                resources.getColor(
                    R.color.textColor, this@FollowersFollowingActivity.theme))
            button_followings?.setTextColor(
                resources.getColor(
                    R.color.black, this@FollowersFollowingActivity.theme))

            button_followers?.setBackgroundResource(R.drawable.button_tab_background);
            button_followings?.setBackgroundResource(0)





            /*if (pos == 0 || pos == 2) {
                pos = 2
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(
                    this@FollowersFollowingActivity, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.anim_left);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.addAnimation(leftSwipe);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followings?.startAnimation(set)

            } else if (pos == 1) {
                pos = 2
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(
                    this@FollowersFollowingActivity, R.anim.anim_right)
                val leftSwipe = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.anim_left);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.addAnimation(leftSwipe);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followings?.startAnimation(set)
            } else {

                pos = 1
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val rightSwipe = AnimationUtils.loadAnimation(
                    this@FollowersFollowingActivity, R.anim.anim_right);
                val set = AnimationSet(true)
                set.addAnimation(rightSwipe);
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followings?.startAnimation(set)
            }*/

            arrayList.clear()

            isLoadingFollowers = false
            isLastPageFollowers = false
            cuurentPageFollowers = 0

            getFollowerList()

        }

        button_followings?.setOnClickListener {
            // yPos = ll_options.y
            Log.d("on click", "on click::$yPos")
            clicked = "2"
            progressDialog.show()
            followingArrayList.clear()

            isLoading = false
            isLastPage = false
            currentPage = 0

            button_followings?.setTextColor(
                resources.getColor(
                    R.color.textColor, this@FollowersFollowingActivity.theme))
            button_followers?.setTextColor(
                resources.getColor(
                    R.color.black, this@FollowersFollowingActivity.theme))
            button_followings?.setBackgroundResource(R.drawable.button_tab_background);
            button_followers?.setBackgroundResource(0)
            isClicked = true
            Log.d("tag", "pos::::$pos")
            /*if (pos == 0 || pos == 1) {
                pos = 1

                val rightSwipe = AnimationUtils.loadAnimation(
                    this@FollowersFollowingActivity, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)

                val set = AnimationSet(true)
                set.addAnimation(rightSwipe)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followers?.startAnimation(set)

            } else if (pos == 2) {
                pos = 1

                val rightSwipe = AnimationUtils.loadAnimation(
                    this@FollowersFollowingActivity, R.anim.anim_right);
                val leftSwipe = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)

                val set = AnimationSet(true)
                set.addAnimation(rightSwipe)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followers?.startAnimation(set)
            } else {
                pos = 2
                val leftSwipe = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.anim_left);
                val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
                val set = AnimationSet(true)
                set.addAnimation(leftSwipe)
                set.addAnimation(moveAnim);
                set.duration = 300;
                set.fillAfter = true
                set.interpolator = LinearInterpolator()
                button_followers?.startAnimation(set)
            }*/
            getFollowingList()
        }



        etSearchUser?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                imgClose.visible()
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }


    private fun getFollowingList() {
        progressDialog.show()
        Log.d("tag", "currentPage::::::getFollowingList:::$currentPage")
        viewModel?.getFollowingList(
            this@FollowersFollowingActivity, userId.toString(), currentPage * Utils.PAGE_SIZE)
    }

    fun setPadding() {
        yPos?.observe(this, Observer {
            val padding: Int = Utils.getScreenWidth(this) / 2 - Utils.dpToPx(
                this, it )

            Log.e("Tag Value", "$it")
            rvFollowings.setPadding(0, padding, 0, 0)
        })
    }

    fun observerFollowingList() {
        viewModel?.followingArrayList?.observe(this, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.hide()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relFollowersFollowing), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            viewModel?.getFollowingList(
                                this@FollowersFollowingActivity, userId.toString(), currentPage * Utils.PAGE_SIZE)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()
                    txtNoDataFollowing?.visibility = View.VISIBLE
                    rvFollowings?.visibility = View.GONE
                    rvFollowers?.visibility = View.GONE
                    txtNoData?.visibility = View.GONE
                }

                is NetworkResponse.SUCCESS.getFollowingList -> {
                    progressDialog.hide()
                    isLoading = false
                    if (it.response != null) {

                        /*val gson = Gson()

                        val userInfoListJsonString = gson.toJson(it.response)

                        val myType = object : TypeToken<List<FollowingListQuery.Following>>() {}.type

                        var followingArrayList = gson.fromJson<List<FollowingListQuery.Following>>(
                            userInfoListJsonString, myType) as ArrayList<FollowingListQuery.Following>
                        Log.e("Ta Size", "${followingArrayList.size}")
                        followingAdapter.addItems(followingArrayList)

                        runOnUiThread { followingAdapter.notifyDataSetChanged() }*/


                        followingArrayList.addAll(it.response)
                        Log.d("tag", "followingArrayList::::SIZE::::${followingArrayList.size}")
                        rvFollowings?.adapter?.notifyDataSetChanged()
                        if (it.response.size < Utils.PAGE_SIZE) {
                            isLastPage = true
                        }

                        if (rvFollowings?.adapter?.itemCount == 0) {
                            txtNoDataFollowing?.visibility = View.VISIBLE
                            rvFollowings?.visibility = View.GONE
                            rvFollowers?.visibility = View.GONE
                            txtNoData?.visibility = View.GONE
                        } else {
                            rvFollowings?.visibility = View.VISIBLE
                            txtNoDataFollowing?.visibility = View.GONE
                            rvFollowers?.visibility = View.GONE
                            txtNoData?.visibility = View.GONE

                        }


                    } else {
                        customToast(
                            getString(R.string.api_error_message), this@FollowersFollowingActivity, 0)
                    }
                }
            }
        })
    }

    private fun getFollowerList() {
        progressDialog.show()
        viewModel?.getFollowerList(
            this@FollowersFollowingActivity, userId.toString(), cuurentPageFollowers * Utils.PAGE_SIZE)
    }

    fun observerFollowerList() {

        viewModel?.followerArrayList?.observe(this, Observer {
            when (it) {
                is NetworkResponse.ERROR -> {
                    progressDialog.hide()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relFollowersFollowing), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            viewModel?.getFollowerList(
                                this@FollowersFollowingActivity, userId.toString(), currentPage * Utils.PAGE_SIZE)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()

                    txtNoData?.visibility = View.VISIBLE
                    rvFollowers?.visibility = View.GONE
                    rvFollowings?.visibility = View.GONE
                    txtNoDataFollowing?.visibility = View.GONE
                }

                is NetworkResponse.SUCCESS.getFollowerList -> {
                    progressDialog.hide()
                    isLoadingFollowers = false
                    if (it.response != null) {
                        arrayList.addAll(it.response)
                        rvFollowers?.adapter?.notifyDataSetChanged()
                        if (it.response.size < Utils.PAGE_SIZE) {
                            isLastPageFollowers = true
                        }

                        if (rvFollowers?.adapter?.itemCount == 0) {
                            txtNoData?.visibility = View.VISIBLE
                            rvFollowers?.visibility = View.GONE
                            rvFollowings?.visibility = View.GONE
                            txtNoDataFollowing?.visibility = View.GONE

                        } else {
                            txtNoData?.visibility = View.GONE
                            rvFollowers?.visibility = View.VISIBLE
                            rvFollowings?.visibility = View.GONE
                            txtNoDataFollowing?.visibility = View.GONE

                        }


                    } else {
                        this@FollowersFollowingActivity.runOnUiThread {
                            customToast(
                                getString(R.string.api_error_message), this@FollowersFollowingActivity, 0)

                        }
                    }
                }
            }

        })
    }

    private fun filter(text: String) {
        val filteredNames: ArrayList<FollowerListQuery.Follower> = ArrayList()
        val filterFollowingNames: ArrayList<FollowingListQuery.Following> = ArrayList()
        Log.d("tag", "list.size:::" + arrayList.size)
        Log.d("tag", "list.size:::" + filterFollowingNames.size)
        Log.d("tag", "clicked:::" + clicked)

        if (clicked == "1") {
            if (arrayList.isNotEmpty()) {
                for (s in arrayList) {
                    if (s.user()?.name()!!.toLowerCase().contains(text.toLowerCase())) {
                        filteredNames.add(s)
                    }
                }
            }
            Log.d("tag", "filteredNames::::$filteredNames")

            rvFollowers?.apply {
                layoutManager = LinearLayoutManager(
                    rvFollowers?.context, RecyclerView.VERTICAL, false)

                filterList = (adapter as FollowerAdapter).filterList(filteredNames)!!
                adapter = FollowerAdapter(
                    arrayList, this@FollowersFollowingActivity, userId.toString(), this@FollowersFollowingActivity)

                if (adapter!!.itemCount == 0) {
                    txtNoData?.visibility = View.VISIBLE
                    rvFollowings?.visibility = View.GONE
                    txtNoDataFollowing?.visibility = View.GONE


                } else {
                    rvFollowers?.visibility = View.VISIBLE
                    txtNoData?.visibility = View.GONE
                    rvFollowings?.visibility = View.GONE
                    txtNoDataFollowing?.visibility = View.GONE
                }
            }
        } else {

            if (followingArrayList.isNotEmpty()) {
                for (s in followingArrayList) {
                    if (s.user().name()!!.toLowerCase().contains(text.toLowerCase())) {

                        filterFollowingNames.add(s)
                    }
                }
            }

            rvFollowings?.apply {
                layoutManager = LinearLayoutManager(rvFollowings?.context, RecyclerView.VERTICAL, false)
                adapter = FollowingAdapter(followingArrayList,
                    this@FollowersFollowingActivity, userId.toString(), this@FollowersFollowingActivity)
                filterFollowingList = (adapter as FollowingAdapter).filterList(filterFollowingNames)!!

                if (adapter!!.itemCount == 0) {
                    txtNoDataFollowing?.visibility = View.VISIBLE
                    rvFollowers?.visibility = View.GONE
                    txtNoData?.visibility = View.GONE

                } else {
                    rvFollowings?.visibility = View.VISIBLE
                    txtNoDataFollowing?.visibility = View.GONE
                    rvFollowers?.visibility = View.GONE
                    txtNoData?.visibility = View.GONE
                }

            }
        }



        Log.d("tag", "list1::::$filterList")
    }


    private fun initFollowerRecyclerView() {
        followersAdapter = FollowerAdapter(
            arrayList, this@FollowersFollowingActivity, userId.toString(), this@FollowersFollowingActivity)
        val padding: Int = Utils.getScreenWidth(this) / 2 - Utils.dpToPx(
            this, 157f)
        rvFollowers.setPadding(0, padding, 0, 0)
        val sliderLayoutManager = SliderLayoutManager(this).apply {
            callback = object : SliderLayoutManager.OnItemSelectedListener {
                override fun onItemSelected(layoutPosition: Int) {

                }
            }
        }

        rvFollowers.layoutManager = sliderLayoutManager
        rvFollowers.suppressLayout(false)
        rvFollowers.isNestedScrollingEnabled = false
        rvFollowers.smoothScrollToPosition(0)
        Log.d("tag", "arrayList::;$arrayList")

        rvFollowers?.apply {
            /*layoutManager = LinearLayoutManager(
                rvFollowers?.context, RecyclerView.VERTICAL, false

            )*/

            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {

                override fun loadMoreItems() {
                    isLoadingFollowers = true
                    cuurentPageFollowers++
                    getFollowerList()

                    Log.d("tag", "Pagination Current page 11 : " + cuurentPageFollowers)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPageFollowers)
                    return isLastPageFollowers
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : $isLoadingFollowers")
                    return isLoadingFollowers
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    val layoutManager1 = layoutManager as LinearLayoutManager
                    val firstVisibleItemPosition = layoutManager1!!.findFirstCompletelyVisibleItemPosition()
                    Log.e("Test Y Value", "$dy : $firstVisibleItemPosition")
                    if (dy > 0) {
                        if (!imgSearch?.isVisible!!) addTopToBottomAnimation()
                        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(windowToken, 0)
                        ll_options1?.gone()
                    } else {
                        /* if (firstVisibleItemPosition == 0) {
                             ll_options1?.visible()
                         }*/
                    }

                }


            })

            adapter = FollowerAdapter(
                arrayList, this@FollowersFollowingActivity, userId.toString(), this@FollowersFollowingActivity)

            recyclerTouchListener.setClickable(object : RecyclerTouchListener.OnRowClickListener {

                override fun onRowClicked(position: Int) {

                }


                override fun onIndependentViewClicked(independentViewID: Int, position: Int) {

                }


            }).setSwipeOptionViews(R.id.imgCard, R.id.relDetails).setSwipeable(R.id.relDetails,
                R.id.relCard,
                RecyclerTouchListener.OnSwipeOptionsClickListener() { viewID, position ->

                    when (viewID) {
                        R.id.imgCard -> {

                            val preferences: SharedPreferences = this@FollowersFollowingActivity.getSharedPreferences(
                                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
                            val token = preferences.getString("TOKEN", null)
                            val apolloClientNew = ApolloClient.setupApollo(token.toString())
                            Log.d(
                                "tag", "arrayList[position].user()._id()::${arrayList[position].user()!!._id()}")

                            apolloClientNew.mutate(
                                FollowerDeleteMutation(
                                    arrayList[position].user()!!._id()))
                                .enqueue(object : ApolloCall.Callback<FollowerDeleteMutation.Data>() {
                                    override fun onFailure(e: ApolloException) {
                                        Log.d("tag", "Error Data : ${e.message}")

                                        customToast(
                                            e.message.toString(), this@FollowersFollowingActivity, 0)
                                        MyApplication.mainActivity.logoutUser()
                                        openNewActivity(LoginActivity::class.java)
                                    }

                                    override fun onResponse(response: Response<FollowerDeleteMutation.Data>) {
                                        this@FollowersFollowingActivity.runOnUiThread {
                                            Log.d("tag", "res delete adapter::${response}")
                                            if (response.data?.followerDelete()?.status() == true) {
                                                followersAdapter.deleteItem(arrayList, position)

                                                rvFollowers.adapter?.notifyItemRemoved(position)
                                                rvFollowers.adapter?.notifyDataSetChanged()
                                                Log.d(
                                                    "tag", "arrayList.size::${arrayList.size}")

                                                if (arrayList.size == 0) {
                                                    txtNoData?.visibility = View.VISIBLE
                                                } else {
                                                    txtNoData?.visibility = View.GONE

                                                }

                                                customToast(
                                                    "User has been removed!", this@FollowersFollowingActivity, 1)
                                            } else {
                                                val error = response.errors?.get(0)?.message
                                                Log.d("tag", "Data Error: $error")
                                                val errorCode = response.errors?.get(0)?.customAttributes?.get(
                                                    "status")

                                                if (errorCode?.toString()?.equals("401") == true) {
                                                    customToast(
                                                        error.toString(), this@FollowersFollowingActivity, 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    openNewActivity(LoginActivity::class.java)
                                                } else if (errorCode?.toString()?.equals("403") == true) {
                                                    customToast(
                                                        error.toString(), this@FollowersFollowingActivity, 0)
                                                    MyApplication.mainActivity.logoutUser()
                                                    openNewActivity(LoginActivity::class.java)
                                                } else {

                                                    customToast(
                                                        error.toString(), this@FollowersFollowingActivity, 0)
                                                }
                                            }
                                        }
                                    }

                                })

                        }
                        R.id.relDetails -> {

                        }

                    }
                })
            rvFollowers?.addOnItemTouchListener(recyclerTouchListener)

        }

    }

    private fun initFollowingRecyclerView() {
        Log.d("yPos", "on click on initFollowingRecyclerView::${yPos}")
        val padding: Int = Utils.getScreenWidth(this) / 2 - Utils.dpToPx(
            this, 157f)
        rvFollowings.setPadding(0, padding, 0, 0)
        val sliderLayoutManager = SliderLayoutManager(this).apply {
            callback = object : SliderLayoutManager.OnItemSelectedListener {
                override fun onItemSelected(layoutPosition: Int) {
                    Log.e("tag pos:","pos:::$layoutPosition")
                 /*   if (layoutPosition > 0) {

                        if (!imgSearch?.isVisible!!){
//                            yPos.value=yPos.value?.plus(50)
                            addTopToBottomAnimation()
                        }

                    }*/
                }
            }
        }

        rvFollowings.layoutManager = sliderLayoutManager
        rvFollowings.suppressLayout(false)
        rvFollowings.isNestedScrollingEnabled = false
        rvFollowings.smoothScrollToPosition(0)
        rvFollowings?.apply {
            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getFollowingList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    Log.d("dy pos", "DY:::::$dy")



                }
            })
            adapter = FollowingAdapter(
                followingArrayList,
                this@FollowersFollowingActivity,
                userId.toString(),
                this@FollowersFollowingActivity)
        }
        followingAdapter = FollowingAdapter(
            followingArrayList,
            this@FollowersFollowingActivity,
            userId.toString(),
            this@FollowersFollowingActivity)

        /*    rvFollowings?.itemAnimator = null
            rvFollowings.setHasFixedSize(true)
            rvFollowings?.setPadding(0,200,0,0)

            //        rvFollowings?.itemAnimator = DefaultItemAnimator()
            *//* val simpleItemAnimator : SimpleItemAnimator ?= null*//*
       *//* ((rvFollowings.itemAnimator as SimpleItemAnimator)).supportsChangeAnimations = false
        (rvFollowings.itemAnimator as SimpleItemAnimator).endAnimations()*//*
        rvFollowings?.apply {
            layoutManager = LinearLayoutManager(
                rvFollowings?.context, RecyclerView.VERTICAL, false

            )

            addOnScrollListener(object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getFollowingList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    *//*((rvFollowings.itemAnimator as SimpleItemAnimator)).supportsChangeAnimations = false
                    (rvFollowings.itemAnimator as SimpleItemAnimator).endAnimations()*//*
                    val layoutManager1 = layoutManager as LinearLayoutManager
                    val firstVisibleItemPosition = layoutManager1!!.findFirstCompletelyVisibleItemPosition()
                    Log.e("Test Y Value", "$dy : $firstVisibleItemPosition")
                    if (dy > 0) {
                        if (!imgSearch?.isVisible!!) addTopToBottomAnimation()
                        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(windowToken, 0)
                       // ll_options1?.gone()
                    } else {
                       *//* if (firstVisibleItemPosition == 0) {
                            ll_options1?.visible()
                        }*//*
                    }
                }

            })
            adapter = FollowingAdapter(
                followingArrayList,
                this@FollowersFollowingActivity,
                rvFollowings!!,
                userId.toString(),
                this@FollowersFollowingActivity)
        }
*/
    }



    override fun onResume() {
        super.onResume()
        rvFollowers?.addOnItemTouchListener(recyclerTouchListener)

    }

    private fun addBottomToTopAnimation() {
        searchLayout?.visible()
        val slideUp = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.slide_from_right)
        searchLayout?.startAnimation(slideUp)
        imgSearch?.gone()
    }

    private fun addTopToBottomAnimation() {
        val slideUp = AnimationUtils.loadAnimation(this@FollowersFollowingActivity, R.anim.slide_out_left)
        searchLayout?.startAnimation(slideUp)
        searchLayout?.gone()
        imgSearch?.visible()

        imgSearch?.alpha = 0f
        imgSearch?.animate()?.alpha(1f)?.duration = 400

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onItemClickListenerGuest(userId: String, type: String) {
        val bundle = bundleOf("userId" to userId, "type" to type)
        openActivity(ProfileGuestActivity::class.java, bundle)
        overridePendingTransition(0,0)
    }

    override fun onItemClickListenerUser() {
        val bundle = bundleOf("type" to "5")
        openActivity(BottomBarActivity::class.java, bundle)
        finish()
        finishAffinity()
    }

    override fun onItemClickListenerFollows(position: Int) {
        runOnUiThread { followingAdapter.deleteItem(this, position) }
    }

    private fun getRelativeTop(myView: View): Int {
        return if (myView.parent === myView.rootView) myView.top else myView.top + getRelativeTop(
            myView.parent as View)
    }
}