package com.example.sare.profile.viewmodel

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.fragment.findNavController
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.layout_profile_more_option.view.*
import kotlinx.coroutines.runBlocking

class UserVideoListViewModel : ViewModel() {
    var videoArrayList = MutableLiveData<NetworkResponse>()
    var draftArrayList = MutableLiveData<NetworkResponse>()
    var bookmarkArrayList = MutableLiveData<NetworkResponse>()
    var reportArrayList = MutableLiveData<NetworkResponse>()
    var bookmarkVideo = MutableLiveData<NetworkResponse>()
    var removeVideo = MutableLiveData<NetworkResponse>()
    var blockUser = MutableLiveData<NetworkResponse>()

    fun getUserVideoList(context: Context, userId: String, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::" + token)

        val apolloClient = ApolloClient.setupApollo(token.toString())

        apolloClient.query(UserVideosQuery(userId.toString(), pageCount, PAGE_SIZE, "Published"))
            .enqueue(object : ApolloCall.Callback<UserVideosQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    videoArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UserVideosQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.userVideos() != null) {
                                videoArrayList.postValue(NetworkResponse.SUCCESS.userVideoPublishedList(response.data()?.userVideos()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                videoArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())

                            }
                        } catch (e: Exception) {
                            videoArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getDraftVideos(context: Context, userId: String, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserVideosQuery(userId.toString(), pageCount, PAGE_SIZE, "Draft"))
            .enqueue(object : ApolloCall.Callback<UserVideosQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    draftArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UserVideosQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.userVideos() != null) {

                                draftArrayList.postValue(
                                    NetworkResponse.SUCCESS.userVideoDraftList(
                                        response.data()?.userVideos()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                draftArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            draftArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getBookMarkVideos(context: Context, userId: String, pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "pageCount:::Published videos:$pageCount")
        Log.d("tag", "PAGE_SIZE:::Published videos:$PAGE_SIZE")
        val apolloClient = ApolloClient.setupApollo(token.toString())

        apolloClient.query(UserVideosQuery(userId.toString(), pageCount, PAGE_SIZE, "BookMark"))
            .enqueue(object : ApolloCall.Callback<UserVideosQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    bookmarkArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UserVideosQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.userVideos() != null) {

                                bookmarkArrayList.postValue(
                                    NetworkResponse.SUCCESS.userVideoBookMarkList(
                                        response.data()?.userVideos()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                bookmarkArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            bookmarkArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getReportList(context: Context) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(ReportListQuery.builder().build())
            .enqueue(object : ApolloCall.Callback<ReportListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    reportArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<ReportListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.reportType() != null) {

                                reportArrayList.postValue(
                                    NetworkResponse.SUCCESS.getReportList(
                                        response.data()?.reportType()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                reportArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            reportArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }

                    }
                }
            })
    }

    fun removeVideo(context: Context, videoId: String) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient?.mutate(RemoveVideoMutation(videoId))
            ?.enqueue(object : ApolloCall.Callback<RemoveVideoMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    removeVideo.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<RemoveVideoMutation.Data>) {
                    runBlocking {
                        try {


                            if (response.data()
                                    ?.removeVideo() != null || response.data?.removeVideo()
                                    ?.status() == false
                            ) {
                                removeVideo.postValue(
                                    NetworkResponse.SUCCESS.removeVideo(
                                        response.data()?.removeVideo()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                removeVideo.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            removeVideo.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun removeBookMark(context: Context, videoId: String, action: Boolean) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient?.mutate(BookmarkVideoMutation(videoId, action))
            ?.enqueue(object : ApolloCall.Callback<BookmarkVideoMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    bookmarkVideo.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<BookmarkVideoMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.bookMark() != null) {
                                bookmarkVideo.postValue(
                                    NetworkResponse.SUCCESS.bookmarkVideo(
                                        response.data()?.bookMark()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                bookmarkVideo.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            bookmarkVideo.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    private fun blockUser(context: Context,id: String) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew?.mutate(BlockUserMutation(id))
            ?.enqueue(object : ApolloCall.Callback<BlockUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    blockUser.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<BlockUserMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.blockUser() != null) {
                                blockUser.postValue(
                                    NetworkResponse.SUCCESS.blockUser(
                                        response.data()?.blockUser()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                blockUser.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            blockUser.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

}