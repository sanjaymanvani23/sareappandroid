package com.example.sare.profile.adapter

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.view.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.UserVideosQuery
import com.example.sare.Utils
import com.example.sare.profile.fragment.ProfileUserFragment
import kotlinx.android.synthetic.main.item_draft_videos_list.view.*
import kotlinx.android.synthetic.main.popup_delete.*

@RequiresApi(Build.VERSION_CODES.M)
class DraftVideoGridAdapter(
    val list: ArrayList<UserVideosQuery.UserVideo>,
    private val context: Context,
    private val userFragment: ProfileUserFragment, val recyclerView: RecyclerView

) :
    RecyclerView.Adapter<DraftVideoGridAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_draft_videos_list, parent, false)
        return ViewHolder(
            v
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, userFragment, list, recyclerView)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: UserVideosQuery.UserVideo,
            context: Context,
            userFragment: ProfileUserFragment,
            mainList: ArrayList<UserVideosQuery.UserVideo>,
            recyclerView: RecyclerView
        ) {
            var imgColorLogo = itemView.imgDraftVideo
            var imgDraftDelete = itemView.imgDraftDelete
            Glide.with(context)
                .load(Utils.PUBLIC_URL + (list.imageName().toString()))
                .error(R.drawable.video_default)
                .placeholder(R.drawable.video_default)
                .into(imgColorLogo)

            imgDraftDelete.setOnClickListener {
                //opens the delete popup
                val dialog = Dialog(userFragment.requireActivity(), R.style.DialogSlideAnim)
                dialog.setContentView(R.layout.popup_delete)
                val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
                lp.copyFrom(dialog.window?.attributes)
                lp.width = WindowManager.LayoutParams.MATCH_PARENT
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                lp.gravity = Gravity.CENTER
                lp.windowAnimations = R.style.DialogAnimation
                dialog.window?.attributes = lp
                dialog.setCancelable(true)
                dialog.btnDelete.setOnClickListener {
                    userFragment.removeVideo(list._id().toString(), 2, adapterPosition)
                    mainList.removeAt(position)
                    recyclerView.adapter?.notifyItemChanged(position)
                    recyclerView.adapter?.notifyItemRangeChanged(position, mainList.size)
                    dialog.dismiss()
                }

                dialog.btnCancel.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()
            }
        }
    }
}