package com.example.sare.profile.adapter

import android.app.Dialog
import android.content.Context
import android.os.Build
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.BookmarkUserVideosQuery
import com.example.sare.R
import com.example.sare.UserVideosQuery
import com.example.sare.Utils
import com.example.sare.profile.fragment.ProfileUserFragment
import com.example.sare.profile.listener.ItemOnclickListener
import kotlinx.android.synthetic.main.item_bookmark_video_list.view.*
import kotlinx.android.synthetic.main.popup_delete.*

@RequiresApi(Build.VERSION_CODES.M)
class BookmarkedVideoGridAdapter(var list: ArrayList<UserVideosQuery.UserVideo>,private val context: Context, private val userFragment: ProfileUserFragment,var itemOnclickListener: ItemOnclickListener,val recyclerView: RecyclerView) :RecyclerView.Adapter<BookmarkedVideoGridAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): ViewHolder {
        val v =LayoutInflater.from(parent.context).inflate(R.layout.item_bookmark_video_list, parent, false)
        return ViewHolder(v,itemOnclickListener)
    }

    //this method is binding the data on the list
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(
            list[position],
            context,
            userFragment,
            position,
            list as ArrayList,
            recyclerView
        )
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,var itemOnclickListener: ItemOnclickListener) : RecyclerView.ViewHolder(itemView) {

        @RequiresApi(Build.VERSION_CODES.M)
        fun bindItems(
            list: UserVideosQuery.UserVideo,
            context: Context,
            userFragment: ProfileUserFragment,
            position: Int,
            mainList: ArrayList<UserVideosQuery.UserVideo>,
            recyclerView: RecyclerView
        ) {
            var imgColorLogo = itemView.imgBookmarkVideo
            var item = itemView.llItem


            Glide.with(context)
                .load(Utils.PUBLIC_URL + (list.imageName().toString()))
                .error(R.drawable.video_default)
                .placeholder(R.drawable.video_default)
                .into(imgColorLogo)
            val likes = itemView.txtBookmarkViews
            val likesCount = itemView.txtBookmarkLikCounts
            val imgBookmarkDelete = itemView.imgBookmarkDelete

            likes.text = list.views().toString()
            if (list.likeCounts().toString() > 0.toString()) {
                likesCount.text = list.likeCounts().toString()
            } else {
                likesCount.text = 0.toString()
            }
            item.setOnClickListener {
                itemOnclickListener.onItemClickListener(position)
            }

            imgBookmarkDelete.setOnClickListener {
                //opens the delete popup
                val dialog = Dialog(userFragment.requireActivity(), R.style.DialogSlideAnim)
                dialog.setContentView(R.layout.popup_delete)
                val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
                lp.copyFrom(dialog.window?.attributes)
                lp.width = WindowManager.LayoutParams.MATCH_PARENT
                lp.height = WindowManager.LayoutParams.WRAP_CONTENT
                lp.gravity = Gravity.CENTER
                lp.windowAnimations = R.style.DialogAnimation
                dialog.window?.attributes = lp
                dialog.setCancelable(true)
                dialog.btnDelete.setOnClickListener {
                    userFragment.removeBookmarkVideo(list._id().toString(), position)
                    mainList.removeAt(position)
                    recyclerView.adapter?.notifyItemChanged(position)
                    recyclerView.adapter?.notifyItemRangeChanged(position, mainList.size)
                    dialog.dismiss()
                }

                dialog.btnCancel.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()

            }


        }
    }

   
    override fun getItemId(position: Int): Long {
        return if (list != null) list[position]._id().toLong() else 0
    }

}