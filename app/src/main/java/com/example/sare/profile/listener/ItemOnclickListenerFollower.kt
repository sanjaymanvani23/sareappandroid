package com.example.sare.profile.listener

interface ItemOnclickListenerFollower {
    fun onItemClickListenerGuest(userId:String,type:String)
    fun onItemClickListenerUser()
    fun onItemClickListenerFollows(position:Int)
}