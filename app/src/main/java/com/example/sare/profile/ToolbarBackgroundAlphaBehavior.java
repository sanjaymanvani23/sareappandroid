package com.example.sare.profile;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.appbar.AppBarLayout;

public class ToolbarBackgroundAlphaBehavior extends AppBarLayout.ScrollingViewBehavior {

    public ToolbarBackgroundAlphaBehavior() {
    }

    public ToolbarBackgroundAlphaBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof AppBarLayout;
    }


    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        if (dependency instanceof AppBarLayout) {
            float ratio = (float) getCurrentScrollValue((Toolbar) child, dependency) / getTotalScrollRange((Toolbar) child, dependency);
            float alpha = 1f - Math.min(1f, Math.max(0f, ratio));

            Log.d("tag", "alpha::::" + alpha);
            int drawableAlpha = (int) (alpha * 255);

            Log.d("tag", "drawableAlpha::::" + drawableAlpha);

            child.getBackground().setAlpha(drawableAlpha);
          //  parent.getStatusBarBackground().setAlpha(drawableAlpha);
        }
        return false;
    }


    private int getCurrentScrollValue(Toolbar child, View dependency) {
        return dependency.getBottom() - child.getTop();
    }

    private float getTotalScrollRange(Toolbar child, View dependency) {
        return ((AppBarLayout) dependency).getTotalScrollRange() - child.getTop();
    }
}
