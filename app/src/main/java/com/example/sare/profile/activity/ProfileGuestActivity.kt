package com.example.sare.profile.activity

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.ChatFragment
import com.example.sare.dashboard.UserVideoFragment
import com.example.sare.dashboard.UserVideosActivity
import com.example.sare.dashboard.hideKeyboard
import com.example.sare.extensions.*
import com.example.sare.profile.adapter.ReportListAdapter
import com.example.sare.profile.adapter.VideoGridAdapter
import com.example.sare.profile.listener.ItemOnclickListener
import com.example.sare.profile.viewmodel.UserVideoListViewModel
import com.example.sare.ui.login.LoginActivity
import com.facebook.login.LoginFragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import iknow.android.utils.KeyboardUtil
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.*
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.view.*
import kotlinx.android.synthetic.main.layout_guest_profile_scroll.view.recyclerView
import kotlinx.android.synthetic.main.layout_my_profile_scroll_new.view.*
import kotlinx.android.synthetic.main.layout_profile_more_option.*
import kotlinx.android.synthetic.main.layout_profile_more_option.view.*
import kotlinx.android.synthetic.main.popup_report.*
import org.json.JSONObject
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@RequiresApi(Build.VERSION_CODES.M) class ProfileGuestActivity : AppCompatActivity(), ItemOnclickListener {
    var data: ArrayList<String> = ArrayList()
    var token: String? = null
    var videoListViewModel: UserVideoListViewModel? = null
    var userId: String? = null
    var type: String? = null
    var showRecyclerView: Boolean = false
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var videoList: MutableList<UserVideosQuery.UserVideo> = mutableListOf()

    private var profileUserVideoList: MutableList<VideoListQuery.Video> = mutableListOf()
    var progressDialog: CustomProgressDialogNew? = null
    var name: String? = null
    var colorCode: String? = null
    var image: String? = null
    var id: String? = null
    var alpha: Float = 0.0f
    lateinit var onBackPressedCallback: OnBackPressedCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_guest)
        getSharedPreferences()
        progressDialog = CustomProgressDialogNew(this)
        intent.extras.run {
            userId = getString("userId", "").toString()
            type = getString("type", "").toString()
        }

        getUserDetails()
        Log.d("tag", "userId::::$userId")
        Log.d("tag", "type::::$type")
        setTheme()
        videoListViewModel = ViewModelProvider(this)[UserVideoListViewModel::class.java]
        setBottomSheet()
        initRecyclerView()
        setObservers()
        getVideoList()
        setListeners()
        collapsing()
    }

    private fun setListeners() {
        btnUpScroll?.setOnClickListener {
            btnUpScroll?.gone()
            recyclerView?.smoothScrollToPosition(0)
            AppFragment_AppBarLayout!!.setExpanded(true, true)
        }
        ll_back?.setOnClickListener {
            onBackPressed()
        }
        imgBack?.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        //        startActivity(Intent(this, MainActivity::class.java).putExtra("type",2))
    }

    private fun setObservers() {
        getVideoListObserver()
    }


    private fun collapsing() {
        val mAppBarLayout = findViewById(R.id.AppFragment_AppBarLayout) as AppBarLayout
        val mToolbar = findViewById(R.id.AppFragment_Toolbar) as Toolbar
        mAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset -> /* mToolbar.setBackgroundColor(
                        changeAlpha(
                            resources.getColor(R.color.pureWhite),
                            Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()
                        )
                    )*/

            mToolbar.alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()

            alpha = Math.abs(verticalOffset * 1.0f) / appBarLayout.getTotalScrollRange()
            Log.d("tag", "alpha::::::$alpha")
            Log.d("tag", "videoList.size::::::${videoList.size}")
            if (alpha == 1f) {
                Log.d("tag", "1f::::::")
                mToolbar.visibility = View.VISIBLE
                btnUpScroll?.visible()
                imgMore1?.isClickable = true
                imgBack?.isClickable = true
                ll_back?.isClickable = false
                imgMore?.isClickable = false

            } else if (alpha > 0.5f) {
                mToolbar.visibility = View.VISIBLE

            } else if (alpha == 0f) {
                Log.d("tag", "else::::")
                mToolbar.visibility = View.GONE
                btnUpScroll?.gone()
                imgMore1?.isClickable = false
                imgBack?.isClickable = false
                ll_back?.isClickable = true
                imgMore?.isClickable = true
            }

        })


    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.transparent)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.transparent)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }


    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")

    }

    private fun getVideoList() {
        runOnUiThread {
            progressDialog?.show()
        }
        Log.d("tag", "Pagination Current page : " + currentPage)
        videoListViewModel?.getUserVideoList(
            this, userId.toString(), currentPage * Utils.PAGE_SIZE)
    }

    override fun onResume() {
        super.onResume()
        videoList.clear()
        isLoading = false
        isLastPage = false
        currentPage = 0
    }

    private fun getVideoListObserver() {
        videoListViewModel?.videoArrayList?.observe(this, Observer {
            runOnUiThread {
                progressDialog?.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    runOnUiThread {
                        progressDialog?.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getUserVideoList(
                                this, userId.toString(), currentPage * PAGE_SIZE)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.userVideoPublishedList -> {
                    runOnUiThread {
                        progressDialog?.hide()
                    }
                    isLoading = false
                    Log.d("tag", "response::" + it.response)
                    if (it.response != null) {

                        val gson = Gson()
                        val userInfoListJsonString = gson.toJson(it.response)
                        val myType = object : TypeToken<List<VideoListQuery.Video>>() {}.type
                        val userVideoList = gson.fromJson<List<VideoListQuery.Video>>(
                            userInfoListJsonString, myType)

                        profileUserVideoList.addAll(userVideoList)

                        videoList.addAll(it.response)

                        recyclerView?.adapter?.notifyDataSetChanged()
                        if (it.response.size < PAGE_SIZE) {
                            isLastPage = true
                        }


                        if (recyclerView?.adapter?.itemCount == 0) {
                            txtNoData?.visible()
                            relScroll?.gone()
                            disableScroll(AppFragment_AppBarLayout!!)
                        } else {
                            enableScroll(AppFragment_CollapsingToolbarLayout!!)
                            txtNoData?.gone()
                            relScroll?.visible()

                            /* txtNoData?.setOnClickListener {
                                 getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

                             }*/
                        }

                    } else {
                        runOnUiThread {
                            customToast(getString(R.string.api_error_message), this, 0)
                        }
                    }
                }
            }

        })
    }

    private fun enableScroll(collapsingToolbarLayout: CollapsingToolbarLayout) {
        Log.d("tag", "enableScroll")

        val params = collapsingToolbarLayout.getLayoutParams() as AppBarLayout.LayoutParams
        params.scrollFlags = (AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED)
        collapsingToolbarLayout.setLayoutParams(params)
    }

    private fun disableScroll(appBarLayout: AppBarLayout) {
        Log.d("tag", "disableScroll")
        /* val params =
             collapsingToolbarLayout.layoutParams as AppBarLayout.LayoutParams
         params.scrollFlags = 0
 //        collapsingToolbarLayout.isNestedScrollingEnabled = true
         collapsingToolbarLayout.layoutParams = params*/

        val params = appBarLayout.layoutParams as CoordinatorLayout.LayoutParams

        if (params.behavior == null) params.behavior = AppBarLayout.Behavior()
        val behaviour = params.behavior as AppBarLayout.Behavior
        behaviour.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                return false
            }
        })
    }


    private fun initRecyclerView() {

        recyclerView?.apply {
            layoutManager = GridLayoutManager(
                recyclerView?.context, 3)

            addOnScrollListener(object : PaginationListener(this.layoutManager as GridLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    getVideoList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }


            })
            adapter = VideoGridAdapter(
                videoList, context, this@ProfileGuestActivity)

        }


    }

    private fun setBottomSheet() {
        val llBottomSheet = bottom_sheet_options

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        imgMore1?.setOnClickListener {
            overlay_view_guest?.visible()
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        imgMore?.setOnClickListener {
            overlay_view_guest?.visible()
            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }

        overlay_view_guest?.setOnClickListener {

            overlay_view_guest?.gone()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        overlay_view_report_popup?.setOnClickListener {
            KeyboardUtil.hideKeyboard(this)
            overlay_view_report_popup?.gone()
            layout_popup_report?.gone()
            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }
        bottom_sheet_options?.txtReport?.setOnClickListener {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            overlay_view_report_popup?.visible()
            layout_popup_report?.visible()
            ll_select_report_reason?.setOnClickListener {
                rvReportList?.visible()
                reportList()
                if (showRecyclerView == true) {

                    rvReportList?.gone()
                    reportList()
                    showRecyclerView = false

                } else {
                    rvReportList?.visible()
                    showRecyclerView = true
                }
            }


            btnReportSend?.setOnClickListener {
                if (txtReportReason?.text!!.isEmpty() || txtTitle?.text!!.isEmpty()) customToast(
                    "Please select report reason!!",
                    this,
                    0)
            }

        }


        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view_guest?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")


                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view_guest?.visible()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view_guest?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")


                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view_guest?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")


                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }


    private fun reportList() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        videoListViewModel?.getReportList(this)
        videoListViewModel?.reportArrayList?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            videoListViewModel?.getReportList(this)
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.getReportList -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)

                    initReportRecyclerView(it.response)
                }

            }
        })
    }


    private fun initReportRecyclerView(list: List<ReportListQuery.ReportType>) {
        rvReportList?.apply {
            layoutManager = LinearLayoutManager(
                rvReportList?.context, RecyclerView.VERTICAL, false)
            adapter = ReportListAdapter(list, context)

            (adapter as ReportListAdapter).onItemClick = { data ->
                rvReportList?.gone()

                // do something with your item
                Log.d("tag", data.reportName().toString())
                txtReportReason?.text = data.reportName().toString()


                btnReportSend?.setOnClickListener {
                    if (txtReportReason?.text!!.isEmpty() && txtTitle?.text!!.isEmpty()) {
                        customToast(
                            "Report cant be submitted. Please select report reason!!", this@ProfileGuestActivity, 0)
                    } else if (txtReportReason?.text!!.isEmpty()) {
                        customToast("Please select report reason!!", this@ProfileGuestActivity, 0)
                    } else if (txtTitle?.text!!.isEmpty()) {
                        customToast("Please write report title!!", this@ProfileGuestActivity, 0)
                    } else if (txtTypeSomething?.text!!.isEmpty()) {
                        customToast(
                            "Please type something about report!!", this@ProfileGuestActivity, 0)
                    } else {
                        val apolloClient = ApolloClient.setupApollo(token.toString())


                        apolloClient.mutate(
                            StoreReportMutation(
                                userId.toString(),
                                data.reportType()!!,
                                txtTitle?.text.toString(),
                                txtReportReason?.text.toString()))
                            .enqueue(object : ApolloCall.Callback<StoreReportMutation.Data>() {
                                override fun onFailure(e: ApolloException) {
                                    e.printStackTrace()
                                    Log.d("tag", "Error::" + e.message)
                                    customToastMain(e.message.toString(), 0)
                                    MyApplication.mainActivity.logoutUser()
                                    openNewActivity(LoginActivity::class.java)
                                }

                                override fun onResponse(response: Response<StoreReportMutation.Data>) {
                                    Log.d("tag", "response::$response")
                                    if (response.data?.storeReportProfile() != null) {
                                        runOnUiThread {
                                            customToast(
                                                "Report Submitted!!", this@ProfileGuestActivity, 1)
                                            txtTitle?.setText("")
                                            txtReportReason?.text = ""
                                            txtTypeSomething?.setText("")
                                            hideKeyboard(this@ProfileGuestActivity)

                                            overlay_view_report_popup?.gone()
                                            layout_popup_report?.gone()
                                            val mainLayout = layout_popup_report

                                        }
                                    } else {
                                        runOnUiThread {
                                            val error = response.errors?.get(0)?.message
                                            Log.d("tag", "Data Error: $error")
                                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")

                                            if (errorCode?.toString()?.equals("401") == true) {
                                                customToast(
                                                    error.toString(), this@ProfileGuestActivity, 0)
                                                MyApplication.mainActivity.logoutUser()
                                                openNewActivity(LoginActivity::class.java)
                                            } else if (errorCode?.toString()?.equals("403") == true) {
                                                customToast(
                                                    error.toString(), this@ProfileGuestActivity, 0)
                                                MyApplication.mainActivity.logoutUser()
                                                openNewActivity(LoginActivity::class.java)
                                            } else {
                                                customToast(
                                                    error.toString(), this@ProfileGuestActivity, 0)
                                            }
                                            txtTitle?.setText("")
                                            txtReportReason?.text = ""
                                            txtTypeSomething?.setText("")
                                            hideKeyboard(this@ProfileGuestActivity)


                                            overlay_view_report_popup?.gone()
                                            layout_popup_report?.gone()
                                        }
                                    }
                                }

                            })
                    }
                }
            }

        }
    }


    private fun getUserDetails() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(userId.toString()))
            .enqueue(object : ApolloCall.Callback<UserDetailsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    /*Toast.makeText(this, e.message.toString(), Toast.LENGTH_SHORT)
                        .show()*/
                    runOnUiThread {
                        progressDialog.hide()
                    }

                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            getUserDetails()
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    //                    runBlocking {
                    Log.d("tag", "ResponseData::$response")
                    runOnUiThread {
                        if (response.data()?.user() != null) {
                            setData(response)
                        } else {
                            val error = response.errors?.get(0)?.message
                            Log.d("tag", "Data Error: $error")
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            if (errorCode?.toString()?.equals("401") == true) {
                                customToast(error.toString(), this@ProfileGuestActivity, 0)
                               MyApplication.mainActivity.logoutUser()
                                openNewActivity(LoginActivity::class.java)
                            } else if (errorCode?.toString()?.equals("403") == true) {
                                customToast(error.toString(), this@ProfileGuestActivity, 0)
                               MyApplication.mainActivity.logoutUser()
                                openNewActivity(LoginActivity::class.java)
                            } else {
                                customToast(error.toString(), this@ProfileGuestActivity, 0)
                            }
                        }

                    }
                }
            })
    }

    @SuppressLint("SetTextI18n") private fun setData(response: Response<UserDetailsQuery.Data>) {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)


        relative_layout_followers?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWERS")

            /* startActivity(Intent(this, FollowersFollowingActivity::class.java)
                 .putExtra("userId",response.data!!.user()._id())
                 .putExtra("name",response.data!!.user().name())
                 .putExtra("colorCode",response.data!!.user().colorCode())
                 .putExtra("image",response.data!!.user().image())
                 .putExtra("type","FOLLOWERS")
             )*/
            openActivity(FollowersFollowingActivity::class.java, bundle)
            overridePendingTransition(0, 0)
        }



        relative_layout_followings?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image(),
                "type" to "FOLLOWINGS")

            openActivity(FollowersFollowingActivity::class.java, bundle)
            overridePendingTransition(0, 0)
        }


        imgProfilePic?.loadUrl(PUBLIC_URL + response.data?.user()?.image()!!)
        imgProfilePicUser?.setBorder(response.data?.user()?.colorCode() ?: Utils.COLOR_CODE)

        imgProfile?.loadUrl(PUBLIC_URL + response.data?.user()?.image()!!)
        imgProfileBorder?.setBorder(response.data?.user()?.colorCode() ?: Utils.COLOR_CODE)

        val textToCopy = "https://sare.in/graphql/" + response.data!!.user()._id()

        txtName?.text = response.data!!.user().name().toString()
        txtName1?.text = response.data!!.user().name().toString()
        txtId?.text = "@" + response.data!!.user().username().toString()
        if (response.data!!.user().likeCounts().toString() < 0.toString()) {
            txtLikes?.text = 0.toString() + " Like"
        } else {
            txtLikes?.text = response.data!!.user().likeCounts().toString() + " Likes"
        }

        if (response.data!!.user().followingCounts()!! > 0) {
            txtFollowingCount?.text = response.data!!.user().followingCounts().toString()
        } else {
            txtFollowingCount?.text = 0.toString()
        }
        val imageView = header_cover_image
        Glide.with(this).load(PUBLIC_URL + (response.data?.user()?.coverImage().toString()))
            // .load(Utils.COVER_IMAGE_URL + response.data?.user()?.coverImage())
            .error(R.drawable.cover_default).into(imageView!!)
        if (response.data!!.user().postsCounts()!! > 0) {
            txtPostCounts?.text = response.data!!.user().postsCounts().toString()
        } else {
            txtPostCounts?.text = "0"
        }


        txtJoinedDate?.text = "Joined On " + getParsedDate(response.data!!.user().createdAt().toString())

        if (response.data!!.user().followerCounts()!! > 0) {
            txtFollowersCount?.text = response.data!!.user().followerCounts().toString()
        } else {
            txtFollowersCount?.text = 0.toString()
        }
        //        if (response.data!!.user().s)

        if (response.data!!.user().blocked() == true) {
            bottom_sheet_options?.txtBlock?.text = "Unblock"

        } else {
            bottom_sheet_options?.txtBlock?.text = "Block"

        }


        if (response.data!!.user().followed() == true) {
            button_follow?.text = "Following"
        } else {
            button_follow?.text = "Follow"
        }
        button_follow?.setOnClickListener {
            button_follow?.isEnabled = false
            if (button_follow?.text == "Following") {
                button_follow?.text == "Follow"
                unFollowUser(response.data!!.user()._id())
                /*if (response.data!!.user().followerCounts()!! > 0) {
                    txtFollowersCount?.text =
                        (txtFollowersCount?.text.toString().toInt() - 1).toString()
                } else {
                    txtFollowersCount?.text = 0.toString()
                }*/
                if (response.data!!.user().followerCounts()!! > 0) {
                    txtFollowersCount?.text = (txtFollowersCount?.text.toString().toInt() - 1).toString()
                } else {
                    txtFollowersCount?.text = 0.toString()
                }

            } else {
                button_follow?.text == "Follow"
                followUser(response.data!!.user()._id())
/*
                if (response.data!!.user().followingCounts()!! > 0) {
                    txtFollowersCount?.text = (response.data!!.user().followingCounts()!! + 1).toString()
                } else {
                    txtFollowersCount?.text = 0.toString()
                }*/

                if (response.data!!.user().followerCounts()!! > 0) {
                    txtFollowersCount?.text = (txtFollowersCount?.text.toString().toInt() + 1).toString()
                } else {
                    txtFollowersCount?.text = 0.toString()
                }

            }
        }

        /*  button_following?.setOnClickListener {
              unFollowUser(response.data!!.user()._id())
              if (response.data!!.user().followerCounts()!! > 0) {
                  txtFollowersCount?.text =
                      (txtFollowersCount?.text.toString().toInt() - 1).toString()
              } else {
                  txtFollowersCount?.text = 0.toString()
              }

              button_follow?.visible()
          }*/

        if (!response.data!!.user().bio().isNullOrEmpty()) {
            quote?.visible()
            quote?.text = response.data!!.user().bio().toString()
        } else {
            quote?.gone()
        }

        //        button_chat?.setOnClickListener {
        //
        //        }

        bottom_sheet_options?.txtCopyProfileLink?.setOnClickListener {
            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clipData = ClipData.newPlainText("text", textToCopy)
            clipboardManager.setPrimaryClip(clipData)
            Log.d("tag", "clip data::$clipData")
            customToast("Link copied to clipboard!!", this, 1)
        }
        bottom_sheet_options?.txtShareProfile?.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_SUBJECT, "Share Profile Link.!!")
            share.putExtra(Intent.EXTRA_TEXT, textToCopy)
            startActivity(Intent.createChooser(share, "Share link!"))

        }

        bottom_sheet_options?.txtBlock?.setOnClickListener {
            if (response.data!!.user().blocked() == true || bottom_sheet_options?.txtBlock?.text == "Unblock") {
                unBlockUser(response.data!!.user()._id().toString())
            } else {
                blockUser(response.data!!.user()._id().toString())
            }
        }

        button_chat?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.data!!.user()._id(),
                "name" to response.data!!.user().name(),
                "colorCode" to response.data!!.user().colorCode(),
                "image" to response.data!!.user().image())
            val chatFragment = ChatFragment()
            findNavController(chatFragment)
        }


    }

    private fun getParsedDate(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        var parsedDate: String? = null
        parsedDate = try {
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("MMMM yyyy")
            outputFormatter1.format(date1)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        return parsedDate.toString()
    }


    private fun blockUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::$token")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            BlockUserMutation(
                id))

            .enqueue(object : ApolloCall.Callback<BlockUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            blockUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<BlockUserMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data != null && response.data?.blockUser()?.status()!!) {
                        runOnUiThread {
                            customToast("User Blocked!", this@ProfileGuestActivity, 1)
                        }
                        bottom_sheet_options?.txtBlock?.text = "Unblock"


                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                        }
                    }

                }
            })
    }


    private fun unBlockUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        Log.d("tag", "token::$token")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            UnBlockUserMutation(
                id))

            .enqueue(object : ApolloCall.Callback<UnBlockUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            unBlockUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UnBlockUserMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data != null && response.data?.unblockUser()?.status()!!) {
                        runOnUiThread {
                            customToast("User Unblocked!", this@ProfileGuestActivity, 1)
                        }
                        bottom_sheet_options?.txtBlock?.text = "Block"


                    } else {
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                        }
                    }

                }
            })
    }


    private fun followUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        Log.d("tag", "token::$token")
        Log.d("tag", "id::$id")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            FollowUserMutation(
                id))

            .enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    customToast(e.message.toString(), this@ProfileGuestActivity, 0)
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            followUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<FollowUserMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data?.follow()?.status() == true) {
                        Log.d("tag", "response follow:::$response")
                        runOnUiThread {
                            button_follow?.isEnabled = true
                            button_follow?.text = "Following"
                        }


                    } else {
                        button_follow?.isEnabled = true
                        val error = response.errors?.get(0)?.message
                        Log.d("tag", "Data Error: $error")
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                        }
                    }

                }
            })

    }


    private fun unFollowUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            UnFollowUserMutation(
                id)).enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar.make(
                            findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            unFollowUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response unfollow:::$response")
                    if (response.data?.unfollow()?.status() == true) {
                        runOnUiThread {
                            button_follow?.isEnabled = true
                            button_follow?.text = "Follow"
                            /*   txtFollowersCount?.text =
                                (txtFollowersCount?.text.toString().toInt() + 1).toString()*/

                        }


                    } else {
                        button_follow?.isEnabled = true
                        val error = response.errors?.get(0)?.message
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                           MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else {
                            customToast(error.toString(), this@ProfileGuestActivity, 0)
                        }
                    }

                }
            })
    }

    /* override fun onItemClickListener(position: Int) {
         val gson = Gson()
         val userInfoListJsonString = gson.toJson(profileUserVideoList)
         var bundle = bundleOf(
             "navigationFlagProfile" to "ProfilePublic",
             "videoList" to userInfoListJsonString,
             "position" to position,
             "isVideoCurrentPage" to currentPage + 1
         )

         val userVideoFragment = UserVideoFragment()
         findNavController(userVideoFragment)

     }*/
    override fun onItemClickListener(position: Int) {
        val gson = Gson()
        val userInfoListJsonString = gson.toJson(profileUserVideoList)
        var bundle = bundleOf(
            "navigationFlagProfile" to "ProfilePublic",
            "videoList" to userInfoListJsonString,
            "position" to position,
            "isVideoCurrentPage" to currentPage + 1)
        openActivity(UserVideosActivity::class.java, bundle)
        finish()
    }

}