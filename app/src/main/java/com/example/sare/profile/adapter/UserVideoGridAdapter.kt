package com.example.sare.profile.adapter


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.BookmarkUserVideosQuery
import com.example.sare.R
import com.example.sare.UserVideosQuery
import kotlinx.android.synthetic.main.item_bookmark_video_list.view.*
import kotlinx.android.synthetic.main.item_published_videos_list.view.*
import kotlinx.android.synthetic.main.item_draft_videos_list.view.*


class UserVideoGridAdapter(
    val type: Int,
    val list: List<UserVideosQuery.UserVideo>,
    val bookmarkList: List<BookmarkUserVideosQuery.BookMarkVideo>,
    val draftList: List<UserVideosQuery.UserVideo>,
    private val context: Context
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        val PUBLISHED_VIDEO = 1
        val BOOKMARK_VIDEO = 2
        val DRAFT_VIDEO = 3

    }

    //this method is returning the view for each item in the list
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): UserVideoGridAdapter.ViewHolder {
//        val v =
//            LayoutInflater.from(parent.context).inflate(R.layout.item_video_list, parent, false)
//        return ViewHolder(v)
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v: View
        when (viewType) {
            PUBLISHED_VIDEO -> {
                v = LayoutInflater.from(context)
                    .inflate(R.layout.item_published_videos_list, parent, false)
                return PublishedTypeViewHolder(
                    v
                )
            }
            BOOKMARK_VIDEO -> {
                v = LayoutInflater.from(context)
                    .inflate(R.layout.item_bookmark_video_list, parent, false)
                return BookmarkTypeViewHolder(
                    v
                )
            }
            DRAFT_VIDEO -> {
                v = LayoutInflater.from(context)
                    .inflate(R.layout.item_draft_videos_list, parent, false)
                return DraftTypeViewHolder(
                    v
                )
            }
            else -> {
                v = LayoutInflater.from(context)
                    .inflate(R.layout.item_published_videos_list, parent, false)
                return PublishedTypeViewHolder(
                    v
                )

            }
        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        holder.bindItems(list[position])
        Log.d("tag", "type::" + type)
        Log.d("tag", "PUBLISHED_VIDEO::" + PUBLISHED_VIDEO)
//        if (holder === PUBLISHED_VIDEO) {
//            (holder as PublishedTypeViewHolder).bindItems(list[position])
//        } else if (type === BOOKMARK_VIDEO) {
//            (holder as BookmarkTypeViewHolder).bindItems(bookmarkList[position])
//        } else if (type === DRAFT_VIDEO) {
//            (holder as DraftTypeViewHolder).bindItems(draftList[position])
//        } else {
//            (holder as PublishedTypeViewHolder).bindItems(list[position])
//
//        }

        when (holder) {
            is PublishedTypeViewHolder -> holder.bindItems(list[position])
            is BookmarkTypeViewHolder -> holder.bindItems(bookmarkList[position])
            is DraftTypeViewHolder -> holder.bindItems(draftList[position])
            else -> (holder as PublishedTypeViewHolder).bindItems(list[position])

        }
    }
//
//    override fun getItemViewType(position: Int): Int {
//
//        return list[position].viewType
//    }


    //this method is giving the size of the list
    override fun getItemCount(): Int {
        when {
            type === PUBLISHED_VIDEO -> {
                list.size
            }
            type === BOOKMARK_VIDEO -> {
                bookmarkList.size
            }
            type === DRAFT_VIDEO -> {
                draftList.size
            }
        }
        return draftList.size
    }

    override fun getItemViewType(position: Int): Int {
        Log.d("tag", "Get Item View Type Running")

//        val type = return when (position) {
//            1 -> { // put your condition, according to your requirements
//                PUBLISHED_VIDEO;
//            }
//            2 -> {
//                BOOKMARK_VIDEO
//            }
//            3 -> {
//                DRAFT_VIDEO
//            }
//            //        if(position <)
//            else -> PUBLISHED_VIDEO
//
//        }
//        return type

        when (position) {
            1 -> { // put your condition, according to your requirements
                PUBLISHED_VIDEO;
            }
            2 -> {
                BOOKMARK_VIDEO
            }
            3 -> {
                DRAFT_VIDEO
            }
        }
        return -1

    }

    //the class is hodling the list view
//    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
//
//        fun bindItems(list: UserVideosQuery.UserVideo) {
//            var imgColorLogo = itemView.imgVideo
//
//            val likes = itemView.txtViews
//
//            likes.text = list.views().toString() + " Views"
//
//        }
//    }

    class PublishedTypeViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bindItems(list: UserVideosQuery.UserVideo) {
            var imgColorLogo = itemView.imgPublishedVideo

            val likes = itemView.txtPublishedViews

            likes.text = list.views().toString() + " Views"

        }
    }

    class BookmarkTypeViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bindItems(list: BookmarkUserVideosQuery.BookMarkVideo) {
            var imgColorLogo = itemView.imgBookmarkVideo

            val likes = itemView.txtBookmarkViews

            likes.text = list.video()?.views().toString() + " Views"

        }
    }

    class DraftTypeViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        fun bindItems(list: UserVideosQuery.UserVideo) {
            var imgColorLogo = itemView.imgDraftVideo


        }
    }
}
