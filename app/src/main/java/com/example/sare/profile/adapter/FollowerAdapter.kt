package com.example.sare.profile.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.extensions.*
import com.example.sare.profile.activity.ProfileGuestActivity
import com.example.sare.profile.listener.ItemOnclickListenerFollower
import kotlinx.android.synthetic.main.item_follower_following_list.view.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M) class FollowerAdapter(var list: MutableList<FollowerListQuery.Follower>,
    private val context: Context,
    val userId: String,
    var listener: ItemOnclickListenerFollower) : RecyclerView.Adapter<FollowerAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FollowerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_follower_following_list, parent, false)
        return ViewHolder(v, listener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context, userId)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        //        return list.size
        if (list == null) {
            return 0
        }
        return list.size
    }

    override fun getItemId(position: Int): Long {
        return if (list != null) list[position].user()!!._id().toLong() else 0
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View, var listener: ItemOnclickListenerFollower) : RecyclerView.ViewHolder(itemView) {

        var relDetails = itemView.relDetails
        var cardView = itemView.smallView


        fun bindItems(item: FollowerListQuery.Follower, context: Context, userId: String) {

            val name = itemView.txtName
            val id = itemView.txtId
            val imgCard = itemView.imgCard
            val likes = itemView.txtLikes
            val button = itemView.button_following
            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val txtVideoCounts = itemView.txtVideoCounts
            val relSingleItem = itemView.relDetails
            val relDelete = itemView.relCard
            val relCard = itemView.relCard
            val main_layout = itemView.main_layout
            val cardButton = itemView.cardButton

            if (item.user()?.likeCounts().toString() < 0.toString()) {
                likes.text = 0.toString()
            } else {
                likes.text = item.user()?.likeCounts().toString()
            }

            if (item.user()?.postsCounts().toString() < 0.toString()) {
                txtVideoCounts.text = 0.toString()
            } else {
                txtVideoCounts.text = item.user()?.postsCounts().toString()
            }
            name.text = item.user()?.name().toString()
            id.text = "@" + item.user()?.username().toString()
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            var userIdMain: String? = null
            val token = preferences.getString("TOKEN", null)
            val user = preferences.getString("USER", null)
            if (user != null) {
                val mainObject = JSONObject(user)
                userIdMain = mainObject.getString("_id")
            }

            if (item.user()?._id() == userIdMain) {
                button.gone()
                cardButton.gone()
            } else {
                button.visible()
                cardButton.visible()
            }

            if (userId == userIdMain) {
                relDelete.visibility = View.VISIBLE
                relCard.visibility = View.VISIBLE
            } else {
                relDelete.visibility = View.GONE
                relCard.visibility = View.GONE

            }

            main_layout.setOnClickListener {
                if (item.user()?._id() == userIdMain) {
                    listener.onItemClickListenerUser()
                } else {
                    listener.onItemClickListenerGuest(item.user()?._id().toString(), "FOLLOWERS")
                }
            }

            imgProfile.setOnClickListener {
                if (item.user()?._id() == userIdMain) {
                    listener.onItemClickListenerUser()
                } else {
                    listener.onItemClickListenerGuest(item.user()?._id().toString(), "FOLLOWERS")
                }
            }

            val urlString = Utils.PUBLIC_URL + (item.user()?.image().toString())

            val colorCode = item.user()?.colorCode()

            imgProfile?.loadUrl(urlString)
            imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

            val apolloClientNew = ApolloClient.setupApollo(token.toString())

            if (item.user()?.followed() == true) {
                button.text = "Following"
                button.setTextColor(Color.parseColor("#000000"))

            } else {
                button.text = "Follow"
                button.setTextColor(Color.parseColor("#FF005F"))

            }

            button.setOnClickListener {
                if (button.text == "Following") {

                    apolloClientNew.mutate(
                        UnFollowUserMutation(
                            item.user()!!._id())).enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                        override fun onFailure(e: ApolloException) {
                            Log.d("tag", "Error Data : ${e.message}")
                        }

                        override fun onResponse(response: Response<UnFollowUserMutation.Data>) {

                            context.runCatching {
                                Log.d("tag", "res following adapter::${response}")
                                if (response.data?.unfollow()?.status() == true) {

                                    button.text = "Follow"
                                    button.setTextColor(Color.parseColor("#FF005F"))
                                    customToast(
                                        "User Unfollowed Successfully", context as MainActivity, 1)

                                } else {
                                    val error = response.errors?.get(0)?.customAttributes?.get("status")
                                    Log.d("tag", "Data Error: $error")
                                    if (error?.toString()?.equals("401") == true) {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), context as MainActivity, 0)

                                    } else {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), context as MainActivity, 0)

                                    }
                                }

                            }
                        }
                    })
                } else {
                    apolloClientNew.mutate(
                        FollowUserMutation(
                            item.user()!!._id())).enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                        override fun onFailure(e: ApolloException) {
                            Log.d("tag", "Error Data : ${e.message}")
                        }

                        override fun onResponse(response: Response<FollowUserMutation.Data>) {
                            context.runCatching {
                                if (response.data?.follow() != null && response.data?.follow()?.status()!!) {

                                    button.text = "Following"
                                    button.setTextColor(Color.parseColor("#000000"))

                                    customToast(
                                        "User Followed Successfully", context as MainActivity, 1)

                                } else {

                                    val error = response.errors?.get(0)?.customAttributes?.get("status")
                                    Log.d("tag", "Data Error:1 $error")

                                    if (error?.toString()?.equals("401") == true) {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), context as MainActivity, 0)

                                    } else {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(), context as MainActivity, 0)
                                    }
                                }
                            }
                        }


                    })
                }

            }

        }

    }


    fun filterList(filteredNames: List<FollowerListQuery.Follower>): List<FollowerListQuery.Follower>? {
        this.list = filteredNames as MutableList<FollowerListQuery.Follower>
        notifyDataSetChanged()
        return filteredNames
    }


    fun deleteItem(post: List<FollowerListQuery.Follower>, position: Int) {
        val postId = post[position].user()?._id()


        if (list[position].user()?._id() === postId) {

            this.list.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, itemCount)

        }


    }

}