package com.example.sare.profile.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.AmazonUtil
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.UserVideosQuery
import com.example.sare.profile.fragment.ProfileGuestFragment
import com.example.sare.profile.listener.ItemOnclickListener
import kotlinx.android.synthetic.main.item_video_list.view.*

class VideoGridAdapter(val list: List<UserVideosQuery.UserVideo>, private val context: Context,var itemOnclickListener: ItemOnclickListener) :
    RecyclerView.Adapter<VideoGridAdapter.ViewHolder>() {

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_video_list, parent, false)
        return ViewHolder(v,itemOnclickListener)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View,var itemOnclickListener: ItemOnclickListener) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(list: UserVideosQuery.UserVideo?, context: Context) {
            var imgColorLogo = itemView.imgVideo
            var ll_video_item = itemView.ll_video_item

            ll_video_item.setOnClickListener {
                /*Log.d("tag","list::$list")
                val bundle = bundleOf("videoId" to list?._id(),
                "userId" to list?._id())
                if (profileGuestFragment.findNavController().currentDestination?.id == R.id.profileGuestFragment) {
                    profileGuestFragment.findNavController()
                        .navigate(R.id.action_profileGuestFragment_to_homeFragment2, bundle)
                }*/
                itemOnclickListener.onItemClickListener(adapterPosition)
            }

           // Log.d("tag","video cover images:::::::::${AmazonUtil.getSignedUrl(list?.imageName().toString())}")
            Glide.with(context)
                .load(Utils.PUBLIC_URL +(list?.imageName().toString()))
                .error(R.drawable.video_default)
                .placeholder(R.drawable.video_default)
                .into(imgColorLogo)

            val likes = itemView.txtViews
            val txtLikeCounts = itemView.txtLikeCounts
            if (list?.likeCounts().toString() > 0.toString()) {
                txtLikeCounts.text = list?.likeCounts().toString()
            } else {
                txtLikeCounts.text = 0.toString()
            }
            likes.text = list?.views().toString()

        }
    }
}