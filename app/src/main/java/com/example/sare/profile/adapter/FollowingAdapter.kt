package com.example.sare.profile.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.*
import com.example.sare.profile.activity.FollowersFollowingActivity
import com.example.sare.profile.listener.ItemOnclickListenerFollower
import kotlinx.android.synthetic.main.item_follower_following_list.view.*
import org.json.JSONObject

class FollowingAdapter(private var list: MutableList<FollowingListQuery.Following>,private val context: Context, val userIdMain: String, var listener:
ItemOnclickListenerFollower) : RecyclerView.Adapter<FollowingAdapter.ViewHolder>() {
    var token: String
   // val list: ArrayList<FollowingListQuery.Following> = ArrayList()

    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_follower_following_list, parent, false)
        return ViewHolder(v, listener)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(list[position], context, userIdMain)
        holder.setIsRecyclable(false)
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        holder.button.setOnClickListener {
            if (holder.button.text == "Following") {

                apolloClientNew.mutate(
                    UnFollowUserMutation(
                        list[position].user()._id()))

                    .enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                        override fun onFailure(e: ApolloException) {
                            Log.d("tag", "Error Data : ${e.message}")
                        }

                        override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                            context.runCatching {
                                Log.d("tag", "res following adapter::${response.data?.unfollow()?.status()}")
                                if (response.data?.unfollow()?.status() == true) {
                                    customToast("User Unfollowed Successfully",context as FollowersFollowingActivity,1)
                                    listener.onItemClickListenerFollows(position)

                                } else {
                                    val error = response.errors?.get(0)?.customAttributes?.get("status")
                                    Log.d("tag", "Data Error: $error")
                                    if (error?.toString()?.equals("401") == true) {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(),
                                            context as FollowersFollowingActivity,
                                            0)

                                    } else {
                                        customToast(
                                            response.errors?.get(0)?.message.toString(),
                                            context as FollowersFollowingActivity,
                                            0)

                                    }
                                }
                            }

                        }
                    })
            }
        }

    }

    override fun getItemCount(): Int {
        Log.e("Tag size", "${list.size}")
        return list.size
    }

    fun deleteItem(activity: FollowersFollowingActivity, position: Int) {

        activity.runOnUiThread {
            this.list.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, itemCount)
            notifyDataSetChanged()
        }

    }

    /*fun addItems(list: ArrayList<FollowingListQuery.Following>) {
        this.list.addAll(list)
        notifyDataSetChanged()
        Log.e("Tag sizsdsd", "${list.size}")
    }*/

    //the class is hodling the list view
    class ViewHolder(itemView: View, var listener: ItemOnclickListenerFollower) : RecyclerView.ViewHolder(itemView) {

        val name = itemView.txtName
        val id = itemView.txtId
        val likes = itemView.txtLikes
        val imgProfileBorder = itemView.imgProfileBorder
        val imgProfile = itemView.imgProfile
        val relSingleItem = itemView.relSingleItem
        val txtVideoCounts = itemView.txtVideoCounts
        val cardview = itemView.smallView
        val main_layout = itemView.main_layout

        var button = itemView.button_following
        var cardButton = itemView.cardButton
        fun bindItems(item: FollowingListQuery.Following, context: Context, userIdMain: String) {
            val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
            StrictMode.setThreadPolicy(policy)

            cardview.visibility = View.GONE
            if (item.user().likeCounts().toString() < 0.toString()) {
                likes.text = 0.toString()
            } else {
                likes.text = item.user().likeCounts().toString()
            }

            if (item.user().postsCounts().toString() < 0.toString()) {
                txtVideoCounts.text = 0.toString()
            } else {
                txtVideoCounts.text = item.user().postsCounts().toString()
            }
            name.text = item.user().name().toString()
            id.text = "@" + item.user().username().toString()

            imgProfile?.loadUrl(Utils.PUBLIC_URL + item.user().image().toString())
            imgProfileBorder?.setBorder(item.user().colorCode() ?: Utils.COLOR_CODE)
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
            var userId: String? = null
            val token = preferences.getString("TOKEN", null)
            val user = preferences.getString("USER", null)
            if (user != null) {
                val mainObject = JSONObject(user)
                userId = mainObject.getString("_id")
            }
            if (item.user()._id() == userId) {
                button.gone()
                cardButton.gone()
            } else {
                button.visible()
                cardButton.visible()
            }
            imgProfile.setOnClickListener {

                if (item.user()._id() == userId) {
                    listener.onItemClickListenerUser()

                } else {
                    listener.onItemClickListenerGuest(item.user()._id().toString(), "FOLLOWINGS")
                }
            }


            main_layout.setOnClickListener {
                if (item.user()._id() == userId) {
                    listener.onItemClickListenerUser()
                } else {
                    listener.onItemClickListenerGuest(item.user()._id().toString(), "FOLLOWINGS")
                }
            }


            if (item.user().followed() == true) {
                button.text = "Following"
                button.setTextColor(Color.parseColor("#000000"))
            } else {
                button.text = "Follow"
                button.setTextColor(Color.parseColor("#FF005F"))
            }

        }
    }

    fun filterList(filteredNames: List<FollowingListQuery.Following>): List<FollowingListQuery.Following>? {
        //  list = filteredNames as ArrayList<FollowingListQuery.Following>
        notifyDataSetChanged()
        return filteredNames
    }
}