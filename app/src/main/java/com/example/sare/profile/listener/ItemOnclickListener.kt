package com.example.sare.profile.listener

interface ItemOnclickListener {
    fun onItemClickListener(position:Int)
}