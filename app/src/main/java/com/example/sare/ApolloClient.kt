package com.example.sare

import com.apollographql.apollo.ApolloClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object ApolloClient {

     fun setupApollo(token: String): ApolloClient {
         val logging = HttpLoggingInterceptor()
         logging.level = (HttpLoggingInterceptor.Level.BODY)
        val okHttp = OkHttpClient
            .Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val builder = original.newBuilder().method(
                    original.method,
                    original.body
                )
                builder.header("Authorization", "Berear $token")
                builder.header("content-type", "application/json")
                chain.proceed(builder.build())
            }
            .addInterceptor(logging)
            .build()
        return ApolloClient.builder()
           // .serverUrl("http://192.168.1.5:8888/graphql")
            .serverUrl("https://api.sarevids.com/graphql")
           //.serverUrl("https://sare.in/graphql/development")
            .okHttpClient(okHttp)
            .build()
    }

}