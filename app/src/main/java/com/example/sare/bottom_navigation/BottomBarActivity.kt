package com.example.sare.bottom_navigation

import com.example.sare.Utils

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.StringSingleton.TREDING
import com.example.sare.appManager.ThemeManager
import com.example.sare.base.BaseActivity
import com.example.sare.chat.ChatUserFragment
import com.example.sare.dashboard.VideoFragment
import com.example.sare.extensions.openNewActivity
import com.example.sare.profile.fragment.ProfileUserFragment
import com.example.sare.status.ui.StatusMainFragment
import com.example.sare.ui.login.LoginActivity
import com.example.sare.util.RequestPermission
import com.example.sare.videoRecording.PortraitCameraActivity
import com.tombayley.activitycircularreveal.CircularReveal
import kotlinx.android.synthetic.main.activity_bottom_bar.*
import kotlinx.android.synthetic.main.popup_guest.*
import org.json.JSONObject



@RequiresApi(api = Build.VERSION_CODES.M) class BottomBarActivity : BaseActivity() {
    var anyonymous: Boolean = false
    lateinit var fragment: Fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme()
        setContentView(R.layout.activity_bottom_bar)
        getSharedPreferences()
        MyApplication.bottomBarActivityInsnace(this)
        intent.extras.run {
            var type =getString("type","").toString()
            if (type=="5")
            {
                CallProfileFragment()
            }
            else{
                CallVideoFragment(TREDING)
            }
        }

        if (anyonymous) {
            bottomNavigation?.setItemChatListener(listener = View.OnClickListener {
                showGuestPopup()
            })

            bottomNavigation?.setItemProfileListener(listener = View.OnClickListener {
                showGuestPopup()

            })
            bottomNavigation?.setItemMusicListener(listener = View.OnClickListener {
                showGuestPopup()

            })
            bottomNavigation?.setItemStatusListener(listener = View.OnClickListener {
                showGuestPopup()
            })
        } else {
            bottomNavigation?.setItemChatListener(listener = View.OnClickListener {
                val fragment: Fragment = ChatUserFragment()
                replaceFragment(fragment)
                bottomNavigation?.setItemChatListener()
            })
            bottomNavigation?.setItemHomeListener(listener = View.OnClickListener {
                CallVideoFragment("")
            })
            bottomNavigation?.setItemProfileListener(listener = View.OnClickListener {
                val fragment: Fragment = ProfileUserFragment()
                replaceFragment(fragment)
                bottomNavigation?.setItemProfileListener()
            })
            bottomNavigation?.setItemMusicListener(listener = View.OnClickListener {
                openRequestPermission()
            })
            bottomNavigation?.setItemStatusListener(listener = View.OnClickListener {

                val fragment: Fragment = StatusMainFragment()
                replaceFragment(fragment)
                bottomNavigation?.setItemStatusListener()
            })
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun showGuestPopup() {
        bottomNavigation?.setItemPopupListener()
        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_guest)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.btnRegister.setOnClickListener {
            dialog.dismiss()
            MyApplication.mainActivity.logoutUser()
            openNewActivity(LoginActivity::class.java)
        }
        dialog.show()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        anyonymous = preferences.getBoolean("ANONYMOUS", false)
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            var image = mainObject.getString("image")
            var colorCode = mainObject.getString("colorCode")
            bottomNavigation?.setUserProfileBorder(colorCode!!)
            bottomNavigation?.setUserProfile(Utils.PUBLIC_URL + image!!)
        }
    }

    internal fun CallVideoFragment(isTrendingFollower: String) {
        val bundle = bundleOf("navigationFlagProfile" to isTrendingFollower)
        fragment = VideoFragment()
        replaceFragment(fragment, bundle)
        bottomNavigation?.setItemHomeListener()
    }

    fun CallProfileFragment() {
        fragment = ProfileUserFragment()
        replaceFragment(fragment)
        bottomNavigation?.setItemProfileListener()
    }


    fun replaceFragment(f: Fragment, bundle: Bundle) {
        fragment = f

        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.relContainer, f)
        if (f is VideoFragment) {
            for (i in 1 until supportFragmentManager.backStackEntryCount) {
                supportFragmentManager.popBackStack()
            }
        } else {
            ft.addToBackStack(f.toString())
        }
        ft.commit()
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState!!)
    }


    fun replaceFragment(f: Fragment) {
        fragment = f
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.relContainer, f)
      /*  if (f is VideoFragment) {
            for (i in 1 until supportFragmentManager.backStackEntryCount) {
                supportFragmentManager.popBackStack()
            }
        } else {
            ft.addToBackStack(f.toString())
        }*/
        ft.commit()
    }

    private fun openRequestPermission() {
        if (requestPermission!!.checkPermission(RequestPermission.PERMISSION_CAMERA) && requestPermission!!.checkPermission(RequestPermission.PERMISSION_CAMERA) && requestPermission!!.checkPermission(RequestPermission.PERMISSION_WRITE_STORAGE) && requestPermission!!.checkPermission(RequestPermission.PERMISSION_READ_STORAGE)) {
            PortraitCameraActivity.startActivity(this)
        } else {
            requestPermission!!.permissionRequestShow(
                object : RequestPermission.PermissionCallBack {
                    override fun callBack(grantAllPermission: Boolean, deniedAllPermission: Boolean, permissionResultList: List<Boolean>) {
                        if (grantAllPermission)
                            PortraitCameraActivity.startActivity(this@BottomBarActivity)
                    }
                },
                RequestPermission.PERMISSION_RECORD_AUDIO, RequestPermission.PERMISSION_CAMERA,
                RequestPermission.PERMISSION_WRITE_STORAGE,
                RequestPermission.PERMISSION_READ_STORAGE
            )
        }
    }
    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }
}