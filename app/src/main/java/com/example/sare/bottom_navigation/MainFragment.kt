package com.example.sare.bottom_navigation

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.sare.R
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.StringSingleton
import com.example.sare.chat.ChatUserFragment
import com.example.sare.dashboard.VideoFragment
import com.example.sare.profile.fragment.ProfileUserFragment
import com.example.sare.status.ui.StatusMainFragment
import com.example.sare.videoRecording.PortraitCameraActivity

//import kotlinx.android.synthetic.main.layout_bottom_navigation.view.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M) class MainFragment : Fragment() {
    var root: View? = null
    var id: String? = ""
    var image: String? = ""
    var username: String? = ""
    var name: String? = ""
    var colorCode: String? = ""
    var type: Int? = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.fragment_main, container, false)
        type = arguments?.getInt("type", 0)

        getSharedPreferences()
        val bundle = bundleOf("userId" to id, "name" to name, "colorCode" to colorCode, "image" to PUBLIC_URL + (image.toString()))

        when (type) {
            1 -> {
                val fragment: Fragment = ChatUserFragment()
                addFragment(fragment, bundle)
                root?.bottomNavigation?.setItemChatListener()
            }
            2 -> {
                val fragment: Fragment = VideoFragment()
                addFragment(fragment, null)
                root?.bottomNavigation?.setItemHomeListener()
            }
            4 -> {
                val fragment: Fragment = StatusMainFragment()
                addFragment(fragment, null)
                root?.bottomNavigation?.setItemStatusListener()
            }
            5 -> {
                val fragment: Fragment = ProfileUserFragment()
                addFragment(fragment, null)
                root?.bottomNavigation?.setItemProfileListener()
            }
            else -> {
                val fragment: Fragment = ChatUserFragment()
                addFragment(fragment, bundle)
            }
        }
        root?.bottomNavigation?.setItemChatListener(listener = View.OnClickListener {
            val fragment: Fragment = ChatUserFragment()
            addFragment(fragment, bundle)
            root?.bottomNavigation?.setItemChatListener()
        })
        root?.bottomNavigation?.setItemHomeListener(listener = View.OnClickListener {
            val fragment: Fragment = VideoFragment()
            addFragment(fragment, null)
            root?.bottomNavigation?.setItemHomeListener()
        })
        root?.bottomNavigation?.setItemProfileListener(listener = View.OnClickListener {
            val fragment: Fragment = ProfileUserFragment()
            addFragment(fragment, null)
            root?.bottomNavigation?.setItemProfileListener()
        })
        root?.bottomNavigation?.setItemMusicListener(listener = View.OnClickListener {
            PortraitCameraActivity.startActivity(activity)
            //root?.bottomNavigation?.setItemMusicListener()
        })
        root?.bottomNavigation?.setItemStatusListener(listener = View.OnClickListener {

            val fragment: Fragment = StatusMainFragment()
            addFragment(fragment, null)
            root?.bottomNavigation?.setItemStatusListener()
        })

        return root
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            id = mainObject.getString("_id")
            username = mainObject.getString("username")
            name = mainObject.getString("name")
            image = mainObject.getString("image")
            colorCode = mainObject.getString("colorCode")
            root?.bottomNavigation?.setUserProfileBorder(colorCode!!)
            root?.bottomNavigation?.setUserProfile(PUBLIC_URL + image!!)
        }
    }

    private fun addFragment(fragment: Fragment, args: Bundle?) {
        val fm: FragmentManager? = fragmentManager
        val ft: FragmentTransaction? = fm?.beginTransaction()
        fm?.beginTransaction()
        if (args != null) {
            fragment.arguments = args
        } else {
            fragment.arguments = arguments
        }
        ft?.replace(R.id.relContainer, fragment)
        ft?.commit()
    }
}