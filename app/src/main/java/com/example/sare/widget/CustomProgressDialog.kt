package com.example.sare.widget

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ProgressBar
import android.widget.TextView
import com.example.sare.R


import kotlinx.android.synthetic.main.custom_progress_dialog.*

class CustomProgressDialog(context: Context?) :
    AlertDialog(context) {
    var textView: TextView? = null
    var progressCircular: ProgressBar? = null

    override fun show() {
        super.show()
        setContentView(R.layout.custom_progress_dialog)
        textView = text
        progressCircular = progress_circular
    }

    override fun hide() {
        super.hide()
        cancel()
    }

  /*  @SuppressLint("SetTextI18n")
    fun setProgress(pos: Int) {
        progressCircular!!.progress = pos.toFloat()
        textView?.text = "$pos%"
    }*/

    init {
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}