package com.example.sare.widget

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.RelativeLayout
import com.example.sare.R

class LangSelectionButtonLayout : RelativeLayout {

    constructor(context: Context? ) : super(context) {
        initBackground()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        initBackground()
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initBackground()
    }

    private fun initBackground() {
        background =
            ViewUtils.generateBackgroundWithShadow(
                this,
                R.color.white,
                R.dimen.io_radius_corner,
                R.color.shadowColor,
                R.dimen.elevation,
                Gravity.BOTTOM
            )
    }
}