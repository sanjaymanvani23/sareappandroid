package com.example.sare.widget.waveForm

class InvalidInputException(message: String) : Exception(message)