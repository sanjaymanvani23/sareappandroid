package com.example.sare.widget;

public interface OnProgressVideoListener {

    void updateProgress(int time, int max, float scale);
}
