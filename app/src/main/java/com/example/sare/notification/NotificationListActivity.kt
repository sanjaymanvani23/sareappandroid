package com.example.sare.notification

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.NotificationListQuery
import com.example.sare.R
import com.example.sare.Utils.Companion.PAGE_SIZE
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.customToast
import com.example.sare.extensions.openNewActivity
import com.example.sare.notification.adapter.NotificationMainAdapter
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_notification_list.*
import org.json.JSONObject

@RequiresApi(Build.VERSION_CODES.M) class NotificationListActivity : AppCompatActivity() {
    var root: View? = null
    var token: String? = null
    var userId: String? = null
    var notificationListViewModel: NotificationListViewModel? = null
    var isLoading = false
    var isLastPage = false
    private var currentPage: Int = 0
    private var notificationList: ArrayList<NotificationListQuery.Notification> = ArrayList()
    lateinit var notificationMainAdapter: NotificationMainAdapter
    var totalItems = 0
    lateinit var paginationListener: PaginationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_list)
        setTheme()
        getPreferences()
        notificationListViewModel = ViewModelProvider(this)[NotificationListViewModel::class.java]
        initNotificationRecyclerView()
        notificationList()
        notificationObserver()
        imgBack?.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun getPreferences() {
        val preferences: SharedPreferences = this.getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        userId = mainObject.getString("_id")
    }

    private fun notificationList() {
        notificationListViewModel?.getNotificationList(this, totalItems)
    }

    private fun notificationObserver() {
        val progressDialog = CustomProgressDialogNew(this)
        this.runOnUiThread {
            progressDialog.show()
        }
        notificationListViewModel?.notificationArrayList?.observe(this, Observer { it ->
            when (it) {

                is NetworkResponse.ERROR -> {
                    progressDialog.hide()

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relNotification), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            notificationListViewModel?.getNotificationList(
                                this, totalItems)
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    progressDialog.hide()

                    isLastPage = true

                    customToast(it.errorMsg, this, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    progressDialog.hide()

                    isLastPage = true

                    customToast(it.errorMsg, this, 0)
                }

                is NetworkResponse.SUCCESS.getNotificationList -> {

                    this.runOnUiThread {
                        progressDialog.hide()
                        isLoading = false
                        if (it.response != null) {

                            for (element in it.response) {
                                notificationMainAdapter.addList(element)
                            }

                            this.runOnUiThread {
                                rvToday?.adapter?.notifyDataSetChanged()
                            }


                            totalItems = 0
                            for (i in notificationList) {
                                totalItems += i.notification()!!.size
                            }
                            paginationListener.visibleCount(totalItems)

                            if (totalItems < PAGE_SIZE) {
                                isLastPage = true
                            }

                            if (rvToday?.adapter?.itemCount == 0) {
                                txtNoData?.visibility = View.VISIBLE
                            } else {
                                txtNoData?.visibility = View.GONE
                            }

                        } else {
                            this.runOnUiThread {
                                customToast(getString(R.string.api_error_message), this, 0)

                            }
                        }

                    }
                }
            }
        })
    }


    private fun initNotificationRecyclerView() {
        notificationMainAdapter = NotificationMainAdapter(
            notificationList, this@NotificationListActivity)
        rvToday?.apply {
            layoutManager = LinearLayoutManager(
                rvToday?.context, RecyclerView.VERTICAL, false)

            paginationListener = object : PaginationListener(this.layoutManager as LinearLayoutManager) {
                override fun loadMoreItems() {
                    isLoading = true
                    currentPage++
                    notificationList()
                    Log.d("tag", "Pagination Current page 11 : " + currentPage)
                }

                override fun isLastPage(): Boolean {
                    Log.d("tag", "Pagination Current page Last page : " + isLastPage)
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    Log.d("tag", "Pagination Current page Loading : " + isLoading)
                    return isLoading
                }

            }

            addOnScrollListener(paginationListener)

            adapter = NotificationMainAdapter(
                notificationList, this@NotificationListActivity)

        }
    }

}