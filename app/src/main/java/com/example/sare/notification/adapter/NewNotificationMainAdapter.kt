package com.example.sare.notification.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.NotificationListQuery
import com.example.sare.R
import com.example.sare.notification.NotificationListActivity
import kotlinx.android.synthetic.main.item_notification_time.view.*


class NewNotificationMainAdapter(
    var list: HashMap<String,ArrayList<NotificationListQuery.Notification1>>,
    private val context: NotificationListActivity
) :
    RecyclerView.Adapter<NewNotificationMainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NewNotificationMainAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification_time, parent, false)
        return ViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: NewNotificationMainAdapter.ViewHolder, position: Int) {
        holder.bindItems(list, context)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: HashMap<String, ArrayList<NotificationListQuery.Notification1>>,
            context: NotificationListActivity
        ) {
            val txtToday = itemView.txtToday
            val recyclerView = itemView.rvMain
            val pos = 0
            var key: String? = null
            txtToday.text = getHashMapKeyFromIndex(list, adapterPosition)

            var notificationList =    list[getHashMapKeyFromIndex(list,adapterPosition)]
            recyclerView.apply {
                layoutManager = LinearLayoutManager(
                    recyclerView.context,
                    RecyclerView.VERTICAL, false
                )
                adapter =
                    NotificationListAdapter(
                     notificationList as ArrayList<NotificationListQuery.Notification1>,
                        context
                    )


            }

            /*    if (txtToday.text.toString() == "Yesterday") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }
                if (txtToday.text.toString() == "This week") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }
                if (txtToday.text.toString() == "This month") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }*/
            /*Log.d("tag", "filteredData::::$filteredData")
            var notificationList = list[adapterPosition].notification()
           notificationList = Collections.unmodifiableList(ArrayList(filteredData))
            Log.d("tag", "notificationList::::$notificationList")*/



        }

        fun getHashMapKeyFromIndex(hashMap: HashMap<String, ArrayList<NotificationListQuery.Notification1>>, index: Int): String? {
            var key: String? = null
            val hs: HashMap<String, ArrayList<NotificationListQuery.Notification1>> = hashMap
            var pos = 0
            for ((key1) in hs.entries) {
                if (index == pos) {
                    key = key1
                }
                pos++
            }
            return key
        }
    }



}