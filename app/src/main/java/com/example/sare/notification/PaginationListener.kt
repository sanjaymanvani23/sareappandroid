package com.example.sare.notification

import android.util.Log
import androidx.annotation.NonNull
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.Utils.Companion.PAGE_SIZE


abstract class PaginationListener(val layoutManager: RecyclerView.LayoutManager?): RecyclerView.OnScrollListener() {
    var visibleItemCount = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
//         visibleItemCount = layoutManager!!.childCount
        val totalItemCount = visibleItemCount
        var firstVisibleItemPosition = 0
        if(layoutManager is GridLayoutManager) {
            val layoutManager1 = layoutManager as GridLayoutManager
            firstVisibleItemPosition = layoutManager1!!.findFirstVisibleItemPosition()
        } else if(layoutManager is LinearLayoutManager) {
            val layoutManager1 = layoutManager as LinearLayoutManager
            firstVisibleItemPosition = layoutManager1!!.findFirstVisibleItemPosition()
        } else{
            firstVisibleItemPosition = 0
        }

        Log.d("PaginationListener","isLoading()::::::${isLoading()}")
        Log.d("PaginationListener","isLastPage()::::::${isLastPage()}")
        Log.d("PaginationListener","visibleItemCount :${visibleItemCount}")
        Log.d("PaginationListener","firstVisibleItemPosition :${firstVisibleItemPosition}")
        Log.d("PaginationListener","totalItemCount :${totalItemCount}")
        if (!isLoading() && !isLastPage()) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                loadMoreItems()
            }
        }
    }

    fun visibleCount(item: Int){
         visibleItemCount = item
        Log.d("tag","visibleItemCount:::$visibleItemCount")
    }



    abstract fun loadMoreItems()
    abstract fun isLastPage(): Boolean
    abstract fun isLoading(): Boolean
}