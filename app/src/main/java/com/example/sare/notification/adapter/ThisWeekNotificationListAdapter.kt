package com.example.sare.notification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R

class ThisWeekNotificationListAdapter(
    var list: List<String>,
    private val context: Context
) :
    RecyclerView.Adapter<ThisWeekNotificationListAdapter.ViewHolder>() {


    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_this_week_notification, parent, false)
        return ViewHolder(
            v
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list.toString(), context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: String,
            context: Context
        ) {

//            val txtReport = itemView.txtName
//            txtReport.text = "@VikramPrajapati"


        }
    }

//class RecyclerViewAdapter(context: Context, list: ArrayList<String>) :
//    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
//
//    companion object {
//        const val TYPE_FOLLOW = 1
//        const val TYPE_LIKED: Int = 2
//    }
//
//    private val context: Context = context
//    var list: ArrayList<String> = list
//
//    //the class is hodling the list view
//    class LikedNotificationViewHolder(itemView: View) :
//        RecyclerView.ViewHolder(itemView) {
//
//        fun bindItems(
//            list: String,
//            context: Context,
//            onItemClick: ((String) -> Unit)
//        ) {
//
//            val txtName = itemView.txtLikedName
//            txtName.text = "@Vikramrajapati"
//            itemView.setOnClickListener {
//                onItemClick.invoke(list)
//            }
//
//
//        }
//    }
//
//    class FollowNotificationViewHolder(itemView: View) :
//        RecyclerView.ViewHolder(itemView) {
//
//        fun bindItems(
//            list: String,
//            context: Context,
//            onItemClick: ((String) -> Unit)
//        ) {
//
//            val txtName = itemView.txtName
//            txtName.text = "@Vikramrajapati"
//            itemView.setOnClickListener {
//                onItemClick.invoke(list)
//            }
//
//
//        }
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        if (viewType == TYPE_FOLLOW) {
//            return FollowNotificationViewHolder(
//                LayoutInflater.from(context).inflate(R.layout.item_following_notification, parent, false)
//            )
//        }
//        return LikedNotificationViewHolder(
//            LayoutInflater.from(context).inflate(R.layout.item_liked_notification, parent, false)
//        )
//    }
//
//    override fun getItemCount(): Int {
//        return list.size
//    }
//
//    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
//        if (list[position].viewType === VIEW_TYPE_ONE) {
//            (holder as View1ViewHolder).bind(position)
//        } else {
//            (holder as View2ViewHolder).bind(position)
//        }
//    }
//
//    override fun getItemViewType(position: Int): Int {
//        return list[position].viewType
//    }
}
