package com.example.sare.notification

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.NotificationListQuery
import com.example.sare.Utils
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class NotificationListViewModel : ViewModel() {
    var notificationArrayList = MutableLiveData<NetworkResponse>()

    fun getNotificationList(context: Context,pageCount: Int) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())

        Log.e("count","pageCount:::::$pageCount")
        apolloClient.query(NotificationListQuery(pageCount, Utils.PAGE_SIZE))
            .enqueue(object : ApolloCall.Callback<NotificationListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    notificationArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<NotificationListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.notification() != null) {
                                notificationArrayList.postValue(
                                    NetworkResponse.SUCCESS.getNotificationList(
                                        response.data()?.notification()!!
                                    )
                                )
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                notificationArrayList.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            notificationArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}