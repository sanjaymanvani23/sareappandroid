package com.example.sare.notification.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.sare.*
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.customToast
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.notification.NotificationListActivity
import kotlinx.android.synthetic.main.item_following_notification.view.*
import kotlinx.android.synthetic.main.item_following_notification.view.relClicked
import kotlinx.android.synthetic.main.item_following_notification.view.txtDuration
import kotlinx.android.synthetic.main.item_liked_notification.view.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NotificationListAdapter(
    var list: MutableList<NotificationListQuery.Notification1>,
    private val context: NotificationListActivity
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_ONE = 1
    private val TYPE_TWO = 2
    private val TYPE_THREE = 3

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {

        val view: View
        return when (viewType) {
            TYPE_ONE -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_following_notification, parent, false)
                ViewHolderFollow(view)

            }
            TYPE_TWO -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_liked_notification, parent, false)
                ViewHolderLike(view)
            }
            TYPE_THREE -> {
                view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_liked_notification, parent, false)
                ViewHolderLike(view)
            }
            else ->
                throw RuntimeException("The type has to be ONE or TWO")

        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val object1: NotificationListQuery.Notification1 = list[position]

        if (object1 != null) {
            when (object1.notificationType()) {
                "FOLLOW" -> {
                    (holder as ViewHolderFollow).bindItems(
                        list, context
                    )
                }
                "LIKE" -> {
                    (holder as ViewHolderLike).bindItems(
                        list, context
                    )
                }
                "COMMENT" -> {
                    (holder as ViewHolderLike).bindItems(
                        list, context
                    )
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun getItemViewType(position: Int): Int {
        for (i in list) {
            when (list[position].notificationType()) {
                "FOLLOW" -> return TYPE_ONE

                "LIKE" -> return TYPE_TWO

                "COMMENT" -> return TYPE_THREE
            }
        }
        return -1
    }


    class ViewHolderFollow(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: List<NotificationListQuery.Notification1>,
            context: NotificationListActivity
        ) {

          val userName = itemView.txtName
            val txtLikes = itemView.txtLikes
            val videoCounts = itemView.txtVideoCounts
            val txtFollowersCount = itemView.txtFollowersCount
            val txtDuration = itemView.txtDuration
            val relClicked = itemView.relClicked
            val imgProfile = itemView.imgProfile
            val btnFollow = itemView.btnFollow
            val imgProfileBorder = itemView.imgProfileBorder
          
           val urlString = Utils.PUBLIC_URL + (list[adapterPosition].user()!!.image().toString())

            val colorCode = list[adapterPosition].user()!!.colorCode()

            var date = Date().toSimpleString()

            imgProfile?.loadUrl(urlString!!)
            imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

            relClicked.setOnClickListener {
                val bundle = bundleOf("userId" to list[adapterPosition].user()?._id())
               /* if (NavHostFragment.findNavController(context).currentDestination?.id == R.id.notificationListFragment) {
                    NavHostFragment.findNavController(context)
                        .navigate(
                            R.id.action_notificationListFragment_to_profileGuestFragment,
                            bundle
                        )
                }*/
            }
            userName.text = list[adapterPosition].user()?.name()
            videoCounts.text = list[adapterPosition].user()?.postsCounts().toString()
            if (list[adapterPosition].user()?.followerCounts()!! > 0) {
                txtFollowersCount.text =
                    list[adapterPosition].user()?.followerCounts().toString() + " Followers"
            } else {
                txtFollowersCount.text = "0 Followers"
            }

            txtLikes.text = list[adapterPosition].user()?.likeCounts().toString()

            txtDuration.text = getParsedDate(list[adapterPosition].createdAt().toString())
            val preferences: SharedPreferences = context.getSharedPreferences(
                StringSingleton.sharedPrefFile,
                Context.MODE_PRIVATE
            )
            val token = preferences.getString("TOKEN", null)

            Log.d("tag", "token::$token")

            val apolloClientNew = ApolloClient.setupApollo(token.toString())

            if (list[adapterPosition].user()?.followed() == true) {
                btnFollow.text = "Following"
                btnFollow.setTextColor(Color.parseColor("#000000"))

            } else {
                btnFollow.text = "Follow"
                btnFollow.setTextColor(Color.parseColor("#FF005F"))

            }

            btnFollow.setOnClickListener {

                if (btnFollow.text == "Following") {

                    apolloClientNew.mutate(
                        UnFollowUserMutation(
                            list[adapterPosition].user()!!._id()
                        )
                    )

                        .enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
                            override fun onFailure(e: ApolloException) {
                                Log.d("tag", "Error Data : ${e.message}")
                            }

                            override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                                context.runOnUiThread {

                                    Log.d("tag", "res following adapter::${response}")
                                    if (response.data?.unfollow()?.status() == true) {

                                        btnFollow.text = "Follow"
                                        btnFollow.setTextColor(Color.parseColor("#FF005F"))
                                        customToast("User Unfollowed Successfully",
                                            context,1)
                                    } else {
                                        val error =
                                            response.errors?.get(position)?.customAttributes?.get("status")
                                        Log.d("tag", "Data Error: $error")
                                        if (error?.toString()?.equals("401") == true) {
                                            customToast( response.errors?.get(position)?.message.toString(),
                                            context,0)
                                        } else {
                                            customToast( response.errors?.get(position)?.message.toString(),
                                            context,0)

                                        }
                                    }

                                }
                            }
                        })
                } else {

                    apolloClientNew.mutate(
                        FollowUserMutation(
                            list[adapterPosition].user()!!._id()
                        )
                    )

                        .enqueue(object :
                            ApolloCall.Callback<FollowUserMutation.Data>() {
                            override fun onFailure(e: ApolloException) {
                                Log.d("tag", "Error Data : ${e.message}")
                            }

                            override fun onResponse(response: Response<FollowUserMutation.Data>) {
                                context.runOnUiThread {

                                    if (response.data?.follow()?.status()!!) {

                                        btnFollow.text = "Following"
                                        btnFollow.setTextColor(Color.parseColor("#000000"))

                                        customToast("User Followed Successfully",context,1)

                                    } else {

                                        val error =
                                            response.errors?.get(position)?.customAttributes?.get("status")
                                        Log.d("tag", "Data Error:1 $error")

                                        if (error?.toString()?.equals("401") == true) {
                                            customToast( response.errors?.get(position)?.message.toString(),
                                            context,0)

                                        } else {
                                            customToast( response.errors?.get(position)?.message.toString(),
                                            context,0)

                                        }
                                    }
                                }


                            }
                        })
                }
            }


        }

        private fun getParsedDate(date: String): String {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            val SECOND_MILLIS = 1000
            val MINUTE_MILLIS = 60 * SECOND_MILLIS
            val HOUR_MILLIS = 60 * MINUTE_MILLIS
            val DAY_MILLIS = 24 * HOUR_MILLIS
            val WEEK_MILLIS = 7 * DAY_MILLIS
            var parsedDate: String? = null
            parsedDate = try {
                Log.d("tag","df.parse(date).time::: ${df.parse(date).time}")

                var time: Long = df.parse(date).time
                Log.d("tag","time::: $time")

                if (time < 1000000000000L) {
                    // if timestamp given in seconds, convert to millis
                    time *= 1000;
                }

                val now = System.currentTimeMillis()


                val diff = now - time;

                Log.d("tag", "diff:::$diff")

                when {
                    diff < MINUTE_MILLIS -> {
                        return "just now";
                    }
                    diff < 2 * MINUTE_MILLIS -> {
                        return "a minute ago";
                    }
                    diff < 50 * MINUTE_MILLIS -> {
                        return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                    }
                    diff < 90 * MINUTE_MILLIS -> {
                        return "an hour ago";
                    }
                    diff < 24 * HOUR_MILLIS -> {
                        return (diff / HOUR_MILLIS).toString() + " hours ago";
                    }
                    diff < 48 * HOUR_MILLIS -> {
                        return "yesterday";

                    }
                    diff < 7 * DAY_MILLIS -> {
                        return (diff / DAY_MILLIS).toString() + " days ago";
                    }
                    diff < 2 * WEEK_MILLIS -> {
                        return "a week ago";
                    }
                    diff < WEEK_MILLIS * 3 -> {
                        return (diff / WEEK_MILLIS).toString() + " weeks ago";
                    }
                    else -> {
                        val date1 = getParsedDate(date.toString())
                        Log.d("tag", date1)
                        parsedDate = date1
                        return parsedDate.toString()
                    }
                }

            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("tag", "Exception::${e.message}")
                val date1 = getParsedDate(date.toString())
                Log.d("tag", date1)
                parsedDate = date1
                return parsedDate.toString()
            }
            return parsedDate.toString()

        }


    }


    class ViewHolderLike(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: List<NotificationListQuery.Notification1>,
            context: NotificationListActivity
        ) {

            val userName = itemView.txtLikedName
            val txtDesc = itemView.txtDesc
            val imgnotification = itemView.imgVideo1
            val cardView = itemView.cardView
            val relClicked = itemView.relClicked
            val txtDuration = itemView.txtDuration

            val imgProfile = itemView.imgProfile1
            val imgProfileBorder = itemView.imgProfileBorder1


            val urlString = Utils.PUBLIC_URL + (list[adapterPosition].user()!!.image().toString())
           

            val colorCode = list[adapterPosition].user()!!.colorCode()

            imgProfile?.loadUrl(urlString!!)
            imgProfileBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)


            txtDuration.text = getParsedDate(list[adapterPosition].createdAt().toString())
            relClicked.setOnClickListener {
              /*  if (NavHostFragment.findNavController(context).currentDestination?.id == R.id.notificationListFragment) {
                    NavHostFragment.findNavController(context)
                        .navigate(R.id.action_notificationListFragment_to_homeFragment2)
                }*/
            }
            if (list[adapterPosition].notificationType().equals("COMMENT")) {
                userName.text = list[adapterPosition].user()?.name()
                txtDesc.text = "Commented on your video"
                Glide.with(context)

                    .load(
                        PUBLIC_URL + (
                                list[adapterPosition].video()?.imageName().toString()
                                )
                    ).error(R.drawable.video_default)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))
                    .placeholder(R.drawable.video_default)
                    .into(imgnotification)


            }


            if (list[adapterPosition].notificationType().equals("LIKE")) {
                userName.text = list[adapterPosition].user()?.name()
                txtDesc.text = "Liked Your video"
                Glide.with(context)
                    .load(
                        PUBLIC_URL + (
                                list[adapterPosition].video()?.imageName().toString()
                                )
                    ).error(R.drawable.video_default)
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))

                    .placeholder(R.drawable.video_default)

                    .into(imgnotification)

            }


        }

        private fun getParsedDate(date: String): String {
            val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            val SECOND_MILLIS = 1000
            val MINUTE_MILLIS = 60 * SECOND_MILLIS
            val HOUR_MILLIS = 60 * MINUTE_MILLIS
            val DAY_MILLIS = 24 * HOUR_MILLIS
            val WEEK_MILLIS = 7 * DAY_MILLIS
            var parsedDate: String? = null
            parsedDate = try {
                var time: Long = df.parse(date).time
                if (time < 1000000000000L) {
                    // if timestamp given in seconds, convert to millis
                    time *= 1000;
                }

                val now = System.currentTimeMillis()


                val diff = now - time;

                Log.d("tag", "diff:::$diff")

                when {
                    diff < MINUTE_MILLIS -> {
                        return "just now";
                    }
                    diff < 2 * MINUTE_MILLIS -> {
                        return "a minute ago";
                    }
                    diff < 50 * MINUTE_MILLIS -> {
                        return (diff / MINUTE_MILLIS).toString() + " minutes ago";
                    }
                    diff < 90 * MINUTE_MILLIS -> {
                        return "an hour ago";
                    }
                    diff < 24 * HOUR_MILLIS -> {
                        return (diff / HOUR_MILLIS).toString() + " hours ago";
                    }
                    diff < 48 * HOUR_MILLIS -> {
                        return "yesterday";

                    }
                    diff < 7 * DAY_MILLIS -> {
                        return (diff / DAY_MILLIS).toString() + " days ago";
                    }
                    diff < 2 * WEEK_MILLIS -> {
                        return "a week ago";
                    }
                    diff < WEEK_MILLIS * 3 -> {
                        return (diff / WEEK_MILLIS).toString() + " weeks ago";
                    }
                    else -> {
                        val date1 = getParsedDate(date.toString())
                        Log.d("tag", date1)
                        parsedDate = date1
                        return parsedDate.toString()
                    }
                }

            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("tag", "Exception::${e.message}")
                val date1 = getParsedDate(date.toString())
                Log.d("tag", date1)
                parsedDate = date1
                return parsedDate.toString()
            }
            return parsedDate.toString()
        }
    }

    fun filter(filterEvents: List<NotificationListQuery.Notification>) {
        list = ArrayList()
        (list as MutableList<NotificationListQuery.Notification>).addAll(filterEvents)
        notifyDataSetChanged()
    }

}