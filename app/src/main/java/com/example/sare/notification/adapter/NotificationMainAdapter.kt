package com.example.sare.notification.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.NotificationListQuery
import com.example.sare.R
import com.example.sare.notification.NotificationListActivity
import kotlinx.android.synthetic.main.item_notification_time.view.*
import java.util.*
import kotlin.collections.ArrayList


class NotificationMainAdapter(
    var list: ArrayList<NotificationListQuery.Notification>,
    private val context: NotificationListActivity
) :
    RecyclerView.Adapter<NotificationMainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NotificationMainAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification_time, parent, false)
        return ViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    public fun addList(item : NotificationListQuery.Notification){
        if (list.size == 0) {
            list.add(item)
        }else{
            when {
                list[list.size - 1].days() == "Today" && item.days()=="Today"-> {
                    var EX_FIELDS: List<NotificationListQuery.Notification1> = ArrayList()

                    var EX_FIELDS1: ArrayList<NotificationListQuery.Notification1> = ArrayList()

                    EX_FIELDS=Collections.unmodifiableList(list[list.size - 1].notification())

                    EX_FIELDS1.addAll(EX_FIELDS)

                    EX_FIELDS1.addAll(Collections.unmodifiableList(item.notification()))

                    var notiObj= NotificationListQuery.Notification(item.__typename(),item.date(),item.days(),EX_FIELDS1)

                    list[list.size-1] = notiObj
                }
                list[list.size - 1].days() == "Yesterday" && item.days()=="Yesterday"-> {
                    var EX_FIELDS: List<NotificationListQuery.Notification1> = ArrayList()

                    var EX_FIELDS1: ArrayList<NotificationListQuery.Notification1> = ArrayList()

                    EX_FIELDS=Collections.unmodifiableList(list[list.size - 1].notification())

                    EX_FIELDS1.addAll(EX_FIELDS)

                    EX_FIELDS1.addAll(Collections.unmodifiableList(item.notification()))
                    var notiObj= NotificationListQuery.Notification(item.__typename(),item.date(),item.days(),EX_FIELDS1)
                    list[list.size-1] = notiObj
                }
                list[list.size - 1].days() == "This week" && item.days()=="This week"-> {

                    var EX_FIELDS: List<NotificationListQuery.Notification1> = ArrayList()

                    var EX_FIELDS1: ArrayList<NotificationListQuery.Notification1> = ArrayList()

                    EX_FIELDS=Collections.unmodifiableList(list[list.size - 1].notification())

                    EX_FIELDS1.addAll(EX_FIELDS)

                    EX_FIELDS1.addAll(Collections.unmodifiableList(item.notification()))

                    var notiObj= NotificationListQuery.Notification(item.__typename(),item.date(),item.days(),EX_FIELDS1)
                    list[list.size-1] = notiObj
                }
                list[list.size - 1].days() == "This month" && item.days()=="This month" -> {
                    var EX_FIELDS: List<NotificationListQuery.Notification1> = ArrayList()

                    var EX_FIELDS1: ArrayList<NotificationListQuery.Notification1> = ArrayList()

                    EX_FIELDS=Collections.unmodifiableList(list[list.size - 1].notification())

                    EX_FIELDS1.addAll(EX_FIELDS)

                    EX_FIELDS1.addAll(Collections.unmodifiableList(item.notification()))

                    var notiObj= NotificationListQuery.Notification(item.__typename(),item.date(),item.days(),EX_FIELDS1)
                    list[list.size-1] = notiObj
                }
                list[list.size - 1].days() == "This year" && item.days()=="This year" -> {
                    var EX_FIELDS: List<NotificationListQuery.Notification1> = ArrayList()
                    EX_FIELDS= Collections.unmodifiableList(item.notification())
                    var notiObj= NotificationListQuery.Notification(item.__typename(),item.date(),item.days(),EX_FIELDS)
                    list[list.size-1] = notiObj
                }
                else->
                {
                    list.add(item)
                }
            }

        }
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: NotificationMainAdapter.ViewHolder, position: Int) {
        holder.bindItems(list, context)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            list: ArrayList<NotificationListQuery.Notification>,
            context: NotificationListActivity
        ) {
            val txtToday = itemView.txtToday
            val recyclerView = itemView.rvMain

            txtToday.text = list[adapterPosition].days()

           recyclerView.apply {
                layoutManager = LinearLayoutManager(
                    recyclerView.context,
                    RecyclerView.VERTICAL, false
                )
                adapter =
                    NotificationListAdapter(
                        list[adapterPosition].notification()!!
                        ,context
                    )


            }

            /*    if (txtToday.text.toString() == "Yesterday") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }
                if (txtToday.text.toString() == "This week") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }
                if (txtToday.text.toString() == "This month") {
                    for (s in list) {
                        Log.d("tag", "s.days()::" + { s.days() })

                        filteredData.addAll(s.notification()!!)

                    }
                }*/
            /*Log.d("tag", "filteredData::::$filteredData")
            var notificationList = list[adapterPosition].notification()
           notificationList = Collections.unmodifiableList(ArrayList(filteredData))
            Log.d("tag", "notificationList::::$notificationList")*/



        }


    }


}