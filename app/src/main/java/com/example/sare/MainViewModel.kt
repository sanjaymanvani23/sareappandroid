package com.d.parastask

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.d.parastask.api.Resource
import com.example.sare.MainRepository
import com.example.sare.MyApplication
import kotlinx.coroutines.Dispatchers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import retrofit2.http.Part
import java.io.IOException

class MainViewModel constructor(private val mainRepository: MainRepository): ViewModel() {

    fun uploadImage(@Part("colorCode") colorCode: RequestBody,
                    @Part file: MultipartBody.Part,
                    @Header("Authorization") token: String) = liveData(Dispatchers.IO) {
        try {
            emit(Resource.success(data = mainRepository.uploadImage(colorCode,file,token)))
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

}