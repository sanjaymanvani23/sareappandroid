package com.example.sare.appManager

import android.content.Intent
import android.util.Log
import com.example.sare.*
import com.example.sare.data.ResponseData
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.showToast
import com.example.sare.ui.login.LoginActivity

sealed class NetworkResponse {

    object START : NetworkResponse()

    data class ERROR(val error: Throwable) : NetworkResponse()
    data class ERROR_AUTHENTICATION(val errorMsg: String, val errorCode: String) : NetworkResponse() {

        init {
            Log.e("tag", "" + errorCode)
            if (errorCode == "401" || "403" == errorCode) {
                Log.e("tag", "" + errorMsg)
                try {
                    //  MyApplication.mainActivityInsnace().customToastMain(errorMsg, 0)
                    var intent = Intent(MyApplication.bottomBarActivityInsnace()!!, LoginActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    MyApplication.bottomBarActivityInsnace().startActivity(intent)
                    MyApplication.bottomBarActivityInsnace().finishAffinity()
                } catch (e: Exception) {
                    Log.e("tag", "" + e.toString())
                }


            }

        }
    }


    data class ERROR_FORBIDDEN(val errorMsg: String) : NetworkResponse()
    data class ERROR_RESPONSE(val errorMsg: String) : NetworkResponse()
    data class ERROR_RESPONSE_NEW(val messageId: String) : NetworkResponse()

    sealed class SUCCESS<out T>(val response: T) : NetworkResponse() {

        data class videoList(val res: List<VideoListQuery.Video>) : SUCCESS<List<VideoListQuery.Video>>(res)

        data class videoAudioList(val res: VideoAudioListQuery.Audio) : SUCCESS<VideoAudioListQuery.Audio>(res)

        data class audioList(val res: List<AudioListQuery.Audio>) : SUCCESS<List<AudioListQuery.Audio>>(res)

        data class categoryList(val res: List<CategoryListQuery.Category>) :
            SUCCESS<List<CategoryListQuery.Category>>(res)

        data class commentList(val res: List<CommentsListQuery.Comment>) : SUCCESS<List<CommentsListQuery.Comment>>(res)

        data class createCommentList(val res: CreateCommentMutation.Comment) :
            SUCCESS<CreateCommentMutation.Comment>(res)

        data class bookmarkVideo(val res: BookmarkVideoMutation.BookMark) : SUCCESS<BookmarkVideoMutation.BookMark>(res)

        data class videoLike(val res: VideoLikeMutation.Like) : SUCCESS<VideoLikeMutation.Like>(res)
        data class videoDisLike(val res: VideoDisLikeMutation.Dislike) : SUCCESS<VideoDisLikeMutation.Dislike>(res)

        data class colorList(val res: List<ColorListQuery.Color>) : SUCCESS<List<ColorListQuery.Color>>(res)

        data class userVideoPublishedList(val res: List<UserVideosQuery.UserVideo>) :
            SUCCESS<List<UserVideosQuery.UserVideo>>(res)

        data class userVideoDraftList(val res: List<UserVideosQuery.UserVideo>) :
            SUCCESS<List<UserVideosQuery.UserVideo>>(res)

        data class userVideoBookMarkList(val res: List<UserVideosQuery.UserVideo>) :
            SUCCESS<List<UserVideosQuery.UserVideo>>(res)

        data class getFollowerList(val res: List<FollowerListQuery.Follower>) :
            SUCCESS<List<FollowerListQuery.Follower>>(res)

        data class getFollowingList(val res: List<FollowingListQuery.Following>) :
            SUCCESS<List<FollowingListQuery.Following>>(res)

        data class getTrendingTagsList(val res: List<TrendingTagsAndProfileQuery.Trending>) :
            SUCCESS<List<TrendingTagsAndProfileQuery.Trending>>(res)

        data class getReportList(val res: List<ReportListQuery.ReportType>) :
            SUCCESS<List<ReportListQuery.ReportType>>(res)

        data class getBugTypes(val res: List<BugTypesQuery.BugType>) : SUCCESS<List<BugTypesQuery.BugType>>(res)

        data class getNotificationList(val res: List<NotificationListQuery.Notification>) :
            SUCCESS<List<NotificationListQuery.Notification>>(res)

        data class getBlockUserList(val res: List<BlockUserListQuery.BlockUserList>) :
            SUCCESS<List<BlockUserListQuery.BlockUserList>>(res)

        data class getChatBlockUserList(val res: List<BlockChatUserListQuery.BlockChatUserList>) :
            SUCCESS<List<BlockChatUserListQuery.BlockChatUserList>>(res)

        data class saveNotificationSettings(val res: UpdateNotificationMutation.UpdateUserNotificationPermission) :
            SUCCESS<UpdateNotificationMutation.UpdateUserNotificationPermission>(res)

        data class displayNotificationSettings(val res: NotificationDisplayQuery.Data) :
            SUCCESS<NotificationDisplayQuery.Data>(res)

        data class getEditUserDetails(val res: UserDetailsQuery.Data) : SUCCESS<UserDetailsQuery.Data>(res)
        data class getUserDetails(val res: UserDetailsQuery.Data) : SUCCESS<UserDetailsQuery.Data>(res)

        data class getGuestLogin(val res: GuestLoginMutation.Data) : SUCCESS<GuestLoginMutation.Data>(res)

        data class getCountry(val res: CountryQuery.Data) : SUCCESS<CountryQuery.Data>(res)

        data class doLogin(val res: GenerateOtpMutation.Data) : SUCCESS<GenerateOtpMutation.Data>(res)

        data class versionCheck(val res: SplashVersionQuery.Data) : SUCCESS<SplashVersionQuery.Data>(res)

        data class verifyOtp(val res: LoginMutation.Data) : SUCCESS<LoginMutation.Data>(res)

        data class ssoLogin(val res: SSOLoginMutation.Data) : SUCCESS<SSOLoginMutation.Data>(res)

        data class uploadImage(val res: ResponseData) : SUCCESS<ResponseData>(res)

        data class acceptAgreement(val res: AcceptAgreementMutation.UpdateUser) :
            SUCCESS<AcceptAgreementMutation.UpdateUser>(res)

        data class updatePrivacyStatus(val res: UpdatePrivacyStatusMutation.Data) :
            SUCCESS<UpdatePrivacyStatusMutation.Data>(res)

        data class followUser(val res: FollowUserMutation.Follow) : SUCCESS<FollowUserMutation.Follow>(res)

        data class unfollowUser(val res: UnFollowUserMutation.Unfollow) : SUCCESS<UnFollowUserMutation.Unfollow>(res)
        data class muteChat(val res: MuteChatMutation.MuteChat) : SUCCESS<MuteChatMutation.MuteChat>(res)
        data class unMuteChat(val res: UnmuteChatMutation.UnmuteChat) : SUCCESS<UnmuteChatMutation.UnmuteChat>(res)

        data class blockUser(val res: BlockUserMutation.BlockUser) : SUCCESS<BlockUserMutation.BlockUser>(res)
        data class blockChat(val res: BlockChatMutation.BlockChat) : SUCCESS<BlockChatMutation.BlockChat>(res)
        data class acceptChat(val res: UnblockChatMutation.AcceptChat) : SUCCESS<UnblockChatMutation.AcceptChat>(res)
        data class deactiveUser(val res: DeactiveUserMutation.DeactiveUser) : SUCCESS<DeactiveUserMutation.DeactiveUser>(res)
        data class logoutUser(val res: LogoutUserMutation.Logout) : SUCCESS<LogoutUserMutation.Logout>(res)

        data class download(val res: DownloadMutation.Download) : SUCCESS<DownloadMutation.Download>(res)

        data class viewCount(val res: ViewCountMutation.View) : SUCCESS<ViewCountMutation.View>(res)

        data class removeVideo(val res: RemoveVideoMutation.RemoveVideo) : SUCCESS<RemoveVideoMutation.RemoveVideo>(res)

        data class getUsersOnSareVids(val res: List<UsersOnSareVidsQuery.User>) :
            SUCCESS<List<UsersOnSareVidsQuery.User>>(res)

        data class getTagsList(val res: List<TagSearchListQuery.Tag>) : SUCCESS<List<TagSearchListQuery.Tag>>(res)

        data class getExploreTagsList(val res: List<ExploreTagsListQuery.Video>) :
            SUCCESS<List<ExploreTagsListQuery.Video>>(res)

        data class getStoryViewerList(val res: List<StoryViewerListQuery.StoryViewerList>) :
            SUCCESS<List<StoryViewerListQuery.StoryViewerList>>(res)

        data class getStoryList(val res: StoryListQuery.StoryList) : SUCCESS<StoryListQuery.StoryList>(res)

        data class getStoryChecked(val res: CheckStoryMutation.AddStoryView) :
            SUCCESS<CheckStoryMutation.AddStoryView>(res)

        data class deleteStory(val res: DeleteStoryMutation.DeleteStory) :
            SUCCESS<DeleteStoryMutation.DeleteStory>(res)

        data class bugReport(val res: ResponseData) : SUCCESS<ResponseData>(res)

        data class getUsersListUsingApp(val res: List<CheckUsersFromContactListQuery.CheckUsersExist>) :
            SUCCESS<List<CheckUsersFromContactListQuery.CheckUsersExist>>(res)


    }
}