package com.example.sare.appManager

object StringSingleton {



    //Lang
    var ENGLISH = "ENGLISH"

    //Colors
    var primary = "primary"
    var button_login_background = "button_login_background"
    var termsAndConditionPageBackground = "button_login_background"
    var textDark = "textDark"
    var textBlack = "textBlack"
    var textBlackHeader = "textBlackHeader"
    var textLight = "textLight"
    var textWhite = "textWhite"
    var viewHeader = "viewHeader"
    var bodyBackground = "bodyBackground"
    var viewBackground = "viewBackground"
    var themeColor = "themeColor"
    var shadowColor = "shadowColor"
    var menuHeader = "menuHeader"
    var textSuccess = "textSuccess"
    var textError = "textError"
    var finalWhite = "finalWhite"
    var placeHolderColor = "placeHolderColor"
    var greenColor = "greenColor"
    var textOtp = "textOtp"
    var ovelayColor = "ovelayColor"
    var statusColor = "statusColor"
    var defaultColor = "defaultColor"
    var dividerColor = "dividerColor"
    var successColor = "successColor"
    var textDarkBlack = "successColor"
    var strokeColor = "strokeColor"

    //Shared Pref
    var sharedPrefFile  = "pref"
    var sharedNightMode = "nightMode"

    //langSelection
    var header = ""
    var chooseLang = "Choose language"
    var countryCode = "+91"
    var editTextMobileNoHint = "Enter Mobile Number"
    var buttonText = ""
    var sendOtp = "Send Otp"
    var facebook = "Facebook"
    var google = "Google"

    //logo
    var mainLogo = "mainLogo"
    var signupLogo = "signupLogo"
    var verificationLogo = "verificationLogo"
    var profileLogo = "profileLogo"
    var confirmRightImage = "confirmRightImage"
    var carChooseLogoImage = "carChooseLogoImage"
    var activityIconChoose = "activityIconChoose"
    var filterIcon = "filterIcon"
    var amenityParkingIcon = "amenityParkingIcon"
    var navDrawerHomeIcon = "navDrawerHomeIcon"
    var filterHomeIcon = "filterHomeIcon"
    var locationHomeIcon = "locationHomeIcon"
    var searchHomeIcon = "searchHomeIcon"
    var wishListHomeIcon = "wishListHomeIcon"
    var searchIcon = "searchIcon"

    var backGround = "backGround"
    var transparent = "transparent"
    var otpBackground = "otpBackground"
    var internalScreenBackgroundColor = "internalScreenBackgroundColor"
    var TREDING="Treding"
    var FOLLOWERS="Followers"

}