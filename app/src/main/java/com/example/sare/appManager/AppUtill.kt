package com.example.sare.appManager

import android.content.Context
import android.content.SharedPreferences

class AppUtill {

    fun isDarkMode (context: Context): Boolean {
        val sharedPreferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE)
        return  sharedPreferences.getBoolean(StringSingleton.sharedNightMode , false)
    }

}