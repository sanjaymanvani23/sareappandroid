package com.example.sare.appManager

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentActivity
import com.example.sare.R
import com.example.sare.appManager.StringSingleton.backGround
import com.example.sare.appManager.StringSingleton.bodyBackground
import com.example.sare.appManager.StringSingleton.button_login_background
import com.example.sare.appManager.StringSingleton.defaultColor
import com.example.sare.appManager.StringSingleton.dividerColor
import com.example.sare.appManager.StringSingleton.finalWhite
import com.example.sare.appManager.StringSingleton.greenColor
import com.example.sare.appManager.StringSingleton.internalScreenBackgroundColor
import com.example.sare.appManager.StringSingleton.menuHeader
import com.example.sare.appManager.StringSingleton.otpBackground
import com.example.sare.appManager.StringSingleton.ovelayColor
import com.example.sare.appManager.StringSingleton.placeHolderColor
import com.example.sare.appManager.StringSingleton.primary
import com.example.sare.appManager.StringSingleton.shadowColor
import com.example.sare.appManager.StringSingleton.statusColor
import com.example.sare.appManager.StringSingleton.strokeColor
import com.example.sare.appManager.StringSingleton.successColor
import com.example.sare.appManager.StringSingleton.termsAndConditionPageBackground
import com.example.sare.appManager.StringSingleton.textBlack
import com.example.sare.appManager.StringSingleton.textBlackHeader
import com.example.sare.appManager.StringSingleton.textDark
import com.example.sare.appManager.StringSingleton.textDarkBlack
import com.example.sare.appManager.StringSingleton.textError
import com.example.sare.appManager.StringSingleton.textLight
import com.example.sare.appManager.StringSingleton.textOtp
import com.example.sare.appManager.StringSingleton.textSuccess
import com.example.sare.appManager.StringSingleton.textWhite
import com.example.sare.appManager.StringSingleton.themeColor
import com.example.sare.appManager.StringSingleton.transparent
import com.example.sare.appManager.StringSingleton.viewBackground
import com.example.sare.appManager.StringSingleton.viewHeader
import kotlin.properties.Delegates


object ThemeManager {
    private var selectedColor by Delegates.notNull<Int>()
    private var selectedImage by Delegates.notNull<Drawable>()
    var typeFace: Typeface? = null
    fun colors(activity: Context, color: String): Int {
        when (color) {
            primary -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#02ADAD") else Color.parseColor("#02ADAD")
            textDark -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#ffffff") else Color.parseColor("#6a6767")
            textBlack -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#919191") else Color.parseColor("#000000")
            textDarkBlack -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#ffffff") else Color.parseColor("#000000")
            textBlackHeader -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FCFCFE") else Color.parseColor("#919191")
            textLight -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#ffffff") else Color.parseColor("#212121")
            textWhite -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#000000") else Color.parseColor("#FFFFFF")
            textOtp -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#686767") else Color.parseColor("#919191")
            viewHeader -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#000000") else Color.parseColor("#FFFFFF")
            bodyBackground -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#212121") else Color.parseColor("#FFFFFF")
            //.isDarkMode(activity)) Color.parseColor("#212121") else Color.parseColor("#F2F2F7")
            statusColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#000000") else Color.parseColor("#F2F2F7")
            viewBackground -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#222222") else Color.parseColor("#FFFFFF")
            themeColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#000000") else Color.parseColor("#FFFFFF")
            shadowColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#000000") else Color.parseColor("#C3C3C7")
            ovelayColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#B33B3B3C") else Color.parseColor("#B3C4C4C8")
            dividerColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#5c5a5a") else Color.parseColor("#1A68686C")
            strokeColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#D1D1D1")
            backGround -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#FF005F")
            transparent -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#00000000")

            otpBackground -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#D9D8D8")
            button_login_background -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#FF005F")

            termsAndConditionPageBackground -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#FFFFFF")

            internalScreenBackgroundColor -> selectedColor = if (AppUtill()
                    .isDarkMode(activity)
            ) Color.parseColor("#FFFFFF") else Color.parseColor("#FCFCFC")
            menuHeader -> selectedColor = Color.parseColor("#000000")
            // Success color
            textSuccess -> selectedColor = Color.parseColor("#9BA75A")
            textError -> selectedColor = Color.parseColor("#FC3246")
            finalWhite -> selectedColor = Color.parseColor("#ffffff")
            placeHolderColor -> selectedColor = Color.parseColor("#919191")
            greenColor -> selectedColor = Color.parseColor("#74da4e")
            defaultColor -> selectedColor = Color.parseColor("#bebcbc")
            successColor -> selectedColor = Color.parseColor("#1FF409")
        }
        return selectedColor
    }

    fun image(activity: FragmentActivity, image: String): Drawable {
        when (image) {

            StringSingleton.mainLogo -> selectedImage =
                if (AppUtill().isDarkMode(activity)) ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.login_screen_logo,
                    activity.theme
                )!! else ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.login_screen_logo,
                    activity.theme
                )!!

            StringSingleton.verificationLogo -> selectedImage =
                if (AppUtill().isDarkMode(activity)) ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.logo_login_header,
                    activity.theme
                )!! else ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.logo_login_header,
                    activity.theme
                )!!

            StringSingleton.profileLogo -> selectedImage =
                if (AppUtill().isDarkMode(activity)) ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.ic_email_verification_dk,
                    activity.theme
                )!! else ResourcesCompat.getDrawable(
                    activity.resources,
                    R.drawable.ic_email_verification_lt,
                    activity.theme
                )!!

        }

        return selectedImage
    }

    fun getDrawable(activity: FragmentActivity, color: Int, cornerRadius: Int): GradientDrawable {
        val gd = GradientDrawable()
        gd.shape = GradientDrawable.RECTANGLE
        gd.setColor(color)
        gd.cornerRadius = activity.resources.getDimension(cornerRadius)
        return gd
    }

//    fun signUpPageEditText(activity: FragmentActivity, color: Int, cornerRadius: Int, stroke: Int): GradientDrawable {
//        val gd = GradientDrawable()
//        gd.shape = GradientDrawable.RECTANGLE
//        gd.setColor(color)
//        gd.cornerRadius = activity.resources.getDimension(cornerRadius)
//        gd.setStroke(stroke, ContextCompat.getColor(activity.applicationContext,
//            strokeColor
//        ))
//        return gd
//    }

    fun otpGetDrawable(
        activity: FragmentActivity,
        color: Int,
        cornerRadius: Int,
        stroke: Int,
        strokeColor: Int = R.color.text_black
    ): GradientDrawable {
        val gd = GradientDrawable()
        gd.shape = GradientDrawable.RECTANGLE
        gd.setColor(color)
        gd.cornerRadius = activity.resources.getDimension(cornerRadius)
        gd.setStroke(
            stroke, ContextCompat.getColor(
                activity.applicationContext,
                strokeColor
            )
        )
        return gd
    }

}