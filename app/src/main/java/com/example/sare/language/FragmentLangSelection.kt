package com.example.sare.language

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.extensions.openNewActivity
import com.example.sare.ui.login.LoginActivity
import kotlinx.android.synthetic.main.fragment_lang_selection.view.*

class FragmentLangSelection : Fragment(),
    LangAdapter.LangClick {

    private var root : View? = null
    private var ar : ArrayList<String>? = ArrayList()
    private var adapter : LangAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        root =  inflater.inflate(R.layout.fragment_lang_selection, container,false)

        setData()

        root?.lang_recyclerview?.layoutManager = LinearLayoutManager(context)
        root?.lang_recyclerview?.setHasFixedSize(true)
        root?.lang_recyclerview?.adapter = adapter
        adapter?.setOnLangClick(this)

        setTheme()

        return root
    }

    fun setData(){
        ar?.add("English")
        ar?.add("Gujarati")
        ar?.add("Marathi")
        ar?.add("Malyalam")
        ar?.add("Bengoli")
        adapter = LangAdapter(
            requireContext(),
            ar,
            requireActivity()
        )
    }

    private fun setTheme(){
        requireActivity().window.statusBarColor =
            ThemeManager.colors(
                requireContext(),
                StringSingleton.bodyBackground
            )
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR

        if (AppUtill().isDarkMode(requireActivity())){
            requireActivity().window.navigationBarColor =
                ThemeManager.colors(
                    requireContext(),
                    StringSingleton.themeColor
                )
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }

        root?.main_layout?.setBackgroundColor(
            ThemeManager.colors(
                requireContext(),
                StringSingleton.bodyBackground
            )
        )
        root?.lang_header_title?.setBackgroundColor(
            ThemeManager.colors(
                requireContext(),
                StringSingleton.themeColor
            )
        )
        root?.lang_header_title?.setTextColor(
            ThemeManager.colors(
                requireContext(),
                StringSingleton.textBlack
            )
        )
    }

    override fun langClick() {
        activity?.openNewActivity(LoginActivity::class.java)
    }
}