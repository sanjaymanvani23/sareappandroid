package com.example.sare.language

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import kotlinx.android.synthetic.main.lang_selection_list.view.*

class LangAdapter(private val context : Context, arrayList: ArrayList<String>?, private val activity: FragmentActivity) : RecyclerView.Adapter<LangAdapter.ViewHolder>() {

    private var list: ArrayList<String>? = ArrayList()
    private var onLangClickListner: LangClick? = null

    init {
        list = arrayList
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.lang_selection_list, parent , false))
    }

    fun setOnLangClick(onlangClick : LangClick){
        onLangClickListner = onlangClick
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.langText.text = list?.get(position)
    }

    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val langText = view.lang_text
        val langLayout = view.lang_layout

        init {
            langText.background =
                ThemeManager.getDrawable(
                    activity,
                    ThemeManager.colors(
                        context,
                        StringSingleton.textWhite
                    ),
                    R.dimen.io_list_corner_radius
                )
            langText.setTextColor(
                ThemeManager.colors(
                    activity,
                    StringSingleton.textBlackHeader
                )
            )

            langText.setOnClickListener {
                langLayout.background =
                    ThemeManager.getDrawable(
                        activity,
                        ThemeManager.colors(
                            context,
                            StringSingleton.primary
                        ),
                        R.dimen.io_list_corner_radius
                    )
                langText.background =
                    ThemeManager.getDrawable(
                        activity,
                        ThemeManager.colors(
                            context,
                            StringSingleton.primary
                        ),
                        R.dimen.io_list_corner_radius
                    )
                langText.setTextColor(
                    ThemeManager.colors(
                        context,
                        StringSingleton.viewHeader
                    )
                )
                onLangClickListner?.langClick()
            }

        }
    }
    interface LangClick{
        fun langClick()
    }
}
