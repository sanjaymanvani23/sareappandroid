package com.example.sare

import android.content.Context
import com.d.parastask.api.ApiHelper
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.Header
import retrofit2.http.Part


class MainRepository constructor(private val apiHelper: ApiHelper) {

    suspend fun uploadImage(@Part("colorCode") colorCode: RequestBody,
                            @Part file: MultipartBody.Part,
                            @Header("Authorization") token: String) = apiHelper.uploadImage(colorCode,file,token)

}