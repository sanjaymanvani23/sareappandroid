package com.example.sare.videoRecording.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.beauty_list_adapter.view.*

class BeautyFilterListAdapter(private val context : Context, private val parent : List<FilterType>) : RecyclerView.Adapter<BeautyFilterListAdapter.ViewHolder>() {

    private var mOnClickListen: OnClickListen? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.beauty_list_adapter, parent,false))
    }

    override fun getItemCount(): Int {
        return parent.size
    }

    fun setOnClick(onClickListen: OnClickListen){
        mOnClickListen = onClickListen
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
         val layout = view.layout

        init {
            layout.setOnClickListener {
                mOnClickListen?.onBeautyImageClick(view = it)
            }
        }
    }

    interface OnClickListen{
        fun onBeautyImageClick(view: View)
    }
}
