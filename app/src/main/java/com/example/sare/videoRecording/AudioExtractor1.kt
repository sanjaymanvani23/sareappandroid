package com.example.sare.videoRecording

import android.media.MediaCodec
import android.media.MediaExtractor
import android.media.MediaMuxer
import android.util.Log
import android.widget.Toast
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer
public class AudioExtractor1 {
    var TAG = "muxing"
    fun muxing(des: File, videoFilePath: String, audioFilePath: String) {

        var outputFile = ""
        try {
            /* val file = File(
             desVideoPath
         )*/
            //file.createNewFile()
            // outputFile = file.absolutePath
            val videoExtractor = MediaExtractor()
            //val afdd: AssetFileDescriptor = assets.openFd("Produce.MP4")
            /*videoExtractor.setDataSource(
            afdd.getFileDescriptor(),
            afdd.getStartOffset(),
            afdd.getLength()
        )*/
            videoExtractor.setDataSource(videoFilePath)
            val audioExtractor = MediaExtractor()
            audioExtractor.setDataSource(audioFilePath)
            Log.d(TAG, "Video Extractor Track Count " + videoExtractor.trackCount)
            Log.d(TAG, "Audio Extractor Track Count " + audioExtractor.trackCount)
            val muxer = MediaMuxer(des.path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)
            videoExtractor.selectTrack(0)
            val videoFormat = videoExtractor.getTrackFormat(0)
            val videoTrack = muxer.addTrack(videoFormat)
            audioExtractor.selectTrack(0)
            val audioFormat = audioExtractor.getTrackFormat(0)
            val audioTrack = muxer.addTrack(audioFormat)
            Log.d(TAG, "Video Format $videoFormat")
            Log.d(TAG, "Audio Format $audioFormat")
            var sawEOS = false
            var frameCount = 0
            val offset = 100
            val sampleSize = 256 * 1024
            val videoBuf = ByteBuffer.allocate(sampleSize)
            val audioBuf = ByteBuffer.allocate(sampleSize)
            val videoBufferInfo: MediaCodec.BufferInfo = MediaCodec.BufferInfo()
            val audioBufferInfo: MediaCodec.BufferInfo = MediaCodec.BufferInfo()
            videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC)
            audioExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC)
            muxer.start()
            while (!sawEOS) {
                videoBufferInfo.offset = offset
                videoBufferInfo.size = videoExtractor.readSampleData(videoBuf, offset)
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    Log.d(TAG, "saw input EOS.")
                    sawEOS = true
                    videoBufferInfo.size = 0
                } else {
                    videoBufferInfo.presentationTimeUs = videoExtractor.sampleTime
                    videoBufferInfo.flags = videoExtractor.sampleFlags
                    muxer.writeSampleData(videoTrack, videoBuf, videoBufferInfo)
                    videoExtractor.advance()
                    frameCount++
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs + " Flags:" + videoBufferInfo.flags + " Size(KB) " + videoBufferInfo.size / 1024
                    )
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs + " Flags:" + audioBufferInfo.flags + " Size(KB) " + audioBufferInfo.size / 1024
                    )
                }
            }
//        Toast.makeText(applicationContext, "frame:$frameCount", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "frame:$frameCount")
            var sawEOS2 = false
            var frameCount2 = 0
            while (!sawEOS2) {
                frameCount2++
                audioBufferInfo.offset = offset
                audioBufferInfo.size = audioExtractor.readSampleData(audioBuf, offset)
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    Log.d(TAG, "saw input EOS.")
                    sawEOS2 = true
                    audioBufferInfo.size = 0
                } else {
                    audioBufferInfo.presentationTimeUs = audioExtractor.sampleTime
                    audioBufferInfo.flags = audioExtractor.sampleFlags
                    muxer.writeSampleData(audioTrack, audioBuf, audioBufferInfo)
                    audioExtractor.advance()
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs + " Flags:" + videoBufferInfo.flags + " Size(KB) " + videoBufferInfo.size / 1024
                    )
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs + " Flags:" + audioBufferInfo.flags + " Size(KB) " + audioBufferInfo.size / 1024
                    )
                }
            }
//        Toast.makeText(applicationContext, "frame:$frameCount2", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "frame:$frameCount2")

            muxer.stop()
            muxer.release()
        } catch (e: IOException) {
            e.printStackTrace()
            Log.d(TAG, "Mixer Error 1 " + e.message)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.d(TAG, "Mixer Error 2 " + e.message)
        }
    }
}