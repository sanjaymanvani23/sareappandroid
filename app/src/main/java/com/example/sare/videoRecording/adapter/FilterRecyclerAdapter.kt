package com.example.sare.videoRecording.adapter

import android.content.Context
import android.graphics.Color
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.extensions.gone
import com.example.sare.extensions.visible
import com.example.sare.videoRecording.FilterType
import com.example.sare.videoRecording.model.FliterModel
import kotlinx.android.synthetic.main.bottom_slider.*
import kotlinx.android.synthetic.main.bottom_slider.view.*
import kotlinx.android.synthetic.main.filter_list.view.*
import kotlinx.android.synthetic.main.filter_list.view.image

class FilterRecyclerAdapter(private val context: Context, private val parent: List<FilterType>?,private val filterList: List<FliterModel>?) :
    RecyclerView.Adapter<FilterRecyclerAdapter.ViewHolder>() {
    private var savedWidth = 0
    private var savedHight = 0
    private var mOnFilterClick: OnFilterClick? = null
    private var clickPosition = 0
    var clickFlag = true
    lateinit var holder: ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.filter_list, parent, false)
        )
    }

    fun setOnClick(onFilterClick: OnFilterClick) {
        mOnFilterClick = onFilterClick
    }

    override fun getItemCount(): Int {
        return parent!!.size
    }
    fun setClickPosition(Position: Int, clickFlag: Boolean) {
        this.clickPosition = Position
        //this.clickFlag = clickFlag
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this.holder = holder

        Log.e("Size Of List Size","${parent!!.size}")

        if(position==0)
        {
            holder.image.setImageResource(R.drawable.background_follow_button)
        }
        else{
            holder.image.setImageResource(R.drawable.girl_image)
        }
        if(clickFlag)
        {
             clickFlag=false
             savedHight =holder.image.height
             savedWidth=holder.image.width
        }
        holder.image.setOnClickListener {
            if (clickPosition == position) {

                if (filterList?.get(position)!!.status) {
                   /* params.width = holder.image.width - 30
                    params.height = holder.image.height - 25
                    filterList?.get(position)!!.status=false
                    holder.image.borderColor = Color.parseColor("#FF005F")
                    mOnFilterClick?.onStartRecordView()
                    holder.image.layoutParams = params
                    notifyItemChanged(position)*/
                    filterList?.get(position)!!.status=false
                    holder.image.animate()!!.translationY(0F).scaleX(0.8F).scaleY(0.8F).setDuration(
                        350
                    ).start()
                    holder.image.borderColor = Color.parseColor("#FF005F")
                    mOnFilterClick?.onStartRecordView()

                } else {
                   /* params.height =holder.image.height + 25
                    params.width = holder.image.width + 30
                    holder.image.borderColor = Color.parseColor("#00000000")
                    filterList?.get(position)!!.status=true
                    mOnFilterClick?.onPauseRecordView()
                    holder.image.layoutParams = params
                    notifyItemChanged(position)*/
                    filterList?.get(position)!!.status=true
                    holder.image.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
                    holder.image.borderColor = Color.parseColor("#00000000")
                    mOnFilterClick?.onPauseRecordView()
                }

            }
        }

        if(!filterList?.get(position)!!.status)
        {

            holder.image.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
           /* val params: ViewGroup.LayoutParams = holder.image.layoutParams
            params.height =holder.image.height + 15
            params.width = holder.image.width + 25*/
            holder.image.borderColor = Color.parseColor("#00000000")
           // holder.image.layoutParams = params
        }

    }


    fun reset() {
        notifyDataSetChanged()
    }

    fun setCurrentPosition(position: Int) {
        Log.e("roleit pos","$position")
        filterList?.get(position)!!.status=false
        holder.image.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        holder.image.borderColor = Color.parseColor("#00000000")
    }

    fun resetpostion(position: Int) {
        if(!filterList?.get(position)!!.status)
        {
            val params: ViewGroup.LayoutParams = holder.image.layoutParams
            params.height =savedHight
            params.width = savedWidth
            holder.image.borderColor = Color.parseColor("#00000000")
            holder.image.layoutParams = params
            notifyItemChanged(position)
        }

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.image
    }

    interface   OnFilterClick {
        fun onClick(position: Int, view: View)
        fun onStartRecordView()
        fun onPauseRecordView()
    }
}
