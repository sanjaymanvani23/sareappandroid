package com.example.sare.videoRecording.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.effect_list.view.*

class EffectListAdapter(private val context : Context, private val parent : List<FilterType>) : RecyclerView.Adapter<EffectListAdapter.ViewHolder>() {

    private var currentPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.effect_list, parent,false))
    }

    override fun getItemCount(): Int {
        return parent.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.effect_image.visibility = GONE
        if (currentPosition == position){
            holder.effect_image.visibility = VISIBLE
        }

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        var layer_selection = view.layer_selection
        var effect_image = view.effect_image

        init {

            layer_selection.setOnClickListener {
                currentPosition = adapterPosition
                notifyDataSetChanged()
            }

        }

    }

}
