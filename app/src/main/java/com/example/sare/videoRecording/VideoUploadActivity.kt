package com.example.sare.videoRecording

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.webkit.MimeTypeMap
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.api.ApiService
import com.example.sare.api.RetrofitBuilder
import com.example.sare.appManager.StringSingleton
import com.example.sare.bottom_navigation.BottomBarActivity
import com.example.sare.customeview.HashTagView.HashTag
import com.example.sare.data.ResponseData
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.gone
import com.example.sare.extensions.openNewActivity
import com.example.sare.extensions.visible
import com.example.sare.ui.login.LoginActivity
import com.example.sare.videoRecording.adapter.VideoFramesAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.activity_video_upload.*
import kotlinx.android.synthetic.main.layout_post_options.*
import kotlinx.android.synthetic.main.layout_post_options1.*
import kotlinx.android.synthetic.main.layout_post_options_2.*
import kotlinx.android.synthetic.main.popup_upload_success.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

@RequiresApi(Build.VERSION_CODES.M) class VideoUploadActivity : Activity() {

    var token: String? = null
    var videoFile: String? = null
    var audioFile: String? = null
    var audioFileNew: String? = null
    var selectedCoverImage: Uri? = null
    var fileCover: File? = null
    var audioTrack = false
    var path: String? = null
    var mTextHashTag: HashTag? = null
    var hashTags = ArrayList<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_upload)
        getData()
        path = intent.getStringExtra("videoFile")
        createVideoFrames(path!!)

        setListeners()
        //        watchVideo()
        setBottomSheet()
        setBottomSheet1()
        setBottomSheet2()
        val additionalSymbols = charArrayOf('#')
        mTextHashTag = HashTag.Creator.create(resources.getColor(R.color.colorPrimary), null, additionalSymbols)
        mTextHashTag!!.handle(et_about_video)
    }

    private fun getData() {
        val preferences: SharedPreferences = getSharedPreferences(StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
        videoFile = intent.getStringExtra("videoFile")

        if (videoFile.isNullOrEmpty()) {
            customToastMain("Video File Not Exit", 0)
        } else {
            if (isVideoHaveAudioTrack(videoFile!!)) {
                createNewFileMp3()
                AudioVideoTask().execute(videoFile, "2")
            }
        }
    }

    private fun isVideoHaveAudioTrack(path: String): Boolean {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(path)
        val hasAudioStr = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO)
        audioTrack = hasAudioStr != null && hasAudioStr == "yes"
        return audioTrack
    }

    fun createNewFileMp3() {
        val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
        val now = Date()
        val fileName: String = formatter.format(now).toString() + ".mp3"
        try {
            val root = File(
                Environment.getExternalStorageDirectory().toString() + File.separator + "Music_Folder", "AudioFiles")

            if (!root.exists()) {
                root.mkdirs()
            }
            val mp3File = File(root, fileName)
            audioFileNew = mp3File.absolutePath
            audioFile = audioFileNew
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    inner class AudioVideoTask : AsyncTask<String?, Int?, String?>() {

        override fun doInBackground(vararg str: String?): String? {
            AudioExtractor().genVideoUsingMuxer(videoFile, audioFileNew, -1, -1, true, false)
            audioFile = audioFile
            return "Success"
        }

        override fun onPostExecute(result: String?) {
        }
    }


    private fun setListeners() {
        imgBack.setOnClickListener {
            onBackPressed()
        }
        et_about_video.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                txtCounter?.text = "${s?.length}/150"
                if (s?.length!! >= 150) {
                    customToastMain(getString(R.string.description_can_more_than_error), 0)
                }
            }
        })

        bt_post.setOnClickListener {
            if (selectedCoverImage == null) {
                customToastMain(getString(R.string.please_select_thumbnail_for_video), 0)
            } else {
                //                bt_post?.startAnimation()
                uploadFile(false)
            }
        }

        bt_draft.setOnClickListener {
            if (selectedCoverImage == null) {
                customToastMain(getString(R.string.please_select_thumbnail_for_video), 0)
            } else {
                uploadFile(true)
            }
        }

    }

    private fun uploadFile(isDraftFlag: Boolean) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }

        val service: ApiService = RetrofitBuilder.getRetrofit().create(ApiService::class.java)

        val fileVideo1 = File(videoFile)

        fileCover = File(getPath(selectedCoverImage))

        val requestFile: RequestBody = fileVideo1.asRequestBody(getMimeType(fileVideo1)!!.toMediaTypeOrNull())

        val requestFile2: RequestBody = fileCover!!.asRequestBody(
            contentResolver.getType(selectedCoverImage!!)!!.toMediaTypeOrNull())

        val fileCoverImage = MultipartBody.Part.createFormData("file", fileCover!!.name, requestFile2)
        val fileVideo = MultipartBody.Part.createFormData("file", fileVideo1.name, requestFile)
        hashTags = mTextHashTag!!.getAllHashTags(true) as ArrayList<String>
        val tagsList: ArrayList<MultipartBody.Part> = ArrayList()
        tagsList.add(MultipartBody.Part.createFormData("tags", hashTags.toString()))
        // add another part within the multipart request
        val titleString = "MY VIDEO"
        val descString = et_about_video.text.toString()

        val isDraftString = isDraftFlag.toString()
        val nameString = ""
        val allowToWatchString = tv_allow_to_watch.text.toString()
        val videoToCommentString = tv_allow_to_comment.text.toString()
        val canAnyoneDuetString = tv_allow_to_duet.text.toString()

        val title: RequestBody = titleString.toRequestBody(MultipartBody.FORM)
        val desc: RequestBody = descString.toRequestBody(MultipartBody.FORM)
        val isDraft: RequestBody = isDraftString.toRequestBody(MultipartBody.FORM)

        val name: RequestBody = nameString.toRequestBody(MultipartBody.FORM)

        val allowToWatch: RequestBody = allowToWatchString.toRequestBody(MultipartBody.FORM)
        val videoToComment: RequestBody = videoToCommentString.toRequestBody(MultipartBody.FORM)

        val canAnyoneDuet: RequestBody = canAnyoneDuetString.toRequestBody(MultipartBody.FORM)

        val call: Call<ResponseData>
        if (audioTrack) {
            val fileAudio1 = File(audioFile)
            val requestFile1: RequestBody = fileAudio1.asRequestBody(getMimeType(fileAudio1)!!.toMediaTypeOrNull())
            val fileAudio = MultipartBody.Part.createFormData("file", fileAudio1.name, requestFile1)
            call = service.videoStore(
                fileCoverImage,
                fileAudio,
                fileVideo,
                title,
                tagsList,
                isDraft,
                name,
                desc,
                allowToWatch,
                videoToComment,
                canAnyoneDuet,
                "Berear " + token.toString())
        } else {
            call = service.videoStore(
                fileCoverImage,
                fileVideo,
                title,
                tagsList,
                isDraft,
                name,
                desc,
                allowToWatch,
                videoToComment,
                canAnyoneDuet,
                "Berear " + token.toString())
        }

        call.enqueue(object : Callback<ResponseData> {
            override fun onResponse(call: Call<ResponseData?>?, response: Response<ResponseData?>?) {
                runOnUiThread {
                    progressDialog.hide()
                }
                //  bt_post?.revertAnimation()

                val status = response?.body()?.status


                if (status == 200) {
                    if (isDraftFlag === true) {
                        customToastMain(getString(R.string.draft_video_success_message), 1)
                        showUploadPopup()
                    } else {
                      //  customToastMain(getString(R.string.upload_video_success_message), 1)
                        showUploadPopup()
                    }
                } else {
                    customToastMain(getString(R.string.api_error_message), 0)
                }
            }

            override fun onFailure(call: Call<ResponseData?>, t: Throwable) {
                progressDialog.hide()
                //   bt_post?.revertAnimation()
                customToastMain(getString(R.string.api_error_message), 0)

            }
        })
    }

    private fun initRecyclerView(frameList: ArrayList<Bitmap>) {
        rv_thumbnail.apply {
            layoutManager = LinearLayoutManager(
                rv_thumbnail.context, RecyclerView.HORIZONTAL, false)

            adapter = VideoFramesAdapter(frameList, context)

            (adapter as VideoFramesAdapter).onItemClick = { data ->
                selectedCoverImage = getUri(this@VideoUploadActivity, data)
            }

        }

    }

    fun getMimeType(file: File): String? {
        var type: String? = null
        val url = file.toString()
        val extension: String = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase())
        }
        if (type == null) {
            type = "image/*" // fallback type. You might set it to */*
        }
        return type
    }

    /*private fun watchVideoOptions() {
        val dialog = BottomSheetDialog(this@VideoUploadActivity, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.layout_post_options)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.viewVideo.text = "Who can view your video"
        dialog.txt1.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt2.setOnClickListener {
            dialog.txt1.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt3.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt1.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt4.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt1.isChecked = false
        }
        if (tv_allow_to_watch.text == "Friends") {
            dialog.txt2.isChecked = true
        } else if (tv_allow_to_watch.text == "Followers") {
            dialog.txt3.isChecked = true
        } else if (tv_allow_to_watch.text == "Everyone") {
            dialog.txt4.isChecked = true
        }
        dialog.txt1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_watch.text = dialog.txt1.text
            }
        }
        dialog.txt2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_watch.text = dialog.txt2.text
            }
        }
        dialog.txt3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_watch.text = dialog.txt3.text
            }
        }
        dialog.txt4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_watch.text = dialog.txt4.text
            }
        }
        dialog.show()
    }*/

    private fun setBottomSheet() {
        val llBottomSheet = bottom_sheet_options

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        rl_allow_to_watch.setOnClickListener {
            overlay_view?.visible()

            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

                viewVideo.text = "Who can view your video"
                txt1.setOnClickListener {
                    txt2.isChecked = false
                    txt3.isChecked = false
                    txt4.isChecked = false
                    txt1.isChecked = true

                }
                txt2.setOnClickListener {
                    txt1.isChecked = false
                    txt3.isChecked = false
                    txt4.isChecked = false
                    txt2.isChecked = true

                }
                txt3.setOnClickListener {
                    txt2.isChecked = false
                    txt1.isChecked = false
                    txt4.isChecked = false
                    txt3.isChecked = true

                }
                txt4.setOnClickListener {
                    txt2.isChecked = false
                    txt3.isChecked = false
                    txt1.isChecked = false
                    txt4.isChecked = true

                }
                if (tv_allow_to_watch.text == "Friends") {
                    txt2.isChecked = true
                } else if (tv_allow_to_watch.text == "Followers") {
                    txt3.isChecked = true
                } else if (tv_allow_to_watch.text == "Everyone") {
                    txt4.isChecked = true
                } else {
                    txt2.isChecked = false
                    txt3.isChecked = false
                    txt4.isChecked = false

                }
                txt1.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_watch.text = txt1.text
                    }
                }
                txt2.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_watch.text = txt2.text

                    }
                }
                txt3.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_watch.text = txt3.text

                    }
                }
                txt4.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_watch.text = txt4.text

                    }
                }

            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }
        overlay_view?.setOnClickListener {
            overlay_view?.gone()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view?.visible()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun setBottomSheet1() {
        val llBottomSheet = bottom_sheet_options1

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        rl_allow_to_comment.setOnClickListener {
            overlay_view1?.visible()

            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                viewVideo1.text = "Who can comment on your video"

                txt11.setOnClickListener {
                    txt21.isChecked = false
                    txt31.isChecked = false
                    txt41.isChecked = false
                    txt11.isChecked = true

                }
                txt21.setOnClickListener {
                    txt11.isChecked = false
                    txt31.isChecked = false
                    txt41.isChecked = false
                    txt21.isChecked = true

                }
                txt31.setOnClickListener {
                    txt21.isChecked = false
                    txt11.isChecked = false
                    txt41.isChecked = false
                    txt31.isChecked = true

                }
                txt41.setOnClickListener {
                    txt21.isChecked = false
                    txt31.isChecked = false
                    txt11.isChecked = false
                    txt41.isChecked = true

                }
                if (tv_allow_to_comment.text == "Friends") {
                    txt21.isChecked = true
                } else if (tv_allow_to_comment.text == "Followers") {
                    txt31.isChecked = true
                } else if (tv_allow_to_comment.text == "Everyone") {
                    txt41.isChecked = true
                } else {
                    txt21.isChecked = false
                    txt31.isChecked = false
                    txt41.isChecked = false
                }
                txt11.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_comment.text = txt11.text
                    }
                }
                txt21.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_comment.text = txt21.text
                    }
                }
                txt31.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_comment.text = txt31.text
                    }
                }
                txt41.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_comment.text = txt41.text
                    }
                }


            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }


        overlay_view1?.setOnClickListener {
            overlay_view1?.gone()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }



        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view1?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view1?.visible()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view1?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view1?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun setBottomSheet2() {
        val llBottomSheet1 = bottom_sheet_options_2

        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet1 as RelativeLayout)

        rl_allow_duet_video.setOnClickListener {
            overlay_view2?.visible()

            if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                viewVideo2.text = "Who can duet your video"

                txt12.setOnClickListener {
                    txt22.isChecked = false
                    txt32.isChecked = false
                    txt42.isChecked = false
                    txt12.isChecked = true

                }
                txt22.setOnClickListener {
                    txt12.isChecked = false
                    txt32.isChecked = false
                    txt42.isChecked = false
                    txt22.isChecked = true

                }
                txt32.setOnClickListener {
                    txt22.isChecked = false
                    txt12.isChecked = false
                    txt42.isChecked = false
                    txt32.isChecked = true

                }
                txt42.setOnClickListener {
                    txt22.isChecked = false
                    txt32.isChecked = false
                    txt12.isChecked = false
                    txt42.isChecked = true

                }
                if (tv_allow_to_duet.text == "Friends") {
                    txt22.isChecked = true
                } else if (tv_allow_to_duet.text == "Followers") {
                    txt32.isChecked = true
                } else if (tv_allow_to_duet.text == "Everyone") {
                    txt42.isChecked = true
                }
                txt12.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_duet.text = txt12.text
                    }
                }
                txt22.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_duet.text = txt22.text
                    }
                }
                txt32.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_duet.text = txt32.text
                    }
                }
                txt42.setOnCheckedChangeListener { buttonView, isChecked ->
                    if (isChecked) {
                        tv_allow_to_duet.text = txt42.text
                    }
                }

            } else {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }




        overlay_view2?.setOnClickListener {

            overlay_view2?.gone()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            Log.d("tag", "setOnClickListener called!!!!")
        }



        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                        overlay_view2?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_HIDDEN called!!!!")
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        overlay_view2?.visible()
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        overlay_view2?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_COLLAPSED called!!!!")
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        overlay_view2?.gone()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                        Log.d("tag", "STATE_DRAGGING called!!!!")
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

    }

    private fun showUploadPopup() {
        val dialog = Dialog(this, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.popup_upload_success)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        dialog.btnDone.setOnClickListener {
            dialog.dismiss()
            startActivity(
                Intent(this@VideoUploadActivity, BottomBarActivity::class.java).putExtra("type", "5"))
            preferences.edit().putString("isPosted", "true").apply()
            finish()
        }
        dialog.show()
    }

    /*private fun commentVideoOptions() {
        val dialog = BottomSheetDialog(this@VideoUploadActivity, R.style.DialogSlideAnim)
        dialog.setContentView(R.layout.layout_post_options)
        val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.BOTTOM
        lp.windowAnimations = R.style.DialogAnimation
        dialog.window?.attributes = lp
        dialog.setCancelable(true)
        dialog.viewVideo.text = "Who can comment on your video"
        dialog.txt1.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt2.setOnClickListener {
            dialog.txt1.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt3.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt1.isChecked = false
            dialog.txt4.isChecked = false
        }
        dialog.txt4.setOnClickListener {
            dialog.txt2.isChecked = false
            dialog.txt3.isChecked = false
            dialog.txt1.isChecked = false
        }
        if (tv_allow_to_comment.text == "Friends") {
            dialog.txt2.isChecked = true
        } else if (tv_allow_to_comment.text == "Followers") {
            dialog.txt3.isChecked = true
        } else if (tv_allow_to_comment.text == "Everyone") {
            dialog.txt4.isChecked = true
        }
        dialog.txt1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_comment.text = dialog.txt1.text
            }
        }
        dialog.txt2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_comment.text = dialog.txt2.text
            }
        }
        dialog.txt3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_comment.text = dialog.txt3.text
            }
        }
        dialog.txt4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tv_allow_to_comment.text = dialog.txt4.text
            }
        }
        dialog.show()
    }*/

    private fun duetVideoOptions() {
        /* val dialog = BottomSheetDialog(this@VideoUploadActivity, R.style.DialogSlideAnim)
         dialog.setContentView(R.layout.layout_post_options)
         val lp: WindowManager.LayoutParams = WindowManager.LayoutParams()
         lp.copyFrom(dialog.window?.attributes)
         lp.width = WindowManager.LayoutParams.MATCH_PARENT
         lp.height = WindowManager.LayoutParams.WRAP_CONTENT
         lp.gravity = Gravity.BOTTOM
         lp.windowAnimations = R.style.DialogAnimation
         dialog.window?.attributes = lp
         dialog.setCancelable(true)
         dialog.viewVideo.text = "Who can duet your video"
         dialog.txt1.setOnClickListener {
             dialog.txt2.isChecked = false
             dialog.txt3.isChecked = false
             dialog.txt4.isChecked = false
         }
         dialog.txt2.setOnClickListener {
             dialog.txt1.isChecked = false
             dialog.txt3.isChecked = false
             dialog.txt4.isChecked = false
         }
         dialog.txt3.setOnClickListener {
             dialog.txt2.isChecked = false
             dialog.txt1.isChecked = false
             dialog.txt4.isChecked = false
         }
         dialog.txt4.setOnClickListener {
             dialog.txt2.isChecked = false
             dialog.txt3.isChecked = false
             dialog.txt1.isChecked = false
         }
         if (tv_allow_to_duet.text == "Friends") {
             dialog.txt2.isChecked = true
         } else if (tv_allow_to_duet.text == "Followers") {
             dialog.txt3.isChecked = true
         } else if (tv_allow_to_duet.text == "Everyone") {
             dialog.txt4.isChecked = true
         }
         dialog.txt1.setOnCheckedChangeListener { buttonView, isChecked ->
             if (isChecked) {
                 tv_allow_to_duet.text = dialog.txt1.text
             }
         }
         dialog.txt2.setOnCheckedChangeListener { buttonView, isChecked ->
             if (isChecked) {
                 tv_allow_to_duet.text = dialog.txt2.text
             }
         }
         dialog.txt3.setOnCheckedChangeListener { buttonView, isChecked ->
             if (isChecked) {
                 tv_allow_to_duet.text = dialog.txt3.text
             }
         }
         dialog.txt4.setOnCheckedChangeListener { buttonView, isChecked ->
             if (isChecked) {
                 tv_allow_to_duet.text = dialog.txt4.text
             }
         }
         dialog.show()*/
    }

    fun createVideoFrames(videoFile: String) {
        var retriever: MediaMetadataRetriever? = null
        var inputStream: FileInputStream? = null
        try {
            retriever = MediaMetadataRetriever()
            inputStream = FileInputStream(File(videoFile).absolutePath)
            retriever.setDataSource(inputStream.fd)
            val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)?.toLong()

            val frameList: ArrayList<Bitmap> = ArrayList<Bitmap>()

            val duration_millisec = time!!.toInt() //duration in millisec

            val duration_second = duration_millisec / 1000 //millisec to sec.

            val frames_per_second = 2 //no. of frames want to retrieve per second

            val numeroFrameCaptured = frames_per_second * duration_second

            /*   for (i in 0..4) {
                   //setting time position at which you want to retrieve frames
                   frameList.add(retriever.getFrameAtTime(5000000 * i.toLong())!!)
               }*/

            val interval: Long = ((duration_second - 0) / (5 - 1)).toLong()

            for (i in 0 until 5) {
                val frameTime: Long = 0 + interval * i
                var bitmap: Bitmap? = retriever.getFrameAtTime(
                    frameTime * 5000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC) ?: continue
                try {
                    frameList.add(bitmap!!)
                } catch (t: Throwable) {
                    t.printStackTrace()
                }

            }
            initRecyclerView(frameList)
        } catch (e: Exception) {
            println("Exception============$e")
        }
    }

    fun getUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(
            inContext.contentResolver, inImage, "IMG_" + Calendar.getInstance().time, null)
        return Uri.parse(path)
    }

    private fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = contentResolver.query(uri!!, projection, null, null, null) ?: return null
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s = cursor.getString(column_index)
        cursor.close()
        return s
    }

    /* fun getPath(context: Context, uri: Uri): String? {
         val isKitKat: Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
         // DocumentProvider
         if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
             // ExternalStorageProvider
             if (isExternalStorageDocument(uri)) {
                 val docId: String = DocumentsContract.getDocumentId(uri)
                 val split = docId.split(":".toRegex()).toTypedArray()
                 val type = split[0]
                 if ("primary".equals(type, ignoreCase = true)) {
                     return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                 }
                 // TODO handle non-primary volumes
             } else if (isDownloadsDocument(uri)) {
                 val id: String = DocumentsContract.getDocumentId(uri)
                 val contentUri: Uri = ContentUris.withAppendedId(
                     Uri.parse("content://downloads/public_downloads"),
                     java.lang.Long.valueOf(id)
                 )
                 return getDataColumn(context, contentUri, null, null)
             } else if (isMediaDocument(uri)) {
                 val docId: String = DocumentsContract.getDocumentId(uri)
                 val split = docId.split(":".toRegex()).toTypedArray()
                 val type = split[0]
                 var contentUri: Uri? = null
                 if ("image" == type) {
                     contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                 } else if ("video" == type) {
                     contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                 } else if ("audio" == type) {
                     contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                 }
                 val selection = "_id=?"
                 val selectionArgs = arrayOf(
                     split[1]
                 )
                 return getDataColumn(context, contentUri, selection, selectionArgs)
             }
         } else if ("content".equals(uri.scheme, ignoreCase = true)) {
             return getDataColumn(context, uri, null, null)
         } else if ("file".equals(uri.scheme, ignoreCase = true)) {
             return uri.path
         }
         return null
     }*/

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val column_index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    override fun onDestroy() {
        super.onDestroy()
        runOnUiThread {
            //  bt_post?.dispose()
        }
    }

}
