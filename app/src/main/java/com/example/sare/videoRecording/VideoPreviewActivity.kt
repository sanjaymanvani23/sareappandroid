package com.example.sare.videoRecording

import android.content.Intent
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.e.vediotrimmer.interfaces.VideoTrimListener
import com.example.sare.CustomProgressDialogNew
import com.example.sare.R
import com.example.sare.extensions.createVideoFile
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.invisible
import com.example.sare.extensions.visible
import com.example.sare.util.OptiConstant
import com.example.sare.util.OptiConstant.Companion.SPEED_0_3
import com.example.sare.util.OptiConstant.Companion.SPEED_0_5
import com.example.sare.util.OptiConstant.Companion.SPEED_1
import com.example.sare.util.OptiConstant.Companion.SPEED_2
import com.example.sare.util.OptiConstant.Companion.SPEED_3
import com.example.sare.util.OptiFFMpegCallback
import com.example.sare.util.OptiVideoEditor
import kotlinx.android.synthetic.main.activity_video_preview.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class VideoPreviewActivity : AppCompatActivity(), VideoTrimListener {

    var videoFile: String? = null
    var audioFile: String? = null
    var videoPathGallery: String? = null
    var newVideoFile: String? = null
    var speed: String? = null
    var speedTempo: String? = null

    var audioTrack = false
    var videoAudioFile: String? = null
    lateinit var progressDialog : CustomProgressDialogNew
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_preview)

        videoFile = intent.getStringExtra("videoFile")
        audioFile = intent.getStringExtra("audioFile")
        speed = intent.getStringExtra("speed")
        videoAudioFile = audioFile
        videoPathGallery = intent.getStringExtra("path")
        Log.e("Tag  MixerVideo","$speed")
        Log.e("Tag  videoFile","$videoFile")
        progressDialog = CustomProgressDialogNew(this)
        if (trimmerView != null) {
            if(videoFile.isNullOrEmpty())
            {
                setVideoPlayerUrl(videoPathGallery!!)
            }
            else
            {
                trimmerView.invisible()
                if (!audioFile.toString().isNullOrEmpty() && audioFile.toString() != "null") {

                    Log.e("Tag  audioFile"," audioFile not emeptu$audioFile")

                    if (isVideoHaveAudioTrack(audioFile!!)) {
                        createNewMp4File()
                        Log.e("Tag  createNewMp4File"," createNewMp4File $audioFile")
                        var fileExtension = videoAudioFile!!.substring(videoAudioFile!!.lastIndexOf("."))
                        if (fileExtension.equals(".mp4", ignoreCase = true)) {
                            createNewFileMp3()
                            AudioVideoTask().execute(videoFile, "2")
                        } else {
                            Log.e("Tag  MixerAudioFile"," MixerAudioFile $audioFile")
                            MixerAudioFile()
                        }
                    }
                    else{
                        Log.e("Tag  audioFile"," audioFile not in file$audioFile")
                    }
                }
                else{
                    MixerVideoPlayBackSpeed()
                }

            }

        }
    }

    fun createNewMp4File() {
        val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
        val now = Date()
        val fileName: String = formatter.format(now).toString() + ".mp4"
        try {
            val root = File(
                Environment.getExternalStorageDirectory().toString() + File.separator + "Music_Folder", "AudioFiles")
            //File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs()
            }
            Log.d("tag","fileName:::$fileName")
            Log.d("tag","root:::$root")
            val mp3File = File(root, fileName)
            newVideoFile = mp3File.absolutePath
            Log.d("tag","newVideoFile:::$newVideoFile")
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun createNewFileMp3() {
        val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
        val now = Date()
        val fileName: String = formatter.format(now).toString() + ".mp3"
        try {
            val root = File(
                Environment.getExternalStorageDirectory().toString() + File.separator + "Music_Folder", "AudioFiles")

            if (!root.exists()) {
                root.mkdirs()
            }
            val mp3File = File(root, fileName)
            audioFile = mp3File.absolutePath

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun isVideoHaveAudioTrack(path: String): Boolean {
        val retriever = MediaMetadataRetriever()
        retriever.setDataSource(path)
        val hasAudioStr = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO)
        audioTrack = hasAudioStr != null && hasAudioStr == "yes"
        return audioTrack
    }

    fun MixerAudioFile() {
        val outputFile = createVideoFile(applicationContext!!)

        OptiVideoEditor.with(this).setType(OptiConstant.VIDEO_AUDIO_MERGE).setFile(File(videoFile!!))
            .setAudioFile(File(audioFile!!)).setOutputPath(outputFile.path).setCallback(object : OptiFFMpegCallback {
                override fun onProgress(progress: String) {

                }

                override fun onSuccess(convertedFile: File, type: String) {
                    videoFile = outputFile.path
                    MixerVideoPlayBackSpeed()
                }

                override fun onFailure(error: Exception) {
                  //  customToastMain(error.toString(),2)
                    Log.e("Tag ",error.toString())
                }

                override fun onNotAvailable(error: Exception) {

                }

                override fun onFinish() {

                }

            }).main()
    }

    fun MixerVideoPlayBackSpeed()
    {

        when(speed)
        {
            SPEED_1->
            {
                trimmerView.visible()
                setVideoPlayerUrl(videoFile!!)
                return
            }
            SPEED_0_3->
            {
                speed="1.75"
                speedTempo = "0.50"
            }
            SPEED_0_5->{
                speed="1.50"
                speedTempo = "0.50"
            }
            SPEED_2->
            {
                speed="0.70"
                speedTempo = "2.0"
            }
            SPEED_3->
            {
                speed=SPEED_0_5
                speedTempo = "2.0"
            }
        }

        Log.e("Tag  MixerVideo","$videoFile")
        val outputFile = createVideoFile(applicationContext!!)
        progressDialog.show()
        OptiVideoEditor.with(this)
            .setType(OptiConstant.VIDEO_PLAYBACK_SPEED)
            .setFile(File(videoFile.toString()!!))
            .setOutputPath(outputFile.path)
            .setIsHavingAudio(true)
            .setSpeedTempo(speed.toString(), speedTempo.toString())
            .setCallback(object : OptiFFMpegCallback {
                override fun onProgress(progress: String) {

                }
                override fun onSuccess(convertedFile: File, type: String) {
                    progressDialog.hide()
                    videoFile = outputFile.path
                    trimmerView.visible()
                    setVideoPlayerUrl(videoFile!!)
                }

                override fun onFailure(error: Exception) {
                    progressDialog.hide()
                    customToastMain(getString(R.string.api_error_message),2)
                }

                override fun onNotAvailable(error: Exception) {
                    progressDialog.hide()
                    customToastMain(getString(R.string.api_error_message),2)
                }

                override fun onFinish() {
                    progressDialog.hide()
                    Log.e("roleit", "Finish")
                }

            })
            .main()
    }

    fun setVideoPlayerUrl(videoFile:String)
    {
        Log.e("File Path","$videoFile")
       // var filePath = "/storage/emulated/0/Download/2021-02-03T17:25:04.678.mp4"
        trimmerView.setOnTrimVideoListener(this@VideoPreviewActivity)
        trimmerView.initVideoByURI(Uri.parse(videoFile))
    }

      override fun onStartTrim() {
      }

      override fun onFinishTrim(url: String?, durations: Double, startTime: Long?, endTime: Long?) {
          val intent = Intent(this@VideoPreviewActivity, VideoUploadActivity::class.java)
              .putExtra("videoFile", url)
          startActivity(intent)
          finish()
      }

      override fun onCancel() {

      }
    inner class AudioVideoTask : AsyncTask<String?, Int?, String?>() {

        override fun doInBackground(vararg str: String?): String? {

            AudioExtractor().genVideoUsingMuxer(videoAudioFile, audioFile, -1, -1, true, false)
            return "Success"
        }

        override fun onPostExecute(result: String?) {
            AudioExtractor().mux(videoFile, audioFile, newVideoFile)
            videoFile = newVideoFile
        }
    }
}