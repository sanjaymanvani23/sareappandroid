package com.example.sare.videoRecording

import android.animation.ValueAnimator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.FileProvider
import androidx.viewpager.widget.ViewPager

import com.example.sare.AudioListQuery
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.R
import com.example.sare.base.BaseActivity
import com.example.sare.dashboard.downloadFile
import com.example.sare.dashboard.model.DownloadResult
import com.example.sare.extensions.*
import com.example.sare.musicList.FragmentAddLocalMusic
import com.example.sare.musicList.FragmentAddOnlineMusic
import com.example.sare.musicList.FragmentAddVideoMusic
import com.example.sare.musicList.UseMusicListeners
import com.example.sare.util.OptiConstant
import com.example.sare.util.OptiFFMpegCallback
import com.example.sare.util.OptiVideoEditor
import com.example.sare.videoRecording.adapter.MusicSliderViewPagerAdapter
import io.ktor.client.*

import kotlinx.android.synthetic.main.activity_video_status_preview.*
import kotlinx.android.synthetic.main.activity_video_status_preview.add_music_popup
import kotlinx.android.synthetic.main.activity_video_status_preview.back
import kotlinx.android.synthetic.main.activity_video_status_preview.back_image
import kotlinx.android.synthetic.main.activity_video_status_preview.find_songs
import kotlinx.android.synthetic.main.activity_video_status_preview.iv_music_cancel
import kotlinx.android.synthetic.main.activity_video_status_preview.iv_search
import kotlinx.android.synthetic.main.activity_video_status_preview.local_songs
import kotlinx.android.synthetic.main.activity_video_status_preview.music
import kotlinx.android.synthetic.main.activity_video_status_preview.search_bar
import kotlinx.android.synthetic.main.activity_video_status_preview.search_edit_text
import kotlinx.android.synthetic.main.activity_video_status_preview.songs_viewpager
import kotlinx.android.synthetic.main.activity_video_status_preview.tv_music
import kotlinx.android.synthetic.main.activity_video_status_preview.txtCounter
import kotlinx.android.synthetic.main.activity_video_status_preview.video_songs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import java.io.File
import java.util.*

class VideoStatusPreviewActivity : BaseActivity(), UseMusicListeners {
    var handler: Handler? = null
    var videoFile: String? = null
    var isCompleted = true
    var fragmentAddOnlineMusic: FragmentAddOnlineMusic? = null
    var fragmentAddVideoMusic: FragmentAddVideoMusic? = null
    var fragmentAddLocalMusic: FragmentAddLocalMusic? = null
    var newMp3File:String?=null
    private val mAnimationHandler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_status_preview)

        videoFile = intent.getStringExtra("videoFile")

        fragmentAddOnlineMusic = FragmentAddOnlineMusic()
        fragmentAddVideoMusic = FragmentAddVideoMusic()
        fragmentAddLocalMusic = FragmentAddLocalMusic()

        fragmentAddOnlineMusic?.setUseMusicListener(this)
        fragmentAddLocalMusic?.setUseMusicListener(this)
        fragmentAddVideoMusic?.setUseMusicListener(this)

        val musicSliderViewPagerAdapter = MusicSliderViewPagerAdapter(supportFragmentManager,fragmentAddOnlineMusic!!,fragmentAddVideoMusic!!,fragmentAddLocalMusic!!,3)

        songs_viewpager.adapter = musicSliderViewPagerAdapter
        songs_viewpager.currentItem = 0

        songs_viewpager.setDurationScroll(500)
        songs_viewpager.setCanScroll(false)
        btnPost.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 0
            if (getAudioFileDir().exists()) {
                getAudioFileDir().deleteRecursively()
            }
        }

        find_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 0
        }

        local_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 1
        }

        video_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 2
        }


        Handler().postDelayed(Runnable {
            initVideoView()
        }, 0)

        imgPlay.setOnClickListener {
            imgPlay.gone()
            if (isCompleted)
                startVideo()
            else
                resumeVideo()
        }
        et_caption.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                txtCounter?.text = "${s?.length}/100"
                if (s?.length!! >= 100) {
                    showToast(getString(R.string.description_can_more_than_error))
                }
            }
        })

        music.setOnClickListener {
            music.gone()
            back.gone()
            btnPost.gone()
            imgPlay.gone()
            bottomCaptionLayout.gone()
            handler = Looper.myLooper()?.let { it1 -> Handler(it1) }
            setAnimateViewAtPositionTop(add_music_popup, 0, 600)
            val runnable = Runnable {
                fragmentAddOnlineMusic?.recyclerViewVisibility()
            }
            handler?.postDelayed(runnable, 600)

        }


        songs_viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {

                when (position) {
                    0 -> {
                        fragmentAddOnlineMusic?.recyclerViewVisibility()
                        find_songs.highLightText()
                        local_songs.normalText()
                        video_songs.normalText()

                    }
                    1 -> {
                        fragmentAddLocalMusic?.recyclerViewVisibility()

                        local_songs.highLightText()
                        find_songs.normalText()
                        video_songs.normalText()
                    }
                    2 -> {
                        fragmentAddVideoMusic?.recyclerViewVisibility()
                        video_songs.highLightText()
                        find_songs.normalText()
                        local_songs.normalText()
                    }
                }

            }

        })

        back_image.setOnClickListener {
            music.visible()
            back.visible()
            btnPost.visible()
            imgPlay.visible()
            bottomCaptionLayout.visible()
            fragmentAddOnlineMusic?.onPause()
            fragmentAddLocalMusic?.onPause()
            fragmentAddVideoMusic?.onPause()

            handler = Looper.myLooper()?.let { it1 -> Handler(it1) }

            val runnable = Runnable {
                //fragmentAddOnlineMusic?.recyclerViewInvisibility()
            }
            handler?.postDelayed(runnable, 610)

            setAnimateViewAtPositionTop(add_music_popup, 2500, 600)
        }
        search_edit_text.addTextChangedListener(EditTextWatcher(search_edit_text))

        iv_search.setOnClickListener {
            iv_search.gone()
            search_bar.visible()
        }
    }

    private fun initVideoView() {

        val sharedFileUri = FileProvider.getUriForFile(
            this, applicationContext.packageName + ".provider", File(videoFile)
        )
        mVideoView.setVideoURI(sharedFileUri)
        mVideoView.setOnCompletionListener {
            isCompleted = true
        }
        mVideoView.setOnPreparedListener {
            startVideo()
            tvTotalDuration.text = getMinuteSeconds(it.duration.toLong())
        }
        mVideoView.setOnClickListener {
            imgPlay.visible()
            pauseVideo()
        }

    }

    fun getMinuteSeconds(millis: Long): String? {
        val minutes = (millis / 1000 / 60).toInt()
        val seconds = Math.ceil(millis / 1000.toDouble()).toInt() - minutes * 60
        return String.format(Locale.US, "%d:%02d", minutes, seconds)
    }


    private fun startVideo() {
        if (!mVideoView.isPlaying) {
            mAnimationHandler.post(mAnimationRunnable)
            mVideoView.start()
            isCompleted = false
        }
    }
    private val mAnimationRunnable = Runnable {
        updateVideoProgress()
    }

    private fun updateVideoProgress() {
        val currentPosition = mVideoView!!.currentPosition.toLong()
        txtDuration.text = getMinuteSeconds(currentPosition)
        if (currentPosition >= mVideoView!!.duration) {
            pauseVideo()
        } else {
            mAnimationHandler.post(mAnimationRunnable)
        }
    }
    private fun stopVideo() {
        if (mVideoView.isPlaying) {
            mVideoView.stopPlayback()
            isCompleted = true
        }
    }

    private fun pauseVideo() {
        if (mVideoView.isPlaying) {
            mVideoView.pause()
        }
    }

    private fun resumeVideo() {
        if (!mVideoView.isPlaying) {
            mVideoView.resume()
        }
    }
    private fun setAnimateViewAtPositionTop(view: View, margin: Int, duration: Long) {

        val params3 =
            view.layoutParams as ViewGroup.MarginLayoutParams
        val animator3 =
            ValueAnimator.ofInt(params3.topMargin, margin)
        animator3.addUpdateListener { valueAnimator ->
            params3.topMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }
        animator3.duration = duration
        animator3.start()

    }

    override fun setAudioListener(dummy: SongsModel, leftDuration: Int, rightDuration: Int) {
        Log.d("tag", "Audio Online 111: " + dummy)
        back_image.performClick()
        tv_music.text = dummy.mSongsName
        iv_music_cancel.visible()
        newMp3File=dummy.mPath
        MixerAudioFile()
    }

    override fun setAudioListener(dummy: AudioListQuery.Audio,leftDuration: Int,rightDuration: Int) {
        back_image.performClick()
        tv_music.text =dummy.name()
        iv_music_cancel.visible()
        downloadWithFlow(com.example.sare.Utils.AUDIO_URL+dummy.name())
    }

    private fun downloadWithFlow(url: String) {
        newMp3File=createNewFileMp3()
        val progressDialog = CustomProgressDialogNew(this)
        progressDialog.show()
        CoroutineScope(Dispatchers.IO).launch {
            ktor.downloadFile(File(newMp3File), url).collect {
                withContext(Dispatchers.Main) {
                    when (it) {
                        is DownloadResult.Success -> {
                            if(!newMp3File.isNullOrEmpty())
                            {
                                MixerAudioFile()
                            }
                            progressDialog.hide()
                        }
                        is DownloadResult.Error -> {
                            progressDialog.hide()
                            Log.d("tag", "error:::" + it.message)
                        }
                        is DownloadResult.Progress -> {
                            Log.d("tag", "progresss:::" + it.progress)
                            progressDialog.setProgress(it.progress)
                        }
                        else -> "Throws Error.!!"
                    }
                }
            }
        }
    }

    inner class EditTextWatcher(private val search_edit_text: EditText?) :
        TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

            when (songs_viewpager.currentItem) {
                0 -> {
                    fragmentAddOnlineMusic?.searchQuery(search_edit_text?.text.toString())
                }
                1 -> {
                    fragmentAddLocalMusic?.searchQuery(search_edit_text?.text.toString())
                }
                2 -> {
                    fragmentAddVideoMusic?.searchQuery(search_edit_text?.text.toString())
                }
            }

        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

    }

    fun MixerAudioFile() {

        val progressDialog = CustomProgressDialogNew(this)
        progressDialog.show()

        val outputFile = createVideoFile(applicationContext!!)
        var Mp3File=File(newMp3File)
        OptiVideoEditor.with(this).setType(OptiConstant.VIDEO_AUDIO_MERGE).setFile(File(videoFile!!))
            .setAudioFile(Mp3File).setOutputPath(outputFile.path).setCallback(object : OptiFFMpegCallback {
                override fun onProgress(progress: String) {

                }

                override fun onSuccess(convertedFile: File, type: String) {
                    Log.e("VideoStatus Success","Suceess ${outputFile.path}")
                    progressDialog.dismiss()
                    videoFile = outputFile.path
                    initVideoView()
                    imgPlay.gone()
                }

                override fun onFailure(error: Exception) {
                    Log.e("VideoStatus error","${error.toString()}")
                    progressDialog.dismiss()
                }

                override fun onNotAvailable(error: Exception) {
                    progressDialog.dismiss()
                }

                override fun onFinish() {
                    progressDialog.dismiss()
                }

            }).main()
    }


}