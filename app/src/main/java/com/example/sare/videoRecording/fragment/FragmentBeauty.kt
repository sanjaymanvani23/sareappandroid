package com.example.sare.videoRecording.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.videoRecording.FilterType
import com.example.sare.videoRecording.SliderLayoutManager
import com.example.sare.videoRecording.adapter.BeautyFilterListAdapter
import kotlinx.android.synthetic.main.fragment_beauty_filter.view.*

class FragmentBeauty : Fragment(), BeautyFilterListAdapter.OnClickListen {

    var root: View? = null
    private var filterTypes: List<FilterType>? = null
    var allFilterListAdapter: BeautyFilterListAdapter? = null
    var swipeCount = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        root = inflater.inflate(R.layout.fragment_beauty_filter, container, false)
        setFilterRecyclerView()
        return root
    }

    fun onSwipeGesture(swipeCount: Int) {
        root?.beauty_recyclerview?.smoothScrollToPosition(swipeCount)

    }

    private fun setFilterRecyclerView() {

        filterTypes = FilterType.createFilterList()
        allFilterListAdapter = BeautyFilterListAdapter(
            requireContext(), filterTypes!!)

        val padding: Int = Utils.getScreenWidth(requireContext()) / 2 - Utils.dpToPx(requireContext(), 30f)
        root?.beauty_recyclerview?.setPadding(padding, 0, padding, 0)

        allFilterListAdapter?.setOnClick(this)
        root?.beauty_recyclerview?.adapter = allFilterListAdapter
        root?.beauty_recyclerview?.layoutManager = SliderLayoutManager(requireContext())
        root?.beauty_recyclerview?.suppressLayout(false)
        root?.beauty_recyclerview?.isNestedScrollingEnabled = false

        root?.beauty_recyclerview?.smoothScrollToPosition(0)


    }

    override fun onBeautyImageClick(view: View) {
        root?.beauty_recyclerview?.smoothScrollToPosition(root?.beauty_recyclerview!!.getChildLayoutPosition(view))
    }

}