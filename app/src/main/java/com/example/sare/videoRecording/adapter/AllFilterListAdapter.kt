package com.example.sare.videoRecording.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.all_filter_list.view.*

class AllFilterListAdapter(private val context : Context, private val parent : List<FilterType>) : RecyclerView.Adapter<AllFilterListAdapter.ViewHolder>() {

    private var selectedPositionCheckBox = -1
    private var selectedPositionFavorite = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.all_filter_list, parent,false))
    }

    override fun getItemCount(): Int {
        return parent.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.checkbox.setImageDrawable(ResourcesCompat.getDrawable(context.resources,
            R.drawable.ic_camera_checkbox, context.theme))
        holder.favorite.setImageDrawable(ResourcesCompat.getDrawable(context.resources,
            R.drawable.ic_camera_star_deselected, context.theme))

        if (selectedPositionCheckBox == position){
            holder.checkbox.setImageDrawable(ResourcesCompat.getDrawable(context.resources,
                R.drawable.ic_camera_checkbox_selected, context.theme))
        }

        if (selectedPositionFavorite == position){
            holder.favorite.setImageDrawable(ResourcesCompat.getDrawable(context.resources,
                R.drawable.ic_camera_favorite, context.theme))
        }

        holder.filterName.text = parent[position].name

    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val filterName = view.filter_name
        val checkbox = view.checkbox
        val favorite = view.favorite

        init {

            checkbox.setOnClickListener {
                selectedPositionCheckBox = -1
                selectedPositionFavorite = -1

                selectedPositionCheckBox = adapterPosition
                notifyItemChanged(adapterPosition)
            }

            favorite.setOnClickListener {
                selectedPositionFavorite = -1
                selectedPositionCheckBox = -1

                selectedPositionFavorite = adapterPosition
                notifyItemChanged(adapterPosition)
            }

        }
    }
}
