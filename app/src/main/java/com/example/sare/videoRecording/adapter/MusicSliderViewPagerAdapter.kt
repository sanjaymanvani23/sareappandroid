package com.example.sare.videoRecording.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sare.musicList.FragmentAddLocalMusic
import com.example.sare.musicList.FragmentAddOnlineMusic
import com.example.sare.musicList.FragmentAddVideoMusic

class MusicSliderViewPagerAdapter(
    fm: FragmentManager,
    private var fragmentAddOnlineMusic: FragmentAddOnlineMusic,
    private var fragmentAddVideoMusic: FragmentAddVideoMusic,
    private var fragmentAddLocalMusic: FragmentAddLocalMusic,
    private var tabCount: Int
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {

        return when (position) {
            0 -> {
                fragmentAddOnlineMusic
            }
            1 -> {
                fragmentAddLocalMusic
            }
            2 -> {
                fragmentAddVideoMusic
            }
            else -> fragmentAddOnlineMusic

        }
    }

    override fun getCount(): Int {
        return tabCount
    }

}