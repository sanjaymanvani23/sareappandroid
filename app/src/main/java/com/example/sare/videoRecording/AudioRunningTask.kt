package com.example.sare.videoRecording

import android.media.AudioAttributes
import android.media.MediaPlayer
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.AudioListQuery
import com.example.sare.appManager.NetworkResponse
import com.example.sare.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.net.URL

class AudioRunningTask : ViewModel() {
    private val status = MutableLiveData<Resource<ByteArray>>()
    private val audioStatus = MutableLiveData<Resource<MediaPlayer>>()
    var mPlayer: MediaPlayer? = null
    var url = URL("https://sare123.b-cdn.net/kgf.mp3")
    var audioUrl = "https://sare123.b-cdn.net/kgf.mp3"
    fun getStatus(): LiveData<Resource<ByteArray>> {
        return status
    }

    fun getAudioStatus(): LiveData<Resource<MediaPlayer>> {
        return audioStatus
    }


    fun startRunningTask() {
        viewModelScope.launch {
            status.postValue(Resource.loading(null))
            try {

                var response = getUrlByteStream()
                status.postValue(Resource.success(response))
            }catch (e : Exception){
                status.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    fun startRunningTask(url : URL) {
        viewModelScope.launch {
            status.postValue(Resource.loading(null))
            try {
                val response = getUrlByteStream(url)
                Log.e("ResponseData", response.toString())
                status.postValue(Resource.success(response))
            }catch (e : Exception){
                status.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    fun startLongRunningTask(audioUrl:String) {
        if(!audioUrl.isNullOrEmpty())
            this.audioUrl =audioUrl
        viewModelScope.launch {
            audioStatus.postValue(Resource.loading(null))
            try {
                // do a long running task
                var audioMediaPlayer = doLongRunningTask()
                audioStatus.postValue(Resource.success(audioMediaPlayer))
            } catch (e: Exception) {
                audioStatus.postValue(Resource.error("Something Went Wrong", null))
            }
        }
    }

    private suspend fun getUrlByteStream() : ByteArray{
        var response: ByteArray? = null
        withContext(Dispatchers.Default){

            val inputStream: InputStream = BufferedInputStream(url.openStream())
            val out = ByteArrayOutputStream()
            val buf = ByteArray(1024)
            var n = 0
            while (-1 != inputStream.read(buf).also { n = it }) {
                out.write(buf, 0, n)
            }
            out.close()
            inputStream.close()
            response = out.toByteArray()
        }
        return response!!
    }

    private suspend fun getUrlByteStream(url1 : URL) : ByteArray{
        var response: ByteArray? = null
        withContext(Dispatchers.Default){

            val inputStream: InputStream = BufferedInputStream(url1.openStream())
            val out = ByteArrayOutputStream()
            val buf = ByteArray(1024)
            var n = 0
            while (-1 != inputStream.read(buf).also { n = it }) {
                out.write(buf, 0, n)
            }
            out.close()
            inputStream.close()
            response = out.toByteArray()
        }
        return response!!
    }

    private suspend fun doLongRunningTask() : MediaPlayer {
        withContext(Dispatchers.Default) {

            mPlayer = MediaPlayer()

            mPlayer!!.setAudioAttributes(
                AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            )
            try {
                mPlayer!!.setDataSource(audioUrl)
                mPlayer!!.prepare()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        return mPlayer!!
    }

    var audioArrayList = MutableLiveData<NetworkResponse>()


    fun getAudioList(token : String){
        val apolloClient = ApolloClient.setupApollo(token.toString())

        apolloClient.query(AudioListQuery(0,100))
            .enqueue(object : ApolloCall.Callback<AudioListQuery.Data>(){
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    audioArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<AudioListQuery.Data>) {
                    try {
                        runBlocking {
                            audioArrayList.postValue(NetworkResponse.SUCCESS.audioList(response.data()?.audios()?.audio()!!))
                            Log.d("tag","audioArrayList:::$audioArrayList")
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            })
    }
}