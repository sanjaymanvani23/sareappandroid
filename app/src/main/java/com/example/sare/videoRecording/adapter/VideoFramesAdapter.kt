package com.example.sare.videoRecording.adapter

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.graphics.get
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sare.R
import kotlinx.android.synthetic.main.item_video_frames.view.*

class VideoFramesAdapter(val list: List<Bitmap>, private val context: Context) :
    RecyclerView.Adapter<VideoFramesAdapter.ViewHolder>() {
    var onItemClick: ((Bitmap) -> Unit)? = null

    var selectedIMageView: ImageView?= null
    var selectedPosition: Int = -1
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.item_video_frames, parent, false)
        return ViewHolder(
            v
        )
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d("tag","list::::$list")
        holder.bindItems(list[position], context, onItemClick!!)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is hodling the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(
            item: Bitmap,
            context: Context,
            onItemClick: (Bitmap) -> Unit

        ) {
            var imgVideoFrame = itemView.imgVideoFrame
            var imgVideoSelect = itemView.imgVideoSelect


                imgVideoFrame.setImageBitmap(item)
            //Glide.with(context).load(item).error(R.drawable.cover_default).into(imgVideoFrame)



            if(selectedPosition == adapterPosition) {
                itemView.imgVideoSelect.visibility = View.VISIBLE
            } else {
                itemView.imgVideoSelect.visibility = View.GONE
            }
            selectedIMageView?.visibility = View.GONE

            itemView.setOnClickListener {

                onItemClick.invoke(item)
                selectedIMageView?.visibility = View.GONE
                itemView.imgVideoSelect.visibility = View.VISIBLE
                selectedIMageView = itemView.imgVideoSelect
                selectedPosition = adapterPosition
                
            }
        }
    }


}