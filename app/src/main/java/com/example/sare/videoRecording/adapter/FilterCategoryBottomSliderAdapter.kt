package com.example.sare.videoRecording.adapter

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.videoRecording.FilterType
import kotlinx.android.synthetic.main.filter_category_bottom_slider_adapter.view.*

class FilterCategoryBottomSliderAdapter(private val context : Context, private val parent : List<FilterType>?) : RecyclerView.Adapter<FilterCategoryBottomSliderAdapter.ViewHolder>() {

    private var mOnFilterClick : OnFilterClick? = null
    private var currentPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.filter_category_bottom_slider_adapter,parent,false))
    }

    fun setOnClick(onFilterClick: OnFilterClick){
        mOnFilterClick = onFilterClick
    }

    override fun getItemCount(): Int {
        return parent!!.size
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.categoryName.setTextColor(context.resources.getColor(R.color.category_name_color, context.theme))

        if (currentPosition == position){
            holder.categoryName.setTextColor(Color.WHITE)
        }

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val categoryName = view.category_name

        init {
            categoryName.setOnClickListener {
                currentPosition = adapterPosition
                mOnFilterClick?.onClick1(adapterPosition)
                notifyDataSetChanged()
            }
        }
    }

    interface OnFilterClick{
        fun onClick1(position: Int)
    }
}
