package com.example.sare.videoRecording

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.media.*
import android.media.MediaCodec.BufferInfo
import android.net.Uri
import android.opengl.GLException
import android.os.*
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.DisplayMetrics
import android.util.Log
import android.util.SparseIntArray
import android.view.*
import android.view.View.*
import android.view.animation.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.coremedia.iso.boxes.Container
import com.daasuu.gpuv.camerarecorder.CameraRecordListener
import com.daasuu.gpuv.camerarecorder.GPUCameraRecorder
import com.daasuu.gpuv.camerarecorder.GPUCameraRecorderBuilder
import com.daasuu.gpuv.camerarecorder.LensFacing
import com.example.sare.AudioListQuery
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MusicAdd.SongsModel
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.base.BaseActivity
import com.example.sare.dashboard.downloadFile
import com.example.sare.dashboard.model.DownloadResult

import com.example.sare.extensions.gone
import com.example.sare.extensions.invisible
import com.example.sare.extensions.secToTime
import com.example.sare.extensions.visible

import com.example.sare.extensions.customToast

import com.example.sare.musicList.FragmentAddLocalMusic
import com.example.sare.musicList.FragmentAddOnlineMusic
import com.example.sare.musicList.FragmentAddVideoMusic
import com.example.sare.musicList.UseMusicListeners
import com.example.sare.network.Status
import com.example.sare.util.AudioVideoCutter.CheapSoundFile
import com.example.sare.util.AudioVideoCutter.Util
import com.example.sare.util.OptiConstant
import com.example.sare.util.OptiFFMpegCallback
import com.example.sare.util.OptiUtils
import com.example.sare.util.OptiVideoEditor
import com.example.sare.videoRecording.adapter.*
import com.example.sare.videoRecording.adapter.FilterRecyclerAdapter.OnFilterClick
import com.example.sare.videoRecording.fragment.FragmentBeauty
import com.example.sare.videoRecording.fragment.FragmentNormal
import com.example.sare.widget.CountDownAnimation
import com.example.sare.widget.SampleCameraGLView
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import com.googlecode.mp4parser.authoring.Movie
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator
import kotlinx.android.synthetic.main.activity_camera_portrate.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.*
import java.net.URL
import java.nio.BufferOverflowException
import java.nio.ByteBuffer
import java.nio.IntBuffer
import java.nio.channels.WritableByteChannel
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.opengles.GL10
import kotlin.math.roundToInt

open class  BaseCameraActivity : BaseActivity(), OnFilterClick,
    FragmentNormal.OnRecordButtonClick, FilterCategoryBottomSliderAdapter.OnFilterClick,
    UseMusicListeners {
    lateinit var progressDialog : CustomProgressDialogNew
    var isFliter=true

    private var sampleGLView: SampleCameraGLView? = null
    private var mGPUCameraRecorder: GPUCameraRecorder? = null
    private var filepath: String? = null
    private var lensFacing = LensFacing.BACK

    @JvmField
    protected var cameraWidth = 1280

    @JvmField
    protected var cameraHeight = 720

    @JvmField
    protected var videoWidth = 720

    @JvmField
    protected var videoHeight = 720
    private var toggleClick = false
    private var filterTypes: List<FilterType>? = null
    var slide = true
    var speedVisible = true
    var speedDialogOpen = true
    private var pause = true
    private var recordVideo = true
    var fragmentAddOnlineMusic: FragmentAddOnlineMusic? = null
    var fragmentAddVideoMusic: FragmentAddVideoMusic? = null
    var fragmentAddLocalMusic: FragmentAddLocalMusic? = null

    private var progressRunnable: Runnable? = null
    var handler: Handler? = null
    private var testCount = 0f
    private var countTimer = 0f
    var mPlayer: MediaPlayer? = null
    private lateinit var viewModel: AudioRunningTask
    var flashVisible = true
    var mFragmentNormal: FragmentNormal? = null
    var mFragmentBeauty: FragmentBeauty? = null
    var bitmapCaptureImage: Bitmap? = null
    var swipeCount = 0
    var pauseTimer1 = 15f
    var pauseTimer2 = 15f
    var timerDelay = 0
    var stringFormat = 0f
    val displayMetrics = DisplayMetrics()
    var screenWidth = 0
    var speedCount = 1f
    var listPauseView = mutableListOf<View>()
    private var mScaleGestureDetector: ScaleGestureDetector? = null
    private val mScaleFactor = 1.0f
    var videoFile = mutableListOf<File>()
    var speedList = mutableListOf<Float>()



    private fun onSwipeGesture(view: View) {
        view.setOnTouchListener(object : OnSwipeTouchListener(applicationContext) {
            override fun onSwipeLeft() {
                super.onSwipeLeft()
                swipeCount++
                if (swipeCount > 0) {
                    mFragmentNormal?.onSwipeGesture(swipeCount)
                    mFragmentBeauty?.onSwipeGesture(swipeCount)
                    if (mGPUCameraRecorder != null) {
                        mGPUCameraRecorder!!.setFilter(
                            FilterType.createGlFilter(
                                filterTypes!![swipeCount],
                                applicationContext
                            )
                        )
                    }
                }
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
                swipeCount--
                mFragmentNormal?.onSwipeGesture(swipeCount)
                mFragmentBeauty?.onSwipeGesture(swipeCount)

                if (swipeCount < filterTypes!!.size) {
                    mFragmentNormal?.setOnSwipeFilter(swipeCount)
                    if (mGPUCameraRecorder != null) {
                        mGPUCameraRecorder!!.setFilter(
                            FilterType.createGlFilter(
                                filterTypes!![swipeCount],
                                applicationContext
                            )
                        )
                    }
                }
            }

            override fun onTouch(view: View?, motionEvent: MotionEvent?): Boolean {
                return mScaleGestureDetector?.onTouchEvent(motionEvent) ?: super.onTouch(
                    view,
                    motionEvent
                )
            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.M)
    protected fun onCreateActivity() {

        viewModel = ViewModelProvider(this)[AudioRunningTask::class.java]
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenWidth = displayMetrics.widthPixels
        Log.e("margin base::","${screenWidth} ::: ${displayMetrics.widthPixels}")
        mScaleGestureDetector = ScaleGestureDetector(this, ScaleListener())
        //mediaExtractor()



        setListeners()
        setViewModel()
        setFilterRecyclerView()
        setEffectRecyclerView()
        setFilterCategoryRecyclerView()

        search_edit_text.addTextChangedListener(EditTextWatcher(search_edit_text))
        onSwipeGesture(viewPager)

/*
        onSwipeGesture(menu)
*/


        // BottomSliderViewPagerAdapter

        mFragmentNormal =
            FragmentNormal(this)

        mFragmentBeauty =
            FragmentBeauty()

        val adapter =
            BottomSliderViewPagerAdapter(
                supportFragmentManager,
                mFragmentNormal!!,
                1
            )

        viewPager.adapter = adapter
        normal_text.setTextColor(resources.getColor(R.color.pureWhite, theme))
        viewPager.currentItem = 1
        viewPager.setDurationScroll(600)
        viewPager.setCanScroll(false)

        /////////////////////////////////////////////////////////////////////////////////


        // MusicSliderViewPagerAdapter

        fragmentAddOnlineMusic = FragmentAddOnlineMusic()
        fragmentAddVideoMusic = FragmentAddVideoMusic()
        fragmentAddLocalMusic = FragmentAddLocalMusic()

        fragmentAddOnlineMusic?.setUseMusicListener(this)
        fragmentAddLocalMusic?.setUseMusicListener(this)
        fragmentAddVideoMusic?.setUseMusicListener(this)

        val musicSliderViewPagerAdapter =
            MusicSliderViewPagerAdapter(
                supportFragmentManager,
                fragmentAddOnlineMusic!!,
                fragmentAddVideoMusic!!,
                fragmentAddLocalMusic!!,
                3
            )

        songs_viewpager.adapter = musicSliderViewPagerAdapter
        songs_viewpager.currentItem = 0
        highLightText(find_songs)
        songs_viewpager.setDurationScroll(500)
        songs_viewpager.setCanScroll(false)

        /////////////////////////////////////////////////////////////////////////////////

        find_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 0
        }

        local_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 1
        }

        video_songs.setOnClickListener {
            search_edit_text.text.clear()
            songs_viewpager.currentItem = 2
        }

        ///////////////////////Songs Viewpager Listener
        songs_viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {

                when (position) {
                    0 -> {
                        fragmentAddOnlineMusic?.recyclerViewVisibility()
                        highLightText(find_songs)
                        normalText(local_songs)
                        normalText(video_songs)
                    }
                    1 -> {
                        fragmentAddLocalMusic?.recyclerViewVisibility()
                        normalText(find_songs)
                        highLightText(local_songs)
                        normalText(video_songs)
                    }
                    2 -> {
                        fragmentAddVideoMusic?.recyclerViewVisibility()
                        normalText(find_songs)
                        normalText(local_songs)
                        highLightText(video_songs)
                    }
                }

            }

        })
        //////////////////////////////////////

        ///////////////////////Bottom Viewpager Listener
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        beautyFilter()
                    }
                    1 -> {
                        normalFilter()
                    }
                    2 -> {
                        storyFilter()
                    }
                }
            }
        })

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        back.setOnClickListener {
            //onDestroy()
            finish()
        }

        btn_next.setOnClickListener {
            stopRecording()
            if (filepath != null) {
                DownloadFilesTask().execute()
            } else {
                customToast("Please record video",this,0)
            }

        }

        btn_clear.setOnClickListener {
            //if(!recordVideo) {
            stopRecording()
            btn_clear.visibility = GONE
            back.visibility = VISIBLE
            btn_next.isEnabled = false
            btn_next.setTextColor(resources.getColor(R.color.white_50))
            filepath = null
            releaseCamera()
            setUpCamera()
            slide = true
            speedVisible = true
            speedDialogOpen = true
            pause = true
            recordVideo = true

            //private var progressRunnable: Runnable? = null
            //var handler: Handler? = null
            testCount = 0f
            countTimer = 0f
            //var mPlayer: MediaPlayer? = null
            //private lateinit var viewModel: AudioRunningTask
            flashVisible = true
            //var mFragmentNormal : FragmentNormal? = null
            //var mFragmentBeauty : FragmentBeauty? = null
            //var bitmapCaptureImage : Bitmap? = null
            swipeCount = 0
            pauseTimer1 = 15f
            pauseTimer2 = 15f
            timerDelay = 0
            stringFormat = 0f
            screenWidth = 0
            speedCount = 1f
            progress_horizontal.progress = 0
            mFragmentNormal?.videoTimer(15)
            listPauseView.forEach {
                layout.removeView(it)
            }
            listPauseView.clear()
            mFragmentNormal?.clearClicked()
            videoFile.clear()
            speedList.clear()
            //}
            sticker?.isEnabled = true
            speed?.isEnabled = true
            btn_filter?.isEnabled = true
            speed.visible()
            timer.visible()
            btn_filter.visible()

            setAnimateViewAtPosition(divider, convertDpToPixel(7f, this).toInt())
        }

        iv_search.setOnClickListener {
            iv_search.visibility = View.GONE
            search_bar.visibility = View.VISIBLE
        }

        beauty_text.setTextColor(resources.getColor(R.color.white_50, theme))
        normal_text.setTextColor(resources.getColor(R.color.pureWhite, theme))
        story_text.setTextColor(resources.getColor(R.color.white_50, theme))
    }


    private fun visibilityGoneView() {
        //speed.visibility = GONE
        //timer.visibility = GONE
        //btn_filter.visibility = GONE
        //sticker.visibility = GONE
        speed?.isEnabled = false
        timer?.isEnabled = false
    }

    private fun visibilityVisibleView() {
        //speed.visibility = VISIBLE
        //timer.visibility = VISIBLE
        //btn_filter.visibility = VISIBLE
        //sticker.visibility = VISIBLE
        speed?.isEnabled = true
        timer?.isEnabled = true
    }

    private fun normalText(view: TextView) {
        //view.setTextColor(ThemeManager.colors(applicationContext, StringSingleton.placeHolderColor))
        view.setTextColor(Color.parseColor("#66FFFFFF"))
        view.setBackgroundColor(Color.parseColor("#00000000"))
    }

    private fun highLightText(view: TextView) {
        //view.setTextColor(Color.WHITE)
        view.setTextColor(Color.parseColor("#FFFFFF"))
        view.setBackgroundResource(R.drawable.add_audio_tab_selected_bg)
    }


    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources
            .displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun beautyFilter() {

        visibilityGoneView()
        //val marginLeftSlideLayoutUnits = (screenWidth / 2) - (beauty_text.width / 2)
        /*val marginLeftSlideLayoutUnits = (screenWidth / 2) - (beauty_text.width / 2) - convertDpToPixel(
            22f,
            this
        ) */
        val marginLeftSlideLayoutUnits =
            (beauty_text.width) + (normal_text.width / 2) + convertDpToPixel(
                68f,
                this
            )
        animateSelectedView(slide_layout, marginLeftSlideLayoutUnits.toInt(), 600)

        beauty_text.setTextColor(resources.getColor(R.color.pureWhite, theme))
        normal_text.setTextColor(resources.getColor(R.color.white_50, theme))
        story_text.setTextColor(resources.getColor(R.color.white_50, theme))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun normalFilter() {

        /*val marginLeftSlideLayoutUnits = (screenWidth / 2) - ((beauty_text.width) + (normal_text.width / 2) + convertDpToPixel(
            20f,
            this
        ))*/
        val marginLeftSlideLayoutUnits = 0
        visibilityVisibleView()

        val params =
            slide_layout.layoutParams as ViewGroup.MarginLayoutParams
        val animator =
            ValueAnimator.ofInt(params.leftMargin, marginLeftSlideLayoutUnits)
        animator.addUpdateListener { valueAnimator ->
            params.leftMargin = (valueAnimator.animatedValue as Int)
            slide_layout.requestLayout()
        }
        animator.duration = 600
        animator.start()

        beauty_text.setTextColor(resources.getColor(R.color.white_50, theme))
        normal_text.setTextColor(resources.getColor(R.color.pureWhite, theme))
        story_text.setTextColor(resources.getColor(R.color.white_50, theme))
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun storyFilter() {
        //val marginLeftSlideLayoutUnits = (screenWidth / 2) - ((slide_layout.width) - (story_text.width / 2))
        val marginLeftSlideLayoutUnits =
            -((story_text.width) + (normal_text.width / 2) + convertDpToPixel(
                57f,
                this
            ))

        visibilityGoneView()

        animateSelectedView(slide_layout, marginLeftSlideLayoutUnits.toInt(), 600)

        beauty_text.setTextColor(resources.getColor(R.color.white_50, theme))
        normal_text.setTextColor(resources.getColor(R.color.white_50, theme))
        story_text.setTextColor(resources.getColor(R.color.pureWhite, theme))
    }

    private fun setFilterRecyclerView() {
        filterTypes = FilterType.createFilterList()
        val allFilterListAdapter =
            AllFilterListAdapter(
                applicationContext,
                filterTypes!!
            )
        recycler_view?.adapter = allFilterListAdapter
        recycler_view?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
    }

    private fun setEffectRecyclerView() {
        filterTypes = FilterType.createFilterList()
        val allFilterListAdapter =
            EffectListAdapter(
                applicationContext,
                filterTypes!!
            )
        effect_view?.adapter = allFilterListAdapter

        val layoutManager =
            AutoFitGridLayoutManager(
                this,
                200
            )
        effect_view.layoutManager = layoutManager
    }

    private fun setViewModel() {

        viewModel.getAudioStatus().observe(this, androidx.lifecycle.Observer {

            when (it.status) {
                Status.SUCCESS -> {
                    mPlayer = it.data
                    progressBar.visibility = GONE
                }
                Status.ERROR -> {
                    progressBar.visibility = GONE
                }
                Status.LOADING -> {
                    progressBar.visibility = VISIBLE
                }
            }

        })
        viewModel.startLongRunningTask("")

        viewModel.getStatus().observe(this, androidx.lifecycle.Observer {

            when (it.status) {
                Status.SUCCESS -> {
                    wave?.setRawData(it.data!!)
                }
                Status.ERROR -> {
                    Log.e("Success", it.message.toString())
                }
                Status.LOADING -> {
                    Log.e("Success", it.message.toString())
                }
            }
        })

        viewModel.startRunningTask()

    }

    private fun setFilterCategoryRecyclerView() {
        filterTypes = FilterType.createFilterList()
        val filterRecyclerAdapter =
            FilterCategoryBottomSliderAdapter(
                applicationContext,
                filterTypes
            )
        filterRecyclerAdapter.setOnClick(this)
        filter_category_recyclerview.adapter = filterRecyclerAdapter
        filter_category_recyclerview.layoutManager = LinearLayoutManager(
            applicationContext,
            RecyclerView.HORIZONTAL,
            false
        )
    }

    private fun setAnimateViewAtPosition(view: View, margin: Int) {

        val params3 =
            view.layoutParams as ViewGroup.MarginLayoutParams
        val animator3 =
            ValueAnimator.ofInt(params3.bottomMargin, margin)
        animator3.addUpdateListener { valueAnimator ->
            params3.bottomMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }
        animator3.duration = 500
        animator3.start()

    }

    private fun setAnimateViewAtPositionTop(view: View, margin: Int, duration: Long) {

        val params3 =
            view.layoutParams as ViewGroup.MarginLayoutParams
        val animator3 =
            ValueAnimator.ofInt(params3.topMargin, margin)
        animator3.addUpdateListener { valueAnimator ->
            params3.topMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }
        animator3.duration = duration
        animator3.start()

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setListeners() {

        iv_music_cancel.setOnClickListener {
            tv_music.text = "Add Music"
            iv_music_cancel.visibility = GONE
        }

        back_image.setOnClickListener {
            viewPager.visibility = VISIBLE
            progress_horizontal.visibility = VISIBLE
            audioWave.visibility = GONE
            slide_layout.visibility = VISIBLE
            divider.visibility = VISIBLE
            music.visibility = VISIBLE
            back.visibility = VISIBLE
            btn_next.visibility = VISIBLE
            if(btn_clear.isVisible)
                btn_clear.visibility = VISIBLE
            right.visibility = VISIBLE
            listPauseView.forEach {
                it.visibility = VISIBLE
            }

            fragmentAddOnlineMusic?.onPause()
            fragmentAddLocalMusic?.onPause()
            fragmentAddVideoMusic?.onPause()

            handler = Looper.myLooper()?.let { it1 -> Handler(it1) }

            if (mGPUCameraRecorder != null) {
                mGPUCameraRecorder!!.setFilter(
                    FilterType.createGlFilter(
                        FilterType.DEFAULT,
                        applicationContext
                    )
                )
            }
            val runnable = Runnable {
                //fragmentAddOnlineMusic?.recyclerViewInvisibility()
            }
            handler?.postDelayed(runnable, 610)

            setAnimateViewAtPositionTop(add_music_popup, 2500, 600)
        }

        music.setOnClickListener {
            viewPager.visibility = GONE
            progress_horizontal.visibility = INVISIBLE
            audioWave.visibility = INVISIBLE
            slide_layout.visibility = GONE
            divider.visibility = GONE
            music.visibility = GONE
            back.visibility = GONE
            btn_next.visibility = GONE
            btn_clear.visibility = GONE
            right.visibility = GONE
            listPauseView.forEach {
                it.visibility = INVISIBLE
            }

            setAnimateViewAtPositionTop(add_music_popup, 0, 600)

            handler = Looper.myLooper()?.let { it1 -> Handler(it1) }

            val runnable = Runnable {
                fragmentAddOnlineMusic?.recyclerViewVisibility()
            }
            if (mGPUCameraRecorder != null) {
                mGPUCameraRecorder!!.setFilter(
                    FilterType.createGlFilter(
                        FilterType.BOX_BLUR,
                        applicationContext
                    )
                )
            }
            handler?.postDelayed(runnable, 600)

        }

        close_arrow.setOnClickListener {
            setAnimateViewAtPositionTop(relative_layout, 2500, 500)
        }

        sticker.setOnClickListener {
            viewPager.visibility = GONE

            visibilityGoneView()

            setAnimateViewAtPosition(bottom_filter1, 0)
            setAnimateViewAtPosition(divider, -100)
        }

        cancel_action.setOnClickListener {

            viewPager.visibility = VISIBLE
            visibilityVisibleView()

            setAnimateViewAtPosition(bottom_filter1, -700)
            setAnimateViewAtPosition(divider, 10)

        }

        val marginLeftSpeedScreenUnits = (screenWidth / 7) + 7
        speed.setOnClickListener {
            if(!isFliter)
            {
                mFragmentNormal?.clearFilter()
                isFliter=true
            }

            changeSpeed()
        }

        speed_1x_point.setOnClickListener {
            if (speedVisible) {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 800)
                setNormalViews()
                darkTextSpeed(speed_1x_point)

                animateSelectedView(speed_1x_point, marginLeftSpeedScreenUnits * 3, 500)

                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation(speed_1x)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_3x)
                speedCount = 0.3f
                speedVisible = false

            } else {

                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

                animateSelectedView(speed_1x_point, marginLeftSpeedScreenUnits, 500)

                visibleAnimation(speed_2x_point)
                visibleAnimation(speed_1x)
                visibleAnimation(speed_2x)
                visibleAnimation(speed_3x)
                speedCount = 0.3f
                speedVisible = true

            }

        }

        speed_2x_point.setOnClickListener {
            if (speedVisible) {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 800)

                setNormalViews()
                darkTextSpeed(speed_2x_point)
                animateSelectedView(speed_2x_point, marginLeftSpeedScreenUnits * 3, 500)
                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_1x)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_3x)
                speedCount = 0.5f
                speedVisible = false
            } else {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

                animateSelectedView(speed_2x_point, marginLeftSpeedScreenUnits * 2, 500)
                visibleAnimation(speed_1x_point)
                visibleAnimation(speed_1x)
                visibleAnimation(speed_2x)
                visibleAnimation(speed_3x)
                speedCount = 0.5f
                speedVisible = true
            }
        }

        speed_1x.setOnClickListener {
            if (speedVisible) {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 800)

                setNormalViews()
                darkTextSpeed(speed_1x)
                animateSelectedView(speed_1x, marginLeftSpeedScreenUnits * 3, 500)

                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_3x)
                speedCount = 1f
                speedVisible = false
            } else {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

                animateSelectedView(speed_1x, marginLeftSpeedScreenUnits * 3, 500)
                visibleAnimation(speed_1x_point)
                visibleAnimation(speed_2x_point)
                visibleAnimation(speed_2x)
                visibleAnimation(speed_3x)
                speedCount = 1f
                speedVisible = true
            }
        }

        speed_2x.setOnClickListener {
            if (speedVisible) {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 800)

                setNormalViews()
                darkTextSpeed(speed_2x)
                animateSelectedView(speed_2x, marginLeftSpeedScreenUnits * 3, 500)
                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation(speed_1x)
                inVisibleAnimation(speed_3x)
                speedCount = 2f
                speedVisible = false
            } else {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

                animateSelectedView(speed_2x, marginLeftSpeedScreenUnits * 4, 500)
                visibleAnimation(speed_1x_point)
                visibleAnimation(speed_2x_point)
                visibleAnimation(speed_1x)
                visibleAnimation(speed_3x)
                speedCount = 2f
                speedVisible = true
            }
        }

        speed_3x.setOnClickListener {
            if (speedVisible) {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 800)

                setNormalViews()
                darkTextSpeed(speed_3x)
                animateSelectedView(speed_3x, marginLeftSpeedScreenUnits * 3, 500)
                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_1x)
                speedCount = 3f
                speedVisible = false
            } else {
                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

                animateSelectedView(speed_3x, marginLeftSpeedScreenUnits * 5, 500)
                visibleAnimation(speed_1x_point)
                visibleAnimation(speed_2x_point)
                visibleAnimation(speed_1x)
                visibleAnimation(speed_2x)
                speedCount = 3f
                speedVisible = true
            }
        }

        scroll_timer.setOnTouchListener(OnTouchListener { v, event -> true })

        timer.setOnClickListener {
            if ((recordVideo || !pause) && tv_timer_count.visibility == GONE) {
                when (timerDelay) {
                    0 -> {
                        val rotate = RotateAnimation(
                            0f,
                            135f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f
                        )
                        rotate.duration = 100;
                        rotate.fillAfter = true
                        rotate.interpolator = LinearInterpolator()
                        iv_timer_round.startAnimation(rotate)
                        //scroll_timer.scrollToDescendant(tv_timer_3)
                        scroll_timer.smoothScrollTo(0, tv_timer_0.height)
                        timerDelay = 3
                    }
                    3 -> {
                        val rotate = RotateAnimation(
                            135f,
                            180f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f
                        )
                        rotate.duration = 100;
                        rotate.fillAfter = true
                        rotate.interpolator = LinearInterpolator()
                        iv_timer_round.startAnimation(rotate)
                        scroll_timer.smoothScrollTo(
                            tv_timer_0.height,
                            tv_timer_0.height + tv_timer_3.height
                        )
                        timerDelay = 5
                    }
                    5 -> {
                        val rotate = RotateAnimation(
                            180f,
                            225f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f
                        )
                        rotate.duration = 100;
                        rotate.fillAfter = true
                        rotate.interpolator = LinearInterpolator()
                        iv_timer_round.startAnimation(rotate)
                        scroll_timer.smoothScrollTo(
                            tv_timer_0.height + tv_timer_3.height,
                            tv_timer_0.height + tv_timer_3.height + tv_timer_5.height
                        )
                        timerDelay = 10
                    }
                    10 -> {
                        val rotate = RotateAnimation(
                            225f,
                            -0f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f,
                            Animation.RELATIVE_TO_SELF,
                            0.5f
                        )
                        rotate.duration = 100;
                        rotate.fillAfter = true
                        rotate.interpolator = LinearInterpolator()
                        iv_timer_round.startAnimation(rotate)
                        scroll_timer.smoothScrollTo(
                            tv_timer_0.height + tv_timer_3.height + tv_timer_5.height,
                            0
                        )
                        timerDelay = 0
                    }
                }
            }
        }

        beauty_text.setOnClickListener {
            beautyFilter()

            viewPager.currentItem = 0
        }

        normal_text.setOnClickListener {
            normalFilter()
            music.visible()
            timer.visible()
            speed.visible()
            viewPager.currentItem = 1
            isVideoOrStatus=true
        }

        story_text.setOnClickListener {
            music.invisible()
            timer.gone()
            speed.gone()
            storyFilter()
            viewPager.currentItem = 2
            isVideoOrStatus=false
        }

        btn_flash.setOnClickListener {
            flashVisible = if (flashVisible) {

                flashImage.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_shoot_flashon,
                        theme
                    )
                )
                false
            } else {

                flashImage.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_shoot_flashoff,
                        theme
                    )
                )
                true
            }

            if (mGPUCameraRecorder != null && mGPUCameraRecorder!!.isFlashSupport) {
                mGPUCameraRecorder!!.switchFlashMode()
                mGPUCameraRecorder!!.changeAutoFocus()
            }
        }

        btn_switch_camera.setOnClickListener {

            releaseCamera()
            lensFacing = if (lensFacing == LensFacing.BACK) {
                LensFacing.FRONT
            } else {
                LensFacing.BACK
            }
            toggleClick = true
        }

        btn_filter.setOnClickListener {

            if(isFliter)
            {
                if(!speedDialogOpen)
                {
                    changeSpeed()
                }
                mFragmentNormal?.setOnFilterClick()
                sticker?.isEnabled = false
                isFliter=false
            }
            else{

                mFragmentNormal?.clearFilter()
                isFliter=true
            }


        }
    }
    fun changeSpeed()
    {
        val marginLeftSpeedScreenUnits = (screenWidth / 7) + 7
        if (speedDialogOpen)
        {

            val moveAnim = TranslateAnimation(0f, -2f, 0f, -1f)
            val rotate = RotateAnimation(
                0f,
                -90f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            val set = AnimationSet(true)
            set.addAnimation(moveAnim);
            set.addAnimation(rotate);
            set.duration = 100;
            set.fillAfter = true
            set.interpolator = LinearInterpolator()
            iv_speed_round.startAnimation(set)
            iv_speed.setImageResource(R.drawable.ic_shoot_speed_on)

            setNormalViews()
            darkTextSpeed(speed_1x)


            Log.d("Camera Activity", "Margin Left : ${marginLeftSpeedScreenUnits} :: ${screenWidth}")
            animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits, 500)

            visibleAnimation(speed_layout)
            visibleAnimation(speed_1x_point)
            visibleAnimation(speed_2x_point)
            visibleAnimation(speed_1x)
            visibleAnimation(speed_2x)
            visibleAnimation(speed_3x)


            animateSelectedView(speed_1x_point, marginLeftSpeedScreenUnits, 500) //167
            animateSelectedView(speed_2x_point, marginLeftSpeedScreenUnits * 2, 500) //310
            animateSelectedView(speed_1x, marginLeftSpeedScreenUnits * 3, 500) //470
            animateSelectedView(speed_2x, marginLeftSpeedScreenUnits * 4, 500) //630
            animateSelectedView(speed_3x, marginLeftSpeedScreenUnits * 5, 500) //790

            speedCount = 1f
            speedDialogOpen = false

        }
        else {
            if (!speedVisible) {
                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation(speed_1x)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_3x)
            } else {


                animateSelectedView(left_side_speed, marginLeftSpeedScreenUnits * 7, 500)

                inVisibleAnimation(speed_1x_point)
                inVisibleAnimation(speed_2x_point)
                inVisibleAnimation1(speed_1x)
                inVisibleAnimation(speed_2x)
                inVisibleAnimation(speed_3x)
            }

            val moveAnim = TranslateAnimation(-2f, 0f, -1f, 0f)
            val rotate = RotateAnimation(
                -90f,
                0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            val set = AnimationSet(true)
            set.addAnimation(moveAnim);
            set.addAnimation(rotate);
            set.duration = 100;
            set.fillAfter = true
            set.interpolator = LinearInterpolator()
            iv_speed_round.startAnimation(set)
            iv_speed.setImageResource(R.drawable.ic_shoot_speed_off)
            speedVisible = true
            speedCount = 1f
            speedDialogOpen = true

        }
    }


    inner class EditTextWatcher(private val search_edit_text: EditText?) :
        TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

            when (songs_viewpager.currentItem) {
                0 -> {
                    fragmentAddOnlineMusic?.searchQuery(search_edit_text?.text.toString())
                }
                1 -> {
                    fragmentAddLocalMusic?.searchQuery(search_edit_text?.text.toString())
                }
                2 -> {
                    fragmentAddVideoMusic?.searchQuery(search_edit_text?.text.toString())
                }
            }

        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

    }



    fun stopNextButtonRecording() {
        mGPUCameraRecorder!!.stop()
//        handler!!.removeCallbacks(
//            timerRunnable!!
//        )
        handler!!.removeCallbacks(
            progressRunnable!!
        )

        recordVideo = true

        DownloadFilesTask().execute()
    }

    fun darkTextSpeed(view: TextView) {

        view.setTextColor(Color.BLACK)
        view.setBackgroundResource(R.drawable.circle_speed_video)

    }

    private fun visibleAnimation(view: View) {

        view.animate().alpha(10F).setDuration(400).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = VISIBLE
            }
        })

    }

    fun animateSelectedView(view: View, marginLeft: Int, duration: Long) {

        val params =
            view.layoutParams as ViewGroup.MarginLayoutParams

        val animator =
            ValueAnimator.ofInt(params.leftMargin, marginLeft)

        animator.addUpdateListener { valueAnimator ->
            params.leftMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }

        animator.duration = duration
        animator.start()

    }

    fun inVisibleAnimation(view: View) {

        view.animate().alpha(0F).setDuration(100).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = INVISIBLE
            }
        })

    }

    fun inVisibleAnimation1(view: View) {

        view.animate().alpha(0F).setDuration(400).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = INVISIBLE
            }
        })

    }

    fun setNormalViews() {
        normalTextSpeedStyle(speed_1x_point)
        normalTextSpeedStyle(speed_2x_point)
        normalTextSpeedStyle(speed_1x)
        normalTextSpeedStyle(speed_2x)
        normalTextSpeedStyle(speed_3x)
    }

    fun normalTextSpeedStyle(view: TextView) {
        view.setTextColor(Color.WHITE)
        view.setBackgroundResource(R.drawable.transparent_background)
    }

    private fun startRecording() {
        createNewMp4File()
        videoFile.add(File(filepath))
        speedList.add(speedCount)
        mGPUCameraRecorder!!.start(filepath)
        recordVideo = false
        pauseButtonToggleEvents()
    }

    private fun resumeRecording() {
        mGPUCameraRecorder!!.onResume()
        pauseButtonToggleEvents()
    }

    private fun pauseButtonToggleEvents() {
//        mPlayer?.start()
        updateProgress()
        pause = true
    }

    fun createNewMp4File() {
        val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
        val now = Date()
        val fileName: String = formatter.format(now).toString() + ".mp4"
        try {
            val root = File(Environment.getExternalStorageDirectory().toString() + File.separator + "MusicFolder", "Video")
            //File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs()
            }
            val mp3File = File(root, fileName)
            filepath = mp3File.absolutePath

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun destroyHandler() {
        handler!!.removeCallbacks(
            progressRunnable!!
        )
    }

    override fun onResume() {
        super.onResume()
        setUpCamera()
    }

    override fun onStop() {
        super.onStop()
        releaseCamera()
    }

    private fun releaseCamera() {
        if (sampleGLView != null) {
            sampleGLView!!.onPause()
        }
        if (mGPUCameraRecorder != null) {
            mGPUCameraRecorder!!.stop()
            mGPUCameraRecorder!!.release()
            mGPUCameraRecorder = null
        }
        if (sampleGLView != null) {
            (findViewById<View>(R.id.wrap_view) as FrameLayout).removeView(
                sampleGLView
            )
            sampleGLView = null
        }
    }

    private fun setUpCameraView() {
        runOnUiThread {
            val frameLayout =
                findViewById<FrameLayout>(R.id.wrap_view)
            frameLayout.removeAllViews()
            sampleGLView = null
            sampleGLView = SampleCameraGLView(applicationContext)
            sampleGLView!!.setTouchListener { event: MotionEvent, width: Int, height: Int ->
                if (mGPUCameraRecorder == null) return@setTouchListener
                mGPUCameraRecorder!!.changeManualFocusPoint(event.x, event.y, width, height)
            }
            frameLayout.addView(sampleGLView)
        }
    }

    private fun setUpCamera() {
        setUpCameraView()
        mGPUCameraRecorder = GPUCameraRecorderBuilder(this, sampleGLView, false)
            //.filter(FilterType.createGlFilter(FilterType.BOX_BLUR, this))
            .cameraRecordListener(object : CameraRecordListener {
                override fun onGetFlashSupport(flashSupport: Boolean) {
                    runOnUiThread {
                        findViewById<View>(R.id.btn_flash).isEnabled = flashSupport
                    }
                }

                override fun onRecordComplete() {
                    Log.d("MAin", "DATA RECORD COMPLETED")
                    /*  exportMp4ToGallery(
                        applicationContext,
                        filepath
                    )*/
                }

                override fun onRecordStart() {
                    runOnUiThread {}
                }

                override fun onError(exception: Exception) {
                    Log.e("mGPUCameraRecorder", exception.toString())
                }

                override fun onCameraThreadFinish() {
                    Log.d("MAin", "DATA RECORD COMPLETED 11")
                    if (toggleClick) {
                        runOnUiThread { setUpCamera() }
                    }
                    toggleClick = false
                }

                override fun onVideoFileReady() {}
            })
            .videoSize(videoWidth, videoHeight)
            .cameraSize(cameraWidth, cameraHeight)
            .lensFacing(lensFacing)
            .build()
    }

    override fun onClick(position: Int, view: View) {
        if (mGPUCameraRecorder != null) {
            mGPUCameraRecorder!!.setFilter(
                FilterType.createGlFilter(
                    filterTypes!![position],
                    applicationContext
                )
            )
        }
    }

    override fun onStartRecordView() {
    }

    override fun onPauseRecordView() {

    }

    private interface BitmapReadyCallbacks {
        fun onBitmapReady(bitmap: Bitmap?)
    }

    private fun captureBitmap(bitmapReadyCallbacks: BitmapReadyCallbacks) {
        sampleGLView!!.queueEvent {
            val egl =
                EGLContext.getEGL() as EGL10
            val gl =
                egl.eglGetCurrentContext().gl as GL10
            val snapshotBitmap = createBitmapFromGLSurface(
                sampleGLView!!.measuredWidth,
                sampleGLView!!.measuredHeight,
                gl
            )
            runOnUiThread { bitmapReadyCallbacks.onBitmapReady(snapshotBitmap) }
        }
    }

    private fun createBitmapFromGLSurface(
        w: Int,
        h: Int,
        gl: GL10
    ): Bitmap? {
        val bitmapBuffer = IntArray(w * h)
        val bitmapSource = IntArray(w * h)
        val intBuffer = IntBuffer.wrap(bitmapBuffer)
        intBuffer.position(0)
        try {
            gl.glReadPixels(
                0,
                0,
                w,
                h,
                GL10.GL_RGBA,
                GL10.GL_UNSIGNED_BYTE,
                intBuffer
            )
            var offset1: Int
            var offset2: Int
            var texturePixel: Int
            var blue: Int
            var red: Int
            var pixel: Int
            for (i in 0 until h) {
                offset1 = i * w
                offset2 = (h - i - 1) * w
                for (j in 0 until w) {
                    texturePixel = bitmapBuffer[offset1 + j]
                    blue = texturePixel shr 16 and 0xff
                    red = texturePixel shl 16 and 0x00ff0000
                    pixel = texturePixel and -0xff0100 or red or blue
                    bitmapSource[offset2 + j] = pixel
                }
            }
        } catch (e: GLException) {
            Log.e("CreateBitmap", "createBitmapFromGLSurface: " + e.message, e)
            return null
        }
        return Bitmap.createBitmap(
            bitmapSource,
            w,
            h,
            Bitmap.Config.ARGB_8888
        )
    }

    fun saveAsPngImage(bitmap: Bitmap, filePath: String?) {
        try {
            val file = filePath?.let { File(it) }
            val outStream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
            outStream.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun updateProgress() {
        if (handler == null) {
            handler = Handler(Looper.myLooper()!!)
        }
        //Log.d("Base Activity", "Data Speed count: $speedCount")
        progressRunnable = Runnable {
            val temp = when (speedCount) {
                0.3f -> 17f / 0.3f
                0.5f -> 17f / 0.5f
                2f -> 17f / 2f
                3f -> 17f / 3f
                else -> 17f
            }
            /* testCount += when(speedCount) {
                 0.3f -> 17f * 0.3f
                 0.5f -> 17f * 0.2f
                 2f -> 17f / 2f
                 3f -> 17f / 3f
                 else -> 17f
             }*/
            testCount += temp
            //Log.d("Base Activity", "Data : $temp")
            //testCount += 17f
            val percentage = ((testCount * 100).toDouble() / 15000)
            if (((testCount * 100).toDouble() / 15000).toInt() <= 100) {
                countTimer = (((percentage * 15000) / 100) / 1000).toFloat()
                progress_horizontal!!.progress = (percentage * 1000.toDouble().toInt()).roundToInt()
                //wave.progress = percentage.toFloat()

            }

           // var time=
            mFragmentNormal?.videoTimer(15 - (((percentage * 15000) / 100) / 1000).toInt())

            if (countTimer.toInt() >= 10) {
                stringFormat = countTimer.toString().subSequence(0, 4).toString().toFloat()
            } else {
                stringFormat = countTimer.toString().subSequence(0, 3).toString().toFloat()
            }

            if ((mPlayer?.currentPosition!! * 100).toDouble() / mPlayer?.duration!!.toFloat() <= 100.00F) {
                wave.progress =
                    ((mPlayer?.currentPosition!! * 100).toDouble() / mPlayer?.duration!!).toFloat()
            }


            if (btn_clear.visibility == GONE && stringFormat >= 3) {
                btn_clear.visibility = VISIBLE
                btn_next.setTextColor(resources.getColor(R.color.pureWhite))
                btn_next.isEnabled = true
                back.visibility = GONE
            }
            if (stringFormat <= pauseTimer1) {
                if (stringFormat <= pauseTimer2) {
                    handler!!.postDelayed(progressRunnable!!, 10)
                } else {
                    pauseTimer2 = 15f
                    stopRunnableAtPause1()
                }

            } else {
                pauseTimer1 = 15f
                stopRunnableAtPause1()
            }

        }

        handler!!.postDelayed(progressRunnable!!, 10)

    }

    fun addPauseView() {
        if (stringFormat < 15) {
            val tv = View(this)
            tv.setBackgroundColor(Color.parseColor("#FFFFFF"))
            val params = RelativeLayout.LayoutParams(
                8, ViewGroup.LayoutParams.MATCH_PARENT
            )
            params.addRule(RelativeLayout.ALIGN_TOP, R.id.progress_horizontal)
            params.addRule(RelativeLayout.ALIGN_BOTTOM, R.id.progress_horizontal)
            val percentage = ((testCount * 100).toDouble() / 15000)
            params.leftMargin =
                ((progress_horizontal.width * percentage) / 100).roundToInt() + pxToDp(
                    18
                )
            layout.addView(tv, params)
            listPauseView.add(tv)
        }

        when (timerDelay) {
            /*0 -> {
                val rotate = RotateAnimation(
                    0f,
                    135f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f
                )
                rotate.duration = 100;
                rotate.fillAfter = true
                rotate.interpolator = LinearInterpolator()
                iv_timer_round.startAnimation(rotate)
                //scroll_timer.scrollToDescendant(tv_timer_3)
                scroll_timer.smoothScrollTo(0, tv_timer_0.height)
                timerDelay = 3
            }*/
            3 -> {
                val rotate = RotateAnimation(
                    135f,
                    -0f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f
                )
                rotate.duration = 100;
                rotate.fillAfter = true
                rotate.interpolator = LinearInterpolator()
                iv_timer_round.startAnimation(rotate)
                scroll_timer.smoothScrollTo(
                    tv_timer_0.height,
                    0
                )
                timerDelay = 0
            }
            5 -> {
                val rotate = RotateAnimation(
                    180f,
                    -0f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f
                )
                rotate.duration = 100;
                rotate.fillAfter = true
                rotate.interpolator = LinearInterpolator()
                iv_timer_round.startAnimation(rotate)
                scroll_timer.smoothScrollTo(
                    tv_timer_0.height + tv_timer_3.height,
                    0
                )
                timerDelay = 0
            }
            10 -> {
                val rotate = RotateAnimation(
                    225f,
                    -0f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f
                )
                rotate.duration = 100;
                rotate.fillAfter = true
                rotate.interpolator = LinearInterpolator()
                iv_timer_round.startAnimation(rotate)
                scroll_timer.smoothScrollTo(
                    tv_timer_0.height + tv_timer_3.height + tv_timer_5.height,
                    0
                )
                timerDelay = 0
            }
        }
        Log.d(
            "Base Camera Activity",
            "Progress --------------------------------------------------------------"
        )
    }

    fun stopRunnableAtPause1() {
        mFragmentNormal?.invisibleAtPause1()
    }

    @SuppressLint("SimpleDateFormat")
    companion object {
        fun exportMp4ToGallery(
            context: Context,
            filePath: String?
        ) {
            val values = ContentValues(2)
            values.put(MediaStore.Video.Media.MIME_TYPE, "video/mp4")
            values.put(MediaStore.Video.Media.DATA, filePath)
            context.contentResolver.insert(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                values
            )
            context.sendBroadcast(
                Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.parse("file://$filePath")
                )

            )
        }

        lateinit var androidMoviesFolder: File
        fun getVideoUrl(): String {

            androidMoviesFolder =
                File(Environment.getExternalStorageDirectory().absolutePath + "/Movies")
            // context.getDir("Movies", Context.MODE_PRIVATE); //Creating an internal dir;
            if (!androidMoviesFolder.exists()) {
                androidMoviesFolder.mkdirs();
            } else {
                androidMoviesFolder =
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES)
            }



            return androidMoviesFolder
                .absolutePath + "/" + SimpleDateFormat("yyyyMM_dd-HHmmss")
                .format(Date()) + "mGPUCameraRecorder.mp4"
        }


        private fun exportPngToGallery(
            context: Context,
            filePath: String
        ) {
            val mediaScanIntent =
                Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
            val f = File(filePath)
            val contentUri = Uri.fromFile(f)
            mediaScanIntent.data = contentUri
            context.sendBroadcast(mediaScanIntent)
        }

        val imageFilePath: String
            get() = androidImageFolder
                .absolutePath + "/" + SimpleDateFormat("yyyyMM_dd-HHmmss")
                .format(Date()) + "mGPUCameraRecorder.png"

        private val androidImageFolder: File
            get() = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)

        var isVideoOrStatus=true
    }

    override fun onRecordClick() {

        when {
            isVideoOrStatus -> {
                speed.invisible()
                timer.invisible()
                btn_filter.invisible()
            }
            else -> {
                speed.gone()
                timer.gone()
                btn_filter.invisible()
            }
        }

        if (timerDelay == 0) {
            startRecording()
        } else {
            mFragmentNormal?.onTimerOnRecordGone(true)
            val countDownAnimation = CountDownAnimation(tv_timer_count, timerDelay)
            // Use a set of animations
            val scaleAnimation: Animation = ScaleAnimation(
                1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )
            val alphaAnimation: Animation = AlphaAnimation(1.0f, 0.0f)
            val animationSet = AnimationSet(false)
            animationSet.addAnimation(scaleAnimation)
            animationSet.addAnimation(alphaAnimation)
            countDownAnimation.animation = animationSet


            countDownAnimation.setCountDownListener(object : CountDownAnimation.CountDownListener {
                override fun onCountDownEnd(animation: CountDownAnimation?) {
                    mFragmentNormal?.onTimerOnRecordGone(false)
                    startRecording()
                }
            })
            countDownAnimation.start()
        }
    }

    override fun onPauseClick() {

        if(isVideoOrStatus)
        {
            speed.visible()
            timer.visible()
        }
        else{
            speed.gone()
            timer.gone()
        }

        btn_filter.visible()
      //  pauseRecording()
        addPauseView()
        stopRecording()
       // pauseRecording()
    }

    private fun pauseRecording() {

        addPauseView()
        mGPUCameraRecorder!!.pause()
        destroyHandler()
        pause = false
    }

    fun stopRecording() {
        mGPUCameraRecorder!!.stop()
        destroyHandler()
        recordVideo = true
    }
    override fun onResumeClick() {
        when {
            isVideoOrStatus -> {
                speed.invisible()
                timer.invisible()
                btn_filter.invisible()
            }
            else -> {
                speed.gone()
                timer.gone()
                btn_filter.invisible()
            }
        }


        if (timerDelay == 0) {
            resumeRecording()
           // startRecording()
        } else {
            mFragmentNormal?.onTimerOnRecordGone(true)
            val countDownAnimation = CountDownAnimation(tv_timer_count, timerDelay)
            // Use a set of animations
            val scaleAnimation: Animation = ScaleAnimation(
                1.0f, 0.0f, 1.0f,
                0.0f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )
            val alphaAnimation: Animation = AlphaAnimation(1.0f, 0.0f)
            val animationSet = AnimationSet(false)
            animationSet.addAnimation(scaleAnimation)
            animationSet.addAnimation(alphaAnimation)
            countDownAnimation.animation = animationSet

            countDownAnimation.setCountDownListener(object : CountDownAnimation.CountDownListener {
                override fun onCountDownEnd(animation: CountDownAnimation?) {
                    mFragmentNormal?.onTimerOnRecordGone(false)
                    resumeRecording()
                }
            })
            countDownAnimation.start()
        }

    }

    override fun onEditClick() {
        setAnimateViewAtPosition(divider, -100)
    }

    override fun onFilterCancleClick() {

        sticker?.isEnabled = true
        speed?.isEnabled = true
        btn_filter?.isEnabled = true
        if(!btn_clear.isVisible)
            setAnimateViewAtPosition(divider, convertDpToPixel(7f, this).toInt())

    }

    override fun onItemFilterClick(position: Int) {
        if (mGPUCameraRecorder != null) {
            mGPUCameraRecorder!!.setFilter(
                FilterType.createGlFilter(
                    filterTypes!![position],
                    applicationContext
                )
            )
        }
    }

    override fun onDisableClick() {
        if (mGPUCameraRecorder != null) {
            mGPUCameraRecorder!!.setFilter(
                FilterType.createGlFilter(
                    filterTypes!![0],
                    applicationContext
                )
            )
        }
    }

    override fun onFilterMenuClick() {
        setAnimateViewAtPositionTop(relative_layout, 100, 500)
    }

    override fun onPause1TimerClose(pauseTimer: Float) {
        Log.d("Music", "Music Pause 1 : " + pauseTimer)
        if (pauseTimer in 0f..15f) {
            pauseTimer1 = pauseTimer
        }
    }

    override fun onPause2TimerClose(pauseTimer: Float) {
        Log.d("Music", "Music Pause 2 : " + pauseTimer)
        if (pauseTimer in 0f..15f) {
            pauseTimer2 = pauseTimer
        }
    }

    override fun onTimerSliderUp() {
        visibilityGoneView()
    }

    override fun onTimerSliderDown() {
        visibilityVisibleView()
    }

    override fun onClick1(position: Int) {
    }

    fun pxToDp(px: Int): Int {
        return (px / Resources.getSystem().getDisplayMetrics().density).toInt()
    }

    private fun mediaExtractor() {
        val extractor = MediaExtractor();
        var frameRate = 24; //may be default
        try {
            //Adjust data source as per the requirement if file, URI, etc.
            //extractor.setDataSource(sharedFileUri.path!!)
            extractor.setDataSource("/storage/emulated/0/Movies/202010_23-113520mGPUCameraRecorder.mp4")
            val numTracks = extractor.getTrackCount();
            //for (int i = 0; i < numTracks; ++i) {
            for (i in 0 until numTracks) {
                val format = extractor.getTrackFormat(i);
                val mime = format.getString(MediaFormat.KEY_MIME);
                //if (mime?.startsWith("video/")!!) {
                if (format.containsKey(MediaFormat.KEY_FRAME_RATE)) {
                    frameRate = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                }
                //}
            }
            Log.d("Base Camera Activity", "Data ttttttt: $frameRate")
        } catch (e: IOException) {
            e.printStackTrace();
        } finally {
            //Release stuff
            extractor.release();
        }

    }

    var audioFile: String? = ""
    override fun setAudioListener(dummy: SongsModel, leftDuration: Int, rightDuration: Int) {
        Log.d("tag", "Audio Online 111: " + dummy)
        back_image.performClick()
        tv_music.text = dummy.mSongsName
        iv_music_cancel.visibility = VISIBLE
        audioFile = dummy.mPath


        Log.e("leftDuration ", "$leftDuration :::$rightDuration")

        if(!audioFile.equals(".mp4",ignoreCase = true) )
        {
            cutAudioFile(audioFile!!,leftDuration,rightDuration)
        }
        val mp = MediaPlayer.create(this, Uri.parse(audioFile))
        val duration = mp.duration
        pauseTimer2 =duration.toFloat()
        mp.release()



        /*var fileExtension=audioFile!!.substring(audioFile!!.lastIndexOf("."))

        var checkRightDuration=rightDuration/1000

        Log.d("tag", "Audio Online 111: $audioFile :: $leftDuration ::: $rightDuration ::: $audioFileSecond")

        if(!fileExtension.equals(".mp4",ignoreCase = true) && checkRightDuration<=audioFileSecond)
        {
            cutAudioFile(audioFile!!,leftDuration,rightDuration)
        }*/

        //val thread = Thread(convert)
        // thread.start()
        //   AudioVideoTask().execute(Utils.AUDIO_URL + dummy.mPath)
    }



    fun cutAudioFile(inputFilePath:String,leftDuration: Int, rightDuration: Int)
    {
     /*   val cheapSoundFile: CheapSoundFile = CheapSoundFile.create(inputFilePath) { true }

        val mSampleRate: Int = cheapSoundFile.sampleRate

        val mSamplesPerFrame: Int = cheapSoundFile.samplesPerFrame
        val startFrame:Int
        startFrame = if(0<leftDuration) {
            var leftDuration=leftDuration/1000
            Util.secondsToFrames(0.0, mSampleRate, mSamplesPerFrame)
        } else{
            Util.secondsToFrames(0.0, mSampleRate, mSamplesPerFrame)
        }
        var rightDuration=rightDuration/1000
        val endFrame: Int = Util.secondsToFrames(10.0, mSampleRate, mSamplesPerFrame)
        audioFile = createNewFileMp3().absolutePath
        cheapSoundFile.WriteFile(File(audioFile), startFrame, endFrame - startFrame)*/

        try {
            FFmpeg.getInstance(this).loadBinary(object : FFmpegLoadBinaryResponseHandler {
                override fun onFailure() {
                    Log.v("FFMpeg", "Failed to load FFMpeg library.")
                }

                override fun onSuccess() {
                    Log.v("FFMpeg", "FFMpeg Library loaded!")
                }

                override fun onStart() {
                    Log.v("FFMpeg", "FFMpeg Started")
                }

                override fun onFinish() {
                    Log.v("FFMpeg", "FFMpeg Stopped")
                }
            })
        } catch (e: FFmpegNotSupportedException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val outputFile = OptiUtils.createAudioFile(applicationContext)
        Log.e("Success Path", "outputFile: ${outputFile.absolutePath}")

        OptiVideoEditor.with(applicationContext!!)
            .setType(OptiConstant.AUDIO_TRIM)
            .setAudioFile(File(audioFile!!))
            .setOutputPath(outputFile.absolutePath)
            .setStartTime(secToTime(leftDuration.toLong()))
            .setEndTime(secToTime(rightDuration.toLong()))
            .setCallback(object : OptiFFMpegCallback {
                override fun onProgress(progress: String) {

                }

                override fun onSuccess(convertedFile: File, type: String) {
                    audioFile=outputFile.absolutePath
                    Log.e("Success PSSSSath", "outputFile: ${outputFile.absolutePath}")
                }

                override fun onFailure(error: Exception) {
                    Log.e("Success Fail Path", "outputFile: ${error.toString()}")
                }

                override fun onNotAvailable(error: Exception) {

                }

                override fun onFinish() {
                    Log.e("Success Fiiii", "outputFile: ${outputFile.absolutePath}")
                }

            })
            .main()
    }

    fun createNewFileMp3():File
    {
        val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
        val now = Date()
        val fileName: String = formatter.format(now).toString() + ".mp3"
        try {
            val root = File(
                Environment.getExternalStorageDirectory()
                    .toString() + File.separator + "Music_Folder", "AudioFiles"
            )
            //File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            if (!root.exists()) {
                root.mkdirs()
            }
            val mp3File = File(root, fileName)

            return mp3File
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null!!
    }

    override fun setAudioListener(
        dummy: AudioListQuery.Audio,
        leftDuration: Int,
        rightDuration: Int
    ) {
        Log.d("tag", "Audio Online 111: " + dummy)
        back_image.performClick()
        tv_music.text = dummy.name()
        iv_music_cancel.visibility = VISIBLE
        Log.d("tag", "Audio Online 111: " + audioFile)
        //AudioVideoTask().execute(Utils.AUDIO_URL + dummy.name())
        downloadWithFlow(Utils.AUDIO_URL + dummy.name())
    }
    private fun downloadWithFlow(url: String) {
        audioFile= com.example.sare.extensions.createNewFileMp3()
        val progressDialog = CustomProgressDialogNew(this)
        progressDialog.show()
        CoroutineScope(Dispatchers.IO).launch {
            ktor.downloadFile(File(audioFile), url).collect {
                withContext(Dispatchers.Main) {
                    when (it) {
                        is DownloadResult.Success -> {
                            progressDialog.hide()
                        }
                        is DownloadResult.Error -> {
                            progressDialog.hide()
                            Log.d("tag", "error:::" + it.message)
                        }
                        is DownloadResult.Progress -> {
                            Log.d("tag", "progresss:::" + it.progress)
                            progressDialog.setProgress(it.progress)
                        }
                        else -> "Throws Error.!!"
                    }
                }
            }
        }
    }


    private class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        var mScaleFactor = 1f

        // when a scale gesture is detected, use it to resize the image
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            mScaleFactor *= scaleGestureDetector.scaleFactor
            Log.d("Test", "Base Factor Scale : " + mScaleFactor)
            //mImageView.setScaleX(mScaleFactor)
            //mImageView.setScaleY(mScaleFactor)
            return true
        }
    }

    open fun concatenateFiles(dst: File, sources: MutableList<File>): Boolean {

        if (sources == null || sources.size == 0) {
            return false
        }
        var result: Boolean
        var extractor: MediaExtractor? = null
        var muxer: MediaMuxer? = null
        try {
            // Set up MediaMuxer for the destination.
            muxer = MediaMuxer(dst.path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)

            // Copy the samples from MediaExtractor to MediaMuxer.
            var sawEOS = false
            //int bufferSize = MAX_SAMPLE_SIZE;
            val bufferSize = 1 * 1024 * 1024
            var frameCount = 0
            val offset = 100
            val dstBuf: ByteBuffer = ByteBuffer.allocate(bufferSize)
            val bufferInfo: BufferInfo = BufferInfo()
            var timeOffsetUs: Long = 0
            var dstTrackIndex = -1


            for (fileIndex in 0 until sources.size) {
                val numberOfSamplesInSource = getNumberOfSamples(sources[fileIndex])

                // Set up MediaExtractor to read from the source.
                extractor = MediaExtractor()
                extractor.setDataSource(sources[fileIndex].path)

                // Set up the tracks.
                val indexMap = SparseIntArray(extractor.trackCount)
                for (i in 0 until extractor.trackCount) {
                    extractor.selectTrack(i)
                    val format = extractor.getTrackFormat(i)
                    if (dstTrackIndex < 0) {
                        dstTrackIndex = muxer.addTrack(format)
                        muxer.start()
                    }
                    indexMap.put(i, dstTrackIndex)
                }
                var lastPresentationTimeUs: Long = 0
                var currentSample = 0
                sawEOS = false
                while (!sawEOS) {
                    bufferInfo.offset = offset
                    bufferInfo.size = extractor.readSampleData(dstBuf, offset)
                    if (bufferInfo.size < 0) {
                        sawEOS = true
                        bufferInfo.size = 0
                        timeOffsetUs += lastPresentationTimeUs + 0
                    } else {
                        lastPresentationTimeUs = extractor.sampleTime
                        bufferInfo.presentationTimeUs =
                            (((extractor.sampleTime) + timeOffsetUs) / speedList[fileIndex]).toLong() // presentationTimesUs /3 means 3x speed
                        bufferInfo.flags = extractor.sampleFlags
                        val trackIndex = extractor.sampleTrackIndex
                        if (currentSample < numberOfSamplesInSource) {
                            muxer.writeSampleData(indexMap.get(trackIndex), dstBuf, bufferInfo)
                        }
                        extractor.advance()
                        frameCount++
                        currentSample++
                        Log.d(
                            "tag2", "File Size Frame (" + frameCount + ") " +
                                    "PresentationTimeUs:" + bufferInfo.presentationTimeUs +
                                    " Flags:" + bufferInfo.flags +
                                    " TrackIndex:" + trackIndex +
                                    " Size(KB) " + bufferInfo.size / 1024
                        )
                    }
                }
                extractor.release()
                extractor = null
            }
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
            result = false
        } finally {
            extractor?.release()
            if (muxer != null) {
                muxer.stop()
                muxer.release()
            }
        }
        return result
    }

    open fun getNumberOfSamples(src: File): Int {
        val extractor = MediaExtractor()
        var result: Int
        try {
            extractor.setDataSource(src.path)
            extractor.selectTrack(0)
            result = 0
            while (extractor.advance()) {
                result++
            }
        } catch (e: IOException) {
            result = -1
        } finally {
            extractor.release()
        }
        return result
    }

    inner class DownloadFilesTask : AsyncTask<URL?, Int?, String?>() {
        private var des = ""
        override fun doInBackground(vararg urls: URL?): String {
           /* des = androidMoviesFolder
          //                .absolutePath + "/" + SimpleDateFormat("yyyyMM_dd-HHmmss")
          //                .format(Date()) + "mGPUCameraRecorder_new.mp4"
          //          //  concatenateFiles(File(des), videoFile)*/
            return "des"
        }

        override fun onPostExecute(result: String?) {
            onStop()
            if (audioFile.toString().isNullOrEmpty()) {
                callActivity(filepath!!)

            } else {
                callActivity(filepath!!)
            }
        }
    }

    fun callActivity(des:String)
    {
        if(isVideoOrStatus)
        {
            progressDialog = CustomProgressDialogNew(this)
            progressDialog.show()

            Handler().postDelayed({
                progressDialog!!.dismiss()
                val intent = Intent(this@BaseCameraActivity, VideoPreviewActivity::class.java)
                .putExtra("videoFile", des)
                .putExtra("audioFile", audioFile)
                .putExtra("speed",speedCount.toString())
                startActivity(intent)
                finish()

            }, 3000)


        }
        else
        {
            val intent = Intent(this@BaseCameraActivity, VideoStatusPreviewActivity::class.java)
                .putExtra("videoFile", des)
            startActivity(intent)
            finish()
        }
    }

    fun onHandler() {

    }


    inner class AudioVideoTask : AsyncTask<String?, Int?, String?>() {
        private var des = ""

        override fun doInBackground(vararg str: String?): String {
            des = androidMoviesFolder
                .absolutePath + "/" + SimpleDateFormat("yyyyMM_dd-HHmmss")
                .format(Date()) + "mGPUCameraRecorder_new_1.mp4"
            return des
        }

        override fun onPostExecute(result: String?) {
            //showDialog("Downloaded $result bytes")
            Log.d("tag", "Audio Online 111: onPostExecute" + des)
            onStop()
            val intent = Intent(this@BaseCameraActivity, VideoPreviewActivity::class.java)
                .putExtra("videoFile", des)
                .putExtra("audioFile", des)
            startActivity(intent)
            finish()
        }
    }

    val TAG = "BaseCameraActivity"
    open fun muxing(des: File, videoFilePath: String, audioFilePath: String) {
        var outputFile = ""
        try {
            /* val file = File(
                 desVideoPath
             )*/
            //file.createNewFile()
            // outputFile = file.absolutePath
            val videoExtractor = MediaExtractor()
            //val afdd: AssetFileDescriptor = assets.openFd("Produce.MP4")
            /*videoExtractor.setDataSource(
                afdd.getFileDescriptor(),
                afdd.getStartOffset(),
                afdd.getLength()
            )*/
            videoExtractor.setDataSource(videoFilePath)
            val audioExtractor = MediaExtractor()
            audioExtractor.setDataSource(audioFilePath)
            Log.d(TAG, "Video Extractor Track Count " + videoExtractor.trackCount)
            Log.d(TAG, "Audio Extractor Track Count " + audioExtractor.trackCount)
            val muxer = MediaMuxer(des.path, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)
            videoExtractor.selectTrack(0)
            val videoFormat = videoExtractor.getTrackFormat(0)
            val videoTrack = muxer.addTrack(videoFormat)
            audioExtractor.selectTrack(0)
            val audioFormat = audioExtractor.getTrackFormat(0)
            val audioTrack = muxer.addTrack(audioFormat)
            Log.d(TAG, "Video Format $videoFormat")
            Log.d(TAG, "Audio Format $audioFormat")
            var sawEOS = false
            var frameCount = 0
            val offset = 100
            val sampleSize = 256 * 1024
            val videoBuf = ByteBuffer.allocate(sampleSize)
            val audioBuf = ByteBuffer.allocate(sampleSize)
            val videoBufferInfo: MediaCodec.BufferInfo = BufferInfo()
            val audioBufferInfo: MediaCodec.BufferInfo = BufferInfo()
            videoExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC)
            audioExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC)
            muxer.start()
            while (!sawEOS) {
                videoBufferInfo.offset = offset
                videoBufferInfo.size = videoExtractor.readSampleData(videoBuf, offset)
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    Log.d(TAG, "saw input EOS.")
                    sawEOS = true
                    videoBufferInfo.size = 0
                } else {
                    videoBufferInfo.presentationTimeUs = videoExtractor.sampleTime
                    videoBufferInfo.flags = videoExtractor.sampleFlags
                    muxer.writeSampleData(videoTrack, videoBuf, videoBufferInfo)
                    videoExtractor.advance()
                    frameCount++
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs + " Flags:" + videoBufferInfo.flags + " Size(KB) " + videoBufferInfo.size / 1024
                    )
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs + " Flags:" + audioBufferInfo.flags + " Size(KB) " + audioBufferInfo.size / 1024
                    )
                }
            }
            Toast.makeText(applicationContext, "frame:$frameCount", Toast.LENGTH_SHORT).show()
            var sawEOS2 = false
            var frameCount2 = 0
            while (!sawEOS2) {
                frameCount2++
                audioBufferInfo.offset = offset
                audioBufferInfo.size = audioExtractor.readSampleData(audioBuf, offset)
                if (videoBufferInfo.size < 0 || audioBufferInfo.size < 0) {
                    Log.d(TAG, "saw input EOS.")
                    sawEOS2 = true
                    audioBufferInfo.size = 0
                } else {
                    audioBufferInfo.presentationTimeUs = audioExtractor.sampleTime
                    audioBufferInfo.flags = audioExtractor.sampleFlags
                    muxer.writeSampleData(audioTrack, audioBuf, audioBufferInfo)
                    audioExtractor.advance()
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Video PresentationTimeUs:" + videoBufferInfo.presentationTimeUs + " Flags:" + videoBufferInfo.flags + " Size(KB) " + videoBufferInfo.size / 1024
                    )
                    Log.d(
                        TAG,
                        "Frame (" + frameCount + ") Audio PresentationTimeUs:" + audioBufferInfo.presentationTimeUs + " Flags:" + audioBufferInfo.flags + " Size(KB) " + audioBufferInfo.size / 1024
                    )
                }
            }
            Toast.makeText(applicationContext, "frame:$frameCount2", Toast.LENGTH_SHORT).show()
            muxer.stop()
            muxer.release()
        } catch (e: IOException) {
            e.printStackTrace()
            Log.d(TAG, "Mixer Error 1 " + e.message)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            Log.d(TAG, "Mixer Error 2 " + e.message)
        }
    }


    open fun mux(outputFile: String?, videoFile: String?, audioFile: String?): Boolean {
        val video: Movie
        try {
            video = MovieCreator.build(videoFile)
        } catch (e: RuntimeException) {
            e.printStackTrace()
            return false
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        val audio: Movie
        try {
            Log.d("BaseCameraActivity", "Audio : " + audioFile)
            audio = MovieCreator.build(audioFile)
            Log.d("BaseCameraActivity", "audio : " + audio)

            val out: Container = DefaultMp4Builder().build(audio)

            val currentMillis = System.currentTimeMillis()
            val fos =
                FileOutputStream(File(Environment.getExternalStorageDirectory().absolutePath + "/Audio/test$currentMillis.m4a"))
            out.writeContainer(fos.channel)
            fos.close()

            val inFile = File(audioFile)
            if (inFile.delete()) {
                val tempOutFile =
                    File(Environment.getExternalStorageDirectory().absolutePath + "/Audio/test$currentMillis.m4a")
                tempOutFile.renameTo(inFile)
            }
            Log.d("BaseCameraActivity", "inFile : " + inFile)

        } catch (e: IOException) {
            e.printStackTrace()
            return false
        } catch (e: NullPointerException) {
            e.printStackTrace()
            return false
        }
        val audioTrack = audio.tracks[0]
        video.addTrack(audioTrack)
        val out = DefaultMp4Builder().build(video)
        val fos: FileOutputStream
        fos = try {
            FileOutputStream(outputFile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
            return false
        }
        val byteBufferByteChannel = BufferedWritableFileByteChannel(fos)
        try {
            out.writeContainer(byteBufferByteChannel)
            byteBufferByteChannel.close()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
            return false
        }
        return true
    }

    private class BufferedWritableFileByteChannel(private val outputStream: OutputStream) :
        WritableByteChannel {
        private var isOpen = true
        private val byteBuffer: ByteBuffer
        private val rawBuffer = ByteArray(BUFFER_CAPACITY)

        @Throws(IOException::class)
        override fun write(inputBuffer: ByteBuffer): Int {
            val inputBytes = inputBuffer.remaining()
            if (inputBytes > byteBuffer.remaining()) {
                dumpToFile()
                byteBuffer.clear()
                if (inputBytes > byteBuffer.remaining()) {
                    throw BufferOverflowException()
                }
            }
            byteBuffer.put(inputBuffer)
            return inputBytes
        }

        override fun isOpen(): Boolean {
            return isOpen
        }

        @Throws(IOException::class)
        override fun close() {
            dumpToFile()
            isOpen = false
        }

        private fun dumpToFile() {
            try {
                outputStream.write(rawBuffer, 0, byteBuffer.position())
            } catch (e: IOException) {
                throw RuntimeException(e)
            }
        }

        companion object {
            private const val BUFFER_CAPACITY = 1000000
        }

        init {
            byteBuffer = ByteBuffer.wrap(rawBuffer)
        }
    }

    //val AUDIO_RECORDING_FILE_NAME = "audio_Capturing-190814-034638.422.wav" // Input PCM file

    val COMPRESSED_AUDIO_FILE_NAME = "convertedmp4.m4a" // Output MP4/M4A file

    val COMPRESSED_AUDIO_FILE_MIME_TYPE = "audio/mp4a-latm"
    val COMPRESSED_AUDIO_FILE_BIT_RATE = 64000 // 64kbps

    val SAMPLING_RATE = 48000
    val BUFFER_SIZE = 48000
    val CODEC_TIMEOUT_IN_MS = 5000
    var LOGTAG = "CONVERT AUDIO"

    var convert = Runnable {
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND)
        try {
            val filePath =
                audioFile
            val inputFile = File(filePath)
            val fis = FileInputStream(inputFile)
            val outputFile =
                File(Environment.getExternalStorageDirectory().absolutePath + "/" + COMPRESSED_AUDIO_FILE_NAME)
            if (outputFile.exists()) outputFile.delete()
            val mux =
                MediaMuxer(outputFile.absolutePath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4)
            var outputFormat =
                MediaFormat.createAudioFormat(COMPRESSED_AUDIO_FILE_MIME_TYPE, SAMPLING_RATE, 1)
            outputFormat.setInteger(
                MediaFormat.KEY_AAC_PROFILE,
                MediaCodecInfo.CodecProfileLevel.AACObjectLC
            )
            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, COMPRESSED_AUDIO_FILE_BIT_RATE)
            outputFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 16384)
            val codec = MediaCodec.createEncoderByType(COMPRESSED_AUDIO_FILE_MIME_TYPE)
            codec.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
            codec.start()
            val codecInputBuffers = codec.inputBuffers // Note: Array of buffers
            val codecOutputBuffers = codec.outputBuffers
            val outBuffInfo = BufferInfo()
            val tempBuffer = ByteArray(BUFFER_SIZE)
            var hasMoreData = true
            var presentationTimeUs = 0.0
            var audioTrackIdx = 0
            var totalBytesRead = 0
            var percentComplete = 0
            do {
                var inputBufIndex = 0
                while (inputBufIndex != -1 && hasMoreData) {
                    inputBufIndex = codec.dequeueInputBuffer(CODEC_TIMEOUT_IN_MS.toLong())
                    if (inputBufIndex >= 0) {
                        val dstBuf = codecInputBuffers[inputBufIndex]
                        dstBuf.clear()
                        val bytesRead = fis.read(tempBuffer, 0, dstBuf.limit())
                        Log.e("bytesRead", "Readed $bytesRead")
                        if (bytesRead == -1) { // -1 implies EOS
                            hasMoreData = false
                            codec.queueInputBuffer(
                                inputBufIndex, 0, 0,
                                presentationTimeUs.toLong(), MediaCodec.BUFFER_FLAG_END_OF_STREAM
                            )
                        } else {
                            totalBytesRead += bytesRead
                            dstBuf.put(tempBuffer, 0, bytesRead)
                            codec.queueInputBuffer(
                                inputBufIndex, 0, bytesRead,
                                presentationTimeUs.toLong(), 0
                            )
                            presentationTimeUs =
                                1000000L * (totalBytesRead / 2) / SAMPLING_RATE.toDouble()
                        }
                    }
                }
                // Drain audio
                var outputBufIndex = 0
                while (outputBufIndex != MediaCodec.INFO_TRY_AGAIN_LATER) {
                    outputBufIndex =
                        codec.dequeueOutputBuffer(outBuffInfo, CODEC_TIMEOUT_IN_MS.toLong())
                    if (outputBufIndex >= 0) {
                        val encodedData = codecOutputBuffers[outputBufIndex]
                        encodedData.position(outBuffInfo.offset)
                        encodedData.limit(outBuffInfo.offset + outBuffInfo.size)
                        if (outBuffInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG != 0 && outBuffInfo.size != 0) {
                            codec.releaseOutputBuffer(outputBufIndex, false)
                        } else {
                            mux.writeSampleData(
                                audioTrackIdx,
                                codecOutputBuffers[outputBufIndex], outBuffInfo
                            )
                            codec.releaseOutputBuffer(outputBufIndex, false)
                        }
                    } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                        outputFormat = codec.outputFormat
                        Log.v(LOGTAG, "Output format changed - $outputFormat")
                        audioTrackIdx = mux.addTrack(outputFormat)
                        mux.start()
                    } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
                        Log.e(LOGTAG, "Output buffers changed during encode!")
                    } else if (outputBufIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {
                        // NO OP
                    } else {
                        Log.e(
                            LOGTAG,
                            "Unknown return code from dequeueOutputBuffer - $outputBufIndex"
                        )
                    }
                }
                percentComplete = Math.round(
                    totalBytesRead.toFloat() / inputFile.length()
                        .toFloat() * 100.0
                ).toInt()
                Log.v(LOGTAG, "Conversion % - $percentComplete")
            } while (outBuffInfo.flags != MediaCodec.BUFFER_FLAG_END_OF_STREAM)
            fis.close()
            mux.stop()
            mux.release()
            Log.v(LOGTAG, "Compression done ...")
        } catch (e: FileNotFoundException) {
            Log.e(LOGTAG, "File not found!", e)
        } catch (e: IOException) {
            Log.e(LOGTAG, "IO exception!", e)
        }

        //mStop = false;
        // Notify UI thread...
    }


}