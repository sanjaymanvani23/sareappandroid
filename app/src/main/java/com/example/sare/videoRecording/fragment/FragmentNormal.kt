package com.example.sare.videoRecording.fragment

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.view.animation.AnimationUtils
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.extensions.invisible
import com.example.sare.videoRecording.*
import com.example.sare.videoRecording.BaseCameraActivity.Companion.isVideoOrStatus
import com.example.sare.videoRecording.adapter.FilterCategoryBottomSliderAdapter
import com.example.sare.videoRecording.adapter.FilterRecyclerAdapter
import com.example.sare.videoRecording.model.FliterModel
import kotlinx.android.synthetic.main.bottom_slider.*
import kotlinx.android.synthetic.main.bottom_slider.view.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue

class FragmentNormal(baseCameraActivity: BaseCameraActivity) : Fragment(), FilterRecyclerAdapter.OnFilterClick {

    var clickFlag = true
    var lastPosition = 0
    private var savedWidth = 0
    private var savedHight = 0
    var slide = true
    var mOnRecordButtonClick: OnRecordButtonClick = baseCameraActivity
    private var recordVideo = true
    private var filterTypes: List<FilterType>? = null

    private var fliterModelList: ArrayList<FliterModel>? = ArrayList()
    var root: View? = null
    var viewWidth = 0
    var viewHeight = 0
    var viewWidth1 = 0
    var viewHeight1 = 0
    var filterRecyclerAdapter: FilterRecyclerAdapter? = null

    var swipeCount = 0
    private var yDelta = 0
    var layoutParams: RelativeLayout.LayoutParams? = null
    private var startClickTime: Long = 0
    private val MAX_CLICK_DURATION = 200
    private var pauseClick = false
    private var pause1Click = false
    private var timerText2 = 0
    private var timerText1 = 0
    private var textDuration1 = 0
    private var textDuration2 = 15
    private var duration = 0
    private val REQUEST_TAKE_GALLERY_VIDEO = 1210
    val displayMetrics = DisplayMetrics()
    var screenWidth = 0
    lateinit var sliderLayoutManager: SliderLayoutManager
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenWidth = displayMetrics.widthPixels

        root = inflater.inflate(R.layout.bottom_slider, container, false)
        root?.circle?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        root?.imgPuase?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()



        root?.fixTime?.visibility = GONE
        root?.pause_button?.visibility = GONE

        setFilterRecyclerView()
        setFilterCategoryRecyclerView()


        viewWidth = root?.record_button?.width!!
        viewWidth1 = root?.circle?.width!!
        viewHeight = root?.record_button?.height!!
        viewHeight1 = root?.circle?.height!!

        //root?.pauseText?.setOnTouchListener(onTouchListener())
        root?.pauseText1?.setOnTouchListener(onTouchListener())

        layoutParams = root?.timer_image?.layoutParams as RelativeLayout.LayoutParams

        root?.max_timer_text?.setOnClickListener {

            duration = 0
            textDuration1 = 15
            textDuration2 = 15

            root?.selected_time1?.visibility = GONE

            val pauseText1LayoutParams = root?.pauseText1?.layoutParams as RelativeLayout.LayoutParams
            pauseText1LayoutParams.topMargin = convertDpToPixel(46f, requireContext()).toInt()
            pauseText1LayoutParams.rightMargin = 22
            pauseText1LayoutParams.bottomMargin = 0
            root?.pauseText1?.layoutParams = pauseText1LayoutParams

            val pauseText1BgLayoutParams = root?.pauseText1_bg?.layoutParams as RelativeLayout.LayoutParams
            pauseText1BgLayoutParams.topMargin = convertDpToPixel(46f, requireContext()).toInt()
            pauseText1BgLayoutParams.rightMargin = 22
            pauseText1BgLayoutParams.bottomMargin = 0
            root?.pauseText1_bg?.layoutParams = pauseText1BgLayoutParams

            val layoutParams1 = root?.pause_view_line1?.layoutParams as RelativeLayout.LayoutParams
            layoutParams1.topMargin = convertDpToPixel(
                46f, requireContext()).toInt() + (pauseText1LayoutParams.height / 2)
            layoutParams1.rightMargin = 0
            layoutParams1.bottomMargin = 0
            root?.pause_view_line1?.layoutParams = layoutParams1

            val timerText1Params = root?.timer_text1?.layoutParams as RelativeLayout.LayoutParams
            timerText1Params.topMargin = convertDpToPixel(46f, requireContext()).toInt()
            timerText1Params.rightMargin = 0
            timerText1Params.bottomMargin = 0
            root?.timer_text1?.layoutParams = timerText1Params

            root?.timer_text1?.text = "15.0"
            root?.selected_time1?.text = "15.0"

        }

        root?.time?.setOnClickListener {

            if (slide) {
                val slideUp = AnimationUtils.loadAnimation(
                    requireContext(), R.anim.slide_up)

                root?.time_slider?.startAnimation(slideUp)
                root?.time_slider?.visibility = VISIBLE

                visibleAnimation(root?.pause_view_line1!!)
                visibleAnimation(root?.pauseText1!!)
                visibleAnimation(root?.pauseText1_bg!!)

                if (root?.selected_time1?.text.toString().substring(
                        0, (root?.selected_time1?.text?.length ?: 1) - 1).toFloat() in 3f..13f
                ) {
                    visibleAnimation(root?.selected_time1!!)
                }

                mOnRecordButtonClick.onTimerSliderUp()

                slide = false

            } else {

                duration = textDuration1 - textDuration2
                val sliderDown = AnimationUtils.loadAnimation(
                    requireContext(), R.anim.timer_slide_down)

                root?.time_slider?.startAnimation(sliderDown)
                root?.time_slider?.visibility = GONE

                inVisibleAnimation(root?.pause_view_line1!!)
                inVisibleAnimation(root?.pauseText1!!)
                inVisibleAnimation(root?.pauseText1_bg!!)
                inVisibleAnimation(root?.selected_time1!!)

                mOnRecordButtonClick.onTimerSliderDown()

                mOnRecordButtonClick.onPause1TimerClose(
                    pauseTimer = selected_time1.text.toString().substring(
                        0, selected_time1.text.length - 1).toFloat())
                slide = true

            }
        }

        root?.record_button?.setOnClickListener {
            if (root?.time_slider?.visibility == VISIBLE) {
                Toast.makeText(
                    requireContext(), "Please set pause than after start video recording.", Toast.LENGTH_SHORT).show()
            } else {
                root?.edit_image?.invisible()
                mOnRecordButtonClick.onEditClick()
                if (recordVideo) {
                    root?.record_button?.visibility = INVISIBLE
                    root?.pause_button?.visibility = VISIBLE
                    root?.circle?.animate()!!.translationY(0F).scaleX(0.8F).scaleY(0.8F).setDuration(
                        350).start()
                    root?.imgPuase?.animate()!!.translationY(0F).scaleX(0.8F).scaleY(0.8F).setDuration(
                        350).start()
                    recordVideo = false
                    root?.time?.visibility = GONE
                    root?.fixTime?.visibility = VISIBLE
                    mOnRecordButtonClick.onRecordClick()
                    root?.viewRedCirclePause?.visibility = VISIBLE

                } else {
                    root?.pause_button?.visibility = VISIBLE
                    root?.record_button?.visibility = INVISIBLE
                    root?.circle?.animate()!!.translationY(0F).scaleX(0.8F).scaleY(0.8F).setDuration(
                        350).start()
                    root?.imgPuase?.animate()!!.translationY(0F).scaleX(0.8F).scaleY(0.8F).setDuration(
                        350).start()
                    root?.time?.visibility = GONE
                    root?.fixTime?.visibility = VISIBLE
                    root?.viewRedCirclePause?.visibility = VISIBLE
                    mOnRecordButtonClick.onResumeClick()
                }
            }
        }

        root?.cancel_action?.setOnClickListener { root?.recycler_view?.smoothScrollToPosition(0)

            /*  root?.record_button?.visibility = VISIBLE
              root?.filterConstraintLayout?.visibility=View.INVISIBLE
              root?.cancel_action!!.visibility = INVISIBLE
              mOnRecordButtonClick.onFilterCancleClick()
              root?. bottom_filter!!.visibility = INVISIBLE
              root?. cancel_action!!.visibility = INVISIBLE
              setAnimateViewAtPosition(root?.cancel_action!!, convertDpToPixel(-30f, requireContext()).toInt()) // 135
              root?.edit_image?.visibility = VISIBLE
  */

            //  root?.record_button!!.animate().scaleX(1.0f).scaleY(1.0f)
            //  root?.pause_button!!.animate().scaleX(1.0f).scaleY(1.0f)

            // setAnimateViewAtPosition1(root?.bottom_filter!!, 0) //15

            //   setAnimateViewAtPosition(root?.bottom_filter!!, -650)
            // setAnimateViewAtPosition(root?.record_button!!, convertDpToPixel(50f, requireContext()).toInt()) // 135
            //   setAnimateViewAtPosition(root?.time!!, convertDpToPixel(65f, requireContext()).toInt())
            //  setAnimateViewAtPosition(root?.pause_button!!, convertDpToPixel(50f, requireContext()).toInt())
            //  setAnimateViewAtPosition(root?.fixTime!!, convertDpToPixel(65f, requireContext()).toInt())
            //setAnimateViewAtPosition(root?.timer_round!!, convertDpToPixel(63f, requireContext()).toInt())
            //   setAnimateViewAtPosition(root?.time_slider!!, convertDpToPixel(67f, requireContext()).toInt())

            //root?.time?.isEnabled = true
            //root?.fixTime?.isEnabled = true

        }

        root?.pause_button?.setOnClickListener {
            root?.pause_button?.visibility = INVISIBLE
            root?.record_button?.visibility = VISIBLE
            root?.circle?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
            root?.imgPuase?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
            root?.time?.visibility = VISIBLE
            root?.fixTime?.visibility = GONE
            mOnRecordButtonClick.onPauseClick()
            root?.viewRedCirclePause?.visibility = INVISIBLE
        }

        root?.edit_image?.setOnClickListener {
            val intent = Intent()
            intent.type = "video/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO)

        }
    //    setAnimateViewAtPosition1()
        return root
    }

    @SuppressLint("SetTextI18n") fun videoTimer(timer: Int) {
        Log.e("Tag Data", "" + time_text.text)

        if(timer==10)
        {
            Log.e("Tag Data 34343 ::", "" + time_text.text)
            time_text.text = timer.toString() + "s"
            fixTimeText.text = timer.toString() + "s"
        }
        else{
            time_text.text = timer.toString() + "s"
            fixTimeText.text = timer.toString() + "s"
        }

      /*
        //Log.e("Tag Data",""+time_text.text)
      //
       // Log.e("Tag fixTimeText",""+fixTimeText.text)*/
    }

    fun visibleAnimation(view: View) {

        view.animate().alpha(10F).setDuration(500).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = VISIBLE
            }
        })

    }

    fun onTimerOnRecordGone(flag: Boolean)
    {
        if(flag)
        {
            root?.record_button?.visibility = INVISIBLE
            root?.pause_button?.visibility = INVISIBLE
        }
        else{
            root?.record_button?.visibility = INVISIBLE
            root?.pause_button?.visibility = VISIBLE
        }

    }

    fun inVisibleAnimation(view: View) {

        view.animate().alpha(0F).setDuration(0).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.visibility = INVISIBLE
            }
        })

    }

    fun clearFilter() {
        root?.record_button?.visibility = VISIBLE
        root?.filterConstraintLayout?.visibility = View.INVISIBLE
        root?.cancel_action!!.visibility = INVISIBLE
        mOnRecordButtonClick.onFilterCancleClick()
        root?.bottom_filter!!.visibility = INVISIBLE
        root?.cancel_action!!.visibility = INVISIBLE
        setAnimateViewAtPosition(
            root?.cancel_action!!, convertDpToPixel(-30f, requireContext()).toInt()) // 135
        root?.edit_image?.visibility = VISIBLE
    }

    fun invisibleAtPause1() {
        root?.pause_button?.visibility = INVISIBLE
        root?.record_button?.visibility = VISIBLE
        root?.circle?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        root?.imgPuase?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        root?.time?.visibility = VISIBLE
        root?.fixTime?.visibility = GONE
        root?.filterTouchesWhenObscured
        mOnRecordButtonClick.onPauseClick()

        root?.filterConstraintLayout?.visibility = View.INVISIBLE
        root?.bottom_filter!!.visibility = INVISIBLE
        root?.cancel_action!!.visibility = INVISIBLE
        setAnimateViewAtPosition(
            root?.cancel_action!!, convertDpToPixel(-30f, requireContext()).toInt()) // 135
    }

    fun clearClicked() {
        root?.circle?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        root?.imgPuase?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
        root?.pause_button?.visibility = INVISIBLE
        root?.record_button?.visibility = VISIBLE
        root?.cancel_action!!.visibility = INVISIBLE
        root?.edit_image?.visibility = VISIBLE

        root?.bottom_filter!!.visibility = INVISIBLE
        root?.cancel_action!!.visibility = INVISIBLE
        setAnimateViewAtPosition(root?.cancel_action!!, convertDpToPixel(-30f, requireContext()).toInt()) // 135

        root?.filterConstraintLayout?.visibility = View.INVISIBLE
        root?.filterConstraintLayout!!.backgroundTintList = ColorStateList.valueOf(
            Color.parseColor(
                "#FFFFFF"))
        root?.recycler_view?.isLayoutFrozen = false
        filterRecyclerAdapter!!.setCurrentPosition(lastPosition)
        root?.recycler_view?.smoothScrollToPosition(0)
        recordVideo = true
    }

    private fun animateView(view: ImageView) {
        val scaleAnimation = ScaleAnimation(
            1.0f, 0.5f, 1.0f, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        prepareAnimation(scaleAnimation)
        val animation = AnimationSet(true)
        animation.addAnimation(scaleAnimation)
        animation.duration = 200
        animation.fillAfter = true
        view.startAnimation(animation)
    }

    private fun prepareAnimation(animation: Animation): Animation? {
        animation.repeatCount = 1
        animation.repeatMode = Animation.REVERSE
        return animation
    }

    fun setOnSwipeFilter(position: Int) {
        //  filterRecyclerAdapter?.setCurrentPosition(position)
    }

    fun onSwipeGesture(swipeCount: Int) {
        Log.e("Tag ", "S")
        root?.recycler_view?.smoothScrollToPosition(swipeCount)

    }

    fun setOnFilterClick() {

        slide = if (slide) {

            root?.record_button?.visibility = INVISIBLE
            root?.filterConstraintLayout!!.visibility = VISIBLE
            root?.bottom_filter!!.visibility = VISIBLE
           // root?.transformationLayout!!.startTransform()
            //  val marginLeftSpeedScreenUnits = (screenWidth / 7) + 7

            //  Log.e("margin ::","$marginLeftSpeedScreenUnits ${screenWidth}" )

            //visibleAnimation(root?.bottom_filter!!)

            //  animateSelectedView(root?.bottom_filter!!,marginLeftSpeedScreenUnits,500)

            /* TransitionManager.beginDelayedTransition(root?.main_layout!!)

             val params: ViewGroup.LayoutParams =  root?.bottom_filter!!.layoutParams
                 params.width = screenWidth
                 params.height = root?.bottom_filter!!.height
             root?.bottom_filter!!.layoutParams = params*/

            // startAnimation()
            //setAnimateViewAtPosition1(root?.bottom_filter!!, 720) //15
            //  setAnimateViewAtPosition(root?.time!!, convertDpToPixel(11f, requireContext()).toInt()) // 27
            //   setAnimateViewAtPosition(root?.fixTime!!, convertDpToPixel(11f, requireContext()).toInt()) //27
            //   setAnimateViewAtPosition(root?.timer_round!!, convertDpToPixel(9f, requireContext()).toInt()) //22
            //   setAnimateViewAtPosition(root?.time_slider!!, convertDpToPixel(11f, requireContext()).toInt()) // 27
            //   setAnimateViewAtPosition(root?.pause_button!!, -convertDpToPixel(4f, requireContext()).toInt()) // 15

            //root?.time?.isEnabled = false
            //root?.fixTime?.isEnabled = false

            mOnRecordButtonClick.onEditClick()
            root?.edit_image?.visibility = GONE
            true

        } else {
            root?.circle?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
            root?.imgPuase?.animate()!!.scaleX(1.1F).scaleY(1.1F).setDuration(150).start()
            root?.record_button?.visibility = VISIBLE
            root?.filterConstraintLayout?.visibility = INVISIBLE
            root?.bottom_filter!!.visibility = INVISIBLE
            mOnRecordButtonClick.onEditClick()
            //  root?.bottom_filter?.startAnimation(slideRight)
            root?.bottom_filter?.visibility = GONE
            root?.edit_image?.visibility = VISIBLE
            false
        }
    }

    private fun setAnimateViewAtPosition(view: View, margin: Int) {

        val params3 = view.layoutParams as ViewGroup.MarginLayoutParams
        val animator3 = ValueAnimator.ofInt(params3.bottomMargin, margin)

        animator3.addUpdateListener { valueAnimator ->
            params3.bottomMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }
        animator3.duration = 500
        animator3.start()

    }

    /* fun startAnimation()
     {
         val x: Int = root?.record_button!!.left
         val y: Int = root?.record_button!!.bottom-15
         val startRadius = 0
         val endRadius: Int = Math.max(root?.bottom_filter!!.width, root?.bottom_filter!!.height)
         val animator: Animator = ViewAnimationUtils.createCircularReveal(root?.bottom_filter!!,x,y,startRadius.toFloat(),endRadius.toFloat())
         animator.interpolator = AccelerateDecelerateInterpolator()
         animator.duration = 2000

         animator.start()
     }*/
    private fun setAnimateViewAtPosition1() {

      /*  root?.time?.clearAnimation()
        val anim = RotateAnimation(30F, 360F, ( root?.time!!.width / 2).toFloat(), ( root?.time!!.height / 2).toFloat())
        anim.fillAfter = true
        anim.repeatCount = 0
        anim.duration = 10000
        root?.time?.startAnimation(anim)*/

        val animation1 = AnimationUtils.loadAnimation(
            requireContext(), R.anim.time_animation)
        root?.time?.startAnimation(animation1)

    }

    private fun setFilterCategoryRecyclerView() {
        filterTypes = FilterType.createFilterList()
        val filterRecyclerAdapter = FilterCategoryBottomSliderAdapter(
            requireContext(), filterTypes)
        /* root?.filter_category_recyclerview?.adapter = filterRecyclerAdapter
         root?.filter_category_recyclerview?.layoutManager = LinearLayoutManager(
             requireContext(),
             RecyclerView.HORIZONTAL,
             false
         )*/
    }


    private fun setFilterRecyclerView() {

        filterTypes = FilterType.createFilterList()

        for (i in filterTypes!!.indices) {
            fliterModelList!!.add(FliterModel(true))
        }

        filterRecyclerAdapter = FilterRecyclerAdapter(requireContext(), filterTypes, fliterModelList)

        filterRecyclerAdapter?.setOnClick(this)

        root?.recycler_view?.adapter = filterRecyclerAdapter

        val padding: Int = Utils.getScreenWidth(requireContext()) / 2 - Utils.dpToPx(
            requireContext(), 35f)

        Log.e("Tag padding", "$padding")

        root?.recycler_view?.setPadding(padding, 0, padding, 0)


        sliderLayoutManager = SliderLayoutManager(requireContext()).apply {
            callback = object : SliderLayoutManager.OnItemSelectedListener {
                override fun onItemSelected(layoutPosition: Int) {
                    mOnRecordButtonClick.onItemFilterClick(layoutPosition)
                    if (layoutPosition > 0) {
                        root?.cancel_action!!.visibility = VISIBLE
                        setAnimateViewAtPosition(root?.cancel_action!!, convertDpToPixel(20f, requireContext()).toInt()) // 135
                    } else {
                        root?.cancel_action!!.visibility = INVISIBLE
                        setAnimateViewAtPosition(
                            root?.cancel_action!!,
                            convertDpToPixel(-30f, requireContext()).toInt()) // 135
                    }
                    //    tvSelectedItem.setText(data[layoutPosition])
                    Log.e("Last Postion ", "$lastPosition :: $layoutPosition")
                    filterRecyclerAdapter!!.setClickPosition(layoutPosition, true)
                    lastPosition = layoutPosition
                }
            }
        }
        sliderLayoutManager.setOrientationView(RecyclerView.HORIZONTAL)
        root?.recycler_view?.layoutManager = sliderLayoutManager
        root?.recycler_view?.suppressLayout(false)
        root?.recycler_view?.isNestedScrollingEnabled = false
        root?.recycler_view?.smoothScrollToPosition(0)

        /*  root?.image?.setOnClickListener {

              Log.e("Tag Click","image Click")
                  TransitionManager.beginDelayedTransition(root?.main_layout!!)
                  val params: ViewGroup.LayoutParams = image.layoutParams
               if (clickFlag) {
                   savedWidth=image.width
                   savedHight=image.height
                   params.width = image.width-25
                   params.height = image.height-15
                   clickFlag=false
                   root?.image?.borderColor = Color.parseColor("#FF0000")

               } else {
                   params.height=image.height+15
                   params.width = image.width+25
                   root?.image?.borderColor = Color.parseColor("#00000000")
                   clickFlag=true
               }
               image.layoutParams = params
          }*/
        root?.recycler_view?.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                try {
                    root?.filterConstraintLayout!!.backgroundTintList = ColorStateList.valueOf(
                        Color.parseColor(
                            "#FFFFFF"))
                    filterRecyclerAdapter!!.notifyItemChanged(lastPosition)
                } catch (e: Exception) {
                }

            }
        })

    }


    fun animateSelectedView(view: View, marginLeft: Int, duration: Long) {

        val params = view.layoutParams as ViewGroup.MarginLayoutParams

        val animator = ValueAnimator.ofInt(params.leftMargin, marginLeft)

        animator.addUpdateListener { valueAnimator ->
            params.leftMargin = (valueAnimator.animatedValue as Int)
            view.requestLayout()
        }

        animator.duration = duration
        animator.start()

    }

    override fun onClick(position: Int, view: View) {
        mOnRecordButtonClick.onItemFilterClick(position)
        //root?.recycler_view?.smoothScrollToPosition(root?.recycler_view!!.getChildLayoutPosition(view))
    }

    override fun onStartRecordView() {

        root?.recycler_view?.isLayoutFrozen = true
        root?.filterConstraintLayout!!.backgroundTintList = ColorStateList.valueOf(
            Color.parseColor(
                "#FF005F"))
        onRecordingStart()
    }

    override fun onPauseRecordView() {
        root?.recycler_view?.isLayoutFrozen = false
        root?.filterConstraintLayout!!.backgroundTintList = ColorStateList.valueOf(
            Color.parseColor(
                "#FFFFFF"))
        onRecordingPause()
    }

    fun onRecordingStart() {
        if (root?.time_slider?.visibility == VISIBLE) {
            Toast.makeText(
                requireContext(), "Please set pause than after start video recording.", Toast.LENGTH_SHORT).show()
        } else {

            if (recordVideo) {
                recordVideo = false
                root?.time?.visibility = GONE
                root?.fixTime?.visibility = VISIBLE
                mOnRecordButtonClick.onRecordClick()
                root?.viewRedCirclePause?.visibility = VISIBLE
            } else {
                root?.time?.visibility = GONE
                root?.fixTime?.visibility = VISIBLE
                root?.viewRedCirclePause?.visibility = VISIBLE
                mOnRecordButtonClick.onResumeClick()
            }
        }
    }

    fun onRecordingPause() {
        root?.time?.visibility = VISIBLE
        root?.fixTime?.visibility = GONE
        mOnRecordButtonClick.onPauseClick()
        root?.viewRedCirclePause?.visibility = INVISIBLE
    }

    @SuppressLint("ClickableViewAccessibility") private fun onTouchListener(): OnTouchListener? {
        return OnTouchListener { view, event ->

            val y = event.rawY.toInt()
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_DOWN -> {
                    startClickTime = Calendar.getInstance().timeInMillis

                    val lParams = view.layoutParams as RelativeLayout.LayoutParams
                    yDelta = y - lParams.topMargin

                    /* if (view.id == R.id.pauseText) {

                        val expandCenter = AnimationUtils.loadAnimation(requireContext(), R.anim.expand_center)
                        root?.timer_text2?.startAnimation(expandCenter)
                        root?.timer_text2?.visibility = VISIBLE
                        view.animate().scaleY(1.6f).duration = 500
                        pauseClick = true
                    }*/
                    if (view.id == R.id.pauseText1) {

                        val expandCenter = AnimationUtils.loadAnimation(
                            requireContext(), R.anim.expand_center)
                        root?.timer_text1?.startAnimation(expandCenter)
                        root?.timer_text1?.visibility = VISIBLE

                        pauseText1_bg.animate().scaleY(1.6f).duration = 500
                        pause1Click = true
                    }
                }

                MotionEvent.ACTION_CANCEL -> {
                    /*if (view.id == R.id.pauseText) {

                        val collapseCenter = AnimationUtils.loadAnimation(requireContext(), R.anim.collapse_center)
                        root?.timer_text2?.startAnimation(collapseCenter)
                        root?.timer_text2?.visibility = GONE
                        view.animate().scaleY(1.0f).duration = 500
                    }*/
                    if (view.id == R.id.pauseText1) {

                        val collapseCenter = AnimationUtils.loadAnimation(
                            requireContext(), R.anim.collapse_center)
                        root?.timer_text1?.startAnimation(collapseCenter)
                        root?.timer_text1?.visibility = INVISIBLE

                        pauseText1_bg.animate().scaleY(1.0f).duration = 500
                    }
                }

                MotionEvent.ACTION_UP -> {

                    /* if (view.id == R.id.pauseText) {

                        val collapseCenter = AnimationUtils.loadAnimation(requireContext(), R.anim.collapse_center)
                        root?.timer_text2?.startAnimation(collapseCenter)
                        root?.timer_text2?.visibility = GONE
                        view.animate().scaleY(1.0f).duration = 500
                    }*/

                    if (view.id == R.id.pauseText1) {

                        val collapseCenter = AnimationUtils.loadAnimation(
                            requireContext(), R.anim.collapse_center)
                        root?.timer_text1?.startAnimation(collapseCenter)
                        root?.timer_text1?.visibility = INVISIBLE

                        pauseText1_bg.animate().scaleY(1.0f).duration = 500
                    }

                }

                MotionEvent.ACTION_MOVE -> {

                    /*if (view.id == R.id.pauseText) {
                        val perSectionPx = timer_image.height / 15
                        Log.d("FragmentNormal", "Position X : ${timer_image.y}, ${timer_image.height}, Y : ${y - yDelta}")
                        Log.d("FragmentNormal", "Position X 11111: $perSectionPx, ${perSectionPx * 3}, ${perSectionPx * 12}")
                        //Log.d("FragmentNormal", "Position X111 : ${(y - yDelta) <= (timer_image.y - (perSectionPx * 12) + timer_image.height - 20)}")
                        //Log.d("FragmentNormal", "Position X1111 : ${(y - yDelta) >= (timer_image.y - (perSectionPx * 3))}")
                        //if ((y - yDelta) >= 130 && (y - yDelta) <= 565) {
                        if ((y - yDelta) >= (timer_image.y + perSectionPx * 3) && (y - yDelta) <= (timer_image.y + timer_image.height - 20)) {

                            val layoutParams2: RelativeLayout.LayoutParams? =
                                view.layoutParams as RelativeLayout.LayoutParams
                            timerText2 = (y - yDelta) - timer_image.y.toInt()
                            Log.d("FragmentNormal", "Timer : ${timerText2}")
                            layoutParams2!!.topMargin = y - yDelta
                            layoutParams2.rightMargin = 22
                            layoutParams2.bottomMargin = 0
                            view.layoutParams = layoutParams2

                            val layoutParams1 = root?.pause_view_line?.layoutParams as RelativeLayout.LayoutParams
                            layoutParams1.topMargin = y - yDelta + (layoutParams2.height / 2)
                            layoutParams1.rightMargin = 0
                            layoutParams1.bottomMargin = 0
                            root?.pause_view_line?.layoutParams = layoutParams1


                            val timerText2Params = root?.timer_text2?.layoutParams as RelativeLayout.LayoutParams
                            timerText2Params.topMargin = y - yDelta - 7
                            timerText2Params.rightMargin = -4
                            timerText2Params.bottomMargin = 0
                            root?.timer_text2?.layoutParams = timerText2Params

                            val selectedTime2Params = root?.selected_time2?.layoutParams as RelativeLayout.LayoutParams
                            selectedTime2Params.topMargin = y - yDelta
                            selectedTime2Params.rightMargin = 0
                            selectedTime2Params.bottomMargin = 0
                            root?.selected_time2?.layoutParams = selectedTime2Params

                            val perSectionPx = timer_image.height / 15
                            textDuration2 = (15-(timerText2.toFloat()/perSectionPx)).toInt() - 1

                            val timerPause2 = (15-(timerText2.toFloat()/perSectionPx)) - 1

                            if (timerPause2 in 3F..12F){
                                root?.selected_time2?.visibility = VISIBLE
                            }else{
                                root?.selected_time2?.visibility = GONE
                            }

                            if (timerPause2 >=10F){
                                root?.timer_text2?.text = timerPause2.absoluteValue.toString().substring(0,4)
                                root?.selected_time2?.text = timerPause2.absoluteValue.toString().substring(0,4)
                            }else{
                                root?.timer_text2?.text = timerPause2.absoluteValue.toString().substring(0,3)
                                root?.selected_time2?.text = timerPause2.absoluteValue.toString().substring(0,3)
                            }

                            //root?.timer_text2?.text = timerText2.toString()
                        }

                    } else */if (view.id == R.id.pauseText1) {
                        //if ((y - yDelta) >= 130 && (y - yDelta) <= 565) {
                        val perSectionPx = timer_image.height / 15
                        var timerPause1 = (15 - (timerText1.toFloat() / perSectionPx)) - 1
                        Log.d("Music", "Timer Pause 1 : $perSectionPx")

                        if ((timerPause1 >= 3F && timerPause1 <= 12F) && (y - yDelta) >= timer_image.y && (y - yDelta) <= (timer_image.y + timer_image.height - 20)) {
                            timerText1 = (y - yDelta) - timer_image.y.toInt()
                            Log.d("FragmentNormal", "Timer : ${timerText1}")
                            val layoutParams2: RelativeLayout.LayoutParams? = view.layoutParams as RelativeLayout.LayoutParams

                            layoutParams2!!.topMargin = y - yDelta
                            layoutParams2.rightMargin = 22
                            layoutParams2.bottomMargin = 0
                            view.layoutParams = layoutParams2

                            val layoutParams1 = root?.pause_view_line1?.layoutParams as RelativeLayout.LayoutParams
                            layoutParams1.topMargin = y - yDelta + (layoutParams2.height / 2)
                            layoutParams1.rightMargin = 0
                            layoutParams1.bottomMargin = 0
                            root?.pause_view_line1?.layoutParams = layoutParams1

                            val layoutParams3 = root?.pauseText1_bg?.layoutParams as RelativeLayout.LayoutParams
                            layoutParams3.topMargin = y - yDelta
                            layoutParams3.rightMargin = 0
                            layoutParams3.bottomMargin = 0
                            root?.pauseText1_bg?.layoutParams = layoutParams3

                            val timerText1Params = root?.timer_text1?.layoutParams as RelativeLayout.LayoutParams
                            timerText1Params.topMargin = y - yDelta - 4
                            timerText1Params.rightMargin = 10
                            timerText1Params.bottomMargin = 0
                            root?.timer_text1?.layoutParams = timerText1Params

                            val selectedTime1Params = root?.selected_time1?.layoutParams as RelativeLayout.LayoutParams
                            selectedTime1Params.topMargin = y - yDelta + 3
                            selectedTime1Params.rightMargin = 18
                            selectedTime1Params.bottomMargin = 0
                            root?.selected_time1?.layoutParams = selectedTime1Params

                            textDuration1 = (15 - (timerText1.toFloat() / perSectionPx)).toInt() - 1

                            if (timerPause1 in 3F..12F) {
                                root?.selected_time1?.visibility = VISIBLE
                            } else {
                                root?.selected_time1?.visibility = GONE
                            }

                            if (timerPause1 >= 10F) {
                                root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                    0, 4) + "s"
                                root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                    0, 4) + "s"
                            } else {
                                root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                    0, 3) + "s"
                                root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                    0, 3) + "s"
                            }
                            //root?.timer_text1?.text = timerText1.toString()
                        } else {
                            if (timerPause1 < 3f) {
                                val layoutParams2: RelativeLayout.LayoutParams? = view.layoutParams as RelativeLayout.LayoutParams

                                layoutParams2!!.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    31f, requireContext())).toInt()
                                layoutParams2.rightMargin = 22
                                layoutParams2.bottomMargin = 0
                                view.layoutParams = layoutParams2

                                val layoutParams1 = root?.pause_view_line1?.layoutParams as RelativeLayout.LayoutParams
                                layoutParams1.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    25f, requireContext())).toInt()
                                layoutParams1.rightMargin = 0
                                layoutParams1.bottomMargin = 0
                                root?.pause_view_line1?.layoutParams = layoutParams1

                                val layoutParams3 = root?.pauseText1_bg?.layoutParams as RelativeLayout.LayoutParams
                                layoutParams3.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    31f, requireContext())).toInt()
                                layoutParams3.rightMargin = 0
                                layoutParams3.bottomMargin = 0
                                root?.pauseText1_bg?.layoutParams = layoutParams3

                                val timerText1Params = root?.timer_text1?.layoutParams as RelativeLayout.LayoutParams
                                timerText1Params.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    33f, requireContext())).toInt()
                                timerText1Params.rightMargin = 10
                                timerText1Params.bottomMargin = 0
                                root?.timer_text1?.layoutParams = timerText1Params

                                val selectedTime1Params = root?.selected_time1?.layoutParams as RelativeLayout.LayoutParams
                                selectedTime1Params.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    30f, requireContext())).toInt()
                                selectedTime1Params.rightMargin = 18
                                selectedTime1Params.bottomMargin = 0
                                root?.selected_time1?.layoutParams = selectedTime1Params

                                textDuration1 = (15 - (timerText1.toFloat() / perSectionPx)).toInt() - 1
                                timerPause1 = 3f
                                if (timerPause1 in 3F..12F) {
                                    root?.selected_time1?.visibility = VISIBLE
                                } else {
                                    root?.selected_time1?.visibility = GONE
                                }

                                if (timerPause1 >= 10F) {
                                    root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 4) + "s"
                                    root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 4) + "s"
                                } else {
                                    root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 3) + "s"
                                    root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 3) + "s"
                                }
                                timerText1 = (y - yDelta) - timer_image.y.toInt()
                            } else if (timerPause1 > 12f) {

                                val layoutParams2: RelativeLayout.LayoutParams? = view.layoutParams as RelativeLayout.LayoutParams


                                layoutParams2!!.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    97f, requireContext())).toInt()
                                layoutParams2.rightMargin = 22
                                layoutParams2.bottomMargin = 0
                                view.layoutParams = layoutParams2

                                val layoutParams1 = root?.pause_view_line1?.layoutParams as RelativeLayout.LayoutParams
                                layoutParams1.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    91f, requireContext())).toInt()
                                layoutParams1.rightMargin = 0
                                layoutParams1.bottomMargin = 0
                                root?.pause_view_line1?.layoutParams = layoutParams1

                                val layoutParams3 = root?.pauseText1_bg?.layoutParams as RelativeLayout.LayoutParams
                                layoutParams3.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    97f, requireContext())).toInt()
                                layoutParams3.rightMargin = 0
                                layoutParams3.bottomMargin = 0
                                root?.pauseText1_bg?.layoutParams = layoutParams3

                                val timerText1Params = root?.timer_text1?.layoutParams as RelativeLayout.LayoutParams
                                timerText1Params.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    99f, requireContext())).toInt()
                                timerText1Params.rightMargin = 10
                                timerText1Params.bottomMargin = 0
                                root?.timer_text1?.layoutParams = timerText1Params

                                val selectedTime1Params = root?.selected_time1?.layoutParams as RelativeLayout.LayoutParams
                                selectedTime1Params.topMargin = (timer_image.y + timer_image.height - convertDpToPixel(
                                    97f, requireContext())).toInt()
                                selectedTime1Params.rightMargin = 18
                                selectedTime1Params.bottomMargin = 0
                                root?.selected_time1?.layoutParams = selectedTime1Params

                                textDuration1 = (15 - (timerText1.toFloat() / perSectionPx)).toInt() - 1
                                timerPause1 = 12f
                                if (timerPause1 in 3F..12F) {
                                    root?.selected_time1?.visibility = VISIBLE
                                } else {
                                    root?.selected_time1?.visibility = GONE
                                }

                                if (timerPause1 >= 10F) {
                                    root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 4) + "s"
                                    root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 4) + "s"
                                } else {
                                    root?.timer_text1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 3) + "s"
                                    root?.selected_time1?.text = timerPause1.absoluteValue.toString().substring(
                                        0, 3) + "s"
                                }
                                timerText1 = (y - yDelta) - timer_image.y.toInt()
                            }
                        }
                    }
                }
            }
            root?.main_layout?.invalidate()
            true
        }
    }

    interface OnRecordButtonClick {
        fun onRecordClick()
        fun onPauseClick()
        fun onResumeClick()
        fun onEditClick()
        fun onFilterCancleClick()
        fun onItemFilterClick(position: Int)
        fun onDisableClick()
        fun onFilterMenuClick()
        fun onPause1TimerClose(pauseTimer: Float)
        fun onPause2TimerClose(pauseTimer: Float)
        fun onTimerSliderUp()
        fun onTimerSliderDown()
    }

    /*fun getPath(uri: Uri): String? {
        var cursor: Cursor? = null
        return try {
            val projection =
                arrayOf(MediaStore.Images.Media.DATA)
            cursor = requireContext().getContentResolver().query(uri, projection, null, null, null)
            val column_index =
                cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor?.moveToFirst()
            cursor?.getString(column_index!!)
        } finally {
            cursor?.close()
        }
    }*/

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode === RESULT_OK) {
            if (requestCode === REQUEST_TAKE_GALLERY_VIDEO) {
                val selectedVideoUri = data!!.data
                Log.d("tag", "selectedVideoUri:::$selectedVideoUri")

                val selectedVideoPath = getPath(requireContext(), selectedVideoUri!!)

                Log.d("tag", "selectedVideoPath:::$selectedVideoPath")
                if (isVideoOrStatus) {
                    if (selectedVideoPath != null) {
                        val intent = Intent(requireActivity(), VideoPreviewActivity::class.java)
                        intent.putExtra("path", selectedVideoPath)
                        startActivity(intent)
                    }
                } else {
                    if (selectedVideoPath != null) {
                        val intent = Intent(requireActivity(), VideoStatusPreviewActivity::class.java)
                        intent.putExtra("videoFile", selectedVideoPath)
                        startActivity(intent)
                    }
                }

            }
        }
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }


    fun getPath(context: Context, uri: Uri): String? {
        val isKitKat: Boolean = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id: String = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                if ("image" == type) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if ("video" == type) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if ("audio" == type) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1])
                return getDataColumn(context, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {
            return getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column)
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val column_index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(column_index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }


}