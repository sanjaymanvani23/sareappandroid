package com.example.sare.videoRecording.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.sare.videoRecording.fragment.FragmentBeauty
import com.example.sare.videoRecording.fragment.FragmentNormal

class BottomSliderViewPagerAdapter  (
    fm: FragmentManager,
    private var fragmentNormal: FragmentNormal,
    private var tabCount: Int
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {


    override fun getItem(position: Int): Fragment {

        return when (position) {

            0 -> {
                fragmentNormal
            }

            else ->  fragmentNormal
        }
    }

    override fun getCount(): Int {
        return tabCount
    }

}