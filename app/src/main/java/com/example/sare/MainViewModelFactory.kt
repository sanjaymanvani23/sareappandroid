package com.d.parastask

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.d.parastask.api.ApiHelper
import com.example.sare.MainRepository
import com.example.sare.MyApplication

class MainViewModelFactory(private val apiHelper: ApiHelper): ViewModelProvider.Factory {
//    private val context = getApplication<Application>().applicationContext

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }
}