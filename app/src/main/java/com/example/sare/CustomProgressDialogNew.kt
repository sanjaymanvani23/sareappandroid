package com.example.sare

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ProgressBar
import android.widget.TextView

import kotlinx.android.synthetic.main.custom_progress_view.*
import kotlin.math.roundToInt

class CustomProgressDialogNew(context: Context?) :
    AlertDialog(context) {

    var textView: TextView? = null
    var progressCircular: ProgressBar? = null

    override fun show() {
        super.show()
        setContentView(R.layout.custom_progress_view)
        textView = cp_title
        progressCircular = cp_pbar
        textView?.text = "Please Wait..."
    }

    override fun hide() {
        super.hide()
        dismiss()
//        cancel()
    }

    fun setProgress(pos: Int) {
        progressCircular?.setProgress(pos)
        textView?.text = "$pos%"
    }
    init {
        setCancelable(false)
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}