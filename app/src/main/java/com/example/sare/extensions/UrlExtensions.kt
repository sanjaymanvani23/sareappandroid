package com.example.sare.extensions

import android.content.Context
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.example.sare.AmazonUtil
import com.example.sare.MyApplication
import com.example.sare.R
import androidx.navigation.fragment.findNavController
import com.example.sare.ui.login.LoginActivity
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


fun getVideoUrl(url: String): String {
    var mediaUrl1 = AmazonUtil.getSignedUrl(url)
    var mediaUrl = ""
    if (mediaUrl1 != null) {
        Log.e("Sare App Video Inside ", "" + mediaUrl1)
        mediaUrl = mediaUrl1.replace(
            "https://s3.ap-south-1.amazonaws.com/vids.sarevids.com/sarevidsvideos/",
            "https://d20p3vlgg4r8y8.cloudfront.net/"
        )
    }
    return mediaUrl

}

@RequiresApi(Build.VERSION_CODES.M) fun Fragment.callLoginFragment(context: Context,errorMsg:String)
{
    context?.showToast(errorMsg)
//    MyApplication.logoutClearPreference(context)
    MyApplication.mainActivity.logoutUser()
    activity?.openNewActivity(LoginActivity::class.java)
}
fun createVideoFile(context: Context): File {
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())

   /* val root = File(
        Environment.getExternalStorageDirectory().toString() + File.separator + "MusicFolder", "Video")
    */
    val imageFileName: String = "SaraApp"+ timeStamp + "_"
    val storageDir: File = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)!!
    if (!storageDir.exists()) storageDir.mkdirs()
    return File.createTempFile(imageFileName, ".mp4", storageDir)
}
fun createNewAudioFile(context: Context): File {
    val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(
        Date()
    )

     val root = File(Environment.getExternalStorageDirectory().toString() + File.separator + "MusicFolder", "Audio")

    val imageFileName: String = "SaraApp"+ timeStamp + "_"
    val storageDir: File = context.getExternalFilesDir(Environment.DIRECTORY_MOVIES)!!
    if (!root.exists()) root.mkdirs()
    return File.createTempFile(imageFileName, ".mp3", root)
}

fun createNewFileMp3():String {
    val formatter = SimpleDateFormat("yyyyMMdd_HHmmss")
    val now = Date()
    val fileName: String = formatter.format(now).toString() + ".mp3"
    try {
        val root = File(Environment.getExternalStorageDirectory().toString() + File.separator + "MusicFolder", "AudioFiles")

        if (!root.exists()) {
            root.mkdirs()
        }
        val mp3File = File(root, fileName)
       return mp3File.absolutePath
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return null!!
}

fun getAudioFileDir(): File {
  return File(Environment.getExternalStorageDirectory().toString() + File.separator + "MusicFolder", "AudioFiles")
}
fun secToTime(totalSeconds: Long): String {
    return String.format(
        "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(totalSeconds),
        TimeUnit.MILLISECONDS.toMinutes(totalSeconds) -
                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(totalSeconds)), // The change is in this line
        TimeUnit.MILLISECONDS.toSeconds(totalSeconds) -
                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(totalSeconds))
    )
}


