package com.example.sare.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.startActivityForResult
import com.example.sare.bottom_navigation.BottomBarActivity

fun <T> Context.openActivity(it: Class<T>) {
    val intent = Intent(this, it)
    startActivity(intent)
}
fun <T> Context.openActivity(it: Class<T>, bundle: Bundle) {
    val intent = Intent(this, it)
    intent.putExtras(bundle)
    startActivity(intent)
}

fun <T> Context.openBottomBarActivity() {
    val intent = Intent(this, BottomBarActivity::class.java)
    startActivity(intent)
}

fun <T> Activity.openNewActivity(it: Class<T>) {
    val intent = Intent(this, it)
    startActivity(intent)
    finish()
    finishAffinity()
}