package com.example.sare.extensions


import android.graphics.Color
import android.view.View
import android.view.View.*
import android.widget.Button
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.core.text.HtmlCompat
import com.example.sare.R


/**
 * Sets the View's visibility to [View.GONE]
 */
fun View.gone() {
    this.visibility = GONE
}

/**
 * Sets the View's visibility to [View.INVISIBLE]
 */
fun View.invisible() {
    this.visibility = INVISIBLE
}

/**
 * Sets the View's visibility to [View.VISIBLE]
 */
fun View.visible() {
    this.visibility = VISIBLE
}
fun TextView.normalText() {
    this.setTextColor(Color.parseColor("#66FFFFFF"))
    this.setBackgroundColor(Color.parseColor("#00000000"))
}
fun TextView.highLightText() {
    this.setTextColor(Color.parseColor("#FFFFFF"))
    this.setBackgroundResource(R.drawable.add_audio_tab_selected_bg)
}
fun Button.setButtonBgColor(bgColor:String) {
    this.setTextColor(Color.parseColor(bgColor))
}