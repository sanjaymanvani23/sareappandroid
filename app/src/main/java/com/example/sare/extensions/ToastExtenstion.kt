package com.example.sare.extensions


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.setPadding
import com.example.sare.R

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun Activity.showToast(message: String) {
    this.runOnUiThread {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }
}

fun customToast(text: String, activity: Activity, type: Int) {
    activity.runOnUiThread {
    val inflater: LayoutInflater = activity.layoutInflater
    val layout: View = inflater.inflate(
        R.layout.toast_custom,
        activity.findViewById(R.id.toast_root_layout) /*as ViewGroup?*/
    )

    val message = layout?.findViewById<TextView>(R.id.toast_message)

    message?.text = text

    val constraintLayout = layout?.findViewById<LinearLayout>(R.id.ll)
    if (type == 1) {
        constraintLayout?.backgroundTintList = activity.resources.getColorStateList(R.color.toast_green)
    } else {
        constraintLayout?.backgroundTintList = activity.resources.getColorStateList(R.color.toast_red)
    }

    val toast = Toast(activity)
    toast.setGravity(Gravity.TOP, 0, 0)
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout
    toast.show()

    /*
    val toast: Toast = Toast.makeText(
        activity,
        message,
        Toast.LENGTH_SHORT
    )
    toast.setGravity(Gravity.TOP, 0, 20)
    val toastView = toast.view
    if (type == 1) {
        toastView?.backgroundTintList = activity.resources.getColorStateList(R.color.light_green)
    } else {
        toastView?.backgroundTintList = activity.resources.getColorStateList(R.color.toast_red)
    }
    val toastMessage: TextView = toastView!!.findViewById<View>(android.R.id.message) as TextView
    toastMessage.textSize = 16F
    toastMessage.setTextColor(Color.WHITE)
    toastMessage.gravity = Gravity.TOP
    toastMessage.setPadding(16)
    toast.show()*/
    }
}

fun Activity.customToastMain(text: String, type: Int) {
    val inflater: LayoutInflater = layoutInflater
    val layout: View = inflater.inflate(
        R.layout.toast_custom,
        findViewById(R.id.toast_root_layout) /*as ViewGroup?*/
    )

    val message = layout?.findViewById<TextView>(R.id.toast_message)

    message?.text = text

    val constraintLayout = layout?.findViewById<LinearLayout>(R.id.ll)
    if (type == 1) {
        constraintLayout?.backgroundTintList = resources.getColorStateList(R.color.toast_green)
    } else {
        constraintLayout?.backgroundTintList = resources.getColorStateList(R.color.toast_red)
    }

    val toast = Toast(applicationContext)
    toast.setGravity(Gravity.TOP, 0, 0)
    toast.duration = Toast.LENGTH_SHORT
    toast.view = layout
    toast.show()

}