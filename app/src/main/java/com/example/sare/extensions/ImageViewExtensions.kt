package com.example.sare.extensions

import android.graphics.Color
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.Utils
import kotlinx.android.synthetic.main.fragment_video.view.*


fun ImageView.loadUrl(url: String) {
    Glide.with(context)
        .asBitmap()
        .load(url)
        .dontAnimate()
        .placeholder(R.drawable.ic_shape_round)
        .error(R.drawable.ic_shape_round)/*.placeholder(R.drawable.profile_default)
        .error(R.drawable.profile_default)*/
        .into(this)
}


fun ImageView.setBorder(colorcode: String) {
    if(colorcode.isNotEmpty())
    {
        try {
            if(colorcode.contains("#"))
                this.setColorFilter(Color.parseColor(colorcode), android.graphics.PorterDuff.Mode.SRC_IN)
            else
                this.setColorFilter(Color.parseColor("#$colorcode"), android.graphics.PorterDuff.Mode.SRC_IN)
        }
        catch (e:Exception)
        {

        }

    }

}
