package com.example.sare.network

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}