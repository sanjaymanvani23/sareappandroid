package com.example.sare.auth.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.SplashVersionQuery
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class SplashViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    val apolloClient = ApolloClient.setupApollo("")
    var version = MutableLiveData<NetworkResponse>()

    fun versionCheck(userId: String) {
        apolloClient.query(SplashVersionQuery(userId.toString()))
            .enqueue(object : ApolloCall.Callback<SplashVersionQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error:::" + e.message)
                    version.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<SplashVersionQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.version() != null) {
                                version.postValue(
                                    NetworkResponse.SUCCESS.versionCheck(
                                        response.data()!!
                                    )
                                )
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                version.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            version.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }
}