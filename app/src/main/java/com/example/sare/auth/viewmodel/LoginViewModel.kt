package com.example.sare.auth.viewmodel

import android.annotation.SuppressLint
import android.app.Application
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var loginGuest = MutableLiveData<NetworkResponse>()
    var country = MutableLiveData<NetworkResponse>()
    var login = MutableLiveData<NetworkResponse>()
    var userDetails = MutableLiveData<NetworkResponse>()
    var sso = MutableLiveData<NetworkResponse>()
    var sso1 = MutableLiveData<NetworkResponse>()
    val uuid = Settings.Secure.getString(context?.contentResolver,Settings.Secure.ANDROID_ID)
    val apolloClient = ApolloClient.setupApollo("")

    fun doGuestLogin() {
        apolloClient.mutate(GuestLoginMutation(uuid,"Android")).enqueue(object : ApolloCall.Callback<GuestLoginMutation.Data>() {

                override fun onFailure(e: ApolloException) {
                    loginGuest.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<GuestLoginMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.guestLogin() != null) {
                                loginGuest.postValue(NetworkResponse.SUCCESS.getGuestLogin(response.data!!))
                            } else {

                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                loginGuest.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))

                            }
                        } catch (e: Exception) {
                            loginGuest.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }

                }
            })
    }

    fun getCountry(timezone: String) {
        apolloClient.query(CountryQuery(timezone)).enqueue(object : ApolloCall.Callback<CountryQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    country.postValue(NetworkResponse.ERROR(e))
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(response: Response<CountryQuery.Data>) {
                    Log.d("tag", "ResponseData:::$response")
                    try {
                        if (response.data?.country() != null) {
                            country.postValue(NetworkResponse.SUCCESS.getCountry(response.data!!))
                        } else {

                            val errorCode =
                                response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                            country.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))                        }
                    } catch (e: Exception) {
                        country.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })

    }

    fun doLogin(mobileNo: String, countryId: String) {
        apolloClient.mutate(
            GenerateOtpMutation(
                mobileNo.toString(),
                countryId.toString(), "ANDROID"
            )
        )

            .enqueue(object : ApolloCall.Callback<GenerateOtpMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("Fragemnt Login", "Error Data : ${e.message}")
                    login.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<GenerateOtpMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.generateOtp() != null) {
                                login.postValue(
                                    NetworkResponse.SUCCESS.doLogin(
                                        response.data!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                login.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            login.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }

                }
            })

    }

    fun getUserDetails(id: String, token: String) {

        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(id))
            ?.enqueue(
                object : ApolloCall.Callback<UserDetailsQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.d("tag", "Error:: getUserDetails::" + e.message)
                        userDetails.postValue(NetworkResponse.ERROR(e))

                    }

                    override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                        Log.d("tag", "Response User Data::$response")
                        try {
                            if (response.data?.user() != null) {
                                userDetails.postValue(
                                    NetworkResponse.SUCCESS.getEditUserDetails(
                                        response.data!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                userDetails.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            userDetails.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                })
    }

    fun ssoLogin(
        mobile: String,
        id: String,
        email: String,
        type: String,
        name: String,
        deviceType: String,
        deviceToken: String,
        lat: String,
        longitude: String,
        city: String,
        state: String,
        country: String
    ) {

        Log.d("tag", "sso 1: $mobile")
        Log.d("tag", "sso 1: $id")
        Log.d("tag", "sso 1: $email")
        Log.d("tag", "sso 1: $type")
        Log.d("tag", "sso 1: $name")
        Log.d("tag", "sso 1: $uuid")
        apolloClient.mutate(
            SSOLoginMutation(
                mobile,
                id,
                email,
                type,
                uuid.toString(),
                name,
                deviceType,
                deviceToken,
                lat,
                longitude,
                city,
                state,
                country
            )
        )
            .enqueue(object : ApolloCall.Callback<SSOLoginMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    if (type == "G") {
                        sso1.postValue(NetworkResponse.ERROR(e))
                    } else {
                        sso.postValue(NetworkResponse.ERROR(e))
                    }

                }

                  override fun onResponse(response: Response<SSOLoginMutation.Data>) {
                    Log.d("tag", "sso Response Data:: $response")
                    try {
                        if (response.data() != null) {
                            if (type == "G") {
                                sso1.postValue(
                                    NetworkResponse.SUCCESS.ssoLogin(
                                        response.data()!!
                                    )
                                )
                            } else {
                                sso.postValue(
                                    NetworkResponse.SUCCESS.ssoLogin(
                                        response.data()!!
                                    )
                                )
                            }
                        } else {
                            val errorCode =
                                response.errors?.get(0)?.customAttributes?.get("status")
                            if (type == "G"){
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                sso1.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }else{
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                sso.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        }
                    } catch (e: Exception) {
                        if (type == "G") {
                            sso1.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        } else {
                            sso.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })

    }
}