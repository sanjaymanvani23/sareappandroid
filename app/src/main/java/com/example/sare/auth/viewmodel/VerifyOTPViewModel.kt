package com.example.sare.auth.viewmodel

import android.app.Activity
import android.app.Application
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.LoginMutation
import com.example.sare.UserDetailsQuery
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class VerifyOTPViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext
    var verifyOtp = MutableLiveData<NetworkResponse>()
    var userDetails = MutableLiveData<NetworkResponse>()

    val apolloClient = ApolloClient.setupApollo("")
    val uuid = Settings.Secure.getString(
        context?.contentResolver,
        Settings.Secure.ANDROID_ID
    )

    fun verifyOTP(
        simpleMobileNo: String,
        countryId: String,
        otp: String,
        activity: Activity,
        deviceType: String,
        deviceToken: String,
        latitude: String,
        longitude: String,
        city: String,
        state: String,
        country: String
    ) {
        Log.d("tag", "uuid:::::$uuid")
        apolloClient.mutate(
            LoginMutation(
                simpleMobileNo,
                countryId,
                otp,
                uuid.toString(),
                "ANDROID",
                deviceToken, latitude, longitude, city, state, country
            )
        )
            .enqueue(object : ApolloCall.Callback<LoginMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    verifyOtp.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<LoginMutation.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.login() != null) {
                                verifyOtp.postValue(
                                    NetworkResponse.SUCCESS.verifyOtp(
                                        response.data()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                Log.d("tag", "error_code:::$errorCode")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                verifyOtp.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            verifyOtp.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

    fun getUserDetails(id: String, token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserDetailsQuery(id))
            ?.enqueue(
                object : ApolloCall.Callback<UserDetailsQuery.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.d("tag", "Error::" + e.message)
                        userDetails.postValue(NetworkResponse.ERROR(e))

                    }

                    override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                        Log.d("tag", "Response User Data::$response")
                        try {
                            if (response.data()?.user() != null) {
                                userDetails.postValue(
                                    NetworkResponse.SUCCESS.getEditUserDetails(
                                        response.data()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(response.errors?.get(0)?.message.toString(), errorCode.toString())
                                userDetails.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            userDetails.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                })
    }
}