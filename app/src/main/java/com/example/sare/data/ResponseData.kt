package com.example.sare.data

import com.google.gson.annotations.SerializedName

data class ResponseData(
    @SerializedName("statusCode")
    val status : Int,

    @SerializedName("message")
    val message : String,

    @SerializedName("path")
    val path : String

)