package com.example.sare.chat

import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.adapter.ChatUserAdapter
import com.example.sare.chat.viewmodel.ChatUserViewModel
import com.example.sare.database.ChatUser
import com.example.sare.extensions.gone
import com.example.sare.extensions.openActivity
import com.example.sare.extensions.visible
import com.example.sare.profile.RecyclerItemTouchHelper
import com.example.sare.profile.RecyclerTouchListener
import kotlinx.android.synthetic.main.layout_chat_history.view.*
import org.json.JSONObject


class ChatUserFragment : Fragment(), RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    var root: View? = null
    var viewModel: ChatUserViewModel? = null
    var list: MutableList<ChatUser> = ArrayList<ChatUser>()
    var userId: String? = null
    var filterList: List<ChatUser> = ArrayList()
    lateinit var adapter: ChatUserAdapter
    var image: String? = null
    var colorCode: String? = null
    var name: String? = null
    lateinit var recyclerTouchListener: RecyclerTouchListener
    lateinit var recyclerItemTouchHelper: RecyclerItemTouchHelper

    @RequiresApi(Build.VERSION_CODES.M) override fun onCreateView(inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root = inflater.inflate(R.layout.layout_chat_history, container, false)
        setTheme()
        //showVerifyPopup()
        val onBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
//                NavHostFragment.findNavController(this@ChatUserFragment).navigateUp()
                activity?.finish()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(
            requireActivity(), onBackPressedCallback)
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user = preferences.getString("USER", null)
        if (user != null) {
            val mainObject = JSONObject(user)
            userId = mainObject.getString("_id")
            image = mainObject.getString("image")
            name = mainObject.getString("name")
            colorCode = mainObject.getString("colorCode")
        }

        setListeners()

        Log.d("tag", "chat user userId::$userId")
        viewModel = ViewModelProvider(this)[ChatUserViewModel::class.java]
        viewModel?.getChatList(requireContext())

        /*root?.searchLayout?.visibility = View.GONE

        root?.imgSearch?.setOnClickListener {
            root?.searchLayout?.visibility = View.VISIBLE
            root?.imgSearch?.visibility = View.GONE
        }
        root?.imgBackUserChat?.setOnClickListener {
            NavHostFragment.findNavController(this).navigateUp()
        }
        root?.etSearchUser?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                filter(s.toString())
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
            }

            override fun afterTextChanged(s: Editable) {}
        })*/

        recyclerTouchListener = RecyclerTouchListener(requireActivity(), root?.rvChat)


        adapter = ChatUserAdapter(list, requireContext(), object : ChatUserAdapter.OnItemClickListener {
            override fun clickListener(user: ChatUser) {
                val bundle = bundleOf(
                    "userId" to user.userId,
                    "name" to user.userName,
                    "colorCode" to user.userColorCode,
                    "image" to user.userImage,
                    "blocked" to user.blocked)
                activity?.openActivity(ChatActivity::class.java, bundle)
                //                activity?.overridePendingTransition(0,0)
                /*findNavController().navigate(
                    R.id.action_chatUserFragment_to_chatFragment, bundle)*/
            }

        })
        root?.rvChat?.layoutManager = LinearLayoutManager(requireContext())
        root?.rvChat?.adapter = adapter
        recyclerItemTouchHelper = RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this)
        ItemTouchHelper(recyclerItemTouchHelper).attachToRecyclerView(root?.rvChat)
        recyclerTouchListener.setClickable(object : RecyclerTouchListener.OnRowClickListener {

            override fun onRowClicked(position: Int) {

            }


            override fun onIndependentViewClicked(independentViewID: Int, position: Int) {

            }


        }).setSwipeOptionViews(R.id.imgDelete, R.id.imgArchive, R.id.relDetails).setSwipeable(R.id.relDetails,
            R.id.relCard,
            RecyclerTouchListener.OnSwipeOptionsClickListener { viewID, position ->

                when (viewID) {
                    R.id.imgDelete -> {
                        Toast.makeText(
                            requireActivity(), "Clicked on delete!", Toast.LENGTH_SHORT).show()
                    }
                    R.id.imgArchive -> {
                        Toast.makeText(
                            requireActivity(), "Clicked on archive!", Toast.LENGTH_SHORT).show()
                    }

                }
            })
        root?.rvChat?.addOnItemTouchListener(recyclerTouchListener)




        viewModel?.allUser?.observe(viewLifecycleOwner, Observer { chatUser ->
            chatUser?.let {
                list.clear()
                Log.d("tag", "chat :: data::: $it")
                list.addAll(it)
                adapter.notifyDataSetChanged()
                if (adapter.itemCount == 0) {
                    root?.txtNoDataChat?.visibility = View.VISIBLE
                } else {
                    root?.txtNoDataChat?.visibility = View.GONE

                }
                //chatAdapter.setChatMessages(it)
                //root?.rvChat?.scrollToPosition(chatAdapter.getChatMessages().size - 1)
            }
        })

        return root
    }

    private fun setListeners() {
        root?.imgSearch?.setOnClickListener {
            activity?.openActivity(SearchActivity::class.java)
            activity?.overridePendingTransition(0, 0)
        }
        root?.imgAdd?.setOnClickListener {
            activity?.openActivity(AddUserActivity::class.java)
            //            activity?.overridePendingTransition(0,0)
        }
        root?.imgSearch?.setOnClickListener {
            addSlideFromRightAnimation()
        }
        root?.txtCancel?.setOnClickListener {
            addSlideOutLeftAnimation()
        }
        root?.imgClose?.setOnClickListener {
            root?.etSearchUser?.setText("")
        }

        root?.etSearchUser?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                filter(p0.toString())
            }
        })
    }

    private fun addSlideFromRightAnimation() {
        root?.searchLayout?.visible()
        val slideUp = AnimationUtils.loadAnimation(requireActivity(), R.anim.slide_from_right)
        root?.searchLayout?.startAnimation(slideUp)
        root?.imgSearch?.gone()
    }

    private fun addSlideOutLeftAnimation() {
        val slideUp = AnimationUtils.loadAnimation(requireActivity(), R.anim.slide_out_left)
        root?.searchLayout?.startAnimation(slideUp)
        root?.searchLayout?.gone()
        root?.imgSearch?.visible()
        root?.imgSearch?.alpha = 0f
        root?.imgSearch?.animate()?.alpha(1f)?.duration = 400
    }

    override fun onResume() {
        super.onResume()
        root?.rvChat?.addOnItemTouchListener(recyclerTouchListener)
    }


    private fun filter(text: String) {
        val filteredNames: ArrayList<ChatUser> = ArrayList()
        if (list.isNotEmpty()) {
            for (s in list) {
                if (s.userName.toLowerCase().contains(text.toLowerCase())) {
                    filteredNames.add(s)
                }
            }
        }
        filterList = adapter.filterList(filteredNames)!!

        if (filterList.isEmpty()){
            root?.txtNoDataChat?.visible()
        }else{
            root?.txtNoDataChat?.gone()
        }

    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.transparent)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireActivity())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.transparent)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }


    override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int, position: Int) {
        adapter.notifyItemChanged(viewHolder!!.adapterPosition)

    }

    fun showVerifyPopup() {
        root?.layout_popup_verify?.visible()
        /* root?.imgClose?.setOnClickListener {
             root?.layout_popup_verify?.gone()
         }*/
    }

}