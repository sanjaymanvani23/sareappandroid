package com.example.sare.chat.data

import androidx.lifecycle.LiveData
import com.example.sare.database.ChatMessage
import com.example.sare.database.ChatMessageDao

class ChatRepository(private val chatMessageDao: ChatMessageDao) {

    //val allChatMessage: LiveData<List<ChatMessage>> = chatMessageDao.getChatMessages()

    fun getChatMessages(senderId: String, receiverId: String): LiveData<List<ChatMessage>> {
        return chatMessageDao.getChatMessages(senderId, receiverId)
    }

    suspend fun insert(chatMessage: ChatMessage) {
        chatMessageDao.insert(chatMessage)
    }

}