package com.example.sare.chat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CheckUsersFromContactListQuery
import com.example.sare.R
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.chat.AddUserActivity
import com.example.sare.chat.ChatActivity
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.openActivity
import com.example.sare.extensions.setBorder
import com.example.sare.profile.activity.ProfileGuestActivity
import kotlinx.android.synthetic.main.item_search_user.view.*

class SearchUserListAdapter(var list: List<CheckUsersFromContactListQuery.CheckUsersExist>,
    private val context: Context) : RecyclerView.Adapter<SearchUserListAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchUserListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_search_user, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(item: CheckUsersFromContactListQuery.CheckUsersExist, context: Context) {
            val name = itemView.txtName
            val id = itemView.txtId
            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val relSingleItem = itemView.relDetails

            name.text = item.name()
            id.text = "@" + item.username()
            imgProfile.loadUrl(PUBLIC_URL + item.image().toString())
            imgProfileBorder.setBorder(item.colorCode().toString())

            relSingleItem.setOnClickListener {

                val bundle = bundleOf(
                "userId" to item._id(),
                "name" to item.username(),
                "colorCode" to item.colorCode(),
                "image" to item.image())
                val activity = context as AddUserActivity
                activity.openActivity(ChatActivity::class.java, bundle)
                activity.overridePendingTransition(0,0)
                    }


        }

    }

}