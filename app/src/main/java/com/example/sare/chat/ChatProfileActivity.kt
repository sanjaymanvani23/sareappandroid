package com.example.sare.chat

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.example.sare.*
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.viewmodel.ChatProfileViewModel
import com.example.sare.extensions.*
import com.example.sare.profile.activity.FollowersFollowingActivity
import com.example.sare.profile.adapter.ReportListAdapter
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import iknow.android.utils.KeyboardUtil.hideKeyboard
import kotlinx.android.synthetic.main.activity_chat_profile.*
import kotlinx.android.synthetic.main.popup_report.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@RequiresApi(Build.VERSION_CODES.M) class ChatProfileActivity : AppCompatActivity() {
    var userId = ""
    var blocked = false
    var chatProfileViewModel: ChatProfileViewModel? = null
    var token = ""
    var showRecyclerView: Boolean = false
    var muteStatus: Boolean = false
    var blockStatus: Boolean = false
    lateinit var dialog: Dialog
    var mainData: ReportListQuery.ReportType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_profile)
        setTheme()
        getSharedPreferences()
        chatProfileViewModel = ViewModelProvider(this)[ChatProfileViewModel::class.java]
        userId = intent?.getStringExtra("userId").toString()
        blocked = intent.getBooleanExtra("blocked", false)
        Log.d("tag", "userId:::$userId")
        getUserDetails(userId)

        openReportDialog()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }


    private fun openReportDialog() {
        //        val llBottomSheet = layout_popup_report
        //
        //        val bottomSheetBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(llBottomSheet as RelativeLayout)

        overlay_view_report_popup?.setOnClickListener {
            hideKeyboard(this)
            rvReportList?.gone()
            overlay_view_report_popup?.gone()
            layout_popup_report?.gone()

            Log.d("tag", "layout_popup_report setOnClickListener called!!!!")
        }
        ll_report.setOnClickListener {
            txtTitle?.setText("")
            txtReportReason?.text = ""
            txtTypeSomething?.setText("")
            overlay_view_report_popup?.visible()
            layout_popup_report?.visible()
            ll_select_report_reason?.setOnClickListener {
                rvReportList?.visible()
                reportList()
                if (showRecyclerView == true) {

                    rvReportList?.gone()
                    reportList()
                    showRecyclerView = false

                } else {
                    rvReportList?.visible()
                    showRecyclerView = true
                }
            }

            btnReportSend?.setOnClickListener {
                if (txtReportReason?.text!!.isEmpty() || txtTitle?.text!!.isEmpty()) {
                    customToastMain(
                        "Please select report reason!!",  0)
                }
            }

            /*   btnReportSend?.setOnClickListener {
                   if (txtReportReason?.text!!.isEmpty() || txtTitle?.text!!.isEmpty()) {
                       customToastMain(
                           "Please select report reason!!", 0)
                   }
               }*/

        }
    }


    private fun reportList() {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        chatProfileViewModel?.getReportList()
        chatProfileViewModel?.reportArrayList?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            chatProfileViewModel?.getReportList()
                        }
                    snackbar.show()
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.getReportList -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    Log.d("tag", "response::" + it.response)

                    initReportRecyclerView(it.response)
                }

            }
        })
    }


    private fun sendReport(data: ReportListQuery.ReportType) {
        if (txtReportReason?.text!!.isEmpty() && txtTitle?.text!!.isEmpty()) {
            customToastMain(
                "Report cant be submitted. Please select report reason!!", 0)
        } else if (txtReportReason?.text!!.isEmpty()) {
            customToastMain("Please select report reason!!", 0)
        } else if (txtTitle?.text!!.isEmpty()) {
            customToastMain("Please write report title!!", 0)
        } else if (txtTypeSomething?.text!!.isEmpty()) {
            customToastMain(
                "Please type something about report!!", 0)
        } else {
            val apolloClient = ApolloClient.setupApollo(token.toString())


            apolloClient.mutate(
                StoreReportMutation(
                    userId.toString(),
                    data.reportType()!!,
                    txtTitle?.text.toString(),
                    txtReportReason?.text.toString()))
                .enqueue(object : ApolloCall.Callback<StoreReportMutation.Data>() {
                    override fun onFailure(e: ApolloException) {
                        e.printStackTrace()
                        Log.d("tag", "Error::" + e.message)
                        customToastMain(e.message.toString(), 0)
//                        MyApplication.logoutClearPreference(applicationContext)
                        MyApplication.mainActivity.logoutUser()
                        openNewActivity(LoginActivity::class.java)
                    }

                    override fun onResponse(response: Response<StoreReportMutation.Data>) {
                        Log.d("tag", "response::$response")
                        if (response.data?.storeReportProfile() != null) {
                            runOnUiThread {
                                customToastMain(
                                    "Report Submitted!!", 1)
                                txtTitle?.setText("")
                                txtReportReason?.text = ""
                                txtTypeSomething?.setText("")
                                hideKeyboard(this@ChatProfileActivity)

                                overlay_view_report_popup?.gone()
                                layout_popup_report?.gone()
                                val mainLayout = layout_popup_report

                            }
                        } else {
                            runOnUiThread {
                                val error = response.errors?.get(0)?.message
                                Log.d("tag", "Data Error: $error")
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")

                                if (errorCode?.toString()?.equals("401") == true) {
                                    customToastMain(
                                        error.toString(), 0)
                                    MyApplication.mainActivity.logoutUser()
                                    openNewActivity(LoginActivity::class.java)
                                } else if (errorCode?.toString()?.equals("403") == true) {
                                    customToastMain(
                                        error.toString(), 0)
                                   /* MyApplication.logoutClearPreference(
                                        this@ChatProfileActivity)*/
                                    MyApplication.mainActivity.logoutUser()
                                    openNewActivity(LoginActivity::class.java)
                                } else {
                                    customToastMain(
                                        error.toString(), 0)
                                }
                                txtTitle?.setText("")
                                txtReportReason?.text = ""
                                txtTypeSomething?.setText("")
                                hideKeyboard(this@ChatProfileActivity)


                                overlay_view_report_popup?.gone()
                                layout_popup_report?.gone()
                            }
                        }
                    }

                })
        }
    }


    private fun initReportRecyclerView(list: List<ReportListQuery.ReportType>) {
        rvReportList?.apply {
            layoutManager = LinearLayoutManager(
                rvReportList?.context, RecyclerView.VERTICAL, false)
            adapter = ReportListAdapter(list, context)

            (adapter as ReportListAdapter).onItemClick = { data ->
                rvReportList?.gone()

                // do something with your item
                Log.d("tag", data.reportName().toString())
                txtReportReason?.text = data.reportName().toString()

                mainData = data

//                sendReport(mainData!!)

            }
            btnReportSend?.setOnClickListener {
                sendReport(mainData!!)
            }
        }
    }


    private fun getUserDetails(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.show()
        }
        chatProfileViewModel?.getUserDetails(id)

        chatProfileViewModel?.userDetails?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.layout), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        chatProfileViewModel?.getUserDetails(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.getUserDetails -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    setData(it.response)
                }
            }
        })

    }

    private fun muteChat(id: String) {
        chatProfileViewModel?.muteChat(id)

        chatProfileViewModel?.muteChat?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.layout), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        chatProfileViewModel?.muteChat(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.muteChat -> {
                    muteStatus = true
                    imgMute.setImageResource(R.drawable.chat_mute_ic)
                    txtMuteUnmute.text = "Unmute"

                }
            }
        })
    }

    private fun unMuteChat(id: String) {
        chatProfileViewModel?.unMuteChat(id)

        chatProfileViewModel?.unMuteChat?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.layout), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        chatProfileViewModel?.unMuteChat(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.unMuteChat -> {
                    muteStatus = false
                    imgMute.setImageResource(R.drawable.chat_unmute_ic)
                    txtMuteUnmute.text = "Mute"

                }
            }
        })

    }

    private fun blockChat(id: String) {
        chatProfileViewModel?.blockChat(id)

        chatProfileViewModel?.blockChat?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.layout), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        chatProfileViewModel?.blockChat(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.blockChat -> {
                    blockStatus = true
                    imgBlock.setImageResource(R.drawable.ic_chat_block_new)
                    txtBlockUnblock.text = "Unblock"
                }
            }
        })

    }

    private fun unBlockChat(id: String) {
        chatProfileViewModel?.unBlockChat(id)

        chatProfileViewModel?.unBlockChat?.observe(this, Observer {

            when (it) {
                is NetworkResponse.ERROR -> {

                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.layout), "Connection Error!", Snackbar.LENGTH_SHORT).setAction("RETRY") {
                        chatProfileViewModel?.unBlockChat(id)
                    }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {

                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }
                is NetworkResponse.SUCCESS.acceptChat -> {
                    blockStatus = false
                    imgBlock.setImageResource(R.drawable.chat_unlock_ic)
                    txtBlockUnblock.text = "Block"

                }
            }
        })

    }

    private fun setData(response: UserDetailsQuery.Data) {
        val policy = StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build()
        StrictMode.setThreadPolicy(policy)


        relative_layout_followers?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.user()._id(),
                "name" to response.user().name(),
                "colorCode" to response.user().colorCode(),
                "image" to response.user().image(),
                "type" to "FOLLOWERS")
            openActivity(FollowersFollowingActivity::class.java, bundle)
            overridePendingTransition(0, 0)
        }



        relative_layout_followings?.setOnClickListener {
            val bundle = bundleOf(
                "userId" to response.user()._id(),
                "name" to response.user().name(),
                "colorCode" to response.user().colorCode(),
                "image" to response.user().image(),
                "type" to "FOLLOWINGS")

            openActivity(FollowersFollowingActivity::class.java, bundle)
            overridePendingTransition(0, 0)
        }


        imgProfilePic?.loadUrl(Utils.PUBLIC_URL + response.user()?.image()!!)
        imgProfileBorder?.setBorder(response.user()?.colorCode() ?: Utils.COLOR_CODE)

        val textToCopy = "https://www.roleit.in/shareit/?" + response.user()._id()
        ll_share.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_TEXT, "Download ROLEIT App from Playstore  \n $textToCopy")
            startActivity(Intent.createChooser(share, "Share to"))

            //  openActivity(ShareProfileActivity::class.java)
        }
        txtName?.text = response.user().name().toString()
        txtId?.text = "@" + response.user().username().toString()
        if (response.user().likeCounts().toString() < 0.toString()) {
            txtLikes?.text = 0.toString() + " Like"
        } else {
            txtLikes?.text = response.user().likeCounts().toString() + " Likes"
        }

        if (response.user().followingCounts()!! > 0) {
            txtFollowingCount?.text = response.user().followingCounts().toString()
        } else {
            txtFollowingCount?.text = 0.toString()
        }
        val imageView = header_cover_image
        Glide.with(this).load(Utils.PUBLIC_URL + (response.user()?.coverImage().toString()))
            .load(Utils.COVER_IMAGE_URL + response.user()?.coverImage()).error(R.drawable.cover_default)
            .into(imageView!!)
        if (response.user().postsCounts()!! > 0) {
            txtPostCounts?.text = response.user().postsCounts().toString()
        } else {
            txtPostCounts?.text = "0"
        }

        txtJoinedDate?.text = "Joined On " + getParsedDate(response.user().createdAt().toString())

        if (response.user().followerCounts()!! > 0) {
            txtFollowersCount?.text = response.user().followerCounts().toString()
        } else {
            txtFollowersCount?.text = 0.toString()
        }


        ll_back.setOnClickListener {
            onBackPressed()
        }

        ll_mute.setOnClickListener {
            Log.d("tag", "mutesTATUS :: $muteStatus")
            if (muteStatus) {
                unMuteChat(userId)
            } else {
                muteChat(userId)
            }
        }

        ll_block.setOnClickListener {
            Log.d("tag", "blockstatus :: $blockStatus")
            if (blockStatus) {
                unBlockChat(userId)
            } else {
                blockChat(userId)
            }
        }

        if (blocked){
            imgBlock.setImageResource(R.drawable.ic_chat_block_new)
            txtBlockUnblock.text = "Unblock"
        }else{
            imgBlock.setImageResource(R.drawable.chat_unlock_ic)
            txtBlockUnblock.text = "Block"
        }

        if (response.user().followed() == true) {
            button_follow?.text = "Following"
        } else {
            button_follow?.text = "Follow"
        }
        button_follow?.setOnClickListener {
            button_follow?.isEnabled = false
            if (button_follow?.text == "Following") {
                unFollowUser(response.user()._id())
            } else {
                followUser(response.user()._id())
            }
        }


    }

    private fun getParsedDate(date: String): String {
        val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        var parsedDate: String? = null
        parsedDate = try {
            val date1: Date = df.parse(date)
            val outputFormatter1: DateFormat = SimpleDateFormat("MMMM yyyy")
            outputFormatter1.format(date1)
        } catch (e: ParseException) {
            e.printStackTrace()
            Log.e("tag", "Exception::${e.message}")
            date
        }
        return parsedDate.toString()
    }


    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.transparent)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.transparent)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    private fun followUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        Log.d("tag", "token::$token")
        Log.d("tag", "id::$id")
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            FollowUserMutation(
                id))

            .enqueue(object : ApolloCall.Callback<FollowUserMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("tag", "Error Data : ${e.message}")
                    customToastMain(e.message.toString(), 0)
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            followUser(id)
                        }
                    snackbar.show()
                }

                override fun onResponse(response: Response<FollowUserMutation.Data>) {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    if (response.data?.follow()?.status() == true) {
                        Log.d("tag", "response follow:::$response")
                        runOnUiThread {
                            button_follow?.isEnabled = true
                            button_follow?.text = "Following"
                            txtFollowersCount?.text = (txtFollowersCount?.text.toString().toInt() + 1).toString()
                        }


                    } else {
                        runOnUiThread {
                            button_follow?.isEnabled = true
                            val error = response.errors?.get(0)?.message
                            Log.d("tag", "Data Error: $error")
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            if (errorCode?.toString()?.equals("401") == true) {
                                customToastMain(error.toString(), 0)
                                MyApplication.mainActivity.logoutUser()
                                openNewActivity(LoginActivity::class.java)
                            } else if (errorCode?.toString()?.equals("403") == true) {
                                customToastMain(error.toString(), 0)
                                MyApplication.mainActivity.logoutUser()
                                openNewActivity(LoginActivity::class.java)
                            } else {
                                customToastMain(error.toString(), 0)
                            }
                        }
                    }

                }
            })

    }


    private fun unFollowUser(id: String) {
        val progressDialog = CustomProgressDialogNew(this)
        runOnUiThread {
            progressDialog.hide()
        }
        val apolloClientNew = ApolloClient.setupApollo(token.toString())
        apolloClientNew.mutate(
            UnFollowUserMutation(
                id)).enqueue(object : ApolloCall.Callback<UnFollowUserMutation.Data>() {
            override fun onFailure(e: ApolloException) {
                Log.d("tag", "Error Data : ${e.message}")
                runOnUiThread {
                    progressDialog.hide()
                }
                val snackbar: Snackbar = Snackbar.make(
                    findViewById(R.id.coordinateLayout), "Connection Error!", Snackbar.LENGTH_SHORT)
                    .setAction("RETRY") {
                        unFollowUser(id)
                    }
                snackbar.show()
            }

            override fun onResponse(response: Response<UnFollowUserMutation.Data>) {
                runOnUiThread {
                    progressDialog.hide()
                }
                Log.d("tag", "response unfollow:::$response")
                if (response.data?.unfollow()?.status() == true) {
                    runOnUiThread {
                        button_follow?.isEnabled = true
                        button_follow?.text = "Follow"
                        txtFollowersCount?.text = (txtFollowersCount?.text.toString().toInt() - 1).toString()

                    }


                } else {
                    runOnUiThread {
                        button_follow?.isEnabled = true
                        val error = response.errors?.get(0)?.message
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        if (errorCode?.toString()?.equals("401") == true) {
                            customToastMain(error.toString(), 0)
                            MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else if (errorCode?.toString()?.equals("403") == true) {
                            customToastMain(error.toString(), 0)
                            MyApplication.mainActivity.logoutUser()
                            openNewActivity(LoginActivity::class.java)
                        } else {
                            customToastMain(error.toString(), 0)
                        }
                    }
                }

            }
        })
    }


}