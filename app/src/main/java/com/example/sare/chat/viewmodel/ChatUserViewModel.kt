package com.example.sare.chat.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.UserChatHistoryQuery
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.database.*
import kotlinx.coroutines.runBlocking
import java.text.SimpleDateFormat
import java.util.*

class ChatUserViewModel(application: Application): AndroidViewModel(application) {

    var chatUserList = MutableLiveData<NetworkResponse>()

    val chatUserDao: ChatUserDao
    val chatMessageDao: ChatMessageDao
    val allUser: LiveData<List<ChatUser>>

    init {
        chatUserDao = SareRoomDatabase.getDatabase(application).chatUserDao()
        chatMessageDao = SareRoomDatabase.getDatabase(application).chatMessageDao()
        allUser = chatUserDao.getChatUser()
    }

    fun getChatList(context: Context) {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val token = preferences.getString("TOKEN", null)
        //val userId = preferences.getString("UserID", null)
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UserChatHistoryQuery())
            .enqueue(object : ApolloCall.Callback<UserChatHistoryQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "error:::" + e.message)

                    //chatUserList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UserChatHistoryQuery.Data>) {
                    runBlocking {
                        Log.d("tag", "response:::" + response)
                        try{
                        response.data()?.userChatHistory()?.forEach {
                            val userReceiver = it.receiverUser()
                            val userReceiverData = chatUserDao.getChatUserByUserId(userReceiver?._id()!!)
                            if(userReceiverData == null) {
                                val chatUser = ChatUser(userReceiver?._id(), it.message(), it.receiver(),
                                    userReceiver?.image()?:"", userReceiver?.name()?:"", userReceiver?.colorCode()
                                        ?:"", convertDate(it.createdAt().toString()),userReceiver.blocked()!!)
                                chatUserDao.insert(chatUser)
                            }
                            val userSender = it.senderUser()
                            val userSenderData = chatUserDao.getChatUserByUserId(userSender?._id()!!)
                            if(userSenderData == null) {
                                val chatUser = ChatUser(userSender?._id()?:"", it.message(), it.receiver(),
                                    userSender?.image()?:"", userSender?.name()?:"", userSender?.colorCode()?:"",
                                    convertDate(it.createdAt().toString()), userSender.blocked()!!)
                                chatUserDao.insert(chatUser)
                            }
                            if(chatMessageDao.getChatMessageById(it?.receiver()!!).size == 0) {
                                val userMessageDio = ChatMessage(
                                    it?.senderUser()?._id()?:"",
                                    it?.receiverUser()?._id()?:"",
                                    it?.message(),
                                    it?.receiver(),
                                    convertDate(it.createdAt().toString())
                                )
                                chatMessageDao.insert(userMessageDio)
                            }

                        }
                        } catch (e: Exception) {
                          //  chatUserList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                        }
                        /*chatArrayList.postValue(
                            NetworkResponse.SUCCESS.getFollowingList(
                                //response.data()?.userChatHistory()!!
                            )
                        )*/
                    }
                }
            })
    }

    fun convertDate(date: String) : Date {
        val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        return format.parse(date)
    }

}