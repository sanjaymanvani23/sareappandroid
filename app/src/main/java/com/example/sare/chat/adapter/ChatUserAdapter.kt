package com.example.sare.chat.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.example.sare.CryptLib
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.Utils.Companion.PUBLIC_URL
import com.example.sare.database.ChatUser
import com.example.sare.extensions.gone
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.example.sare.extensions.visible
import kotlinx.android.synthetic.main.item_chat_user.view.*
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class ChatUserAdapter(var list: List<ChatUser>, private val context: Context, val itemClickListener: OnItemClickListener) :
    RecyclerView.Adapter<ChatUserAdapter.ViewHolder>() {

    val key = "SahilThakar"
    val cryptLib = CryptLib()
    private val viewBinderHelper = ViewBinderHelper()

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat_user, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return list.size
    }

    //the class is holding the list view
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var viewForeground: LinearLayout
        lateinit var backGroundLeft: RelativeLayout
        lateinit var backGroundRight: RelativeLayout
        fun bindItems(list: ChatUser, context: Context) {
            viewForeground = itemView.relDetails
            val name = itemView.txtName
            val tv_msg = itemView.tv_msg
            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val imgBlocked = itemView.imgBlocked
            val txtTime = itemView.txtTime
           backGroundLeft = itemView.relPin
           backGroundRight = itemView.relDelete
//           val swipe_layout = itemView.swipe_layout

//            viewBinderHelper.bind(swipe_layout, list.id.toString());
            //val button = itemView.button_following
            //likes.text = list.user().likeCounts().toString()
            name.text = list.userName
            val decryptedMessage = cryptLib.decryptCipherTextWithRandomIV(list.messageContent, key)
            tv_msg.text = decryptedMessage
            Log.d("tag","dateTime:: ${list.dateTime}")


            imgProfile?.loadUrl(PUBLIC_URL+(
                    list.userImage.toString())!!)
            imgProfileBorder?.setBorder(list.userColorCode ?: Utils.COLOR_CODE)
            itemView.relDetails.setOnClickListener {
                itemClickListener.clickListener(list)
            }

            txtTime.text = getParsedTime(list.dateTime.toString())

            Log.d("tag","list.blocked:::${list.blocked}")
            if (list.blocked){
                imgBlocked.visible()
            }else{
                imgBlocked.gone()
            }
        }


        private fun getParsedTime(date: String): String {
            val df: DateFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy")
            df.setTimeZone(TimeZone.getTimeZone("GMT"));
            val SECOND_MILLIS = 1000
            val MINUTE_MILLIS = 60 * SECOND_MILLIS
            val HOUR_MILLIS = 60 * MINUTE_MILLIS
            val DAY_MILLIS = 24 * HOUR_MILLIS
            val WEEK_MILLIS = 7 * DAY_MILLIS
            var parsedDate: String? = null
            parsedDate = try {
                var time: Long = df.parse(date).time
                if (time < 1000000000000L) {
                    // if timestamp given in seconds, convert to millis
                    time *= 1000;
                }

                val now = System.currentTimeMillis()

                val diff = now - time;

                Log.d("tag", "diff:::$diff")

                when {
                    diff < MINUTE_MILLIS -> {
                        return "just now";
                    }
                    diff < 50 * MINUTE_MILLIS -> {
                        return (diff / MINUTE_MILLIS).toString() + "m ago";
                    }
                   /* diff < 90 * MINUTE_MILLIS -> {
                        return "an hour ago";
                    }*/
                    /*diff < 24 * HOUR_MILLIS -> {
                        return (diff / HOUR_MILLIS).toString() + " hrs ago";
                    }*/
                    diff < 48 * HOUR_MILLIS -> {
                        return "1 day";

                    }
                    else -> {
                        val date1 = getParsedDate(date.toString())
                        Log.d("tag", date1)
                        parsedDate = date1
                        return parsedDate.toString()
                    }
                }

            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("tag", "Exception::${e.message}")
                val date1 = getParsedDate(date.toString())
                Log.d("tag", date1)
                parsedDate = date1
                return parsedDate.toString()
            }
            return parsedDate.toString()
        }

        private fun getParsedDate(date: String): String {

            val df: DateFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy", Locale.getDefault())
            var parsedDate: String? = null
            parsedDate = try {
                Log.d("tag", "date:::1:$date")
                val date1: Date = df.parse(date)
                val outputFormatter1: DateFormat = SimpleDateFormat("dd/MM/yy", Locale.getDefault())
                outputFormatter1.format(date1)

            } catch (e: ParseException) {
                e.printStackTrace()
                Log.e("tag", "Exception::${e.message}")
                date
            }
            Log.d("tag", "date::$parsedDate")
            return parsedDate.toString()
        }

    }

    fun filterList(filteredNames: List<ChatUser>): List<ChatUser>? {
        this.list = filteredNames
        notifyDataSetChanged()
        return filteredNames
    }


    fun showMenu(position: Int) {

//        for (i in list.indices) {
//            list[i].setShowMenu(false)
//        }
//        list[position].setShowMenu(true)
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun clickListener(user: ChatUser)
    }

}