/**
 * @author Joyce Hong
 * @email soja0524@gmail.com
 * @created 2019-09-03
 * @desc
 */

package com.example.sare.chat.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CryptLib
import com.example.sare.R
import com.example.sare.database.ChatMessage
import com.example.sare.toSimpleString
import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.*


class ChatAdapter(
    val context: Context,
    val senderId: String,
    val senderProfileColor: String,
    val receiverProfileColor: String
) : RecyclerView.Adapter<ChatAdapter.ViewHolder>(){

    private var chatMessage = emptyList<ChatMessage>()
    val key = "SahilThakar"
    val cryptLib = CryptLib()
   // var date = "11"
    var date: String = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

       // Log.d("chatlist size",chatList.size.toString())
        var view : View? = null
        when(viewType){

            0 -> {
                view = LayoutInflater.from(context).inflate(
                    R.layout.item_chat_sender,
                    parent,
                    false
                )
                Log.d("user inflating", "viewType : ${viewType}")
            }

            1 -> {
                view = LayoutInflater.from(context).inflate(
                    R.layout.item_chat_reciever,
                    parent,
                    false
                )
                Log.d("partner inflating", "viewType : ${viewType}")
            }
            2 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_date, parent, false)
                Log.d("someone in or out", "viewType : ${viewType}")
            }
            3 -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_date, parent, false)
                Log.d("someone in or out", "viewType : ${viewType}")
            }
        }

        return ViewHolder(view!!)
    }

    override fun getItemCount(): Int {
        return chatMessage.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(chatMessage[position].senderId == senderId) {
            0
        } else {
            1
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val messageData  = chatMessage[position]
        val userName = messageData.senderId
        //val content = messageData.messageContent
        Log.e("Chat Adapter", "Chat ${messageData.messageContent}")
        val content = cryptLib.decryptCipherTextWithRandomIV(messageData.messageContent, key)
        //val viewType = messageData.senderId == "paras"
        Log.e(
            "Chat Adapter",
            "Chat Date :  ${date}" + messageData.dateTime.toSimpleString("dd/MM/yyyy")
        )
        if(date != messageData.dateTime.toSimpleString("dd/MM/yyyy")) {
            date = messageData.dateTime.toSimpleString("dd/MM/yyyy")

            holder.date.text = if(Date().toSimpleString("dd/MM/yyyy") == date) {
                "Today"
            } else if(Date().toSimpleString("dd/MM/yyyy") == date) {
                "Yesterday"
            } else {
                date
            }
            holder.date.visibility = View.VISIBLE
        } else {
            holder.date.visibility = View.GONE
        }

        if(chatMessage[position].senderId == senderId) {
            holder.message.text = content
            holder.time.text = messageData.dateTime.toSimpleString("hh:mm a")
            try {
                holder.ll_main.setBackgroundTintList(
                    ColorStateList.valueOf(
                        Color.parseColor(
                            senderProfileColor
                        )
                    )
                )
            } catch (e: Exception) {
                holder.ll_main.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")))
            }

        } else {
            holder.message.text = content
            holder.time.text = messageData.dateTime.toSimpleString("hh:mm a")
            try {
                holder.ll_main.setBackgroundTintList(
                    ColorStateList.valueOf(
                        Color.parseColor(
                            receiverProfileColor
                        )
                    )
                )
            } catch (e: Exception) {
                holder.ll_main.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#000000")))
            }
        }

        /*when(messageData.senderId) {

            Constants.getChatSender() -> {
                holder.message.setText(content)
            }
            Constants.getChatReceiver() ->{
                holder.message.setText(content)
            }
            USER_JOIN -> {
                val text = "${userName} has entered the room"
                holder.text.setText(text)
            }
            USER_LEAVE -> {
                val text = "${userName} has leaved the room"
                holder.text.setText(text)
            }
        }*/

    }
    inner class ViewHolder(itemView: View):  RecyclerView.ViewHolder(itemView) {
        //val userName = itemView.findViewById<TextView>(R.id.username)
        val message = itemView.findViewById<TextView>(R.id.message)
        val text = itemView.findViewById<TextView>(R.id.text)
        val time = itemView.findViewById<TextView>(R.id.time)
        val date = itemView.findViewById<TextView>(R.id.date)
        val ll_main = itemView.findViewById<LinearLayout>(R.id.ll_main)
    }

    internal fun setChatMessages(chatMessages: List<ChatMessage>) {
        val gson = Gson()
        val jsonArray = gson.toJson(chatMessages)
        Log.e("Tag :", jsonArray.toString())
        this.chatMessage = chatMessages
        notifyDataSetChanged()
    }

    internal fun getChatMessages(): List<ChatMessage> {
        return this.chatMessage
    }

}