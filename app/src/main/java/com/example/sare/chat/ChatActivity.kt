package com.example.sare.chat

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sare.CryptLib
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.adapter.ChatAdapter
import com.example.sare.chat.data.HeartBeat
import com.example.sare.chat.data.NewMessage
import com.example.sare.chat.data.Subscribe
import com.example.sare.chat.data.TokenStore
import com.example.sare.chat.viewmodel.ChatViewModel
import com.example.sare.database.ChatMessage
import com.example.sare.extensions.*
import kotlinx.android.synthetic.main.activity_chat.*
import com.google.gson.Gson
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_chat.imgBack
import kotlinx.android.synthetic.main.activity_chat.imgProfile
import kotlinx.android.synthetic.main.activity_chat.imgSearch
import kotlinx.android.synthetic.main.activity_chat.layoutHeader
import kotlinx.android.synthetic.main.activity_chat.searchLayout
import kotlinx.android.synthetic.main.activity_chat.txtName
import kotlinx.android.synthetic.main.layout_chat_history.view.*
import org.json.JSONObject
import java.net.URISyntaxException

class ChatActivity : AppCompatActivity() {
    private lateinit var chatViewModel: ChatViewModel

    private val TAG = ChatFragment::class.java.simpleName

    //private val URL_SERVER = "https://sare.in"
    private val URL_SERVER = "https://api.sarevids.com"
    //private val URL_SERVER = "http://192.168.1.28:5000"

    private var mSocket: Socket? = null
    private val gson: Gson = Gson()

    //For setting the recyclerView.
    //private val chatList: ArrayList<Message> = arrayListOf();
    lateinit var chatAdapter: ChatAdapter

    private var senderId: String = ""
    private var receiverId: String = ""
    val key = "SahilThakar"
    val cryptLib = CryptLib()
    var userId: String = ""
    var name: String? = null
    var image: String? = null
    var colorCode: String? = null
    var blocked: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        getSocket()
        mSocket?.on(Socket.EVENT_CONNECT, onConnect)
        mSocket?.on("updateChat", onUpdateChat)
        mSocket?.on("sentMessages", sentMessage)

        userId = intent?.getStringExtra("userId").toString()
        Log.d("tag", "guest chat id::$userId")
        setTheme()
        chatViewModel = ViewModelProvider(this).get(ChatViewModel::class.java)
        setupUI()
    }


    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }

        
    }

    private fun setupUI() {

        userId = intent?.getStringExtra("userId").toString()
        receiverId = userId
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user: String? = preferences.getString("USER", null)

        val mainObject = JSONObject(user)

        senderId = mainObject.getString("_id")
        Log.d("tag", "senderId::::$senderId")
        // senderId = preferences.getString("UserID", "")!!
        name = intent?.getStringExtra("name").toString()
        blocked = intent.getBooleanExtra("blocked", false)

        image = intent?.getStringExtra("image").toString()
        colorCode = intent?.getStringExtra("colorCode").toString()
        setData(colorCode, name)

        imgBack.setOnClickListener {
           onBackPressed()
        }

        colorCode = colorCode!!.replace("#", "#26")
        chatAdapter = ChatAdapter(this, senderId, colorCode.toString(), colorCode.toString())
        rv_chat.adapter = chatAdapter

        rv_chat.apply {
            layoutManager = LinearLayoutManager(this@ChatActivity).apply {
                stackFromEnd = true
                reverseLayout = false
            }
        }
        tv_send.setOnClickListener {
            if (et_message.text.isNotEmpty()) sendMessage()
        }

        chatViewModel.getChatMessages(senderId, receiverId).observe(this, Observer { chatMessages ->
            chatMessages?.let {
                chatAdapter.setChatMessages(it)
                rv_chat.scrollToPosition(chatAdapter.getChatMessages().size - 1)
            }
        })


        layoutHeader?.setOnClickListener {
            openActivity(ChatProfileActivity::class.java, bundleOf("userId" to userId, "blocked" to blocked))
        }

        imgSearch?.setOnClickListener {
            addSlideFromRightAnimation()
        }
        txtCancel?.setOnClickListener {
            addSlideOutLeftAnimation()
        }
        imgClose?.setOnClickListener {
            etSearchUser?.setText("")
        }

    }

    private fun setData(colorCode: String?, name: String?) {
        /*val policy =
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        StrictMode.setThreadPolicy(policy)*/
        txtName.text = name

        /*  imgProfile?.loadDiamondImage(
              Utils.PUBLIC_URL +(
                  image.toString()),
                  colorCode ?: Utils.COLOR_CODE,
                  this
              )*/

        imgProfile.loadUrl(
            Utils.PUBLIC_URL + (image.toString()))
        imgProfileBorder.setBorder(colorCode ?: Utils.COLOR_CODE)

        /*try {

            val url = URL(Utils.IMAGE_URL + image)
            var image: Bitmap? = null
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            imageView3?.loadCircularImage(
                image,
                5f,
                0,
                R.drawable.ic_login_profile,
                image!!, colorCode, this
            )
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.message)
            var image = BitmapFactory.decodeResource(resources, R.drawable.ic_login_profile)

            Log.d("tag", "error:::${e.message}")
            imageView3?.loadCircularImage(
                image,
                5f,
                0,
                R.drawable.ic_login_profile,
                image!!, "#000000", this
            )*/

        //            try {
        //
        //            } catch (e: Exception) {
        //                //something went wrong
        //                e.printStackTrace()
        //
        //            }

        //}


    }

    private fun getSocket(): Socket? {
        if (mSocket == null) {
            try {
                val opts = IO.Options()
                opts.forceNew = true
                opts.query = "socket.io"
                mSocket = IO.socket(URL_SERVER, opts)
                mSocket?.connect()
            } catch (e: URISyntaxException) {
                Log.e("TAGgetSocket", "getSocket: " + e.message)
                throw RuntimeException(e)
            }
        }
        return mSocket
    }

    var onUpdateChat = Emitter.Listener {
        Log.d(TAG, "Data 1111: ${it[0]}")
        //{"userName":"paras","messageContent":"test"}
        if (it.isNotEmpty()) {
            val decryptedMessage = cryptLib.decryptCipherTextWithRandomIV(it[0].toString(), key)
            Log.d(TAG, "Data : $decryptedMessage")
            //val chat: Message = gson.fromJson(it[0].toString(), Message::class.java)
            mSocket?.emit("messageAcknowledgment", it[0].toString(), senderId)
            val chatMessage: ChatMessage = gson.fromJson(decryptedMessage, ChatMessage::class.java)

            Log.e("tag", "ChatMessage  :" + chatMessage.dateTime)
            //chat.viewType = MessageType.CHAT_PARTNER.index
            //Log.d(TAG, "Data : ${mSocket?.id()}")
            addItemToRecyclerView(chatMessage)
        }
    }

    var onConnect = Emitter.Listener {
        val data = Subscribe(senderId)
        val jsonData = gson.toJson(data)
        Log.d(TAG, "Data : ${mSocket?.id()}")
        val encryptedContent = cryptLib.encryptPlainTextWithRandomIV(jsonData, key)
        mSocket?.emit("subscribe", encryptedContent)

        val dataToken = TokenStore(senderId)
        val jsonDataToken = gson.toJson(dataToken)
        val encryptedContentToken = cryptLib.encryptPlainTextWithRandomIV(jsonDataToken, key)
        mSocket?.emit("tokenStore", encryptedContentToken)

        handler.postDelayed(r, 0)

    }

    var sentMessage = Emitter.Listener {
        val decryptedMessage = cryptLib.decryptCipherTextWithRandomIV(it[0].toString(), key)
        Log.e("messageAcknowledgment", decryptedMessage)
        mSocket?.emit("sentMessageAcknowledgment", it[0].toString())

    }

    private fun sendMessage() {
        val content = et_message.text.toString().trim()
        val encryptedContent = cryptLib.encryptPlainTextWithRandomIV(content, key)
        //val sendData = SendMessage(userName, content, roomName)
        val sendData = NewMessage("1111111", encryptedContent, receiverId, senderId)
        val jsonData = gson.toJson(sendData)
        Log.d("chat", "" + jsonData.toString())
        val encryptedContentData = cryptLib.encryptPlainTextWithRandomIV(jsonData, key)
        mSocket?.emit("newMessage", encryptedContentData)

        //val message = Message(senderId, content,receiverId,"", MessageType.CHAT_MINE.index)
        val chatMessage = ChatMessage(
            senderId, receiverId, encryptedContent, "")
        addItemToRecyclerView(chatMessage)
    }

    private fun addItemToRecyclerView(chatMessage: ChatMessage) {

        //Since this function is inside of the listener,
        // You need to do it on UIThread!
        //requireActivity().runOnUiThread {
        Log.e("Tag Chat", "" + chatMessage.dateTime)
        chatViewModel.insert(chatMessage)
        et_message.setText("")
        //}
    }

    val handler = Handler()
    val r: Runnable = object : Runnable {
        override fun run() {
            val dataHeartBeat = HeartBeat(senderId, receiverId)
            val jsonDataHeartBeat = gson.toJson(dataHeartBeat)
            val encryptedHeartBeat = cryptLib.encryptPlainTextWithRandomIV(jsonDataHeartBeat, key)
            mSocket?.emit("heartBeat", encryptedHeartBeat)
            handler.postDelayed(this, 1000)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(r)
        mSocket?.disconnect()
    }


    private fun addSlideFromRightAnimation() {
        searchLayout?.visible()
        val slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_from_right)
        searchLayout?.startAnimation(slideUp)
        imgSearch?.gone()
    }

    private fun addSlideOutLeftAnimation() {
        val slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_out_left)
        searchLayout?.startAnimation(slideUp)
        searchLayout?.gone()
        imgSearch?.visible()
        imgSearch?.alpha = 0f
        imgSearch?.animate()?.alpha(1f)?.duration = 400
    }
}