package com.example.sare.chat.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.sare.chat.data.ChatRepository
import com.example.sare.database.ChatMessage
import com.example.sare.database.SareRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ChatViewModel(application: Application): AndroidViewModel(application) {

    private val repository: ChatRepository
   // var allChatMessage: LiveData<List<ChatMessage>>

    init {
        val chatMessageDao = SareRoomDatabase.getDatabase(application).chatMessageDao()
        repository = ChatRepository(chatMessageDao)
        //allChatMessage = repository.allChatMessage
    }

    /*fun getChatMessages(senderId: String, receiverId: String) {
        viewModelScope.launch {
            allChatMessage = repository.getChatMessages(senderId, receiverId)
        }
    }*/

    fun getChatMessages(senderId: String, receiverId: String) = repository.getChatMessages(senderId, receiverId)

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(chatMessage: ChatMessage) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(chatMessage)
    }
}