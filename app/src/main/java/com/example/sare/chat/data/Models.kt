/**
 * @author Joyce Hong
 * @email soja0524@gmail.com
 * @created 2019-09-03
 * @desc
 */

package com.example.sare.chat.data

data class Message (val senderId: String, val receiverId: String, val messageContent: String, val messageId: String, var viewType : Int)

data class Subscribe(val userId: String)
data class TokenStore(val userId: String, val token: String = "", val mobileType: String = "ANDROID")
data class HeartBeat(val senderId: String, val receiverId: String)
data class NewMessage(val localMessageId: String, val messageContent: String, val receiverId: String, val senderId: String)
//data class updateChat(val userName: String, val messageContent: String)
