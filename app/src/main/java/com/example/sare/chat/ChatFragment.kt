package com.example.sare.chat

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sare.CryptLib
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.adapter.ChatAdapter
import com.example.sare.chat.data.HeartBeat
import com.example.sare.chat.data.NewMessage
import com.example.sare.chat.data.Subscribe
import com.example.sare.chat.data.TokenStore
import com.example.sare.chat.viewmodel.ChatViewModel
import com.example.sare.database.ChatMessage
import com.example.sare.databinding.FragmentChatBinding
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import com.google.gson.Gson
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.emitter.Emitter
import org.json.JSONObject
import java.net.URISyntaxException


class ChatFragment : Fragment() {

    private lateinit var binding: FragmentChatBinding
    private lateinit var chatViewModel: ChatViewModel

    private val TAG = ChatFragment::class.java.simpleName

    //private val URL_SERVER = "https://sare.in"
    private val URL_SERVER = "https://api.sarevids.com"
    //private val URL_SERVER = "http://192.168.1.28:5000"

    private var mSocket: Socket? = null
    private val gson: Gson = Gson()

    //For setting the recyclerView.
    //private val chatList: ArrayList<Message> = arrayListOf();
    lateinit var chatAdapter: ChatAdapter

    private var senderId: String = ""
    private var receiverId: String = ""
    val key = "SahilThakar"
    val cryptLib = CryptLib()
    var userId: String = ""
    var name: String? = null
    var image: String? = null
    var colorCode: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSocket()
        mSocket?.on(Socket.EVENT_CONNECT, onConnect)
        mSocket?.on("updateChat", onUpdateChat)
        mSocket?.on("sentMessages", sentMessage)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false)
        userId = arguments?.get("userId").toString()
        Log.d("tag", "guest chat id::$userId")
        setTheme()
        chatViewModel = ViewModelProvider(this).get(ChatViewModel::class.java)
        setupUI()
        /*val view = inflater.inflate(R.layout.fragment_chat, container, false)
        val textView = view.findViewById<TextView>(R.id.textView)

        if (android.os.Build.VERSION.SDK_INT >= 29){
            view.background.colorFilter = BlendModeColorFilter(Color.parseColor("#FF0000"), BlendMode.SRC_ATOP)
        } else {
            textView.background.setColorFilter(
                Color.parseColor("#FF0000"),
                PorterDuff.Mode.SRC_ATOP
            )
        }*/

        return binding.root
    }

    private fun setTheme() {
        requireActivity().window.statusBarColor = ThemeManager.colors(
            requireContext(), StringSingleton.bodyBackground)
        requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(requireContext())) {
            requireActivity().window.navigationBarColor = ThemeManager.colors(
                requireContext(), StringSingleton.bodyBackground)
            requireActivity().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }

        binding.root.setBackgroundColor(
            ThemeManager.colors(requireContext(), StringSingleton.bodyBackground))
        binding.tvName.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textBlack))
        binding.tvWant.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textBlack))
        binding.tvLikesCount.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvVideoCount.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvFollowersCount.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textBlack))
        binding.tvFollowers.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvMessage.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvAccept.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvDelete.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvBlock.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))

        binding.etMessage.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight))
        binding.tvBottom.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.bodyBackground))
        /*binding.tvProfileName.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textBlack)
        )
        binding.tvLastSeen.setTextColor(
            ThemeManager.colors(requireContext(), StringSingleton.textLight)
        )*/
    }

    private fun setupUI() {

        userId = arguments?.getString("userId").toString()
        receiverId = userId
        val preferences: SharedPreferences = requireActivity().getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val user: String? = preferences.getString("USER", null)

        val mainObject = JSONObject(user)

        senderId = mainObject.getString("_id")
        Log.d("tag", "senderId::::$senderId")
        // senderId = preferences.getString("UserID", "")!!
        name = arguments?.getString("name").toString()

        image = arguments?.getString("image").toString()
        colorCode = arguments?.getString("colorCode").toString()
        setData(colorCode, name)

        binding.imgBack.setOnClickListener {
            //            requireActivity().onBackPressed()
            NavHostFragment.findNavController(this).navigateUp()
        }

        colorCode = colorCode!!.replace("#", "#26")
        chatAdapter = ChatAdapter(requireContext(), senderId, colorCode.toString(), colorCode.toString())
        binding.rvChat.adapter = chatAdapter

        binding.rvChat.apply {
            layoutManager = LinearLayoutManager(requireContext()).apply {
                stackFromEnd = true
                reverseLayout = false
            }
        }
        binding.tvSend.setOnClickListener {
            if (binding.etMessage.text.isNotEmpty()) sendMessage()
        }

        chatViewModel.getChatMessages(senderId, receiverId).observe(viewLifecycleOwner, Observer { chatMessages ->
            chatMessages?.let {
                chatAdapter.setChatMessages(it)
                binding.rvChat.scrollToPosition(chatAdapter.getChatMessages().size - 1)
            }
        })

    }

    private fun setData(colorCode: String?, name: String?) {
        /*val policy =
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        StrictMode.setThreadPolicy(policy)*/
        binding.txtName.text = name

        /*  binding.imgProfile?.loadDiamondImage(
              Utils.PUBLIC_URL +(
                  image.toString()),
                  colorCode ?: Utils.COLOR_CODE,
                  requireContext()
              )*/

        binding.imgProfile.loadUrl(
            Utils.PUBLIC_URL + (image.toString()))
        binding.imgProfileBorder.setBorder(colorCode ?: Utils.COLOR_CODE)

        /*try {

            val url = URL(Utils.IMAGE_URL + image)
            var image: Bitmap? = null
            image = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            binding.imageView3?.loadCircularImage(
                image,
                5f,
                0,
                R.drawable.ic_login_profile,
                image!!, colorCode, requireContext()
            )
        } catch (e: Exception) {
            e.printStackTrace()
            println(e.message)
            var image = BitmapFactory.decodeResource(resources, R.drawable.ic_login_profile)

            Log.d("tag", "error:::${e.message}")
            binding.imageView3?.loadCircularImage(
                image,
                5f,
                0,
                R.drawable.ic_login_profile,
                image!!, "#000000", requireContext()
            )*/

        //            try {
        //
        //            } catch (e: Exception) {
        //                //something went wrong
        //                e.printStackTrace()
        //
        //            }

        //}


    }

    private fun getSocket(): Socket? {
        if (mSocket == null) {
            try {
                val opts = IO.Options()
                opts.forceNew = true
                opts.query = "socket.io"
                mSocket = IO.socket(URL_SERVER, opts)
                mSocket?.connect()
            } catch (e: URISyntaxException) {
                Log.e("TAGgetSocket", "getSocket: " + e.message)
                throw RuntimeException(e)
            }
        }
        return mSocket
    }

    var onUpdateChat = Emitter.Listener {
        Log.d(TAG, "Data 1111: ${it[0]}")
        //{"userName":"paras","messageContent":"test"}
        if (it.isNotEmpty()) {
            val decryptedMessage = cryptLib.decryptCipherTextWithRandomIV(it[0].toString(), key)
            Log.d(TAG, "Data : $decryptedMessage")
            //val chat: Message = gson.fromJson(it[0].toString(), Message::class.java)
            mSocket?.emit("messageAcknowledgment", it[0].toString(), senderId)
            val chatMessage: ChatMessage = gson.fromJson(decryptedMessage, ChatMessage::class.java)

            Log.e("tag", "ChatMessage  :" + chatMessage.dateTime)
            //chat.viewType = MessageType.CHAT_PARTNER.index
            //Log.d(TAG, "Data : ${mSocket?.id()}")
            addItemToRecyclerView(chatMessage)
        }
    }

    var onConnect = Emitter.Listener {
        val data = Subscribe(senderId)
        val jsonData = gson.toJson(data)
        Log.d(TAG, "Data : ${mSocket?.id()}")
        val encryptedContent = cryptLib.encryptPlainTextWithRandomIV(jsonData, key)
        mSocket?.emit("subscribe", encryptedContent)

        val dataToken = TokenStore(senderId)
        val jsonDataToken = gson.toJson(dataToken)
        val encryptedContentToken = cryptLib.encryptPlainTextWithRandomIV(jsonDataToken, key)
        mSocket?.emit("tokenStore", encryptedContentToken)

        handler.postDelayed(r, 0)

    }

    var sentMessage = Emitter.Listener {
        val decryptedMessage = cryptLib.decryptCipherTextWithRandomIV(it[0].toString(), key)
        Log.e("messageAcknowledgment", decryptedMessage)
        mSocket?.emit("sentMessageAcknowledgment", it[0].toString())

    }

    private fun sendMessage() {
        val content = binding.etMessage.text.toString().trim()
        val encryptedContent = cryptLib.encryptPlainTextWithRandomIV(content, key)
        //val sendData = SendMessage(userName, content, roomName)
        val sendData = NewMessage("1111111", encryptedContent, receiverId, senderId)
        val jsonData = gson.toJson(sendData)
        Log.d("chat", "" + jsonData.toString())
        val encryptedContentData = cryptLib.encryptPlainTextWithRandomIV(jsonData, key)
        mSocket?.emit("newMessage", encryptedContentData)

        //val message = Message(senderId, content,receiverId,"", MessageType.CHAT_MINE.index)
        val chatMessage = ChatMessage(
            senderId, receiverId, encryptedContent, "")
        addItemToRecyclerView(chatMessage)
    }

    private fun addItemToRecyclerView(chatMessage: ChatMessage) {

        //Since this function is inside of the listener,
        // You need to do it on UIThread!
        //requireActivity().runOnUiThread {
        Log.e("Tag Chat", "" + chatMessage.dateTime)
        chatViewModel.insert(chatMessage)
        binding.etMessage.setText("")
        //}
    }

    val handler = Handler()
    val r: Runnable = object : Runnable {
        override fun run() {
            val dataHeartBeat = HeartBeat(senderId, receiverId)
            val jsonDataHeartBeat = gson.toJson(dataHeartBeat)
            val encryptedHeartBeat = cryptLib.encryptPlainTextWithRandomIV(jsonDataHeartBeat, key)
            mSocket?.emit("heartBeat", encryptedHeartBeat)
            handler.postDelayed(this, 1000)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacks(r)
        mSocket?.disconnect()
    }
}