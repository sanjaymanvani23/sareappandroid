package com.example.sare.chat.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.chat.model.Contacts
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import kotlinx.android.synthetic.main.item_invite_user.view.*

class AddUserAdapter(var list: List<Contacts>, private val context: Context) :
    RecyclerView.Adapter<AddUserAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddUserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_invite_user, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(list[position], context)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        if (list == null) {
            return 0;
        }
        return list.size
    }

    //the class is holding the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public fun bindItems(item: Contacts, context: Context) {
            val name = itemView.txtName
            val id = itemView.txtId

            val imgProfile = itemView.imgProfile
            val imgProfileBorder = itemView.imgProfileBorder
            val btnInvite = itemView.btnInvite

            name.text = item.name
            id.text = item.phone

            imgProfile.loadUrl("")
            imgProfileBorder.setBorder("#000000")

            btnInvite.setOnClickListener {
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                share.putExtra(Intent.EXTRA_TEXT, "Download ROLEIT App from Playstore")
                context.startActivity(Intent.createChooser(share, "Share to"))
            }

        }

    }

}