package com.example.sare.chat

import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.Telephony
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import com.bumptech.glide.Glide
import com.example.sare.R
import com.example.sare.Utils
import com.example.sare.appManager.StringSingleton
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.loadUrl
import com.example.sare.extensions.setBorder
import kotlinx.android.synthetic.main.activity_share_profile.*
import org.json.JSONObject

class ShareProfileActivity : AppCompatActivity() {
    var id: String? = null
    var image: String? = null
    var colorCode: String? = null
    var username: String? = null
    var linkToShare = ""
    lateinit var instaUri: Uri
    private val MY_CAMERA_PERMISSION_CODE = 100

    var coverImage: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_profile)
        getSharedPreferences()
        setData()
        setupUI()
    }

    private fun getSharedPreferences(){
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile,
            Context.MODE_PRIVATE
        )
        val user = preferences.getString("USER", null)
        val mainObject = JSONObject(user)
        id = mainObject.getString("_id")
        if (mainObject.has("image")) {
            image = mainObject.getString("image")
        }
        if (mainObject.has("coverImage")) {

            coverImage = mainObject.getString("coverImage")
        }
        colorCode = mainObject.get("colorCode").toString()
        username = mainObject.getString("username")
        image = Utils.PUBLIC_URL +(image.toString())
    }
    private fun setData() {
        val policy =
            StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build()
        StrictMode.setThreadPolicy(policy)
        tv_user_name!!.text = username
        val imageView = iv_cover_photo
        try {
            Glide.with(this)
                .load(Utils.PUBLIC_URL +(coverImage.toString()))
                .placeholder(R.drawable.cover_default)
                .error(R.drawable.cover_default)
                .into(imageView!!)
        } catch (e: Exception) {
            Glide.with(this)
                .load(R.drawable.cover_default)
                .error(R.drawable.cover_default)
                .into(imageView!!)
        }
        iv_profile_pic?.loadUrl(image.toString())
        ivProfilePicBorder?.setBorder(colorCode ?: Utils.COLOR_CODE)

    }

    private fun setupUI() {
        linkToShare = "SARE APP \n \n https://sare.in/$id"
        iv_back?.setOnClickListener {
            onBackPressed()
        }

        cv_facebook?.setOnClickListener {
            var facebookAppFound = false
            var shareIntent =
                Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)


            /*if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED ||
                requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                PackageManager.PERMISSION_DENIED

            ) {
                val permissions = arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
                //show popup to request runtime permission
                requestPermissions(permissions, MY_CAMERA_PERMISSION_CODE)
            } else {

                var shareIntent =
                    Intent(Intent.ACTION_SEND)
                // shareIntent.type = "text/plain"
                shareIntent.type = "image/*"
                val cardView: CardView? = cv_profile

                val bitmap =
                    getBitmapFromView(cardView!!)

                Log.d("tag", "bitmap:::::${bitmap}")

                val bytes = ByteArrayOutputStream()
                bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                val path: String = MediaStore.Images.Media.insertImage(
                    requireActivity().getContentResolver(),
                    bitmap,
                    "Title",
                    null
                )
                instaUri = Uri.parse(path)
                shareIntent.putExtra(Intent.EXTRA_STREAM, instaUri)*/
                //shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                shareIntent.setPackage("com.instagram.android")
                requireActivity().startActivity(shareIntent)
            }
*/
            val pm = packageManager
            val activityList: List<ResolveInfo> =
                pm.queryIntentActivities(shareIntent, 0)
            for (app in activityList) {
                if (app.activityInfo.packageName.contains("com.facebook.katana")) {
                    val activityInfo: ActivityInfo = app.activityInfo
                    val name =
                        ComponentName(
                            activityInfo.applicationInfo.packageName,
                            activityInfo.name
                        )
                    shareIntent.addCategory(Intent.CATEGORY_LAUNCHER)
                    shareIntent.component = name
                    facebookAppFound = true
                    break
                }
            }
            if (!facebookAppFound) {
                val url = linkToShare
                val sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=$url"
                shareIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl))
            }
           startActivity(shareIntent)
        }
        cv_insta?.setOnClickListener {
            try {
                /* if (requireActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                     PackageManager.PERMISSION_DENIED ||
                     requireActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                     PackageManager.PERMISSION_DENIED
 
                 ) {
                     val permissions = arrayOf(
                         Manifest.permission.READ_EXTERNAL_STORAGE,
                         Manifest.permission.WRITE_EXTERNAL_STORAGE
                     )
                     //show popup to request runtime permission
                     requestPermissions(permissions, MY_CAMERA_PERMISSION_CODE)
                 } else {*/

                val shareIntent =
                    Intent(Intent.ACTION_SEND)
                // shareIntent.type = "text/plain"
                shareIntent.type = "image/*"
                /* val cardView: CardView? = cv_profile

                 val bitmap =
                     getBitmapFromView(cardView!!)

                 Log.d("tag", "bitmap:::::${bitmap}")

                 val bytes = ByteArrayOutputStream()
                 bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                 val path: String = MediaStore.Images.Media.insertImage(
                     requireActivity().getContentResolver(),
                     bitmap,
                     "Title",
                     null
                 )
                 instaUri = Uri.parse(path)
                 shareIntent.putExtra(Intent.EXTRA_STREAM, instaUri)*/
                shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                shareIntent.setPackage("com.instagram.android")
                startActivity(shareIntent)
                //}
            } catch (e: Exception) {
                e.printStackTrace()
                Log.e("tag", "error:::${e.message}")
                customToastMain("Instagram has not been installed",0)
            }

        }

        cv_whats_app?.setOnClickListener {
            val pm: PackageManager = packageManager
            try {
                val waIntent =
                    Intent(Intent.ACTION_SEND)

                waIntent.type = "text/plain"
                val info: PackageInfo =
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA)
                //Check if package exists or not. If not then code
                //in catch block will be called
                waIntent.setPackage("com.whatsapp")
                waIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                startActivity(Intent.createChooser(waIntent, "Share to"))
            } catch (e: PackageManager.NameNotFoundException) {
                customToastMain(getString(R.string.share_whatsapp_not_instaled),0)
            }
        }

        cv_copy?.setOnClickListener {
            val clipboardManager =
                getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clipData = ClipData.newPlainText("text", linkToShare)
            clipboardManager.primaryClip = clipData
            customToastMain("Link copied to clipboard!!",1)
        }
        ll_invite_sms?.setOnClickListener {
            if (getDefaultSmsAppPackageName(this) != null) {
                val smsUri =
                    Uri.parse("smsto:")
                val intent = Intent(Intent.ACTION_VIEW, smsUri)
                intent.putExtra("sms_body", linkToShare)
                intent.type = "vnd.android-dir/mms-sms"
                startActivity(intent)
            }

        }
        ll_invite_email?.setOnClickListener {
            //            //need this to prompts email client only
            //            //need this to prompts email client only
            val send = Intent(Intent.ACTION_SENDTO)
            val uriText = "mailto:" +
                    "?subject=" + Uri.encode("Download Sare App") +
                    "&body=" + Uri.encode(linkToShare)
            val uri = Uri.parse(uriText)
            send.data = uri
            try {
                startActivity(Intent.createChooser(send, "Share to"))
            } catch (e: Exception) {
                e.printStackTrace()
                customToastMain("Mail not send",0)
            }
        }

        ll_share_link?.setOnClickListener {
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.putExtra(Intent.EXTRA_TEXT, linkToShare)
            startActivity(Intent.createChooser(share, "Share to"))

        }
    }

    fun getDefaultSmsAppPackageName(@NonNull context: Context): String? {
        val defaultSmsPackageName: String
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context)
            return defaultSmsPackageName
        } else {
            val intent = Intent(Intent.ACTION_VIEW)
                .addCategory(Intent.CATEGORY_DEFAULT).setType("vnd.android-dir/mms-sms")
            val resolveInfos =
                context.packageManager.queryIntentActivities(intent, 0)
            if (resolveInfos != null && !resolveInfos.isEmpty()) return resolveInfos[0].activityInfo.packageName
        }
        return null
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode === MY_CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                val shareIntent =
                    Intent(Intent.ACTION_SEND)
                shareIntent.type = "image/*"
                shareIntent.putExtra(Intent.EXTRA_STREAM, instaUri)
                shareIntent.putExtra(Intent.EXTRA_TEXT, linkToShare)
                shareIntent.setPackage("com.instagram.android")
                startActivity(shareIntent)
            } else {
                customToastMain(getString(R.string.permission_denied), 0)
            }
        }
    }
}