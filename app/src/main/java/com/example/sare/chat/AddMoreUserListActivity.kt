package com.example.sare.chat

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.adapter.SearchUserListAdapter
import kotlinx.android.synthetic.main.activity_chat_users_list.*

class AddMoreUserListActivity : AppCompatActivity() {
    var list: ArrayList<String> = ArrayList()
    var title: String ?= null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_users_list)
        setTheme()
        title = intent.getStringExtra("title")
        txtTitle.text = title.toString()
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        list.add("")
        /*rvList.apply {
            layoutManager = LinearLayoutManager(
                rvList.context,
                RecyclerView.VERTICAL, false
            )
            adapter = SearchUserListAdapter(
                list,
                context
            )

        }*/
        setListeners()
    }

    private fun setListeners() {
        imgBack?.setOnClickListener {
            onBackPressed()
        }
    }
    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }
}