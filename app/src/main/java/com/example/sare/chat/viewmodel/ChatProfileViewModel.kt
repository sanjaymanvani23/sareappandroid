package com.example.sare.chat.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.*
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import kotlinx.coroutines.runBlocking

class ChatProfileViewModel(application: Application) : AndroidViewModel(application) {
    private val context = getApplication<Application>().applicationContext

    var userDetails = MutableLiveData<NetworkResponse>()
    var reportArrayList = MutableLiveData<NetworkResponse>()
    var muteChat = MutableLiveData<NetworkResponse>()
    var unMuteChat = MutableLiveData<NetworkResponse>()
    var blockChat = MutableLiveData<NetworkResponse>()
    var unBlockChat = MutableLiveData<NetworkResponse>()

    var apolloClient: com.apollographql.apollo.ApolloClient? = null

    init {
        val preferences: SharedPreferences = context.getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        val token = preferences.getString("TOKEN", null)
        Log.d("tag", "token::$token")
        apolloClient = com.example.sare.ApolloClient.setupApollo(token ?: "")
    }

    fun getUserDetails(id: String) {
        apolloClient?.query(UserDetailsQuery(id))?.enqueue(object : ApolloCall.Callback<UserDetailsQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                e.printStackTrace()
                Log.d("tag", "Error::" + e.message)
                userDetails.postValue(NetworkResponse.ERROR(e))

            }

            override fun onResponse(response: Response<UserDetailsQuery.Data>) {
                Log.d("tag", "Response User Data::$response")
                try {
                    if (response.data()?.user() != null) {
                        userDetails.postValue(
                            NetworkResponse.SUCCESS.getUserDetails(
                                response.data()!!))
                    } else {
                        val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                        NetworkResponse.ERROR_AUTHENTICATION(
                            response.errors?.get(0)?.message.toString(), errorCode.toString())
                        userDetails.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                    }
                } catch (e: Exception) {
                    userDetails.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                }
            }
        })
    }

    fun muteChat(senderId: String) {
        apolloClient?.mutate(MuteChatMutation(senderId))
            ?.enqueue(object : ApolloCall.Callback<MuteChatMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    muteChat.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<MuteChatMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.muteChat() != null) {
                            muteChat.postValue(
                                NetworkResponse.SUCCESS.muteChat(
                                    response.data?.muteChat()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            muteChat.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        muteChat.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun unMuteChat(senderId: String) {
        apolloClient?.mutate(UnmuteChatMutation(senderId))
            ?.enqueue(object : ApolloCall.Callback<UnmuteChatMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    unMuteChat.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<UnmuteChatMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.unmuteChat() != null) {
                            unMuteChat.postValue(
                                NetworkResponse.SUCCESS.unMuteChat(
                                    response.data?.unmuteChat()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            unMuteChat.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        unMuteChat.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun blockChat(senderId: String) {
        apolloClient?.mutate(BlockChatMutation(senderId))
            ?.enqueue(object : ApolloCall.Callback<BlockChatMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    blockChat.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<BlockChatMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.blockChat() != null) {
                            blockChat.postValue(
                                NetworkResponse.SUCCESS.blockChat(
                                    response.data?.blockChat()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            blockChat.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        blockChat.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun unBlockChat(senderId: String) {
        apolloClient?.mutate(UnblockChatMutation(senderId))
            ?.enqueue(object : ApolloCall.Callback<UnblockChatMutation.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    Log.d("tag", "Error::" + e.message)
                    unBlockChat.postValue(NetworkResponse.ERROR(e))

                }

                override fun onResponse(response: Response<UnblockChatMutation.Data>) {
                    Log.d("tag", "Response User Data::$response")
                    try {
                        if (response.data()?.acceptChat() != null) {
                            unBlockChat.postValue(
                                NetworkResponse.SUCCESS.acceptChat(
                                    response.data?.acceptChat()!!))
                        } else {
                            val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                            NetworkResponse.ERROR_AUTHENTICATION(
                                response.errors?.get(0)?.message.toString(), errorCode.toString())
                            unBlockChat.postValue(NetworkResponse.ERROR_RESPONSE(response.errors?.get(0)?.message.toString()))
                        }
                    } catch (e: Exception) {
                        unBlockChat.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                    }
                }
            })
    }

    fun getReportList() {
        apolloClient?.query(ReportListQuery.builder().build())
            ?.enqueue(object : ApolloCall.Callback<ReportListQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    reportArrayList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<ReportListQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data?.reportType() != null) {
                                reportArrayList.postValue(
                                    NetworkResponse.SUCCESS.getReportList(
                                        response.data?.reportType()!!))
                            } else {
                                val errorCode = response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(), errorCode.toString())
                                reportArrayList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0)?.message.toString()))
                            }
                        } catch (e: Exception) {
                            reportArrayList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))

                        }
                    }
                }
            })
    }
}