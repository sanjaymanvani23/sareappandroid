package com.example.sare.chat

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sare.CheckUsersFromContactListQuery
import com.example.sare.CustomProgressDialogNew
import com.example.sare.MyApplication
import com.example.sare.R
import com.example.sare.appManager.AppUtill
import com.example.sare.appManager.NetworkResponse
import com.example.sare.appManager.StringSingleton
import com.example.sare.appManager.ThemeManager
import com.example.sare.chat.adapter.AddUserAdapter
import com.example.sare.chat.adapter.SearchUserListAdapter
import com.example.sare.chat.model.Contacts
import com.example.sare.chat.viewmodel.AddUserViewModel
import com.example.sare.extensions.customToastMain
import com.example.sare.extensions.gone
import com.example.sare.extensions.openNewActivity
import com.example.sare.extensions.visible
import com.example.sare.ui.login.LoginActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_add_user.*

@RequiresApi(Build.VERSION_CODES.M) class AddUserActivity : AppCompatActivity() {
    var listPhoneNumbers: ArrayList<String> = ArrayList()
    var list: ArrayList<Contacts> = ArrayList()
    var addUserViewModel: AddUserViewModel? = null
    var token: String = ""
    var usersUsingAppList: List<CheckUsersFromContactListQuery.CheckUsersExist> = ArrayList()
    lateinit var progressDialog : CustomProgressDialogNew

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_add_user)
        getSharedPreferences()
        progressDialog = CustomProgressDialogNew(this)
        addUserViewModel = ViewModelProvider(this)[AddUserViewModel::class.java]
        checkContactsPermission()
        setTheme()
        setListeners()
        getUsersListUsingApp()
    }

    private fun getSharedPreferences() {
        val preferences: SharedPreferences = getSharedPreferences(
            StringSingleton.sharedPrefFile, Context.MODE_PRIVATE)
        token = preferences.getString("TOKEN", null)
    }

    private fun checkContactsPermission() {
        if (checkSelfPermission(
                Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_DENIED && checkSelfPermission(
                Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_DENIED
        ) {
            val permissions = arrayOf(
                Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS)
            //show popup to request runtime permission
            requestPermissions(permissions, 105)
        } else {
            getAllContactsFromPhone()
        }
    }

    private fun getAllContactsFromPhone() {
        val phones: Cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)
        var phoneNumber = ""
        var name = ""
        list.clear()
        while (phones.moveToNext()) {
             name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
             phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
            Log.d("tag", "phoneNumber::$phoneNumber")

            phoneNumber = phoneNumber.replace(" ","")
            phoneNumber = phoneNumber.replace("*","")
            phoneNumber = phoneNumber.replace("#","")
            phoneNumber = phoneNumber.replace("+","")
            phoneNumber = phoneNumber.replace("-","")
            phoneNumber = phoneNumber.replace("_","")
            listPhoneNumbers.add(phoneNumber)
            val contactsModel = Contacts()
            contactsModel.name = name
            contactsModel.phone = phoneNumber.trim()
            list.add(contactsModel)
        }
        phones.close()
    }


    private fun initInviteUserRecyclerView(list: List<Contacts>) {
        progressDialog.hide()
        rvInviteUser?.apply {
            layoutManager = LinearLayoutManager(
                rvInviteUser?.context, RecyclerView.VERTICAL, false)
            adapter = AddUserAdapter(
                list, this@AddUserActivity)

            if (adapter?.itemCount != 0) {
                relInvitees.visible()
                txtUserOnSareVids.visible()
            } else {
                relInvitees.gone()
                txtUserOnSareVids.gone()
            }

        }
    }

    private fun getUsersListUsingApp() {
        runOnUiThread {
            progressDialog.show()
        }
        addUserViewModel?.getUsersListUsingApp(listPhoneNumbers, token.toString())
        addUserViewModel?.totalUsersList?.observe(this, Observer {
            runOnUiThread {
                progressDialog.hide()
            }
            when (it) {
                is NetworkResponse.ERROR -> {
                    it.error.printStackTrace()
                    val snackbar: Snackbar = Snackbar.make(
                        findViewById(R.id.relAddUserActivity), "Connection Error!", Snackbar.LENGTH_SHORT)
                        .setAction("RETRY") {
                            addUserViewModel?.getUsersListUsingApp(listPhoneNumbers, token.toString())
                        }
                    snackbar.show()
                }
                is NetworkResponse.ERROR_AUTHENTICATION -> {
                    customToastMain(it.errorMsg, 0)
                    MyApplication.mainActivity.logoutUser()
                    openNewActivity(LoginActivity::class.java)
                }

                is NetworkResponse.ERROR_RESPONSE -> {
                    customToastMain(it.errorMsg, 0)
                }

                is NetworkResponse.SUCCESS.getUsersListUsingApp -> {
                    runOnUiThread {
                        progressDialog.hide()
                    }
                    usersUsingAppList = it.response
                    initUsersUsingAppRecyclerView(usersUsingAppList)
                    val iterator = list.iterator()
                    while (iterator.hasNext()) {
                        val item = iterator.next()
                        for (element in usersUsingAppList) {
                            if (item.phone == element.mobile()) {
                                iterator.remove()
                            }
                        }
                    }
                    initInviteUserRecyclerView(list)
                }

            }
        })
    }

    private fun initUsersUsingAppRecyclerView(list: List<CheckUsersFromContactListQuery.CheckUsersExist>) {
        rvFollowUserList?.apply {
            layoutManager = LinearLayoutManager(
                rvFollowUserList?.context, RecyclerView.VERTICAL, false)
            adapter = SearchUserListAdapter(list, context)

            if (adapter?.itemCount != 0) {
                relFollow.visible()
                txtFriends.visible()
            } else {
                relFollow.gone()
                txtFriends.gone()
            }
        }
    }

    private fun filter(text: String) {
        val filteredContacts: ArrayList<Contacts> = ArrayList()
        val filterUsers: ArrayList<CheckUsersFromContactListQuery.CheckUsersExist> = ArrayList()

        if (list.isNotEmpty() && usersUsingAppList.isNotEmpty()) {
            for (s in list) {
                if (s.name!!.toLowerCase().contains(text.toLowerCase())) {
                    filteredContacts.add(s)
                }
            }
            for (s in usersUsingAppList) {
                if (s.name()!!.toLowerCase().contains(text.toLowerCase())) {
                    filterUsers.add(s)
                }
            }
        }
        initInviteUserRecyclerView(filteredContacts)
        initUsersUsingAppRecyclerView(filterUsers)
    }


    private fun setTheme() {
        window.statusBarColor = ThemeManager.colors(
            this, StringSingleton.bodyBackground)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        if (AppUtill().isDarkMode(this)) {
            window.navigationBarColor = ThemeManager.colors(
                this, StringSingleton.bodyBackground)
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 105) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permission Granted
                //Do your work here
                //Perform operations here only which requires permission
                getAllContactsFromPhone()
            } else {
                customToastMain(getString(R.string.permission_denied), 0)
                onBackPressed()

            }
        }
    }

    private fun setListeners() {
        imgBack?.setOnClickListener {
            onBackPressed()
        }

        nestedScrollView?.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            Log.d("tag","scrollY::::$scrollY")
            if (scrollY > 1500) {
                btnGoUp?.visible()
            } else {
                btnGoUp?.gone()
            }
        }
        btnGoUp?.setOnClickListener {
            nestedScrollView?.smoothScrollTo(0, 0)
        }
        txtSeeAll?.setOnClickListener {
            startActivity(Intent(this, AddMoreUserListActivity::class.java).putExtra("title", "Friends"))
            overridePendingTransition(0, 0)
        }

        txtSeeAllContacts?.setOnClickListener {
            startActivity(Intent(this, AddMoreUserListActivity::class.java).putExtra("title", "Contacts"))
            overridePendingTransition(0, 0)
        }

        txtSeeAllInvitees?.setOnClickListener {
            startActivity(Intent(this, AddMoreUserListActivity::class.java).putExtra("title", "Invite To SareVids"))
            overridePendingTransition(0, 0)
        }

        imgClose?.setOnClickListener {
            etSearchUser?.setText("")
        }

        etSearchUser?.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                imgClose.visible()
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

}