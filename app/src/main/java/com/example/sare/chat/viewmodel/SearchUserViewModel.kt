package com.example.sare.chat.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.example.sare.ApolloClient
import com.example.sare.UsersOnSareVidsQuery
import com.example.sare.Utils
import com.example.sare.appManager.NetworkResponse
import kotlinx.coroutines.runBlocking

class SearchUserViewModel : ViewModel() {
    var totalUsersList = MutableLiveData<NetworkResponse>()

    fun getTotalUsersList(pageCount: Int, token: String) {
        val apolloClient = ApolloClient.setupApollo(token.toString())
        apolloClient.query(UsersOnSareVidsQuery(pageCount, Utils.PAGE_SIZE, "createdAt", "", ""))
            .enqueue(object : ApolloCall.Callback<UsersOnSareVidsQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    e.printStackTrace()
                    totalUsersList.postValue(NetworkResponse.ERROR(e))
                }

                override fun onResponse(response: Response<UsersOnSareVidsQuery.Data>) {
                    runBlocking {
                        try {
                            if (response.data()?.users() != null) {
                                totalUsersList.postValue(
                                    NetworkResponse.SUCCESS.getUsersOnSareVids(
                                        response.data()?.users()!!.user()!!
                                    )
                                )
                            } else {
                                val errorCode =
                                    response.errors?.get(0)?.customAttributes?.get("status")
                                NetworkResponse.ERROR_AUTHENTICATION(
                                    response.errors?.get(0)?.message.toString(),
                                    errorCode.toString()
                                )
                                totalUsersList.postValue(
                                    NetworkResponse.ERROR_RESPONSE(
                                        response.errors?.get(
                                            0
                                        )?.message.toString()
                                    )
                                )
                            }
                        } catch (e: Exception) {
                            totalUsersList.postValue(NetworkResponse.ERROR_RESPONSE("Something went wrong!"))
                        }
                    }
                }
            })
    }

}