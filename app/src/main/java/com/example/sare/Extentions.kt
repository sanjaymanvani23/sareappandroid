package com.example.sare

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.util.Log
import android.util.TypedValue
import android.widget.ImageView
import android.widget.TextView
import ch.qos.logback.core.util.Loader.getResources
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import com.bumptech.glide.request.target.CustomTarget
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min


fun Date.toSimpleString(dateFormat: String = "dd/MM/yyyy"): String {
    val format = SimpleDateFormat(dateFormat)
    return format.format(this)
}

/**
 * Load model into ImageView as a circle image with borderSize (optional) using Glide
 *
 * @param model - Any object supported by Glide (Uri, File, Bitmap, String, resource id as Int, ByteArray, and Drawable)
 * @param borderSize - The border size in pixel
 * @param borderColor - The border color
 */

@JvmOverloads
fun <T> TextView.makePositiveValue(model: TextView) {

    model.setText(0)

}


fun <T> ImageView.loadCircularImage(
    model: T,
    borderSize: Float = 0F,
    borderColor: Int = Color.WHITE,
    defaultImage: Int,
    bitmapNew: Bitmap?, color: String?,
    context: Context
) {

    Glide.with(context)
        .asBitmap()
        .load(model)
        .placeholder(defaultImage)
        .error(defaultImage)
        .into(object : BitmapImageViewTarget(this) {
            override fun setResource(resource: Bitmap?) {

                Log.d("tag", "resource::$resource")
                imageShape(bitmapNew, context, this@loadCircularImage, color)
            }
        })



}

fun <T> ImageView.loadDiamondImage(
    model: T,
    borderColor: String?,
    context: Context
) {
    synchronized(context) {
        Glide.with(this)
            .asBitmap()
            .load(model)
            .placeholder(R.drawable.ic_profile_icon_1)
            .error(R.drawable.ic_profile_icon_1)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(
                    resource: Bitmap,
                    transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                ) {
                    Log.d("tag", "Glide : onResourceReady : +$borderColor")
                    imageShape(resource, context, this@loadDiamondImage, borderColor)
                }

                override fun onLoadStarted(placeholder: Drawable?) {
                    super.onLoadStarted(placeholder)
                    Log.d("tag", "Glide : onLoadStarted")
                    val icon = BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_profile_icon_1
                    )
                    imageShape(icon, context, this@loadDiamondImage, borderColor)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                    Log.d("tag", "Glide : onLoadCleared")
                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    super.onLoadFailed(errorDrawable)
                    Log.d("tag", "Glide : onLoadFailed")
                    val icon = BitmapFactory.decodeResource(
                        context.resources,
                        R.drawable.ic_profile_icon_1
                    )
                    imageShape(icon, context, this@loadDiamondImage, borderColor)

                }


            })
    }
}

private fun imageShape(
    bitmapNew: Bitmap?,
    context: Context,
    imgView: ImageView,
    borderColor: String?
) {
    val color = borderColor
    val bitmap =
        BitmapFactory.decodeResource(
            context.resources,
            R.drawable.ic_email_verification_lt
        )
    var original = bitmapNew
    val expectedHeight = 200
    val expectedWidth = 200

    original = getResizedBitmap(
        original!!,
        dipToPixels(
            context,
            expectedHeight.toFloat()
        ),
        dipToPixels(
            context,
            expectedWidth.toFloat()
        )
    )

    val result =
        Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
    val mCanvas = Canvas(result).apply {
        // Set transparent initial area
        drawARGB(0, 0, 0, 0)
    }

    val paint = Paint()

    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN)

    // Draw the transparent initial area
    paint.isAntiAlias = true
    paint.style = Paint.Style.FILL

    val widthMask = bitmap.width
    val heightMask = bitmap.height
    val centerX = (widthMask - original!!.width) * 0.5f
    val centerY = (heightMask - original.height) * 0.5f


    paint.isAntiAlias = true
    paint.style = Paint.Style.STROKE
    //paint.color = Color.BLUE
    //paint.strokeWidth = 10f

    mCanvas.drawBitmap(original, centerX, centerY, null)
    //mCanvas.drawBitmap(original, 0f, 0f, null)
    mCanvas.drawBitmap(bitmap, 0f, 0f, paint)
    paint.xfermode = null

    imgView.scaleType = ImageView.ScaleType.CENTER_INSIDE
    //imgProfilePic.setPadding(8, 8, 6, 6)
    imgView.setPadding(
        Utils.dpToPx(context, 2.5f),
        Utils.dpToPx(context, 2.5f),
        Utils.dpToPx(context, 2f),
        Utils.dpToPx(context, 2f)
    )
  /*  imgView.setPadding(
        Utils.dpToPx(context, 2f),
        Utils.dpToPx(context, 2f),
        Utils.dpToPx(context, 1.5f),
        Utils.dpToPx(context, 1.5f)
    )*/
    try {
        Log.d("tag", "colorCode try:::::::$color")

        imgView.background =
            context.resources.getDrawable(R.drawable.ic_email_verification_dk, context.theme)
            context.resources.getDrawable(R.drawable.ic_email_verification_dk).setColorFilter(
                Color.parseColor(color),
                PorterDuff.Mode.SRC_IN
            )
    } catch (e: Exception) {
        Log.d("tag", "colorCode Catch::::::$color")
        e.printStackTrace()
        imgView.background =
            context.resources.getDrawable(R.drawable.ic_email_verification_dk, context.theme)
            context.resources.getDrawable(R.drawable.ic_email_verification_dk).setColorFilter(
            Color.parseColor("#000000"),
            PorterDuff.Mode.SRC_IN
        )
    }

    imgView.setImageBitmap(result)
    //imgView.adjustViewBounds = true
}

//    imgProfilePic.setPadding(17, 4, 17, 0)


//    if (color != null) {
private fun getResizedBitmap(
    image: Bitmap,
    newHeight: Float,
    newWidth: Float
): Bitmap? {
    val width = image.width
    val height = image.height
    val scaleWidth = newWidth / width
    val scaleHeight = newHeight / height
    val matrix = Matrix()
    matrix.postScale(scaleWidth, scaleHeight)
    return Bitmap.createBitmap(image, 0, 0, width, height, matrix, false)
}

fun dipToPixels(context: Context, dipValue: Float): Float {
    val metrics = context.resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, metrics)
}

/**
 * Create a new bordered bitmap with the specified borderSize and borderColor
 *
 * @param borderSize - The border size in pixel
 * @param borderColor - The border color
 * @return A new bordered bitmap with the specified borderSize and borderColor
 */
fun Bitmap.createBitmapWithBorder(borderSize: Float, borderColor: Int = Color.WHITE): Bitmap {
    val borderOffset = (borderSize * 2).toInt()
    val halfWidth = width / 2
    val halfHeight = height / 2
    val circleRadius = min(halfWidth, halfHeight).toFloat()
    val newBitmap = Bitmap.createBitmap(
        width + borderOffset,
        height + borderOffset,
        Bitmap.Config.ARGB_8888
    )

    // Center coordinates of the image
    val centerX = halfWidth + borderSize
    val centerY = halfHeight + borderSize

    val paint = Paint()
    val canvas = Canvas(newBitmap).apply {
        // Set transparent initial area
        drawARGB(0, 0, 0, 0)
    }

    // Draw the transparent initial area
    paint.isAntiAlias = true
    paint.style = Paint.Style.FILL
    canvas.drawCircle(centerX, centerY, circleRadius, paint)

    // Draw the image
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawBitmap(this, borderSize, borderSize, paint)

    // Draw the createBitmapWithBorder
    paint.xfermode = null
    paint.style = Paint.Style.STROKE
    paint.color = borderColor
    paint.strokeWidth = borderSize
    canvas.drawCircle(centerX, centerY, circleRadius, paint)
    return newBitmap
}
